<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Suggested_imagings_patient;
use Faker\Generator as Faker;

$factory->define(Suggested_imagings_patient::class, function (Faker $faker) {

    return [
        'si_patient_id' => $faker->randomDigitNotNull,
        'si_doctor_id' => $faker->randomDigitNotNull,
        'suggested_imagings' => $faker->text,
        'created_by' => $faker->randomDigitNotNull,
        'updated_by' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
