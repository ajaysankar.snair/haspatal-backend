<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Service_details;
use Faker\Generator as Faker;

$factory->define(Service_details::class, function (Faker $faker) {

    return [
        'service' => $faker->word,
        'status' => $faker->word,
        'created_by' => $faker->word,
        'updated_by' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
