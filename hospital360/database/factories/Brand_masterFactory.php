<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Brand_master;
use Faker\Generator as Faker;

$factory->define(Brand_master::class, function (Faker $faker) {

    return [
        'brand_id' => $faker->word,
        'brand_name' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
