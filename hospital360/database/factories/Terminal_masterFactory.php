<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Terminal_master;
use Faker\Generator as Faker;

$factory->define(Terminal_master::class, function (Faker $faker) {

    return [
        'terminal_name' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
