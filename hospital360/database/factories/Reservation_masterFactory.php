<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Reservation_master;
use Faker\Generator as Faker;

$factory->define(Reservation_master::class, function (Faker $faker) {

    return [
        'r_service_id' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
