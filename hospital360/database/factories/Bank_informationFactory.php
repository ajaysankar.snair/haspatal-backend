<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Bank_information;
use Faker\Generator as Faker;

$factory->define(Bank_information::class, function (Faker $faker) {

    return [
        'bank_name' => $faker->word,
        'account_name' => $faker->word,
        'ifsc_code' => $faker->word,
        'account_no' => $faker->word,
        'cancle_cheque' => $faker->word,
        'status' => $faker->word,
        'created_by' => $faker->word,
        'updated_by' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
