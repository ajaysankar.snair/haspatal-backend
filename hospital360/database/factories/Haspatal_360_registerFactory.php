<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Haspatal_360_register;
use Faker\Generator as Faker;

$factory->define(Haspatal_360_register::class, function (Faker $faker) {

    return [
        'name_prefix' => $faker->word,
        'full_name' => $faker->word,
        'email' => $faker->word,
        'mo_prefix' => $faker->word,
        'mobile_no' => $faker->word,
        'created_by' => $faker->randomDigitNotNull,
        'updated_by' => $faker->randomDigitNotNull,
        'status' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
