<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Prescription_details;
use Faker\Generator as Faker;

$factory->define(Prescription_details::class, function (Faker $faker) {

    return [
        'patient_id' => $faker->randomDigitNotNull,
        'doctor_id' => $faker->randomDigitNotNull,
        'booking_id' => $faker->randomDigitNotNull,
        'prescription' => $faker->text,
        'prescription_image' => $faker->word,
        'status' => $faker->randomDigitNotNull,
        'created_by' => $faker->randomDigitNotNull,
        'updated_by' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
