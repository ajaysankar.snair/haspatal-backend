<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Present_complaint_patient;
use Faker\Generator as Faker;

$factory->define(Present_complaint_patient::class, function (Faker $faker) {

    return [
        'pc_patient_id' => $faker->randomDigitNotNull,
        'pc_doctor_id' => $faker->randomDigitNotNull,
        'book_id' => $faker->text,
        'present_complaint' => $faker->word,
        'complaint_pic' => $faker->word,
        'complaint_pdf' => $faker->word,
        'created_by' => $faker->randomDigitNotNull,
        'updated_by' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
