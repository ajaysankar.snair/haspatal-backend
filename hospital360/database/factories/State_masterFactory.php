<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\State_master;
use Faker\Generator as Faker;

$factory->define(State_master::class, function (Faker $faker) {

    return [
        'country_id' => $faker->randomDigitNotNull,
        'state_name' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
