<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Video_like_dislike;
use Faker\Generator as Faker;

$factory->define(Video_like_dislike::class, function (Faker $faker) {

    return [
        'vl_video_id' => $faker->randomDigitNotNull,
        'vl_user_id' => $faker->randomDigitNotNull,
        'vl_status' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
