<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Video;
use Faker\Generator as Faker;

$factory->define(Video::class, function (Faker $faker) {

    return [
        'user_id' => $faker->randomDigitNotNull,
        'description' => $faker->word,
        'video' => $faker->word,
        'thum' => $faker->word,
        'gif' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
