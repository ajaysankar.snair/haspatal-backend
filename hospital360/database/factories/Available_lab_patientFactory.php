<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Available_lab_patient;
use Faker\Generator as Faker;

$factory->define(Available_lab_patient::class, function (Faker $faker) {

    return [
        'a_patient_id' => $faker->randomDigitNotNull,
        'a_doctor_id' => $faker->randomDigitNotNull,
        'available_lab' => $faker->text,
        'created_by' => $faker->randomDigitNotNull,
        'updated_by' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
