<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Color_master;
use Faker\Generator as Faker;

$factory->define(Color_master::class, function (Faker $faker) {

    return [
        'co_name' => $faker->word,
        'co_code' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
