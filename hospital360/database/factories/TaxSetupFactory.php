<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\TaxSetup;
use Faker\Generator as Faker;

$factory->define(TaxSetup::class, function (Faker $faker) {

    return [
        'tax_name' => $faker->word,
        'rate' => $faker->randomDigitNotNull,
        'type' => $faker->word,
        'duration' => $faker->word,
        'status' => $faker->word,
        'created_by' => $faker->randomDigitNotNull,
        'updated_by' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
