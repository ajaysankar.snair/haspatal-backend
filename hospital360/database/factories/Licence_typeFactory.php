<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Licence_type;
use Faker\Generator as Faker;

$factory->define(Licence_type::class, function (Faker $faker) {

    return [
        'licence_name' => $faker->word,
        'status' => $faker->word,
        'created_by' => $faker->word,
        'updated_by' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
