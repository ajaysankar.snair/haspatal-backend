<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\City_master;
use Faker\Generator as Faker;

$factory->define(City_master::class, function (Faker $faker) {

    return [
        'city_name' => $faker->word,
        'city_state_id' => $faker->randomDigitNotNull,
        'city_country_id' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
