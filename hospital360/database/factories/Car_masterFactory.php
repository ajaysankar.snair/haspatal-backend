<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Car_master;
use Faker\Generator as Faker;

$factory->define(Car_master::class, function (Faker $faker) {

    return [
        'car_name' => $faker->word,
        'brand' => $faker->randomDigitNotNull,
        'car_type' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
