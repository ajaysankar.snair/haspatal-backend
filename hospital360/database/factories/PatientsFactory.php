<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Patients;
use Faker\Generator as Faker;

$factory->define(Patients::class, function (Faker $faker) {

    return [
        'first_name' => $faker->word,
        'last_name' => $faker->word,
        'email' => $faker->word,
        'mobile' => $faker->word,
        'country' => $faker->word,
        'state' => $faker->word,
        'city' => $faker->word,
        'zipcode' => $faker->word,
        'address' => $faker->word,
        'personal_profile' => $faker->word,
        'family_profile' => $faker->word,
        'social_profile' => $faker->word,
        'work_profile' => $faker->word,
        'contact_phone1' => $faker->word,
        'contact_phone2' => $faker->word,
        'contact_phone3' => $faker->word,
        'email1' => $faker->word,
        'email2' => $faker->word,
        'language' => $faker->word,
        'profile_pic' => $faker->word,
        'status' => $faker->word,
        'created_by' => $faker->word,
        'updated_by' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
