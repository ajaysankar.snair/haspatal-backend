<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Doctor_availability;
use Faker\Generator as Faker;

$factory->define(Doctor_availability::class, function (Faker $faker) {

    return [
        'doctor_id' => $faker->word,
        'date' => $faker->word,
        'time_slot' => $faker->word,
        'status' => $faker->word,
        'created_by' => $faker->word,
        'updated_by' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
