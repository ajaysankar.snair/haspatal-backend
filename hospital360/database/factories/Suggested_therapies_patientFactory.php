<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Suggested_therapies_patient;
use Faker\Generator as Faker;

$factory->define(Suggested_therapies_patient::class, function (Faker $faker) {

    return [
        'st_patient_id' => $faker->randomDigitNotNull,
        'st_doctor_id' => $faker->randomDigitNotNull,
        'suggested_therapies' => $faker->text,
        'created_by' => $faker->randomDigitNotNull,
        'updated_by' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
