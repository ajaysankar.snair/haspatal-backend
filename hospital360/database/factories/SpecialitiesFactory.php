<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Specialities;
use Faker\Generator as Faker;

$factory->define(Specialities::class, function (Faker $faker) {

    return [
        'specility' => $faker->word,
        'description' => $faker->word,
        'status' => $faker->word,
        'created_by' => $faker->word,
        'updated_by' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
