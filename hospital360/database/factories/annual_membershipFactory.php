<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\annual_membership;
use Faker\Generator as Faker;

$factory->define(annual_membership::class, function (Faker $faker) {

    return [
        'srevice' => $faker->randomDigitNotNull,
        'start_date' => $faker->word,
        'end_date' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
