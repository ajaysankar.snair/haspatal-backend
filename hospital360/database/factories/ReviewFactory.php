<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Review;
use Faker\Generator as Faker;

$factory->define(Review::class, function (Faker $faker) {

    return [
        'patient_id' => $faker->randomDigitNotNull,
        'doctor_id' => $faker->randomDigitNotNull,
        'book_id' => $faker->word,
        'rating' => $faker->randomDigitNotNull,
        'review_que' => $faker->word,
        'review_ans' => $faker->word,
        'created_by' => $faker->randomDigitNotNull,
        'updated_by' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
