<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Employe;
use Faker\Generator as Faker;

$factory->define(Employe::class, function (Faker $faker) {

    return [
        'em_name' => $faker->word,
        'em_email' => $faker->word,
        'em_mobile_no' => $faker->word,
        'em_country_id' => $faker->randomDigitNotNull,
        'em_state_id' => $faker->randomDigitNotNull,
        'em_city_id' => $faker->randomDigitNotNull,
        'em_address' => $faker->word,
        'em_car_type' => $faker->randomDigitNotNull,
        'em_car_pic' => $faker->word,
        'em_car_no' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
