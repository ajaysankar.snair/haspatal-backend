<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Doctor_withdraw;
use Faker\Generator as Faker;

$factory->define(Doctor_withdraw::class, function (Faker $faker) {

    return [
        'doctor_id' => $faker->randomDigitNotNull,
        'current_balance' => $faker->word,
        'requested_amount' => $faker->word,
        'available_balance' => $faker->word,
        'created_by' => $faker->randomDigitNotNull,
        'updated_by' => $faker->randomDigitNotNull,
        'status' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
