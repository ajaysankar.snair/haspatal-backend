<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Patient_add_fund;
use Faker\Generator as Faker;

$factory->define(Patient_add_fund::class, function (Faker $faker) {

    return [
        'patient_id' => $faker->randomDigitNotNull,
        'requested_amount' => $faker->word,
        'balance_after_approve' => $faker->word,
        'approved_balance' => $faker->word,
        'transaction_type' => $faker->word,
        'transaction_proof' => $faker->word,
        'transaction_date' => $faker->word,
        'status' => $faker->randomDigitNotNull,
        'created_by' => $faker->randomDigitNotNull,
        'updated_by' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
