<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Order_360;
use Faker\Generator as Faker;

$factory->define(Order_360::class, function (Faker $faker) {

    return [
        'patient_id' => $faker->randomDigitNotNull,
        'doctor_id' => $faker->randomDigitNotNull,
        'book_id' => $faker->word,
        'role_id' => $faker->randomDigitNotNull,
        'created_by' => $faker->randomDigitNotNull,
        'updated_by' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
