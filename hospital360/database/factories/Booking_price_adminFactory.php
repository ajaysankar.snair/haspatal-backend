<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Booking_price_admin;
use Faker\Generator as Faker;

$factory->define(Booking_price_admin::class, function (Faker $faker) {

    return [
        'book_price' => $faker->word,
        'description' => $faker->word,
        'created_by' => $faker->randomDigitNotNull,
        'updated_by' => $faker->randomDigitNotNull,
        'status' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
