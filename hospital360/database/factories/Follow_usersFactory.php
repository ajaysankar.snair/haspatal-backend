<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Follow_users;
use Faker\Generator as Faker;

$factory->define(Follow_users::class, function (Faker $faker) {

    return [
        'f_user_id' => $faker->randomDigitNotNull,
        'follow_user_id' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
