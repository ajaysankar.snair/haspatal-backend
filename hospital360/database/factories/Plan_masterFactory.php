<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Plan_master;
use Faker\Generator as Faker;

$factory->define(Plan_master::class, function (Faker $faker) {

    return [
        'pl_name' => $faker->word,
        'pl_price' => $faker->randomDigitNotNull,
        'pl_desc' => $faker->word,
        'status' => $faker->randomDigitNotNull,
        'created_by' => $faker->randomDigitNotNull,
        'updated_by' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
