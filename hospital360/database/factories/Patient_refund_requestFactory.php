<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Patient_refund_request;
use Faker\Generator as Faker;

$factory->define(Patient_refund_request::class, function (Faker $faker) {

    return [
        'requested_amount' => $faker->word,
        'balance_after_approve' => $faker->word,
        'approved_balance' => $faker->word,
        'status' => $faker->randomDigitNotNull,
        'created_by' => $faker->randomDigitNotNull,
        'updated_by' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
