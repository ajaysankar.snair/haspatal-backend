<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Order_query;
use Faker\Generator as Faker;

$factory->define(Order_query::class, function (Faker $faker) {

    return [
        'book_id' => $faker->word,
        'patient_id' => $faker->randomDigitNotNull,
        'doctor_id' => $faker->randomDigitNotNull,
        'query' => $faker->word,
        'created_by' => $faker->randomDigitNotNull,
        'updated_by' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
