<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Key_points_patient;
use Faker\Generator as Faker;

$factory->define(Key_points_patient::class, function (Faker $faker) {

    return [
        'patient_id' => $faker->randomDigitNotNull,
        'doctor_id' => $faker->randomDigitNotNull,
        'key_point' => $faker->text,
        'created_by' => $faker->randomDigitNotNull,
        'updated_by' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
