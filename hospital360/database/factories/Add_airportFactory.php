<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Add_airport;
use Faker\Generator as Faker;

$factory->define(Add_airport::class, function (Faker $faker) {

    return [
        'ar_name' => $faker->word,
        'ar_country_id' => $faker->randomDigitNotNull,
        'ar_state_id' => $faker->randomDigitNotNull,
        'ar_city_id' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
