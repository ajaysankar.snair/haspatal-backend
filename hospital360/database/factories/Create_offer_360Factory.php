<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Create_offer_360;
use Faker\Generator as Faker;

$factory->define(Create_offer_360::class, function (Faker $faker) {

    return [
        'offer_id' => $faker->word,
        'created_by' => $faker->randomDigitNotNull,
        'updated_by' => $faker->randomDigitNotNull,
        'status' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
