<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Consultant;
use Faker\Generator as Faker;

$factory->define(Consultant::class, function (Faker $faker) {

    return [
        'c_patient_id' => $faker->randomDigitNotNull,
        'c_doctor_id' => $faker->randomDigitNotNull,
        'c_book_id' => $faker->word,
        'consultant_note' => $faker->word,
        'created_by' => $faker->randomDigitNotNull,
        'updated_by' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
