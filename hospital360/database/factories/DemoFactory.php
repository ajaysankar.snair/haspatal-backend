<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Demo;
use Faker\Generator as Faker;

$factory->define(Demo::class, function (Faker $faker) {

    return [
        'cd_test' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
