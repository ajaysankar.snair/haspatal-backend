<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Service_master;
use Faker\Generator as Faker;

$factory->define(Service_master::class, function (Faker $faker) {

    return [
        'service_name' => $faker->word,
        'service_price' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
