<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Hospital_register;
use Faker\Generator as Faker;

$factory->define(Hospital_register::class, function (Faker $faker) {

    return [
        'hospital_name' => $faker->word,
        'email' => $faker->word,
        'address' => $faker->word,
        'contact_person_name' => $faker->word,
        'contact_details' => $faker->word,
        'date_time' => $faker->word,
        'logo' => $faker->word,
        'created_by' => $faker->randomDigitNotNull,
        'updated_by' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
