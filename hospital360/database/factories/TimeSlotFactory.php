<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\TimeSlot;
use Faker\Generator as Faker;

$factory->define(TimeSlot::class, function (Faker $faker) {

    return [
        'doctor_id' => $faker->word,
        'booking_time_slot' => $faker->word,
        'mrg_from_time' => $faker->word,
        'mrg_to_time' => $faker->word,
        'aft_from_time' => $faker->word,
        'aft_to_time' => $faker->word,
        'eve_from_time' => $faker->word,
        'eve_to_time' => $faker->word,
        'status' => $faker->word,
        'created_by' => $faker->word,
        'updated_by' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
