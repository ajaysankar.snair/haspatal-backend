<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Examination_notes_patient;
use Faker\Generator as Faker;

$factory->define(Examination_notes_patient::class, function (Faker $faker) {

    return [
        'e_patient_id' => $faker->randomDigitNotNull,
        'e_doctor_id' => $faker->randomDigitNotNull,
        'examination_notes' => $faker->text,
        'created_by' => $faker->randomDigitNotNull,
        'updated_by' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
