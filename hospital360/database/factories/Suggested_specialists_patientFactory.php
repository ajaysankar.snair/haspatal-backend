<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Suggested_specialists_patient;
use Faker\Generator as Faker;

$factory->define(Suggested_specialists_patient::class, function (Faker $faker) {

    return [
        'ss_patient_id' => $faker->randomDigitNotNull,
        'ss_doctor_id' => $faker->randomDigitNotNull,
        'suggested_specialists' => $faker->text,
        'created_by' => $faker->randomDigitNotNull,
        'updated_by' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
