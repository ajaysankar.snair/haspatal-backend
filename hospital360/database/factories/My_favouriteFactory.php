<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\My_favourite;
use Faker\Generator as Faker;

$factory->define(My_favourite::class, function (Faker $faker) {

    return [
        'patient_id' => $faker->randomDigitNotNull,
        'doctor_id' => $faker->randomDigitNotNull,
        'status' => $faker->randomDigitNotNull,
        'created_by' => $faker->randomDigitNotNull,
        'updated_by' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
