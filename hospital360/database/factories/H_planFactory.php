<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\H_plan;
use Faker\Generator as Faker;

$factory->define(H_plan::class, function (Faker $faker) {

    return [
        'plan_name' => $faker->word,
        'price' => $faker->word,
        'limit' => $faker->randomDigitNotNull,
        'description' => $faker->word,
        'status' => $faker->randomDigitNotNull,
        'created_by' => $faker->randomDigitNotNull,
        'updated_by' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
