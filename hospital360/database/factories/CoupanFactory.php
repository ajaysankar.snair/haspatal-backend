<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Coupan;
use Faker\Generator as Faker;

$factory->define(Coupan::class, function (Faker $faker) {

    return [
        'promo_code' => $faker->word,
        'used_promo_code_time' => $faker->word,
        'amount_type' => $faker->word,
        'price' => $faker->randomDigitNotNull,
        'sdate' => $faker->word,
        'edate' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
