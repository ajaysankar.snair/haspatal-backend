<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Buy_plan;
use Faker\Generator as Faker;

$factory->define(Buy_plan::class, function (Faker $faker) {

    return [
        'plan_id' => $faker->randomDigitNotNull,
        'price' => $faker->randomDigitNotNull,
        'limit' => $faker->randomDigitNotNull,
        'payment_id' => $faker->word,
        'order_id' => $faker->word,
        'p_status' => $faker->randomDigitNotNull,
        'created_by' => $faker->randomDigitNotNull,
        'updated_by' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
