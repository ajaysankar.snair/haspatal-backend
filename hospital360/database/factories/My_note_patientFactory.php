<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\My_note_patient;
use Faker\Generator as Faker;

$factory->define(My_note_patient::class, function (Faker $faker) {

    return [
        'my_patient_id' => $faker->randomDigitNotNull,
        'my_doctor_id' => $faker->randomDigitNotNull,
        'my_note' => $faker->text,
        'created_by' => $faker->randomDigitNotNull,
        'updated_by' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
