<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\review_360;
use Faker\Generator as Faker;

$factory->define(review_360::class, function (Faker $faker) {

    return [
        'patient_id' => $faker->randomDigitNotNull,
        'f_id' => $faker->randomDigitNotNull,
        'rating' => $faker->word,
        'review_que' => $faker->word,
        'review_ans' => $faker->word,
        'created_by' => $faker->randomDigitNotNull,
        'updated_by' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
