<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\MemberDetails;
use Faker\Generator as Faker;

$factory->define(MemberDetails::class, function (Faker $faker) {

    return [
        'first_name' => $faker->word,
        'last_name' => $faker->word,
        'email' => $faker->word,
        'mobile' => $faker->word,
        'floor_no' => $faker->word,
        'street_no' => $faker->word,
        'house_no' => $faker->word,
        'id_proof' => $faker->word,
        'image' => $faker->word,
        'no_of_family_member' => $faker->word,
        'notification_type' => $faker->word,
        'member_type' => $faker->word,
        'status' => $faker->word,
        'ctreate_by' => $faker->randomDigitNotNull,
        'updated_by' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
