<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Wallet_360_transaction;
use Faker\Generator as Faker;

$factory->define(Wallet_360_transaction::class, function (Faker $faker) {

    return [
        'user_id' => $faker->randomDigitNotNull,
        'wa_amount' => $faker->randomDigitNotNull,
        'cut_amount' => $faker->randomDigitNotNull,
        'status' => $faker->randomDigitNotNull,
        'created_by' => $faker->randomDigitNotNull,
        'updated_by' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
