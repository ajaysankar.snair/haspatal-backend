<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Customer;
use Faker\Generator as Faker;

$factory->define(Customer::class, function (Faker $faker) {

    return [
        'cust_name' => $faker->word,
        'cust_email' => $faker->word,
        'cust_mobile_no' => $faker->word,
        'cust_country_id' => $faker->randomDigitNotNull,
        'cust_state_id' => $faker->randomDigitNotNull,
        'cust_city_id' => $faker->randomDigitNotNull,
        'cust_address' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
