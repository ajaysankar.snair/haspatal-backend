<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Business_type;
use Faker\Generator as Faker;

$factory->define(Business_type::class, function (Faker $faker) {

    return [
        'b_name' => $faker->word,
        'b_desc' => $faker->word,
        'created_by' => $faker->randomDigitNotNull,
        'updated_by' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
