<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Your_coverage_360;
use Faker\Generator as Faker;

$factory->define(Your_coverage_360::class, function (Faker $faker) {

    return [
        'c_state_id' => $faker->word,
        'c_city_id' => $faker->word,
        'c_pincode' => $faker->word,
        'created_by' => $faker->randomDigitNotNull,
        'updated_by' => $faker->randomDigitNotNull,
        'status' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
