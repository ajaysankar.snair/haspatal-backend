<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Sound_master;
use Faker\Generator as Faker;

$factory->define(Sound_master::class, function (Faker $faker) {

    return [
        'sound_name' => $faker->word,
        'description' => $faker->text,
        'thum' => $faker->word,
        'section' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
