<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Wallet_360;
use Faker\Generator as Faker;

$factory->define(Wallet_360::class, function (Faker $faker) {

    return [
        'user_id' => $faker->randomDigitNotNull,
        'payment_id' => $faker->word,
        'wa_amount' => $faker->randomDigitNotNull,
        'status' => $faker->randomDigitNotNull,
        'created_by' => $faker->randomDigitNotNull,
        'updated_by' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
