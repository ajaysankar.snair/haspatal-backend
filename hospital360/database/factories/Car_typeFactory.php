<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Car_type;
use Faker\Generator as Faker;

$factory->define(Car_type::class, function (Faker $faker) {

    return [
        'car_type_name' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
