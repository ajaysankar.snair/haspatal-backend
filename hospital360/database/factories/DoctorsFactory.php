<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Doctors;
use Faker\Generator as Faker;

$factory->define(Doctors::class, function (Faker $faker) {

    return [
        'first_name' => $faker->word,
        'last_name' => $faker->word,
        'email' => $faker->word,
        'bio' => $faker->word,
        'pincode' => $faker->word,
        'state' => $faker->word,
        'city' => $faker->word,
        'address' => $faker->word,
        'main_mobile' => $faker->word,
        'second_mobile' => $faker->word,
        'clinic_team_leader' => $faker->word,
        'experience' => $faker->word,
        'language' => $faker->word,
        'speciality' => $faker->word,
        'licence_type' => $faker->word,
        'licence_no' => $faker->word,
        'valide_upto' => $faker->word,
        'issued_by' => $faker->word,
        'licence_copy' => $faker->word,
        'profile_pic' => $faker->word,
        'status' => $faker->word,
        'created_by' => $faker->word,
        'updated_by' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
