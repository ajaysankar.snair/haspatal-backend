<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\PropertyDetails;
use Faker\Generator as Faker;

$factory->define(PropertyDetails::class, function (Faker $faker) {

    return [
        'property_type' => $faker->randomDigitNotNull,
        'propery_name' => $faker->word,
        'no_of_floor' => $faker->word,
        'no_of_street' => $faker->word,
        'no_of_house' => $faker->word,
        'serve_no' => $faker->word,
        'area' => $faker->word,
        'address' => $faker->word,
        'country' => $faker->word,
        'state' => $faker->word,
        'city' => $faker->word,
        'status' => $faker->word,
        'created_by' => $faker->randomDigitNotNull,
        'updated_by' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
