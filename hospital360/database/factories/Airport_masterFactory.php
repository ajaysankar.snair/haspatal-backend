<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Airport_master;
use Faker\Generator as Faker;

$factory->define(Airport_master::class, function (Faker $faker) {

    return [
        'airport_id' => $faker->word,
        'airport_name' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
