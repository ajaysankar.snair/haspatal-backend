<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Business_register;
use Faker\Generator as Faker;

$factory->define(Business_register::class, function (Faker $faker) {

    return [
        'b_id' => $faker->randomDigitNotNull,
        'b_name' => $faker->word,
        'gst_no' => $faker->word,
        'working_hr' => $faker->word,
        'address' => $faker->word,
        'country_id' => $faker->randomDigitNotNull,
        'state_id' => $faker->randomDigitNotNull,
        'city_id' => $faker->randomDigitNotNull,
        'pincode' => $faker->word,
        'b_lic' => $faker->word,
        'created_by' => $faker->randomDigitNotNull,
        'updated_by' => $faker->randomDigitNotNull,
        'status' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
