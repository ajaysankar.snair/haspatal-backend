<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Haspatalwalletfund extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('Haspatalwalletfund', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('patient');
            $table->string('doctor');
            $table->string('hasptalcharge')->nullable();
            $table->string('pharmacy')->nullable();
            $table->string('promo')->nullable();
            $table->string('booking_id')->nullable();
            $table->timestamps();
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('couponrates');
        //
    }
}
