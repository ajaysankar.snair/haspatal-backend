<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDoctorDetailsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doctor_details', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email');
            $table->string('bio');
            $table->string('pincode');
            $table->string('state');
            $table->string('city');
            $table->string('address');
            $table->string('main_mobile');
            $table->string('second_mobile');
            $table->string('clinic_team_leader');
            $table->string('experience');
            $table->string('language');
            $table->string('speciality');
            $table->string('licence_type');
            $table->string('licence_no');
            $table->string('valide_upto');
            $table->string('issued_by');
            $table->string('licence_copy');
            $table->string('profile_pic');
            $table->smallInteger('status');
            $table->string('created_by');
            $table->string('updated_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('doctor_details');
    }
}
