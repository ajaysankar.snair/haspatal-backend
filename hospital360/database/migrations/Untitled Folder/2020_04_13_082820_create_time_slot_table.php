<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTimeSlotTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('time_slot', function (Blueprint $table) {
            $table->increments('id');
            $table->increments('doctor_id');
            $table->string('booking_time_slot');
            $table->string('mrg_from_time');
            $table->string('mrg_to_time');
            $table->string('aft_from_time');
            $table->string('aft_to_time');
            $table->string('eve_from_time');
            $table->string('eve_to_time');
            $table->smallInteger('status');
            $table->string('created_by');
            $table->string('updated_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('time_slot');
    }
}
