<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePatientDetailsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patient_details', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email');
            $table->string('mobile');
            $table->string('country');
            $table->string('state');
            $table->string('city');
            $table->string('zipcode');
            $table->string('address');
            $table->string('personal_profile');
            $table->string('family_profile');
            $table->string('social_profile');
            $table->string('work_profile');
            $table->string('contact_phone1');
            $table->string('contact_phone2');
            $table->string('contact_phone3');
            $table->string('email1');
            $table->string('email2');
            $table->string('language');
            $table->string('profile_pic');
            $table->smallInteger('status');
            $table->string('created_by');
            $table->string('updated_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('patient_details');
    }
}
