<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateReservationMasterTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservation_master', function (Blueprint $table) {
            $table->increments('r_id');
            $table->integer('r_service_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('r_service_id')->references('service_id')->on('service_master');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('reservation_master');
    }
}
