<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAnnualMembershipTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('annual_membership', function (Blueprint $table) {
            $table->increments('annual_id');
            $table->integer('srevice')->unsigned();
            $table->string('start_date');
            $table->string('end_date');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('srevice')->references('service_id')->on('service_master');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('annual_membership');
    }
}
