<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePresentComplaintPatientTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('present_complaint_patient', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pc_patient_id');
            $table->integer('pc_doctor_id');
            $table->text('book_id');
            $table->string('present_complaint');
            $table->string('complaint_pic');
            $table->string('complaint_pdf');
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('present_complaint_patient');
    }
}
