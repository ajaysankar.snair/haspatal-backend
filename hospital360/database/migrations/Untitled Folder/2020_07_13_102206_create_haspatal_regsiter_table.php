<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateHaspatalRegsiterTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('haspatal_regsiter', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('country_id');
            $table->integer('state_id');
            $table->integer('city_id');
            $table->integer('pincode_id');
            $table->string('hospital_name');
            $table->text('address');
            $table->string('year_of_establishment');
            $table->string('gst_no');
            $table->string('license_copy');
            $table->string('website');
            $table->string('contact_person');
            $table->string('contact_person_email');
            $table->string('contact_person_phonoe_no');
            $table->string('other_phone_no');
            $table->string('support_watsapp_no');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('haspatal_regsiter');
    }
}
