<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBuyPlanDrTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('buy_plan_dr', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('dr_id');
            $table->integer('plan_id');
            $table->string('payable_amount');
            $table->string('promo_code');
            $table->string('discount');
            $table->string('gst_amount');
            $table->string('final_amount');
            $table->string('gst_number');
            $table->string('payment_id');
            $table->integer('payment_status');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('buy_plan_dr');
    }
}
