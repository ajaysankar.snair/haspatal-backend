<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSuggestedSpecialistsPatientTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('suggested_specialists_patient', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ss_patient_id');
            $table->integer('ss_doctor_id');
            $table->text('suggested_specialists');
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('suggested_specialists_patient');
    }
}
