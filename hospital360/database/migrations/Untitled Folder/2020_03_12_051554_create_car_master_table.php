<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCarMasterTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('car_master', function (Blueprint $table) {
            $table->increments('car_id');
            $table->string('car_name');
            $table->integer('brand')->unsigned();
            $table->integer('car_type')->unsigned();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('brand')->references('brand_id')->on('Brand_master');
            $table->foreign('car_type')->references('car_type_id')->on('Car_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('car_master');
    }
}
