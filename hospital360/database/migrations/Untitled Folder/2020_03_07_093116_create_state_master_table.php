<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStateMasterTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('state_master', function (Blueprint $table) {
            $table->increments('state_id');
            $table->integer('country_id')->unsigned();
            $table->string('state_name');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('country_id')->references('id')->on('country_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('state_master');
    }
}
