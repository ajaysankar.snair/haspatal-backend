<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBuyPlanTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('buy_plan', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('plan_id');
            $table->integer('price');
            $table->integer('limit');
            $table->string('payment_id');
            $table->string('order_id');
            $table->integer('p_status');
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('buy_plan');
    }
}
