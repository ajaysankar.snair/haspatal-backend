<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCityMasterTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('city_master', function (Blueprint $table) {
            $table->increments('city_id');
            $table->string('city_name');
            $table->integer('city_state_id')->unsigned();
            $table->integer('city_country_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('city_state_id')->references('state_id')->on('state_master');
            $table->foreign('city_country_id')->references('id')->on('country_master');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('city_master');
    }
}
