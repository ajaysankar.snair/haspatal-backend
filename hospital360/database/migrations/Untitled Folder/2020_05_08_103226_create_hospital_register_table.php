<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateHospitalRegisterTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hospital_register', function (Blueprint $table) {
            $table->increments('id');
            $table->string('hospital_name');
            $table->string('email');
            $table->string('address');
            $table->string('contact_person_name');
            $table->string('contact_details');
            $table->date('date_time');
            $table->string('logo');
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('hospital_register');
    }
}
