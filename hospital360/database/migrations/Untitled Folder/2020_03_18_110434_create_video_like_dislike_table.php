<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVideoLikeDislikeTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('video_like_dislike', function (Blueprint $table) {
            $table->increments('vl_id');
            $table->integer('vl_video_id');
            $table->integer('vl_user_id');
            $table->integer('vl_status');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('video_like_dislike');
    }
}
