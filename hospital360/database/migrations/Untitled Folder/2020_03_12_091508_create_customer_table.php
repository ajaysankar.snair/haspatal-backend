<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCustomerTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer', function (Blueprint $table) {
            $table->increments('cust_id');
            $table->string('cust_name');
            $table->string('cust_email');
            $table->string('cust_mobile_no');
            $table->integer('cust_country_id')->unsigned();
            $table->integer('cust_state_id')->unsigned();
            $table->integer('cust_city_id')->unsigned();
            $table->string('cust_address');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('cust_country_id')->references('id')->on('country_master');
            $table->foreign('cust_state_id')->references('state_id')->on('state_master');
            $table->foreign('cust_city_id')->references('city_id')->on('city_master');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('customer');
    }
}
