<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateExaminationNotesPatientTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Examination_notes_patient', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('e_patient_id');
            $table->integer('e_doctor_id');
            $table->text('examination_notes');
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Examination_notes_patient');
    }
}
