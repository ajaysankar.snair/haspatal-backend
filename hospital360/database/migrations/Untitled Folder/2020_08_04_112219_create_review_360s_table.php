<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateReview360sTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('review_360s', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('patient_id');
            $table->integer('f_id');
            $table->string('rating');
            $table->string('review_que');
            $table->string('review_ans');
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('review_360s');
    }
}
