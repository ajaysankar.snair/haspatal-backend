<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePatientAddFundTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patient_add_fund', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('patient_id');
            $table->string('requested_amount');
            $table->string('balance_after_approve');
            $table->string('approved_balance');
            $table->string('transaction_type');
            $table->string('transaction_proof');
            $table->string('transaction_date');
            $table->integer('status');
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('patient_add_fund');
    }
}
