<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCoupanTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupan', function (Blueprint $table) {
            $table->increments('id');
            $table->string('promo_code');
            $table->string('used_promo_code_time');
            $table->string('amount_type');
            $table->integer('price');
            $table->string('sdate');
            $table->string('edate');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('coupan');
    }
}
