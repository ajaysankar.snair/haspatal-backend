<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateHaspatal360RegisterTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('haspatal_360_register', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name_prefix');
            $table->string('full_name');
            $table->string('email');
            $table->string('mo_prefix');
            $table->string('mobile_no');
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->integer('status');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('haspatal_360_register');
    }
}
