<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMedicineListTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medicine_list', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('dr_id');
            $table->string('generic_name');
            $table->string('medicine_name');
            $table->string('drug_from');
            $table->string('drug_strength');
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('medicine_list');
    }
}
