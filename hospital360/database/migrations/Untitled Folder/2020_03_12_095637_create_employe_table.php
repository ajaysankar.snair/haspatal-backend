<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEmployeTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employe', function (Blueprint $table) {
            $table->increments('em_id');
            $table->string('em_name');
            $table->string('em_email');
            $table->string('em_mobile_no');
            $table->integer('em_country_id')->unsigned();
            $table->integer('em_state_id')->unsigned();
            $table->integer('em_city_id')->unsigned();
            $table->string('em_address');
            $table->integer('em_car_type')->unsigned();
            $table->string('em_car_pic');
            $table->string('em_car_no');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('em_country_id')->references('id')->on('country_master');
            $table->foreign('em_state_id')->references('state_id')->on('state_master');
            $table->foreign('em_city_id')->references('city_id')->on('city_master');
            $table->foreign('em_car_type')->references('car_type_id')->on('Car_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('employe');
    }
}
