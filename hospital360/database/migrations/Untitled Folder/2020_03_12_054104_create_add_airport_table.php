<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAddAirportTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('add_airport', function (Blueprint $table) {
            $table->increments('ar_id');
            $table->string('ar_name');
            $table->integer('ar_country_id')->unsigned();
            $table->integer('ar_state_id')->unsigned();
            $table->integer('ar_city_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('ar_country_id')->references('id')->on('country_master');
            $table->foreign('ar_state_id')->references('state_id')->on('state_master');
            $table->foreign('ar_city_id')->references('city_id')->on('city_master');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('add_airport');
    }
}
