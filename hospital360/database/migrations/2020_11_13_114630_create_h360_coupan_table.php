<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateH360CoupanTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('H360_coupan', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('b_id');
            $table->string('coupan_code');
            $table->string('price');
            $table->smallInteger('status');
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('H360_coupan');
    }
}
