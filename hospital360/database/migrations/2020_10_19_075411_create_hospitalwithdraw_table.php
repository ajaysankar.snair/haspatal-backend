<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateHospitalWithdrawTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hospitalWithdraw', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('hospital_id');
            $table->string('current_balance');
            $table->string('requested_amount');
            $table->string('available_balance');
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('hospitalWithdraw');
    }
}
