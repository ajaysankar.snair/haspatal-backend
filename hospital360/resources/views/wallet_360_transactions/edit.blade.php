@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
          <li class="breadcrumb-item">
             <a href="{!! route('wallet360Transactions.index') !!}">Wallet 360 Transaction</a>
          </li>
          <li class="breadcrumb-item active">Edit</li>
        </ol>
    <div class="container-fluid">
         <div class="animated fadeIn">
             @include('coreui-templates::common.errors')
             <div class="row">
                 <div class="col-lg-12">
                      <div class="card">
                          <div class="card-header">
                              <i class="fa fa-edit fa-lg"></i>
                              <strong>Edit Wallet 360 Transaction</strong>
                          </div>
                          <div class="card-body">
                              {!! Form::model($wallet360Transaction, ['route' => ['wallet360Transactions.update', $wallet360Transaction->id], 'method' => 'patch']) !!}

                              @include('wallet_360_transactions.fields')

                              {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
         </div>
    </div>
@endsection