<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{{ $wallet360Transaction->id }}</p>
</div>

<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{{ $wallet360Transaction->user_id }}</p>
</div>

<!-- Wa Amount Field -->
<div class="form-group">
    {!! Form::label('wa_amount', 'Wa Amount:') !!}
    <p>{{ $wallet360Transaction->wa_amount }}</p>
</div>

<!-- Cut Amount Field -->
<div class="form-group">
    {!! Form::label('cut_amount', 'Cut Amount:') !!}
    <p>{{ $wallet360Transaction->cut_amount }}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{{ $wallet360Transaction->status }}</p>
</div>

<!-- Created By Field -->
<div class="form-group">
    {!! Form::label('created_by', 'Created By:') !!}
    <p>{{ $wallet360Transaction->created_by }}</p>
</div>

<!-- Updated By Field -->
<div class="form-group">
    {!! Form::label('updated_by', 'Updated By:') !!}
    <p>{{ $wallet360Transaction->updated_by }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $wallet360Transaction->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $wallet360Transaction->updated_at }}</p>
</div>

