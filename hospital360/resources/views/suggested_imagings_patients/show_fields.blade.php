<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{{ $suggestedImagingsPatient->id }}</p>
</div>

<!-- Si Patient Id Field -->
<div class="form-group">
    {!! Form::label('si_patient_id', 'Si Patient Id:') !!}
    <p>{{ $suggestedImagingsPatient->si_patient_id }}</p>
</div>

<!-- Si Doctor Id Field -->
<div class="form-group">
    {!! Form::label('si_doctor_id', 'Si Doctor Id:') !!}
    <p>{{ $suggestedImagingsPatient->si_doctor_id }}</p>
</div>

<!-- Suggested Imagings Field -->
<div class="form-group">
    {!! Form::label('suggested_imagings', 'Suggested Imagings:') !!}
    <p>{{ $suggestedImagingsPatient->suggested_imagings }}</p>
</div>

<!-- Created By Field -->
<div class="form-group">
    {!! Form::label('created_by', 'Created By:') !!}
    <p>{{ $suggestedImagingsPatient->created_by }}</p>
</div>

<!-- Updated By Field -->
<div class="form-group">
    {!! Form::label('updated_by', 'Updated By:') !!}
    <p>{{ $suggestedImagingsPatient->updated_by }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $suggestedImagingsPatient->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $suggestedImagingsPatient->updated_at }}</p>
</div>

