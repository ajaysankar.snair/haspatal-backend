<!-- Si Patient Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('si_patient_id', 'Si Patient Id:') !!}
    {!! Form::text('si_patient_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Si Doctor Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('si_doctor_id', 'Si Doctor Id:') !!}
    {!! Form::text('si_doctor_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Suggested Imagings Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('suggested_imagings', 'Suggested Imagings:') !!}
    {!! Form::textarea('suggested_imagings', null, ['class' => 'form-control']) !!}
</div>

<!-- Created By Field -->
<div class="form-group col-sm-6">
    {!! Form::label('created_by', 'Created By:') !!}
    {!! Form::text('created_by', null, ['class' => 'form-control']) !!}
</div>

<!-- Updated By Field -->
<div class="form-group col-sm-6">
    {!! Form::label('updated_by', 'Updated By:') !!}
    {!! Form::text('updated_by', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('suggestedImagingsPatients.index') }}" class="btn btn-secondary">Cancel</a>
</div>
