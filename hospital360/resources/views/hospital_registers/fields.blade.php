<div class="row">
<!-- Hospital Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('hospital_name', 'Hospital Name:') !!}
    {!! Form::text('hospital_name', null, ['class' => 'form-control','data-parsley-required-message' =>'Hospital Name is required', 'required']) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::text('email', null, ['class' => 'form-control','data-parsley-required-message' =>'Email is required', 'required']) !!}
</div>
</div>
<div class="row">

<!-- Country Field -->
<div class="form-group col-sm-6">
    {!! Form::label('country', 'Country:') !!}
    {!! Form::select('country', [''=>'Select Country'] + $countryList->toArray(), null, ['class' => 'form-control','data-parsley-required-message' =>'Country is required','required']) !!}
</div>

<!-- State Field -->
<div class="form-group col-sm-6">
    {!! Form::label('state', 'State:') !!}
    {!! Form::select('state', [''=>'Select State'] + $stateList->toArray(), null, ['class' => 'form-control','data-parsley-required-message' =>'State is required','required']) !!}
</div>
  
</div>

<div class="row">
  <!-- City Field -->
<div class="form-group col-sm-6">
    {!! Form::label('city', 'City:') !!}
    {!! Form::select('city', [''=>'Select City'] + $cityList->toArray(), null, ['class' => 'form-control','data-parsley-required-message' =>'City is required','required']) !!}
</div>
<!-- Address Field -->
<div class="form-group col-sm-6">
    {!! Form::label('address', 'Address:') !!}
    {!! Form::textarea('address', null, ['class' => 'form-control','rows'=>'2','data-parsley-required-message' =>'Address is required', 'required']) !!}
</div>

</div>
<div class="row">
  <!-- Contact Person Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('contact_person_name', 'Contact Person Name:') !!}
    {!! Form::text('contact_person_name', null, ['class' => 'form-control','data-parsley-required-message' =>'Contact Person Name is required', 'required']) !!}
</div>
<!-- Contact Details Field -->
<div class="form-group col-sm-6">
    {!! Form::label('contact_details', 'Contact Details:') !!}
    {!! Form::text('contact_details', null, ['class' => 'form-control','data-parsley-required-message' =>'Contact Details is required', 'required']) !!}
</div>
</div>
<div class="row">
  <!-- Date Time Field -->
<div class="form-group col-sm-6">
    {!! Form::label('date_time', 'Date Time:') !!}
    {!! Form::text('date_time', null, ['class' => 'form-control','id'=>'date_time','data-parsley-required-message' =>'Date Time is required', 'required']) !!}
</div>
<!-- Logo Field -->
<!-- <div class="form-group col-sm-6">
    {!! Form::label('logo', 'Logo:') !!}
    {!! Form::file('logo', null, ['class' => 'form-control']) !!}
</div> -->
@if(isset($hospitalRegister))
<!-- Sn Image Field -->
<div class="form-group col-sm-3">
    {!! Form::label('logo', 'Logo:') !!}
    {!! Form::file('logo') !!}
</div>
<div class="form-group col-sm-3">
    <?php if ($hospitalRegister->logo) {
        $img = url('public/media/hospital_logo/').'/'.$hospitalRegister->logo; 
    }else{
        $img = url('public/default-image.jpg'); 
    }?>
    <p><img src="{{$img}}" style="width: 30%"></p>
</div>
@else
<div class="form-group col-sm-6">
    {!! Form::label('logo', 'Logo:') !!}
    {!! Form::file('logo') !!}
</div>
@endif


<div class="clearfix"></div>
 
</div>

@if(isset($hospitalRegister))
@else
<div class="row">
  <?php  $plan= DB::table('roles')->where('id', 15)->pluck('display_name','id'); ?>
<div class="form-group col-sm-6">
    {!! Form::label('role_id', 'Select Role:') !!}
    {!! Form::select('role_id', [''=>'Select Role'] + $plan->toArray(), null, ['class' => 'form-control','data-parsley-required-message' =>'Role is required','required']) !!}
    
</div>
<!-- Main Mobile Field -->
<div class="form-group col-sm-6">
    {!! Form::label('password', 'Password:') !!}
    {!! Form::password('password', ['class' => 'form-control','id' => 'password','data-parsley-required-message' =>'Password is required','required']) !!}
</div>
</div>
<div class="row">
  <!-- Second Mobile Field -->
<div class="form-group col-sm-6">
    {!! Form::label('confirm_password', 'Confirm Password:') !!}
    {!! Form::password('confirm_password', ['class' => 'form-control','data-parsley-required-message' =>'Confirm Password is required','data-parsley-equalto'=>'#password','data-parsley-equalto-message'=>'Confirm password not match with password','required']) !!}
</div>
</div>
@endif
<div class="row">
<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('hospitalRegisters.index') }}" class="btn btn-secondary">Cancel</a>
</div>
</div>


@push('scripts')
   <script type="text/javascript">
           $('#date_time').datetimepicker({
               format: 'YYYY-MM-DD HH:mm:ss',
               useCurrent: true,
               icons: {
                   up: "icon-arrow-up-circle icons font-2xl",
                   down: "icon-arrow-down-circle icons font-2xl"
               },
               sideBySide: true
           })
       </script>
@endpush
