<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{{ $hospitalRegister->id }}</p>
</div>

<!-- Hospital Name Field -->
<div class="form-group">
    {!! Form::label('hospital_name', 'Hospital Name:') !!}
    <p>{{ $hospitalRegister->hospital_name }}</p>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', 'Email:') !!}
    <p>{{ $hospitalRegister->email }}</p>
</div>

<!-- Address Field -->
<div class="form-group">
    {!! Form::label('address', 'Address:') !!}
    <p>{{ $hospitalRegister->address }}</p>
</div>

<!-- Contact Person Name Field -->
<div class="form-group">
    {!! Form::label('contact_person_name', 'Contact Person Name:') !!}
    <p>{{ $hospitalRegister->contact_person_name }}</p>
</div>

<!-- Contact Details Field -->
<div class="form-group">
    {!! Form::label('contact_details', 'Contact Details:') !!}
    <p>{{ $hospitalRegister->contact_details }}</p>
</div>

<!-- Date Time Field -->
<div class="form-group">
    {!! Form::label('date_time', 'Date Time:') !!}
    <p>{{ $hospitalRegister->date_time }}</p>
</div>

<!-- Logo Field -->
<div class="form-group">
    {!! Form::label('logo', 'Logo:') !!}
    <p>{{ $hospitalRegister->logo }}</p>
</div>

<!-- Created By Field -->
<div class="form-group">
    {!! Form::label('created_by', 'Created By:') !!}
    <p>{{ $hospitalRegister->created_by }}</p>
</div>

<!-- Updated By Field -->
<div class="form-group">
    {!! Form::label('updated_by', 'Updated By:') !!}
    <p>{{ $hospitalRegister->updated_by }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $hospitalRegister->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $hospitalRegister->updated_at }}</p>
</div>

