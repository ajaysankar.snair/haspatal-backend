@extends('layouts.app')
@section('title', 'Create Hospital')
@section('content')
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
         <a href="{!! route('hospitalRegisters.index') !!}">Hospital Register</a>
      </li>
      <li class="breadcrumb-item active">Create</li>
    </ol>
     <div class="container-fluid">
          <div class="animated fadeIn">
                @include('coreui-templates::common.errors')
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <i class="fa fa-plus-square-o fa-lg"></i>
                                <strong>Create Hospital Register</strong>
                            </div>
                            <div class="card-body">
                                {!! Form::open(['route' => 'hospitalRegisters.store','id' => 'myForm']) !!}

                                   @include('hospital_registers.fields')

                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
           </div>
    </div>
@endsection
