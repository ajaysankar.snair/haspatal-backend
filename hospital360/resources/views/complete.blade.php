<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Haspatal Admin Template</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900,900i&display=swap" rel="stylesheet">
    <script src="https://kit.fontawesome.com/fa5e54101c.js" crossorigin="anonymous"></script>
    <!-- JavaScript Bundle with Popper -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

    <!-- Theme Style -->
    <link rel="stylesheet" href="css/style.css">
    <style>.bg_red{
    background: #db4130;
}
.logo_container{
    height: 100vh;
    padding: 20px;
    display: flex;
    width: 100%;
    justify-content: center;
    align-items: center;
}
.logo_container img{
    display: block;
    width: 250px;
}
.reg_box{
    padding: 30px;
    display: flex;
    justify-content: center;
    align-items: center;
    height: 100vh;
}
.form_container{
    width: 60%;
}
.head_txt{
    text-align: center;
    margin-bottom: 40px;
}
.login_link{
    margin: 50px 0 10px 0;
    text-align: center;
}
.login_link a{
    text-decoration: none;
}
.form-group{
    margin-bottom: 15px;
}
.form-group label{
    margin-bottom: 5px;
}
@media(max-width: 768px){
    .logo_container{
        height: auto;
        padding: 50px 20px;
    }
    .logo_container img{
        width: 180px;
    }
    .reg_box{
        height: auto;
        padding: 50px 30px;
    }
    .form_container{
        width: 80%;
    }
}
header{
    background: #db4130;
    display: flex;
}
.logo_head_container{
    padding: 20px 30px;
    background: #f5f5f5;
}
.logo_head_container img{
    display: block;
    width: 180px;
}
.top_bx_ct{
    display: flex;
    padding: 10px 30px;
    justify-content: space-between;
    align-items: center;
    background: #f5f5f5;
}
.top_bx_ct h5{
    margin: 0;
    font-size: 16px;
    font-weight: 600;
    text-transform: uppercase;
}
.active{
    background: #db4130 !important;
    color: #fff !important;
}
.tabl_container{
    padding: 0 30px;
}
.tbl_contr{
    padding: 30px;
    box-shadow: 0px 0px 20px rgba(92,111,139,0.12);
    border-radius: 10px;
}
th{
    font-size: 13px;
    color: #5a5a5a;
    /* background: #f5f5f5 !important; */
    border: none;
    text-transform: uppercase;
}
td{
    font-size: 13px;
    padding: .4rem .5rem !important;
    background: #f6f6f6 !important;
    border: none;
    border-bottom: 5px solid #fff !important;
}
tr{
    border: none;
}
table{
    width: 100%;
}
.acpt{
    background: rgb(15, 153, 73);
    cursor: pointer;
}
.rjct{
    background: rgb(165, 19, 19);
    cursor: pointer;
}
.clr{
    background: rgb(111 111 236);
    cursor: pointer;
}
.awt{
    background: rgb(216 142 8);
    cursor: pointer;
}
.aprc_cl{
    display: block;
    padding: 0px 7px;
    border-radius: 5px;
    background: rgb(15, 153, 73);
    color: #fff;
    width: fit-content;
    font-size: 12px;
}
.rjct_cl{
    display: block;
    padding: 0px 7px;
    border-radius: 5px;
    background: rgb(165, 19, 19);
    color: #fff;
    width: fit-content;
    font-size: 12px;
}
.pend_cl{
    display: block;
    padding: 0px 7px;
    border-radius: 5px;
    background: rgb(21 99 245);
    color: #fff;
    width: fit-content;
    font-size: 12px;
}
.btn_spcl_btn{
    display: inline-block;
    font-size: 16px;
    color: #db4130;
    padding: 7px;
    width: 200px;
    border: 2px solid #db4130;
    box-shadow: 0 1px 15px 1px rgb(68 68 68 / 15%);
    border-radius: 30px;
    margin-top: 20px;
}
.btn_cont{
    text-align: center;
}
.dropdown-menu.show{
    left: -82px !important;
}
.btn-slct-clr{
    color: #db4130;
    background-color: #ffffff;
    border-color: #db4130;
}
.doc{
    text-decoration: none;
    color: #db4130;
    cursor: pointer;
}
.view{
    text-decoration: none;
    cursor: pointer;
}
.df_blk{
    display: block;
}
.flx-bx{
    display: flex;
    align-items: center;
    margin-bottom: 15px;
}
.fil_lbl{
    width: 230px;
    font-size: 14px;
    color: #656565;
    font-weight: 500;
}
.p_cls{
    width: 100%;
    padding: 10px 15px;
    background: #f5f5f5;
    border-radius: 5px;
    margin:0;
    font-size: 14px;
}
.btn_group_flx{
    margin-bottom: 20px;
    display: flex;
    justify-content: flex-end;
}
.btn_grp_fl_s{
    display: block;
    margin-right: 5px;
    padding: 5px 15px;
    text-decoration: none;
    color: #fff;
    font-size: 12px;
    border-radius: 5px;
}
.tab_btn{
    display: flex;
    align-items: center;
}
.link_btn{
    text-decoration: none;
    padding: 5px 20px;
    margin-right: 10px;
    color: #000;
    background: #fff;
    font-size: 14px;
    border-radius: 3px;
    box-shadow: 0px 0px 20px rgb(92 111 139 / 12%);
}
.cls_pad{
    padding: 20px 30px;
}
.btn-slct-clr{
    padding: 3px 12px;
    background: #0d6efd;
    border: none;
    color: #fff;
}
.dropdown-item{
    font-size: 14px;
}
.doc_link{
    display: block;
}</style>
  </head>
  <body>
    <section id="wrx_total">
      <header>
        <div class="logo_head_container">
        <img src="{{url('public/images/Logo.png')}}">
        </div>
      </header>

      <div class="row">
          <div class="col-md-12">
            <div class="cls_pad">
              <div class="top_bx_ct">
                <h5>Orders</h5>
                <div class="tab_btn">
                <a class="link_btn active" href="/allreg">Home</a>
                  <a class="link_btn active" href="/orders">Orders</a>
                  <a class="link_btn" href="/todays">Today</a>
                  <a class="link_btn" href="/pending">Pending</a>
                  <a class="link_btn" href="/complete">Completed</a>
                  
                </div>
                </div>
            </div>
            </div>
          </div>

              <div class="col-md-12">
                  <div class="tabl_container">
                      <div class="tbl_contr">
                        <table class="table table-hover table-bordered">
                            <thead>
                              <tr>
                             
                                <th scope="col">Date</th>
                                <th scope="col">Order ID</th>
                                <th scope="col">Name</th>
                                <th scope="col">Pharmacy</th>
                                <th scope="col">Mobile</th>
                                <th scope="col">status</th>
                              
                              </tr>
                            </thead>
                            <tbody>
                            @foreach ($data as $data)
                       <tr>
                       <td>{{ $data->created_at->format('d/m/Y') }}</td>
                                <td>{{ $data->Order_id }}</td>
                                <td>{{ $data->first_name }}</td>
                                <td>{{ $data->b_name }}</td>
                                <td>{{ $data->mobile }}</td>
                                <td>{{ $data->order_status }}</td>
                                </tr>
                 
                                @endforeach
                            </tbody>
                        </table>
                      </div>
                  </div>
              </div>
          </div>
      </section>
  </body>
</html>