@extends('layouts.app')
@section('title', 'Edit Service')
@section('content')
    <ol class="breadcrumb">
          <li class="breadcrumb-item">
             <a href="{!! route('serviceDetails.index') !!}">Service Details</a>
          </li>
          <li class="breadcrumb-item active">Edit</li>
        </ol>
    <div class="container-fluid">
         <div class="animated fadeIn">
             @include('coreui-templates::common.errors')
             <div class="row">
                 <div class="col-lg-12">
                      <div class="card">
                          <div class="card-header">
                              <i class="fa fa-edit fa-lg"></i>
                              <strong>Edit Service Details</strong>
                          </div>
                          <div class="card-body">
                              {!! Form::model($serviceDetails, ['route' => ['serviceDetails.update', $serviceDetails->id], 'method' => 'patch','id'=>'myForm']) !!}

                              @include('service_details.fields')

                              {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
         </div>
    </div>
@endsection