<div class="row">
<!-- Service Field -->
<div class="form-group col-sm-6">
    {!! Form::label('service', 'Service:') !!}
    {!! Form::text('service', null, ['class' => 'form-control','data-parsley-pattern="^[a-zA-Z]+$"','data-parsley-required-message' =>'Service is required','required']) !!}
</div>

<!-- Status Field -->
<!-- <div class="form-group col-sm-6">
    {!! Form::label('status', 'Status:') !!}
    {!! Form::text('status', null, ['class' => 'form-control']) !!}
</div> -->
</div>
<!-- Created By Field -->
<div class="form-group col-sm-6">
    {!! Form::hidden('created_by', Auth::user()->id, ['class' => 'form-control']) !!}
</div>

<!-- Updated By Field -->
<div class="form-group col-sm-6">
    {!! Form::hidden('updated_by', Auth::user()->id, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('serviceDetails.index') }}" class="btn btn-secondary">Cancel</a>
</div>
