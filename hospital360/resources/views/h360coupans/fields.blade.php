<!-- B Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('b_id', 'B Id:') !!}
    {!! Form::select('b_id', ['Select Service' => 'Select Service'], null, ['class' => 'form-control']) !!}
</div>

<!-- Coupan Code Field -->
<div class="form-group col-sm-6">
    {!! Form::label('coupan_code', 'Coupan Code:') !!}
    {!! Form::text('coupan_code', null, ['class' => 'form-control']) !!}
</div>

<!-- Price Field -->
<div class="form-group col-sm-6">
    {!! Form::label('price', 'Price:') !!}
    {!! Form::text('price', null, ['class' => 'form-control']) !!}
</div>

<!-- Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('status', 'Status:') !!}
    {!! Form::select('status', ['Select Status' => 'Select Status'], null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('h360coupans.index') }}" class="btn btn-secondary">Cancel</a>
</div>
