<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{{ $h360coupan->id }}</p>
</div>

<!-- B Id Field -->
<div class="form-group">
    {!! Form::label('b_id', 'B Id:') !!}
    <p>{{ $h360coupan->b_id }}</p>
</div>

<!-- Coupan Code Field -->
<div class="form-group">
    {!! Form::label('coupan_code', 'Coupan Code:') !!}
    <p>{{ $h360coupan->coupan_code }}</p>
</div>

<!-- Price Field -->
<div class="form-group">
    {!! Form::label('price', 'Price:') !!}
    <p>{{ $h360coupan->price }}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{{ $h360coupan->status }}</p>
</div>

<!-- Created By Field -->
<div class="form-group">
    {!! Form::label('created_by', 'Created By:') !!}
    <p>{{ $h360coupan->created_by }}</p>
</div>

<!-- Updated By Field -->
<div class="form-group">
    {!! Form::label('updated_by', 'Updated By:') !!}
    <p>{{ $h360coupan->updated_by }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $h360coupan->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $h360coupan->updated_at }}</p>
</div>

