@extends('layouts.app')
@section('title', 'Edit Time Slot')
@section('content')
    <ol class="breadcrumb">
          <li class="breadcrumb-item">
             <a href="{!! route('timeSlots.index') !!}">Time Slot</a>
          </li>
          <li class="breadcrumb-item active">Edit</li>
        </ol>
    <div class="container-fluid">
         <div class="animated fadeIn">
             @include('coreui-templates::common.errors')
             <div class="row">
                 <div class="col-lg-12">
                      <div class="card">
                          <div class="card-header">
                              <i class="fa fa-edit fa-lg"></i>
                              <strong>Edit Time Slot</strong>
                          </div>
                          <div class="card-body">
                              {!! Form::model($timeSlot, ['route' => ['timeSlots.update', $timeSlot->id], 'method' => 'patch','id' => 'myForm']) !!}

                              @include('time_slots.fields')

                              {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
         </div>
    </div>
@endsection