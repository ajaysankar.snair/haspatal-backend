<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{{ $timeSlot->id }}</p>
</div>

<!-- Doctor Id Field -->
<div class="form-group">
    {!! Form::label('doctor_id', 'Doctor Id:') !!}
    <p>{{ $timeSlot->first_name }} {{ $timeSlot->last_name }}</p>
</div>

<!-- Booking Time Slot Field -->
<div class="form-group">
    {!! Form::label('booking_time_slot', 'Booking Time Slot:') !!}
    <p>{{ $timeSlot->booking_time_slot }}</p>
</div>

<!-- Mrg From Time Field -->
<div class="form-group">
    {!! Form::label('mrg_from_time', 'Mrg From Time:') !!}
    <p>{{ $timeSlot->mrg_from_time }}</p>
</div>

<!-- Mrg To Time Field -->
<div class="form-group">
    {!! Form::label('mrg_to_time', 'Mrg To Time:') !!}
    <p>{{ $timeSlot->mrg_to_time }}</p>
</div>

<!-- Aft From Time Field -->
<div class="form-group">
    {!! Form::label('aft_from_time', 'Aft From Time:') !!}
    <p>{{ $timeSlot->aft_from_time }}</p>
</div>

<!-- Aft To Time Field -->
<div class="form-group">
    {!! Form::label('aft_to_time', 'Aft To Time:') !!}
    <p>{{ $timeSlot->aft_to_time }}</p>
</div>

<!-- Eve From Time Field -->
<div class="form-group">
    {!! Form::label('eve_from_time', 'Eve From Time:') !!}
    <p>{{ $timeSlot->eve_from_time }}</p>
</div>

<!-- Eve To Time Field -->
<div class="form-group">
    {!! Form::label('eve_to_time', 'Eve To Time:') !!}
    <p>{{ $timeSlot->eve_to_time }}</p>
</div>

<!-- Status Field -->
<!-- <div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{{ $timeSlot->status }}</p>
</div> -->

<!-- Created By Field -->
<!-- <div class="form-group">
    {!! Form::label('created_by', 'Created By:') !!}
    <p>{{ $timeSlot->created_by }}</p>
</div> -->

<!-- Updated By Field -->
<!-- <div class="form-group">
    {!! Form::label('updated_by', 'Updated By:') !!}
    <p>{{ $timeSlot->updated_by }}</p>
</div> -->

<!-- Created At Field -->
<!-- <div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $timeSlot->created_at }}</p>
</div> -->

<!-- Updated At Field -->
<!-- <div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $timeSlot->updated_at }}</p>
</div> -->

