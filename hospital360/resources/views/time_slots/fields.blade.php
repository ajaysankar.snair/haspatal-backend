<div class="row">
<!-- Doctor Id Field -->

<!-- Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('date', 'Booking Time In Minute:') !!}
    {!! Form::text('booking_time_slot', null, ['class' => 'form-control']) !!}
    
</div>
</div>
<div class="row">


    <!-- Video Link Field -->
    <div class="form-group col-sm-4">
        {!! Form::label('from_time', 'Morning Time:') !!}
        <!-- {!! Form::text('from_time[]', null, ['class' => 'form-control']) !!} -->
    </div>
    <div class="form-group col-sm-4">
        {!! Form::label('from_time', 'From Time:') !!}
        {!! Form::text('mrg_from_time', null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group col-sm-4">
        {!! Form::label('to_time', 'To Time:') !!}
        {!! Form::text('mrg_to_time', null, ['class' => 'form-control']) !!}
    </div>
<div class="clearfix"></div>

</div>
<div class="row">


    <!-- Video Link Field -->
    <div class="form-group col-sm-4">
        {!! Form::label('from_time', 'Afternoon Time:') !!}
        <!-- {!! Form::text('from_time[]', null, ['class' => 'form-control']) !!} -->
    </div>
    <div class="form-group col-sm-4">
        {!! Form::label('from_time', 'From Time:') !!}
        {!! Form::text('aft_from_time', null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group col-sm-4">
        {!! Form::label('to_time', 'To Time:') !!}
        {!! Form::text('aft_to_time', null, ['class' => 'form-control']) !!}
    </div>
<div class="clearfix"></div>

</div>
<div class="row">


    <!-- Video Link Field -->
    <div class="form-group col-sm-4">
        {!! Form::label('from_time', 'Evening Time:') !!}
        <!-- {!! Form::text('from_time[]', null, ['class' => 'form-control']) !!} -->
    </div>
    <div class="form-group col-sm-4">
        {!! Form::label('from_time', 'From Time:') !!}
        {!! Form::text('eve_from_time', null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group col-sm-4">
        {!! Form::label('to_time', 'To Time:') !!}
        {!! Form::text('eve_to_time', null, ['class' => 'form-control']) !!}
    </div>
<div class="clearfix"></div>

</div>
    <div class="clearfix"></div>
<!-- Created By Field -->
<!-- <div class="form-group col-sm-6">
    {!! Form::label('created_by', 'Created By:') !!}
    {!! Form::text('created_by', null, ['class' => 'form-control']) !!}
</div> -->

<!-- Updated By Field -->
<!-- <div class="form-group col-sm-6">
    {!! Form::label('updated_by', 'Updated By:') !!}
    {!! Form::text('updated_by', null, ['class' => 'form-control']) !!}
</div> -->

<!-- Submit Field -->
<div class="row">
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('doctorAvailabilities.index') }}" class="btn btn-secondary">Cancel</a>
</div>
</div>

@push('scripts')

@endpush