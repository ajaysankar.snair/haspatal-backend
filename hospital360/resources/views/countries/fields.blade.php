<!-- Name Field -->
<div class="row">
<div class="form-group col-sm-6">
    {!! Form::label('country', 'Name:') !!}
    {!! Form::text('country', null, ['class' => 'form-control','data-parsley-pattern="^[a-zA-Z]+$"','data-parsley-required-message' =>'Country Name is required','required']) !!}
</div>

<!-- Status Field -->
<!-- <div class="form-group col-sm-6">
    {!! Form::label('status', 'Status:') !!}
    {!! Form::text('status', null, ['class' => 'form-control']) !!}
</div> -->
</div>
<!-- Created By Field -->
<!-- <div class="form-group col-sm-6">
    {!! Form::label('created_by', 'Created By:') !!}
    {!! Form::text('created_by', null, ['class' => 'form-control']) !!}
</div> -->

<!-- Updated By Field -->
<!-- <div class="form-group col-sm-6">
    {!! Form::label('updated_by', 'Updated By:') !!}
    {!! Form::text('updated_by', null, ['class' => 'form-control']) !!}
</div> -->

<!-- Submit Field -->
<div class="row">
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('countries.index') }}" class="btn btn-secondary">Cancel</a>
</div>
</div>