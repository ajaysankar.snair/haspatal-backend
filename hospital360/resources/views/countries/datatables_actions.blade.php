{!! Form::open(['route' => ['countries.destroy', $id], 'method' => 'delete']) !!}
<div class='btn-group'>
    @permission(['admin','show_country'])
    <a href="{{ route('countries.show', $id) }}" class='btn btn-ghost-success'>
       <i class="fa fa-eye"></i>
    </a>
    @endpermission
    @permission(['admin','edit_country'])
    <a href="{{ route('countries.edit', $id) }}" class='btn btn-ghost-info'>
       <i class="fa fa-edit"></i>
    </a>
    @endpermission
    @permission(['admin','delete_country'])

    {!! Form::button('<i class="fa fa-trash"></i>', [
        'type' => 'submit',
        'class' => 'btn btn-ghost-danger',
        'onclick' => "return confirm('Are you sure?')"
    ]) !!}
    @endpermission
</div>
{!! Form::close() !!}
