<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>@yield('title') | {{config('app.name')}}</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 4.1.1 -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@coreui/coreui@2.1.16/dist/css/coreui.min.css">
     <link rel="stylesheet" href="{{ url('public/asset/parsley/css/parsley.css') }}">
     <link rel="stylesheet" href="{{ url('public/css/newheader.css') }}">
     <link rel="stylesheet" href="{{ url('public/css/coustom.css') }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@icon/coreui-icons-free@1.0.1-alpha.1/coreui-icons-free.css">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.4.1/css/simple-line-icons.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.3.0/css/flag-icon.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="{{ url('public/asset/ckeditor/ckeditor.js') }}"></script>
    <style type="text/css">
      .logbtn{
        background-color: white;
    color: black;
    border-color: white;
      }
     .dropdown-item:focus, .dropdown-item:hover {
    color: #181b1e;
    text-decoration: none;
    background-color: white;
    }
    .header-fixed .app-header {
    position: static;
    z-index: 1020;
    width: 100%;
    height: 50px;
}
.btn35 {
    color: black;
    background-color: white;
   	border: 1px solid #b2b2b2;box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
    font-size: 20px;
    font-weight: 500;
    height: 50px;
    padding: 7%;
}
    </style>
    <script>
    CKEDITOR.replace( 'summary-ckeditor' );
</script>
  <!--    <script type="text/javascript" src="http://js.nicedit.com/nicEdit-latest.js"></script>
      <script type="text/javascript">
      bkLib.onDomLoaded(function() { nicEditors.allTextAreas() });
  </script> -->
    @yield('scripts-header')
    @yield('css')
</head>
<!-- <body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show" style="background-image: url({{url('public/image/background.png')}})"> -->
  <?php use Carbon\Carbon;?>
<body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show" style="background-image: url(https://crimecoverage.in/haspatal/public/image/background2.png);background-repeat: no-repeat;background-size: cover;background-color: white;">
<header class="app-header navbar" style="background: bottom;">
    <button class="navbar-toggler sidebar-toggler d-lg-none mr-auto" type="button" data-toggle="sidebar-show">
        <span class="navbar-toggler-icon"></span>
    </button>
<a class="navbar-brand" href="javascript:;"> 
        <img class="logomobile" src="{{ URL::asset('public/image/1 logo.jpg') }}" style="width: 74%;margin-left: -21%;" alt="logo">
        <?php  $img = url('public/image/haspatallogo.png');  ?> 
        <!-- <img class="navbar-brand-full" src="{{$img}}" style=""  alt="Logo">
        <img class="navbar-brand-minimized" src="{{$img}}" style="" alt="Logo"> -->
        <!-- <h4>Haspatal</h4> -->
    </a> 
   <!--  <button class="navbar-toggler sidebar-toggler d-md-down-none" type="button" data-toggle="sidebar-lg-show">
        <span class="navbar-toggler-icon"></span>
    </button> -->
		<?php 
        	$doctor_id = Auth::user()->userDetailsId;
        	$userss = DB::table('doctor_details')
        	->where('id',$doctor_id)
        	->first();

          $hospital_id = Auth::user()->userDetailsId;
          
          $hospital = \App\Models\HaspatalRegsiter::where('id',$hospital_id)->first();
          

         ?>
    <ul class="nav navbar-nav ml-auto">

     @if(Auth::user()->role_id == 15) 

        <li class="nav-item" style="margin-left: -25%;margin-right: 12%;">
            <a class="nav-link" >
              <button class="btn35">{{$hospital->contact_person_phonoe_no}}</button>
            </a>
        </li>

        <li class="nav-item" style="margin-left: 1%;margin-right: 5%;">
            <a class="nav-link" >
              

              @if ($hospital->logo == null)
              <img  src="{{URL::asset('public/noimg.png')}}" alt="Course" style="width: 190px;max-width: 60px;height: 60px;max-height: 60px;">
               @else

              <?php  $userimage = URL::asset('public/media/hospital_logo/'.$hospital->logo); ?>
                   

                    <div class="">
                        <img  src="<?= $userimage ?>" alt="Course"  style="width: 190px;max-width: 60px;height: 60px;max-height: 60px;">
                    </div>
               @endif

            </a>
        </li>

       @else
              <li class="nav-item" style="margin-left: -15%;margin-right: 5%;">
          
  <a href="#" class="btn btn-danger active" role="button"> <?php $allcount = DB::table('booking_request')->where('doctor_name', Auth::user()->userDetailsId)->where('status',1)->whereDate('booking_request.updated_at', Carbon::today())->count(); echo $allcount; ?></a>  
      
      </li>

        <li class="nav-item" style="margin-left: -2%;margin-right: 5%;">
            <a class="nav-link" >
              <button class="btn35">{{$userss->main_mobile}}</button>
            </a>
        </li>

        <li class="nav-item" style="margin-left: 1%;margin-right: 5%;">
            <a class="nav-link" >
              
              @if ($userss->profile_pic == null)
                                  <img  src="{{URL::asset('public/noimg.png')}}" alt="Course" style="width: 190px;max-width: 60px;height: 60px;max-height: 60px;">
                                   @else

                                  <?php  $userimage = URL::asset('public/media/profile_pic/'.$userss->profile_pic); ?>
                                      

                                        <div class="">
                                            <img  src="<?= $userimage ?>" alt="Course" style="width: 190px;max-width: 60px;height: 60px;max-height: 60px;">
                                        </div>
                                   @endif

            </a>
        </li>
      @endif


        
        <li class="nav-item dropdown">
            <a class="nav-link" style="margin-right: 10px"  href="#" role="button"
               aria-haspopup="true" aria-expanded="false">
                <!-- {{ Auth::user()->first_name }} {{ Auth::user()->last_name }} -->
                <a href="{{ url('/logout') }}" class="logbtn btn  btn-flat"
                   onclick="event.preventDefault(); document.getElementById('logout-form').submit();" style="font-size: 20px;">
                    <i class="fa fa-lock"></i> Logout
                </a>
                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </a>
        </li>
    </ul>

</header>
<div class="app-body" style="margin-top: 2%;">
    
  <main class="main" style="margin-left: 0px;">
   
@yield('content')
    </main>
</div>
</body>
<!-- jQuery 3.1.1 -->
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.20.1/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/@coreui/coreui@2.1.16/dist/js/coreui.min.js"></script>
  <script src="{{ url('public/asset/parsley/js/parsley.js') }} "></script> 
     <script type="text/javascript">
         $(function () {
            
          $('#myForm').parsley().on('field:validated', function() {
            var ok = $('.parsley-error').length === 0;
            $('.bs-callout-info').toggleClass('hidden', !ok);
            $('.bs-callout-warning').toggleClass('hidden', ok);
          })
        });

     </script>
     <script>
function myFunction() {
  var x = document.getElementById("myTopnav");
  if (x.className === "topnav") {
    x.className += " responsive";
  } else {
    x.className = "topnav";
  }
}


</script>

@stack('scripts')

</html>
