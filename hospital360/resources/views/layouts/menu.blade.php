@if(Auth::user()->role_id == 1)
<li class="nav-item nav-dropdown ">
  <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon fa fa-user"></i><span> Users Masters</span></a>
<ul class="nav-dropdown-items">
   
    <li class="nav-item ">
      <a class="nav-link {{ Request::is('user*') ? 'active' : '' }}" href="{!! action('AclController@userIndex') !!}">
      <i class="nav-icon fa  fa-circle-o"></i>
        <span>Users</span>
      </a>
    </li>

    
    <li class="nav-item ">
      <a class="nav-link {{ Request::is('user/role*') ? 'active' : '' }}" href="{!! action('AclController@roleIndex') !!}">
      <i class="nav-icon fa fa-circle-o"></i>
        <span>Role</span>
      </a>
    </li>

</ul>
</li>
@endif

@permission(['admin'])

<li class="nav-item nav-dropdown ">
  <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon fa fa-laptop"></i><span>Masters</span></a>
<ul class="nav-dropdown-items">
   
    @permission(['admin','view_country'])    
    <li class="nav-item {{ Request::is('countries*') ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('countries.index') }}">
            <i class="nav-icon fa fa-circle-o"></i>
            <span>Countries</span>
        </a>
    </li>
    @endpermission

    
    @permission(['admin','view_state'])
    <li class="nav-item {{ Request::is('states*') ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('states.index') }}">
            <i class="nav-icon fa fa-circle-o"></i>
            <span>States</span>
        </a>
    </li>
    @endpermission

    @permission(['admin','view_city'])
    <li class="nav-item {{ Request::is('cities*') ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('cities.index') }}">
            <i class="nav-icon fa fa-circle-o"></i>
            <span>Cities</span>
        </a>
    </li>
    @endpermission

    @permission(['admin','view_language'])
    <li class="nav-item {{ Request::is('languages*') ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('languages.index') }}">
            <i class="nav-icon fa fa-circle-o"></i>
            <span>Languages</span>
        </a>
    </li>
    @endpermission

    @permission(['admin','view_specialities'])
    <li class="nav-item {{ Request::is('specialities*') ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('specialities.index') }}">
            <i class="nav-icon fa fa-circle-o"></i>
            <span>Specialities</span>
        </a>
    </li>
    @endpermission

    @permission(['admin'])

    <li class="nav-item {{ Request::is('businessTypes*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('businessTypes.index') }}">
        <i class="nav-icon fa fa-circle-o"></i>
        <span>Business Types</span>
    </a>
    </li>
    @endpermission

    @permission(['admin'])
    <li class="nav-item {{ Request::is('planMasters*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('planMasters.index') }}">
        <i class="nav-icon fa fa-circle-o"></i>
        <span>Plan Master</span>
    </a>
    </li>
    @endpermission
    @permission(['admin'])

    <li class="nav-item {{ Request::is('hPlans*') ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('hPlans.index') }}">
            <i class="nav-icon fa fa-circle-o"></i>
            <span>360 Plans</span>
        </a>
    </li>
    @endpermission
    @permission(['admin'])

    <li class="nav-item {{ Request::is('offers*') ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('offers.index') }}">
            <i class="nav-icon fa fa-circle-o"></i>
            <span>Offers</span>
        </a>
    </li>
    @endpermission
    @permission(['admin'])

    <li class="nav-item {{ Request::is('bookingPriceAdmins*') ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('bookingPriceAdmins.edit',['id'=>7]) }}">
            <i class="nav-icon fa fa-circle-o"></i>
            <span>Booking Price Admins</span>
        </a>
    </li>
    @endpermission


</ul>
</li>

 @endpermission





@permission(['admin','view_doctor'])
<li class="nav-item {{ Request::is('doctors*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('doctors.index') }}">
        <i class="nav-icon fa fa-user-md"></i>
        <span>Doctors</span>
    </a>
</li>
@endpermission

@permission(['admin','view_patient'])
<li class="nav-item {{ Request::is('patients*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('patients.index') }}">
        <i class="nav-icon fa fa-wheelchair"></i>
        <span>Patients</span>
    </a>
</li>
@endpermission

@permission(['admin','view_service'])
<li class="nav-item {{ Request::is('serviceDetails*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('serviceDetails.index') }}">
        <i class="nav-icon fa fa-stethoscope"></i>
        <span>Service Details</span>
    </a>
</li>
@endpermission
@permission(['admin','view_service_price'])
<li class="nav-item {{ Request::is('setServicePrices*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('setServicePrices.index') }}">
        <i class="nav-icon fa fa-money"></i>
        <span>Set Service Prices</span>
    </a>
</li>
@endpermission
@permission(['admin','view_bankinfo'])
<li class="nav-item {{ Request::is('bankInformations*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('bankInformations.index') }}">
        <i class="nav-icon fa fa-university"></i>
        <span>Bank Informations</span>
    </a>
</li>
@endpermission

@permission(['admin','view_licenceTypes'])
<li class="nav-item {{ Request::is('licenceTypes*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('licenceTypes.index') }}">
        <i class="nav-icon fa fa-id-card-o"></i>
        <span>Licence Types</span>
    </a>
</li>
@endpermission

@permission(['admin','view_booking_requests'])
<!-- <li class="nav-item {{ Request::is('bookingRequests*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('bookingRequests.index') }}">
        <i class="nav-icon fa fa-history"></i>
        <span>Booking Requests</span>
    </a>
</li> -->
@endpermission


<!-- <li class="nav-item {{ Request::is('myFavourites*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('myFavourites.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>My Favourites</span>
    </a>
</li> -->
@permission(['admin','view_doctor_availabilities'])
<!-- <li class="nav-item {{ Request::is('doctorAvailabilities*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('doctorAvailabilities.index') }}">
        <i class="nav-icon fa fa-calendar-times-o"></i>
        <span>Doctor Non Availabilities</span>
    </a>
</li> -->
@endpermission
@permission(['admin','view_prescription_details'])
<!-- <li class="nav-item {{ Request::is('prescriptionDetails*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('prescriptionDetails.index') }}">
        <i class="nav-icon fa fa-sticky-note-o"></i>
        <span>Prescription Details</span>
    </a>
</li> -->
@endpermission
@permission(['admin'])
<!-- <li class="nav-item {{ Request::is('hospitalRegisters*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('hospitalRegisters.index') }}">
        <i class="nav-icon fa fa-hospital-o"></i>
        <span>Hospital Registers</span>
    </a>
</li> -->
@endpermission
@permission(['admin'])
<li class="nav-item {{ Request::is('timeSlots*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('timeSlots.index') }}">
        <i class="nav-icon fa fa-calendar"></i>
        <span>Time Slots</span>
    </a>
</li>
@endpermission
@permission('admin')
<li class="nav-item {{ Request::is('coupans*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('coupans.index') }}">
        <i class="nav-icon fa fa-gift"></i>
        <span>Coupans</span>
    </a>
</li>
@endpermission
@permission('admin')
@endpermission
{{-- <li class="nav-item {{ Request::is('patientAddFunds*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('patientAddFunds.index') }}">
        <i class="nav-icon fa fa-refresh"></i>
        <span>Patient Add Funds</span>
    </a>
</li> --}}
 <!-- <li class="nav-item {{ Request::is('patientRefundRequests*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('patientRefundRequests.index') }}">
        <i class="nav-icon fa fa-hourglass"></i>
        <span>Patient Refund Requests</span>
    </a>
</li>  -->

<!-- <li class="nav-item {{ Request::is('keyPointsPatients*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('keyPointsPatients.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Key Points Patients</span>
    </a>
</li>
<li class="nav-item {{ Request::is('presentMedicationsPatients*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('presentMedicationsPatients.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Present Medications Patients</span>
    </a>
</li>
<li class="nav-item {{ Request::is('examinationNotesPatients*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('examinationNotesPatients.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Examination Notes Patients</span>
    </a>
</li>
<li class="nav-item {{ Request::is('availableLabPatients*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('availableLabPatients.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Available Lab Patients</span>
    </a>
</li>
<li class="nav-item {{ Request::is('perceivedPatients*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('perceivedPatients.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Perceived Patients</span>
    </a>
</li>
<li class="nav-item {{ Request::is('suggestedInvestigationsPatients*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('suggestedInvestigationsPatients.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Suggested Investigations Patients</span>
    </a>
</li>
<li class="nav-item {{ Request::is('suggestedImagingsPatients*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('suggestedImagingsPatients.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Suggested Imagings Patients</span>
    </a>
</li>
<li class="nav-item {{ Request::is('suggestedSpecialistsPatients*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('suggestedSpecialistsPatients.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Suggested Specialists Patients</span>
    </a>
</li>
<li class="nav-item {{ Request::is('suggestedTherapiesPatients*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('suggestedTherapiesPatients.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Suggested Therapies Patients</span>
    </a>
</li>-->
<!-- <li class="nav-item {{ Request::is('presentComplaintPatients*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('presentComplaintPatients.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Present Complaint Patients</span>
    </a>
</li> 

<li class="nav-item {{ Request::is('doctorWithdraws*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('doctorWithdraws.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Doctor Withdraws</span>
    </a>
</li>
<li class="nav-item {{ Request::is('supports*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('supports.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Supports</span>
    </a>
</li> -->

<!-- <li class="nav-item {{ Request::is('reviews*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('reviews.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Reviews</span>
    </a>
</li>-->


<!-- <li class="nav-item {{ Request::is('businessRegisters*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('businessRegisters.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Business Registers</span>
    </a>
</li> -->
@permission(['admin'])
<li class="nav-item {{ Request::is('haspatal360Registers*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('haspatal360Registers.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>H360 Registers</span>
    </a>
</li>
@endpermission
<!-- <li class="nav-item {{ Request::is('yourCoverage360s*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('yourCoverage360s.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Your Coverage 360S</span>
    </a>
</li>
<li class="nav-item {{ Request::is('createOffer360s*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('createOffer360s.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Create Offer 360S</span>
    </a>
</li> -->
<!--<li class="nav-item {{ Request::is('wallet360s*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('wallet360s.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Wallet 360S</span>
    </a>
</li>-->
<!-- <li class="nav-item {{ Request::is('wallet360Transactions*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('wallet360Transactions.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Wallet 360 Transactions</span>
    </a>
</li>
<li class="nav-item {{ Request::is('buyPlans*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('buyPlans.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Buy Plans</span>
    </a>
</li> -->
<!-- <li class="nav-item {{ Request::is('order360s*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('order360s.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Order 360S</span>
    </a>
</li>
 -->
 <!-- <li class="nav-item {{ Request::is('orderQueries*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('orderQueries.index') }}">
        <i class="nav-icon fa fa-first-order"></i>
        <span>Order Queries</span>
    </a>
</li> -->
@permission(['admin'])
<li class="nav-item {{ Request::is('haspatalRegsiters*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('haspatalRegsiters.index') }}">
        <i class="nav-icon fa fa-hospital-o"></i>
        <span>Haspatal Regsiters</span>
    </a>
</li>
@endpermission
<!-- <li class="nav-item {{ Request::is('setConsultPrices*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('setConsultPrices.index') }}">
        <i class="nav-icon fa fa-money"></i>
        <span>Set  Consult  Prices</span>
    </a>
</li> -->
@permission(['admin'])
<li class="nav-item {{ Request::is('review360s*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('review360s.index') }}">
        <i class="nav-icon fa fa-star"></i>
        <span>Review 360S</span>
    </a>
</li>
@endpermission
@permission(['admin'])
<li class="nav-item {{ Request::is('taxes*') ? 'active' : '' }}">

    <a class="nav-link" href="{{ route('taxes.index') }}">

        <i class="nav-icon icon-cursor"></i>

        <span>Taxes</span>

    </a>

</li>
@endpermission
<!-- <li class="nav-item {{ Request::is('buyPlanDrs*') ? 'active' : '' }}">

    <a class="nav-link" href="{{ route('buyPlanDrs.index') }}">

        <i class="nav-icon icon-cursor"></i>

        <span>Buy Plan Drs</span>

    </a>

</li> -->
<!-- <li class="nav-item {{ Request::is('medicines*') ? 'active' : '' }}">

    <a class="nav-link" href="{{ route('medicines.index') }}">

        <i class="nav-icon icon-cursor"></i>

        <span>Medicines</span>

    </a>

</li>
 -->
<!--<li class="nav-item {{ Request::is('labTests*') ? 'active' : '' }}">

    <a class="nav-link" href="{{ route('labTests.index') }}">

        <i class="nav-icon icon-cursor"></i>

        <span>Lab Tests</span>

    </a>

</li>
<li class="nav-item {{ Request::is('imagingLists*') ? 'active' : '' }}">

    <a class="nav-link" href="{{ route('imagingLists.index') }}">

        <i class="nav-icon icon-cursor"></i>

        <span>Imaging Lists</span>

    </a>

</li>
<li class="nav-item {{ Request::is('therapiesLists*') ? 'active' : '' }}">

    <a class="nav-link" href="{{ route('therapiesLists.index') }}">

        <i class="nav-icon icon-cursor"></i>

        <span>Therapies Lists</span>

    </a>

</li>
<li class="nav-item {{ Request::is('counselingsLists*') ? 'active' : '' }}">

    <a class="nav-link" href="{{ route('counselingsLists.index') }}">

        <i class="nav-icon icon-cursor"></i>

        <span>Counselings Lists</span>

    </a>

</li>
<li class="nav-item {{ Request::is('homeCareLists*') ? 'active' : '' }}">

    <a class="nav-link" href="{{ route('homeCareLists.index') }}">

        <i class="nav-icon icon-cursor"></i>

        <span>Home Care Lists</span>

    </a>

</li>
<li class="nav-item {{ Request::is('referralSpecialities*') ? 'active' : '' }}">

    <a class="nav-link" href="{{ route('referralSpecialities.index') }}">

        <i class="nav-icon icon-cursor"></i>

        <span>Referral Specialities</span>

    </a>

</li>



<li class="nav-item {{ Request::is('followupPeriods*') ? 'active' : '' }}">

    <a class="nav-link" href="{{ route('followupPeriods.index') }}">

        <i class="nav-icon icon-cursor"></i>

        <span>Followup Periods</span>

    </a>

</li>-->

@permission(['admin'])
<!-- <li class="nav-item {{ Request::is('hospitalAddFunds*') ? 'active' : '' }}">

    <a class="nav-link" href="{{ route('hospitalAddFunds.index') }}">

        <i class="nav-icon icon-cursor"></i>

        <span>Hospital Add Funds</span>

    </a>

</li> -->
@endpermission
@permission(['admin'])
<li class="nav-item {{ Request::is('hospitalWithdraws*') ? 'active' : '' }}">

    <a class="nav-link" href="{{ route('hospitalWithdraws.index') }}">

        <i class="nav-icon icon-cursor"></i>

        <span>Hospital Withdraws</span>

    </a>

</li>
@endpermission

@role('operation manager')
<li class="nav-item {{ Request::is('tasks*') ? 'active' : '' }}">

    <a class="nav-link" href="{{ route('tasks.index') }}">

        <i class="nav-icon icon-cursor"></i>

        <span>Tasks</span>

    </a>

</li>
@endrole
@role('operator')
<li class="nav-item {{ Request::is('tasks*') ? 'active' : '' }}">

    <a class="nav-link" href="{{ route('operator_task') }}">

        <i class="nav-icon icon-cursor"></i>

        <span>Tasks</span>

    </a>

</li>
@endrole
