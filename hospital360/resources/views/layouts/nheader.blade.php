<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>@yield('title') | {{config('app.name')}}</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 4.1.1 -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@coreui/coreui@2.1.16/dist/css/coreui.min.css">
     <link rel="stylesheet" href="{{ url('public/asset/parsley/css/parsley.css') }}">
     <link rel="stylesheet" href="{{ url('public/css/newheader.css') }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@icon/coreui-icons-free@1.0.1-alpha.1/coreui-icons-free.css">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.4.1/css/simple-line-icons.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.3.0/css/flag-icon.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="{{ url('public/asset/ckeditor/ckeditor.js') }}"></script>
    <style type="text/css">
      .btn-danger:hover {
    color: #fff;
    background-color: #f64846;
    border-color: #f63c3a;
}
    </style>
    <script>
    CKEDITOR.replace( 'summary-ckeditor' );
</script>
  <!--    <script type="text/javascript" src="http://js.nicedit.com/nicEdit-latest.js"></script>
      <script type="text/javascript">
      bkLib.onDomLoaded(function() { nicEditors.allTextAreas() });
  </script> -->
    @yield('scripts-header')
    @yield('css')
</head>
<body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">
<header class="app-header navbar">
    <button class="navbar-toggler sidebar-toggler d-lg-none mr-auto" type="button" data-toggle="sidebar-show">
        <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand" href="{{ url('/dashbord') }}">
        <img  src="{{ URL::asset('public/image/haspatallogo.png') }}" style="width: 100%;margin-left: 10%;" alt="logo">
        <!-- <?php  $img = url('public/image/haspatallogo.png');  ?>  -->
        <!-- <img class="navbar-brand-full" src="{{$img}}" style=""  alt="Logo">
        <img class="navbar-brand-minimized" src="{{$img}}" style="" alt="Logo"> -->
        <!-- <h4>Haspatal</h4> -->
    </a>
   <!--  <button class="navbar-toggler sidebar-toggler d-md-down-none" type="button" data-toggle="sidebar-lg-show">
        <span class="navbar-toggler-icon"></span>
    </button> -->

    <ul class="nav navbar-nav ml-auto">
      <li class="nav-item d-md-down-none">
        
         <h3 style="color: #909090"> Pending Consults&nbsp;&nbsp; <a href="#" class="btn btn-danger active pull-right" role="button"> <?php use Carbon\Carbon;  $allcount = DB::table('booking_request')->where('doctor_name', Auth::user()->userDetailsId)->where('status',1)->whereDate('booking_request.updated_at', Carbon::today())->count(); echo $allcount; ?></a></h3> 
        
      </li>
        <li class="nav-item d-md-down-none">
            <a class="nav-link" href="#">
                <i class="icon-bell"></i>
                <!-- <span class="badge badge-pill badge-danger">5</span> -->
            </a>
        </li>
        <li class="nav-item dropdown">
            <a class="nav-link" style="margin-right: 10px" data-toggle="dropdown" href="#" role="button"
               aria-haspopup="true" aria-expanded="false">
                {{ Auth::user()->first_name }} {{ Auth::user()->last_name }}
            </a>
            <div class="dropdown-menu dropdown-menu-right">
                <!-- <div class="dropdown-header text-center">
                    <strong>Account</strong>
                </div>
                <a class="dropdown-item" href="#">
                    <i class="fa fa-envelope-o"></i> Messages
                    <span class="badge badge-success">42</span>
                </a>
                <div class="dropdown-header text-center">
                    <strong>Settings</strong>
                </div> -->
               <!--  <a class="dropdown-item" href="#">
                    <i class="fa fa-user"></i> Profile</a>
                <a class="dropdown-item" href="#">
                    <i class="fa fa-wrench"></i> Settings</a>
                <div class="dropdown-divider"></div> -->
                <!-- <a class="dropdown-item" href="#">
                    <i class="fa fa-shield"></i> Lock Account</a> -->
                <a href="{{ url('/logout') }}" class="dropdown-item btn btn-danger btn-flat"
                   onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                    <i class="fa fa-lock"></i>Logout
                </a>
                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </div>
        </li>
    </ul>

</header>

<div class="app-body">
    
  <main class="main" style="margin-left: 0px;">
    <div class="topnav" id="myTopnav">
      <a href="{{ url('/dashbord') }}" class="{{ Request::is('dashbord*') ? 'active' : '' }}">Home</a>
      <a href="{{ url('/profile') }}"  class="{{ Request::is('profile*') ? 'active' : '' }}">Profile</a>
      <a href="{{ url('/wallet') }}"  class="{{ Request::is('wallet*') ? 'active' : '' }}">Wallet</a>
      <a href="{{ url('/records') }}"  class="{{ Request::is('records*') ? 'active' : '' }}">Records</a>
      <a href="{{ url('/reviews_menu') }}"  class="{{ Request::is('reviews_menu*') ? 'active' : '' }}">Review</a>
      <a href="{{ url('/consults') }}"  class="{{ Request::is('consults*') ? 'active' : '' }}">Consults</a>
      <a href="{{ url('/support') }}"  class="{{ Request::is('Support*') ? 'active' : '' }}">Support</a>
       <a href="{{ url('/settings_menu') }}"  class="{{ Request::is('settings_menu*') ? 'active' : '' }}">Settings</a>
      <a href="javascript:void(0);" class="icon" onclick="myFunction()">
        <i class="fa fa-bars"></i>
      </a>
  </div>
@yield('content')
    </main>
</div>
<footer class="app-footer" style="margin-left: 0px;background-color: red;">
    <div style="color: white;">
        <a href="#" style="color: #ffffff;">Haspatal</a>
        <span>&copy; <?php echo date('Y'); ?></span>
    </div>
    <div class="ml-auto" style="color: white;">
        <span>Powered by</span>
        <a href="http://aelius.in/" style="color: #ffffff;">Aelius Technologies</a>
    </div>
</footer>
</body>
<!-- jQuery 3.1.1 -->
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.20.1/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/@coreui/coreui@2.1.16/dist/js/coreui.min.js"></script>
  <script src="{{ url('public/asset/parsley/js/parsley.js') }} "></script> 
     <script type="text/javascript">
         $(function () {
            
          $('#myForm').parsley().on('field:validated', function() {
            var ok = $('.parsley-error').length === 0;
            $('.bs-callout-info').toggleClass('hidden', !ok);
            $('.bs-callout-warning').toggleClass('hidden', ok);
          })
        });

     </script>
     <script>
function myFunction() {
  var x = document.getElementById("myTopnav");
  if (x.className === "topnav") {
    x.className += " responsive";
  } else {
    x.className = "topnav";
  }
}


</script>

@stack('scripts')

</html>
