<!-- Dr Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('dr_id', 'Dr Id:') !!}
    {!! Form::text('dr_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Plan Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('plan_id', 'Plan Id:') !!}
    {!! Form::text('plan_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Payable Amount Field -->
<div class="form-group col-sm-6">
    {!! Form::label('payable_amount', 'Payable Amount:') !!}
    {!! Form::text('payable_amount', null, ['class' => 'form-control']) !!}
</div>

<!-- Promo Code Field -->
<div class="form-group col-sm-6">
    {!! Form::label('promo_code', 'Promo Code:') !!}
    {!! Form::text('promo_code', null, ['class' => 'form-control']) !!}
</div>

<!-- Discount Field -->
<div class="form-group col-sm-6">
    {!! Form::label('discount', 'Discount:') !!}
    {!! Form::text('discount', null, ['class' => 'form-control']) !!}
</div>

<!-- Gst Amount Field -->
<div class="form-group col-sm-6">
    {!! Form::label('gst_amount', 'Gst Amount:') !!}
    {!! Form::text('gst_amount', null, ['class' => 'form-control']) !!}
</div>

<!-- Final Amount Field -->
<div class="form-group col-sm-6">
    {!! Form::label('final_amount', 'Final Amount:') !!}
    {!! Form::text('final_amount', null, ['class' => 'form-control']) !!}
</div>

<!-- Gst Number Field -->
<div class="form-group col-sm-6">
    {!! Form::label('gst_number', 'Gst Number:') !!}
    {!! Form::text('gst_number', null, ['class' => 'form-control']) !!}
</div>

<!-- Payment Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('payment_id', 'Payment Id:') !!}
    {!! Form::text('payment_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Payment Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('payment_status', 'Payment Status:') !!}
    {!! Form::text('payment_status', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('buyPlanDrs.index') }}" class="btn btn-secondary">Cancel</a>
</div>
