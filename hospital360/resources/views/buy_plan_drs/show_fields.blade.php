<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{{ $buyPlanDr->id }}</p>
</div>

<!-- Dr Id Field -->
<div class="form-group">
    {!! Form::label('dr_id', 'Dr Id:') !!}
    <p>{{ $buyPlanDr->dr_id }}</p>
</div>

<!-- Plan Id Field -->
<div class="form-group">
    {!! Form::label('plan_id', 'Plan Id:') !!}
    <p>{{ $buyPlanDr->plan_id }}</p>
</div>

<!-- Payable Amount Field -->
<div class="form-group">
    {!! Form::label('payable_amount', 'Payable Amount:') !!}
    <p>{{ $buyPlanDr->payable_amount }}</p>
</div>

<!-- Promo Code Field -->
<div class="form-group">
    {!! Form::label('promo_code', 'Promo Code:') !!}
    <p>{{ $buyPlanDr->promo_code }}</p>
</div>

<!-- Discount Field -->
<div class="form-group">
    {!! Form::label('discount', 'Discount:') !!}
    <p>{{ $buyPlanDr->discount }}</p>
</div>

<!-- Gst Amount Field -->
<div class="form-group">
    {!! Form::label('gst_amount', 'Gst Amount:') !!}
    <p>{{ $buyPlanDr->gst_amount }}</p>
</div>

<!-- Final Amount Field -->
<div class="form-group">
    {!! Form::label('final_amount', 'Final Amount:') !!}
    <p>{{ $buyPlanDr->final_amount }}</p>
</div>

<!-- Gst Number Field -->
<div class="form-group">
    {!! Form::label('gst_number', 'Gst Number:') !!}
    <p>{{ $buyPlanDr->gst_number }}</p>
</div>

<!-- Payment Id Field -->
<div class="form-group">
    {!! Form::label('payment_id', 'Payment Id:') !!}
    <p>{{ $buyPlanDr->payment_id }}</p>
</div>

<!-- Payment Status Field -->
<div class="form-group">
    {!! Form::label('payment_status', 'Payment Status:') !!}
    <p>{{ $buyPlanDr->payment_status }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $buyPlanDr->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $buyPlanDr->updated_at }}</p>
</div>

