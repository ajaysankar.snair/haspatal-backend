<!-- Specialities Field -->
<div class="row">
<div class="form-group col-sm-6">
    {!! Form::label('specialities', 'Specialities:') !!}
    {!! Form::text('specialities', null, ['class' => 'form-control']) !!}
</div>

<!-- Doctor Field -->
<div class="form-group col-sm-6">
    {!! Form::label('doctor', 'Doctor:') !!}
    {!! Form::text('doctor', null, ['class' => 'form-control']) !!}
</div>

<!-- Consult Price Field -->
<div class="form-group col-sm-6">
    {!! Form::label('consult_price', 'Consult Price:') !!}
    {!! Form::text('consult_price', null, ['class' => 'form-control']) !!}
</div>

<!-- Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('date', 'Date:') !!}
    {!! Form::text('date', null, ['class' => 'form-control','id'=>'date']) !!}
</div>

@push('scripts')
   <script type="text/javascript">
           $('#date').datetimepicker({
               format: 'YYYY-MM-DD HH:mm:ss',
               useCurrent: true,
               icons: {
                   up: "icon-arrow-up-circle icons font-2xl",
                   down: "icon-arrow-down-circle icons font-2xl"
               },
               sideBySide: true
           })
       </script>
@endpush


<!-- Created By Field -->

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('setConsultPrices.index') }}" class="btn btn-secondary">Cancel</a>
</div>
</div>