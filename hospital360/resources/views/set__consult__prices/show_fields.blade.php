<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{{ $setConsultPrices->id }}</p>
</div>

<!-- Specialities Field -->
<div class="form-group">
    {!! Form::label('specialities', 'Specialities:') !!}
    <p>{{ $setConsultPrices->specialities }}</p>
</div>

<!-- Doctor Field -->
<div class="form-group">
    {!! Form::label('doctor', 'Doctor:') !!}
    <p>{{ $setConsultPrices->doctor }}</p>
</div>

<!-- Consult Price Field -->
<div class="form-group">
    {!! Form::label('consult_price', 'Consult Price:') !!}
    <p>{{ $setConsultPrices->consult_price }}</p>
</div>

<!-- Date Field -->
<div class="form-group">
    {!! Form::label('date', 'Date:') !!}
    <p>{{ $setConsultPrices->date }}</p>
</div>

<!-- Created By Field -->
<div class="form-group">
    {!! Form::label('created_by', 'Created By:') !!}
    <p>{{ $setConsultPrices->created_by }}</p>
</div>

<!-- Updated By Field -->
<div class="form-group">
    {!! Form::label('updated_by', 'Updated By:') !!}
    <p>{{ $setConsultPrices->updated_by }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $setConsultPrices->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $setConsultPrices->updated_at }}</p>
</div>

