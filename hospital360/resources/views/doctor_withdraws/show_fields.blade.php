<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{{ $doctorWithdraw->id }}</p>
</div>

<!-- Doctor Id Field -->
<div class="form-group">
    {!! Form::label('doctor_id', 'Doctor Id:') !!}
    <p>{{ $doctorWithdraw->doctor_id }}</p>
</div>

<!-- Current Balance Field -->
<div class="form-group">
    {!! Form::label('current_balance', 'Current Balance:') !!}
    <p>{{ $doctorWithdraw->current_balance }}</p>
</div>

<!-- Requested Amount Field -->
<div class="form-group">
    {!! Form::label('requested_amount', 'Requested Amount:') !!}
    <p>{{ $doctorWithdraw->requested_amount }}</p>
</div>

<!-- Available Balance Field -->
<div class="form-group">
    {!! Form::label('available_balance', 'Available Balance:') !!}
    <p>{{ $doctorWithdraw->available_balance }}</p>
</div>

<!-- Created By Field -->
<div class="form-group">
    {!! Form::label('created_by', 'Created By:') !!}
    <p>{{ $doctorWithdraw->created_by }}</p>
</div>

<!-- Updated By Field -->
<div class="form-group">
    {!! Form::label('updated_by', 'Updated By:') !!}
    <p>{{ $doctorWithdraw->updated_by }}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{{ $doctorWithdraw->status }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $doctorWithdraw->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $doctorWithdraw->updated_at }}</p>
</div>

