@extends('layouts.app')
@section('title', 'Dashboard')
@section('content')

<div class="container-fluid animated fadeIn mt-3">
  <div id="callScreen">

      <?php 
        if (Auth::id() == "1") 
        {
      ?>
          <div class="row">

            <div class="col-sm-6 col-lg-4" >
              <div class="card text-white bg-primary">
                <div class="card-body pb-0">
                  <div class="text-value">{{$total_patients}}</div>
                  <div>Total Patients</div>
                </div>
                <div class="chart-wrapper mt-3 mx-3"></div>
              </div>
            </div>

            <div class="col-sm-6 col-lg-4" >
              <div class="card text-white bg-warning">
                <div class="card-body pb-0">
                  <div class="text-value">{{$total_doctors}}</div>
                  <div>Total Doctors</div>
                </div>
                <div class="chart-wrapper mt-3 mx-3"></div>
              </div>
            </div>

            <div class="col-sm-6 col-lg-4" >
              <div class="card text-white bg-info">
                <div class="card-body pb-0">
                  <div class="text-value">{{$total_haspatal_regsiter}}</div>
                  <div>Haspatal Regsiters</div>
                </div>
                <div class="chart-wrapper mt-3 mx-3"></div>
              </div>
            </div>
        
          </div>
      <?php
        }
      ?>
  </div>

</div>
<!-- <script>
var chat_appid = '1715924732c3be3';
var chat_auth = 'c23e0835bfaa11cdad5c56a36ce1c8791775d33e';
</script>
<?php if(!empty(Auth::id())) { ?>
 <script>
    var chat_id = "<?php echo Auth::id(); ?>";
    var chat_name = "<?php echo isset(Auth::user()->name) ? Auth::user()->name : Auth::user()->email; ?>"; 
    var chat_link = ""; 
    var chat_avatar = ""; 
    var chat_role = ""; 
    var chat_friends = ''; // eg: 14,16,20 in case if friends feature is enabled.
    </script>
<?php } ?>
<script>
var chat_height = '600px';
var chat_width = '990px';

document.write('<div id="cometchat_embed_synergy_container" style="width:'+chat_width+';height:'+chat_height+';max-width:100%;border:1px solid #CCCCCC;border-radius:5px;overflow:hidden;"></div>');

var chat_js = document.createElement('script'); 
chat_js.type = 'text/javascript'; 
//chat_js.src = 'https://fast.cometondemand.net/'+chat_appid+'x_xchatx_xcorex_xembedcode.js';

chat_js.onload = function() {
    var chat_iframe = {};chat_iframe.module="synergy";chat_iframe.style="min-height:"+chat_height+";min-width:"+chat_width+";";chat_iframe.width=chat_width.replace('px','');chat_iframe.height=chat_height.replace('px','');chat_iframe.src='https://'+chat_appid+'.cometondemand.net/cometchat_embedded.php'; if(typeof(addEmbedIframe)=="function"){addEmbedIframe(chat_iframe);}
}

var chat_script = document.getElementsByTagName('script')[0]; chat_script.parentNode.insertBefore(chat_js, chat_script);
</script> -->
<!-- <script type="text/javascript" src="https://unpkg.com/@cometchat-pro/chat@2.0.7/CometChat.js"></script>

<script type="text/javascript">
  window.onload = (function () {
  var appId = "1583130789928e2";
  let cometChatSettings = new CometChat.AppSettingsBuilder().subscribePresenceForAllUsers().setRegion('us').build();
  CometChat.init(appId, cometChatSettings)
    .then(
      () => {
        console.log("Initialization completed successfully");
        //You can now call login function.
          var UID = "superhero1";
          var apiKey = "f0ead65846606835fc6b114e7e1ac2d9e496ab41";

          CometChat.login(UID, apiKey).then(
            User => {
              console.log("Login successfully:", { User });
              // User loged in successfully.
              var receiverID = "superhero1";
              var callType = CometChat.CALL_TYPE.VIDEO;
              var receiverType = CometChat.RECEIVER_TYPE.USER;

              var call = new CometChat.Call(receiverID, callType, receiverType);

              CometChat.initiateCall(call).then(
                outGoingCall => {
                  console.log("Call initiated successfully:", outGoingCall);
                  // perform action on success. Like show your calling screen.
                  var sessionID = outGoingCall.sessionId;

                  CometChat.startCall(
                    sessionID,
                    document.getElementById("callScreen"),
                    new CometChat.OngoingCallListener({
                      onUserJoined: user => {
                        /* Notification received here if another user joins the call. */
                        console.log("User joined call:", user);
                        /* this method can be use to display message or perform any actions if someone joining the call */
                      },
                      onUserLeft: user => {
                        /* Notification received here if another user left the call. */
                        console.log("User left call:", user);
                        /* this method can be use to display message or perform any actions if someone leaving the call */
                      },
                      onCallEnded: call => {
                        /* Notification received here if current ongoing call is ended. */
                        console.log("Call ended:", call);
                        /* hiding/closing the call screen can be done here. */
                      }
                    })
                  );
                },
                error => {
                  console.log("Call initialization failed with exception:", error);
                }
              );
            },
            error => {
              console.log("Login failed with exception:", { error });
              // User login failed, check error and take appropriate action.
            }
          );
       },
       error => {
        console.log("Initialization failed with error:", error);
        //Check the reason for error and take appropriate action.
      }
    );

    
    



 })
</script> -->

<!-- <script type="text/javascript">
  var receiverID = "superhero2";
var callType = CometChat.CALL_TYPE.VIDEO;
var receiverType = CometChat.RECEIVER_TYPE.USER;

var call = new CometChat.Call(receiverID, callType, receiverType);

CometChat.initiateCall(call).then(
  outGoingCall => {
    console.log("Call initiated successfully:", outGoingCall);
    // perform action on success. Like show your calling screen.
  },
  error => {
    console.log("Call initialization failed with exception:", error);
  }
);

</script> -->
@endsection
