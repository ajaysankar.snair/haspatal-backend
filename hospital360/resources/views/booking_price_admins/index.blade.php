@extends('layouts.app')
@section('title', 'View Booking Price')
@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">Booking Price Admins</li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
             @include('flash::message')
             <div class="row">
                 <div class="col-lg-12">
                     <div class="card">
                         <div class="card-header">
                             <i class="fa fa-align-justify"></i>
                             Booking_price_admins
                             <a class="pull-right" href="{{ route('bookingPriceAdmins.create') }}"><i class="fa fa-plus-square fa-lg"></i></a>
                         </div>
                         <div class="card-body">
                             @include('booking_price_admins.table')
                              <div class="pull-right mr-3">
                                     
                              </div>
                         </div>
                     </div>
                  </div>
             </div>
         </div>
    </div>
@endsection

