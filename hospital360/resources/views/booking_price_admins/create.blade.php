@extends('layouts.app')
@section('title', 'Create Booking Price')
@section('content')
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
         <a href="{!! route('bookingPriceAdmins.index') !!}">Booking Price Admin</a>
      </li>
      <li class="breadcrumb-item active">Create</li>
    </ol>
     <div class="container-fluid">
          <div class="animated fadeIn">
                @include('coreui-templates::common.errors')
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <i class="fa fa-plus-square-o fa-lg"></i>
                                <strong>Create Booking Price Admin</strong>
                            </div>
                            <div class="card-body">
                                {!! Form::open(['route' => 'bookingPriceAdmins.store','id' => 'myForm']) !!}

                                   @include('booking_price_admins.fields')

                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
           </div>
    </div>
@endsection
