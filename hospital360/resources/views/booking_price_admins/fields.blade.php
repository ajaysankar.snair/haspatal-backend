<div class="row">
<!-- Book Price Field -->
<div class="form-group col-sm-6">
    {!! Form::label('book_price', 'Book Price:') !!}
    {!! Form::text('book_price', null, ['class' => 'form-control','data-parsley-type="integer"','data-parsley-required','required']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-6">
    {!! Form::label('description', 'Description:') !!}
    {!! Form::text('description', null, ['class' => 'form-control','data-parsley-type="alphanum"','data-parsley-required','required']) !!}
</div>
</div>
<div class="row">
<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <!-- <a href="{{ route('bookingPriceAdmins.index') }}" class="btn btn-secondary">Cancel</a> -->
</div>
</div>
