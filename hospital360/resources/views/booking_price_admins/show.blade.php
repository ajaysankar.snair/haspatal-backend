@extends('layouts.app')
@section('title', 'Show Booking Price')
@section('content')
     <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{ route('bookingPriceAdmins.index') }}">Booking Price Admin</a>
            </li>
            <li class="breadcrumb-item active">Detail</li>
     </ol>
     <div class="container-fluid">
          <div class="animated fadeIn">
                 @include('coreui-templates::common.errors')
                 <div class="row">
                     <div class="col-lg-12">
                         <div class="card">
                             <div class="card-header">
                                 <strong>Details</strong>
                                  <a href="{{ route('bookingPriceAdmins.index') }}" class="btn btn-light">Back</a>
                             </div>
                             <div class="card-body">
                                 @include('booking_price_admins.show_fields')
                             </div>
                         </div>
                     </div>
                 </div>
          </div>
    </div>
@endsection
