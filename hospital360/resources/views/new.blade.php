@extends('layouts.nheader')
@section('title', 'Dashboard')
@section('content')
<style type="text/css">
    .card:hover {
        color: white;
  background-color: red;
}
.card{
  height: 215px; 
  border-radius: 20px; 
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
}
.card-title{
    margin-top: 5%;
}
a:hover {
    color: white;
    text-decoration: none;
}
</style>
<!-- <ol class="breadcrumb">
        <li class="breadcrumb-item">Doctors</li>
    </ol> -->
    <div class="container-fluid" style="margin-top: 3%;">
        <div class="animated fadeIn">
             <div class="container">
    <div class="card-deck">
        <div class="card  newcard text-center border-danger">
     <a href="{{ url('/profile') }}" style="color: black;">           
            <div class="card-block" style="padding: 50px;">
            	<img  src="{{ URL::asset('public/image/icons/Profile.png') }}" style="width: 30%;" alt="logo">
                <h4 class="card-title">Profile</h4>
            </div>
            
    </a>
        </div>

        <div class="card newcard text-center border-danger">
            <div class="card-block" style="padding: 50px;">
            	<img  src="{{ URL::asset('public/image/icons/Records.png') }}" style="width: 30%;" alt="logo">
                <h4 class="card-title">Records</h4>
                
            </div>
            
        </div>


         <a href="{{ url('/consults') }}" style="color: black;">   
        <div class="card newcard text-center border-danger">
            <div class="card-block" style="padding: 38px;">
            	<img  src="{{ URL::asset('public/image/icons/consult.png') }}" style="width: 30%;" alt="logo">
                <h4 class="card-title">Consults</h4>
                
            </div>
            
        </div>
    </a>
    </div>
    <div class="card-deck mt-4">

        <div class="card newcard text-center border-danger">
            <a href="{{ url('/wallet') }}" style="color: black;">   
            <div class="card-block" style="padding: 50px;">
            	<img  src="{{ URL::asset('public/image/icons/Wallet.png') }}" style="width: 30%;" alt="logo">
                <h4 class="card-title">Wallet</h4>
                
            </div>
        </a>
            
        </div>

        <div class="card newcard text-center border-danger">
            <div class="card-block" style="padding: 50px;">
            	<i class="fa fa-stethoscope" style="font-size: 80px;"></i>
                <h4 class="card-title">Review</h4>
                
            </div>
            
        </div>
        <div class="card newcard text-center border-danger">
            <div class="card-block" style="padding: 50px;">
            	<i class="fa fa-stethoscope" style="font-size: 80px;"></i>
                <h4 class="card-title">Support</h4>
            </div>
            
        </div>
    </div>
</div>

         </div>
    </div>
@endsection