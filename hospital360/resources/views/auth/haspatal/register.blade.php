<!DOCTYPE html>
<html lang="en">
<head>
    <base href="./">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Haspatal Register | Haspatal</title>
    <meta name="description" content="">
    <meta name="keyword" content="">
    <!-- Bootstrap-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ url('public/asset/parsley/css/parsley.css') }}">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@coreui/coreui@2.1.16/dist/css/coreui.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@icon/coreui-icons-free@1.0.1-alpha.1/coreui-icons-free.css">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.4.1/css/simple-line-icons.css"
          rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.3.0/css/flag-icon.min.css">

    <style type="text/css">
        .bg-primary {
    background-color: red!important;
}
.btn-primary {
    color: #fff;
    background-color: red;
    border-color: red;
}
 @media only screen and (min-width: 360px) and (max-width: 640px) { 

.logincss{
    margin-top: 35% !important;
}
.loginimage{
        margin-right: 25% !important;
}
    }
    </style>
</head>
<body class="app flex-row align-items-center" style="background-image: url(https://crimecoverage.in/haspatal/public/image/background.png);background-repeat: no-repeat;background-size: cover;background-color: white;">
<div class="container">
    <div class="row justify-content-center logincss">
        <div class="col-md-12">
            <title>Registration | Haspatal</title>
            <h3 class="text-center" style="margin-top: 5%;">Registration Form</h3><br>
            <div class="card-group" style="width: 100%;margin-left: 5%;box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);">
                <div class="card p-4"> 
                    <div class="card-body">
                        @include('coreui-templates::common.errors')
                        @include('flash::message')
                        <form method="post" action="{{ route('haspatal.otp') }}" id="myForm"  enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-md-4" style="border-right: 1px solid gray">
                                    <h4>Easy Navigation</h4>
                                    <div class="input-group mb-3">
                                        <?php
                                            $countryList = DB::table('country')->pluck('country','id');
                                            $stateList = DB::table('state')->pluck('state','id');   
                                            $cityList = DB::table('city')->pluck('city','id');   
                                        ?>
                                         {!! Form::select('country_id', ['' => 'Select Country'] + $countryList->toArray(), null, ['class' => 'form-control','id' =>'countryId','required','data-parsley-required-message'=>'Country is required' ]) !!}
                                        @if ($errors->has('country_id'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('country_id') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="input-group mb-3">
                                        <!--  {!! Form::select('state_id', ['' => 'Select State']+ $stateList->toArray(), null, ['class' => 'form-control','required','data-parsley-required-message'=>'State is required']) !!} -->
                                        {!! Form::select('state_id', ['' => 'Select State'], null, ['class' => 'form-control','id'=>'state']) !!}
                                        @if ($errors->has('state_id'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('state_id') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="input-group mb-3">
                                         <!-- {!! Form::select('city_id', ['' => 'Select City']+ $cityList->toArray(), null, ['class' => 'form-control','required','data-parsley-required-message'=>'City is required']) !!} -->
                                        {!! Form::select('city_id', ['' => 'Select City'], null, ['class' => 'form-control','id'=>'city']) !!}
                                        @if ($errors->has('city_id'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('city_id') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="input-group mb-3">
                                         <input type="text" class="form-control" placeholder="Pincode" name="pincode_id" data-parsley-required-message='Pincode is required' required value="{{ old('pincode_id') }}">
                                        @if ($errors->has('pincode_id'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('pincode_id') }}</strong>
                                            </span>
                                        @endif
                                    </div>                            
                                </div>
                                <div class="col-md-4" style="border-right: 1px solid gray">
                                    <h4>Hospital Info</h4>
                                    <div class="input-group mb-3">
                                         <input type="hospital_name" class="form-control" placeholder="Hospital Name" name="hospital_name" required data-parsley-required-message='Hospital Name is required' value="{{ old('hospital_name') }}">
                                        @if ($errors->has('hospital_name'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('hospital_name') }}</strong>
                                            </span>
                                        @endif
                                    </div> 

                                    <div class="input-group mb-3">
                                         <textarea type="address" class="form-control" placeholder="Addeess" required name="address" data-parsley-required-message='Addeess is required' value="{{ old('address') }}"></textarea>
                                        @if ($errors->has('address'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('address') }}</strong>
                                            </span>
                                        @endif
                                    </div> 

                                    <div class="input-group mb-3">
                                         <select class="form-control" placeholder="Year Of Establishment" required name="year_of_establishment" data-parsley-required-message="City is required">
                                            <option>Year Of Establishment</option>
                                             @for($i=date('Y');$i>=1970;$i--)
                                             <option value="{{$i}}">{{$i}}</option>
                                             @endfor
                                         </select>
                                         

                                        @if ($errors->has('year_of_establishment'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('year_of_establishment') }}</strong>
                                            </span>
                                        @endif
                                    </div> 

                                     <div class="input-group mb-3">
                                         <input type="gst_no" class="form-control" placeholder="GST No" required name="gst_no" data-parsley-required-message='GST No is required' value="{{ old('gst_no') }}">
                                        @if ($errors->has('gst_no'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('gst_no') }}</strong>
                                            </span>
                                        @endif
                                    </div> 

                                    <div class="input-group mb-3">
                                         <h9>License Copy</h9>
                                         <!-- <input type="license_copy" class="form-control" placeholder="License Copy" name="license_copy" data-parsley-required-message='License Copy is required' value="{{ old('license_copy') }}"> -->
                                         {!! Form::file('license_copy') !!}
                                        @if ($errors->has('license_copy'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('license_copy') }}</strong>
                                            </span>
                                        @endif
                                    </div> 

                                    <div class="input-group mb-3">
                                         <h9>Hospital Profile</h9>
                                         <!-- <input type="license_copy" class="form-control" placeholder="License Copy" name="license_copy" data-parsley-required-message='License Copy is required' value="{{ old('license_copy') }}"> -->
                                         {!! Form::file('hospital_profile') !!}
                                        @if ($errors->has('hospital_profile'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('hospital_profile') }}</strong>
                                            </span>
                                        @endif
                                    </div> 


                                    <div class="input-group mb-3">
                                         <h9>Hospital Location</h9>
                                         <!-- <input type="license_copy" class="form-control" placeholder="License Copy" name="license_copy" data-parsley-required-message='License Copy is required' value="{{ old('license_copy') }}"> -->
                                         {!! Form::file('hospital_location') !!}
                                        @if ($errors->has('hospital_location'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('hospital_location') }}</strong>
                                            </span>
                                        @endif
                                    </div> 

                                    <!-- <div class="input-group mb-3">
                                         <input type="website" class="form-control" required placeholder="Website" name="website" data-parsley-required-message='Website is required' value="{{ old('website') }}">
                                        @if ($errors->has('website'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('website') }}</strong>
                                            </span>
                                        @endif
                                    </div>  -->

                                </div>
                                <div class="col-md-4" style="border-right: 0px solid gray">
                                    <h4>Contact Info</h4>
                                    <div class="input-group mb-3">
                                        <input type="text" class="form-control" required placeholder="Email" name="email" data-parsley-required-message='Email is required' value="{{ old('email') }}">
                                        @if ($errors->has('email'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div> 

                                    <div class="input-group mb-3">
                                        <input type="text" class="form-control" required placeholder="Mobile No" name="mobile" data-parsley-required-message='Mobile is required' value="{{ old('mobile') }}">
                                        @if ($errors->has('mobile'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('mobile') }}</strong>
                                            </span>
                                        @endif
                                    </div> 

                                    <div class="input-group mb-3">
                                        <input type="text" class="form-control" required placeholder="Contact Person" name="contact_person" data-parsley-required-message='Contact Person is required' value="{{ old('contact_person') }}">
                                        @if ($errors->has('contact_person'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('contact_person') }}</strong>
                                            </span>
                                        @endif
                                    </div> 

                                    <div class="input-group mb-3">
                                         <input type="text" class="form-control" required placeholder="Contact Person Email" name="contact_person_email" data-parsley-required-message='Contact Person Email is required' value="{{ old('contact_person_email') }}">
                                        @if ($errors->has('contact_person_email'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('contact_person_email') }}</strong>
                                            </span>
                                        @endif
                                    </div> 

                                    <div class="input-group mb-3">
                                         <input type="text" class="form-control" required placeholder="Contact Person Phone No" name="contact_person_phonoe_no" data-parsley-required-message='Contact Person Phone No is required' value="{{ old('contact_person_phonoe_no') }}">
                                        @if ($errors->has('contact_person_phonoe_no'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('contact_person_phonoe_no') }}</strong>
                                            </span>
                                        @endif
                                    </div> 

                                    <div class="input-group mb-3">
                                         <input type="text" class="form-control" required placeholder="Other Phone No" name="other_phone_no" data-parsley-required-message='Other Phone No is required' value="{{ old('other_phone_no') }}">
                                        @if ($errors->has('other_phone_no'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('other_phone_no') }}</strong>
                                            </span>
                                        @endif
                                    </div> 

                                    <div class="input-group mb-3">
                                         <input type="text" class="form-control" required placeholder="Support Watsapp No" name="support_watsapp_no" data-parsley-required-message='Support Watsapp No is required' value="{{ old('support_watsapp_no') }}">
                                        @if ($errors->has('support_watsapp_no'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('support_watsapp_no') }}</strong>
                                            </span>
                                        @endif
                                    </div> 

                                    <div class="input-group mb-3">
                                        <input type="text" class="form-control" required placeholder="Telemedicine Coordinator" name="telemedicine_coordinator" data-parsley-required-message='Telemedicine Coordinator is required' value="{{ old('telemedicine_coordinator') }}">
                                        @if ($errors->has('telemedicine_coordinator'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('telemedicine_coordinator') }}</strong>
                                            </span>
                                        @endif
                                    </div> 
                                    <?php
                                    $otp = rand(1111,9999);
                                    ?>
                                    <input type="hidden" name="otp" value="{{$otp}}">
                                </div>
                            </div>
                            <div class="row" style="margin-top: 3%">
                                <div class="col-md-6">
                                    <input type="checkbox" name="tearm_and_condition" required="">
                                    I agree all the <span style="color: red">Terms and Conditions</span>
                                </div>
                                <div class="col-md-6" style="text-align: right;">
                                    <button class="btn btn-primary px-4" type="submit">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- CoreUI and necessary plugins-->
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/@coreui/coreui@2.1.16/dist/js/coreui.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/1.4.0/perfect-scrollbar.js"></script>
 <script src="{{ url('public/asset/parsley/js/parsley.js') }} "></script> 
     <script type="text/javascript">
         $(function () {
            
          $('#myForm').parsley().on('field:validated', function() {
            var ok = $('.parsley-error').length === 0;
            $('.bs-callout-info').toggleClass('hidden', !ok);
            $('.bs-callout-warning').toggleClass('hidden', ok);
          })
        });

     </script>
     <script type="text/javascript">
    
$('#countryId').on('load change',function(){
   var country = $(this).val();


    // alert("test")
    if(country){
        $.ajax({
           type:"GET",
           url:"{{url('get-state-list')}}?country_id="+country,
           success:function(res){
            if(res){
                $("#state").empty();
                $("#state").append('<option value="">Select State</option>');
                $.each(res,function(key,value){
                    $("#state").append('<option value="'+key+'">'+value+'</option>');
                });

            }else{
               $("#state").empty();
            }
           }
        });
    }else{
        $("#state").empty();
    }

});
</script>
<script type="text/javascript">
$('#state').on('load change',function(){
    var state = $(this).val();
    // alert("test")
    if(state){
        $.ajax({
           type:"GET",
           url:"{{url('get-city-list')}}?state_id="+state,
           success:function(res){
            if(res){
                $("#city").empty();
                $("#city").append('<option value="">Select City</option>');
                $.each(res,function(key,value){
                    $("#city").append('<option value="'+key+'">'+value+'</option>');
                });

            }else{
               $("#city").empty();
            }
           }
        });
    }else{
        $("#city").empty();
    }

});
</script>
<!-- <script type="text/javascript">
$('#countryId').on('load change',function(){
   var country = $(this).val();


    // alert("test")
    if(country){
        $.ajax({
           type:"GET",
           url:"{{url('getstate')}}?country_code="+country,
           success:function(res){
            if(res){
                $("#state").empty();
                $("#state").append('<option value="">Select State</option>');
                $.each(res,function(key,value){
                    $("#state").append('<option value="'+key+'">'+value+'</option>');
                });

            }else{
               $("#state").empty();
            }
           }
        });
    }else{
        $("#state").empty();
    }

});
</script>
<script type="text/javascript">
$('#state').on('load change',function(){
    var state = $(this).val();
    // alert("test")
    if(state){
        $.ajax({
           type:"GET",
           url:"{{url('getcity')}}?country_code="+state,
           success:function(res){
            if(res){
                $("#city").empty();
                $("#city").append('<option value="">Select City</option>');
                $.each(res,function(key,value){
                    $("#city").append('<option value="'+key+'">'+value+'</option>');
                });

            }else{
               $("#city").empty();
            }
           }
        });
    }else{
        $("#city").empty();
    }

});
</script> -->
</body>
</html>
