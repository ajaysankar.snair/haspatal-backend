<!DOCTYPE html>
<html lang="en">
<head>
    <base href="./">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>H360 Register | Haspatal</title>
    <meta name="description" content="">
    <meta name="keyword" content="">
    <!-- Bootstrap-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ url('public/asset/parsley/css/parsley.css') }}">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@coreui/coreui@2.1.16/dist/css/coreui.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@icon/coreui-icons-free@1.0.1-alpha.1/coreui-icons-free.css">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.4.1/css/simple-line-icons.css"
          rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.3.0/css/flag-icon.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/css/bootstrap-datetimepicker.min.css">

    <style type="text/css">
        .bg-primary {
    background-color: red!important;
}
.btn-primary {
    color: #fff;
    background-color: red;
    border-color: red;
}
 @media only screen and (min-width: 360px) and (max-width: 640px) { 

.logincss{
    margin-top: 35% !important;
}
.loginimage{
        margin-right: 25% !important;
}
    }
    </style>
</head>
<body class="app flex-row align-items-center" style="background-image: url(https://crimecoverage.in/haspatal/public/image/background.png);background-repeat: no-repeat;background-size: cover;background-color: white;">
<div class="container">
    <div class="row justify-content-center logincss">
        <div class="col-md-12">
            <title>Registration | Haspatal</title>
            <h3 class="text-center" style="margin-top: 5%;">Registration Form</h3><br>
            <div class="card-group" style="width: 100%;margin-left: 5%;box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);">
                <div class="card p-4"> 
                    <div class="card-body">
                        @include('coreui-templates::common.errors')
                        @include('flash::message')
                        <form method="post" action="{{ route('haspatal.register360') }}" id="myForm"  enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-md-4" style="border-right: 1px solid gray">
                                    <!-- <h4>Contact Info</h4> -->
                                    <div class="input-group mb-3">
                                        <!-- <input type="text" class="form-control" required placeholder="Email" name="email" data-parsley-required-message='Email is required' value="{{ old('email') }}"> -->
                                        <select class="form-control" name="role_id">
                                            <option value="">Select Service</option>
                                            <option value="11">Pharmacies</option>
                                            <option value="12">Diagnostics</option>
                                            <option value="13">Imaging</option>
                                            <option value="14">Homecare</option>
                                            <option value="16">Specialist</option>
                                        </select>
                                        @if ($errors->has('role_id'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('role_id') }}</strong>
                                            </span>
                                        @endif
                                    </div> 

                                    <div class="input-group mb-3">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <select class="form-control" name="name_prefix" required data-parsley-required-message='Name Prefix is required'>
                                                    <option value="Mr.">Mr.</option>
                                                    <option value="Miss.">Miss.</option>
                                                </select>
                                            </div>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" required placeholder="Full Name" name="full_name" data-parsley-required-message='Email is required' value="{{ old('full_name') }}">
                                                @if ($errors->has('full_name'))
                                                    <span class="invalid-feedback">
                                                        <strong>{{ $errors->first('full_name') }}</strong>
                                                    </span>
                                                @endif
                                            </div>    
                                        </div>
                                        
                                    </div> 

                                    <div class="input-group mb-3">
                                        <input type="text" class="form-control" required placeholder="Email" name="email" data-parsley-required-message='Email is required' value="{{ old('email') }}">
                                        @if ($errors->has('email'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div> 

                                    <div class="input-group mb-3">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <input type="text" class="form-control" placeholder="+91" name="mo_prefix" required data-parsley-required-message='Mo Prefix is required'>
                                            </div>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" required placeholder="Mobile No" name="mobile_no" data-parsley-required-message='mobile is required' value="{{ old('mobile_no') }}">
                                                @if ($errors->has('mobile_no'))
                                                    <span class="invalid-feedback">
                                                        <strong>{{ $errors->first('mobile_no') }}</strong>
                                                    </span>
                                                @endif
                                                
                                            </div>
                                        </div>
                                    </div> 

                                    

                                    <div class="input-group mb-3">
                                        <input type="password" class="form-control" required placeholder="Password" name="password" data-parsley-required-message='password is required' value="{{ old('password') }}">
                                        @if ($errors->has('password'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div> 

                                    
                                    <?php
                                    $otp = rand(1111,9999);
                                    ?>
                                    <input type="hidden" name="otp" value="{{$otp}}">
                                </div>
                                <div class="col-md-4" style="border-right: 1px solid gray">
                                    <!-- <h4>Easy Navigation</h4> -->
                                    
                                    <div class="input-group mb-3">
                                         <input type="text" class="form-control" placeholder="Business Name" name="b_name" required data-parsley-required-message='Business Name is required' value="{{ old('b_name') }}">
                                        @if ($errors->has('b_name'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('b_name') }}</strong>
                                            </span>
                                        @endif
                                    </div>   
                                    <div class="input-group mb-3">
                                         <h9>Business Card Photo</h9>
                                         {!! Form::file('b_card_pic') !!}
                                        @if ($errors->has('b_card_pic'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('b_card_pic') }}</strong>
                                            </span>
                                        @endif
                                    </div> 
                                    <div class="input-group mb-3">
                                         <h9>Shop Photo</h9>
                                         {!! Form::file('shop_pic') !!}
                                        @if ($errors->has('shop_pic'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('shop_pic') }}</strong>
                                            </span>
                                        @endif
                                    </div> 
                                    <div class="input-group mb-3">
                                         <h9>Buisness License </h9>
                                         {!! Form::file('b_lic') !!}
                                        @if ($errors->has('b_lic'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('b_lic') }}</strong>
                                            </span>
                                        @endif
                                    </div> 
                                     <div class="input-group mb-3">
                                         We Are 24 Hours Open 
                                         <input type="checkbox" name="open_24" id="open_24" class="form-control">
                                    </div> 
                                    <div class="input-group mb-3">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <input type="text" class="form-control" placeholder="Open Time" name="open_time" value="{{ old('open_time') }}" id="open_time">
                                                @if ($errors->has('open_time'))
                                                    <span class="invalid-feedback">
                                                        <strong>{{ $errors->first('open_time') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                            <div class="col-md-6">
                                                <input type="text" class="form-control" placeholder="Close Time" name="close_time" value="{{ old('close_time') }}" id="close_time">
                                                @if ($errors->has('close_time'))
                                                    <span class="invalid-feedback">
                                                        <strong>{{ $errors->first('close_time') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                         
                                    </div>   
                                   
                                    <div class="input-group mb-3">
                                         All Days Open 
                                         <input type="checkbox" name="all_days_open" id="all_days_open" class="form-control">
                                    </div> 
                                    <div class="input-group mb-3">
                                         <input type="text" class="form-control" placeholder="Weekly off" name="weekly_off"  value="{{ old('weekly_off') }}" id="weekly_off">
                                        @if ($errors->has('weekly_off'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('weekly_off') }}</strong>
                                            </span>
                                        @endif
                                    </div> 
                                    

                                        
                                </div>
                                <div class="col-md-4" >
                                    <!-- <h4>Hospital Info</h4> -->
                                    <div class="input-group mb-3">
                                        <?php
                                            $countryList = DB::table('country')->pluck('country','id');
                                            $stateList = DB::table('state')->pluck('state','id');   
                                            $cityList = DB::table('city')->pluck('city','id');   
                                        ?>
                                         {!! Form::select('country_id', ['' => 'Select Country'] + $countryList->toArray(), null, ['class' => 'form-control','id' =>'countryId','required','data-parsley-required-message'=>'Country is required' ]) !!}
                                        @if ($errors->has('country_id'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('country_id') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="input-group mb-3">
                                        {!! Form::select('state_id', ['' => 'Select State'], null, ['class' => 'form-control','required','data-parsley-required-message'=>'State is required','id'=>'state']) !!}
                                        <!-- {!! Form::select('state_id', ['' => 'Select State'], null, ['class' => 'form-control']) !!} -->
                                        @if ($errors->has('state_id'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('state_id') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="input-group mb-3">
                                         
                                        {!! Form::select('city_id', ['' => 'Select City'], null, ['class' => 'form-control','id'=>'city']) !!}
                                        @if ($errors->has('city_id'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('city_id') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="input-group mb-3">
                                        <input type="text" class="form-control" placeholder="Pincode" name="pincode_id" data-parsley-required-message='Pincode is required' required value="{{ old('pincode_id') }}">
                                        @if ($errors->has('pincode_id'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('pincode_id') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="input-group mb-3">
                                         <textarea type="address" class="form-control" placeholder="Addeess" required name="address" data-parsley-required-message='Addeess is required' value="{{ old('address') }}"></textarea>
                                        @if ($errors->has('address'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('address') }}</strong>
                                            </span>
                                        @endif
                                    </div> 

                                    

                                    <div class="input-group mb-3">
                                         <input type="text" class="form-control" placeholder="Contact Person" name="conatact_person" required data-parsley-required-message='Contact Person is required' value="{{ old('conatact_person') }}">
                                        @if ($errors->has('conatact_person'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('conatact_person') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="input-group mb-3">
                                         <input type="text" class="form-control" placeholder="Mobile 1" name="mobile1" required data-parsley-required-message='Mobile 1 is required' value="{{ old('mobile1') }}">
                                        @if ($errors->has('mobile1'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('mobile1') }}</strong>
                                            </span>
                                        @endif
                                    </div> 

                                    <div class="input-group mb-3">
                                         <input type="text" class="form-control" placeholder="Mobile 2" name="mobile2" required data-parsley-required-message='Mobile 2 is required' value="{{ old('mobile2') }}">
                                        @if ($errors->has('mobile2'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('mobile2') }}</strong>
                                            </span>
                                        @endif
                                    </div> 
                                    
                                    <div class="input-group mb-3">
                                         <textarea class="form-control" placeholder="Patient Select You" required name="patient_select_you" data-parsley-required-message='Patient Select You is required' value="{{ old('patient_select_you') }}"></textarea>
                                        @if ($errors->has('patient_select_you'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('patient_select_you') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                </div>
                                
                            </div>
                            <div class="row" style="margin-top: 3%">
                                <div class="col-md-6">
                                   <!--  <input type="checkbox" name="tearm_and_condition" required="">
                                    I agree all the <span style="color: red">Terms and Conditions</span> -->
                                </div>
                                <div class="col-md-6" style="text-align: right;">
                                    <button class="btn btn-primary px-4" type="submit">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- CoreUI and necessary plugins-->
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.1/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/@coreui/coreui@2.1.16/dist/js/coreui.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/1.4.0/perfect-scrollbar.js"></script>
 <script src="{{ url('public/asset/parsley/js/parsley.js') }} "></script> 
     <script type="text/javascript">
         $(function () {
            
          $('#myForm').parsley().on('field:validated', function() {
            var ok = $('.parsley-error').length === 0;
            $('.bs-callout-info').toggleClass('hidden', !ok);
            $('.bs-callout-warning').toggleClass('hidden', ok);
          })
        });

     </script>

<script type="text/javascript">
    var startTime = $('#open_time').datetimepicker({
        format: 'HH:mm',
        useCurrent: false,
        icons: {
                   up: "icon-arrow-up-circle icons font-2xl",
                   down: "icon-arrow-down-circle icons font-2xl"
               },
        sideBySide: false
    })

    var endTime = $('#close_time').datetimepicker({
        format: 'HH:mm',
        useCurrent: false,
        minDate: startTime.data("DateTimePicker").date(),
        icons: {
                   up: "icon-arrow-up-circle icons font-2xl",
                   down: "icon-arrow-down-circle icons font-2xl"
               },
        sideBySide: false
    })

    function setMinDate() {
    return endTime
      .data("DateTimePicker").minDate(
        startTime.data("DateTimePicker").date()
      )
    ;
  }
  
  var bound = false;
  function bindMinEndTimeToStartTime() {
  
    return bound || startTime.on('dp.change', setMinDate);
  }

  endTime.on('dp.change', () => {
    bindMinEndTimeToStartTime();
    bound = true;
    setMinDate();
  });
</script>

<script type="text/javascript">
    $(function () {

        $('#open_24').click(function() {
            
            if(this.checked) {
                $("#open_time").hide();
                $("#close_time").hide();
            } else {
                $("#open_time").show();
                $("#close_time").show();
            }
        });
        
    });
</script>
<script type="text/javascript">
    $(function () {

        $('#all_days_open').click(function() {
            
            if(this.checked) {
                $("#weekly_off").hide();
            } else {
                $("#weekly_off").show();
            }
        });
        
    });
</script>

<script type="text/javascript">
    
</script>
    
<script type="text/javascript">

$('#countryId').on('load change',function(){
   var country = $(this).val();


    // alert("test")
    if(country){
        $.ajax({
           type:"GET",
           url:"{{url('get-state-list')}}?country_id="+country,
           success:function(res){
            if(res){
                $("#state").empty();
                $("#state").append('<option value="">Select State</option>');
                $.each(res,function(key,value){
                    $("#state").append('<option value="'+key+'">'+value+'</option>');
                });

            }else{
               $("#state").empty();
            }
           }
        });
    }else{
        $("#state").empty();
    }

});
</script>
<script type="text/javascript">
    $('#state').on('load change',function(){
        var state = $(this).val();
        // alert("test")
        if(state){
            $.ajax({
               type:"GET",
               url:"{{url('get-city-list')}}?state_id="+state,
               success:function(res){
                if(res){
                    $("#city").empty();
                    $("#city").append('<option value="">Select City</option>');
                    $.each(res,function(key,value){
                        $("#city").append('<option value="'+key+'">'+value+'</option>');
                    });

                }else{
                   $("#city").empty();
                }
               }
            });
        }else{
            $("#city").empty();
        }

    });
</script>
<!-- <script type="text/javascript">
$('#countryId').on('load change',function(){
   var country = $(this).val();


    // alert("test")
    if(country){
        $.ajax({
           type:"GET",
           url:"{{url('getstate')}}?country_code="+country,
           success:function(res){
            if(res){
                $("#state").empty();
                $("#state").append('<option value="">Select State</option>');
                $.each(res,function(key,value){
                    $("#state").append('<option value="'+key+'">'+value+'</option>');
                });

            }else{
               $("#state").empty();
            }
           }
        });
    }else{
        $("#state").empty();
    }

});
</script>
<script type="text/javascript">
$('#state').on('load change',function(){
    var state = $(this).val();
    // alert("test")
    if(state){
        $.ajax({
           type:"GET",
           url:"{{url('getcity')}}?country_code="+state,
           success:function(res){
            if(res){
                $("#city").empty();
                $("#city").append('<option value="">Select City</option>');
                $.each(res,function(key,value){
                    $("#city").append('<option value="'+key+'">'+value+'</option>');
                });

            }else{
               $("#city").empty();
            }
           }
        });
    }else{
        $("#city").empty();
    }

});
</script> -->
</body>
</html>
