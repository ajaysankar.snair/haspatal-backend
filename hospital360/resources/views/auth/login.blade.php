<!DOCTYPE html>
<html lang="en">
<head>
    <base href="./">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Login | Haspatal</title>
    <meta name="description" content="">
    <meta name="keyword" content="">
    <!-- Bootstrap-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ url('public/asset/parsley/css/parsley.css') }}">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@coreui/coreui@2.1.16/dist/css/coreui.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@icon/coreui-icons-free@1.0.1-alpha.1/coreui-icons-free.css">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.4.1/css/simple-line-icons.css"
          rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.3.0/css/flag-icon.min.css">

    <style type="text/css">
        .bg-primary {
    background-color: red!important;
}
.btn-primary {
    color: #fff;
    background-color: red;
    border-color: red;
}
 @media only screen and (min-width: 360px) and (max-width: 640px) { 

.logincss{
    margin-top: 35% !important;
}
.loginimage{
        margin-right: 25% !important;
}
    }
    </style>
</head>
<body class="app flex-row align-items-center" style="background-image: url(https://crimecoverage.in/haspatal/public/image/background.png);background-repeat: no-repeat;background-size: cover;background-color: white;">
<div class="container">
    <div class="row justify-content-center logincss">
        <div class="col-md-6" style="padding: 5%">
            <h1 class="text-center" style="margin-top: 15%;">Sign in Here</h1><br>

            <div class="card-group" style="width: 80%;margin-left: 13%;box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);border: 1px solid red;">
                <div class="card p-4"> 
                    <div class="card-body">
                        @include('flash::message')
                        <form method="post" action="{{ url('/login') }}" id="myForm1">
                            @csrf
                           <!--  <?php  $img = url('public/image/logo.png');  ?>
                             <img class="navbar-brand-full" src="{{$img}}" style="width: 100%;margin-bottom: 10%;"  alt="Logo"> -->
                            <!-- <h1><center>Haspatal</center></h1> -->
                            <!-- <img src="{{ URL::asset('public/image/haspatallogo.png') }}" alt="logo" style="width: 100%;"><br><br> -->
                            <!-- <p class="text-muted">Sign In to your account</p> -->
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                      <i class="icon-user"></i>
                                    </span>
                                </div>
                                <input type="email" class="form-control {{ $errors->has('email')?'is-invalid':'' }}" name="email" value="{{ old('email') }}"
                                       placeholder="Email" data-parsley-required-message='Email is required' >
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="input-group mb-4">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                      <i class="icon-lock"></i>
                                    </span>
                                </div>
                                <input type="password" class="form-control {{ $errors->has('password')?'is-invalid':'' }}" placeholder="Password" name="password" data-parsley-required-message='Password is required'>
                                @if ($errors->has('password'))
                                    <span class="invalid-feedback">
                                       <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="row">
                                <div class="col-12 text-center" >
                                    <button class="btn btn-primary px-4" type="submit" style="width: 77%;">Login</button>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 text-center">
                                    <a class="btn btn-link px-0" href="{{ url('/password/reset') }}">
                                        Forgot password?
                                    </a>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
         <div class="col-md-6" style="padding: 5%">
             <img  class="loginimage" src="{{ URL::asset('public/image/Dr Login.jpg') }}" alt="logo" style="width: 50%; float: right;">
        </div>
    </div>
</div>
<!-- CoreUI and necessary plugins-->
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/@coreui/coreui@2.1.16/dist/js/coreui.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/1.4.0/perfect-scrollbar.js"></script>
 <script src="{{ url('public/asset/parsley/js/parsley.js') }} "></script> 
     <script type="text/javascript">
         $(function () {
            
          $('#myForm').parsley().on('field:validated', function() {
            var ok = $('.parsley-error').length === 0;
            $('.bs-callout-info').toggleClass('hidden', !ok);
            $('.bs-callout-warning').toggleClass('hidden', ok);
          })
        });

     </script>
</body>
</html>
