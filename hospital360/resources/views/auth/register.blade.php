<!DOCTYPE html>
<html lang="en">
<head>
    <base href="./">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Register | {{config('app.name')}}</title>
    <meta name="description" content="Haspatal">
    <meta name="keyword" content="">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@coreui/coreui@2.1.16/dist/css/coreui.min.css">
     <link rel="stylesheet" href="{{ url('public/asset/parsley/css/parsley.css') }}">
     <link rel="stylesheet" href="{{ url('public/css/newheader.css') }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@icon/coreui-icons-free@1.0.1-alpha.1/coreui-icons-free.css">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.4.1/css/simple-line-icons.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.3.0/css/flag-icon.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="{{ url('public/asset/ckeditor/ckeditor.js') }}"></script>
<style type="text/css">
    .parsley-required{
    color: red;
}
.alert-success {
    color: white;
    background-color: green;
    border-color: #cdedd8;
}
.btn-danger {
    color: #fff;
    background-color: red;
    border-color: red;
    width: 12%;
}
</style>
</head>
<body class="app flex-row align-items-center">
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card mx-4">

                <div class="card-body p-4">
                   {!! Form::open(['route' => 'doctorsreg.update', 'files' => true,'id' => 'myForm']) !!}
                        @csrf
                         <img src="{{ URL::asset('public/image/haspatallogo.png') }}" alt="logo" style="width: 30%;margin-left: 34%;"><br><br>
                        <p class="text-muted">Create your account</p>
                        <?php 
                        $countryList = DB::table('country')->pluck('country','id');
                        $stateList = DB::table('state')->pluck('state','id');
                        $cityList = DB::table('city')->pluck('city','id');
                        $languageList = DB::table('language')->pluck('language','id');
                        $specilities= DB::table('specilities')->pluck('specility','id');
                        $licenceTyes= DB::table('licence_type')->pluck('licence_name','id');
                        $Plan= DB::table('plan_master')->pluck('pl_name','pl_id');
                         ?>
                         @include('flash::message')
                        <div class="row">
<div class="form-group col-sm-6">
    {!! Form::label('first_name', 'First Name:') !!}
    {!! Form::text('first_name', null, ['class' => 'form-control','data-parsley-required-message' =>'First Name is required','required']) !!}
</div>

<!-- Last Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('last_name', 'Last Name:') !!}
    {!! Form::text('last_name', null, ['class' => 'form-control','data-parsley-required-message' =>'Last Name is required','required']) !!}
</div>
</div>
<div class="row">
<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::email('email', null, ['class' => 'form-control','data-parsley-required-message' =>'Email is required','required']) !!}
</div>

<!-- Bio Field -->
<div class="form-group col-sm-6 col-lg-6">
    {!! Form::label('bio', 'Bio:') !!}
    {!! Form::textarea('bio', null, ['class' => 'form-control','rows' => '3','data-parsley-required-message' =>'Bio is required','required']) !!}
</div>
</div>
<div class="row">
<!-- Pincode Field -->
<div class="form-group col-sm-6">
    {!! Form::label('pincode', 'Pincode:') !!}
    {!! Form::text('pincode', null, ['class' => 'form-control','data-parsley-required-message' =>'Pincode is required','required']) !!}
</div>

<!-- State Field -->
<div class="form-group col-sm-6">
    {!! Form::label('state', 'State:') !!}
    {!! Form::select('state', [''=>'Select State'] + $stateList->toArray(), null, ['class' => 'form-control','data-parsley-required-message' =>'State is required','required']) !!}
</div>
</div>
<div class="row">
<!-- City Field -->
<div class="form-group col-sm-6">
    {!! Form::label('city', 'City:') !!}
    {!! Form::select('city', [''=>'Select City'] + $cityList->toArray(), null, ['class' => 'form-control','data-parsley-required-message' =>'City is required','required']) !!}
</div>

<!-- Address Field -->
<div class="form-group col-sm-6 col-lg-6">
    {!! Form::label('address', 'Address:') !!}
    {!! Form::textarea('address', null, ['class' => 'form-control','rows' => '3','data-parsley-required-message' =>'Address is required','required']) !!}
</div>
</div>
<div class="row">
<!-- Main Mobile Field -->
<div class="form-group col-sm-6">
    {!! Form::label('main_mobile', 'Main Mobile:') !!}
    {!! Form::text('main_mobile', null, ['class' => 'form-control','data-parsley-required-message' =>'Main Mobile is required','required']) !!}
</div>

<!-- Second Mobile Field -->
<div class="form-group col-sm-6">
    {!! Form::label('second_mobile', 'Second Mobile:') !!}
    {!! Form::text('second_mobile', null, ['class' => 'form-control']) !!}
</div>
</div>
<div class="row">
<!-- Clinic Team Leader Field -->
<div class="form-group col-sm-6">
    {!! Form::label('clinic_team_leader', 'Clinic Team Leader:') !!}
    {!! Form::text('clinic_team_leader', null, ['class' => 'form-control','data-parsley-required-message' =>'Clinic TL Name is required','required']) !!}
</div>

<!-- Experience Field -->
<div class="form-group col-sm-6">
    {!! Form::label('experience', 'Experience:') !!}
    {!! Form::text('experience', null, ['class' => 'form-control','data-parsley-required-message' =>'Experience is required', 'required']) !!}
</div>
</div>
<div class="row">
<!-- Language Field -->
<div class="form-group col-sm-6">
    {!! Form::label('language', 'Language:') !!}
    <!-- {!! Form::select('language[]', [''=>'Select Language'] + $languageList->toArray(), null, ['class' => 'form-control','data-parsley-required-message' =>'Language is required','required','multiple' => 'multiple']) !!} -->

    {!! Form::select('language[]',$languageList->toArray(),null,['class'=>'form-control','data-parsley-required-message' =>'Language is required','required','multiple' => 'multiple']) !!}
</div>

<!-- Speciality Field -->
<div class="form-group col-sm-6">
    {!! Form::label('speciality', 'Speciality:') !!}
    {!! Form::select('speciality', [''=>'Select Specilities'] + $specilities->toArray(), null, ['class' => 'form-control','data-parsley-required-message' =>'Speciality is required','required']) !!}
</div>
</div>
<div class="row">
<!-- Licence Type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('licence_type', 'Licence Type:') !!}
    {!! Form::select('licence_type', [''=>'Select Licence Type'] + $licenceTyes->toArray(), null, ['class' => 'form-control','data-parsley-required-message' =>'Licence Type is required','required']) !!}
    
</div>

<!-- Licence No Field -->
<div class="form-group col-sm-6">
    {!! Form::label('licence_no', 'Licence No:') !!}
    {!! Form::text('licence_no', null, ['class' => 'form-control','data-parsley-required-message' =>'Licence No is required','required']) !!}
</div>
</div>
<div class="row">
<!-- Valide Upto Field -->
<div class="form-group col-sm-6">
    {!! Form::label('valide_upto', 'Valide Upto:') !!}
    {!! Form::text('valide_upto', null, ['class' => 'form-control','id'=>'valide_upto','data-parsley-required-message' =>'Valid Upto is required', 'required']) !!}
</div>
<!-- Issued By Field -->
<div class="form-group col-sm-6">
    {!! Form::label('issued_by', 'Issued By:') !!}
    {!! Form::text('issued_by', null, ['class' => 'form-control','data-parsley-required-message' =>'Issued By is required', 'required']) !!}
</div>
</div>
<div class="row">
<!-- Licence Copy Field -->
<div class="form-group col-sm-6">
    {!! Form::label('licence_copy', 'Licence Copy:') !!}
    {!! Form::file('licence_copy',['data-parsley-required-message' =>'Licence Copy is required','required']) !!}
</div>
<div class="clearfix"></div>

<!-- Profile Pic Field -->
<div class="form-group col-sm-6">
    {!! Form::label('profile_pic', 'Profile Pic:') !!}
    {!! Form::file('profile_pic',['data-parsley-required-message' =>'Profile Pic is required','required']) !!}
</div>
</div>
<div class="row">
    <div class="form-group col-sm-6">
    {!! Form::label('price', 'Price:') !!}
    {!! Form::text('price', null, ['class' => 'form-control','data-parsley-required-message' =>'Price is required', 'required']) !!}
</div>
<div class="form-group col-sm-6">
    {!! Form::label('plan_id', 'Select Plan:') !!}
    {!! Form::select('plan', [''=>'Select Plan'] + $Plan->toArray(), null, ['class' => 'form-control','data-parsley-required-message' =>'Plan is required','required']) !!}
    
</div>
</div>
<div class="row">
<!-- Main Mobile Field -->
<div class="form-group col-sm-6">
    {!! Form::label('password', 'Password:') !!}
    {!! Form::password('password', ['class' => 'form-control','id' => 'password','data-parsley-required-message' =>'Password is required','required']) !!}
</div>

<!-- Second Mobile Field -->
<div class="form-group col-sm-6">
    {!! Form::label('confirm_password', 'Confirm Password:') !!}
    {!! Form::password('confirm_password', ['class' => 'form-control','data-parsley-required-message' =>'Confirm Password is required','data-parsley-equalto'=>'#password','data-parsley-equalto-message'=>'Confirm password not match with password','required']) !!}
</div>
</div>
<div class="clearfix"></div>

<!-- Status Field -->
<!-- <div class="form-group col-sm-6">
    {!! Form::label('status', 'Status:') !!}
    {!! Form::text('status', null, ['class' => 'form-control']) !!}
</div> -->

<!-- Created By Field -->
<!-- <div class="form-group col-sm-6">
    {!! Form::label('created_by', 'Created By:') !!}
    {!! Form::text('created_by', null, ['class' => 'form-control']) !!}
</div>
 -->
<!-- Updated By Field -->
<!-- <div class="form-group col-sm-6">
    {!! Form::label('updated_by', 'Updated By:') !!}
    {!! Form::text('updated_by', null, ['class' => 'form-control']) !!}
</div> -->
<div class="row">
<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-danger']) !!}
    <a href="{{ url('/login') }}" class="btn btn-primary">Back To Login</a>
</div>
</div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
<!-- CoreUI and necessary plugins-->
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.20.1/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/@coreui/coreui@2.1.16/dist/js/coreui.min.js"></script>
  <script src="{{ url('public/asset/parsley/js/parsley.js') }} "></script> 
     <script type="text/javascript">
         $(function () {
            
          $('#myForm').parsley().on('field:validated', function() {
            var ok = $('.parsley-error').length === 0;
            $('.bs-callout-info').toggleClass('hidden', !ok);
            $('.bs-callout-warning').toggleClass('hidden', ok);
          })
        });

     </script>
<script type="text/javascript">
           $('#valide_upto').datetimepicker({
               format: 'YYYY-MM-DD',
               useCurrent: true,
               icons: {
                   up: "icon-arrow-up-circle icons font-2xl",
                   down: "icon-arrow-down-circle icons font-2xl"
               },
               sideBySide: true
           })
       </script>
</body>
</html>
