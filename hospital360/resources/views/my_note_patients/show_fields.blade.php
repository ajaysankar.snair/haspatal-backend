<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{{ $myNotePatient->id }}</p>
</div>

<!-- My Patient Id Field -->
<div class="form-group">
    {!! Form::label('my_patient_id', 'My Patient Id:') !!}
    <p>{{ $myNotePatient->my_patient_id }}</p>
</div>

<!-- My Doctor Id Field -->
<div class="form-group">
    {!! Form::label('my_doctor_id', 'My Doctor Id:') !!}
    <p>{{ $myNotePatient->my_doctor_id }}</p>
</div>

<!-- My Note Field -->
<div class="form-group">
    {!! Form::label('my_note', 'My Note:') !!}
    <p>{{ $myNotePatient->my_note }}</p>
</div>

<!-- Created By Field -->
<div class="form-group">
    {!! Form::label('created_by', 'Created By:') !!}
    <p>{{ $myNotePatient->created_by }}</p>
</div>

<!-- Updated By Field -->
<div class="form-group">
    {!! Form::label('updated_by', 'Updated By:') !!}
    <p>{{ $myNotePatient->updated_by }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $myNotePatient->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $myNotePatient->updated_at }}</p>
</div>

