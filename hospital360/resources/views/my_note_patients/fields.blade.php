<!-- My Patient Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('my_patient_id', 'My Patient Id:') !!}
    {!! Form::text('my_patient_id', null, ['class' => 'form-control']) !!}
</div>

<!-- My Doctor Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('my_doctor_id', 'My Doctor Id:') !!}
    {!! Form::text('my_doctor_id', null, ['class' => 'form-control']) !!}
</div>

<!-- My Note Field -->
<div class="form-group col-sm-6">
    {!! Form::label('my_note', 'My Note:') !!}
    {!! Form::text('my_note', null, ['class' => 'form-control']) !!}
</div>

<!-- Created By Field -->
<div class="form-group col-sm-6">
    {!! Form::label('created_by', 'Created By:') !!}
    {!! Form::text('created_by', null, ['class' => 'form-control']) !!}
</div>

<!-- Updated By Field -->
<div class="form-group col-sm-6">
    {!! Form::label('updated_by', 'Updated By:') !!}
    {!! Form::text('updated_by', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('myNotePatients.index') }}" class="btn btn-secondary">Cancel</a>
</div>
