<!DOCTYPE html>
<html lang="en">
   <head>
      <title>Haspatal Admin Template</title>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <!-- CSS only -->
      <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
      <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900,900i&display=swap" rel="stylesheet">
      <script src="https://kit.fontawesome.com/fa5e54101c.js" crossorigin="anonymous"></script>
      <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
      <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
      <!-- Theme Style -->
      <!-- <link rel="stylesheet" href="css/style.css"> -->
      <style>
         .bg_red{
         background: #db4130;
         }
         .logo_container{
         height: 100vh;
         padding: 20px;
         display: flex;
         width: 100%;
         justify-content: center;
         align-items: center;
         }
         .logo_container img{
         display: block;
         width: 250px;
         }
         .reg_box{
         padding: 30px;
         display: flex;
         justify-content: center;
         align-items: center;
         height: 100vh;
         }
         .form_container{
         width: 60%;
         }
         .head_txt{
         text-align: center;
         margin-bottom: 40px;
         }
         .login_link{
         margin: 50px 0 10px 0;
         text-align: center;
         }
         .login_link a{
         text-decoration: none;
         }
         .form-group{
         margin-bottom: 15px;
         }
         .form-group label{
         margin-bottom: 5px;
         }
         @media(max-width: 768px){
         .logo_container{
         height: auto;
         padding: 50px 20px;
         }
         .logo_container img{
         width: 180px;
         }
         .reg_box{
         height: auto;
         padding: 50px 30px;
         }
         .form_container{
         width: 80%;
         }
         }
         header{
         background: #db4130;
         }
         .logo_head_container{
         padding: 20px 30px;
         }
         .logo_head_container img{
         display: block;
         width: 180px;
         }
         .top_bx_ct{
         display: flex;
         padding: 30px;
         justify-content: space-between;
         align-items: center;
         }
         .top_bx_ct h5{
         margin: 0;
         }
         .active{
         background: #db4130;
         color: #fff !important;
         }
         .tabl_container{
         padding: 0 30px;
         }
         .tbl_contr{
         padding: 30px;
         box-shadow: 0px 0px 20px rgba(92,111,139,0.12);
         border-radius: 10px;
         }
         th{
         font-size: 14px;
         color: #5a5a5a;
         background: #f5f5f5 !important;
         }
         td{
         font-size: 13px;
         padding: .3rem .5rem !important;
         }
         table{
         width: 100%;
         }
         .acpt{
         background: rgb(15, 153, 73);
         cursor: pointer;
         }
         .rjct{
         background: rgb(165, 19, 19);
         cursor: pointer;
         }
         .clr{
         background: rgb(111 111 236);
         cursor: pointer;
         }
         .awt{
         background: rgb(216 142 8);
         cursor: pointer;
         }
         .aprc_cl{
         display: block;
         padding: 0px 7px;
         border-radius: 5px;
         background: rgb(15, 153, 73);
         color: #fff;
         width: fit-content;
         font-size: 12px;
         }
         .rjct_cl{
         display: block;
         padding: 0px 7px;
         border-radius: 5px;
         background: rgb(165, 19, 19);
         color: #fff;
         width: fit-content;
         font-size: 12px;
         }
         .btn_spcl_btn{
         display: inline-block;
         font-size: 16px;
         color: #db4130;
         padding: 7px;
         width: 200px;
         border: 2px solid #db4130;
         box-shadow: 0 1px 15px 1px rgb(68 68 68 / 15%);
         border-radius: 30px;
         margin-top: 20px;
         }
         .btn_cont{
         text-align: center;
         }
         .dropdown-menu.show{
         left: -37px !important;
         }
         .btn-slct-clr{
         color: #db4130;
         background-color: #ffffff;
         border-color: #db4130;
         }
         .doc{
         text-decoration: none;
         color: #db4130;
         cursor: pointer;
         }
         .view{
         text-decoration: none;
         cursor: pointer;
         }
         .df_blk{
         display: block;
         }
         .flx-bx{
         display: flex;
         align-items: center;
         margin-bottom: 15px;
         }
         .fil_lbl{
         width: 230px;
         font-size: 14px;
         color: #656565;
         font-weight: 500;
         }
         .p_cls{
         width: 100%;
         padding: 10px 15px;
         background: #f5f5f5;
         border-radius: 5px;
         margin:0;
         font-size: 14px;
         }
         .btn_group_flx{
         margin-bottom: 20px;
         display: flex;
         justify-content: flex-end;
         }
         .btn_grp_fl_s{
         display: block;
         margin-right: 5px;
         padding: 5px 15px;
         text-decoration: none;
         color: #fff;
         font-size: 12px;
         border-radius: 5px;
         }
         .card {
         box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
         transition: 0.3s;
         width: 100%;
         }
         .card:hover {
         box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
         }
         .container {
         padding: 2px 16px;
         width:100%;
         }
      </style>
   </head>
   <body>
      <section id="wrx_total">
         <header>
            <div class="logo_head_container">
               <!-- <img src="images/Logo.png"> -->
               <img src="{{url('public/images/Logo.png')}}">
            </div>
         </header>
         <div class="card" style="padding: 10px 25px;">
            <h1 style="margin: 0; font-size: 20px;">Terms and Condition</h1>
         </div>
         <div class="card">
            <div class="container" style="padding: 20px 16px;">
               <p style="font-size:14px;">These terms and conditions, specific to Healthcare Associates who are registered at Haspatal 360 App of Healthcare Infosystems ("H360 Terms "), form a legally binding agreement between Healthcare Infosystems. ("We" or "Us" or Our or "HIS" or "Company"), having its registered office at 704B, Unitech Arcadia Towers, Sector 49, Gurugram, Haryana 122003
                  and You ("You" or "Your"), as a Healthcare Associates User of Our Website, Mobile and Web Applications, Systems and Services
               </p>
               </br>
               <p style='margin:0.25in 0in 0.25in 0.25in;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";'><strong><span style='font-size:13px;font-family:"Montserrat Medium";'>1. Definitions&nbsp;</span></strong></p>
               <p style='margin:0.25in 0in 0.25in 0.25in;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";'><span style='font-size:13px;font-family:"Montserrat Medium";'>As used in these Healthcare Associates Terms, the following terms shall have the meaning set forth below:</span></p>
               <p style='margin:0.25in 0in 0.25in 0.25in;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";'><span style='font-size:13px;font-family:"Montserrat Medium";'>(i) &quot;Account&quot; means the credit or debit balance maintained by You with the Website;</span></p>
               <p style='margin:0.25in 0in 0.25in 0.25in;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";'><span style='font-size:13px;font-family:"Montserrat Medium";'>(ii) &quot;Effective Date&quot; means the date on which You accept these Healthcare Associates Terms by clicking &apos;Sign Up&apos; or &quot;start my free trial&quot; or &quot;get started for free&quot; or &apos;I Accept&apos;;</span></p>
               <p style='margin:0.25in 0in 0.25in 0.25in;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";'><span style='font-size:13px;font-family:"Montserrat Medium";'>(iii) &quot;User Information&quot; means information regarding Registered Users which includes personal, Business, Legal,&nbsp;and medical information and any other information which may be provided by a Registered Users to You or maybe transferred to You using Haspatal App;</span></p>
               <p style='margin:0.25in 0in 0.25in 0.25in;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";'><span style='font-size:13px;font-family:"Montserrat Medium";'>(iv) &quot;Services&quot; means the services offered to You by HIS that involves the use of the System, which may include the orders management service, electronic medical records service, and other services as may be introduced by HIS from time to time;</span></p>
               <p style='margin:0.25in 0in 0.25in 0.25in;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";'><span style='font-size:13px;font-family:"Montserrat Medium";'>(v) &quot;Website&quot; means &nbsp;<a data-saferedirecturl="https://www.google.com/url?q=http://www.haspatal.com&source=gmail&ust=1610785820591000&usg=AFQjCNG8ju6Kd96JcSsX4jiS0LP7MH_scA" href="http://www.haspatal.com" target="_blank">www.haspatal.com</a></span></p>
               <p style='margin:0.25in 0in 0.25in 0.25in;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";'><span style='font-size:13px;font-family:"Montserrat Medium";'>(vi) &quot;System&quot; means the technology platform provided as part of the Website consisting of hardware and/or software used or provided by Us for the purpose of providing the Services to You;</span></p>
               <p style='margin:0.25in 0in 0.25in 0.25in;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";'><span style='font-size:13px;font-family:"Montserrat Medium";'>All other capitalized terms shall have the meaning ascribed to them in the Other Terms.</span></p>
               <p style='margin:0.25in 0in 0.25in 51pt;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";'><span style='font-size:13px;font-family:"Montserrat Medium";'>&nbsp;</span></p>
               <p style='margin:0.25in 0in 0.25in 0.25in;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";'><strong><span style='font-size:13px;font-family:"Montserrat Medium";'>2. Grant of Rights</span></strong></p>
               <p style='margin:0.25in 0in 0.25in 0.25in;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";'><span style='font-size:13px;font-family:"Montserrat Medium";'>(i) Subject to the terms of the Agreement, we grant to You and You accept a non-exclusive, personal, non-transferable, limited right to have access to and to use the System for the duration of Your engagement with Us.</span></p>
               <p style='margin:0.25in 0in 0.25in 0.25in;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";'><span style='font-size:13px;font-family:"Montserrat Medium";'>The aforementioned right does not extend to (a) use the System for time-sharing, rental, or service bureau purposes; (b) make the system, in whole or in part, available to any other person, entity, or business; (c) modify the contents of the Systems and the Website or use such content for any commercial purpose, or any public display, performance, sale, or rental other than envisaged in the Agreement; (c) copy, reverse engineer, decompile or disassemble the System or the Website, in whole or in part, or otherwise attempt to discover the source code to the software used in the System, or (d) modify the System or associated software or combine the System with any other software or services not provided or approved by Us.</span></p>
               <p style='margin:0.25in 0in 0.25in 0.25in;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";'><span style='font-size:13px;font-family:"Montserrat Medium";'>You will obtain no rights to the System except for the limited rights to use the System expressly granted by this Healthcare Associates Terms.</span></p>
               <p style='margin:0.25in 0in 0.25in 0.25in;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";'><span style='font-size:13px;font-family:"Montserrat Medium";'>(ii) The System/Website may links or references which direct you to third-party websites/applications/content or service providers, including advertisers and e-commerce websites (collectively referred to as &quot;<strong>Third Party Websites</strong>&quot;). Links to such Third Party Websites are provided for your convenience only. You are expected to exercise your own independent judgment and prudence when visiting/using any Third Party Websites via a link available on the System / Website. Should You decide to click on the links to visit such Third Party Website, You do so of Your own volition. Your usage of such Third Party Websites and all content available on such Third Party Websites are subject to the terms of use of the respective Third Party Website and HIS is not responsible for Your use of any Third Party Websites&nbsp;</span></p>
               <p style='margin:0.25in 0in 0.25in 0.25in;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";'><strong><span style='font-size:13px;font-family:"Montserrat Medium";'>
                  3. By accepting these&nbsp;</span></strong><strong><span style='font-size:13px;font-family:"Montserrat Medium";'>Healthcare Associates&nbsp;Terms, You agree that:</span></strong>
               </p>
               <p style='margin:0.25in 0in 0.25in 0.25in;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";'><span style='font-size:13px;font-family:"Montserrat Medium";'>(i) You will acquire, install, configure and maintain all hardware, software and communications systems necessary on your end to access the System (&quot;<strong>Implementation</strong>&quot;) and receive the Services. To the extent possible, such an assessment should be done before making advance payment for the Service. Your Implementation will comply with the specifications from time to time established by Us. You will ensure that Your Implementation is compatible with the System and Services. If We notify You that Your Implementation is incompatible with the System and/or Services, You will rectify such incompatibility, and We will have the right to suspend Services to You until such rectification has been implemented. Under no circumstances will You are eligible for any refund, compensatory extension of use, or any financial assistance in relation to Implementation whatsoever</span></p>
               <p style='margin:0.25in 0in 0.25in 0.25in;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";'><strong><span style='font-size:13px;font-family:"Montserrat Medium";'>4.&nbsp;</span></strong><span style='font-size:13px;font-family:"Montserrat Medium";'>Engagement of&nbsp;</span><span style='font-size:13px;font-family:"Montserrat Medium";'>Healthcare Associates&rsquo; &nbsp;by<strong>&nbsp;HIS</strong></span></p>
               <p style='margin:0.25in 0in 0.25in 0.25in;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";'><span style='font-size:13px;font-family:"Montserrat Medium";'>(i) In certain cases, You and HIS may agree that You will commit to providing information and responses on the Website for a specific period of time (such as a specific number of hours per day/week/ month). In such a case, while all the terms of the Agreement will continue to apply to You, there may be some additional terms that will apply to You which will be agreed between You and HIS.</span></p>
               <p style='margin:0.25in 0in 0.25in 0.25in;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";'><strong><span style='font-size:13px;font-family:"Montserrat Medium";'>5. Access to the System and Use of Services</span></strong></p>
               <p style='margin:0.25in 0in 0.25in 0.25in;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";'><span style='font-size:13px;font-family:"Montserrat Medium";'>(i) Verification. You agree that Your receipt of Services is subject to verification by Us of Your identity and credentials as a health care associate such as a pharmacy store, diagnostic lab, therapy center, counseling center, home care services, and imaging center as submitted by you for registration of your business.&nbsp;</span></p>
               <p style='margin:0.25in 0in 0.25in 0.25in;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";'><span style='font-size:13px;font-family:"Montserrat Medium";'>As part of the registration process and at any time thereafter, You may be required to provide Us with various information such as your business establishment certificates issued by appropriate government agencies, departments or by respective councils in order to prove that You are a valid healthcare Associate in the field that You claim (&quot;<strong>Credential Information</strong>&quot;).&nbsp;</span></p>
               <p style='margin:0.25in 0in 0.25in 0.25in;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";'><span style='font-size:13px;font-family:"Montserrat Medium";'>We may verify such Credential Information or may ask You for additional information. We may also make inquiries from third parties to verify the authenticity of Your Credential Information. You hereby authorize Us to make such inquiries from such third parties, and You agree to hold them and Us harmless from any claim or liability arising from the request for or disclosure of such information.&nbsp;</span></p>
               <p style='margin:0.25in 0in 0.25in 0.25in;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";'><span style='font-size:13px;font-family:"Montserrat Medium";'>You agree that We may terminate Your access to or use of the System and Services at any time if We are unable at any time to determine or verify Your Credential Information. We reserve to right to carry out re-verification of Credential Information from time to time or as and when required, and the above rights and commitments will extend to re-verification as well.&nbsp;</span></p>
               <p style='margin:0.25in 0in 0.25in 0.25in;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";'><span style='font-size:13px;font-family:"Montserrat Medium";'>(ii) Safeguards.&nbsp;</span></p>
               <p style='margin:0.25in 0in 0.25in 0.25in;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";'><span style='font-size:13px;font-family:"Montserrat Medium";'>a) You will implement and maintain appropriate administrative, physical, and technical safeguards to protect the System from access, use or alteration; and You will always use the User ID assigned to You.</span></p>
               <p style='margin:0.25in 0in 0.25in 0.25in;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";'><span style='font-size:13px;font-family:"Montserrat Medium";'>b) You will immediately notify Us of any breach or suspected breach of the security of the System of which You become aware, or any unauthorized use or disclosure of information within or obtained from the System and You will take such action to mitigate the breach or suspected breach as We may direct, and will cooperate with Us in investigating and mitigating such breach.</span></p>
               <p style='margin:0.25in 0in 0.25in 0.25in;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";'><span style='font-size:13px;font-family:"Montserrat Medium";'>(iii) No Third-Party Access.&nbsp;You will not permit any third party to have access to the System or to use the System or the Services without Our prior written consent. You will not allow any third party to access the System or provide information to Registered Users/ Non-Registered Users on the Website. You will promptly notify Us of any order or demand for compulsory disclosure of health information if the disclosure requires access to or use of the System or Services.</span></p>
               <p style='margin:0.25in 0in 0.25in 0.25in;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";'><strong><span style='font-size:13px;font-family:"Montserrat Medium";'>6. Compliance</span></strong></p>
               <p style='margin:0.25in 0in 0.25in 0.25in;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";'><span style='font-size:13px;font-family:"Montserrat Medium";'>(i) You are solely responsible for ensuring that Your use of the System and the Services complies with applicable law. You will also ensure that Your use of the System, the Website, and the Services is always in accordance with the terms of the Agreement. You will not undertake or permit any unlawful use of the System or Services, or take any action that would render the operation or use of the System or Services by us.&nbsp;</span></p>
               <p style='margin:0.25in 0in 0.25in 0.25in;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";'><span style='font-size:13px;font-family:"Montserrat Medium";'>(ii) Without limiting the generality of the foregoing, You represent that You shall not use the System in violation of any applicable laws including prevalent Code of Medical and business Ethics Regulations or any other code of conduct governed by your council. Notwithstanding the generality of the foregoing, You shall not use the System to</span></p>
               <p style='margin:0.25in 0in 0.25in 0.25in;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";'><span style='font-size:13px;font-family:"Montserrat Medium";'>a) Interact with Registered Users at the time of medical emergencies.</span></p>
               <p style='margin:0.25in 0in 0.25in 0.25in;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";'><span style='font-size:13px;font-family:"Montserrat Medium";'>b) &nbsp;Interact with a Registered Users/ doctors/hospitals seeking any sort of appointments which constitute Haspatal business.</span></p>
               <p style='margin:0.25in 0in 0.25in 0.25in;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";'><span style='font-size:13px;font-family:"Montserrat Medium";'>c) Discriminate in any way between appointments or orders booked in the ordinary course and booked through HIS.</span></p>
               <p style='margin:0.25in 0in 0.25in 0.25in;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";'><span style='font-size:13px;font-family:"Montserrat Medium";'>d) Boast of orders, deliveries, processes, or facilities through System, Services, or Website.</span></p>
               <p style='margin:0.25in 0in 0.25in 0.25in;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";'><span style='font-size:13px;font-family:"Montserrat Medium";'>e) Directly or indirectly solicit Registered Users for orders.</span></p>
               <p style='margin:0.25in 0in 0.25in 0.25in;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";'><span style='font-size:13px;font-family:"Montserrat Medium";'>f) Claim to be a discount seller, wholesaler, distributor providing extra services, through System, Services or Website, unless You have valid evidence to share that you actually can provide those services.</span></p>
               <p style='margin:0.25in 0in 0.25in 0.25in;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";'><span style='font-size:13px;font-family:"Montserrat Medium";'>(iii) You shall keep Your Credential Information updated and will inform Us immediately should any portion of Your Credential Information be revoked, is canceled, or expires.</span></p>
               <p style='margin:0.25in 0in 0.25in 0.25in;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";'><strong><span style='font-size:13px;font-family:"Montserrat Medium";'>7. User information&nbsp;</span></strong></p>
               <p style='margin:0.25in 0in 0.25in 0.25in;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";'><span style='font-size:13px;font-family:"Montserrat Medium";'>(i) You hereby acknowledge that You may get access to User Information including identifiable health-related information.</span></p>
               <p style='margin:0.25in 0in 0.25in 0.25in;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";'><span style='font-size:13px;font-family:"Montserrat Medium";'>(ii) You represent and warrant that You will, at all times during the use of the Services and thereafter, comply with all laws directly or indirectly applicable to You that may now or hereafter govern the collection, use, transmission, processing, receipt, reporting, disclosure, maintenance, and storage of User Information, including but not limited to the Information Technology Act, 2000 and The Information Technology (Reasonable Security Practices and Procedures and Sensitive Personal Data or Information) Rules, 2011 made thereunder.,</span></p>
               <p style='margin:0.25in 0in 0.25in 0.25in;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";'><span style='font-size:13px;font-family:"Montserrat Medium";'>(iii) Notwithstanding the generality of the aforementioned provision</span></p>
               <p style='margin:0.25in 0in 0.25in 0.25in;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";'><span style='font-size:13px;font-family:"Montserrat Medium";'>(a) You acknowledge that You have read, understood, and agree to comply with HIS&apos;s Privacy Policy available at&nbsp;</span><a data-saferedirecturl="https://www.google.com/url?q=http://www.haspatal.com/privacy&source=gmail&ust=1610785820592000&usg=AFQjCNFa20TJ1vn-562xdzgE_nSM8RE-Ig" href="http://www.haspatal.com/privacy" style="color:blue;text-decoration:underline;" target="_blank"><span style='font-size:13px;font-family:"Montserrat Medium";'>www.haspatal.com/privacy</span></a><span style='font-size:13px;font-family:"Montserrat Medium";'>&nbsp;when dealing with User Information.&nbsp;</span></p>
               <p style='margin:0.25in 0in 0.25in 0.25in;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";'><span style='font-size:13px;font-family:"Montserrat Medium";'>(b) You represent and warrant that You will not use the User Information of Registered Users and Non-Registered Users for any other purpose than for providing information to such Registered Users and Non-Registered Users and /or fixing appointments with the Registered Users.</span></p>
               <p style='margin:0.25in 0in 0.25in 0.25in;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";'><span style='font-size:13px;font-family:"Montserrat Medium";'>&nbsp;</span></p>
               <p style='margin:0.25in 0in 0.25in 0.25in;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";'><strong><span style='font-size:13px;font-family:"Montserrat Medium";'>8. Cooperation&nbsp;</span></strong></p>
               <p style='margin:0.25in 0in 0.25in 0.25in;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";'><span style='font-size:13px;font-family:"Montserrat Medium";'>(i) You will cooperate with Us in the administration of The system, including providing reasonable assistance in evaluating the System and collecting and reporting data requested by Us for the purposes of administering the System.</span></p>
               <p style='margin:0.25in 0in 0.25in 0.25in;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";'><span style='font-size:13px;font-family:"Montserrat Medium";'>(ii) We may provide Your reference to other potential users of the system as a referral to Our Services. In case You would not like to be contacted by potential users, You can send Us an email at unsubscribe<u><span style="color:blue;">@<a data-saferedirecturl="https://www.google.com/url?q=http://haspatal.com&source=gmail&ust=1610785820592000&usg=AFQjCNGIRvp2vQmgire2D7h3Vzlot5zfHw" href="http://haspatal.com" target="_blank">haspatal.com</a></span></u> regarding the same. We shall cease providing Your reference to potential users within 48 hours of receipt of such written request. &nbsp;</span></p>
               <p style='margin:0.25in 0in 0.25in 0.25in;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";'><strong><span style='font-size:13px;font-family:"Montserrat Medium";'>9. Providing Healthcare Associate Data&nbsp;</span></strong></p>
               <p style='margin:0.25in 0in 0.25in 0.25in;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";'><span style='font-size:13px;font-family:"Montserrat Medium";'>You agree that System may provide its users your business information and contact information and reviews by your other customers about your services. You also agree that you have not hidden any information required by the registered users in determining to place an order/ request for an appointment to you.</span></p>
               <p style='margin:0.25in 0in 0.25in 0.25in;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";'><strong><span style='font-size:13px;font-family:"Montserrat Medium";'>10. Intellectual Property Rights</span></strong></p>
               <p style='margin:0.25in 0in 0.25in 0.25in;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";'><span style='font-size:13px;font-family:"Montserrat Medium";'>All intellectual property rights in and title to the System, the present or future modifications/upgradations thereof and standard enhancements thereto shall remain the property of HIS and its licensors. This Healthcare Associates Terms or the Agreement do not and shall not transfer any ownership or proprietary interest in the System from HIS to You, except as may be otherwise expressly provided in these Healthcare Associates Terms or as may be agreed to by and between HIS and You.&nbsp;</span></p>
               <p style='margin:0.25in 0in 0.25in 0.25in;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";'><strong><span style='font-size:13px;font-family:"Montserrat Medium";'>11. Fees and Charges</span></strong></p>
               <p style='margin:0.25in 0in 0.25in 0.25in;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";'><span style='font-size:13px;font-family:"Montserrat Medium";'>(i) You will be obligated to pay Us Our standard service fee (the &quot;<strong>Service Fee</strong>&quot;) for the Services to which You have access Your subscription to the System and Our Services. You also agree to pay, Our then-current rates, for all goods or services that You request from Us and that are not included in Our standard services (&quot;<strong>Miscellaneous Charges</strong>&quot;). We will notify You of the Service Fee and Miscellaneous Charges when You are granted access to a service, and We will notify You of the applicable Miscellaneous Charges before providing services to which a Miscellaneous Charge will apply.&nbsp;</span></p>
               <p style='margin:0.25in 0in 0.25in 0.25in;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";'><span style='font-size:13px;font-family:"Montserrat Medium";'>(ii) You understand that HIS provides various Services with respect to the System. While some of these services are provided for free, there are certain Services for which costs will be incurred by You if You decide to avail such Services (&quot;<strong>Paid Services</strong>&quot;).&nbsp;</span></p>
               <p style='margin:0.25in 0in 0.25in 0.25in;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";'><span style='font-size:13px;font-family:"Montserrat Medium";'>For more information on HIS&apos;s Service Fees with respect to Paid Services including modes of payment, please check <a data-saferedirecturl="https://www.google.com/url?q=http://doctors.haspatal.com&source=gmail&ust=1610785820592000&usg=AFQjCNEDAuMutfyfcGR8yrTNrklUOVYUSQ" href="http://doctors.haspatal.com" target="_blank">doctors.haspatal.com</a>. &nbsp;HIS shall have the right to change the Service Fees for the Paid Services at any time, with or without notice to You. Your continued use of the System and the Website shall be deemed to be Your acceptance of such changes.&nbsp;</span></p>
               <p style='margin:0.25in 0in 0.25in 0.25in;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";'><span style='font-size:13px;font-family:"Montserrat Medium";'>(iii) You understand and acknowledge that We follow a prepay payment model for Our Services. Accordingly, Your access to the Services can be limited or suspended, or terminated in case You are unable to maintain any credit balance in Your Account which you may have agreed to at the time of subscription (&quot;<strong>Non-Payment State</strong>&quot;). HIS reserves the right to charge additional charges (&quot;<strong>Activation Fees</strong>&quot;) in case Your Services need to be restored after being suspended for non-payment.&nbsp;</span></p>
               <p style='margin:0.25in 0in 0.25in 0.25in;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";'><strong><span style='font-size:13px;font-family:"Montserrat Medium";'>(iv)</span></strong><span style='font-size:13px;font-family:"Montserrat Medium";'>&nbsp;All charges and fees shall be exclusive of all state, municipal, or other government excise, sales, service, use, occupational, or like taxes now in force. We have a right to pass on any additional taxes or other state levies to You and You agree to pay any tax (excluding taxes on Our net income) that We may be required to collect or pay now or at any time in the future and that is imposed upon the sale or delivery of items and Services purchased under these Healthcare Associate Terms.</span></p>
               <p style='margin:0.25in 0in 0.25in 0.25in;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";'><strong><span style='font-size:13px;font-family:"Montserrat Medium";'>(v)</span></strong><span style='font-size:13px;font-family:"Montserrat Medium";'>&nbsp;You are responsible for any charges You incur to use the System, such as telephone and equipment charges, and fees charged by third-party vendors of products and services.&nbsp;</span></p>
               <p style='margin:0.25in 0in 0.25in 0.25in;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";'><strong><span style='font-size:13px;font-family:"Montserrat Medium";'>(f)</span></strong><span style='font-size:13px;font-family:"Montserrat Medium";'>&nbsp;We reserve to change the terms applicable to the fee at any point in time by giving You fifteen (15) days&apos; prior intimation via email. The revised fee terms shall be automatically applicable to You after the fifteen (15) days&apos; notice period. You agree that no separate confirmation or approval is required from You to amend the terms with regard to the fee.</span></p>
               <p style='margin:0.25in 0in 0.25in 0.25in;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";'><strong><span style='font-size:13px;font-family:"Montserrat Medium";'>(vi)</span></strong><span style='font-size:13px;font-family:"Montserrat Medium";'>&nbsp;We do not have any refund policy for subscriptions. In case you have opted for our subscription plans and wish to discontinue our services, You can stop payment of the Service Fees at any point in time to discontinue using Our Service. At such point, Your subscription shall last till the end of Your subscription period and shall then be moved to Non-Payment State. Provisions of Section 11(iii) above shall apply for such accounts.&nbsp;</span></p>
               <p style='margin:0.25in 0in 0.25in 0.25in;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";'><strong><span style='font-size:13px;font-family:"Montserrat Medium";'>(vii)</span></strong><span style='font-size:13px;font-family:"Montserrat Medium";'>&nbsp;You will be required to yourself collect the payments for your services and products as sold to registered users. At no point in time, HIS shall be considered/ assumed / perceived /or determined by you as responsible for the failure of payments of orders, appointments, or services provided by You to our registered users.&nbsp;</span></p>
               <p style='margin:0.25in 0in 0.25in 0.25in;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";'><strong><span style='font-size:13px;font-family:"Montserrat Medium";'>(viii)</span></strong><span style='font-size:13px;font-family:"Montserrat Medium";'>&nbsp;Whenever a registered user places an order for you, our service charges shall be automatically deducted from your wallet balance. In case your wallet balance is not sufficient for deducting our service fees for a single order, the display of your business information to registered users will be stopped automatically and it will be resumed only after you add funds to your wallet balance.</span></p>
               <p style='margin:0.25in 0in 0.25in 0.25in;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";'><span style='font-size:13px;font-family:"Montserrat Medium";'>a. You will deliver and complete all the orders as placed by our registered users and accepted by you within one working day. We will keep reviewing your compliance to this delivery time and as per the company policy, your business info as visible to&nbsp;</span></p>
               <p style='margin:0.25in 0in 0.25in 0.25in;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";'><span style='font-size:13px;font-family:"Montserrat Medium";'>b. You may at times request for an extension of the delivery period, however, it is mutually agreed and well understood that such an extension can not supersede/override the right of the user to cancel the order at any time.&nbsp;</span></p>
               <p style='margin:0.25in 0in 0.25in 0.25in;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";'><span style='font-size:13px;font-family:"Montserrat Medium";'>c. You will not charge any amount extra from our registered users, for any services that you selected as your standard or offered services. For other services, you will be required to inform our registered users of any/ all extra charges and shall supply the ordered items/medicine, etc only after their consent.&nbsp;</span></p>
               <p style='margin:0.25in 0in 0.25in 0.25in;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";'><span style='font-size:13px;font-family:"Montserrat Medium";'>(ix) You hereby grant Us wrote consent to collect, store, handle and use Your financial information such as bank account or credit card or debit card, or other payment instrument details for the purpose of paying for Services and access to System in accordance with Our Privacy Policy.</span></p>
               <p style='margin:0.25in 0in 0.25in 0.25in;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";'><span style='font-size:13px;font-family:"Montserrat Medium";'>(x) In case, the registered users send us a complaint regarding the poor quality of services or products and we are of the opinion that the supplied products and services provided by you to Registered User against the charged amount are unsatisfactory or deficient in some way, then we reserve the right to ask you to, (i) replace the products with the certified quality products at no extra costs or (ii) refund the amount paid by our registered user for the services/products. (iii) To discuss with the complaining user and send us his satisfaction note within 3 days. You hereby agree that our decision in this regard shall be final and binding and also that you understand that non-compliance with our instructions may result in the suspension of your business account with us irrespective of compliance with any other terms whatsoever.&nbsp;</span></p>
               <p style='margin:0.25in 0in 0.25in 0.25in;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";'><strong><span style='font-size:13px;font-family:"Montserrat Medium";'>12. Confidential Information</span></strong></p>
               <p style='margin:0.25in 0in 0.25in 0.25in;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";'><span style='font-size:13px;font-family:"Montserrat Medium";'>(a) You will treat all information received from Us as confidential. You may not disclose Our confidential information to any other person and You may not use any confidential information except as provided herein. Except as otherwise provided in H360 Terms and Other Terms, You may not, without Our prior written consent, at any time, during, or after the applicability of these Terms, directly or indirectly, divulge or disclose confidential information for any purpose or use confidential information for Your own benefit or for the purposes or benefit of any other person. You agree to hold all confidential information in strict confidence and to take all measures necessary to prevent unauthorized copying, use, or disclosure of confidential information, and to keep the confidential information from being disclosed into the public domain or into the possession of persons not bound to maintain confidentiality. You will disclose confidential information only to your employees, agents, or contractors who have a need to use it for the purposes permitted under the Terms and Other Terms only. You will inform all such recipients of the confidential nature of confidential information and will instruct them to deal with confidential information in accordance with these Terms. You will promptly notify Us in writing of any improper disclosure, misappropriation, or misuse of the confidential information by any person, which may come to Your attention.</span></p>
               <p style='margin:0.25in 0in 0.25in 0.25in;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";'><span style='font-size:13px;font-family:"Montserrat Medium";'>(b) You agree that We will suffer irreparable harm if You fail to comply with the obligations set forth in this Section 12, and You further, agree that monetary damages will be inadequate to compensate Us for any such breach. Accordingly, You agree that We will, in addition to any other remedies available to Us at law or in equity, be entitled to seek injunctive relief to enforce the provisions hereof, immediately and without the necessity of posting a bond.&nbsp;</span></p>
               <p style='margin:0.25in 0in 0.25in 0.25in;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";'><span style='font-size:13px;font-family:"Montserrat Medium";'>(c) This Section 12 will survive the termination or expiration of these Terms of Agreement for any reason.&nbsp;</span></p>
               <p style='margin:0.25in 0in 0.25in 0.25in;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";'><strong><span style='font-size:13px;font-family:"Montserrat Medium";'>13. Disclaimer and Exclusion of Warranties</span></strong></p>
               <p style='margin:0.25in 0in 0.25in 0.25in;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";'><span style='font-size:13px;font-family:"Montserrat Medium";'>(A) YOU ACKNOWLEDGE THAT ACCESS TO THE SYSTEM WILL BE PROVIDED OVER VARIOUS FACILITIES AND COMMUNICATION LINES, AND INFORMATION WILL BE TRANSMITTED OVER LOCAL EXCHANGE AND INTERNET BACKBONE CARRIER LINES AND THROUGH ROUTERS, SWITCHES, AND OTHER DEVICES (COLLECTIVELY, &quot;<strong>CARRIER LINES</strong>&quot;) OWNED, MAINTAINED, AND SERVICED BY THIRD-PARTY CARRIERS, UTILITIES, AND INTERNET SERVICE PROVIDERS, ALL OF WHICH ARE BEYOND OUR CONTROL. WE ASSUME NO LIABILITY FOR OR RELATING TO THE INTEGRITY, PRIVACY, SECURITY, CONFIDENTIALITY OR USE OF ANY INFORMATION WHILE IT IS TRANSMITTED ON THE CARRIER LINES, OR ANY DELAY, FAILURE, INTERRUPTION, INTERCEPTION, LOSS, TRANSMISSION, OR CORRUPTION OF ANY DATA OR OTHER INFORMATION ATTRIBUTABLE TO TRANSMISSION ON THE CARRIER LINES. USE OF THE CARRIER LINES IS SOLELY AT YOUR RISK AND IS SUBJECT TO ALL APPLICABLE LOCAL, STATE, NATIONAL, AND INTERNATIONAL LAWS. &nbsp;</span></p>
               <p style='margin:0.25in 0in 0.25in 0.25in;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";'><span style='font-size:13px;font-family:"Montserrat Medium";'>(B) THE SERVICES, THE WEBSITE THE SYSTEM, ACCESS TO THE SYSTEM AND THE INFORMATION CONTAINED ON THE SYSTEM IS PROVIDED &quot;AS IS&quot; AND &quot;AS AVAILABLE&quot; BASIS WITHOUT ANY WARRANTY OF ANY KIND, EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT. YOU ARE SOLELY RESPONSIBLE FOR ANY AND ALL ACTS OR OMISSIONS TAKEN OR MADE IN RELIANCE ON THE SYSTEM OR THE INFORMATION IN THE SYSTEM, INCLUDING INACCURATE OR INCOMPLETE INFORMATION. IT IS EXPRESSLY AGREED THAT IN NO EVENT SHALL WE BE LIABLE FOR ANY SPECIAL, INDIRECT, CONSEQUENTIAL, REMOTE OR EXEMPLARY DAMAGES, INCLUDING BUT NOT LIMITED TO, LOSS OF PROFITS OR REVENUES, LOSS OF USE, OR LOSS OF INFORMATION OR DATA, WHETHER A CLAIM FOR ANY SUCH LIABILITY OR DAMAGES IS PREMISED UPON BREACH OF CONTRACT, BREACH OF WARRANTY, NEGLIGENCE, STRICT LIABILITY, OR ANY OTHER THEORY OF LIABILITY, EVEN IF WE HAVE BEEN APPRISED OF THE POSSIBILITY OR LIKELIHOOD OF SUCH DAMAGES OCCURRING. WE DISCLAIM ANY AND ALL LIABILITY FOR ERRONEOUS TRANSMISSIONS AND LOSS OF SERVICE RESULTING FROM COMMUNICATION FAILURES BY TELECOMMUNICATION SERVICE PROVIDERS OR THE SYSTEM.&nbsp;</span></p>
               <p style='margin:0.25in 0in 0.25in 0.25in;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";'><span style='font-size:13px;font-family:"Montserrat Medium";'>(C) YOU ACKNOWLEDGE THAT OTHER USERS HAVE ACCESS TO THE SYSTEM AND ARE RECEIVING OUR SERVICES. SUCH OTHER USERS HAVE COMMITTED TO COMPLY WITH THESE TERMS &amp; CONDITIONS AND OUR POLICIES AND PROCEDURES CONCERNING USE OF THE SYSTEM; HOWEVER, THE ACTIONS OF SUCH OTHER USERS ARE BEYOND OUR CONTROL. ACCORDINGLY, WE DO NOT ASSUME ANY LIABILITY FOR OR RELATING TO ANY IMPAIRMENT OF THE PRIVACY, SECURITY, CONFIDENTIALITY, INTEGRITY, AVAILABILITY, OR RESTRICTED USE OF ANY INFORMATION ON THE SYSTEM RESULTING FROM ANY USERS&apos; ACTIONS OR FAILURES TO ACT.&nbsp;</span></p>
               <p style='margin:0.25in 0in 0.25in 0.25in;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";'><span style='font-size:13px;font-family:"Montserrat Medium";'>(D) WE ARE NOT RESPONSIBLE FOR UNAUTHORIZED ACCESS TO YOUR, DATA, FACILITIES OR EQUIPMENT BY INDIVIDUALS OR ENTITIES USING THE SYSTEM OR FOR UNAUTHORIZED ACCESS TO, ALTERATION, THEFT. CORRUPTION, LOSS, OR DESTRUCTION OF YOUR, DATA FILES, PROGRAMS, PROCEDURES, OR INFORMATION THROUGH THE SYSTEM, WHETHER BY ACCIDENT, FRAUDULENT MEANS OR DEVICES, OR ANY OTHER MEANS. YOU ARE SOLELY RESPONSIBLE FOR VALIDATING THE ACCURACY OF ALL OUTPUT AND REPORTS, AND FOR PROTECTING YOUR DATA AND PROGRAMS FROM LOSS BY IMPLEMENTING APPROPRIATE SECURITY MEASURES, INCLUDING ROUTINE BACKUP PROCEDURES. YOU HEREBY WAIVE ANY DAMAGES OCCASIONED BY LOST OR CORRUPT DATA, INCORRECT REPORTS, OR INCORRECT DATA FILES RESULTING FROM PROGRAMMING ERROR, OPERATOR ERROR, EQUIPMENT OR SOFTWARE MALFUNCTION, SECURITY VIOLATIONS, OR THE USE OF THIRD-PARTY SOFTWARE. WE ARE NOT RESPONSIBLE FOR THE CONTENT OF ANY INFORMATION TRANSMITTED OR RECEIVED THROUGH OUR PROVISION OF THE SERVICES.&nbsp;</span></p>
               <p style='margin:0.25in 0in 0.25in 0.25in;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";'><span style='font-size:13px;font-family:"Montserrat Medium";'>(E) WE EXPRESSLY DISCLAIM ANY LIABILITY FOR THE CONSEQUENCES TO YOU ARISING BECAUSE OF YOUR USE OF THE SYSTEM OR THE SERVICES.</span></p>
               <p style='margin:0.25in 0in 0.25in 0.25in;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";'><span style='font-size:13px;font-family:"Montserrat Medium";'>(F) WE DO NOT WARRANT THAT YOUR USE OF THE SYSTEM AND THE SERVICES UNDER THESE TERMS WILL NOT VIOLATE ANY LAW OR REGULATION APPLICABLE TO YOU.</span></p>
               <p style='margin:0.25in 0in 0.25in 0.25in;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";'><strong><span style='font-size:13px;font-family:"Montserrat Medium";'>14. Limitation Of Liability</span></strong></p>
               <p style='margin:0.25in 0in 0.25in 0.25in;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";'><span style='font-size:13px;font-family:"Montserrat Medium";'>NOTWITHSTANDING THE OTHER TERMS OF THESE TERMS, IN THE EVENT HIS SHOULD HAVE ANY LIABILITY TO YOU OR ANY THIRD PARTY FOR ANY LOSS, HARM OR DAMAGE, YOU AND HIS AGREE THAT SUCH LIABILITY SHALL UNDER NO CIRCUMSTANCES EXCEED THE VALUE OF ANY FEES RECEIVED BY HIS FROM YOU IN THE PRECEDING TWELVE MONTHS OR INR 5000 WHICHEVER IS LOWER. YOU AND HIS AGREE THAT THE FOREGOING LIMITATION OF LIABILITY IS AN AGREED ALLOCATION OF RISK BETWEEN YOU AND HIS. YOU ACKNOWLEDGE THAT WITHOUT YOUR ASSENT TO THIS SECTION 14, HIS WOULD NOT PROVIDE ACCESS TO THE SYSTEM, TO YOU.&nbsp;</span></p>
               <p style='margin:0.25in 0in 0.25in 0.25in;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";'><strong><span style='font-size:13px;font-family:"Montserrat Medium";'>15. Indemnification&nbsp;</span></strong></p>
               <p style='margin:0.25in 0in 0.25in 0.25in;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";'><span style='font-size:13px;font-family:"Montserrat Medium";'>You agree to indemnify, defend, and hold harmless HIS, Our and their affiliates, officers, directors, and agents, from and against any claim, cost, or liability, including reasonable attorneys&apos; fees, arising out of: (a) the use of the Services; (b) any breach by You of any representations, warranties or agreements contained in these Terms ; (c) the actions of any person gaining access to the System under a User ID assigned to You; (d) the actions of anyone using a User ID, password, or other unique identifier assigned You that adversely affects the System or any information accessed through the System;</span></p>
               <p style='margin:0.25in 0in 0.25in 0.25in;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";'><strong><span style='font-size:13px;font-family:"Montserrat Medium";'>16. Termination; Modification; Suspension; Termination</span></strong></p>
               <p style='margin:0.25in 0in 0.25in 0.25in;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";'><span style='font-size:13px;font-family:"Montserrat Medium";'>(a) We or You may terminate our Services at any time without cause upon thirty (30) days prior written notice to You.</span></p>
               <p style='margin:0.25in 0in 0.25in 0.25in;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";'><span style='font-size:13px;font-family:"Montserrat Medium";'>(b) We may update or change the Services and/or the H360 Terms and/ or the Service Fee set forth in these H360 Terms from time to time and recommend that You review these H360 Terms on a regular basis. You understand and agree that Your continued use of the Services after the H360 Terms have been updated or changed constitutes Your acceptance of the revised H360 Terms. Without limiting the foregoing, if We make a change to these H360 Terms that materially affects Your use of the Services, We may post a notice on the Website or notify You via email of any such change.</span></p>
               <p style='margin:0.25in 0in 0.25in 0.25in;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";'><span style='font-size:13px;font-family:"Montserrat Medium";'>(c) Termination, Suspension, or Amendment as a result of applicable laws - Notwithstanding anything to the contrary in these H360 Terms , We have the right, on providing notice to You, immediately to terminate, suspend, or amend the provision of the Services without liability: (a) to comply with any order issued or proposed to be issued by any governmental agency; (b) to comply with any provision of law, any standard of participation in any reimbursement program or any accreditation standard; or (c) if performance of any term of these H360 Terms by either Party would cause it to be in violation of the law.&nbsp;</span></p>
               <p style='margin:0.25in 0in 0.25in 0.25in;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";'><span style='font-size:13px;font-family:"Montserrat Medium";'>(d) We may terminate the provision of Services to You through the System immediately upon notice to You: (i) if You are named as a defendant in a criminal proceeding for a violation of federal or state law; (ii) if a finding or stipulation is made or entered into that You have violated any standard or requirement of federal or state law relating to the privacy or security of health information is made in any administrative or civil proceeding; or (iii) You cease to be qualified to provide services as a health care professional, or We are unable to verify Your qualifications as notified to Us under these H360 Terms.</span></p>
               <p style='margin:0.25in 0in 0.25in 0.25in;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";'><span style='font-size:13px;font-family:"Montserrat Medium";'>(e) We may suspend Your Services immediately pending Your cure of any breach of these H360 Terms, or in the event We determine in Our sole discretion that accesses to or use of the System by You may jeopardize the System or the confidentiality, privacy, security, integrity, or availability of information within the System, or that You have violated or may violate this H360 Terms or Other Terms, or has jeopardized or may jeopardize the rights of any third party, or that any person is or may be making unauthorized use of the A system with any User ID assigned to You. Our election to suspend the Services shall not waive or affect Our rights to terminate these H360 Terms as applicable to You as permitted under these H360 Terms.&nbsp;</span></p>
               <p style='margin:0.25in 0in 0.25in 0.25in;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";'><span style='font-size:13px;font-family:"Montserrat Medium";'>(f) Upon termination, You will cease to use the System and We will terminate Your access to the System. Upon termination for any reason, You will remove all software provided under H360 Terms from Your computer systems, You will cease to have access to the System, and You will return to Us all hardware, software, and documentation provided by or on behalf of Us.</span></p>
            </div>
         </div>
      </section>
   </body>
</html>
