<div class="row">
<!-- B Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('b_name','Business Name:') !!}
    {!! Form::text('b_name', null, ['class' => 'form-control','data-parsley-pattern="^[a-zA-Z]+$"','data-parsley-required']) !!}
</div>

<!-- B Desc Field -->
<div class="form-group col-sm-6">
    {!! Form::label('b_desc', 'Description:') !!}
    {!! Form::text('b_desc', null, ['class' => 'form-control','data-parsley-type="alphanum"','data-parsley-required']) !!}
</div>
</div>
<div class="row">
<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('businessTypes.index') }}" class="btn btn-secondary">Cancel</a>
</div>
</div>
