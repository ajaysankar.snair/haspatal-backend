<!-- Home Care Service Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('home_care_service_name', 'Home Care Service Name:') !!}
    {!! Form::text('home_care_service_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Created By Field -->
<div class="form-group col-sm-6">
    {!! Form::label('created_by', 'Created By:') !!}
    {!! Form::text('created_by', null, ['class' => 'form-control']) !!}
</div>

<!-- Updated By Field -->
<div class="form-group col-sm-6">
    {!! Form::label('updated_by', 'Updated By:') !!}
    {!! Form::text('updated_by', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('homeCareLists.index') }}" class="btn btn-secondary">Cancel</a>
</div>
