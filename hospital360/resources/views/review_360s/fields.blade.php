<!-- Patient Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('patient_id', 'Patient Id:') !!}
    {!! Form::text('patient_id', null, ['class' => 'form-control']) !!}
</div>

<!-- F Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('f_id', 'F Id:') !!}
    {!! Form::text('f_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Rating Field -->
<div class="form-group col-sm-6">
    {!! Form::label('rating', 'Rating:') !!}
    {!! Form::text('rating', null, ['class' => 'form-control']) !!}
</div>

<!-- Review Que Field -->
<div class="form-group col-sm-6">
    {!! Form::label('review_que', 'Review Que:') !!}
    {!! Form::text('review_que', null, ['class' => 'form-control']) !!}
</div>

<!-- Review Ans Field -->
<div class="form-group col-sm-6">
    {!! Form::label('review_ans', 'Review Ans:') !!}
    {!! Form::text('review_ans', null, ['class' => 'form-control']) !!}
</div>

<!-- Created By Field -->
<div class="form-group col-sm-6">
    {!! Form::label('created_by', 'Created By:') !!}
    {!! Form::text('created_by', null, ['class' => 'form-control']) !!}
</div>

<!-- Updated By Field -->
<div class="form-group col-sm-6">
    {!! Form::label('updated_by', 'Updated By:') !!}
    {!! Form::text('updated_by', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('review360s.index') }}" class="btn btn-secondary">Cancel</a>
</div>
