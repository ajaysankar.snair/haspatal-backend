

<!-- Patient Id Field -->
<div class="form-group">
    {!! Form::label('patient_id', 'Patient Id:') !!}
    <p>{{ $review360->patient_id }}</p>
</div>

<!-- F Id Field -->
<div class="form-group">
    {!! Form::label('f_id', 'F Id:') !!}
    <p>{{ $review360->f_id }}</p>
</div>

<!-- Rating Field -->
<div class="form-group">
    {!! Form::label('rating', 'Rating:') !!}
    <p>{{ $review360->rating }}</p>
</div>

<!-- Review Que Field -->
<div class="form-group">
    {!! Form::label('review_que', 'Review Que:') !!}
    <p>{{ $review360->review_que }}</p>
</div>

<!-- Review Ans Field -->
<div class="form-group">
    {!! Form::label('review_ans', 'Review Ans:') !!}
    <p>{{ $review360->review_ans }}</p>
</div>


