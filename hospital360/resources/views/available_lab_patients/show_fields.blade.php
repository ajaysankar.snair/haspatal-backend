<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{{ $availableLabPatient->id }}</p>
</div>

<!-- A Patient Id Field -->
<div class="form-group">
    {!! Form::label('a_patient_id', 'A Patient Id:') !!}
    <p>{{ $availableLabPatient->a_patient_id }}</p>
</div>

<!-- A Doctor Id Field -->
<div class="form-group">
    {!! Form::label('a_doctor_id', 'A Doctor Id:') !!}
    <p>{{ $availableLabPatient->a_doctor_id }}</p>
</div>

<!-- Available Lab Field -->
<div class="form-group">
    {!! Form::label('available_lab', 'Available Lab:') !!}
    <p>{{ $availableLabPatient->available_lab }}</p>
</div>

<!-- Created By Field -->
<div class="form-group">
    {!! Form::label('created_by', 'Created By:') !!}
    <p>{{ $availableLabPatient->created_by }}</p>
</div>

<!-- Updated By Field -->
<div class="form-group">
    {!! Form::label('updated_by', 'Updated By:') !!}
    <p>{{ $availableLabPatient->updated_by }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $availableLabPatient->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $availableLabPatient->updated_at }}</p>
</div>

