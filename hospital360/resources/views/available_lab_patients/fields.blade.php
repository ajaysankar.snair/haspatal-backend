<!-- A Patient Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('a_patient_id', 'A Patient Id:') !!}
    {!! Form::text('a_patient_id', null, ['class' => 'form-control']) !!}
</div>

<!-- A Doctor Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('a_doctor_id', 'A Doctor Id:') !!}
    {!! Form::text('a_doctor_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Available Lab Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('available_lab', 'Available Lab:') !!}
    {!! Form::textarea('available_lab', null, ['class' => 'form-control']) !!}
</div>

<!-- Created By Field -->
<div class="form-group col-sm-6">
    {!! Form::label('created_by', 'Created By:') !!}
    {!! Form::text('created_by', null, ['class' => 'form-control']) !!}
</div>

<!-- Updated By Field -->
<div class="form-group col-sm-6">
    {!! Form::label('updated_by', 'Updated By:') !!}
    {!! Form::text('updated_by', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('availableLabPatients.index') }}" class="btn btn-secondary">Cancel</a>
</div>
