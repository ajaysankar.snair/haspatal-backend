<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{{ $hospitalAddFund->id }}</p>
</div>

<!-- Hospital Id Field -->
<div class="form-group">
    {!! Form::label('hospital_id', 'Hospital Id:') !!}
    <p>{{ $hospitalAddFund->hospital_id }}</p>
</div>

<!-- Payment Id Field -->
<div class="form-group">
    {!! Form::label('payment_id', 'Payment Id:') !!}
    <p>{{ $hospitalAddFund->payment_id }}</p>
</div>

<!-- H Amount Field -->
<div class="form-group">
    {!! Form::label('h_amount', 'H Amount:') !!}
    <p>{{ $hospitalAddFund->h_amount }}</p>
</div>

<!-- Created By Field -->
<div class="form-group">
    {!! Form::label('created_by', 'Created By:') !!}
    <p>{{ $hospitalAddFund->created_by }}</p>
</div>

<!-- Updated By Field -->
<div class="form-group">
    {!! Form::label('updated_by', 'Updated By:') !!}
    <p>{{ $hospitalAddFund->updated_by }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $hospitalAddFund->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $hospitalAddFund->updated_at }}</p>
</div>

