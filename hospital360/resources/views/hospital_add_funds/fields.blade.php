<!-- Hospital Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('hospital_id', 'Hospital Id:') !!}
    {!! Form::text('hospital_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Payment Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('payment_id', 'Payment Id:') !!}
    {!! Form::text('payment_id', null, ['class' => 'form-control']) !!}
</div>

<!-- H Amount Field -->
<div class="form-group col-sm-6">
    {!! Form::label('h_amount', 'H Amount:') !!}
    {!! Form::text('h_amount', null, ['class' => 'form-control']) !!}
</div>

<!-- Created By Field -->
<div class="form-group col-sm-6">
    {!! Form::label('created_by', 'Created By:') !!}
    {!! Form::text('created_by', null, ['class' => 'form-control']) !!}
</div>

<!-- Updated By Field -->
<div class="form-group col-sm-6">
    {!! Form::label('updated_by', 'Updated By:') !!}
    {!! Form::text('updated_by', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('hospitalAddFunds.index') }}" class="btn btn-secondary">Cancel</a>
</div>
