<!-- Requested Amount Field -->
<div class="form-group col-sm-6">
    {!! Form::label('requested_amount', 'Requested Amount:') !!}
    {!! Form::text('requested_amount', null, ['class' => 'form-control']) !!}
</div>

<!-- Balance After Approve Field -->
<div class="form-group col-sm-6">
    {!! Form::label('balance_after_approve', 'Balance After Approve:') !!}
    {!! Form::text('balance_after_approve', null, ['class' => 'form-control']) !!}
</div>

<!-- Approved Balance Field -->
<div class="form-group col-sm-6">
    {!! Form::label('approved_balance', 'Approved Balance:') !!}
    {!! Form::text('approved_balance', null, ['class' => 'form-control']) !!}
</div>

<!-- Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('status', 'Status:') !!}
    {!! Form::text('status', null, ['class' => 'form-control']) !!}
</div>

<!-- Created By Field -->
<div class="form-group col-sm-6">
    {!! Form::label('created_by', 'Created By:') !!}
    {!! Form::text('created_by', null, ['class' => 'form-control']) !!}
</div>

<!-- Updated By Field -->
<div class="form-group col-sm-6">
    {!! Form::label('updated_by', 'Updated By:') !!}
    {!! Form::text('updated_by', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('patientRefundRequests.index') }}" class="btn btn-secondary">Cancel</a>
</div>
