<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{{ $patientRefundRequest->id }}</p>
</div>

<!-- Requested Amount Field -->
<div class="form-group">
    {!! Form::label('requested_amount', 'Requested Amount:') !!}
    <p>{{ $patientRefundRequest->requested_amount }}</p>
</div>

<!-- Balance After Approve Field -->
<div class="form-group">
    {!! Form::label('balance_after_approve', 'Balance After Approve:') !!}
    <p>{{ $patientRefundRequest->balance_after_approve }}</p>
</div>

<!-- Approved Balance Field -->
<div class="form-group">
    {!! Form::label('approved_balance', 'Approved Balance:') !!}
    <p>{{ $patientRefundRequest->approved_balance }}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{{ $patientRefundRequest->status }}</p>
</div>


<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $patientRefundRequest->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $patientRefundRequest->updated_at }}</p>
</div>

