<div class="row">
<!-- Pl Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('pl_name', 'Plan Name:') !!}
    {!! Form::text('pl_name', null, ['class' => 'form-control','data-parsley-required-message' =>'Plan Name is required','required']) !!}
</div>

<!-- Pl Price Field -->
<!-- <div class="form-group col-sm-6">
    {!! Form::label('pl_price', 'Plan Deduction:') !!}
    {!! Form::text('pl_price', null, ['class' => 'form-control','data-parsley-required-message' =>'Plan Deduction is required','required']) !!}
</div> -->
<?php 
    
    for ($i=0; $i < 100; $i++) { 
        $deduction[] = $i;
    }

?>
<div class="form-group col-sm-6">
    {!! Form::label('pl_deduction', 'Deduction Percentage:') !!}
    {!! Form::select('pl_deduction', [''=>'Select Deduction']+$deduction , null, ['class' => 'form-control','data-parsley-required-message' =>'Deduction is required','required']) !!}
</div>
</div>
<div class="row">
    <div class="form-group col-sm-6">
    {!! Form::label('pl_daily_sub_amount', 'Daily Subscription Amount:') !!}
    {!! Form::text('pl_daily_sub_amount', null, ['class' => 'form-control','data-parsley-required-message' =>'Daily Subscription Amount is required','required']) !!}
    </div>
    <div class="form-group col-sm-6">
    {!! Form::label('pl_validity', 'Validity (days):') !!}
    {!! Form::text('pl_validity', null, ['class' => 'form-control','data-parsley-required-message' =>'Validity is required','required']) !!}
    </div>
</div>
<div class="row">

<!-- Pl Desc Field -->
<div class="form-group col-sm-6">
    {!! Form::label('pl_desc', 'Plan Description:') !!}
    {!! Form::textarea('pl_desc', null, ['class' => 'form-control','data-parsley-required-message' =>'Plan Description is required','required']) !!}
</div>
<!-- Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('status', 'Status:') !!}
    {!! Form::select('status', [''=>'Select Status','1'=>'Active','2'=>"Deactive"] , null, ['class' => 'form-control','data-parsley-required-message' =>'City is required','required']) !!}
    
</div>
</div>

<!-- Created By Field -->
<!-- <div class="form-group col-sm-6">
    {!! Form::label('created_by', 'Created By:') !!}
    {!! Form::text('created_by', null, ['class' => 'form-control']) !!}
</div> -->

<!-- Updated By Field -->
<!-- <div class="form-group col-sm-6">
    {!! Form::label('updated_by', 'Updated By:') !!}
    {!! Form::text('updated_by', null, ['class' => 'form-control']) !!}
</div> -->

<!-- Submit Field -->
<div class="row">
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('planMasters.index') }}" class="btn btn-secondary">Cancel</a>
</div>
</div>
