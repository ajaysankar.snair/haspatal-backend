@extends('layouts.app')
@section('title', 'Edit Plan')
@section('content')
    <ol class="breadcrumb">
          <li class="breadcrumb-item">
             <a href="{!! route('planMasters.index') !!}">Plan Master</a>
          </li>
          <li class="breadcrumb-item active">Edit</li>
        </ol>
    <div class="container-fluid">
         <div class="animated fadeIn">
             @include('coreui-templates::common.errors')
             <div class="row">
                 <div class="col-lg-12">
                      <div class="card">
                          <div class="card-header">
                              <i class="fa fa-edit fa-lg"></i>
                              <strong>Edit Plan Master</strong>
                          </div>
                          <div class="card-body">
                              {!! Form::model($planMaster, ['route' => ['planMasters.update', $planMaster->pl_id], 'method' => 'patch']) !!}

                              @include('plan_masters.fields')

                              {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
         </div>
    </div>
@endsection