{!! Form::open(['route' => ['planMasters.destroy', $pl_id], 'method' => 'delete']) !!}
<div class='btn-group'>
    <a href="{{ route('planMasters.show', $pl_id) }}" class='btn btn-ghost-success'>
       <i class="fa fa-eye"></i>
    </a>
    <a href="{{ route('planMasters.edit', $pl_id) }}" class='btn btn-ghost-info'>
       <i class="fa fa-edit"></i>
    </a>
    {!! Form::button('<i class="fa fa-trash"></i>', [
        'type' => 'submit',
        'class' => 'btn btn-ghost-danger',
        'onclick' => "return confirm('Are you sure?')"
    ]) !!}
</div>
{!! Form::close() !!}
