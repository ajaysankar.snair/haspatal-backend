<!-- Pl Id Field -->
<div class="form-group">
    {!! Form::label('pl_id', 'Pl Id:') !!}
    <p>{{ $planMaster->pl_id }}</p>
</div>

<!-- Pl Name Field -->
<div class="form-group">
    {!! Form::label('pl_name', 'Pl Name:') !!}
    <p>{{ $planMaster->pl_name }}</p>
</div>

<!-- Pl Price Field -->
<div class="form-group">
    {!! Form::label('pl_price', 'Pl Price:') !!}
    <p>{{ $planMaster->pl_price }}</p>
</div>

<!-- Pl Desc Field -->
<div class="form-group">
    {!! Form::label('pl_desc', 'Pl Desc:') !!}
    <p>{{ $planMaster->pl_desc }}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{{ $planMaster->status }}</p>
</div>

<!-- Created By Field -->
<div class="form-group">
    {!! Form::label('created_by', 'Created By:') !!}
    <p>{{ $planMaster->created_by }}</p>
</div>

<!-- Updated By Field -->
<div class="form-group">
    {!! Form::label('updated_by', 'Updated By:') !!}
    <p>{{ $planMaster->updated_by }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $planMaster->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $planMaster->updated_at }}</p>
</div>

