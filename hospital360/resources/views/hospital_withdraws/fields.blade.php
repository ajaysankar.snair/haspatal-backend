<!-- Hospital Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('hospital_id', 'Hospital Id:') !!}
    {!! Form::text('hospital_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Current Balance Field -->
<div class="form-group col-sm-6">
    {!! Form::label('current_balance', 'Current Balance:') !!}
    {!! Form::text('current_balance', null, ['class' => 'form-control']) !!}
</div>

<!-- Requested Amount Field -->
<div class="form-group col-sm-6">
    {!! Form::label('requested_amount', 'Requested Amount:') !!}
    {!! Form::text('requested_amount', null, ['class' => 'form-control']) !!}
</div>

<!-- Available Balance Field -->
<div class="form-group col-sm-6">
    {!! Form::label('available_balance', 'Available Balance:') !!}
    {!! Form::text('available_balance', null, ['class' => 'form-control']) !!}
</div>

<!-- Created By Field -->
<div class="form-group col-sm-6">
    {!! Form::label('created_by', 'Created By:') !!}
    {!! Form::text('created_by', null, ['class' => 'form-control']) !!}
</div>

<!-- Updated By Field -->
<div class="form-group col-sm-6">
    {!! Form::label('updated_by', 'Updated By:') !!}
    {!! Form::text('updated_by', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('hospitalWithdraws.index') }}" class="btn btn-secondary">Cancel</a>
</div>
