<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{{ $hospitalWithdraw->id }}</p>
</div>

<!-- Hospital Id Field -->
<div class="form-group">
    {!! Form::label('hospital_id', 'Hospital Id:') !!}
    <p>{{ $hospitalWithdraw->hospital_id }}</p>
</div>

<!-- Current Balance Field -->
<div class="form-group">
    {!! Form::label('current_balance', 'Current Balance:') !!}
    <p>{{ $hospitalWithdraw->current_balance }}</p>
</div>

<!-- Requested Amount Field -->
<div class="form-group">
    {!! Form::label('requested_amount', 'Requested Amount:') !!}
    <p>{{ $hospitalWithdraw->requested_amount }}</p>
</div>

<!-- Available Balance Field -->
<div class="form-group">
    {!! Form::label('available_balance', 'Available Balance:') !!}
    <p>{{ $hospitalWithdraw->available_balance }}</p>
</div>

<!-- Created By Field -->
<div class="form-group">
    {!! Form::label('created_by', 'Created By:') !!}
    <p>{{ $hospitalWithdraw->created_by }}</p>
</div>

<!-- Updated By Field -->
<div class="form-group">
    {!! Form::label('updated_by', 'Updated By:') !!}
    <p>{{ $hospitalWithdraw->updated_by }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $hospitalWithdraw->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $hospitalWithdraw->updated_at }}</p>
</div>

