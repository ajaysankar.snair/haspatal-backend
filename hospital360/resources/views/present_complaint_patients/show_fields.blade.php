<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{{ $presentComplaintPatient->id }}</p>
</div>

<!-- Pc Patient Id Field -->
<div class="form-group">
    {!! Form::label('pc_patient_id', 'Pc Patient Id:') !!}
    <p>{{ $presentComplaintPatient->pc_patient_id }}</p>
</div>

<!-- Pc Doctor Id Field -->
<div class="form-group">
    {!! Form::label('pc_doctor_id', 'Pc Doctor Id:') !!}
    <p>{{ $presentComplaintPatient->pc_doctor_id }}</p>
</div>

<!-- Book Id Field -->
<div class="form-group">
    {!! Form::label('book_id', 'Book Id:') !!}
    <p>{{ $presentComplaintPatient->book_id }}</p>
</div>

<!-- Present Complaint Field -->
<div class="form-group">
    {!! Form::label('present_complaint', 'Present Complaint:') !!}
    <p>{{ $presentComplaintPatient->present_complaint }}</p>
</div>

<!-- Complaint Pic Field -->
<div class="form-group">
    {!! Form::label('complaint_pic', 'Complaint Pic:') !!}
    <p>{{ $presentComplaintPatient->complaint_pic }}</p>
</div>

<!-- Complaint Pdf Field -->
<div class="form-group">
    {!! Form::label('complaint_pdf', 'Complaint Pdf:') !!}
    <p>{{ $presentComplaintPatient->complaint_pdf }}</p>
</div>

<!-- Created By Field -->
<div class="form-group">
    {!! Form::label('created_by', 'Created By:') !!}
    <p>{{ $presentComplaintPatient->created_by }}</p>
</div>

<!-- Updated By Field -->
<div class="form-group">
    {!! Form::label('updated_by', 'Updated By:') !!}
    <p>{{ $presentComplaintPatient->updated_by }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $presentComplaintPatient->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $presentComplaintPatient->updated_at }}</p>
</div>

