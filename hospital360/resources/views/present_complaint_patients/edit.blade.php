@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
          <li class="breadcrumb-item">
             <a href="{!! route('presentComplaintPatients.index') !!}">Present Complaint Patient</a>
          </li>
          <li class="breadcrumb-item active">Edit</li>
        </ol>
    <div class="container-fluid">
         <div class="animated fadeIn">
             @include('coreui-templates::common.errors')
             <div class="row">
                 <div class="col-lg-12">
                      <div class="card">
                          <div class="card-header">
                              <i class="fa fa-edit fa-lg"></i>
                              <strong>Edit Present Complaint Patient</strong>
                          </div>
                          <div class="card-body">
                              {!! Form::model($presentComplaintPatient, ['route' => ['presentComplaintPatients.update', $presentComplaintPatient->id], 'method' => 'patch', 'files' => true]) !!}

                              @include('present_complaint_patients.fields')

                              {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
         </div>
    </div>
@endsection