<!-- Pc Patient Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('pc_patient_id', 'Pc Patient Id:') !!}
    {!! Form::text('pc_patient_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Pc Doctor Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('pc_doctor_id', 'Pc Doctor Id:') !!}
    {!! Form::text('pc_doctor_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Book Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('book_id', 'Book Id:') !!}
    {!! Form::text('book_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Present Complaint Field -->
<div class="form-group col-sm-6">
    {!! Form::label('present_complaint', 'Present Complaint:') !!}
    {!! Form::text('present_complaint', null, ['class' => 'form-control']) !!}
</div>

<!-- Complaint Pic Field -->
<div class="form-group col-sm-6">
    {!! Form::label('complaint_pic', 'Complaint Pic:') !!}
    {!! Form::file('complaint_pic[]',['multiple' => 'multiple']) !!}
</div>
<div class="clearfix"></div>

<!-- Complaint Pdf Field -->
<div class="form-group col-sm-6">
    {!! Form::label('complaint_pdf', 'Complaint Pdf:') !!}
    {!! Form::text('complaint_pdf', null, ['class' => 'form-control']) !!}
</div>

<!-- Created By Field -->
<div class="form-group col-sm-6">
    {!! Form::label('created_by', 'Created By:') !!}
    {!! Form::text('created_by', null, ['class' => 'form-control']) !!}
</div>

<!-- Updated By Field -->
<div class="form-group col-sm-6">
    {!! Form::label('updated_by', 'Updated By:') !!}
    {!! Form::text('updated_by', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('presentComplaintPatients.index') }}" class="btn btn-secondary">Cancel</a>
</div>
