<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Haspatal Admin Template</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <link
		href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900,900i&display=swap"
		rel="stylesheet">

    <!-- Theme Style -->
    <!-- <link rel="stylesheet" href="css/style.css"> -->
    <style>
.bg_red{
    background: #db4130;
}
.logo_container{
    height: 100vh;
    padding: 20px;
    display: flex;
    width: 100%;
    justify-content: center;
    align-items: center;
}
.logo_container img{
    display: block;
    width: 250px;
}
.reg_box{
    padding: 30px;
    display: flex;
    justify-content: center;
    align-items: center;
    height: 100vh;
}
.form_container{
    width: 60%;
}
.head_txt{
    text-align: center;
    margin-bottom: 40px;
}
.login_link{
    margin: 50px 0 10px 0;
    text-align: center;
}
.login_link a{
    text-decoration: none;
}
.form-group{
    margin-bottom: 15px;
}
.form-group label{
    margin-bottom: 5px;
}
@media(max-width: 768px){
    .logo_container{
        height: auto;
        padding: 50px 20px;
    }
    .logo_container img{
        width: 180px;
    }
    .reg_box{
        height: auto;
        padding: 50px 30px;
    }
    .form_container{
        width: 80%;
    }
}
header{
    background: #db4130;
}
.logo_head_container{
    padding: 20px 30px;
}
.logo_head_container img{
    display: block;
    width: 180px;
}
.top_bx_ct{
    display: flex;
    padding: 30px;
    justify-content: space-between;
    align-items: center;
}
.top_bx_ct h5{
    margin: 0;
}
.active{
    background: #db4130;
    color: #fff !important;
}
.tabl_container{
    padding: 0 30px;
}
.tbl_contr{
    padding: 30px;
    box-shadow: 0px 0px 20px rgba(92,111,139,0.12);
    border-radius: 10px;
}
th{
    font-size: 14px;
    color: #5a5a5a;
    background: #f5f5f5 !important;
}
td{
    font-size: 13px;
    padding: .3rem .5rem !important;
}
table{
    width: 100%;
}
.acpt{
    background: rgb(15, 153, 73);
    cursor: pointer;
}
.rjct{
    background: rgb(165, 19, 19);
    cursor: pointer;
}
.clr{
    background: rgb(111 111 236);
    cursor: pointer;
}
.awt{
    background: rgb(216 142 8);
    cursor: pointer;
}
.aprc_cl{
    display: block;
    padding: 0px 7px;
    border-radius: 5px;
    background: rgb(15, 153, 73);
    color: #fff;
    width: fit-content;
    font-size: 12px;
}
.rjct_cl{
    display: block;
    padding: 0px 7px;
    border-radius: 5px;
    background: rgb(165, 19, 19);
    color: #fff;
    width: fit-content;
    font-size: 12px;
}
.btn_spcl_btn{
    display: inline-block;
    font-size: 16px;
    color: #db4130;
    padding: 7px;
    width: 200px;
    border: 2px solid #db4130;
    box-shadow: 0 1px 15px 1px rgb(68 68 68 / 15%);
    border-radius: 30px;
    margin-top: 20px;
}
.btn_cont{
    text-align: center;
}
.dropdown-menu.show{
    left: -37px !important;
}
.btn-slct-clr{
    color: #db4130;
    background-color: #ffffff;
    border-color: #db4130;
}
.doc{
    text-decoration: none;
    color: #db4130;
    cursor: pointer;
}
.view{
    text-decoration: none;
    cursor: pointer;
}
.df_blk{
    display: block;
}
.flx-bx{
    display: flex;
    align-items: center;
    margin-bottom: 15px;
}
.fil_lbl{
    width: 230px;
    font-size: 14px;
    color: #656565;
    font-weight: 500;
}
.p_cls{
    width: 100%;
    padding: 10px 15px;
    background: #f5f5f5;
    border-radius: 5px;
    margin:0;
    font-size: 14px;
}
.btn_group_flx{
    margin-bottom: 20px;
    display: flex;
    justify-content: flex-end;
}
.btn_grp_fl_s{
    display: block;
    margin-right: 5px;
    padding: 5px 15px;
    text-decoration: none;
    color: #fff;
    font-size: 12px;
    border-radius: 5px;
}

    </style>
  </head>
  <body>
      <section id="wrx_total">
          <div class="row">
              <div class="col-md-5 bg_red">
                  <div class="logo_container">
                      <img src="{{url('public/images/Logo.png')}}">
                  </div>
              </div>
              <div class="col-md-7">
                  <div class="reg_box">
                      <div class="form_container">
                      @if( session('success'))
        <div class="alert alert-success">{{ session('success') }}</div>
      @endif
      @if ($errors->any())
        <div class="alert alert-danger">
          {!! implode('', $errors->all(':message <br>')) !!}
        </div>
      @endif
                      <form id="settings-form" action="{{ route('otpVerification') }}" method="post">
                        {!! csrf_field() !!}
                            <h4 class="head_txt">Please check your Email for OTP Verification</h4>
                          
                            <input type="hidden" name="user_id" value="{{  $user->id  }}">
                            <div class="form-group">
                                <label>Enter Your OTP</label>
                                <input type="text" class="form-control" id="otp" name="otp" placeholder="Enter your OTP here.." required>
                                <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
                            </div>
                          
                            <div class="btn_cont">
                                <!-- <a  type="submit" class="btn btn_spcl_btn" >Register</a> -->
                                <button type="submit" class="btn btn_spcl_btn">
                    <span class="glyphicon glyphicon-trash">Send Verification</span>
                </button>
                            </div>
                            <!-- <p class="login_link">Already Registered, <a href="{{ url('/loginadmin') }}">Login</a></p> -->
                        </form>
                      </div>
                  </div>
              </div>
          </div>
      </section>
  </body>
  <script>
  console.log("hi");
  </script>
</html>