<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{{ $buyPlan->id }}</p>
</div>

<!-- Plan Id Field -->
<div class="form-group">
    {!! Form::label('plan_id', 'Plan Id:') !!}
    <p>{{ $buyPlan->plan_id }}</p>
</div>

<!-- Price Field -->
<div class="form-group">
    {!! Form::label('price', 'Price:') !!}
    <p>{{ $buyPlan->price }}</p>
</div>

<!-- Limit Field -->
<div class="form-group">
    {!! Form::label('limit', 'Limit:') !!}
    <p>{{ $buyPlan->limit }}</p>
</div>

<!-- Payment Id Field -->
<div class="form-group">
    {!! Form::label('payment_id', 'Payment Id:') !!}
    <p>{{ $buyPlan->payment_id }}</p>
</div>

<!-- Order Id Field -->
<div class="form-group">
    {!! Form::label('order_id', 'Order Id:') !!}
    <p>{{ $buyPlan->order_id }}</p>
</div>

<!-- P Status Field -->
<div class="form-group">
    {!! Form::label('p_status', 'P Status:') !!}
    <p>{{ $buyPlan->p_status }}</p>
</div>

<!-- Created By Field -->
<div class="form-group">
    {!! Form::label('created_by', 'Created By:') !!}
    <p>{{ $buyPlan->created_by }}</p>
</div>

<!-- Updated By Field -->
<div class="form-group">
    {!! Form::label('updated_by', 'Updated By:') !!}
    <p>{{ $buyPlan->updated_by }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $buyPlan->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $buyPlan->updated_at }}</p>
</div>

