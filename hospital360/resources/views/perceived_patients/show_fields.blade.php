<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{{ $perceivedPatient->id }}</p>
</div>

<!-- P Patient Id Field -->
<div class="form-group">
    {!! Form::label('p_patient_id', 'P Patient Id:') !!}
    <p>{{ $perceivedPatient->p_patient_id }}</p>
</div>

<!-- P Doctor Id Field -->
<div class="form-group">
    {!! Form::label('p_doctor_id', 'P Doctor Id:') !!}
    <p>{{ $perceivedPatient->p_doctor_id }}</p>
</div>

<!-- Perceived Field -->
<div class="form-group">
    {!! Form::label('perceived', 'Perceived:') !!}
    <p>{{ $perceivedPatient->perceived }}</p>
</div>

<!-- Created By Field -->
<div class="form-group">
    {!! Form::label('created_by', 'Created By:') !!}
    <p>{{ $perceivedPatient->created_by }}</p>
</div>

<!-- Updated By Field -->
<div class="form-group">
    {!! Form::label('updated_by', 'Updated By:') !!}
    <p>{{ $perceivedPatient->updated_by }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $perceivedPatient->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $perceivedPatient->updated_at }}</p>
</div>

