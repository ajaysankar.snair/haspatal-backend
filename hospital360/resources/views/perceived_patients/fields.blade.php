<!-- P Patient Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('p_patient_id', 'P Patient Id:') !!}
    {!! Form::text('p_patient_id', null, ['class' => 'form-control']) !!}
</div>

<!-- P Doctor Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('p_doctor_id', 'P Doctor Id:') !!}
    {!! Form::text('p_doctor_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Perceived Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('perceived', 'Perceived:') !!}
    {!! Form::textarea('perceived', null, ['class' => 'form-control']) !!}
</div>

<!-- Created By Field -->
<div class="form-group col-sm-6">
    {!! Form::label('created_by', 'Created By:') !!}
    {!! Form::text('created_by', null, ['class' => 'form-control']) !!}
</div>

<!-- Updated By Field -->
<div class="form-group col-sm-6">
    {!! Form::label('updated_by', 'Updated By:') !!}
    {!! Form::text('updated_by', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('perceivedPatients.index') }}" class="btn btn-secondary">Cancel</a>
</div>
