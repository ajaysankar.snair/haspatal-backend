<div class="row">
     <?php
        $countryList = \App\Models\Country::orderBy('country','asc')->pluck('country','id');
        $stateList = \App\Models\State::orderBy('state','asc')->pluck('state','id');
        $cityList = \App\Models\City::orderBy('city','asc')->pluck('city','id'); 
    ?>
    <!-- Country Id Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('country_id', 'Country Id:') !!}
       {!! Form::select('country_id', ['' => 'Select Country'] + $countryList->toArray(), null, ['class' => 'form-control','id' =>'countryId','required','data-parsley-required-message'=>'Country is required' ]) !!}
                                        
    </div>

    <!-- State Id Field -->
   <!--  <div class="form-group col-sm-6">
        {!! Form::label('state_id', 'State Id:') !!}
        {!! Form::select('state_id', ['Select State' => 'Select State'], null, ['class' => 'form-control','id'=>'state']) !!}
    </div> -->

    <!-- City Id Field -->
   <!--  <div class="form-group col-sm-6">
        {!! Form::label('city_id', 'City Id:') !!}
        {!! Form::select('city_id', ['Select City' => 'Select City'], null, ['class' => 'form-control','id'=>'city']) !!}
    </div> -->
     @if(isset($haspatalRegsiter))
        <div class="form-group col-sm-6">
            <?php $state  = \App\Models\State::orderBy('state','asc')->where('id',$haspatalRegsiter->country_id)->pluck('state','id');
             ?>
            {!! Form::label('state_id', 'State:') !!}
            {!! Form::select('state_id', ['' => 'Select State']+$state->toArray(), null, ['class' => 'form-control','id'=>'state']) !!}
        </div>
        @else
        <div class="form-group col-sm-6">
            {!! Form::label('state_id', 'State:') !!}
            {!! Form::select('state_id', ['Select State' => 'Select State'], null, ['class' => 'form-control','id'=>'state']) !!}
        </div>
        @endif
        @if(isset($haspatalRegsiter))
        <div class="form-group col-sm-6">
            <?php $city = \App\Models\City::orderBy('city','asc')->where('id',$haspatalRegsiter->state_id)->pluck('city','id');   ?>
            {!! Form::label('city_id', 'City:') !!}
            {!! Form::select('city_id', ['' => 'Select City']+$city->toArray(), null, ['class' => 'form-control','id'=>'city']) !!}
        </div>
        @else
        <!-- Sm City Field -->
        <div class="form-group col-sm-6">
            {!! Form::label('city_id', 'City:') !!}
            {!! Form::select('city_id', ['Select City' => 'Select City'], null, ['class' => 'form-control','id'=>'city']) !!}
        </div>
        @endif

    <!-- Pincode Id Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('pincode_id', 'Pincode Id:') !!}
     <!--   // {!! Form::select('pincode_id', ['Select Pincode' => 'Select Pincode'], null, ['class' => 'form-control']) !!} -->
       {!! Form::text('pincode_id', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Hospital Name Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('hospital_name', 'Hospital Name:') !!}
        {!! Form::text('hospital_name', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Address Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('address', 'Address:') !!}
        {!! Form::textarea('address', null, ['class' => 'form-control','rows' => 3]) !!}
    </div>

    <!-- Year Of Establishment Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('year_of_establishment', 'Year Of Establishment:') !!}
        {!! Form::text('year_of_establishment', null, ['class' => 'form-control']) !!}
        
    </div>

    <!-- Gst No Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('gst_no', 'Gst No:') !!}
        {!! Form::text('gst_no', null, ['class' => 'form-control']) !!}
    </div>

    <!-- License Copy Field -->
   <!--  <div class="form-group col-sm-6">
        {!! Form::label('license_copy', 'License Copy:') !!}
        {!! Form::file('license_copy') !!}
    </div>
    <div class="clearfix"></div> -->

    @if(isset($haspatalRegsiter))
        <!-- Sm Image Field -->
        <div class="form-group col-sm-3">
            {!! Form::label('hospital_location', 'Hospital Location:') !!}
            {!! Form::file('hospital_location') !!}
        </div>
        <div class="col-sm-3">
            <?php 
                if ($haspatalRegsiter->hospital_location) {
                    $img = url('public/media/hospital_location/').'/'.$haspatalRegsiter->hospital_location; 
                }else{
                    $img = url('public/default-image.jpg'); 
                }
            ?>
            <img src="{{$img}}" style="width: 50%">
        </div>

        @else
        <!-- Sm Image Field -->
        <div class="form-group col-sm-6">
            {!! Form::label('hospital_location', 'Hospital Location:') !!}
            {!! Form::file('hospital_location') !!}
        </div>
        <div class="clearfix"></div>
        @endif

        @if(isset($haspatalRegsiter))
        <!-- Sm Image Field -->
        <div class="form-group col-sm-3">
            {!! Form::label('hospital_profile', 'Hospital Profile:') !!}
            {!! Form::file('hospital_profile') !!}
        </div>
        <div class="col-sm-3">
            <?php 
                if ($haspatalRegsiter->hospital_profile) {
                    $img = url('public/media/hospital_profile/').'/'.$haspatalRegsiter->hospital_profile; 
                }else{
                    $img = url('public/default-image.jpg'); 
                }
            ?>
            <img src="{{$img}}" style="width: 50%">
        </div>

        @else
        <!-- Sm Image Field -->
        <div class="form-group col-sm-6">
            {!! Form::label('hospital_profile', 'License Copy:') !!}
            {!! Form::file('hospital_profile') !!}
        </div>
        <div class="clearfix"></div>
        @endif
   @if(isset($haspatalRegsiter))
        <!-- Sm Image Field -->
        <div class="form-group col-sm-3">
            {!! Form::label('license_copy', 'License Copy:') !!}
            {!! Form::file('license_copy') !!}
        </div>
        <div class="col-sm-3">
            <?php 
                if ($haspatalRegsiter->license_copy) {
                    $img = url('public/media/license_copy/').'/'.$haspatalRegsiter->license_copy; 
                }else{
                    $img = url('public/default-image.jpg'); 
                }
            ?>
            <img src="{{$img}}" style="width: 50%">
        </div>

        @else
        <!-- Sm Image Field -->
        <div class="form-group col-sm-6">
            {!! Form::label('license_copy', 'License Copy:') !!}
            {!! Form::file('license_copy') !!}
        </div>
        <div class="clearfix"></div>
        @endif
    <!-- Website Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('website', 'Website:') !!}
        {!! Form::text('website', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group col-sm-6">
        {!! Form::label('email', 'Email:') !!}
        {!! Form::text('email', null, ['class' => 'form-control']) !!}
    </div> 

    <div class="form-group col-sm-6">
        {!! Form::label('mobile', 'Mobile:') !!}
        {!! Form::text('mobile', null, ['class' => 'form-control']) !!}
    </div> 

    <!-- Contact Person Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('contact_person', 'Contact Person:') !!}
        {!! Form::text('contact_person', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Contact Person Email Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('contact_person_email', 'Contact Person Email:') !!}
        {!! Form::email('contact_person_email', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Contact Person Phonoe No Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('contact_person_phonoe_no', 'Contact Person Phonoe No:') !!}
        {!! Form::text('contact_person_phonoe_no', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Other Phone No Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('other_phone_no', 'Other Phone No:') !!}
        {!! Form::text('other_phone_no', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Support Watsapp No Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('support_watsapp_no', 'Support Watsapp No:') !!}
        {!! Form::text('support_watsapp_no', null, ['class' => 'form-control']) !!}
    </div>

     

        @if(isset($haspatalRegsiter))
    <div class="form-group col-sm-6">
         
        {!! Form::label('token', 'Status:') !!}
        {!! Form::select('ref_code',array('0' => 'Deactive', '1' => 'Active'),null,['class' => 'form-control', 'id' => 'status','required']) !!}
   
    </div>
    @endif
    <!-- Submit Field -->
    <div class="form-group col-sm-12">
        {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
        <a href="{{ route('haspatalRegsiters.index') }}" class="btn btn-secondary">Cancel</a>
    </div>
</div>