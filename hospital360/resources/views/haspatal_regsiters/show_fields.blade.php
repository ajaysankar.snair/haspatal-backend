

<!-- Country Id Field -->
<div class="form-group">
    <?php $country_data = App\Models\Country::where('id',$haspatalRegsiter->country_id)->first(); ?>
    {!! Form::label('country_id', 'Country Name:') !!}
    <p> @isset($country_data->country)
        {{$country_data->country}}
        @endisset
     </p>
</div>

<!-- State Id Field -->
<div class="form-group">
    <?php $state_data = App\Models\State::where('id',$haspatalRegsiter->state_id)->first(); ?>
    {!! Form::label('state_id', 'State Name:') !!}
    <p>{{ $state_data->state }}</p>
    <p> @isset($state_data->state )
        {{$state_data->state }}
        @endisset
     </p>
</div>

<!-- City Id Field -->
<div class="form-group">
    <?php $city_data = App\Models\City::where('id',$haspatalRegsiter->city_id)->first(); ?>
    {!! Form::label('city_id', 'City Name:') !!}
    <p> @isset($city_data->city)
        {{$city_data->city}}
        @endisset
     </p>
</div>

<!-- Pincode Id Field -->
<div class="form-group">
    {!! Form::label('pincode_id', 'Pincode Id:') !!}
    <p>{{ $haspatalRegsiter->pincode_id }}</p>
</div>

<!-- Hospital Name Field -->
<div class="form-group">
    {!! Form::label('hospital_name', 'Hospital Name:') !!}
    <p>{{ $haspatalRegsiter->hospital_name }}</p>
</div>

<!-- Address Field -->
<div class="form-group">
    {!! Form::label('address', 'Address:') !!}
    <p>{{ $haspatalRegsiter->address }}</p>
</div>

<!-- Year Of Establishment Field -->
<div class="form-group">
    {!! Form::label('year_of_establishment', 'Year Of Establishment:') !!}
    <p>{{ $haspatalRegsiter->year_of_establishment }}</p>
</div>

<!-- Gst No Field -->
<div class="form-group">
    {!! Form::label('gst_no', 'Gst No:') !!}
    <p>{{ $haspatalRegsiter->gst_no }}</p>
</div>

<!-- License Copy Field -->
<div class="form-group">
    {!! Form::label('license_copy', 'License Copy:') !!}
    <p>{{ $haspatalRegsiter->license_copy }}</p>
</div>

<!-- Website Field -->
<div class="form-group">
    {!! Form::label('website', 'Website:') !!}
    <p>{{ $haspatalRegsiter->website }}</p>
</div>

<!-- Contact Person Field -->
<div class="form-group">
    {!! Form::label('contact_person', 'Contact Person:') !!}
    <p>{{ $haspatalRegsiter->contact_person }}</p>
</div>

<!-- Contact Person Email Field -->
<div class="form-group">
    {!! Form::label('contact_person_email', 'Contact Person Email:') !!}
    <p>{{ $haspatalRegsiter->contact_person_email }}</p>
</div>

<!-- Contact Person Phonoe No Field -->
<div class="form-group">
    {!! Form::label('contact_person_phonoe_no', 'Contact Person Phonoe No:') !!}
    <p>{{ $haspatalRegsiter->contact_person_phonoe_no }}</p>
</div>

<!-- Other Phone No Field -->
<div class="form-group">
    {!! Form::label('other_phone_no', 'Other Phone No:') !!}
    <p>{{ $haspatalRegsiter->other_phone_no }}</p>
</div>

<!-- Support Watsapp No Field -->
<div class="form-group">
    {!! Form::label('support_watsapp_no', 'Support Watsapp No:') !!}
    <p>{{ $haspatalRegsiter->support_watsapp_no }}</p>
</div>

<!-- Created At Field -->

