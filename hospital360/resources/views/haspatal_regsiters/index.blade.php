@extends('layouts.app')
@section('title', 'Haspatal Regsiter')
@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">Haspatal Regsiters</li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
             @include('flash::message')
             <div class="row">
                 <div class="col-lg-12">
                     <div class="card">
                         <div class="card-header">
                             <i class="fa fa-align-justify"></i>
                             HaspatalRegsiters
                             <a class="pull-right" href="{{ route('haspatalRegsiters.create') }}"><i class="fa fa-plus-square fa-lg"></i></a>
                         </div>
                         <div class="card-body">
                             @include('haspatal_regsiters.table')
                              <div class="pull-right mr-3">
                                     
                              </div>
                         </div>
                     </div>
                  </div>
             </div>
         </div>
    </div>
@endsection

