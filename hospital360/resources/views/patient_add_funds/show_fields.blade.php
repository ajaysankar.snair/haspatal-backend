<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{{ $patientAddFund->id }}</p>
</div>

<!-- Patient Id Field -->
<div class="form-group">
    {!! Form::label('patient_id', 'Patient Id:') !!}
    <p>{{ $patientAddFund->patient_id }}</p>
</div>

<!-- Requested Amount Field -->
<div class="form-group">
    {!! Form::label('requested_amount', 'Requested Amount:') !!}
    <p>{{ $patientAddFund->requested_amount }}</p>
</div>

<!-- Balance After Approve Field -->
<div class="form-group">
    {!! Form::label('balance_after_approve', 'Balance After Approve:') !!}
    <p>{{ $patientAddFund->balance_after_approve }}</p>
</div>

<!-- Approved Balance Field -->
<div class="form-group">
    {!! Form::label('approved_balance', 'Approved Balance:') !!}
    <p>{{ $patientAddFund->approved_balance }}</p>
</div>

<!-- Transaction Type Field -->
<div class="form-group">
    {!! Form::label('transaction_type', 'Transaction Type:') !!}
    <p>{{ $patientAddFund->transaction_type }}</p>
</div>

<!-- Transaction Proof Field -->
<div class="form-group">
    {!! Form::label('transaction_proof', 'Transaction Proof:') !!}
    <p>{{ $patientAddFund->transaction_proof }}</p>
</div>

<!-- Transaction Date Field -->
<div class="form-group">
    {!! Form::label('transaction_date', 'Transaction Date:') !!}
    <p>{{ $patientAddFund->transaction_date }}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{{ $patientAddFund->status }}</p>
</div>

<!-- Created By Field -->
<div class="form-group">
    {!! Form::label('created_by', 'Created By:') !!}
    <p>{{ $patientAddFund->created_by }}</p>
</div>

<!-- Updated By Field -->
<div class="form-group">
    {!! Form::label('updated_by', 'Updated By:') !!}
    <p>{{ $patientAddFund->updated_by }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $patientAddFund->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $patientAddFund->updated_at }}</p>
</div>

