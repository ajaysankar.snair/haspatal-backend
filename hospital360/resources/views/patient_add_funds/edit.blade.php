@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
          <li class="breadcrumb-item">
             <a href="{!! route('patientAddFunds.index') !!}">Patient Add Fund</a>
          </li>
          <li class="breadcrumb-item active">Edit</li>
        </ol>
    <div class="container-fluid">
         <div class="animated fadeIn">
             @include('coreui-templates::common.errors')
             <div class="row">
                 <div class="col-lg-12">
                      <div class="card">
                          <div class="card-header">
                              <i class="fa fa-edit fa-lg"></i>
                              <strong>Edit Patient Add Fund</strong>
                          </div>
                          <div class="card-body">
                              {!! Form::model($patientAddFund, ['route' => ['patientAddFunds.update', $patientAddFund->id], 'method' => 'patch', 'files' => true]) !!}

                              @include('patient_add_funds.fields')

                              {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
         </div>
    </div>
@endsection