@extends('layouts.nheader')
@section('title', 'Records')
@section('content')
<style type="text/css">
    .card:hover {
        color: white;
  background-color: red;
}
.card{
  height: 215px; 
  border-radius: 20px; 
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
}
.card-title{
    margin-top: 5%;
}
a:hover {
    /*color: white !important;*/
    text-decoration: none;
}
</style>
<!-- <ol class="breadcrumb">
        <li class="breadcrumb-item">Doctors</li>
    </ol> -->
    <div class="container-fluid" style="margin-top: 3%;">
        <div class="animated fadeIn">
             <div class="container">
                    <div class="row">

                        <div class="col-sm-4">
                            <div class="card  newcard text-center border-danger">
                                 <a href="{{ url('/myconsults_records') }}" style="color: black;"> 
                                <div class="card-block" style="padding: 50px;">
                                    <img  src="{{ URL::asset('public/image/icons/My consults.png') }}" style="width: 30%;" alt="logo">
                                    <h4 class="card-title">My Consults</h4>
                                </div>
                                </a>
                                
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="card newcard text-center border-danger">
                                <a href="{{ url('/myvisit_summaries') }}" style="color: black;"> 
                                <div class="card-block" style="padding: 50px;">
                                     <img  src="{{ URL::asset('public/image/icons/MY smuumaries.png') }}" style="width: 30%;" alt="logo">
                                    <h4 class="card-title">My Visit Summaries</h4>
                                    
                                </div>
                                </a>
                                
                            </div>
                         </div>
                    
                         <!-- <div class="col-sm-4">
                                <div class="card newcard text-center border-danger">
                                    <a href="{{ url('/mybookings') }}" style="color: black;"> 
                                    <div class="card-block" style="padding: 50px;">
                                         <i class="fa fa-stethoscope" style="font-size: 80px;"></i>
                                        <h4 class="card-title">My Bookings</h4>
                                        
                                    </div>
                                    </a>
                                    
                                </div>

                        </div> -->

                <div class="col-sm-4">
                    <div class="card newcard text-center border-danger">
                        <a href="{{ url('/my_prescriptions') }}" style="color: black;"> 
                        <div class="card-block" style="padding: 50px;">
                             <img  src="{{ URL::asset('public/image/icons/Prescription.png') }}" style="width: 40%;" alt="logo">
                            <h4 class="card-title">My Prescriptions</h4>
                            
                        </div>
                    </a>
                        
                    </div>
                </div>



                </div>

            </div>

         </div>
    </div>
@endsection