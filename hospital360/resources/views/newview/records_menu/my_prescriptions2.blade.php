@extends('layouts.nheader')
@section('title', 'My Prescriptions')
@section('content')
<style type="text/css">
    /*.card:hover {
        color: white;
  background-color: red;
}*/
.card{
  height: 400px; 
  border-radius: 20px; 
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
}
.card-title{
    margin-top: 5%;
}
.bio{
   /* border-radius: 17px !important;*/
    border: 1px solid red;
    /*margin-left: 5%;
    margin-right: 5%;*/
    height: 30px;
}
.btn-danger {
    color: #fff;
    background-color: red;
    border-color: red;
}
/*a:hover {
    color: white !important;
    text-decoration: none;
}*/
hr {
    margin-top: 1rem;
    margin-bottom: 1rem;
    border: 0;
    border-top: 1px solid red;
}
.form-control {
    border: 1px solid red;
}
label {
    font-size: 15px;
    margin-top: 1%;
}
.parsley-required{
    color: red;
}
.alert-success {
    color: white;
    background-color: green;
    border-color: #cdedd8;
}
</style>
<!-- <ol class="breadcrumb">
        <li class="breadcrumb-item">Doctors</li>
    </ol> -->
    
    <div class="container-fluid" style="margin-top: 3%;">
        <div class="animated fadeIn">
             <div class="container">
                
                <div class="row">
                      <div class="col-sm-8">
                         <div class="card newcard  border-danger">
                            <div class="card-block" style="margin-top: -4%;">
                                <h3 class="card-title text-center">My Prescriptions</h3>
                                <div class="row" style="margin-left: 1%;margin-right: 1%;">
                                   <div class="col-sm-12">
                                         <div class="table-responsive">          
                                        <table class="table">
                                          <thead>
                                            <tr>
                                              <th>ID</th>
                                              <th>Booking ID</th>
                                              <th>Patient Name</th>
                                              <th>Prescriptions</th>
                                              
                                            </tr>
                                          </thead>
                                          <tbody>
                                             <?php if(isset($prescription_details)) { ?>
                                             @if (!$prescription_details->isEmpty())
                                             @foreach($prescription_details as $value)
                                            <tr>
                                              <td> {{$value->id}}</td>
                                              <td> {{$value->booking_id}}</td>
                                              <td>{{$value->first_name}} {{$value->lastname}}</td>
                                              <td>{{$value->prescription}}  </td>
                                            </tr>
                                          </tbody>
                                           @endforeach
                                           @else
                                           <h3 class="text-center" style="margin-top: 5%;"> No Records Found</h3>
                                           @endif
                                         <?php } ?>
                                        </table>
                                        </div>
                                    

                                    </div>

                                </div>
                                <!-- <div class="text-center" style="margin-top: 1%;">
                                  <button class="btn btn-danger" type="button" style="width: 30%;">Submit</button>
                                </div> -->
                            </div>
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="card newcard text-center border-danger">
                            <div class="card-block">
                                    <h4 class="card-title">Need Help?</h4>
                                    <hr>    
                                    <h4 class="card-title">Video Tutorial</h4>
                                    <hr>    
                                    <h4 class="card-title">Helpbook</h4>
                                    <hr>  
                                </div>
                                
                            </div>
                      </div>
                </div>
                    
            </div>

         </div>
    </div>
       
@endsection
@push('scripts')
   <script type="text/javascript">
           $('#start_date').datetimepicker({
               format: 'YYYY-MM-DD',
               useCurrent: true,
               icons: {
                   up: "icon-arrow-up-circle icons font-2xl",
                   down: "icon-arrow-down-circle icons font-2xl"
               },
               sideBySide: true
           })
       </script>
       <script type="text/javascript">
           $('#end_date').datetimepicker({
               format: 'YYYY-MM-DD',
               useCurrent: true,
               icons: {
                   up: "icon-arrow-up-circle icons font-2xl",
                   down: "icon-arrow-down-circle icons font-2xl"
               },
               sideBySide: true
           })
       </script>
@endpush