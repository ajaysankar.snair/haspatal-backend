@extends('layouts.nheader')
@section('title', 'Add Support')
@section('content')
<style type="text/css">
    /*.card:hover {
        color: white;
  background-color: red;
}*/
.card{
  /*/height: 370px; */
  border-radius: 20px; 
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
}
.card-title{
    margin-top: 5%;
}
.bio{
    border-radius: 17px !important;
    border: 1px solid red;
    margin-left: 5%;
    margin-right: 5%;
    height: 230px;
}
.btn-danger {
    color: #fff;
    background-color: red;
    border-color: red;
}
/*a:hover {
    color: white !important;
    text-decoration: none;
}*/
hr {
    margin-top: 1rem;
    margin-bottom: 1rem;
    border: 0;
    border-top: 1px solid red;
}
.form-control {
    border: 1px solid red;
}
label {
    font-size: 15px;
    margin-top: 1%;
}
.parsley-required{
    color: red;
}
.alert-success {
    color: white;
    background-color: green;
    border-color: #cdedd8;
}
</style>
<!-- <ol class="breadcrumb">
        <li class="breadcrumb-item">Doctors</li>
    </ol> -->
    {!! Form::open(['route' => 'doctor_add_support.store', 'files' => true,'id' => 'myForm']) !!}
    <div class="container-fluid" style="margin-top: 3%;">
        <div class="animated fadeIn">
             <div class="container">
                  @include('flash::message')
                <div class="row">
                      <div class="col-sm-8">
                         <div class="card newcard  border-danger">
                            <div class="card-block" style="margin-top: -4%;">
                                <h3 class="card-title text-center">Support</h3>
                                <div class="row" style="margin-left: 1%;margin-right: 1%;margin-bottom: 5%;">
                                  <div class="col-sm-12">                                       
                                       <label for="title">Title</label>
                                        {!! Form::text('title', null, ['class' => 'form-control','data-parsley-required-message' =>'Title is required','required']) !!}                                        
                                    </div>
                                    <div class="col-sm-12">                                       
                                       <label for="ticket_no">Ticket No.</label>
                                        {!! Form::text('ticket_no', null, ['class' => 'form-control','data-parsley-required-message' =>'Ticket No. is required','required']) !!}                                        
                                    </div>
                                   <div class="col-sm-12">                                       
                                       <label for="description">Description</label>
                                        {!! Form::textarea('description', null, ['class' => 'form-control','data-parsley-required-message' =>'Description is required','required','rows'=>'3']) !!}                                        
                                    </div>

                                </div>
                                <div class="text-center" style="margin-top: 1%;margin-bottom: 2%;">
                                  <button class="btn btn-danger" type="submit" style="width: 30%;">Submit</button>
                                </div>
                            </div>
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="card newcard text-center border-danger">
                            <div class="card-block">
                                    <h4 class="card-title">Need Help?</h4>
                                    <hr>    
                                    <h4 class="card-title">Video Tutorial</h4>
                                    <hr>    
                                    <h4 class="card-title">Helpbook</h4>
                                    <hr>
                                    <p class="text-center">New Prices shall not be applicable on previously booked consults</p>  
                                </div>
                                
                            </div>
                      </div>
                </div>
                    
            </div>

         </div>
    </div>
  {!! Form::close() !!}     
@endsection