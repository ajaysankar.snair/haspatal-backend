@extends('layouts.nheader')
@section('title', 'Patients Queue')
@section('content')
<style type="text/css">
    /*.card:hover {
        color: white;
  background-color: red;
}*/
.card{
  height: auto; 
  border-radius: 20px; 
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
}
.card-title{
    margin-top: 5%;
    <?php
    $url1=$_SERVER['REQUEST_URI'];
    header("Refresh: 5; URL=$url1");
?>
}
.bio{
    border-radius: 10px !important;
    border: 1px solid red;
    height: 75px;
}
.btn-danger {
    color: #fff;
    background-color: red;
    border-color: red;
}
/*a:hover {
    color: white !important;
    text-decoration: none;
}*/
hr {
    margin-top: 1rem;
    margin-bottom: 1rem;
    border: 0;
    border-top: 1px solid red;
}
.alert-success {
    color: white;
    background-color: green;
    border-color: #cdedd8;
}
.parsley-required{
    color: red;
}
</style>
<!-- <ol class="breadcrumb">
        <li class="breadcrumb-item">Doctors</li>
    </ol> -->

    <div class="container-fluid" style="margin-top: 3%;">
        <div class="animated fadeIn">
             <div class="container">
                <div class="row">
                      <div class="col-sm-12">
                         <div class="card newcard  border-danger">
                            <div class="card-block" style="margin-top: -4%;">
                                <h3 class="card-title" style="margin-left: 2%;">Patients complaint Pic</h3>
                                <div class="row" style="margin-left: 1%;margin-right: 1%;margin-top: 5%;">

                                  <?php 
                                    $complaint = explode('$', $complaint_patient->complaint_pic); ?>
                                    <?php foreach ($complaint as $value) { ?>
                                      <div class="col-sm-6">
                                      <img src="<?php echo $pic = env('APP_URL').'public/media/complaint_pic/'.$value ?>" style="width: 80%; height: 80%;"> 
                                    </div>
                                    <?php } ?>
                                </div>
                              

                                <div style="margin-top: 5%;margin-bottom: 5%;">
                                  
                                </div>
                            </div>
                            
                        </div>
                      </div>
                </div>
                    
            </div>

         </div>
    </div>
@endsection