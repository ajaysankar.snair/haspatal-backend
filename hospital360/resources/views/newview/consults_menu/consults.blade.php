@extends('layouts.nheader')
@section('title', 'Consults')
@section('content')
<style type="text/css">
    .card:hover {
        color: white;
  background-color: red;
}
.card{
  height: 215px; 
  border-radius: 20px; 
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
}
.card-title{
    margin-top: 5%;
}
a:hover {
   /* color: white !important;*/
    text-decoration: none;
}
</style>
<!-- <ol class="breadcrumb">
        <li class="breadcrumb-item">Doctors</li>
    </ol> -->
    <div class="container-fluid" style="margin-top: 3%;">
        <div class="animated fadeIn">
             <div class="container">
                    <div class="row">

                        <div class="col-sm-4">
                                    <div class="card  newcard text-center border-danger">
                                         <a href="{{ url('/patients_queue') }}" style="color: black;"> 
                                            <div class="card-block" style="padding: 50px;">
                                                <i class="fa fa-stethoscope" style="font-size: 80px;"></i>
                                                <h4 class="card-title">Patients Queue</h4>
                                            </div>
                                        </a>
                                        
                                    </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="card newcard text-center border-danger">
                                <a href="{{ url('/tostart') }}" style="color: black;"> 
                                    <div class="card-block" style="padding: 50px;">
                                        <!-- <img  src="{{ URL::asset('public/image/icons/Address.png') }}" style="width: 30%;" alt="logo"> -->
                                        <i class="fa fa-stethoscope" style="font-size: 80px;"></i>
                                        <h4 class="card-title">To Start</h4>
                                        
                                    </div>
                                </a>
                                
                            </div>
                         </div>
                    
                         <div class="col-sm-4">
                                <div class="card newcard text-center border-danger">
                                        <a href="{{ url('/tocomplete') }}" style="color: black;"> 
                                            <div class="card-block" style="padding: 50px;">
                                                <i class="fa fa-stethoscope" style="font-size: 80px;"></i>
                                                <h4 class="card-title">To Complete</h4>
                                                
                                            </div>
                                         </a>
                                    
                                </div>

                        </div>

                <div class="col-sm-4">
                         <div class="card newcard text-center border-danger">
                            <a href="{{ url('/reschedule') }}" style="color: black;"> 
                                <div class="card-block" style="padding: 50px;">
                                    <i class="fa fa-stethoscope" style="font-size: 80px;"></i>
                                    <h4 class="card-title">Reschedule</h4>
                                    
                                </div>
                            </a>
                            
                        </div>
                </div>

                <div class="col-sm-4">
                         <div class="card newcard text-center border-danger">
                            <a href="{{ url('/cancel') }}" style="color: black;"> 
                                <div class="card-block" style="padding: 50px;">
                                    <i class="fa fa-stethoscope" style="font-size: 80px;"></i>
                                    <h4 class="card-title">Cancel / Refund</h4>
                                    
                                </div>
                            </a>
                            
                        </div>
                </div>

                </div>

            </div>

         </div>
    </div>
@endsection