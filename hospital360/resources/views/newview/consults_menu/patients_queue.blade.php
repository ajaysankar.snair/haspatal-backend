@extends('layouts.nheader')
@section('title', 'Patients Queue')
@section('content')
<style type="text/css">
    /*.card:hover {
        color: white;
  background-color: red;
}*/
.card{
  height: 330px; 
  border-radius: 20px; 
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
}
.card-title{
    margin-top: 5%;
}
.bio{
    border-radius: 17px !important;
    border: 1px solid red;
    height: 140px;
    padding: 25%;
     box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
}
.btn-danger {
    color: #fff;
    background-color: red;
    border-color: red;
}
/*a:hover {
    color: white !important;
    text-decoration: none;
}*/
hr {
    margin-top: 1rem;
    margin-bottom: 1rem;
    border: 0;
    border-top: 1px solid red;
}
.alert-success {
    color: white;
    background-color: green;
    border-color: #cdedd8;
}
.parsley-required{
    color: red;
}
</style>
<!-- <ol class="breadcrumb">
        <li class="breadcrumb-item">Doctors</li>
    </ol> -->
    <?php 
  
 ?>
    <div class="container-fluid" style="margin-top: 3%;">
        <div class="animated fadeIn">
             <div class="container">
                <div class="row">
                      <div class="col-sm-8">
                         <div class="card newcard  border-danger">
                            <div class="card-block" style="margin-top: -4%;">
                                <h3 class="card-title text-center">Patients Queue</h3>
                                <div class="row" style="margin-left: 1%;margin-right: 1%;margin-top: 5%;">
                                   <div class="col-sm-4">

                                    <div class="text-center bio">
                                     <h3>{{ $allcount }}</h3>
                                    </div>

                                    <div class="text-center" style="margin-top: 10%;">
                                     <h4>Total Appoiments</h4> 
                                      </div>
                                    </div>
                                    <div class="col-sm-4">
                                     <div class="text-center bio">
                                    <h3>{{$check_count}}</h3>
                                    </div>

                                    <div class="text-center" style="margin-top: 10%;">
                                      <h4>Checked-in So Far</h4>
                                      </div>
                                    </div>
                                    <div class="col-sm-4">
                                     <div class="text-center bio">
                                     <h3>{{ $waiting_count }}</h3>
                                    </div>

                                    <div class="text-center" style="margin-top: 10%;">
                                      <h4>Patients in Waiting Room</h4>
                                      </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="card newcard text-center border-danger">
                            <div class="card-block">
                                    <h4 class="card-title">Need Help?</h4>
                                    <hr>    
                                    <h4 class="card-title">Video Tutorial</h4>
                                    <hr>    
                                    <h4 class="card-title">Helpbook</h4>
                                    <hr> 
                                   <!--  <p>Click on the box to see the list view </p>  -->
                                </div>
                                
                            </div>
                      </div>
                </div>
                    
            </div>

         </div>
    </div>
@endsection