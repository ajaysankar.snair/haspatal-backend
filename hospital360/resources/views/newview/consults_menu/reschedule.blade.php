@extends('layouts.nheader')
@section('title', 'To Complete')
@section('content')
<style type="text/css">
    /*.card:hover {
        color: white;
  background-color: red;
}*/
.card{
  height: auto; 
  border-radius: 20px; 
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
}
.card-title{
    margin-top: 5%;
}
.bio{
    border-radius: 10px !important;
    border: 1px solid red;
    height: 75px;
}
.btn-danger {
    color: #fff;
    background-color: red;
    border-color: red;
}
/*a:hover {
    color: white !important;
    text-decoration: none;
}*/
hr {
    margin-top: 1rem;
    margin-bottom: 1rem;
    border: 0;
    border-top: 1px solid red;
}
.alert-success {
    color: white;
    background-color: green;
    border-color: #cdedd8;
}
.parsley-required{
    color: red;
}
</style>
<!-- <ol class="breadcrumb">
        <li class="breadcrumb-item">Doctors</li>
    </ol> -->
    <div class="container-fluid" style="margin-top: 3%;">
        <div class="animated fadeIn">
             <div class="container">
                <div class="row">
                      <div class="col-sm-12">
                         <div class="card newcard  border-danger">
                            <div class="card-block" style="margin-top: -4%;">
                                <h3 class="card-title" style="margin-left: 2%;">Reschedule Booking</h3>
                                <div class="row" style="margin-left: 1%;margin-right: 1%;margin-top: 5%;">
                                   <div class="col-sm-2">

                                    <div class="text-center">
                                    <h5>Token</h5>
                                    </div>
                                    </div>

                                    <div class="col-sm-4">
                                     <div class="text-center">
                                    <h5>Patient Name</h5>
                                    </div>
                                    </div>

                                    <div class="col-sm-3">
                                     <div class="text-center">
                                    <h5>Booked Slot</h5>
                                    </div>
                                    </div>

                                    <div class="col-sm-3">
                                     <div class="text-center">
                                    <h5>Waiting Since</h5>
                                    </div>
                                    </div>

                                </div>
                                @if (!$patients->isEmpty())
                                @foreach($patients as $value)
                                <div class="row bio" style="margin-left: 1%;margin-right: 1%;margin-top: 1%;">
                                    <div class="col-sm-2">

                                      <div class="text-center" style="margin-top: 10%;">
                                       <h1 style="color: red;">{{$value->id}}</h1> 
                                      </div>
                                    </div>
                                    <div class="col-sm-4">

                                      <div class="text-center" style="margin-top: 3%;">
                                        <h4 style="color: red;">{{$value->first_name}} {{$value->lastname}}</h4>
                                        <p>Booking ID <span style="color: red;"> {{$value->book_id}}</span> </p>
                                      </div>
                                    </div>

                                    <div class="col-sm-3">

                                      <div class="text-center" style="margin-top: 10%;">
                                        <?php $p_time =  explode(' ',$value->date_time ) ?>
                                        <h4><?php echo $p_time['1']; ?></h4>
                                        </div>
                                    </div>

                                    <div class="col-sm-3">

                                      <div class="text-center" style="margin-top: 10%;">
                                        <h4 style="color: red;">{{$value->waiting_time}} Minutes</h4>
                                        </div>
                                    </div>

                                    <br><br>
                                </div>
                                @endforeach
                                 @else
                                 <h3 class="text-center" style="margin-top: 5%;"> No Records Found</h3>
                                 @endif

                                <div style="margin-top: 5%;margin-bottom: 5%;">
                                  
                                </div>
                            </div>
                            
                        </div>
                      </div>
                </div>
                    
            </div>

         </div>
    </div>
@endsection