@extends('layouts.nheader')
@section('title', 'Doctor Availability')
@section('content')
<style type="text/css">
    /*.card:hover {
        color: white;
  background-color: red;
}*/
.card{
  height: auto; 
  border-radius: 20px; 
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
}
.card-title{
    margin-top: 5%;
}
.bio{
    border-radius: 17px !important;
    border: 1px solid red;
    margin-left: 5%;
    margin-right: 5%;
    height: 230px;
}
.btn-danger {
    color: #fff;
    background-color: red;
    border-color: red;
}
/*a:hover {
    color: white !important;
    text-decoration: none;
}*/
hr {
    margin-top: 1rem;
    margin-bottom: 1rem;
    border: 0;
    border-top: 1px solid red;
}
.form-control {
    border: 1px solid red;
}
label {
    font-size: 15px;
    margin-top: 1%;
}
.alert-success {
    color: white;
    background-color: green;
    border-color: #cdedd8;
}
.parsley-required{
    color: red;
}
</style>
<!-- <ol class="breadcrumb">
        <li class="breadcrumb-item">Doctors</li>
    </ol> -->
    {!! Form::open(['route' => 'dt_doctor_availability.store','id' => 'myForm']) !!}
    <div class="container-fluid" style="margin-top: 3%;">
        <div class="animated fadeIn">
             <div class="container">
              @include('flash::message')
                <div class="row">
                      <div class="col-sm-12">
                         <div class="card newcard  border-danger">
                            <div class="card-block" style="margin-top: -4%;">
                                <h3 class="card-title text-center">Select non availability day</h3>
                                <div class="row" style="margin-left: 1%;margin-right: 1%;">
                                    <div class="col-sm-4">
                                      {!! Form::label('doctor_id', 'Doctor Name:') !!}
                                          <input type="text" name="" class="form-control" value = "{{Auth::user()->first_name}}" readonly="readonly">
                                          <input type = "hidden" id="doctor_id" name = "doctor_id" value = "{{Auth::user()->userDetailsId}}">
<!-- 
                                          <div style="margin-top: 10%;">
                                             <h4>Morning Time:</h4> <br>
                                             <h4>Afternoon Time:</h4> <br>
                                             <h4>Evening Time:</h4> <br>
                                          </div> -->
                                    </div>
                                    <div class="col-sm-4">
                                    {!! Form::label('date', 'Date:') !!}
                                    {!! Form::text('date', null, ['class' => 'form-control','id'=>'timeSlot','data-parsley-required-message' =>'Date is required','required']) !!}

                                    <!-- {!! Form::label('from_time', 'From Time:') !!}
                                    {!! Form::text('mrg_from_time', null, ['class' => 'form-control','id'=>'mrg_from_time','data-parsley-required-message' =>'From Time is required','required']) !!}

                                    {!! Form::label('from_time', 'From Time:') !!}
                                    {!! Form::text('aft_from_time', null, ['class' => 'form-control','id'=>'aft_from_time','data-parsley-required-message' =>'From Time is required','required']) !!}

                                    {!! Form::label('from_time', 'From Time:') !!}
                                    {!! Form::text('eve_from_time', null, ['class' => 'form-control','id'=>'eve_from_time','data-parsley-required-message' =>'From Time is required','required']) !!} -->
                                    </div>
  <!--                                     <div class="col-sm-4">
                                      {!! Form::label('date', 'Booking Time In Minute:') !!}
                                      {!! Form::text('booking_time_slot', null, ['class' => 'form-control','data-parsley-required-message' =>'Booking Time In Minute is required','data-parsley-type' =>'digits','required']) !!}

                                       {!! Form::label('to_time', 'To Time:') !!}
                                        {!! Form::text('mrg_to_time', null, ['class' => 'form-control','id'=>'mrg_to_time','data-parsley-required-message' =>'To Time is required','required']) !!}


                                        {!! Form::label('to_time', 'To Time:') !!}
                                        {!! Form::text('aft_to_time', null, ['class' => 'form-control','id'=>'aft_to_time','data-parsley-required-message' =>'To Time is required','required']) !!}

                                        {!! Form::label('to_time', 'To Time:') !!}
                                        {!! Form::text('eve_to_time', null, ['class' => 'form-control','id'=>'eve_to_time','data-parsley-required-message' =>'To Time is required','required']) !!}
                                      </div> -->

                                 <div class="col-sm-4">
                                     <button class="btn btn-danger" type="submit" style="width: 84%;margin-top: 10%">Submit</button>
                                  </div>
                                </div>
                                <div style="margin-bottom:5%;">
                                  
                                </div>
                                <!-- <div class="text-center" style="margin-top: -3%;margin-bottom: 4%;margin-left: 73%;">
                                  <button class="btn btn-danger" type="submit" style="width: 84%;">Submit</button>
                                </div> -->
                            </div>
                            
                        </div>
                      </div>

                </div>
                    
            </div>

         </div>
    </div>
    {!! Form::close() !!}

     @push('scripts')
   <script type="text/javascript">
           $('#timeSlot').datetimepicker({
               format: 'YYYY-MM-DD',
                minDate:new Date(),
               useCurrent: true,
               icons: {
                   up: "icon-arrow-up-circle icons font-2xl",
                   down: "icon-arrow-down-circle icons font-2xl"
               },
               sideBySide: true
           })
       </script>

       <script type="text/javascript">
           $('#mrg_from_time').datetimepicker({
               format: 'HH:mm:ss',
               useCurrent: true,
               icons: {
                   up: "icon-arrow-up-circle icons font-2xl",
                   down: "icon-arrow-down-circle icons font-2xl"
               },
               sideBySide: true
           })
       </script>

       <script type="text/javascript">
           $('#aft_from_time').datetimepicker({
               format: 'HH:mm:ss',
               useCurrent: true,
               icons: {
                   up: "icon-arrow-up-circle icons font-2xl",
                   down: "icon-arrow-down-circle icons font-2xl"
               },
               sideBySide: true
           })
       </script>


       <script type="text/javascript">
           $('#eve_from_time').datetimepicker({
               format: 'HH:mm:ss',
               useCurrent: true,
               icons: {
                   up: "icon-arrow-up-circle icons font-2xl",
                   down: "icon-arrow-down-circle icons font-2xl"
               },
               sideBySide: true
           })
       </script>

       <script type="text/javascript">
           $('#mrg_to_time').datetimepicker({
               format: 'HH:mm:ss',
               useCurrent: true,
               icons: {
                   up: "icon-arrow-up-circle icons font-2xl",
                   down: "icon-arrow-down-circle icons font-2xl"
               },
               sideBySide: true
           })
       </script>

       <script type="text/javascript">
           $('#aft_to_time').datetimepicker({
               format: 'HH:mm:ss',
               useCurrent: true,
               icons: {
                   up: "icon-arrow-up-circle icons font-2xl",
                   down: "icon-arrow-down-circle icons font-2xl"
               },
               sideBySide: true
           })
       </script>

       <script type="text/javascript">
           $('#eve_to_time').datetimepicker({
               format: 'HH:mm:ss',
               useCurrent: true,
               icons: {
                   up: "icon-arrow-up-circle icons font-2xl",
                   down: "icon-arrow-down-circle icons font-2xl"
               },
               sideBySide: true
           })
       </script>
@endpush

@endsection

