@extends('layouts.new.header')
@section('title', 'Dr Set Price')
@section('content')
<style type="text/css">
    /*.card:hover {
        color: white;
  background-color: red;
}*/
.card{
  height: auto; 
  border-radius: 15px; 
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
}
.card-title{
    margin-top: 5%;
}
.bio{
    border-radius: 17px !important;
    border: 1px solid red;
    margin-left: 5%;
    margin-right: 5%;
    height: 230px;
}
.btn-danger {
    color: #fff;
    background-color: red;
    border-color: red;
}
/*a:hover {
    color: white !important;
    text-decoration: none;
}*/
hr {
    margin-top: 1rem;
    margin-bottom: 1rem;
    border: 0;
    border-top: 1px solid red;
}
.form-control {
   box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
}
label {
    font-size: 15px;
    margin-top: 1%;
}
.pic{
    height: 84px;
    border-radius: 17px !important;
    border: 1px solid red;
}
.alert-success {
    color: white;
    background-color: green;
    border-color: #cdedd8;
}
.parsley-required{
    color: red;
}

.btn2 {
    color: black;
    background-color: white;
    border-color: red;
}

</style>
<!-- <ol class="breadcrumb">
        <li class="breadcrumb-item">Doctors</li>
    </ol> -->
    {!! Form::model($doctors, ['route' => ['doctorsBio.update', $doctors->id], 'method' => 'post', 'files' => true,'id' => 'myForm']) !!}
    <div class="container-fluid">
        <div class="animated fadeIn">
             <div class="container">
              @include('flash::message')
                <div class="row">
                      <div class="col-sm-8">
<!--                          <div class="row" style="margin-left: 1%;margin-right: 1%;margin-top: 2%;">
                                   <div class="col-sm-6">

                                    <div class="text-center">
                                    {!! Form::text('search', null, ['class' => 'form-control']) !!}
                                    </div>
                                    </div>

                                    <div class="col-sm-2">
                                     <div class="text-center">
                                     <button class="btn btn2" type="button" style="width: 100%;">From</button>
                                    </div>
                                    </div>

                                    <div class="col-sm-2">
                                     <div class="text-center">
                                    <button class="btn btn2" type="button" style="width: 100%;">To</button>
                                    </div>
                                    </div>

                                    <div class="col-sm-2">
                                     <div class="text-center">
                                    <button class="btn btn2" type="button" style="width: 100%;">Search</button>
                                    </div>
                                    </div>

                                </div> -->
                                <h1 class="text-center" style="margin-top: 5%;"> Set Price</h1>
                                <div class="row " style="margin-left: 1%;margin-right: 1%;margin-top: 8%;border: 1px solid #b2b2b2;box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);">
                                    <div class="col-sm-12" style="padding: 5%;">
                                        
                                          <!-- <label for="main_mobile">Service</label>
                                            <select class="form-control" data-parsley-required-message="State is required" required="" id="state" name="state"><option value="" selected="selected">Select Service</option><option value="1">1</option><option value="2">2</option><option value="3">3</option></select>
                                              -->
                                        <label for="main_mobile"></label>
                                        <!-- {!! Form::text('second_mobile', null, ['class' => 'form-control','data-parsley-required-message' =>'Moblie 2 is required','required']) !!} -->
                                       <input type="text" class="form-control" name="" value="<?php echo $doctors->price; ?>" readonly></br>
                                       
                                       <input type="text" class="form-control" name="" value="<?php echo $admin_price->book_price; ?>" readonly>
                                        <label for="price"></label>
                                        
                                        {!! Form::text('price', null, ['class' => 'form-control','data-parsley-required-message' =>'Price is required','required','placeholder' => 'Enter New Prices']) !!}
                                        
                                    </div>
                                </div>
                                <h5 class="text-center">New Price shall not be applicable on previously booked consults</h5>
                                <div class="text-center" style="margin-top: 5%;margin-left: 80%;">
                                  <button class="btn btn-danger" type="submit" style="width: 90%;">Submit</button>
                                </div>
                              </div>
                                @include('newview.new.sidemenu.wallet')
                </div>
                    
            </div>

         </div>
    </div>
    {!! Form::close() !!}
@endsection