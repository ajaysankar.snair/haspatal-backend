     
                      <div class="col-sm-4 rowpadding">
                            <div class="card newcard text-center border-danger" style="height: 60px;">
                                            
                                  <div class="card-block">
                                        <p class="card-title text-center" style="font-size: 25px;margin-top: 2%;">{{ Auth::user()->first_name }} {{ Auth::user()->last_name }}</p>
                                  </div>
                                            
                              </div>
                        <h3 class="card-title text-center"> <span style="color: red"><a href="{{ url('/homepage') }}" style="color: red;"><</a></span>&nbsp;&nbsp;&nbsp;&nbsp;<a href="{{ url('/homepage') }}" style="color: black;">My Menu</a></h3>
                        <div class="row" style="margin-top: 5%;">
                              <div class="col-sm-6">  
                                    <div class="card newcard text-center border-danger" style="height: 120px;">
                                         <a href="{{ url('/dr_my_consults_records') }}" style="color: black;">  
                                        <div class="card-block cardpadding" style=" padding: 5px;">
                                            <img class="cardimage" src="{{ URL::asset('public/image/png_Icon/Icone-13.png') }}" style="width: 50%;" alt="logo">
                                        <h4 class="card-title">Consults</h4>
                                            
                                        </div>
                                        </a>
                                    </div>

                                </div>
                                   
                                <div class="col-sm-6">
                                    <div class="card newcard text-center border-danger" style="height: 120px;">
                                        <a href="{{ url('/dr_my_visit_summaries') }}" style="color: black;"> 
                                        <div class="card-block cardpadding" style=" padding: 2px;margin-top: 5%;">
                                             <img class="cardimage" src="{{ URL::asset('public/image/png_Icon/Icone-14.png') }}" style="width: 40%;" alt="logo">
                                           <h4 class="card-title">Visit Summaries</h4>
                                            
                                        </div>
                                        </a>  
                                    </div>

                                </div>
                           </div>    
                          <div class="row">
                                    <div class="col-sm-6">
                                          <div class="card newcard text-center border-danger" style="height: 120px;">
                                            <a href="{{ url('/dr_my_prescriptions') }}" style="color: black;">
                                            <div class="card-block cardpadding" style=" padding: 5px;margin-top: 5%;">
                                                <img class="cardimage" src="{{ URL::asset('public/image/png_Icon/Icone-32.png') }}" style="width: 40%;" alt="logo">
                                                  <h4 class="card-title">Prescriptions</h4>
                                            </div>
                                            </a>
                                         </div>
                                    </div> 
                            </div>
                            
                                @include('newview.new.sidemenu.social')
                      </div>