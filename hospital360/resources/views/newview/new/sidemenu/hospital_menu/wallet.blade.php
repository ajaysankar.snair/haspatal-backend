                      <div class="col-sm-4 rowpadding">
                            <div class="card newcard text-center border-danger" style="height: 60px;">
                                            
                                  <div class="card-block">
                                        <p class="card-title text-center" style="font-size: 25px;margin-top: 2%;">{{ Auth::user()->first_name }} {{ Auth::user()->last_name }}</p>
                                  </div>
                                            
                              </div>
                        <h3 class="card-title text-center"> <span style="color: red"><a href="{{ url('/homepage') }}" style="color: red;"><</a></span>&nbsp;&nbsp;&nbsp;&nbsp;<a href="{{ url('/homepage') }}" style="color: black;">My Menu</a></h3>
                        <div class="row" style="margin-top: 5%;">
                              <div class="col-sm-6">  
                                    <div class="card newcard text-center border-danger" style="height: 120px;">
                                         <a href="{{ url('/h_current_balance') }}" style="color: black;">  
                                        <div class="card-block cardpadding" style=" padding: 2px;">
                                             <img class="cardimage"  src="{{ URL::asset('public/image/png_Icon/Icone-18.png') }}" style="    width: 45%;" alt="logo">
                                        <h4 class="card-title">Current Balance</h4>
                                            
                                        </div>
                                        </a>
                                    </div>

                                </div>
                                   
                                <div class="col-sm-6">
                                    <div class="card newcard text-center border-danger" style="height: 120px;">
                                        <a href="{{ url('/h_add_fund') }}" style="color: black;"> 
                                        <div class="card-block cardpadding" style=" padding: 2px;margin-top: 5%;">
                                             <img class="cardimage"  src="{{ URL::asset('public/image/png_Icon/Icone-43.png') }}" style="    width: 45%;" alt="logo">
                                           <h4 class="card-title">Add Fund</h4>
                                            
                                        </div>
                                        </a>  
                                    </div>

                                </div>
                           </div>    
                          <div class="row">
                                    <div class="col-sm-6">
                                          <div class="card newcard text-center border-danger" style="height: 120px;">
                                            <a href="{{ url('/h_refund') }}" style="color: black;">
                                            <div class="card-block cardpadding" style=" padding: 2px;margin-top: 5%;">
                                                 <img class="cardimage"  src="{{ URL::asset('public/image/png_Icon/Icone-19.png') }}" style="    width: 45%;" alt="logo">
                                                  <h4 class="card-title">Withdraw</h4>
                                            </div>
                                            </a>
                                         </div>
                                    </div>

                                    <div class="col-sm-6">
                                            <div class="card newcard text-center border-danger" style="height: 120px;">
                                                <a href="{{ url('/h_statement') }}" style="color: black;"> 
                                                <div class="card-block cardpadding" style=" padding: 2px;margin-top: 5%;">
                                                    <img class="cardimage"  src="{{ URL::asset('public/image/png_Icon/Icone-22.png') }}" style="    width: 45%;" alt="logo">
                                                <h4 class="card-title">Statement</h4>
                                                    
                                                </div>
                                                </a>
                                            </div>
                                    </div> 
                            </div>
                            <div class="row">
                                    <!-- <div class="col-sm-6">
                                          <div class="card newcard text-center border-danger" style="height: 120px;">
                                           <a href="{{ url('/dr_setprice') }}" style="color: black;">
                                            <div class="card-block cardpadding" style=" padding: 2px;">
                                                <img class="cardimage"  src="{{ URL::asset('public/image/png_Icon/Icone-17.png') }}" style="    width: 45%;" alt="logo">
                                                   <h4 class="card-title">Set Prices</h4>
                                            </div>
                                            </a>
                                         </div>
                                    </div> -->
                                  <div class="col-sm-6">
                                    <div class="card newcard text-center border-danger" style="height: 120px;">
                                        <a href="{{ url('/h_bankinginfo') }}" style="color: black;"> 
                                        <div class="card-block cardpadding" style=" padding: 2px;">
                                                 <img class="cardimage"  src="{{ URL::asset('public/image/png_Icon/Icone-48.png') }}" style="    width: 45%;" alt="logo">
                                            <h4 class="card-title">Banking Info</h4>
                                            
                                        </div>
                                        </a>  
                                    </div>

                                </div>
                            </div>
                            
                           
                      </div>