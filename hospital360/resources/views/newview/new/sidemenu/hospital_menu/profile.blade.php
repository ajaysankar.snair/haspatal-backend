                      <div class="col-sm-4 rowpadding">
                            <div class="card newcard text-center border-danger" style="height: 60px;">
                                            
                                  <div class="card-block">
                                        <p class="card-title text-center" style="font-size: 25px;margin-top: 2%;">{{ Auth::user()->first_name }} {{ Auth::user()->last_name }}</p>
                                  </div>
                                            
                              </div>
                        <h3 class="card-title text-center"> <span style="color: red"><a href="{{ url('/hospital_profile') }}" style="color: red;"><</a></span>&nbsp;&nbsp;&nbsp;&nbsp;<a href="{{ url('/hospital_profile') }}" style="color: black;">My Menu</a></h3>
                        <div class="row" style="margin-top: 5%;">
                              <div class="col-sm-6">  
                                    <div class="card newcard text-center border-danger" style="height: 120px;">
                                        <a href="{{ url('/hasptal_bio') }}" class="{{ Request::is('hasptal_bio*') ? 'active' : '' }}" style="color: black;"> 
                                        <div class="card-block cardpadding" style="padding: 10px;">
                                           <img class="cardimage" src="{{ URL::asset('public/image/png_Icon/Icone-31.png') }}" style="width: 50%;" alt="logo">
                                            <h4 class="card-title">My Bio</h4>
                                            
                                        </div>
                                        </a>
                                    </div>

                                </div>
                                   
                                <div class="col-sm-6">
                                    <div class="card newcard text-center border-danger" style="height: 120px;">
                                        <a href="{{ url('/h_address') }}" style="color: black;">  
                                        <div class="card-block cardpadding" style="padding: 10px;">
                                                <img class="cardimage" src="{{ URL::asset('public/image/png_Icon/Icone-24.png') }}" style="width: 50%;" alt="logo">
                                                <h4 class="card-title">Address</h4>
                                            
                                        </div>
                                        </a>  
                                    </div>

                                </div>
                           </div>    
                          <div class="row">
                                    <div class="col-sm-6">
                                          <div class="card newcard text-center border-danger" style="height: 120px;">
                                            <a href="{{ url('/h_password') }}" style="color: black;"> 
                                            <div class="card-block cardpadding" style="padding: 10px;">
                                                 <img class="cardimage" src="{{ URL::asset('public/image/png_Icon/Icone-24.png') }}" style="width: 50%;" alt="logo">
                                                  <h4 class="card-title">Password</h4>
                                            </div>
                                            </a>
                                         </div>
                                    </div>

                                    <div class="col-sm-6">
                                            <div class="card newcard text-center border-danger" style="height: 120px;">
                                                 <a href="{{ url('/h_contact') }}" style="color: black;"> 
                                                <div class="card-block cardpadding" style="padding: 10px;">
                                                     <img class="cardimage" src="{{ URL::asset('public/image/png_Icon/Icone-23.png') }}" style="width: 50%;" alt="logo">
                                                      <h4 class="card-title">Contact Info</h4>
                                                    
                                                </div>
                                                </a>
                                            </div>
                                    </div> 
                            </div>
                            <div class="row">
                                    <div class="col-sm-6">
                                          <div class="card newcard text-center border-danger" style="height: 120px;">
                                           <a href="{{ url('/h_license') }}" style="color: black;">
                                            <div class="card-block cardpadding" style="padding: 10px;">
                                                 <i class="fa fa-stethoscope" class="cardimage" style=" font-size: 60px;"></i>
                                                <h4 class="card-title">License</h4>
                                            </div>
                                            </a>
                                         </div>
                                    </div>
                                    <div class="col-sm-6">
                                          <div class="card newcard text-center border-danger" style="height: 120px;">
                                            <a href="{{ url('/h_profilepic') }}" style="color: black;">  
                                            <div class="card-block cardpadding" style="padding: 10px;">
                                                 <img class="cardimage" src="{{ URL::asset('public/image/png_Icon/Icone-35.png') }}" style="width: 50%;" alt="logo">
                                                  <h4 class="card-title">Profile Pic</h4>
                                            </div>
                                            </a>
                                         </div>
                                    </div>
                            </div>

                            <!-- <div class="card newcard  border-danger" style="height: 60px;">
                              <div class="card-block">
                                <div class="text-center" style="margin-top: 4%;margin-left: 8%;">
                                  <a href="{{ url('/dr_setting') }}" style="width: 13%;">  <i class="fa fa-cog " style="margin-right:30px;font-size:36px;color:red"></i></a>
                                </div>
                              </div>
                            </div> -->
                            
                            
                      </div>