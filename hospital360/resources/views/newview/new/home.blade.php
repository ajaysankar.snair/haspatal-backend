@extends('layouts.new.header')
@section('title', 'Home')
@section('content')
<style type="text/css">
    /*.card:hover {
        color: white;
  background-color: red;
}*/
.card{
  height: auto; 
  border-radius: 15px; 
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
}
.card-title{
    margin-top: 5%;

}
.bio{
    border-radius: 10px !important;
    border: 1px solid red;
    height: 75px;
}
.btn-danger {
    color: #fff;
    background-color: red;
    border-color: red;
}
/*a:hover {
    color: white !important;
    text-decoration: none;
}*/
hr {
    margin-top: 1rem;
    margin-bottom: 1rem;
    border: 0;
    border-top: 1px solid red;
}
.form-control {
   box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
}
label {
    font-size: 15px;
    margin-top: 1%;
}
.pic{
    height: 84px;
    border-radius: 17px !important;
    border: 1px solid red;
}
.alert-success {
    color: white;
    background-color: green;
    border-color: #cdedd8;
}
.parsley-required{
    color: red;
}

.btn2 {
    color: black;
    background-color: white;
    border-color: red;
}


</style>
</style>
<!-- <ol class="breadcrumb">
        <li class="breadcrumb-item">Doctors</li>
    </ol> -->
   {!! Form::open(['route' => 'homepagedetails', 'files' => true,'id' => 'myForm']) !!} 
    <div class="container-fluid">
        <div class="animated fadeIn">
             <div class="container">
              
                <div class="row">
                      <div class="col-sm-8">
                         <div class="row" style="margin-left: 1%;margin-right: 1%;">
                                   <div class="col-sm-4">

                                    <div class="text-center">
                                     
                                    {!! Form::select('patient_id', [''=>'Select Patient'] + $patient_name->toArray(), null, ['class' => 'form-control','data-parsley-required-message' =>'Patient is required','required']) !!}
                                    </div>
                                    </div>

                                    <div class="col-sm-3">
                                     <div class="text-center">
                                      {!! Form::text('s_date', null, ['class' => 'form-control','id'=>'start_date','data-parsley-required-message' =>'Date is required','required','placeholder' => 'Start Date']) !!}
                                    </div>
                                    </div>

                                    <div class="col-sm-3">
                                     <div class="text-center">
                                      {!! Form::text('e_date', null, ['class' => 'form-control','id'=>'end_date','data-parsley-required-message' =>'Date is required','required','placeholder' => 'End Date']) !!}
                                    </div>
                                    </div>

                                    <div class="col-sm-2">
                                     <div class="text-center">
                                    <button class="btn btn-danger" type="submit" style="width: 100%;">Search</button>
                                    </div>
                                    </div>

                                </div>
                                <h3 class="text-center" style="margin-top: 5%;">My Haspatal Schedule</h3>
                               <div class="row bio " style="margin-left: 1%;margin-right: 1%;margin-top: 3%;border: 1px solid #b2b2b2;box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);">
                                 <div class="col-sm-2" style="padding: 3%;">

                                    <div class="text-center">
                                    <h5>Token</h5>
                                    </div>
                                    </div>

                                    <div class="col-sm-4" style="padding: 3%;">
                                     <div class="text-center">
                                    <h5>Patient Name</h5>
                                    </div>
                                    </div>

                                    <div class="col-sm-2" style="padding: 3%;">
                                     <div class="text-center">
                                    <h5>Booked Slot</h5>
                                    </div>
                                    </div>

                                    <div class="col-sm-2" style="padding: 3%;">
                                     <div class="text-center">
                                    <h5>Waiting Since</h5>
                                    </div>
                                    </div>

                                    <div class="col-sm-2" style="padding: 3%;">
                                     <div class="text-center">
                                    
                                    </div>
                                    </div>
                                </div>
                                @if(!empty($patients_data))
                                 @foreach($patients_data as $value)

                                <div class="row bio" style="margin-left: 1%;margin-right: 1%;margin-top: 3%;border: 1px solid #b2b2b2;box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);">
                                    <div class="col-sm-2">

                                      <div class="text-center" style="margin-top: 10%;">
                                       <h1 style="color: red;">{{$value->id}}</h1> 
                                      </div>
                                    </div>
                                    <div class="col-sm-4">

                                      <div class="text-center" style="margin-top: 3%;">
                                        <h4 style="color: red;">{{$value->first_name}} {{$value->lastname}}</h4>
                                        <p>Booking ID <span style="color: red;"> {{$value->book_id}}</span> </p>
                                      </div>
                                    </div>

                                    <div class="col-sm-2">

                                      <div class="text-center" style="margin-top: 10%;">
                                        <?php $p_time =  explode(' ',$value->date_time ) ?>
                                        <h4><?php echo $p_time['1']; ?></h4>
                                        </div>
                                    </div>

                                    <div class="col-sm-2">

                                      <div class="text-center" style="margin-top: 10%;">
                                        <h4 style="color: red;"> {{$value->waiting_time}} Minutes</h4>
                                        </div>
                                    </div>

                                  <!--   <div class="col-sm-2">

                                      <div class="text-center" style="margin-top: 10%;">
                                        <a href="{{ route('patients_profile',['id' => $value->patient_id,'b_id' => $value->book_id]) }}" class="btn btn-danger active pull-right" role="button" style="width: 90%;">Start</a>
                                        </div>
                                    </div> -->
                                    <br><br>
                                </div>
                                @endforeach
                                @endif
                               
                              </div>
                                @include('newview.new.sidemenu.home')
                </div>
                    
            </div>

         </div>
    </div>
    {!! Form::close() !!}
@endsection

@push('scripts')
   <script type="text/javascript">
           $('#start_date').datetimepicker({
               format: 'DD-MMM-YYYY',
               useCurrent: true,
               icons: {
                   up: "icon-arrow-up-circle icons font-2xl",
                   down: "icon-arrow-down-circle icons font-2xl"
               },
               sideBySide: true
           })
       </script>
       <script type="text/javascript">
           $('#end_date').datetimepicker({
               format: 'DD-MMM-YYYY',
               useCurrent: true,
               icons: {
                   up: "icon-arrow-up-circle icons font-2xl",
                   down: "icon-arrow-down-circle icons font-2xl"
               },
               sideBySide: true
           })
       </script>
@endpush