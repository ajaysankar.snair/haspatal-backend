@extends('layouts.new.header')
@section('title', 'Patients Queue')
@section('content')
<meta name="_token" content="{{ app('Illuminate\Encryption\Encrypter')->encrypt(csrf_token()) }}" />
<style type="text/css">
    /*.card:hover {
        color: white;
  background-color: red;
}*/
.card{
  height: auto; 
  border-radius: 20px; 
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
}
.card-title{
    margin-top: 5%;
}
.bio{
    border-radius: 10px !important;
    border: 1px solid #b2b2b2;box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
    height: 375px;
}
.btn-danger {
    color: #fff;
    background-color: red;
    border-color: red;
}
.btn2 {
    color: black;
    background-color: white;
    border-color: red;
}
.form-control {
   box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
}
/*a:hover {
    color: white !important;
    text-decoration: none;
}*/
hr {
    margin-top: -1rem;
    margin-bottom: 1rem;
    border: 0;
    border-top: 1px solid red;
}
.alert-success {
    color: white;
    background-color: green;
    border-color: #cdedd8;
}
.parsley-required{
    color: red;
}
.cmodel{
  margin-left: 17%;
      margin-top: 225px;
}
.modal-backdrop.show {
    opacity: .0;
}
.modal-content {
  border-radius: 10px !important;
    border: 1px solid red;
}
input[type=checkbox], input[type=radio] {
    box-sizing: border-box;
    padding: 0;
    width: 30px;
    height: 30px;
}
.btn2 {
    color: black;
    background-color: white;
   border: 1px solid #b2b2b2;box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
  border-radius: 10%;
}

</style>
<!-- <ol class="breadcrumb">
        <li class="breadcrumb-item">Doctors</li>
    </ol> -->
    
    <div class="container-fluid">
        <div class="animated fadeIn">
             <div class="container">
              @include('flash::message')
              <div class="row">
                    <div class="col-sm-8">
                      <div class="row" style="margin-left: 1%;margin-right: 1%;margin-top: 2%;">
                                   <div class="col-sm-6">

                                    <div class="text-center">
                                    {!! Form::text('search', null, ['class' => 'form-control']) !!}
                                    </div>
                                    </div>

                                    <div class="col-sm-2">
                                     <div class="text-center">
                                     <button class="btn btn2" type="button" style="width: 100%;">From</button>
                                    </div>
                                    </div>

                                    <div class="col-sm-2">
                                     <div class="text-center">
                                    <button class="btn btn2" type="button" style="width: 100%;">To</button>
                                    </div>
                                    </div>

                                    <div class="col-sm-2">
                                     <div class="text-center">
                                    <button class="btn btn2" type="button" style="width: 100%;">Search</button>
                                    </div>
                                    </div>

                                </div>
                      </div>
                      <div class="col-sm-4 rowpadding">
                          <div class="card newcard text-center border-danger" style="height: 60px;">
                                            
                                  <div class="card-block">
                                        <p class="card-title text-center" style="font-size: 25px;margin-top: 2%;color: red">{{$detail->first_name}} {{$detail->last_name}}</p>
                                  </div>
                                            
                              </div>

                              <form id="done1">
                                    <input type="hidden" name="_token" id="csrf" value="{{Session::token()}}">
                                     <input type = "hidden" id="doctor_name" name = "doctor_name" value = "{{Auth::user()->userDetailsId}}">

                                    <input type = "hidden" id="patient_name" name = "patient_name" value = "{{$detail->id}}">
                                   <input type = "hidden" id="book_id" name = "book_id" value = "{{$book_id_data->book_id}}">
                                    <a class="btn btn2 active" id="done1" role="button" style="width: 30%;margin-left: 103%;    margin-top: -110px;"><img class="cardimage"  src="{{ URL::asset('public/image/png_Icon/Icone-29.png') }}" style="width: 50%;" alt="logo"><br>Checkout</a>
                          </form>
                      </div>
                </div>      
                <div class="row">
                      <div class="col-sm-12">
                         <div class="card newcard  border-danger" style="width: 120%;margin-left: -10%;">
                            <div class="card-block" style="margin-top: -4%;">
                               <div class="row">
                                       <div class="col-sm-12" style="padding-left: 10%;">
                                        <div class="" style="margin-top: 5%;">
                                          <a href="#" class="btn " style="margin-left: -10%;margin-right: 5%; color: red;font-weight: 600;">Available <br>Patient information</a>
                                          
                                           <a href="#" class="btn btn2 active" data-toggle="modal" data-target="#myModal" role="button" style="width: 10%;margin-left: -1%;"><img class="cardimage"  src="{{ URL::asset('public/image/png_Icon/Icone-41.png') }}" style="width: 50%;" alt="logo"><br> Profile</a>

                                            <a href="#" class="btn btn2 active" data-toggle="modal" data-target="#myModal1" role="button" style="width: 10%;margin-left: 2%;">Complaint</a>
                                            <a href="#" class="btn btn2 active" data-toggle="modal" data-target="#myModal8" role="button" style="width: 10%;margin-left: 2%;">Allergies</a>

                                               <a href="#" class="btn btn2 active" data-toggle="modal" data-target="#myModal6" role="button" style="width: 10%;margin-left: 2%;"><img class="cardimage"  src="{{ URL::asset('public/image/png_Icon/Icone-13.png') }}" style="width: 50%;" alt="logo"><br>Consultants</a>
                                            
                                              <a href="#" class="btn btn2 active" data-toggle="modal" data-target="#myModal5" role="button" style="width: 15%;margin-left: 2%;">Lab Report</a>

                                            <a href="#" class="btn btn2 active" data-toggle="modal" data-target="#myModal3" role="button" style="width: 13%;margin-left: 2%;">Medications</a>

                                              <a href="#" class="btn btn2 active" data-toggle="modal" data-target="#myModal13" role="button" style="width: 10%;margin-left: 2%;"><img class="cardimage"  src="{{ URL::asset('public/image/png_Icon/Icone-31.png') }}" style="width: 50%;" alt="logo"><br>Notes</a>


                                        </div>
                                       </div>
                                        <!-- <div class="col-sm-6">
                                           <h5 style="margin-left: 3%;color: red">{{$detail->first_name}} {{$detail->last_name}}</h5>
                                        </div>
                                        <div class="col-sm-6">
                                            <button class="btn btn-success text-center" onclick="video_call()" id="" style="width: 38%;margin-left: 60%;margin-top: -4%;" >Call</button>
                                        </div> -->
                                      </div>
                                <div class="row" style="margin-left: 1%;margin-right: 1%;margin-top: 5%;">
                                   <div class="col-sm-6">

                                    <div class="text-center bio" id="callScreen" style="margin-top: -7%;">                                   
                                      <button class="btn btn-success text-center" id="hidebutton" onclick="video_call()" id="" style="width: 38%;margin-left: 5%;margin-top: 28%;" >Call</button>
                                    </div>
                                    </div>
                                   
                                    <div class="col-sm-6">
                                      <div class="" style="margin-top: -7%;">
                                    <h1 class="text-center" style="margin-top: 10%;"> View information <br>and Action Area </h1>            
                                      </div>
                                    </div>
                                  


                                </div>
                                    <div class="row">
                                       <div class="col-sm-12" style="">
                                        <div class="text-center" style="margin-top: 2%;margin-bottom: 1%;">
                                          <a href="#" class="btn " style="margin-left: -2%;margin-right: 6%; color: red;font-weight: 600;">Actions Support <br>For Doctor</a>
                                            <a href="#" class="btn btn2 active" data-toggle="modal" data-target="#myModal7" role="button" style="width: 10%;margin-left: -1%;">HPI</a>

                                             <a href="#" class="btn btn2 active" data-toggle="modal" data-target="#myModal2" role="button" style="width: 10%;margin-left: 2%;"><img class="cardimage"  src="{{ URL::asset('public/image/png_Icon/Icone-14.png') }}" style="width: 50%;" alt="logo"><br>Treatment</a>

                                              <a href="#" class="btn btn2 active" data-toggle="modal" data-target="#myModal12" role="button" style="width: 10%;margin-left: 2%;"><img class="cardimage"  src="{{ URL::asset('public/image/png_Icon/Icone-32.png') }}" style="width: 50%;" alt="logo"><br>Prescription</a>

                                            <a href="#" class="btn btn2 active" data-toggle="modal" data-target="#myModal10" role="button" style="width: 10%;margin-left: 2%;">Labs</a>
                                             <a href="#" class="btn btn2 active" data-toggle="modal" data-target="#myModal9" role="button" style="width: 10%;margin-left: 2%;">Imagings</a>

                                            <a href="#" class="btn btn2 active" data-toggle="modal" data-target="#myModal11" role="button" style="width: 10%;margin-left: 2%;">Refer</a>
                                             <a href="#" class="btn btn2 active" data-toggle="modal" data-target="#myModal4" role="button" style="width: 10%;margin-left: 2%;">Other</a>

                                        </div>
                                       </div>
                                        <!-- <div class="col-sm-6">
                                           <h5 style="margin-left: 3%;color: red">{{$detail->first_name}} {{$detail->last_name}}</h5>
                                        </div>
                                        <div class="col-sm-6">
                                            <button class="btn btn-success text-center" onclick="video_call()" id="" style="width: 38%;margin-left: 60%;margin-top: -4%;" >Call</button>
                                        </div> -->
                                      </div>
                            </div>
                            
                        </div>
                      </div>
                </div>
                    
            </div>

         </div>
    </div>

<!-- Patient Profile Model Start -->
  <div class="modal fade cmodel" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">      
        <div class="modal-body">
          <div class="row">
             <div class="col-sm-8">
              <h2 style="color: red;"> Patient Profile</h2>
              <div class="row" style="margin-top: 7%;">
              <div class="col-sm-6">
                <h6>Name: {{$detail->first_name}} {{$detail->last_name}}</h6>
                <h6>Age:</h6>
                <h6>Gender:</h6>
              </div>
              <div class="col-sm-2">
                <h6>Height:</h6>
                <h6>weight:</h6>
                <h6>LMP:</h6>

              </div>
            </div>
             </div>
             <div class="col-sm-4">
              <label for="vehicle1" style="font-size: 20px;">Medical&nbsp;</label>
              <input type="checkbox" id="Medical" name="check" onclick="onlyOne(this)" value="Bike"><br>
              <label for="vehicle2" style="font-size: 20px;">Personal&nbsp;</label>
              <input type="checkbox" checked="checked" id="Personal" name="check" onclick="onlyOne(this)" value="Car"><br>
              <label for="vehicle3" style="font-size: 20px;">Family&nbsp;&nbsp;&nbsp;&nbsp;</label>
              <input type="checkbox" id="Family" name="check" onclick="onlyOne(this)" value="Boat"><br><br>
             </div>

          </div>
          <hr>
            <div id="Medical1" style="display: none">
             {{$detail->work_profile}} 
            </div>
            <div id="Personal1" style="display: none">
            {{$detail->personal_profile}}
            </div>
            <div id="Family1" style="display: none">
             {{$detail->family_profile}}
            </div>
        </div>
      </div>
      
    </div>
  </div>
  <!-- Patient Profile Model End -->


  <!-- Present Complaint Model Start -->
  <div class="modal fade cmodel" id="myModal1" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">      
        <div class="modal-body">
           <h2 style="color: red;"> Present Complaint</h2>
          <div style="border-radius: 10px !important;border: 1px solid red;height: 283px;">
            <?php if(!empty($complaint_patient)){ ?>
            <p><?php  echo $complaint_patient->present_complaint; ?></p>
            <?php $pic = env('APP_URL').'public/media/complaint_pic/'.$complaint_patient->complaint_pic; ?>
            <!--  <a type="button" class="btn btn-success text-center" style="margin-top: 43%;margin-left: 2%;" href="<? echo $pic; ?>" target="_blank">Complaint Pic</a> -->
            
                <a href="{{ route('patients_complaint',['id' =>  $complaint_patient->id,'b_id' => $complaint_patient->book_id]) }}" target="_blank" class="btn btn-success text-center" role="button" style="margin-top: 43%;margin-left: 2%;">Complaint Pic</a>
           
           <?php } ?>
          </div>
          <div class="text-center" style="margin-top: 3%;">
                <a href="#" class="btn btn-danger active" role="button" data-dismiss="modal" style="width: 20%;">Done</a>
          </div>
        </div>
      </div>
      
    </div>
  </div>
  <!-- Present Complaint Model End -->
<?php  
    $a = Auth::user()->userDetailsId;
   //echo "<pre>";  print_r($detail); exit();
    ?>
  <!-- Key Points Model Start -->
  <div class="modal fade cmodel" id="myModal2" role="dialog">
    <div class="modal-dialog">
      <form id="myForm1">
      <!-- Modal content-->
      <div class="modal-content">      
        <div class="modal-body">
           <h2 style="color: red;">Treatment</h2>
          <div style="border-radius: 10px !important;border: 1px solid red;height: 283px;">
            
            {!! Form::textarea('key_point', null, ['class' => 'form-control','rows' => '12','data-parsley-required-message' =>'Key Point is required','required' , 'id'=>'key_point']) !!}
             <input type="hidden" name="_token" id="csrf" value="{{Session::token()}}">
            <input type = "hidden" id="doctor_id" name = "doctor_id" value = "{{Auth::user()->userDetailsId}}">
              <input type ="hidden" id="k_book_id" name="k_book_id" value="{{$book_id_data->book_id}}">
            <input type = "hidden" id="patient_id" name = "patient_id" value = "{{$detail->id}}">
          </div>
          <div class="text-center" style="margin-top: 3%;">
            <button class="btn btn-danger" type="button" id="submitdd" data-dismiss="modal" style="width: 20%;">Done</button>
                <!-- <a href="#" class="btn btn-danger active" role="button" data-dismiss="modal" style="width: 20%;">Done</a> -->
          </div>
        </div>
      </div>
       </form>
    </div>
  </div>
  <!-- Key Points Model End -->

  <!-- Present Medications Model Start -->
  <div class="modal fade cmodel" id="myModal3" role="dialog">
    <div class="modal-dialog">
     <form id="myForm3">
      <!-- Modal content-->
      <div class="modal-content">      
        <div class="modal-body">
           <h2 style="color: red;">Medications</h2>
          <div style="border-radius: 10px !important;border: 1px solid red;height: 283px;">
            {!! Form::textarea('present_medications', null, ['class' => 'form-control','rows' => '12','data-parsley-required-message' =>'Key Point is required','required','id'=>'present_medications']) !!}
            <input type="hidden" name="_token" id="csrf" value="{{Session::token()}}">
            <input type = "hidden" id="doctor_id" name = "doctor_id" value = "{{Auth::user()->userDetailsId}}">

            <input type = "hidden" id="patient_id" name = "patient_id" value = "{{$detail->id}}">
            <input type = "hidden" id="m_book_id" name = "m_book_id" value = "{{$book_id_data->book_id}}">
          </div>
          <div class="text-center" style="margin-top: 3%;">
                <button class="btn btn-danger" type="button" id="submit3" data-dismiss="modal" style="width: 20%;">Done</button>
          </div>
        </div>
      </div>
      </form>
    </div>
  </div>
  <!-- Present Medications Model End -->

  <!-- Examination Notes Model Start -->
  <div class="modal fade cmodel" id="myModal4" role="dialog">
    <div class="modal-dialog">
    <form id="myForm4">
      <!-- Modal content-->
      <div class="modal-content">      
        <div class="modal-body">
           <h2 style="color: red;">Other</h2>
          <div style="border-radius: 10px !important;border: 1px solid red;height: 283px;">
            {!! Form::textarea('examination_notes', null, ['class' => 'form-control','rows' => '12','data-parsley-required-message' =>'Key Point is required','required','id'=>'examination_notes']) !!}
            <input type="hidden" name="_token" id="csrf" value="{{Session::token()}}">
            <input type = "hidden" id="e_doctor_id" name = "e_doctor_id" value = "{{Auth::user()->userDetailsId}}">

            <input type = "hidden" id="e_patient_id" name = "e_patient_id" value = "{{$detail->id}}">
            <input type = "hidden" id="e_book_id" name = "e_book_id" value = "{{$book_id_data->book_id}}">
          </div>
          <div class="text-center" style="margin-top: 3%;">
                 <button class="btn btn-danger" type="button" id="submit4" data-dismiss="modal" style="width: 20%;">Done</button>
          </div>
        </div>
      </div>
       </form>
    </div>
  </div>
  <!-- Examination Notes Model End -->

  <!-- Available Lab Findings Model Start -->
  <div class="modal fade cmodel" id="myModal5" role="dialog">
    <div class="modal-dialog">
    <form id="myForm5">
      <!-- Modal content-->
      <div class="modal-content">      
        <div class="modal-body">
           <h2 style="color: red;">Lab Report</h2>
          <div style="border-radius: 10px !important;border: 1px solid red;height: 283px;">
            {!! Form::textarea('available_lab', null, ['class' => 'form-control','rows' => '12','data-parsley-required-message' =>'Key Point is required','required','id'=>'available_lab']) !!}
            <input type="hidden" name="_token" id="csrf" value="{{Session::token()}}">
            <input type = "hidden" id="a_doctor_id" name = "a_doctor_id" value = "{{Auth::user()->userDetailsId}}">
            <input type = "hidden" id="a_patient_id" name = "a_patient_id" value = "{{$detail->id}}">
            <input type = "hidden" id="a_book_id" name = "a_book_id" value = "{{$book_id_data->book_id}}">
          </div>
          <div class="text-center" style="margin-top: 3%;">
                <button class="btn btn-danger" type="button" id="submit5" data-dismiss="modal" style="width: 20%;">Done</button>
          </div>
        </div>
      </div>
      </form>
    </div>
  </div>
  <!-- Available Lab Findings Model End -->

  <!-- Available Summaries  Model Start -->
  <div class="modal fade cmodel" id="myModal6" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">      
        <div class="modal-body">
           <h2 style="color: red;">Consultants</h2>
          <div style="border-radius: 10px !important;border: 1px solid red;height: 283px;">
           <!--   @foreach($my_note_patient as $value)
              <h3 class="text-center">Visit Summaries - {{$value->created_at}}</h3>

             @endforeach -->
            <!-- <h3 class="text-center">Visit Summaries - 12 May 2020</h3><br>
             <h3 class="text-center">Visit Summaries - 12 May 2020</h3><br>
              <h3 class="text-center">Visit Summaries - 12 May 2020</h3><br>
               <h3 class="text-center">Visit Summaries - 12 May 2020</h3><br>
                <h3 class="text-center">Visit Summaries - 12 May 2020</h3><br> -->
          </div>
          <div class="text-center" style="margin-top: 3%;">
                <a href="#" class="btn btn-danger active" role="button" data-dismiss="modal" style="width: 20%;">Done</a>
          </div>
        </div>
      </div>
      
    </div>
  </div>
  <!-- Available Summaries  Model End -->

  <!-- Perceived / Diagnosis Model Start -->
  <div class="modal fade cmodel" id="myModal7" role="dialog">
    <div class="modal-dialog">
   <form id="myForm6">
      <!-- Modal content-->
      <div class="modal-content">      
        <div class="modal-body">
           <h2 style="color: red;">HPI</h2>
          <div style="border-radius: 10px !important;border: 1px solid red;height: 283px;">
            {!! Form::textarea('perceived', null, ['class' => 'form-control','rows' => '12','data-parsley-required-message' =>'Key Point is required','required','id'=>'perceived']) !!}
            <input type="hidden" name="_token" id="csrf" value="{{Session::token()}}">
            <input type = "hidden" id="p_doctor_id"  name = "p_doctor_id" value = "{{Auth::user()->userDetailsId}}">

            <input type = "hidden" id="p_patient_id" name = "p_patient_id" value = "{{$detail->id}}">
            <input type = "hidden" id="p_book_id" name = "p_book_id" value = "{{$book_id_data->book_id}}">
          </div>
          <div class="text-center" style="margin-top: 3%;">
                <button class="btn btn-danger" type="button" id="submit6" data-dismiss="modal" style="width: 20%;">Done</button>
          </div>
        </div>
      </div>
      </form>
    </div>
  </div>
  <!-- Perceived / Diagnosis Model End -->

  <!--Suggested Investigations Model Start -->
  <div class="modal fade cmodel" id="myModal8" role="dialog">
    <div class="modal-dialog">
    <form id="myForm7">
      <!-- Modal content-->
      <div class="modal-content">      
        <div class="modal-body">
           <h2 style="color: red;">Allergies</h2>
          <div style="border-radius: 10px !important;border: 1px solid red;height: 283px;">
            {!! Form::textarea('suggested_investigations', null, ['class' => 'form-control','rows' => '12','data-parsley-required-message' =>'Key Point is required','required' , 'id'=>'suggested_investigations']) !!}
            <input type="hidden" name="_token" id="csrf" value="{{Session::token()}}">
            <input type = "hidden" id="s_doctor_id" name = "s_doctor_id" value = "{{Auth::user()->userDetailsId}}">

            <input type = "hidden" id="s_patient_id" name = "s_patient_id" value = "{{$detail->id}}">
            <input type = "hidden" id="s_book_id" name = "s_book_id" value = "{{$book_id_data->book_id}}">
          </div>
          <div class="text-center" style="margin-top: 3%;">
                <button class="btn btn-danger" type="button" id="submit7" data-dismiss="modal" style="width: 20%;">Done</button>
          </div>
        </div>
      </div>
       </form>
    </div>
  </div>
  <!--Suggested Investigations Model End -->

  <!--Suggested Imagings Model Start -->
  <div class="modal fade cmodel" id="myModal9" role="dialog">
    <div class="modal-dialog">
    	 <form id="myForm8">
      <!-- Modal content-->
      <div class="modal-content">      
        <div class="modal-body">
           <h2 style="color: red;">Imagings</h2>
			<div style="border-radius: 10px !important;border: 1px solid red;height: 283px;">
            {!! Form::textarea('suggested_imagings', null, ['class' => 'form-control','rows' => '12','data-parsley-required-message' =>'Key Point is required','required' , 'id'=>'suggested_imagings']) !!}
            <input type="hidden" name="_token" id="csrf" value="{{Session::token()}}">
            <input type = "hidden" id="si_doctor_id" name = "si_doctor_id" value = "{{Auth::user()->userDetailsId}}">

            <input type = "hidden" id="si_patient_id" name = "si_patient_id" value = "{{$detail->id}}">
            <input type = "hidden" id="si_book_id" name = "si_book_id" value = "{{$book_id_data->book_id}}">
          </div>
          <div class="text-center" style="margin-top: 3%;">
                <button class="btn btn-danger" type="button" id="submit8" data-dismiss="modal" style="width: 20%;">Done</button>
          </div>
        </div>
      </div>
      </form>
    </div>
  </div>
  <!--Suggested Imagings Model End -->

  <!--Suggested Specialists Model Start -->
  <div class="modal fade cmodel" id="myModal10" role="dialog">
    <div class="modal-dialog">
    <form id="myForm9">
      <!-- Modal content-->
      <div class="modal-content">      
        <div class="modal-body">
           <h2 style="color: red;">Lab</h2>
			<div style="border-radius: 10px !important;border: 1px solid red;height: 283px;">
            {!! Form::textarea('suggested_specialists', null, ['class' => 'form-control','rows' => '12','data-parsley-required-message' =>'Key Point is required','required' , 'id'=>'suggested_specialists']) !!}
            <input type="hidden" name="_token" id="csrf" value="{{Session::token()}}">
            <input type = "hidden" id="ss_doctor_id" name = "ss_doctor_id" value = "{{Auth::user()->userDetailsId}}">

            <input type = "hidden" id="ss_patient_id" name = "ss_patient_id" value = "{{$detail->id}}">
            <input type = "hidden" id="ss_book_id" name = "ss_book_id" value = "{{$book_id_data->book_id}}">
          </div>
          <div class="text-center" style="margin-top: 3%;">
                <button class="btn btn-danger" type="button" id="submit9" data-dismiss="modal" style="width: 20%;">Done</button>
          </div>
        </div>
      </div>
      </form>
    </div>
  </div>
  <!--Suggested Specialists Model End -->

  <!--Suggested Therapies Model Start -->
  <div class="modal fade cmodel" id="myModal11" role="dialog">
    <div class="modal-dialog">
    <form id="myForm10">
      <!-- Modal content-->
      <div class="modal-content">      
        <div class="modal-body">
           <h2 style="color: red;">Refer</h2>
			<div style="border-radius: 10px !important;border: 1px solid red;height: 283px;">
            {!! Form::textarea('suggested_therapies', null, ['class' => 'form-control','rows' => '12','data-parsley-required-message' =>'Key Point is required','required' , 'id'=>'suggested_therapies']) !!}
            <input type="hidden" name="_token" id="csrf" value="{{Session::token()}}">
            <input type = "hidden" id="st_doctor_id" name = "st_doctor_id" value = "{{Auth::user()->userDetailsId}}">

            <input type = "hidden" id="st_patient_id" name = "st_patient_id" value = "{{$detail->id}}">
            <input type = "hidden" id="ss_book_id" name = "ss_book_id" value = "{{$book_id_data->book_id}}">
          </div>
          <div class="text-center" style="margin-top: 3%;">
                <button class="btn btn-danger" type="button" id="submit10" data-dismiss="modal" style="width: 20%;">Done</button>
          </div>
        </div>
      </div>
      </form>
    </div>
  </div>
  <!--Suggested Therapies Model End -->

  <!--Prescription Model Start -->
  <div class="modal fade cmodel" id="myModal12" role="dialog">
    <div class="modal-dialog">
    <form id="myForm13">
      <!-- Modal content-->
      <div class="modal-content">      
        <div class="modal-body">
           <h2 style="color: red;">Prescription</h2>
          <div style="border-radius: 10px !important;border: 1px solid red;height: 283px;">
            {!! Form::textarea('prescription', null, ['class' => 'form-control','rows' => '12','data-parsley-required-message' =>'Key Point is required','required' , 'id'=>'prescription']) !!}
            <input type="hidden" name="_token" id="csrf" value="{{Session::token()}}">
            <input type = "hidden" id="doctor_id" name = "doctor_id" value = "{{Auth::user()->userDetailsId}}">

            <input type = "hidden" id="patient_id" name = "patient_id" value = "{{$detail->id}}">
            <input type = "hidden" id="booking_id" name = "booking_id" value = "{{$book_id_data->book_id}}">
          </div>
          <div class="text-center" style="margin-top: 3%;">
                <button class="btn btn-danger" type="button" id="submit13" data-dismiss="modal" style="width: 20%;">Done</button>
          </div>
        </div>
      </div>
      </form>
    </div>
  </div>
  <!--Prescription Model End -->

  <!--My Notes Model Start -->
  <div class="modal fade cmodel" id="myModal13" role="dialog">
    <div class="modal-dialog">
    <form id="myForm11">
      <!-- Modal content-->
      <div class="modal-content">      
        <div class="modal-body">
           <h2 style="color: red;">My Notes</h2>
			<div style="border-radius: 10px !important;border: 1px solid red;height: 283px;">
            {!! Form::textarea('my_note', null, ['class' => 'form-control','rows' => '12','data-parsley-required-message' =>'Key Point is required','required' , 'id'=>'my_note']) !!}
            <input type="hidden" name="_token" id="csrf" value="{{Session::token()}}">
            <input type = "hidden" id="my_doctor_id" name = "my_doctor_id" value = "{{Auth::user()->userDetailsId}}">

            <input type = "hidden" id="my_patient_id" name = "my_patient_id" value = "{{$detail->id}}">
            <input type ="hidden" id="m_book_id" name="m_book_id" value="{{$book_id_data->book_id}}">
          </div>
          <div class="text-center" style="margin-top: 3%;">
                <button class="btn btn-danger" type="button" id="submit11" data-dismiss="modal" style="width: 20%;">Done</button>
          </div>
        </div>
      </div>
      </form>
    </div>
  </div>
  <!--My Notes Model End -->
<script type="text/javascript" src="https://unpkg.com/@cometchat-pro/chat@2.0.7/CometChat.js"></script>

<script type="text/javascript">
function video_call() {
  var appId = "1583130789928e2";
  let cometChatSettings = new CometChat.AppSettingsBuilder().subscribePresenceForAllUsers().setRegion('us').build();
  CometChat.init(appId, cometChatSettings)
    .then(
      () => {
        console.log("Initialization completed successfully");
        //You can now call login function.
          var UID = "{{$doctor}}";
          var apiKey = "f0ead65846606835fc6b114e7e1ac2d9e496ab41";

          CometChat.login(UID, apiKey).then(
            User => {
              console.log("Login successfully:", { User });
              // User loged in successfully.
              var receiverID = "{{$patient->patient_cuid}}";
              var callType = CometChat.CALL_TYPE.VIDEO;
              var receiverType = CometChat.RECEIVER_TYPE.USER;

              var call = new CometChat.Call(receiverID, callType, receiverType);

              CometChat.initiateCall(call).then(
                outGoingCall => {
                  console.log("Call initiated successfully:", outGoingCall);
                  // perform action on success. Like show your calling screen.
                  var sessionID = outGoingCall.sessionId;

                  CometChat.startCall(
                    sessionID,
                    document.getElementById("callScreen"),
                    new CometChat.OngoingCallListener({
                      onUserJoined: user => {
                        /* Notification received here if another user joins the call. */
                        console.log("User joined call:", user);
                        /* this method can be use to display message or perform any actions if someone joining the call */
                      },
                      onUserLeft: user => {
                        /* Notification received here if another user left the call. */
                        console.log("User left call:", user);
                        /* this method can be use to display message or perform any actions if someone leaving the call */
                      },
                      onCallEnded: call => {
                        /* Notification received here if current ongoing call is ended. */
                        console.log("Call ended:", call);
                        /* hiding/closing the call screen can be done here. */
                      }
                    })
                  );
                },
                error => {
                  console.log("Call initialization failed with exception:", error);
                }
              );
            },
            error => {
              console.log("Login failed with exception:", { error });
              // User login failed, check error and take appropriate action.
            }
          );
       },
       error => {
        console.log("Initialization failed with error:", error);
        //Check the reason for error and take appropriate action.
      }
    );

    
    



 }

</script>
<script type="text/javascript">
$(function () {
        $("#medical_1").click(function () {
            if ($(this).is(":checked")) {
                $("#medical").show();
                $("#personal").hide();
            } else {
                $("#medical").hide();
                $("#personal").show();
            }
        });
    });
</script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
<script type="text/javascript">
    $(function () {
        $("#Medical").click(function () {
            if ($(this).is(":checked")) {
                $("#Medical1").show();
                $("#Personal1").hide();
                $("#Family1").hide();
            } else {
                $("#Medical1").hide();
            }
        });
    });
    $(function () {
      var checked = $(this).is(':checked');
        $("#Personal").click(function () {
            if ($(this).is(":checked")) {
                $("#Personal1").show();
                $("#Medical1").hide();
                $("#Family1").hide();
            } else {
                $("#Personal1").hide();
            }
        });
    });
    $(function () {
        $("#Family").click(function () {
            if ($(this).is(":checked")) {
                $("#Family1").show();
                $("#Medical1").hide();
                $("#Personal1").hide();
            } else {
                $("#Family1").hide();
            }
        });
    });
    function onlyOne(checkbox) {
    var checkboxes = document.getElementsByName('check')
    checkboxes.forEach((item) => {
        if (item !== checkbox) item.checked = false
    })
}

$(document).ready(function() {
    if ($('#Personal').is(':checked')) {
        $('#Personal1').show();
    } else {
        $('#Personal1').hide();
    }
});
</script>

<script type="text/javascript">

  var SITEURL = '{{URL::to('')}}';
         $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });

  jQuery('#submitdd').click(function () {
      var patient_id =jQuery('#patient_id').val();
       var doctor_id = jQuery('#doctor_id').val();
      var k_book_id = jQuery('#k_book_id').val();

      var key_point = jQuery('#key_point').val();
      // console.log(key_point);
      if (key_point == '') {
        //alert("Key Point Missing");
        return;
      }
      else{

             crsf = $("input[name=csrf_test_name]").val();
      jQuery.ajax({
          type: "POST",
          url: SITEURL + '/key_points',        
          dataType: 'json',
          data: {_token: jQuery("#csrf").val(),"patient_id":patient_id,"key_point":key_point,"doctor_id":doctor_id,"k_book_id":k_book_id},
          async: false,
         
          success: function(data) {
             //alert();
             if (data != '') {
                alert("data add");
              }
            // console.log(data);
      //          alert("SUCCESS");       
        // console.log('SUCCESS: ' + data.success);
                
        },
    });

      }
     
       
     // alert(patient_id)

    //alert('Key Points saved successfully');
    });
</script>


<script type="text/javascript">
  var SITEURL = '{{URL::to('')}}';
         $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
  jQuery('#submit3').click(function () {
      var patient_id =jQuery('#patient_id').val();

      var doctor_id = jQuery('#doctor_id').val();
      var m_book_id = jQuery('#m_book_id').val();

      var present_medications = jQuery('#present_medications').val();

      if (present_medications == '') {
        //alert("Present Medications Missing");
        return;
      }
      else{
            crsf = $("input[name=csrf_test_name]").val();
      jQuery.ajax({
          type: "POST",
          url: SITEURL + '/present_medications',        
          dataType: 'json',
          data: {_token: jQuery("#csrf").val(),"patient_id":patient_id,"present_medications":present_medications,"doctor_id":doctor_id,"m_book_id":m_book_id},
          async: false,
         
          success: function(data) {
             //alert();
                //console.log(response);
      //          alert("SUCCESS");       
        // console.log('SUCCESS: ' + data.success);
                
        },
    });
       }
     //alert(patient_id)

     // alert('Present Medications saved successfully');
    });
</script>


<script type="text/javascript">
  var SITEURL = '{{URL::to('')}}';
         $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
  jQuery('#submit4').click(function () {
      var e_patient_id =jQuery('#e_patient_id').val();

      var e_doctor_id = jQuery('#e_doctor_id').val();
      var e_book_id = jQuery('#e_book_id').val();
       
      var examination_notes = jQuery('#examination_notes').val();

      if (present_medications == '') {
       // alert("Examination Notes Missing");
        return;
      }
      else{
             crsf = $("input[name=csrf_test_name]").val();
      jQuery.ajax({
          type: "POST",
          url: SITEURL + '/examination_notes',        
          dataType: 'json',
          data: {_token: jQuery("#csrf").val(),"e_patient_id":e_patient_id,"examination_notes":examination_notes,"e_doctor_id":e_doctor_id,"e_book_id":e_book_id},
          async: false,
         
          success: function(data) {
             //alert();
                //console.log(response);
      //          alert("SUCCESS");       
        // console.log('SUCCESS: ' + data.success);
                
        },
    });
      }
     //alert(e_patient_id)

    //alert('Examination Notes saved successfully');
    });
</script>

<script type="text/javascript">
  var SITEURL = '{{URL::to('')}}';
         $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
  jQuery('#submit5').click(function () {
      var a_patient_id =jQuery('#a_patient_id').val();

      var a_doctor_id = jQuery('#a_doctor_id').val();
      var a_book_id = jQuery('#a_book_id').val();
       
      var available_lab = jQuery('#available_lab').val();

      if (present_medications == '') {
        //alert("Available Lab Missing");
        return;
      }
      else{
             crsf = $("input[name=csrf_test_name]").val();
      jQuery.ajax({
          type: "POST",
          url: SITEURL + '/available_lab',        
          dataType: 'json',
          data: {_token: jQuery("#csrf").val(),"a_patient_id":a_patient_id,"available_lab":available_lab,"a_doctor_id":a_doctor_id,"a_book_id":a_book_id},
          async: false,
         
          success: function(data) {
             //alert();
                //console.log(response);
      //          alert("SUCCESS");       
        // console.log('SUCCESS: ' + data.success);
                
        },
    });
      }
     //alert(a_patient_id)

     // alert('Available Lab saved successfully');
    });
</script>


<script type="text/javascript">
  var SITEURL = '{{URL::to('')}}';
         $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
  jQuery('#submit6').click(function () {
      var p_patient_id =jQuery('#p_patient_id').val();

      var p_doctor_id = jQuery('#p_doctor_id').val();
      var p_book_id = jQuery('#p_book_id').val();
       
      var perceived = jQuery('#perceived').val();

       if (perceived == '') {
       // alert("Perceived/Diagnosis Missing");
        return;
      }
      else{
             crsf = $("input[name=csrf_test_name]").val();
      jQuery.ajax({
          type: "POST",
          url: SITEURL + '/perceived_patient',        
          dataType: 'json',
          data: {_token: jQuery("#csrf").val(),"p_patient_id":p_patient_id,"perceived":perceived,"p_doctor_id":p_doctor_id,"p_book_id":p_book_id},
          async: false,
         
          success: function(data) {
             //alert();
                //console.log(response);
      //          alert("SUCCESS");       
        // console.log('SUCCESS: ' + data.success);
                
        },
    });
      }

     //alert(p_patient_id)

     // alert('Perceived Patient saved successfully');
    });
</script>

<script type="text/javascript">
  var SITEURL = '{{URL::to('')}}';
         $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
  jQuery('#submit7').click(function () {
      var s_patient_id =jQuery('#s_patient_id').val();

      var s_doctor_id = jQuery('#s_doctor_id').val();
      var s_book_id = jQuery('#s_book_id').val();
       
      var suggested_investigations = jQuery('#suggested_investigations').val();

      if (suggested_investigations == '') {
       // alert("Suggested Investigations Missing");
        return;
      }
      else{
             crsf = $("input[name=csrf_test_name]").val();
      jQuery.ajax({
          type: "POST",
          url: SITEURL + '/suggested_investigations',        
          dataType: 'json',
          data: {_token: jQuery("#csrf").val(),"s_patient_id":s_patient_id,"suggested_investigations":suggested_investigations,"s_doctor_id":s_doctor_id,"s_book_id":s_book_id},
          async: false,
         
          success: function(data) {
             //alert();
                //console.log(response);
      //          alert("SUCCESS");       
        // console.log('SUCCESS: ' + data.success);
                
        },
    });
      }

     //alert(s_patient_id)

    //alert('Suggested Investigations saved successfully');
    });
</script>

<script type="text/javascript">
  var SITEURL = '{{URL::to('')}}';
         $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
  jQuery('#submit8').click(function () {
      var si_patient_id =jQuery('#si_patient_id').val();

      var si_doctor_id = jQuery('#si_doctor_id').val();
      var si_book_id = jQuery('#si_book_id').val();
       
      var suggested_imagings = jQuery('#suggested_imagings').val();

       if (suggested_imagings == '') {
        //alert("Suggested Imagings Missing");
        return;
      }
      else{
             crsf = $("input[name=csrf_test_name]").val();
      jQuery.ajax({
          type: "POST",
          url: SITEURL + '/suggested_imagings',        
          dataType: 'json',
          data: {_token: jQuery("#csrf").val(),"si_patient_id":si_patient_id,"suggested_imagings":suggested_imagings,"si_doctor_id":si_doctor_id,"si_book_id":si_book_id},
          async: false,
         
          success: function(data) {
             //alert();
                //console.log(response);
      //          alert("SUCCESS");       
        // console.log('SUCCESS: ' + data.success);
                
        },
    });
      }
     //alert(si_patient_id)

       //alert('Suggested Imagings saved successfully');
    });
</script>

<script type="text/javascript">
  var SITEURL = '{{URL::to('')}}';
         $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
  jQuery('#submit9').click(function () {
      var ss_patient_id =jQuery('#ss_patient_id').val();

      var ss_doctor_id = jQuery('#ss_doctor_id').val();
      var ss_book_id = jQuery('#ss_book_id').val();
       
      var suggested_specialists = jQuery('#suggested_specialists').val();

      if (suggested_specialists == '') {
        //alert("Suggested Specialists Missing");
        return;
      }
      else{
             crsf = $("input[name=csrf_test_name]").val();
      jQuery.ajax({
          type: "POST",
          url: SITEURL + '/suggested_specialists',        
          dataType: 'json',
          data: {_token: jQuery("#csrf").val(),"ss_patient_id":ss_patient_id,"suggested_specialists":suggested_specialists,"ss_doctor_id":ss_doctor_id,"ss_book_id":ss_book_id},
          async: false,
         
          success: function(data) {
             //alert();
                //console.log(response);
      //          alert("SUCCESS");       
        // console.log('SUCCESS: ' + data.success);
                
        },
    });

      }
     //alert(ss_patient_id)

      //alert('Suggested Specialists saved successfully');
    });
</script>

<script type="text/javascript">
  var SITEURL = '{{URL::to('')}}';
         $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
  jQuery('#submit10').click(function () {
      var st_patient_id =jQuery('#st_patient_id').val();

      var st_doctor_id = jQuery('#st_doctor_id').val();
      var st_book_id = jQuery('#st_book_id').val();
      var suggested_therapies = jQuery('#suggested_therapies').val();

      if (suggested_therapies == '') {
        //alert("Suggested Therapies Missing");
        return;
      }
      else{
            crsf = $("input[name=csrf_test_name]").val();
      jQuery.ajax({
          type: "POST",
          url: SITEURL + '/suggested_therapies',        
          dataType: 'json',
          data: {_token: jQuery("#csrf").val(),"st_patient_id":st_patient_id,"suggested_therapies":suggested_therapies,"st_doctor_id":st_doctor_id,"ss_book_id":ss_book_id},
          async: false,
         
          success: function(data) {
             //alert();
                //console.log(response);
      //          alert("SUCCESS");       
        // console.log('SUCCESS: ' + data.success);
                
        },
    });

       }
     //alert(st_patient_id)

      //alert('Suggested Therapies saved successfully');
    });
</script>

<script type="text/javascript">
  var SITEURL = '{{URL::to('')}}';
         $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
  jQuery('#submit11').click(function () {
      var my_patient_id =jQuery('#my_patient_id').val();
      var my_doctor_id = jQuery('#my_doctor_id').val();
      var m_book_id = jQuery('#m_book_id').val();

      var my_note = jQuery('#my_note').val();

      if (suggested_therapies == '') {
        alert("My Note Missing");
        return;
      }
      else{
     crsf = $("input[name=csrf_test_name]").val();
      jQuery.ajax({
          type: "POST",
          url: SITEURL + '/my_note',        
          dataType: 'json',
          data: {_token: jQuery("#csrf").val(),"my_patient_id":my_patient_id,"my_note":my_note,"my_doctor_id":my_doctor_id,'m_book_id':m_book_id},
          async: false,
          success: function(data) {
             //alert();
                //console.log(response);
      //          alert("SUCCESS");       
        // console.log('SUCCESS: ' + data.success);
                
        },
    });
      }
       
     //alert(my_patient_id)

      //alert('My Note saved successfully');
    });
</script>

<script type="text/javascript">
  var SITEURL = '{{URL::to('')}}';
         $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
  jQuery('#submit13').click(function () {
      var patient_id =jQuery('#patient_id').val();
      var doctor_id = jQuery('#doctor_id').val();
      var booking_id = jQuery('#booking_id').val();

      var prescription = jQuery('#prescription').val();
      //alert('1');
       // console.log(key_point);
      if (prescription == '') {
        alert("Prescription Missing");
        return;
      }
     crsf = $("input[name=csrf_test_name]").val();
      jQuery.ajax({
          type: "POST",
          url: SITEURL + '/prescription_1',        
          dataType: 'json',
          data: {_token: jQuery("#csrf").val(),"patient_id":patient_id,"prescription":prescription,"doctor_id":doctor_id,'booking_id':booking_id},
          async: false,
          success: function(data) {
             //alert();
                //console.log(response);
      //          alert("SUCCESS");       
        // console.log('SUCCESS: ' + data.success);
                
        },
    });
 
       
     //alert(my_patient_id)

      //alert('Prescription saved successfully');
    });
</script>

<script type="text/javascript">
  var SITEURL = '{{URL::to('')}}';
         $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
  jQuery('#done1').click(function () {
      var patient_name =jQuery('#patient_name').val();
      var doctor_name = jQuery('#doctor_name').val();
      var book_id = jQuery('#book_id').val();
      //alert('1');
       // console.log(key_point);
     crsf = $("input[name=csrf_test_name]").val();
      jQuery.ajax({
          type: "POST",
          url: SITEURL + '/appoiment_done',        
          dataType: 'json',
          data: {_token: jQuery("#csrf").val(),"patient_name":patient_name,"doctor_name":doctor_name,'book_id':book_id},
          async: false,
          success: function(data) {
            window.location.href = SITEURL + '/dr_tostart' ;
                
        },
    });
 
       
     //alert(my_patient_id)

      //alert('Prescription saved successfully');
    });
</script>
<script>
$(document).ready(function(){
  $("#hidebutton").click(function(){
    $("#hidebutton").hide();
  });
});
</script>
@endsection
