@extends('layouts.new.header')
@section('title', 'Dr Review Summery')
@section('content')
<style type="text/css">
    /*.card:hover {
        color: white;
  background-color: red;
}*/
.card{
  height: auto; 
  border-radius: 15px; 
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
}
.card-title{
    margin-top: 5%;
}
.bio{
    border-radius: 17px !important;
    border: 1px solid red;
    margin-left: 5%;
    margin-right: 5%;
    height: 230px;
}
.btn-danger {
    color: #fff;
    background-color: red;
    border-color: red;
}
/*a:hover {
    color: white !important;
    text-decoration: none;
}*/
hr {
    margin-top: 1rem;
    margin-bottom: 1rem;
    border: 0;
    border-top: 1px solid red;
}
.form-control {
   box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
}
label {
    font-size: 15px;
    margin-top: 1%;
}
.pic{
    height: 84px;
    border-radius: 17px !important;
    border: 1px solid red;
}
.alert-success {
    color: white;
    background-color: green;
    border-color: #cdedd8;
}
.parsley-required{
    color: red;
}

.btn2 {
    color: black;
    background-color: white;
    border-color: red;
}
.border{
  border-radius: 15px;
}
.nav-tabs .nav-link.active {
    color: #fff;
    background: #db4130;
    border-color: #c8ced3;
    border-bottom-color: #fff;
}
.nav-tabs .nav-link:focus, .nav-tabs .nav-link:hover {
    border-color: #e4e7ea #e4e7ea #c8ced3;
    background-color: #337ab7;
    color: white;
}
.nav-tabs {
    border-bottom: 0px;
}
</style>
<!-- <ol class="breadcrumb">
        <li class="breadcrumb-item">Doctors</li>
    </ol> -->
   
    <div class="container-fluid">
        <div class="animated fadeIn">
             <div class="container">
               
                <div class="row">
                      <div class="col-sm-8">
<!--                          <div class="row" style="margin-left: 1%;margin-right: 1%;margin-top: 2%;">
                                   <div class="col-sm-6">

                                    <div class="text-center">
                                    {!! Form::text('search', null, ['class' => 'form-control']) !!}
                                    </div>
                                    </div>

                                    <div class="col-sm-2">
                                     <div class="text-center">
                                     <button class="btn btn2" type="button" style="width: 100%;">From</button>
                                    </div>
                                    </div>

                                    <div class="col-sm-2">
                                     <div class="text-center">
                                    <button class="btn btn2" type="button" style="width: 100%;">To</button>
                                    </div>
                                    </div>

                                    <div class="col-sm-2">
                                     <div class="text-center">
                                    <button class="btn btn2" type="button" style="width: 100%;">Search</button>
                                    </div>
                                    </div>

                                </div> -->
                                <h1 class="text-center" style="margin-top: 5%;">Review Summery</h1>


                                <div class="container mt-3">
                                  <!-- Nav tabs -->
                                  <ul class="nav nav-tabs" style="margin-bottom: 2%;border: 1px solid #b2b2b2;box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);height: 60px;border-radius: 10px;">
                                    <li class="nav-item" style="margin-top: 10px;">
                                      <a class="nav-link active" href="#home">Today</a>
                                    </li>
                                    <li class="nav-item" style="margin-top: 10px;"> 
                                      <a class="nav-link" href="#menu1">This Week</a>
                                    </li>
                                    <li class="nav-item" style="margin-top: 10px;">
                                      <a class="nav-link" href="#menu2">This Month</a>
                                    </li>

                                    <li class="nav-item" style="margin-top: 10px;">
                                      <a class="nav-link" href="#menu3">This Year</a>
                                    </li>
                                    <li class="nav-item" style="margin-top: 10px;">
                                      <a class="nav-link" href="#menu4">Total</a>
                                    </li>

                                  </ul>

                                  <!-- Tab panes -->
                                  <div class="tab-content">
                                     
                              <div id="home" class="container tab-pane active" style="overflow: scroll;height: 400px;"><br>

                            <div class="clearfix">
                                <div class="float-left"><strong>Total {{$today}}</strong></div>
                                <div class="float-right">
                                  
                                  <div class="placeholder" style="">
                                                <small class="text-muted">  (4.5 - 5.0)</small>  
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: #db4130;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: #db4130;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: #db4130;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: #db4130;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: #db4130;"></i>
                                  </div>
                                </div>
                          </div>
                        <div class="progress">
                              <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="{{$today}}" aria-valuemin="0" aria-valuemax="100" style="width: {{$today}}%;background-color: #db4130;"></div>
                        </div> <br>

                        <div class="clearfix">
                                <div class="float-left"><strong>Total {{$today_4}}</strong></div>
                                <div class="float-right">
                                 
                                  <div class="placeholder" style="color: lightgray;">
                                                <small class="text-muted">  (3.5 - 4.0)</small>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: #db4130;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: #db4130;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: #db4130;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: #db4130;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: lightgray;"></i>
                                  </div>
                                </div>
                          </div>
                        <div class="progress">
                              <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="{{$today_4}}" aria-valuemin="0" aria-valuemax="100" style="width: {{$today_4}};background-color: #db4130;"></div>
                        </div><br>

                      <div class="clearfix">
                                <div class="float-left"><strong>Total {{$today_3}}</strong></div>
                                <div class="float-right">
                                  
                                  <div class="placeholder" style="color: lightgray;">
                                                <small class="text-muted">  (2.5 - 3.0)</small>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: #db4130;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: #db4130;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: #db4130;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: lightgray;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: lightgray;"></i>
                                  </div>
                                </div>
                          </div>
                        <div class="progress">
                              <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="{{$today_3}}" aria-valuemin="0" aria-valuemax="100" style="width: {{$today_3}}%;background-color: #db4130;"></div>
                        </div><br>

                             <div class="clearfix">
                                <div class="float-left"><strong>Total {{$today_2}}</strong></div>
                                <div class="float-right">
                                 
                                  <div class="placeholder" style="color: lightgray;">
                                                 <small class="text-muted">  (1.5 - 2.0)</small>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: #db4130;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: #db4130;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: lightgray;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: lightgray;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: lightgray;"></i>
                                  </div>
                                </div>
                          </div>
                        <div class="progress">
                              <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="{{$today_2}}" aria-valuemin="0" aria-valuemax="100" style="width: {{$today_2}}%;background-color: #db4130;"></div>
                        </div><br>

                        <div class="clearfix">
                                <div class="float-left"><strong>Total {{$today_1}}</strong></div>
                                <div class="float-right">
                                  
                                  <div class="placeholder" style="color: lightgray;">
                                                <small class="text-muted">  (1.0)</small>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: #db4130;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: lightgray;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: lightgray;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: lightgray;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: lightgray;"></i>
                                  </div>
                                </div>
                          </div>
                        <div class="progress">
                              <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="{{$today_1}}" aria-valuemin="0" aria-valuemax="100" style="width: {{$today_1}}%;background-color: #db4130;"></div>
                        </div>

                        <br>
                        <center>
                        <div>
                          <h5>Total Reviews Today {{$subtotal}} Out of Total {{$today_consults}} Consults</h5>
                        </div>
                        </center>

                  </div>


                          <div id="menu1" class="container tab-pane fade" style="overflow: scroll;height: 400px;"><br>

                                                        <div class="clearfix">
                                <div class="float-left"><strong>Total {{$this_week}}</strong></div>
                                <div class="float-right">
                                  
                                  <div class="placeholder" style="">
                                                <small class="text-muted">  (4.5 - 5.0)</small>  
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: #db4130;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: #db4130;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: #db4130;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: #db4130;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: #db4130;"></i>
                                  </div>
                                </div>
                          </div>
                        <div class="progress">
                              <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="{{$this_week}}" aria-valuemin="0" aria-valuemax="100" style="width: {{$this_week}}%;background-color: #db4130;"></div>
                        </div> <br>

                        <div class="clearfix">
                                <div class="float-left"><strong>Total {{$this_week_4}}</strong></div>
                                <div class="float-right">
                                 
                                  <div class="placeholder" style="color: lightgray;">
                                                <small class="text-muted">  (3.5 - 4.0)</small>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: #db4130;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: #db4130;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: #db4130;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: #db4130;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: lightgray;"></i>
                                  </div>
                                </div>
                          </div>
                        <div class="progress">
                              <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="{{$this_week_4}}" aria-valuemin="0" aria-valuemax="100" style="width: {{$this_week_4}};background-color: #db4130;"></div>
                        </div><br>

                      <div class="clearfix">
                                <div class="float-left"><strong>Total {{$this_week_3}}</strong></div>
                                <div class="float-right">
                                  
                                  <div class="placeholder" style="color: lightgray;">
                                                <small class="text-muted">  (2.5 - 3.0)</small>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: #db4130;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: #db4130;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: #db4130;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: lightgray;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: lightgray;"></i>
                                  </div>
                                </div>
                          </div>
                        <div class="progress">
                              <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="{{$this_week_3}}" aria-valuemin="0" aria-valuemax="100" style="width: {{$this_week_3}}%;background-color: #db4130;"></div>
                        </div><br>

                             <div class="clearfix">
                                <div class="float-left"><strong>Total {{$this_week_2}}</strong></div>
                                <div class="float-right">
                                 
                                  <div class="placeholder" style="color: lightgray;">
                                                 <small class="text-muted">  (1.5 - 2.0)</small>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: #db4130;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: #db4130;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: lightgray;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: lightgray;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: lightgray;"></i>
                                  </div>
                                </div>
                          </div>
                        <div class="progress">
                              <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="{{$this_week_2}}" aria-valuemin="0" aria-valuemax="100" style="width: {{$this_week_2}}%;background-color: #db4130;"></div>
                        </div><br>

                        <div class="clearfix">
                                <div class="float-left"><strong>Total {{$this_week_1}}</strong></div>
                                <div class="float-right">
                                  
                                  <div class="placeholder" style="color: lightgray;">
                                                <small class="text-muted">  (1.0)</small>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: #db4130;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: lightgray;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: lightgray;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: lightgray;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: lightgray;"></i>
                                  </div>
                                </div>
                          </div>
                        <div class="progress">
                              <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="{{$this_week_1}}" aria-valuemin="0" aria-valuemax="100" style="width: {{$this_week_1}}%;background-color: #db4130;"></div>
                        </div>

                        <br>
                       <center>
                        <div>
                          <h5>Total Reviews This Week {{$subtotal_this_week}} Out of Total {{$this_week_consults}} Consults</h5>
                        </div>
                        </center>



                        </div>


                        <div id="menu2" class="container tab-pane fade" style="overflow: scroll;height: 400px;"><br>

                                                                                  <div class="clearfix">
                                <div class="float-left"><strong>Total {{$this_month}}</strong></div>
                                <div class="float-right">
                                  
                                  <div class="placeholder" style="">
                                                <small class="text-muted">  (4.5 - 5.0)</small>  
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: #db4130;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: #db4130;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: #db4130;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: #db4130;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: #db4130;"></i>
                                  </div>
                                </div>
                          </div>
                        <div class="progress">
                              <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="{{$this_month}}" aria-valuemin="0" aria-valuemax="100" style="width: {{$this_month}}%;background-color: #db4130;"></div>
                        </div> <br>

                        <div class="clearfix">
                                <div class="float-left"><strong>Total {{$this_month_4}}</strong></div>
                                <div class="float-right">
                                 
                                  <div class="placeholder" style="color: lightgray;">
                                                <small class="text-muted">  (3.5 - 4.0)</small>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: #db4130;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: #db4130;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: #db4130;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: #db4130;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: lightgray;"></i>
                                  </div>
                                </div>
                          </div>
                        <div class="progress">
                              <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="{{$this_month_4}}" aria-valuemin="0" aria-valuemax="100" style="width: {{$this_month_4}};background-color: #db4130;"></div>
                        </div><br>

                      <div class="clearfix">
                                <div class="float-left"><strong>Total {{$this_month_3}}</strong></div>
                                <div class="float-right">
                                  
                                  <div class="placeholder" style="color: lightgray;">
                                                <small class="text-muted">  (2.5 - 3.0)</small>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: #db4130;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: #db4130;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: #db4130;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: lightgray;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: lightgray;"></i>
                                  </div>
                                </div>
                          </div>
                        <div class="progress">
                              <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="{{$this_month_3}}" aria-valuemin="0" aria-valuemax="100" style="width: {{$this_month_3}}%;background-color: #db4130;"></div>
                        </div><br>

                             <div class="clearfix">
                                <div class="float-left"><strong>Total {{$this_month_2}}</strong></div>
                                <div class="float-right">
                                 
                                  <div class="placeholder" style="color: lightgray;">
                                                 <small class="text-muted">  (1.5 - 2.0)</small>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: #db4130;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: #db4130;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: lightgray;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: lightgray;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: lightgray;"></i>
                                  </div>
                                </div>
                          </div>
                        <div class="progress">
                              <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="{{$this_month_2}}" aria-valuemin="0" aria-valuemax="100" style="width: {{$this_month_2}}%;background-color: #db4130;"></div>
                        </div><br>

                        <div class="clearfix">
                                <div class="float-left"><strong>Total {{$this_month_1}}</strong></div>
                                <div class="float-right">
                                  
                                  <div class="placeholder" style="color: lightgray;">
                                                <small class="text-muted">  (1.0)</small>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: #db4130;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: lightgray;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: lightgray;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: lightgray;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: lightgray;"></i>
                                  </div>
                                </div>
                          </div>
                        <div class="progress">
                              <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="{{$this_month_1}}" aria-valuemin="0" aria-valuemax="100" style="width: {{$this_month_1}}%;background-color: #db4130;"></div>
                        </div>

                        <br>
                       <center>
                        <div>
                          <h5>Total Reviews This Month {{$subtotal_this_month}} Out of Total {{$this_month_consults}} Consults</h5>
                        </div>
                        </center>

                          </div>

                       <div id="menu3" class="container tab-pane fade" style="overflow: scroll;height: 400px;"><br>

                          <div class="clearfix">
                                <div class="float-left"><strong>Total {{$this_year}}</strong></div>
                                <div class="float-right">
                                  
                                  <div class="placeholder" style="">
                                                <small class="text-muted">  (4.5 - 5.0)</small>  
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: #db4130;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: #db4130;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: #db4130;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: #db4130;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: #db4130;"></i>
                                  </div>
                                </div>
                          </div>
                        <div class="progress">
                              <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="{{$this_year}}" aria-valuemin="0" aria-valuemax="100" style="width: {{$this_year}}%;background-color: #db4130;"></div>
                        </div> <br>

                        <div class="clearfix">
                                <div class="float-left"><strong>Total {{$this_year_4}}</strong></div>
                                <div class="float-right">
                                 
                                  <div class="placeholder" style="color: lightgray;">
                                                <small class="text-muted">  (3.5 - 4.0)</small>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: #db4130;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: #db4130;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: #db4130;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: #db4130;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: lightgray;"></i>
                                  </div>
                                </div>
                          </div>
                        <div class="progress">
                              <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="{{$this_year_4}}" aria-valuemin="0" aria-valuemax="100" style="width: {{$this_year_4}};background-color: #db4130;"></div>
                        </div><br>

                      <div class="clearfix">
                                <div class="float-left"><strong>Total {{$this_year_3}}</strong></div>
                                <div class="float-right">
                                  
                                  <div class="placeholder" style="color: lightgray;">
                                                <small class="text-muted">  (2.5 - 3.0)</small>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: #db4130;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: #db4130;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: #db4130;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: lightgray;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: lightgray;"></i>
                                  </div>
                                </div>
                          </div>
                        <div class="progress">
                              <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="{{$this_year_3}}" aria-valuemin="0" aria-valuemax="100" style="width: {{$this_year_3}}%;background-color: #db4130;"></div>
                        </div><br>

                             <div class="clearfix">
                                <div class="float-left"><strong>Total {{$this_year_2}}</strong></div>
                                <div class="float-right">
                                 
                                  <div class="placeholder" style="color: lightgray;">
                                                 <small class="text-muted">  (1.5 - 2.0)</small>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: #db4130;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: #db4130;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: lightgray;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: lightgray;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: lightgray;"></i>
                                  </div>
                                </div>
                          </div>
                        <div class="progress">
                              <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="{{$this_year_2}}" aria-valuemin="0" aria-valuemax="100" style="width: {{$this_year_2}}%;background-color: #db4130;"></div>
                        </div><br>

                        <div class="clearfix">
                                <div class="float-left"><strong>Total {{$this_year_1}}</strong></div>
                                <div class="float-right">
                                  
                                  <div class="placeholder" style="color: lightgray;">
                                                <small class="text-muted">  (1.0)</small>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: #db4130;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: lightgray;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: lightgray;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: lightgray;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: lightgray;"></i>
                                  </div>
                                </div>
                          </div>
                        <div class="progress">
                              <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="{{$this_year_1}}" aria-valuemin="0" aria-valuemax="100" style="width: {{$this_year_1}}%;background-color: #db4130;"></div>
                        </div>

                        <br>
                        <center>
                        <div>
                          <h5>Total Reviews This Year {{$subtotal_this_year}} Out of Total {{$this_year_consults}} Consults</h5>
                        </div>
                        </center>

                          </div>


                        <div id="menu4" class="container tab-pane fade" style="overflow: scroll;height: 400px;"><br>

                          <div class="clearfix">
                                <div class="float-left"><strong>Total {{$this_total}}</strong></div>
                                <div class="float-right">
                                  
                                  <div class="placeholder" style="">
                                                <small class="text-muted">  (4.5 - 5.0)</small>  
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: #db4130;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: #db4130;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: #db4130;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: #db4130;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: #db4130;"></i>
                                  </div>
                                </div>
                          </div>
                        <div class="progress">
                              <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="{{$this_total}}" aria-valuemin="0" aria-valuemax="100" style="width: {{$this_total}}%;background-color: #db4130;"></div>
                        </div> <br>

                        <div class="clearfix">
                                <div class="float-left"><strong>Total {{$this_total_4}}</strong></div>
                                <div class="float-right">
                                 
                                  <div class="placeholder" style="color: lightgray;">
                                                <small class="text-muted">  (3.5 - 4.0)</small>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: #db4130;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: #db4130;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: #db4130;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: #db4130;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: lightgray;"></i>
                                  </div>
                                </div>
                          </div>
                        <div class="progress">
                              <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="{{$this_total_4}}" aria-valuemin="0" aria-valuemax="100" style="width: {{$this_total_4}};background-color: #db4130;"></div>
                        </div><br>

                      <div class="clearfix">
                                <div class="float-left"><strong>Total {{$this_total_3}}</strong></div>
                                <div class="float-right">
                                  
                                  <div class="placeholder" style="color: lightgray;">
                                                <small class="text-muted">  (2.5 - 3.0)</small>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: #db4130;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: #db4130;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: #db4130;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: lightgray;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: lightgray;"></i>
                                  </div>
                                </div>
                          </div>
                        <div class="progress">
                              <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="{{$this_total_3}}" aria-valuemin="0" aria-valuemax="100" style="width: {{$this_total_3}}%;background-color: #db4130;"></div>
                        </div><br>

                             <div class="clearfix">
                                <div class="float-left"><strong>Total {{$this_total_2}}</strong></div>
                                <div class="float-right">
                                 
                                  <div class="placeholder" style="color: lightgray;">
                                                 <small class="text-muted">  (1.5 - 2.0)</small>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: #db4130;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: #db4130;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: lightgray;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: lightgray;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: lightgray;"></i>
                                  </div>
                                </div>
                          </div>
                        <div class="progress">
                              <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="{{$this_total_2}}" aria-valuemin="0" aria-valuemax="100" style="width: {{$this_total_2}}%;background-color: #db4130;"></div>
                        </div><br>

                        <div class="clearfix">
                                <div class="float-left"><strong>Total {{$this_total_1}}</strong></div>
                                <div class="float-right">
                                  
                                  <div class="placeholder" style="color: lightgray;">
                                                <small class="text-muted">  (1.0)</small>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: #db4130;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: lightgray;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: lightgray;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: lightgray;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: lightgray;"></i>
                                  </div>
                                </div>
                          </div>
                        <div class="progress">
                              <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="{{$this_total_1}}" aria-valuemin="0" aria-valuemax="100" style="width: {{$this_total_1}}%;background-color: #db4130;"></div>
                        </div>

                        <br>
                        <center>
                        <div>
                          <h5>Total Reviews Today {{$subtotal_this_total}} Out of Total {{$this_total_consults}} Consults</h5>
                        </div>
                        </center>

                          </div>






                                  </div>
                                </div>
                              </div>
                                  @include('newview.new.sidemenu.review')
                </div>
                    
            </div>

         </div>
    </div>
    
@endsection
@push('scripts')
<script>
$(document).ready(function(){
  $(".nav-tabs a").click(function(){
    $(this).tab('show');
  });
});
</script>
@endpush

