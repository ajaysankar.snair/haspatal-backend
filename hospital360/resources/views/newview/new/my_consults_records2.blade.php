@extends('layouts.new.header')
@section('title', 'My Consults Records')
@section('content')
<style type="text/css">
    /*.card:hover {
        color: white;
  background-color: red;
}*/
.card{
  height: auto; 
  border-radius: 15px; 
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
}
.card-title{
    margin-top: 5%;
}
.bio{
    border-radius: 17px !important;
    border: 1px solid red;
    margin-left: 5%;
    margin-right: 5%;
    height: 230px;
}
.btn-danger {
    color: #fff;
    background-color: red;
    border-color: red;
}
/*a:hover {
    color: white !important;
    text-decoration: none;
}*/
hr {
    margin-top: 1rem;
    margin-bottom: 1rem;
    border: 0;
    border-top: 1px solid red;
}
.form-control {
   box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
}
label {
    font-size: 15px;
    margin-top: 1%;
}
.pic{
    height: 84px;
    border-radius: 17px !important;
    border: 1px solid red;
}
.alert-success {
    color: white;
    background-color: green;
    border-color: #cdedd8;
}
.parsley-required{
    color: red;
}

.btn2 {
    color: black;
    background-color: white;
    border-color: red;
}

</style>
<!-- <ol class="breadcrumb">
        <li class="breadcrumb-item">Doctors</li>
    </ol> -->
    
    <div class="container-fluid">
        <div class="animated fadeIn">
             <div class="container">
               
                <div class="row">
                      <div class="col-sm-8">
                                <h1 class="text-center" style="margin-top: 5%;">My Consults Records</h1>
                                <div class="row " style="margin-left: 1%;margin-right: 1%;margin-top: 8%;border: 1px solid #b2b2b2;box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);">
                                  <div class="col-sm-12">
                                        
                                  <div class="table-responsive">          
                                        <table class="table">
                                          <thead>
                                            <tr>
                                              <th>Token</th>
                                              <th>Booking ID</th>
                                              <th>Patient Name</th>
                                              <th>Booked Slot</th>
                                              <th>Date</th>
                                            </tr>
                                          </thead>
                                          <tbody>
                                             <?php if(isset($patients)) { ?>
                                             @if (!$patients->isEmpty())
                                             @foreach($patients as $value)
                                            <tr>
                                              <td> {{$value->id}}</td>
                                              <td> {{$value->book_id}}</td>
                                              <td>{{$value->first_name}} {{$value->lastname}}</td>
                                              <td>  <?php $p_time =  explode(' ',$value->date_time ) ?>
                                              <?php echo $p_time['1']; ?></td>
                                              <td><?php $p_time =  explode(' ',$value->date_time ) ?>
                                              <?php //echo $p_time['0']; ?>
                                                <?php echo \Carbon\Carbon::parse($value->date_time)->format('d-M-Y') ?>
                                              </td>
                                            </tr>
                                          </tbody>
                                           @endforeach
                                           @else
                                           <h3 class="text-center" style="margin-top: 5%;"> No Records Found</h3>
                                           @endif
                                         <?php } ?>
                                        </table>
                                        </div>
                                        
                                    </div>
                                </div>
                              </div>
                                  @include('newview.new.sidemenu.records')
                </div>
                    
            </div>

         </div>
    </div>

@endsection