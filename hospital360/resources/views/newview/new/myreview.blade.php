@extends('layouts.new.header')
@section('title', 'Dr Review')
@section('content')
<style type="text/css">
    /*.card:hover {
        color: white;
  background-color: red;
}*/
.card{
  height: auto; 
  border-radius: 15px; 
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
}
.card-title{
    margin-top: 5%;
}
.bio{
    border-radius: 17px !important;
    border: 1px solid red;
    margin-left: 5%;
    margin-right: 5%;
    height: 230px;
}
.btn-danger {
    color: #fff;
    background-color: red;
    border-color: red;
}
/*a:hover {
    color: white !important;
    text-decoration: none;
}*/
hr {
    margin-top: 1rem;
    margin-bottom: 1rem;
    border: 0;
    border-top: 1px solid red;
}
.form-control {
   box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
}
label {
    font-size: 15px;
    margin-top: 1%;
}
.pic{
    height: 84px;
    border-radius: 17px !important;
    border: 1px solid red;
}
.alert-success {
    color: white;
    background-color: green;
    border-color: #cdedd8;
}
.parsley-required{
    color: red;
}

.btn2 {
    color: black;
    background-color: white;
    border-color: red;
}
.border{
  border-radius: 15px;
}
.nav-tabs .nav-link.active {
    color: #fff;
    background: #db4130;
    border-color: #c8ced3;
    border-bottom-color: #fff;
}
.nav-tabs .nav-link:focus, .nav-tabs .nav-link:hover {
    border-color: #e4e7ea #e4e7ea #c8ced3;
    background-color: #337ab7;
    color: white;
}
.nav-tabs {
    border-bottom: 0px;
}
</style>
<!-- <ol class="breadcrumb">
        <li class="breadcrumb-item">Doctors</li>
    </ol> -->
   
    <div class="container-fluid">
        <div class="animated fadeIn">
             <div class="container">
               
                <div class="row">
                      <div class="col-sm-8">
<!--                          <div class="row" style="margin-left: 1%;margin-right: 1%;margin-top: 2%;">
                                   <div class="col-sm-6">

                                    <div class="text-center">
                                    {!! Form::text('search', null, ['class' => 'form-control']) !!}
                                    </div>
                                    </div>

                                    <div class="col-sm-2">
                                     <div class="text-center">
                                     <button class="btn btn2" type="button" style="width: 100%;">From</button>
                                    </div>
                                    </div>

                                    <div class="col-sm-2">
                                     <div class="text-center">
                                    <button class="btn btn2" type="button" style="width: 100%;">To</button>
                                    </div>
                                    </div>

                                    <div class="col-sm-2">
                                     <div class="text-center">
                                    <button class="btn btn2" type="button" style="width: 100%;">Search</button>
                                    </div>
                                    </div>

                                </div> -->
                                <h1 class="text-center" style="margin-top: 5%;">Reviews</h1>


                                <div class="container mt-3">
                                  <!-- Nav tabs -->
                                  <ul class="nav nav-tabs" style="margin-bottom: 2%;border: 1px solid #b2b2b2;box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);height: 60px;border-radius: 10px;">
                                    <li class="nav-item" style="margin-top: 10px;">
                                      <a class="nav-link active" href="#home">Today</a>
                                    </li>
                                    <li class="nav-item" style="margin-top: 10px;"> 
                                      <a class="nav-link" href="#menu1">This Week</a>
                                    </li>
                                    <li class="nav-item" style="margin-top: 10px;">
                                      <a class="nav-link" href="#menu2">This Month</a>
                                    </li>

                                    <li class="nav-item" style="margin-top: 10px;">
                                      <a class="nav-link" href="#menu3">This Year</a>
                                    </li>
                                    <li class="nav-item" style="margin-top: 10px;">
                                      <a class="nav-link" href="#menu4">Total</a>
                                    </li>

                                  </ul>

                                  <!-- Tab panes -->
                                  <div class="tab-content">
                                  	 
                      <div id="home" class="container tab-pane active" style="overflow: scroll;height: 400px;"><br>

                           @if(count($today) != 0)

                            @foreach($today as $data)

                         <?php 
                                        $date_time = explode(' ',$data->created_at);
                          ?>

                        <div class="row" style="margin-left: 1%;margin-right: 1%;">
                                   
                                   <div class="col-sm-12 border" style="margin-top: 2%;border: 1px solid #b2b2b2;box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);">

                                    <div class="" style="margin-top: 2%;">
                                     <h4 style="font-size: 17px;"><span style="color: red;font-size: 17px;">Consultation ID </span>{{$data->book_id}} , <span style="color: red;font-size: 17px;">Review Date</span> {{date('d F Y', strtotime($date_time[0]))}} / {{date('g:i a', strtotime($date_time[1]))}}</h4>
                                    
                                     <h5><span style="color: red;">{{$data->patientname}}</span> wrote</h5>
                                      <P>{{$data->review_que}}</P>

                                       @if(isset($data->review_ans))
                                      <h5><span style="color: red;">{{$data->first_name}}</span> wrote</h5>
                                      <P>{{$data->review_ans}}</P>
                                      @else 
                                      
                                      @endif
                                      
                                      @php $rating = $data->rating; @endphp  

                                        <div style="float: right;">
                                            <div class="placeholder" style="color: lightgray;">
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;"></i>
                                                <span style = "color: #db4130;" >({{ $rating }})</span>
                                            </div>

                                            <div class="overlay" style="position: relative;top: -22px;">
                                                
                                                @while($rating>0)
                                                    @if($rating >0.5)
                                                        <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: #db4130;"></i>
                                                    @else
                                                       <i class="fa fa-star-half-o" aria-hidden="true" style="font-size: 20px;color: #db4130;"></i>
                                                    @endif
                                                    @php $rating--; @endphp
                                                @endwhile

                                            </div> 
                                       </div>
                                     
                                      </div>
                                    </div>
                               </div>

                          @endforeach


                                                                 
                        @else 
                            <div class="row" style="margin-left: 1%;margin-right: 1%;">
                                   <div class="col-sm-12" style="margin-top: 2%;border: 1px solid #b2b2b2;box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);">

                                    <div class="" style="margin-top: 2%;">
                                          <div class="alert alert-danger">
                                                 <strong>Sorry! Today</strong> No Reviews Found.
                                            </div>
                                      </div>


                                    </div>
                               </div>

                          @endif


                          </div>


                          <div id="menu1" class="container tab-pane fade" style="overflow: scroll;height: 400px;"><br>


                             @if(count($this_weeks) != 0)

                            @foreach($this_weeks as $data)

                         <?php 
                                        $date_time = explode(' ',$data->created_at);
                          ?>

                        <div class="row" style="margin-left: 1%;margin-right: 1%;">
                                   
                                   <div class="col-sm-12 border" style="margin-top: 2%;border: 1px solid #b2b2b2;box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);">

                                    <div class="" style="margin-top: 2%;">
                                     <h4 style="font-size: 17px;"><span style="color: red;font-size: 17px;">Consultation ID </span>{{$data->book_id}} , <span style="color: red;font-size: 17px;">Review Date</span> {{date('d F Y', strtotime($date_time[0]))}} / {{date('g:i a', strtotime($date_time[1]))}}</h4>
                                    
                                     <h5><span style="color: red;">{{$data->patientname}}</span> wrote</h5>
                                      <P>{{$data->review_que}}</P>

                                      @if(isset($data->review_ans))
                                      <h5><span style="color: red;">{{$data->first_name}}</span> wrote</h5>
                                      <P>{{$data->review_ans}}</P>
                                      @else 
                                      
                                      @endif

                                      @php $rating = $data->rating; @endphp  

                                        <div style="float: right;">
                                            <div class="placeholder" style="color: lightgray;">
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;"></i>
                                                <span style = "color: #db4130;" >({{ $rating }})</span>
                                            </div>

                                            <div class="overlay" style="position: relative;top: -22px;">
                                                
                                                @while($rating>0)
                                                    @if($rating >0.5)
                                                        <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: #db4130;"></i>
                                                    @else
                                                       <i class="fa fa-star-half-o" aria-hidden="true" style="font-size: 20px;color: #db4130;"></i>
                                                    @endif
                                                    @php $rating--; @endphp
                                                @endwhile

                                            </div> 
                                       </div>
                                     
                                      </div>
                                    </div>
                               </div>

                          @endforeach


                                                                 
                        @else 
                            <div class="row" style="margin-left: 1%;margin-right: 1%;">
                                   <div class="col-sm-12" style="margin-top: 2%;border: 1px solid #b2b2b2;box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);">

                                    <div class="" style="margin-top: 2%;">
                                          <div class="alert alert-danger">
                                                 <strong>Sorry! This Week</strong> No Reviews Found.
                                            </div>
                                      </div>


                                    </div>
                               </div>

                          @endif


                        </div>


                        <div id="menu2" class="container tab-pane fade" style="overflow: scroll;height: 400px;"><br>

                           @if(count($thismonth) != 0)

                            @foreach($thismonth as $data)

                         <?php 
                                        $date_time = explode(' ',$data->created_at);
                          ?>

                        <div class="row" style="margin-left: 1%;margin-right: 1%;">
                                   
                                   <div class="col-sm-12 border" style="margin-top: 2%;border: 1px solid #b2b2b2;box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);">

                                    <div class="" style="margin-top: 2%;">
                                     <h4 style="font-size: 17px;"><span style="color: red;font-size: 17px;">Consultation ID </span>{{$data->book_id}} , <span style="color: red;font-size: 17px;">Review Date</span> {{date('d F Y', strtotime($date_time[0]))}} / {{date('g:i a', strtotime($date_time[1]))}}</h4>
                                    
                                     <h5><span style="color: red;">{{$data->patientname}}</span> wrote</h5>
                                      <P>{{$data->review_que}}</P>

                                     @if(isset($data->review_ans))
                                      <h5><span style="color: red;">{{$data->first_name}}</span> wrote</h5>
                                      <P>{{$data->review_ans}}</P>
                                      @else 
                                      
                                      @endif

                                      @php $rating = $data->rating; @endphp  

                                        <div style="float: right;">
                                            <div class="placeholder" style="color: lightgray;">
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;"></i>
                                                <span style = "color: #db4130;" >({{ $rating }})</span>
                                            </div>

                                            <div class="overlay" style="position: relative;top: -22px;">
                                                
                                                @while($rating>0)
                                                    @if($rating >0.5)
                                                        <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: #db4130;"></i>
                                                    @else
                                                       <i class="fa fa-star-half-o" aria-hidden="true" style="font-size: 20px;color: #db4130;"></i>
                                                    @endif
                                                    @php $rating--; @endphp
                                                @endwhile

                                            </div> 
                                       </div>
                                     
                                      </div>
                                    </div>
                               </div>

                          @endforeach


                                                                 
                        @else 
                            <div class="row" style="margin-left: 1%;margin-right: 1%;">
                                   <div class="col-sm-12" style="margin-top: 2%;border: 1px solid #b2b2b2;box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);">

                                    <div class="" style="margin-top: 2%;">
                                          <div class="alert alert-danger">
                                                 <strong>Sorry! This Month</strong> No Reviews Found.
                                            </div>
                                      </div>


                                    </div>
                               </div>

                          @endif


                          </div>

                       <div id="menu3" class="container tab-pane fade" style="overflow: scroll;height: 400px;"><br>
                        @if(count($this_year) != 0)

                            @foreach($this_year as $data)

                         <?php 
                                        $date_time = explode(' ',$data->created_at);
                          ?>

                        <div class="row" style="margin-left: 1%;margin-right: 1%;">
                                   
                                   <div class="col-sm-12 border" style="margin-top: 2%;border: 1px solid #b2b2b2;box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);">

                                    <div class="" style="margin-top: 2%;">
                                     <h4 style="font-size: 17px;"><span style="color: red;font-size: 17px;">Consultation ID </span>{{$data->book_id}} , <span style="color: red;font-size: 17px;">Review Date</span> {{date('d F Y', strtotime($date_time[0]))}} / {{date('g:i a', strtotime($date_time[1]))}}</h4>
                                    
                                     <h5><span style="color: red;">{{$data->patientname}}</span> wrote</h5>
                                      <P>{{$data->review_que}}</P>

                                     @if(isset($data->review_ans))
                                      <h5><span style="color: red;">{{$data->first_name}}</span> wrote</h5>
                                      <P>{{$data->review_ans}}</P>
                                      @else 
                                      
                                      @endif

                                      @php $rating = $data->rating; @endphp  

                                        <div style="float: right;">
                                            <div class="placeholder" style="color: lightgray;">
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;"></i>
                                                <span style = "color: #db4130;" >({{ $rating }})</span>
                                            </div>

                                            <div class="overlay" style="position: relative;top: -22px;">
                                                
                                                @while($rating>0)
                                                    @if($rating >0.5)
                                                        <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: #db4130;"></i>
                                                    @else
                                                       <i class="fa fa-star-half-o" aria-hidden="true" style="font-size: 20px;color: #db4130;"></i>
                                                    @endif
                                                    @php $rating--; @endphp
                                                @endwhile

                                            </div> 
                                       </div>
                                     
                                      </div>
                                    </div>
                               </div>

                          @endforeach


                                                                 
                        @else 
                            <div class="row" style="margin-left: 1%;margin-right: 1%;">
                                   <div class="col-sm-12" style="margin-top: 2%;border: 1px solid #b2b2b2;box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);">

                                    <div class="" style="margin-top: 2%;">
                                          <div class="alert alert-danger">
                                                 <strong>Sorry! This Year</strong> No Reviews Found.
                                            </div>
                                      </div>


                                    </div>
                               </div>

                          @endif
                          </div>


                        <div id="menu4" class="container tab-pane fade" style="overflow: scroll;height: 400px;"><br>

                              @if(count($all_total) != 0)

                            @foreach($all_total as $data)

                         <?php 
                                        $date_time = explode(' ',$data->created_at);
                          ?>

                        <div class="row" style="margin-left: 1%;margin-right: 1%;">
                                   
                                   <div class="col-sm-12 border" style="margin-top: 2%;border: 1px solid #b2b2b2;box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);">

                                    <div class="" style="margin-top: 2%;">
                                     <h4 style="font-size: 17px;"><span style="color: red;font-size: 17px;">Consultation ID </span>{{$data->book_id}} , <span style="color: red;font-size: 17px;">Review Date</span> {{date('d F Y', strtotime($date_time[0]))}} / {{date('g:i a', strtotime($date_time[1]))}}</h4>
                                    
                                     <h5><span style="color: red;">{{$data->patientname}}</span> wrote</h5>
                                      <P>{{$data->review_que}}</P>

                                      @if(isset($data->review_ans))
                                      <h5><span style="color: red;">{{$data->first_name}}</span> wrote</h5>
                                      <P>{{$data->review_ans}}</P>
                                      @else 
                                      
                                      @endif

                                      @php $rating = $data->rating; @endphp  

                                        <div style="float: right;">
                                            <div class="placeholder" style="color: lightgray;">
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;"></i>
                                                <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;"></i>
                                                <span style = "color: #db4130;" >({{ $rating }})</span>
                                            </div>

                                            <div class="overlay" style="position: relative;top: -22px;">
                                                
                                                @while($rating>0)
                                                    @if($rating >0.5)
                                                        <i class="fa fa-star" aria-hidden="true" style="font-size: 20px;color: #db4130;"></i>
                                                    @else
                                                       <i class="fa fa-star-half-o" aria-hidden="true" style="font-size: 20px;color: #db4130;"></i>
                                                    @endif
                                                    @php $rating--; @endphp
                                                @endwhile

                                            </div> 
                                       </div>
                                     
                                      </div>
                                    </div>
                               </div>

                          @endforeach


                                                                 
                        @else 
                            <div class="row" style="margin-left: 1%;margin-right: 1%;">
                                   <div class="col-sm-12" style="margin-top: 2%;border: 1px solid #b2b2b2;box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);">

                                    <div class="" style="margin-top: 2%;">
                                          <div class="alert alert-danger">
                                                 <strong>Sorry! </strong> No Reviews Found.
                                            </div>
                                      </div>


                                    </div>
                               </div>

                          @endif

                          </div>






                                  </div>
                                </div>
                              </div>
                                  @include('newview.new.sidemenu.review')
                </div>
                    
            </div>

         </div>
    </div>
    
@endsection
@push('scripts')
<script>
$(document).ready(function(){
  $(".nav-tabs a").click(function(){
    $(this).tab('show');
  });
});
</script>
@endpush

