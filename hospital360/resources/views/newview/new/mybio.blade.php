@extends('layouts.new.header')
@section('title', 'Dr Bio')
@section('content')
<style type="text/css">
    /*.card:hover {
        color: white;
  background-color: red;
}*/
.card{
  height: auto; 
  border-radius: 15px; 
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
}
.card-title{
    margin-top: 5%;
}
.bio{
    border-radius: 17px !important;
    border: 1px solid red;
    margin-left: 5%;
    margin-right: 5%;
    height: 230px;
}
.btn-danger {
    color: #fff;
    background-color: red;
    border-color: red;
}
/*a:hover {
    color: white !important;
    text-decoration: none;
}*/
hr {
    margin-top: 1rem;
    margin-bottom: 1rem;
    border: 0;
    border-top: 1px solid red;
}
.form-control {
   box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
}
label {
    font-size: 15px;
    margin-top: 1%;
}
.pic{
    height: 120px;
    border-radius: 17px !important;
    border: 1px solid red;
}
.alert-success {
    color: white;
    background-color: green;
    border-color: #cdedd8;
}
.parsley-required{
    color: red;
}

.btn2 {
    color: black;
    background-color: white;
    border-color: red;
}

</style>
<!-- <ol class="breadcrumb">
        <li class="breadcrumb-item">Doctors</li>
    </ol> -->
    {!! Form::model($doctors, ['route' => ['doctorsBio.update', $doctors->id], 'method' => 'post', 'files' => true,'id' => 'myForm']) !!}
    <div class="container-fluid">
        <div class="animated fadeIn">
             <div class="container">
               @include('flash::message')
                <div class="row">
                      <div class="col-sm-8">
                         <!-- <div class="row" style="margin-left: 1%;margin-right: 1%;margin-top: 2%;">
                                   <div class="col-sm-6">

                                    <div class="text-center">
                                    {!! Form::text('search', null, ['class' => 'form-control']) !!}
                                    </div>
                                    </div>

                                    <div class="col-sm-2">
                                     <div class="text-center">
                                     <button class="btn btn2" type="button" style="width: 100%;">From</button>
                                    </div>
                                    </div>

                                    <div class="col-sm-2">
                                     <div class="text-center">
                                    <button class="btn btn2" type="button" style="width: 100%;">To</button>
                                    </div>
                                    </div>

                                    <div class="col-sm-2">
                                     <div class="text-center">
                                    <button class="btn btn2" type="button" style="width: 100%;">Search</button>
                                    </div>
                                    </div>

                                </div> -->
                                <h3 class="text-center" style="margin-top: 5%;">My Bio</h3>
                                <div class="row " style="margin-left: 1%;margin-right: 1%;margin-top: 3%;border: 1px solid #b2b2b2;box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);">
                                    <div class="col-sm-12" style="padding: 5%;">
                                       <div class="">
                                        {!! Form::textarea('bio', null, ['class' => 'form-control','rows' =>   '9','data-parsley-required-message' =>'Bio is required','required','placeholder' => 'My Bio']) !!} 
                                      </div>
                                
                                    </div>
                                  <div class="col-sm-6" style="padding: 5%;">
                                       <div class="">
                                          <?php 
                                          $package_category = App\Models\Language::orderBy('language','asc')->get();
                                          $specilities = DB::table('specilities')->orderBy('specility','asc')->get();

                                          ?>
                                          
                                          <select class="form-control" multiple="" data-parsley-required-message="Language is required" required="" name="language[]" data-parsley-multiple="language[]">
        
                                              <?php  $p_category = explode(',', $doctors->language) ?>
                                              @foreach($package_category as $value)
                                                  <option value="{{$value->id}}" @foreach($p_category as $p) @if($value->id == $p)selected="selected"@endif @endforeach>{{$value->language}}</option>
                                              @endforeach
                                          </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-6" style="padding: 5%;">
                                       <div class="">
                                          {{-- Form::select('speciality', [''=>'Select Specilities'] + $specilities->toArray(), null, ['class' => 'form-control','data-parsley-required-message' =>'Speciality is required','required','placeholder' => 'My Specialty']) --}}
                                        </div>
                                        <select class="form-control" multiple="" data-parsley-required-message="Language is required" required="" name="speciality[]" data-parsley-multiple="speciality[]">
        
                                              <?php  $p_speciality = explode(',', $doctors->speciality) ?>
                                              @foreach($specilities as $value1)
                                                  <option value="{{$value1->id}}" @foreach($p_speciality as $p) @if($value1->id == $p)selected="selected"@endif @endforeach>{{$value1->specility}}</option>
                                              @endforeach
                                          </select>
                                    </div>
                                </div>

                                <div class="row" style="margin-left: 10%;margin-right: 10%;margin-top: 2%;">
                                   <div class="col-sm-6" style="padding: 5%;">
                                     <button type="button" class="btn btn2" data-toggle="modal" data-target="#myModal" style="width: 50%;margin-left: -40%;">Public View</button>
                                   </div>
                                   <div class="col-sm-6" style="padding: 5%;">
                                    <button class="btn btn-danger mobilebutton" type="submit" style="width: 50%;margin-left: 90%;">Update</button>
                                   </div>
                                </div>
                              </div>
                                    @include('newview.new.sidemenu.profile')
                </div>
                    
            </div>

         </div>
    </div>
    <div class="container">
  <div class="modal fade" id="myModal">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Doctor Profile</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <!-- Modal body -->
        <div class="modal-body">
             <div class="row">

                          <div class="col-sm-4">
                            <?php  $courceimage = URL::asset('public/media/profile_pic/'.$doctors->profile_pic); ?>

                            <?php
                              $p_category = explode(',', $doctors->language);
                              $p_speciality = explode(',', $doctors->speciality);

                              $package_category = App\Models\Language::whereIn('id',$p_category)->get();
                              
                              $specilities = DB::table('specilities')->whereIn('id',$p_speciality)->get();

                              $temp =array();
                              foreach ($specilities as $value) {
                                $temp[] = $value->specility;
                              }

                              $temp1=array();
                              foreach ($package_category as $value1) {
                                $temp1[] = $value1->language;
                              }

                            ?>
                            <div class="pic">
                                            <img  src="<?= $courceimage ?>" alt="Course" style="width: 80%;margin-top: 5%;margin-left: 9%;margin-right: 4%;max-width: 80%;max-height: 90%;">
                                        </div>
                          </div>

                          <div class="col-sm-8">
                            <h2 style="color: red"> {{$doctors->first_name}} {{$doctors->last_name}}</h2>
                            <span class="dot"></span>  <span class="dot"></span>  <span class="dot"></span>  <span class="dot"></span>
                            <h6>Experience : {{$doctors->experience}}</h6>
                            <h6>Speciality : {{implode(',',$temp)}}</h6>
                            <h6>Language   : {{implode(',',$temp1)}}</h6>
                          </div>
                          <div class="border1" style="margin-top: 3%;">
                          <p style="margin-left: 3%;text-align: justify;">{{$doctors->bio}}</p>
                          </div>
             </div>             
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
        
      </div>
    </div>
  </div>
  
</div>
    {!! Form::close() !!}
@endsection