@extends('layouts.new.header')
@section('title', 'Dr To Start')
@section('content')
<style type="text/css">
    /*.card:hover {
        color: white;
  background-color: red;
}*/
.card{
  height: auto; 
  border-radius: 15px; 
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
}
.card-title{
    margin-top: 5%;

}
.bio{
    border-radius: 10px !important;
    border: 1px solid red;
    height: 75px;

    <?php
    $url1=$_SERVER['REQUEST_URI'];
    header("Refresh: 15; URL=$url1");
?>

}
.btn-danger {
    color: #fff;
    background-color: red;
    border-color: red;
}
/*a:hover {
    color: white !important;
    text-decoration: none;
}*/
hr {
    margin-top: 1rem;
    margin-bottom: 1rem;
    border: 0;
    border-top: 1px solid red;
}
.form-control {
   box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
}
label {
    font-size: 15px;
    margin-top: 1%;
}
.pic{
    height: 84px;
    border-radius: 17px !important;
    border: 1px solid red;
}
.alert-success {
    color: white;
    background-color: green;
    border-color: #cdedd8;
}
.parsley-required{
    color: red;
}

.btn2 {
    color: black;
    background-color: white;
    border-color: red;
}


</style>
<!-- <ol class="breadcrumb">
        <li class="breadcrumb-item">Doctors</li>
    </ol> -->
   
    <div class="container-fluid" style="">
        <div class="animated fadeIn">
             <div class="container">
               
                <div class="row">
                      <div class="col-sm-8" style="">
<!--                          <div class="row" style="margin-left: 1%;margin-right: 1%;margin-top: 2%;">
                                   <div class="col-sm-6">

                                    <div class="text-center">
                                    {!! Form::text('search', null, ['class' => 'form-control']) !!}
                                    </div>
                                    </div>

                                    <div class="col-sm-2">
                                     <div class="text-center">
                                     <button class="btn btn2" type="button" style="width: 100%;">From</button>
                                    </div>
                                    </div>

                                    <div class="col-sm-2">
                                     <div class="text-center">
                                    <button class="btn btn2" type="button" style="width: 100%;">To</button>
                                    </div>
                                    </div>

                                    <div class="col-sm-2">
                                     <div class="text-center">
                                    <button class="btn btn2" type="button" style="width: 100%;">Search</button>
                                    </div>
                                    </div>

                                </div> -->
                                <h1 class="text-center" style="margin-top: 5%;">To Start</h1>
                                <div class="row bio " style="margin-left: 1%;margin-right: 1%;margin-top: 8%;border: 1px solid #b2b2b2;box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);">
                                 <div class="col-sm-2" style="padding: 3%;">

                                    <div class="text-center">
                                    <h5>Token</h5>
                                    </div>
                                    </div>

                                    <div class="col-sm-4" style="padding: 3%;">
                                     <div class="text-center">
                                    <h5>Patient Name</h5>
                                    </div>
                                    </div>

                                    <div class="col-sm-2" style="padding: 3%;">
                                     <div class="text-center">
                                    <h5>Booked Slot</h5>
                                    </div>
                                    </div>

                                    <div class="col-sm-2" style="padding: 3%;">
                                     <div class="text-center">
                                    <h5>Waiting Since</h5>
                                    </div>
                                    </div>

                                    <div class="col-sm-2" style="padding: 3%;">
                                     <div class="text-center">
                                    
                                    </div>
                                    </div>
                                </div>
                           @foreach($patients as $value)

                                <div class="row bio" id="refresh" style="margin-left: 1%;margin-right: 1%;margin-top: 1%;border: 1px solid #b2b2b2;box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);">
                                    <div class="col-sm-2">

                                      <div class="text-center" style="margin-top: 10%;">
                                       <h1 style="color: red;">{{$value->id}}</h1> 
                                      </div>
                                    </div>
                                    <div class="col-sm-4">

                                      <div class="text-center" style="margin-top: 3%;">
                                        <h4 style="color: red;">{{$value->first_name}} {{$value->lastname}}</h4>
                                        <p>Booking ID <span style="color: red;"> {{$value->book_id}}</span> </p>
                                      </div>
                                    </div>

                                    <div class="col-sm-2">

                                      <div class="text-center" style="margin-top: 10%;">
                                        <?php $p_time =  explode(' ',$value->date_time ) ?>
                                        <h4><?php echo $p_time['1']; ?></h4>
                                        </div>
                                    </div>

                                    <div class="col-sm-2">

                                      <div class="text-center" style="margin-top: 10%;">
                                        <h4 style="color: red;">
                                       
                                         <span id="race<?php echo $value['id'];?>"></span> Waiting</h4>
                                        <!-- <input type="hidden" class="mydata" name="" value="{{$value->date_time}}" id="{{$value->id}}"> -->
                                        <!-- <h4 style="color: red;"> {{$value->waiting_time}} Minutes</h4> -->
                                        </div>
                                    </div>

                                    <div class="col-sm-2">

                                      <div class="text-center" style="margin-top: 20%;">
                                        <a href="{{ route('dr_patients_profile',['id' => $value->patient_id,'b_id' => $value->book_id]) }}" class="btn btn-danger active pull-right" role="button" style="width: 90%;">Start</a>
                                        </div>
                                    </div>
                                    <br><br>
                                </div>
                                @endforeach
                              </div>
                              @include('newview.new.sidemenu.consults')
                </div>
                    
            </div>

         </div>
    </div>
    {!! Form::close() !!}
     @push('scripts')
<script src="http://code.jquery.com/jquery-3.1.1.js"></script>
<!-- <script type="text/javascript">
  var yes;
    refresh();

  function refresh() {
  $("#refresh").html();
  setTimeout(refresh, 5000);
  // alert(yes);
}
</script> -->
<!-- <script type="text/javascript">
  function startTimer(duration, display) {
    var timer = duration, minutes, seconds;
    setInterval(function () {
        minutes = parseInt(timer / 60, 10);
        seconds = parseInt(timer % 60, 10);

        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;

        display.textContent = minutes + ":" + seconds;

        if (--timer < 0) {
            timer = duration;
        }
    }, 1000);
}

window.onload = function () {
  var mytime = $('#mytime').val();
    var fiveMinutes = 60 * mytime,
        display = document.querySelector('#time');
    startTimer(fiveMinutes, display);

    if (fiveMinutes === 0) {
      clearTimeout(display);
    }
      

};
</script> -->

<!-- <script type="text/javascript">
  // Set the date we're counting down to


  var mytime = $('.mydata').val();

  // alert(mytime);

var countDownDate = new Date(mytime).getTime();

// Update the count down every 1 second
var x = setInterval(function() {

  // Get today's date and time
  var now = new Date().getTime();
    
  // Find the distance between now and the count down date
  var distance = countDownDate - now;
    
  // Time calculations for days, hours, minutes and seconds
  var days = Math.floor(distance / (1000 * 60 * 60 * 24));
  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  var seconds = Math.floor((distance % (1000 * 60)) / 1000);
  

  // Output the result in an element with id="demo"
  document.getElementById("demo").innerHTML = minutes + "m " + seconds + "s ";
    
  // If the count down is over, write some text 
  if (distance < 0) {
    clearInterval(x);
    document.getElementById("demo").innerHTML = "Expired";
  }


}, 1000);
</script> -->

<script>
// Set the date we're counting down to
var countdowns = [
<?php foreach ($patients as $race) {?>
  {
    id: <?php echo $race['id']; ?>,
    date: new Date("<?php echo $race['date_time'];?>").getTime()
  },
<?php }?>
];

// Update the count down every 1 second
var timer = setInterval(function() {
  // Get todays date and time
  var now = Date.now();

  var index = countdowns.length - 1;

  // we have to loop backwards since we will be removing
  // countdowns when they are finished
  while(index >= 0) {
    var countdown = countdowns[index];

    // Find the distance between now and the count down date
    var distance = countdown.date - now;

    // Time calculations for days, hours, minutes and seconds
    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);

    var timerElement = document.getElementById("race" + countdown.id);

    // If the count down is over, write some text 
    if (distance < 0) {
      timerElement.innerHTML = "EXPIRED";
      // this timer is done, remove it
      countdowns.splice(index, 1);
    } else {
      timerElement.innerHTML =  minutes + "m " + seconds + "s "; 
    }

    index -= 1;
  }

  // if all countdowns have finished, stop timer
  if (countdowns.length < 1) {
    clearInterval(timer);
  }
}, 1000);
</script>

@endpush
@endsection
