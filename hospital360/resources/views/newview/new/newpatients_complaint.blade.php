@extends('layouts.new.header')
@section('title', 'Dr Address')
@section('content')
<style type="text/css">
    /*.card:hover {
        color: white;
  background-color: red;
}*/
.card{
  height: auto; 
  border-radius: 15px; 
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
}
.card-title{
    margin-top: 5%;
}
.bio{
    border-radius: 17px !important;
    border: 1px solid red;
    margin-left: 5%;
    margin-right: 5%;
    height: 230px;
}
.btn-danger {
    color: #fff;
    background-color: red;
    border-color: red;
}
/*a:hover {
    color: white !important;
    text-decoration: none;
}*/
hr {
    margin-top: 1rem;
    margin-bottom: 1rem;
    border: 0;
    border-top: 1px solid red;
}
.form-control {
   box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
}
label {
    font-size: 15px;
    margin-top: 1%;
}
.pic{
    height: 84px;
    border-radius: 17px !important;
    border: 1px solid red;
}
.alert-success {
    color: white;
    background-color: green;
    border-color: #cdedd8;
}
.parsley-required{
    color: red;
}

.btn2 {
    color: black;
    background-color: white;
    border-color: red;
}


</style>
<!-- <ol class="breadcrumb">
        <li class="breadcrumb-item">Doctors</li>
    </ol> -->
    
    <div class="container-fluid">
        <div class="animated fadeIn">
             <div class="container">
               @include('flash::message')
                <div class="row">
                      <div class="col-sm-12">
                                <h1 class="text-center" style="margin-top: 5%;">Present Complaint Pic</h1>
                                <div class="row " style="margin-left: 1%;margin-right: 1%;margin-top: 10%;border: 1px solid #b2b2b2;box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);">
                                    <div class="col-sm-12" style="padding: 5%;">
                                        <?php 
                                    $complaint = explode('$', $complaint_patient->complaint_pic); ?>

                                    <?php foreach ($complaint as $value) { ?>
                                      <div class="col-sm-6">
                                      <img src="<?php echo $pic = env('APP_URL').'public/media/complaint_pic/'.$value ?>" style="width: 50%; height: 50%;"> 
                                    </div>
                                    <?php } ?>
                                </div>
                               
                              </div>
                              
                </div>
                    
            </div>

         </div>
    </div>
    {!! Form::close() !!}
@endsection