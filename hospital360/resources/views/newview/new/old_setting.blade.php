@extends('layouts.new.header')
@section('title', 'Dr Settings')
@section('content')
<style type="text/css">
    /*.card:hover {
        color: white;
  background-color: red;
}*/
.card{
  height: 230px;
  border-radius: 15px; 
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
}
.card-title{
    margin-top: 5%;
}
.bio{
    border-radius: 17px !important;
    border: 1px solid red;
    margin-left: 5%;
    margin-right: 5%;
    height: 230px;
}
.btn-danger {
    color: #fff;
    background-color: red;
    border-color: red;
}
/*a:hover {
    color: white !important;
    text-decoration: none;
}*/
hr {
    margin-top: 1rem;
    margin-bottom: 1rem;
    border: 0;
    border-top: 1px solid red;
}
.form-control {
   box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
}
label {
    font-size: 15px;
    margin-top: 1%;
}
.pic{
    height: 84px;
    border-radius: 17px !important;
    border: 1px solid red;
}
.alert-success {
    color: white;
    background-color: green;
    border-color: #cdedd8;
}
.parsley-required{
    color: red;
}

.btn2 {
    color: black;
    background-color: white;
    border-color: red;
}

</style>
<!-- <ol class="breadcrumb">
        <li class="breadcrumb-item">Doctors</li>
    </ol> -->
    
    <div class="container-fluid">
        <div class="animated fadeIn">
             <div class="container">
               
                <div class="row">
                      <div class="col-sm-8">
<!--                          <div class="row" style="margin-left: 1%;margin-right: 1%;margin-top: 2%;">
                                   <div class="col-sm-6">

                                    <div class="text-center">
                                    {!! Form::text('search', null, ['class' => 'form-control']) !!}
                                    </div>
                                    </div>

                                    <div class="col-sm-2">
                                     <div class="text-center">
                                     <button class="btn btn2" type="button" style="width: 100%;">From</button>
                                    </div>
                                    </div>

                                    <div class="col-sm-2">
                                     <div class="text-center">
                                    <button class="btn btn2" type="button" style="width: 100%;">To</button>
                                    </div>
                                    </div>

                                    <div class="col-sm-2">
                                     <div class="text-center">
                                    <button class="btn btn2" type="button" style="width: 100%;">Search</button>
                                    </div>
                                    </div>

                                </div> -->
                                <h1 class="text-center" style="margin-top: 5%;">Settings</h1>
                                 @include('flash::message')
                                <div class="row " style="margin-left: 1%;margin-right: 1%;margin-top: 8%;border: 1px solid #b2b2b2;box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);padding: 5%;">
                                  
                                        
                            <div class="col-sm-6">
                                <div class="card  newcard text-center border-danger">
                                     <a href="" data-toggle="modal" data-target="#myModal12" style="color: black;"> 
                                    <div class="card-block" style="padding: 50px;">
                                        <i class="fa fa-stethoscope" style="font-size: 80px;"></i>
                                        <h4 class="card-title">Select Non Availability</h4>
                                    </div>
                                    </a>
                                    
                                </div>
                            </div>

                                  <div class="col-sm-6">
                                          <div class="card newcard text-center border-danger">
                                              <a href="" data-toggle="modal" data-target="#myModal11" style="color: black;"> 
                                              <div class="card-block" style="padding: 50px;">
                                                   <i class="fa fa-stethoscope" style="font-size: 80px;"></i>
                                                  <h4 class="card-title">Time Slots</h4>
                                                  
                                              </div>
                                              </a>
                                              
                                          </div>
                                   </div>
                                        
                                    
                                </div>

                              </div>
                                  @include('newview.new.sidemenu.home')
                </div>
                    
            </div>

         </div>
    </div>

<!-- Select non availability day Model -->
  
  <div class="modal fade bmodel" id="myModal12" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title" id="exampleModalLabel" style="color: red;">{{ Auth::user()->first_name }} {{ Auth::user()->last_name }}</h3>
        <a href="" data-toggle="modal" data-target="#myModal10"><button class="btn btn-danger amodel" type="button" style="float: left;"><i class="fa fa-plus-circle" aria-hidden="true" style="color: #ffff;font-size: 20px;"></i> Add non availability Days</button></a>
      </div>
      <div class="modal-body" style="height: 500px;overflow: scroll;">

      <div class="table-responsive" style="border: 1px solid #f86c6b;box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);">          
                                        <table class="table">
                                          <thead>
                                            <tr>
                                              <th>Non Availability Dates</th>
                                              <th>Actions</th>
                                            </tr>
                                          </thead>
                                          <tbody>
                                             <?php if(isset($doctorsnon)) { ?>
                                             @if (!$doctorsnon->isEmpty())
                                             @foreach($doctorsnon as $value)
                                            <tr>
                                              <td> {{$value->date}}</td>
                                              <td>
                                              {!! Form::open(['route' => ['doctorAvailabilities.destroy', $value->id], 'method' => 'delete']) !!}
                                                <div class='btn-group'>
                                                    {!! Form::button('<i class="fa fa-check-circle" aria-hidden="true" style="color: #ffff;font-size: 20px;"></i> Available</td>', [
                                                        'type' => 'submit',
                                                        'class' => 'btn btn-danger btn-xs',
                                                        'onclick' => "return confirm('Are you sure?')"
                                                    ]) !!}
                                                </div>
                                            {!! Form::close() !!}

                                          </td>
                                                
                                            </tr>
                                          </tbody>
                                           @endforeach
                                           @else
                                           <h3 class="text-center" style="margin-top: 5%;"> No Records Found</h3>
                                           @endif
                                         <?php } ?>
                                        </table>
                   </div>
      </div>
      <div class="modal-footer">
                 <button class="btn btn-danger" type="button" data-dismiss="modal">Close</button> 
      </div>
    </div>
  </div>
</div>


  <div class="modal fade cmodel" id="myModal10" role="dialog">
    <div class="modal-dialog">
    {!! Form::open(['route' => 'dt_doctor_availability.store','id' => 'myForm']) !!}
      <!-- Modal content-->
      <div class="modal-content">  

        <div class="modal-body">
         
           <h3 style="color: red;">Select non availability day</h3>
          
            <div style="padding: 5%;">

            {!! Form::label('doctor_id', 'Doctor Name:') !!}
            <input type="text" name="" class="form-control" value = "{{Auth::user()->first_name}}" readonly="readonly">
            <input type = "hidden" id="doctor_id" name = "doctor_id" value = "{{Auth::user()->userDetailsId}}">

            {!! Form::label('date', 'Date:') !!}
            {!! Form::text('date', null, ['class' => 'form-control','id'=>'timeSlot','data-parsley-required-message' =>'Date is required','required']) !!}

          </div>
          <div class="modal-footer">
             <button class="btn btn-danger" type="submit">Submit</button>
                  <button class="btn btn-danger" type="button" data-dismiss="modal">Close</button> 
         </div>
          
        </div>
      </div>
        {!! Form::close() !!}
    </div>
  </div>


<!-- Time Slots Model -->

  <div class="modal fade cmodel" id="myModal11" role="dialog">
    <div class="modal-dialog">
    @if(isset($doctorstime->id))
     {!! Form::model($doctorstime, ['route' => ['tiemslots_update.update', $doctorstime->id], 'method' => 'post','id' => 'myForm']) !!}
     @else
     {!! Form::open(['route' => 'timeSlots.store','id' => 'myForm']) !!}
    @endif
      <!-- Modal content-->
      <div class="modal-content">  
          
        <div class="modal-body">
           <h2 style="color: red;">Select Time Slots</h2>
          <br>
         <div class="row" style="margin-left: 1%;margin-right: 1%;">
           <div class="col-sm-6">
            {!! Form::label('doctor_id', 'Doctor Name:') !!}
                    <input type="text" name="" class="form-control" value = "{{Auth::user()->first_name}}" readonly="readonly">
                    <input type = "hidden" id="doctor_id" name = "doctor_id" value = "{{Auth::user()->userDetailsId}}">
           </div>
            <div class="col-sm-6">
              {!! Form::label('date', 'Booking Time In Minute:') !!}
                 {!! Form::text('booking_time_slot', null, ['class' => 'form-control','data-parsley-required-message' =>'Booking Time In Minute is required','data-parsley-type' =>'digits','required']) !!}
           </div>
         </div>
        <div class="row" style="margin-left: 1%;margin-right: 1%;margin-top: 5%;">
            <div class="col-sm-4">
                      <div style="">
                            <h4>Morning Time:</h4> <br><br>
                            <h4>Afternoon Time:</h4> <br><br>
                            <h4>Evening Time:</h4> <br><br>
                      </div>
              </div>
              <div class="col-sm-4">
                                   
                  {!! Form::label('from_time', 'From Time:') !!}
                  {!! Form::text('mrg_from_time', null, ['class' => 'form-control','id'=>'mrg_from_time','data-parsley-required-message' =>'From Time is required','required']) !!}
                  <br>
                  {!! Form::label('from_time', 'From Time:') !!}
                  {!! Form::text('aft_from_time', null, ['class' => 'form-control','id'=>'aft_from_time','data-parsley-required-message' =>'From Time is required','required']) !!}
                  <br>
                 {!! Form::label('from_time', 'From Time:') !!}
                 {!! Form::text('eve_from_time', null, ['class' => 'form-control','id'=>'eve_from_time','data-parsley-required-message' =>'From Time is required','required']) !!}
                  <br>                      
                 
                    </div>
            <div class="col-sm-4">

                {!! Form::label('to_time', 'To Time:') !!}
                {!! Form::text('mrg_to_time', null, ['class' => 'form-control','id'=>'mrg_to_time','data-parsley-required-message' =>'To Time is required','required']) !!}

                <br>
                {!! Form::label('to_time', 'To Time:') !!}
                {!! Form::text('aft_to_time', null, ['class' => 'form-control','id'=>'aft_to_time','data-parsley-required-message' =>'To Time is required','required']) !!}
                <br>
                {!! Form::label('to_time', 'To Time:') !!}
                 {!! Form::text('eve_to_time', null, ['class' => 'form-control','id'=>'eve_to_time','data-parsley-required-message' =>'To Time is required','required']) !!}

            </div>
      </div>
          <div class="text-center" style="margin-top: 3%;">
                 <!-- <button class="btn btn-danger" type="submit" id="submit10" data-dismiss="modal" style="width: 20%;">Submit</button> -->
                 <button class="btn btn-danger" type="submit" style="width: 40%;">Submit</button>
          </div>
        </div>
      </div>
        {!! Form::close() !!}
    </div>
  </div>


 @push('scripts')
<script>
    $(document).ready(function(){
      $(".amodel").click(function(){
        $(".bmodel").modal('hide')
      });
    });
</script>

   <script type="text/javascript">
           $('#timeSlot').datetimepicker({
               format: 'YYYY-MM-DD',
                minDate:new Date(),
               useCurrent: true,
               icons: {
                   up: "icon-arrow-up-circle icons font-2xl",
                   down: "icon-arrow-down-circle icons font-2xl"
               },
               sideBySide: true
           })
       </script>

       <script type="text/javascript">
           $('#mrg_from_time').datetimepicker({
               format: 'HH:mm',
               useCurrent: true,
               icons: {
                   up: "icon-arrow-up-circle icons font-2xl",
                   down: "icon-arrow-down-circle icons font-2xl"
               },
               sideBySide: true
           })
       </script>

       <script type="text/javascript">
           $('#aft_from_time').datetimepicker({
               format: 'HH:mm',
               useCurrent: true,
               icons: {
                   up: "icon-arrow-up-circle icons font-2xl",
                   down: "icon-arrow-down-circle icons font-2xl"
               },
               sideBySide: true
           })
       </script>


       <script type="text/javascript">
           $('#eve_from_time').datetimepicker({
               format: 'HH:mm',
               useCurrent: true,
               icons: {
                   up: "icon-arrow-up-circle icons font-2xl",
                   down: "icon-arrow-down-circle icons font-2xl"
               },
               sideBySide: true
           })
       </script>

       <script type="text/javascript">
           $('#mrg_to_time').datetimepicker({
               format: 'HH:mm',
               useCurrent: true,
               icons: {
                   up: "icon-arrow-up-circle icons font-2xl",
                   down: "icon-arrow-down-circle icons font-2xl"
               },
               sideBySide: true
           })
       </script>

       <script type="text/javascript">
           $('#aft_to_time').datetimepicker({
               format: 'HH:mm',
               useCurrent: true,
               icons: {
                   up: "icon-arrow-up-circle icons font-2xl",
                   down: "icon-arrow-down-circle icons font-2xl"
               },
               sideBySide: true
           })
       </script>

       <script type="text/javascript">
           $('#eve_to_time').datetimepicker({
               format: 'HH:mm',
               useCurrent: true,
               icons: {
                   up: "icon-arrow-up-circle icons font-2xl",
                   down: "icon-arrow-down-circle icons font-2xl"
               },
               sideBySide: true
           })
       </script>
@endpush

@endsection

