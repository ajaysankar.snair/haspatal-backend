@extends('layouts.new.header')
@section('title', 'Dr Waiting Room')
@section('content')
<style type="text/css">
    /*.card:hover {
        color: white;
  background-color: red;
}*/
.card{
  height: auto; 
  border-radius: 15px; 
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
}
.card-title{
    margin-top: 5%;
}
.bio{
    border-radius: 17px !important;
    border: 1px solid red;
    margin-left: 5%;
    margin-right: 5%;
    height: 100px;
}
.btn-danger {
    color: #fff;
    background-color: red;
    border-color: red;
}
/*a:hover {
    color: white !important;
    text-decoration: none;
}*/
hr {
    margin-top: 1rem;
    margin-bottom: 1rem;
    border: 0;
    border-top: 1px solid red;
}
.form-control {
   box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
}
label {
    font-size: 15px;
    margin-top: 1%;
}
.pic{
    height: 84px;
    border-radius: 17px !important;
    border: 1px solid red;
}
.alert-success {
    color: white;
    background-color: green;
    border-color: #cdedd8;
}
.parsley-required{
    color: red;
}

.btn2 {
    color: black;
    background-color: white;
    border-color: red;
}

</style>
<!-- <ol class="breadcrumb">
        <li class="breadcrumb-item">Doctors</li>
    </ol> -->
    <div class="container-fluid">
        <div class="animated fadeIn">
             <div class="container">
               
                <div class="row">
                      <div class="col-sm-8">
<!--                          <div class="row" style="margin-left: 1%;margin-right: 1%;margin-top: 2%;">
                                   <div class="col-sm-6">

                                    <div class="text-center">
                                    {!! Form::text('search', null, ['class' => 'form-control']) !!}
                                    </div>
                                    </div>

                                    <div class="col-sm-2">
                                     <div class="text-center">
                                     <button class="btn btn2" type="button" style="width: 100%;">From</button>
                                    </div>
                                    </div>

                                    <div class="col-sm-2">
                                     <div class="text-center">
                                    <button class="btn btn2" type="button" style="width: 100%;">To</button>
                                    </div>
                                    </div>

                                    <div class="col-sm-2">
                                     <div class="text-center">
                                    <button class="btn btn2" type="button" style="width: 100%;">Search</button>
                                    </div>
                                    </div>

                                </div> -->
                                <h1 class="text-center" style="margin-top: 5%;">Waiting Room</h1>
                                <div class="row " style="margin-left: 1%;margin-right: 1%;margin-top: 8%;border: 1px solid #b2b2b2;box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);">
                                  <div class="col-sm-4" style="padding: 5%;">
                                      <div class="text-center bio form-control">
                                     <h3 style="margin-top: 20%;">{{ $allcount }}</h3>
                                    </div>
                                       
                                       <div class="text-center" style="margin-top: 10%;">
                                     <h4>Total Appointments</h4> 
                                      </div>  
                                    </div>
                                    <div class="col-sm-4" style="padding: 5%;">
                                        
                                      <div class="text-center bio form-control">
                                     <h3 style="margin-top: 20%;">{{$check_count}}</h3>
                                    </div>
                                        <div class="text-center" style="margin-top: 10%;">
                                     <h4>Checked-in So Far</h4>
                                      </div> 
                                    </div>
                                    <div class="col-sm-4" style="padding: 5%;">
                                        
                                       
                                       <div class="text-center bio form-control">
                                    <h3 style="margin-top: 20%;">{{ $waiting_count }}</h3>
                                    </div>
                                     <div class="text-center" style="margin-top: 10%;">
                                     <h4>Patients in Waiting Room</h4>
                                      </div>
                                    </div>
                                </div>

                              </div>
                                  @include('newview.new.sidemenu.consults')
                </div>
                    
            </div>

         </div>
    </div>
    {!! Form::close() !!}
@endsection