@extends('layouts.new.header')
@section('title', 'Dr Refund Request')
@section('content')
<style type="text/css">
    /*.card:hover {
        color: white;
  background-color: red;
}*/
.card{
  height: auto; 
  border-radius: 15px; 
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
}
.card-title{
    margin-top: 5%;
}
.bio{
    border-radius: 17px !important;
    border: 1px solid red;
    margin-left: 5%;
    margin-right: 5%;
    height: 230px;
}
.btn-danger {
    color: #fff;
    background-color: red;
    border-color: red;
}
/*a:hover {
    color: white !important;
    text-decoration: none;
}*/
hr {
    margin-top: 1rem;
    margin-bottom: 1rem;
    border: 0;
    border-top: 1px solid red;
}
.form-control {
   box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
}
label {
    font-size: 15px;
    margin-top: 1%;
}
.pic{
    height: 84px;
    border-radius: 17px !important;
    border: 1px solid red;
}
.alert-success {
    color: white;
    background-color: green;
    border-color: #cdedd8;
}
.parsley-required{
    color: red;
}

.btn2 {
    color: black;
    background-color: white;
    border-color: red;
}

</style>
<!-- <ol class="breadcrumb">
        <li class="breadcrumb-item">Doctors</li>
    </ol> -->
    {!! Form::open(['route' => 'doctor_withdraw.store', 'files' => true,'id' => 'myForm']) !!}
    <div class="container-fluid">
        <div class="animated fadeIn">
             <div class="container">
              @include('flash::message')
                <div class="row">
                      <div class="col-sm-8">
<!--                          <div class="row" style="margin-left: 1%;margin-right: 1%;margin-top: 2%;">
                                   <div class="col-sm-6">

                                    <div class="text-center">
                                    {!! Form::text('main_mobile', null, ['class' => 'form-control']) !!}
                                    </div>
                                    </div>

                                    <div class="col-sm-2">
                                     <div class="text-center">
                                     <button class="btn btn2" type="button" style="width: 100%;">From</button>
                                    </div>
                                    </div>

                                    <div class="col-sm-2">
                                     <div class="text-center">
                                    <button class="btn btn2" type="button" style="width: 100%;">To</button>
                                    </div>
                                    </div>

                                    <div class="col-sm-2">
                                     <div class="text-center">
                                    <button class="btn btn2" type="button" style="width: 100%;">Search</button>
                                    </div>
                                    </div>

                                </div> -->
                                <h1 class="text-center" style="margin-top: 5%;">Refund Request</h1>
                                <div class="row " style="margin-left: 1%;margin-right: 1%;margin-top: 8%;border: 1px solid #b2b2b2;box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);">
                                  <div class="col-sm-12" style="padding: 5%;">
                                        
                                      <label for="main_mobile"></label>
                                      <input type="text" id="total_amount" name="current_balance" class="form-control" value="<? echo $total_amount; ?>" readonly placeholder="Current Available Balance">

                                        <label for="requested_amount"></label>
                                        {!! Form::text('requested_amount', null, ['class' => 'form-control','id' => 'r_amount','data-parsley-required-message' =>'Requested Amount is required','required','placeholder' => 'Requested Amount']) !!}


                                        <label for="available_balance"></label>
                                        {!! Form::text('available_balance', null, ['class' => 'form-control','data-parsley-required-message' =>'Available Balance After Request is required','required','id' => 'available_balance','readonly','placeholder' => 'Available Balance After Request']) !!}
                                        
                                    </div>
                                </div>
                                <h5 class="text-center">Refund Request are processed within 2 working days</h5>
                                <div class="text-center" style="margin-top: 5%;margin-left: 80%;">
                                  <button class="btn btn-danger" type="submit" style="width: 90%;">Submit</button>
                                </div>
                              </div>
                                @include('newview.new.sidemenu.wallet')
                </div>
                    
            </div>

         </div>
    </div>
    {!! Form::close() !!}
@endsection
<script type="text/javascript" src="http://code.jquery.com/jquery-1.4.3.min.js" ></script>
<script type="text/javascript">
    $(document).ready(function(){
         $("#r_amount").keyup(function(){
            //alert($(this).val());
              var bla = $('#total_amount').val();
              var r_amount = $(this).val();

              var total = (bla - r_amount);
              $('#available_balance').val(total);
              
        });
    })
</script>