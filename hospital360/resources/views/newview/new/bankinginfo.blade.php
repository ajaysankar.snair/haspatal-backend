@extends('layouts.new.header')
@section('title', 'Dr Banking Information')
@section('content')
<style type="text/css">
    /*.card:hover {
        color: white;
  background-color: red;
}*/
.card{
  height: auto; 
  border-radius: 15px; 
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
}
.card-title{
    margin-top: 5%;
}
.bio{
    border-radius: 17px !important;
    border: 1px solid red;
    margin-left: 5%;
    margin-right: 5%;
    height: 230px;
}
.btn-danger {
    color: #fff;
    background-color: red;
    border-color: red;
}
/*a:hover {
    color: white !important;
    text-decoration: none;
}*/
hr {
    margin-top: 1rem;
    margin-bottom: 1rem;
    border: 0;
    border-top: 1px solid red;
}
.form-control {
   box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
}
label {
    font-size: 15px;
    margin-top: 1%;
}
.pic{
    height: 84px;
    border-radius: 17px !important;
    border: 1px solid red;
}
.alert-success {
    color: white;
    background-color: green;
    border-color: #cdedd8;
}
.parsley-required{
    color: red;
}

.btn2 {
    color: black;
    background-color: white;
    border-color: red;
}

</style>
<!-- <ol class="breadcrumb">
        <li class="breadcrumb-item">Doctors</li>
    </ol> -->
    {!! Form::model($doctors, ['route' => ['doctorsBio.update', $doctors->id], 'method' => 'post', 'files' => true,'id' => 'myForm']) !!} 
    <div class="container-fluid">
        <div class="animated fadeIn">
             <div class="container">
               @include('flash::message')
                <div class="row">
                      <div class="col-sm-8">
                         <!-- <div class="row" style="margin-left: 1%;margin-right: 1%;margin-top: 2%;">
                                   <div class="col-sm-6">

                                    <div class="text-center">
                                    {!! Form::text('search', null, ['class' => 'form-control']) !!}
                                    </div>
                                    </div>

                                    <div class="col-sm-2">
                                     <div class="text-center">
                                     <button class="btn btn2" type="button" style="width: 100%;">From</button>
                                    </div>
                                    </div>

                                    <div class="col-sm-2">
                                     <div class="text-center">
                                    <button class="btn btn2" type="button" style="width: 100%;">To</button>
                                    </div>
                                    </div>

                                    <div class="col-sm-2">
                                     <div class="text-center">
                                    <button class="btn btn2" type="button" style="width: 100%;">Search</button>
                                    </div>
                                    </div>

                                </div> -->
                                <h1 class="text-center" style="margin-top: 5%;">Banking Information</h1>
                                <div class="row " style="margin-left: 1%;margin-right: 1%;margin-top: 8%;border: 1px solid #b2b2b2;box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);">
                                    <div class="col-sm-6" style="padding: 5%;">

                                        <label for="bank_name"></label>
                                        {!! Form::text('bank_name', null, ['class' => 'form-control','data-parsley-required-message' =>'Bank Name is required','required','placeholder' => 'Bank Name']) !!}


                                        <label for="ifsc_code"></label>
                                        {!! Form::text('ifsc_code', null, ['class' => 'form-control','data-parsley-required-message' =>'IFSC Code is required','required','placeholder' => 'IFSC Code']) !!}


                                        
                                    </div>
                                     <div class="col-sm-6" style="padding: 5%;">
                                        <label for="account_name"></label>
                                        {!! Form::text('account_name', null, ['class' => 'form-control','data-parsley-required-message' =>'Account Name is required','required','placeholder' => 'Account Name']) !!}

                                        <label for="account_no"></label>
                                        {!! Form::text('account_no', null, ['class' => 'form-control','data-parsley-required-message' =>'Account Number is required','required','placeholder' => 'Account Number']) !!}
                                     </div>

                                <div class="col-sm-6" style="padding: 5%;">
                                   <div style="margin-left: 3%;margin-right: 3%;">
                                       <label >Upload Cancelled Cheque Picture</label> 
                                        {!! Form::file('cheque_img',['class' => 'form-control']) !!}
                                 </div> 
                                 
                                 <?php  $courceimage = URL::asset('public/media/cheque_img/'.$doctors->cheque_img); ?>
                                  <div class="col-sm-offset-2 col-sm-3" style="margin-left: 113%;">
                                             <img class="" src="<?= $courceimage ?>" alt="Logo" style="width: 181%;margin-top: -225%;margin-left: 280%;">
                                        </div>
                                 </div> 
                                </div>
                                <div class="text-center" style="margin-top: 5%;margin-left: 80%;">
                                  <button class="btn btn-danger" type="submit" style="width: 90%;">Submit</button>
                                </div>
                              </div>
                              @include('newview.new.sidemenu.wallet')

                </div>
                    
            </div>

         </div>
    </div>
    {!! Form::close() !!}
@endsection