@extends('layouts.new.header')
@section('title', 'Home')
@section('content')
<style type="text/css">
    /*.card:hover {
        color: white;
  background-color: red;
}*/
.card{
  height: auto; 
  border-radius: 15px; 
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
}
.card-title{
    margin-top: 5%;

}
.bio{
    border-radius: 10px !important;
    border: 1px solid red;
    height: 75px;
}
.btn-danger {
    color: #fff;
    background-color: red;
    border-color: red;
}
/*a:hover {
    color: white !important;
    text-decoration: none;
}*/
hr {
    margin-top: 1rem;
    margin-bottom: 1rem;
    border: 0;
    border-top: 1px solid red;
}
.form-control {
   box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
}
label {
    font-size: 15px;
    margin-top: 1%;
}
.border{
    
    border-radius: 17px !important;
    
}
.alert-success {
    color: white;
    background-color: green;
    border-color: #cdedd8;
}
.parsley-required{
    color: red;
}

.btn2 {
    color: black;
    background-color: white;
    border-color: red;
}


</style>
</style>
<!-- <ol class="breadcrumb">
        <li class="breadcrumb-item">Doctors</li>
    </ol> -->
   {!! Form::open(['route' => 'homepagedetails', 'files' => true,'id' => 'myForm']) !!} 
    <div class="container-fluid">
        <div class="animated fadeIn">
             <div class="container">
              <?php 


                $r = $_SERVER['REQUEST_URI']; 
                $x = explode('/', $r);
                $z = $x[2];
                
                //  $y = explode(',', $z);
                // $a = end($y);
                
                // echo '<pre>'; print_r($z); exit();
              ?>

                <div class="row">
                      <div class="col-sm-8">
                         <div class="row" style="margin-left: 1%;margin-right: 1%;">
                                   <div class="col-sm-4">

                                    <div class="text-center">
                                     
                                    {!! Form::select('patient_id', [''=>'Select Patient'] + $patient_name->toArray(), null, ['class' => 'form-control','data-parsley-required-message' =>'Patient is required','required']) !!}
                                    </div>
                                    </div>

                                    <div class="col-sm-3">
                                     <div class="text-center">
                                      {!! Form::text('s_date', null, ['class' => 'form-control','id'=>'start_date','data-parsley-required-message' =>'Date is required','required','placeholder' => 'Start Date']) !!}
                                    </div>
                                    </div>

                                    <div class="col-sm-3">
                                     <div class="text-center">
                                      {!! Form::text('e_date', null, ['class' => 'form-control','id'=>'end_date','data-parsley-required-message' =>'Date is required','required','placeholder' => 'End Date']) !!}
                                    </div>
                                    </div>

                                    <div class="col-sm-2">
                                     <div class="text-center">
                                    <button class="btn btn-danger" type="submit" style="width: 100%;">Search</button>
                                    </div>
                                    </div>

                                </div>
                              <input type="hidden" id="ddlYears" value="{{$z}}">
                                <h3 class="text-center" style="margin-top: 5%;">My Haspatal Schedule</h3>
                                <div class="row " style="margin-left: 1%;margin-right: 1%;margin-top: 5%;margin-bottom: 2%;border: 1px solid #b2b2b2;box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);">
                                    <div class="col-sm-12">
                                        <div class="row" style="margin-left: 40%;">
                                          <h2 class="text-center">This Year</h2>
                                                                               
                                        </div>
                                    </div>
                                    <div class="col-sm-6" style="margin-bottom: 5%;">
                                        <div class="" style="margin-left: 1%;margin-right: 1%;margin-top: 10%;">
                                              
                                          <div class="row bio" style="margin-left: 1%;margin-right: 1%;margin-top: 1%;margin-bottom: 5%;border: 1px solid #b2b2b2;box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);">
                                            <div class="col-sm-8">

                                              <div style="margin-top: 10%;margin-left: 10%;">
                                               <h5>January</h5> 
                                              </div>
                                            </div>
                                            <div class="col-sm-2">

                                              <div class="text-center" style="margin-top: 80%;">
                                                <h5 id="jan"></h5>
                                              </div>
                                            </div>

                                            <div class="col-sm-2">
                                               <a href="{{ url('/shiftsdetails',['year' => $z,'startdate' => '-01-01','enddate' => '-01-31']) }}" style="color: black;"> 
                                              <div class="text-center" style="margin-top: 15%;">
                                               <h5 style="color: red;font-weight: bolder;font-size: 35px;">></h5>
                                              </div>
                                            </a>
                                            </div>

                                            <br><br>
                                          </div>
                                          <div class="row bio" style="margin-left: 1%;margin-right: 1%;margin-top: 1%;margin-bottom: 5%;border: 1px solid #b2b2b2;box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);">
                                              <div class="col-sm-8">

                                                <div style="margin-top: 10%;margin-left: 10%;">
                                                 <h5>February</h5> 
                                                </div>
                                              </div>
                                              <div class="col-sm-2">

                                                <div class="text-center" style="margin-top: 80%;">
                                                  <h5 id="feb"></h5>
                                                </div>
                                              </div>

                                              <div class="col-sm-2">
                                                <a href="{{ url('/shiftsdetails',['year' => $z,'startdate' => '-02-01','enddate' => '-02-29']) }}" style="color: black;">
                                                <div class="text-center" style="margin-top: 15%;">
                                                 <h5 style="color: red;font-weight: bolder;font-size: 35px;">></h5>
                                                </div>
                                              </a>
                                              </div>

                                              <br><br>
                                          </div>
                                          <div class="row bio" style="margin-left: 1%;margin-right: 1%;margin-top: 1%;margin-bottom: 5%;border: 1px solid #b2b2b2;box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);">
                                              <div class="col-sm-8">

                                                <div style="margin-top: 10%;margin-left: 10%;">
                                                 <h5>March</h5> 
                                                </div>
                                              </div>
                                              <div class="col-sm-2">

                                                <div class="text-center" style="margin-top: 80%;">
                                                  <h5 id="mar"></h5>
                                                </div>
                                              </div>

                                              <div class="col-sm-2">
                                                <a href="{{ url('/shiftsdetails',['year' => $z,'startdate' => '-03-01','enddate' => '-03-31']) }}" style="color: black;">
                                                <div class="text-center" style="margin-top: 15%;">
                                                 <h5 style="color: red;font-weight: bolder;font-size: 35px;">></h5>
                                                </div>
                                              </a>
                                              </div>

                                              <br><br>
                                          </div>
                                          <div class="row bio" style="margin-left: 1%;margin-right: 1%;margin-top: 1%;margin-bottom: 5%;border: 1px solid #b2b2b2;box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);">
                                              <div class="col-sm-8">

                                                <div style="margin-top: 10%;margin-left: 10%;">
                                                 <h5>April</h5> 
                                                </div>
                                              </div>
                                              <div class="col-sm-2">

                                                <div class="text-center" style="margin-top: 80%;">
                                                  <h5 id="apr"></h5>
                                                </div>
                                              </div>

                                              <div class="col-sm-2">
                                                <a href="{{ url('/shiftsdetails',['year' => $z,'startdate' => '-04-01','enddate' => '-04-31']) }}" style="color: black;">
                                                <div class="text-center" style="margin-top: 15%;">
                                                 <h5 style="color: red;font-weight: bolder;font-size: 35px;">></h5>
                                                </div>
                                              </a>
                                              </div>

                                              <br><br>
                                          </div>
                                          <div class="row bio" style="margin-left: 1%;margin-right: 1%;margin-top: 1%;margin-bottom: 5%;border: 1px solid #b2b2b2;box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);">
                                              <div class="col-sm-8">

                                                <div style="margin-top: 10%;margin-left: 10%;">
                                                 <h5>May</h5> 
                                                </div>
                                              </div>
                                              <div class="col-sm-2">

                                                <div class="text-center" style="margin-top: 80%;">
                                                  <h5 id="may"></h5>
                                                </div>
                                              </div>

                                              <div class="col-sm-2">
                                                <a href="{{ url('/shiftsdetails',['year' => $z,'startdate' => '-05-01','enddate' => '-05-31']) }}" style="color: black;">
                                                <div class="text-center" style="margin-top: 15%;">
                                                 <h5 style="color: red;font-weight: bolder;font-size: 35px;">></h5>
                                                </div>
                                              </a>
                                              </div>

                                              <br><br>
                                          </div>
                                          <div class="row bio" style="margin-left: 1%;margin-right: 1%;margin-top: 1%;margin-bottom: 5%;border: 1px solid #b2b2b2;box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);">
                                              <div class="col-sm-8">

                                                <div style="margin-top: 10%;margin-left: 10%;">
                                                 <h5>June</h5> 
                                                </div>
                                              </div>
                                              <div class="col-sm-2">

                                                <div class="text-center" style="margin-top: 80%;">
                                                  <h5 id="jun"></h5>
                                                </div>
                                              </div>

                                              <div class="col-sm-2">
                                                <a href="{{ url('/shiftsdetails',['year' => $z,'startdate' => '-06-01','enddate' => '-06-31']) }}" style="color: black;">
                                                <div class="text-center" style="margin-top: 15%;">
                                                 <h5 style="color: red;font-weight: bolder;font-size: 35px;">></h5>
                                                </div>
                                              </a>
                                              </div>

                                              <br><br>
                                          </div>
                                                 
                                        </div>

                                    </div>

                                    <div class="col-sm-6" style="margin-top: 4%;margin-bottom: 5%;">                               
                                      <div class="row bio" style="margin-left: 1%;margin-right: 1%;margin-top: 1%;margin-bottom: 5%;border: 1px solid #b2b2b2;box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);">
                                          <div class="col-sm-8">

                                            <div style="margin-top: 10%;margin-left: 10%;">
                                             <h5>July</h5> 
                                            </div>
                                          </div>
                                          <div class="col-sm-2">

                                            <div class="text-center" style="margin-top: 80%;">
                                              <h5 id="jul"></h5>
                                            </div>
                                          </div>

                                          <div class="col-sm-2">
                                            <a href="{{ url('/shiftsdetails',['year' => $z,'startdate' => '-07-01','enddate' => '-07-31']) }}" style="color: black;">
                                            <div class="text-center" style="margin-top: 15%;">
                                             <h5 style="color: red;font-weight: bolder;font-size: 35px;">></h5>
                                            </div>
                                          </a>
                                          </div>

                                          <br><br>
                                      </div>
                                      <div class="row bio" style="margin-left: 1%;margin-right: 1%;margin-top: 1%;margin-bottom: 5%;border: 1px solid #b2b2b2;box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);">
                                          <div class="col-sm-8">

                                            <div style="margin-top: 10%;margin-left: 10%;">
                                             <h5>August</h5> 
                                            </div>
                                          </div>
                                          <div class="col-sm-2">

                                            <div class="text-center" style="margin-top: 80%;">
                                              <h5 id="aug"></h5>
                                            </div>
                                          </div>

                                          <div class="col-sm-2">
                                            <a href="{{ url('/shiftsdetails',['year' => $z,'startdate' => '-08-01','enddate' => '-08-31']) }}" style="color: black;">
                                            <div class="text-center" style="margin-top: 15%;">
                                             <h5 style="color: red;font-weight: bolder;font-size: 35px;">></h5>
                                            </div>
                                          </a>
                                          </div>

                                          <br><br>
                                      </div>
                                      <div class="row bio" style="margin-left: 1%;margin-right: 1%;margin-top: 1%;margin-bottom: 5%;border: 1px solid #b2b2b2;box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);">
                                          <div class="col-sm-8">

                                            <div style="margin-top: 10%;margin-left: 10%;">
                                             <h5>September</h5> 
                                            </div>
                                          </div>
                                          <div class="col-sm-2">

                                            <div class="text-center" style="margin-top: 80%;">
                                              <h5 id="sep"></h5>
                                            </div>
                                          </div>

                                          <div class="col-sm-2">
                                            <a href="{{ url('/shiftsdetails',['year' => $z,'startdate' => '-09-01','enddate' => '-09-31']) }}" style="color: black;">
                                            <div class="text-center" style="margin-top: 15%;">
                                             <h5 style="color: red;font-weight: bolder;font-size: 35px;">></h5>
                                            </div>
                                          </a>
                                          </div>

                                          <br><br>
                                      </div>
                                      <div class="row bio" style="margin-left: 1%;margin-right: 1%;margin-top: 1%;margin-bottom: 5%;border: 1px solid #b2b2b2;box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);">
                                          <div class="col-sm-8">

                                            <div style="margin-top: 10%;margin-left: 10%;">
                                             <h5>October</h5> 
                                            </div>
                                          </div>
                                          <div class="col-sm-2">

                                            <div class="text-center" style="margin-top: 80%;">
                                              <h5 id="oct"></h5>
                                            </div>
                                          </div>

                                          <div class="col-sm-2">
                                            <a href="{{ url('/shiftsdetails',['year' => $z,'startdate' => '-10-01','enddate' => '-10-31']) }}" style="color: black;">
                                            <div class="text-center" style="margin-top: 15%;">
                                             <h5 style="color: red;font-weight: bolder;font-size: 35px;">></h5>
                                            </div>
                                          </a>
                                          </div>

                                          <br><br>
                                      </div>
                                      <div class="row bio" style="margin-left: 1%;margin-right: 1%;margin-top: 1%;margin-bottom: 5%;border: 1px solid #b2b2b2;box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);">
                                          <div class="col-sm-8">

                                            <div style="margin-top: 10%;margin-left: 10%;">
                                             <h5>November</h5> 
                                            </div>
                                          </div>
                                          <div class="col-sm-2">

                                            <div class="text-center" style="margin-top: 80%;">
                                              <h5 id="nov"></h5>
                                            </div>
                                          </div>

                                          <div class="col-sm-2">
                                            <a href="{{ url('/shiftsdetails',['year' => $z,'startdate' => '-11-01','enddate' => '-11-31']) }}" style="color: black;">
                                            <div class="text-center" style="margin-top: 15%;">
                                             <h5 style="color: red;font-weight: bolder;font-size: 35px;">></h5>
                                            </div>
                                          </a>
                                          </div>

                                          <br><br>
                                      </div>
                                      <div class="row bio" style="margin-left: 1%;margin-right: 1%;margin-top: 1%;margin-bottom: 5%;border: 1px solid #b2b2b2;box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);">
                                          <div class="col-sm-8">

                                            <div style="margin-top: 10%;margin-left: 10%;">
                                             <h5>December</h5> 
                                            </div>
                                          </div>
                                          <div class="col-sm-2">

                                            <div class="text-center" style="margin-top: 80%;">
                                              <h5 id="dec"></h5>
                                            </div>
                                          </div>

                                          <div class="col-sm-2">
                                            <a href="{{ url('/shiftsdetails',['year' => $z,'startdate' => '-12-01','enddate' => '-12-31']) }}" style="color: black;">
                                            <div class="text-center" style="margin-top: 15%;">
                                             <h5 style="color: red;font-weight: bolder;font-size: 35px;">></h5>
                                            </div>
                                          </a>
                                          </div>

                                          <br><br>
                                      </div>

                                    </div>

                                    <div class="col-sm-6 offset-sm-3">
                                      <div class="row bio" style="margin-left: 1%;margin-right: 1%;margin-top: 1%;margin-bottom: 5%;border: 1px solid #b2b2b2;box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);">
                                          <div class="col-sm-8">

                                            <div style="margin-top: 10%;margin-left: 10%;">
                                             <h5>This Year's Earnings</h5> 
                                            </div>
                                          </div>
                                          <div class="col-sm-2">

                                            <div class="text-center" style="margin-top: 80%;">
                                              <h5 id="earnings"></h5>
                                            </div>
                                          </div>

                                          <div class="col-sm-2">
                                            <!-- <a href="{{ url('/shiftsdetails',['year' => $z,'startdate' => '-07-01','enddate' => '-07-31']) }}" style="color: black;">
                                            <div class="text-center" style="margin-top: 15%;">
                                             <h5 style="color: red;font-weight: bolder;font-size: 35px;">></h5>
                                            </div>
                                          </a> -->
                                          </div>

                                          <br><br>
                                      </div>
                                    </div>
                                </div>
                                

                                
                               
                              </div>
                                @include('newview.new.sidemenu.home')
                </div>
                    
            </div>

         </div>
    </div>
    {!! Form::close() !!}
@endsection

@push('scripts')
   <script type="text/javascript">
           $('#start_date').datetimepicker({
               format: 'DD-MMM-YYYY',
               useCurrent: true,
               icons: {
                   up: "icon-arrow-up-circle icons font-2xl",
                   down: "icon-arrow-down-circle icons font-2xl"
               },
               sideBySide: true
           })
       </script>
       <script type="text/javascript">
           $('#end_date').datetimepicker({
               format: 'DD-MMM-YYYY',
               useCurrent: true,
               icons: {
                   up: "icon-arrow-up-circle icons font-2xl",
                   down: "icon-arrow-down-circle icons font-2xl"
               },
               sideBySide: true
           })
       </script>
       <script type="text/javascript">

        $('#nextyear').ready(function(){
        var customer_name = $("#ddlYears").val();   
        //alert (customer_name);
        if(customer_name){
            $.ajax({
               type:"GET",
               url:"{{url('/getyear')}}?c_year="+customer_name,
               success:function(res){
                if(res){
                    console.log(res);
                     //$("#newyear").val(res.c_year);
                document.getElementById("jan").innerHTML = res.jan;
                document.getElementById("feb").innerHTML = res.feb;
                document.getElementById("mar").innerHTML = res.mar;
                document.getElementById("apr").innerHTML = res.apr;
                document.getElementById("may").innerHTML = res.may;
                document.getElementById("jun").innerHTML = res.jun;
                document.getElementById("jul").innerHTML = res.jul;
                document.getElementById("aug").innerHTML = res.aug;
                document.getElementById("sep").innerHTML = res.sep;
                document.getElementById("oct").innerHTML = res.oct;
                document.getElementById("nov").innerHTML = res.nov;
                document.getElementById("dec").innerHTML = res.dec;
                document.getElementById("earnings").innerHTML = res.earnings;
    
                x = res.c_year;
                //alert (x);
    
                }
       
               }
            });
        }
        
        
       });
    
    </script>
@endpush