@extends('layouts.new.header')
@section('title', 'Home')
@section('content')
<style type="text/css">
    /*.card:hover {
        color: white;
  background-color: red;
}*/
.card{
  height: auto; 
  border-radius: 15px; 
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
}
.card-title{
    margin-top: 5%;

}
.bio{
    border-radius: 10px !important;
    border: 1px solid red;
    height: 75px;
}
.btn-danger {
    color: #fff;
    background-color: red;
    border-color: red;
}
/*a:hover {
    color: white !important;
    text-decoration: none;
}*/
hr {
    margin-top: 1rem;
    margin-bottom: 1rem;
    border: 0;
    border-top: 1px solid red;
}
.form-control {
   box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
}
label {
    font-size: 15px;
    margin-top: 1%;
}
.border{
    
    border-radius: 17px !important;
    
}
.alert-success {
    color: white;
    background-color: green;
    border-color: #cdedd8;
}
.parsley-required{
    color: red;
}

.btn2 {
    color: black;
    background-color: white;
    border-color: red;
}


</style>
</style>
<!-- <ol class="breadcrumb">
        <li class="breadcrumb-item">Doctors</li>
    </ol> -->
   {!! Form::open(['route' => 'homepagedetails', 'files' => true,'id' => 'myForm']) !!} 
    <div class="container-fluid">
        <div class="animated fadeIn">
             <div class="container">
              
                <div class="row">
                      <div class="col-sm-8">
                         <div class="row" style="margin-left: 1%;margin-right: 1%;">
                                   <div class="col-sm-4">

                                    <div class="text-center">
                                     
                                    {!! Form::select('patient_id', [''=>'Select Patient'] + $patient_name->toArray(), null, ['class' => 'form-control','data-parsley-required-message' =>'Patient is required','required']) !!}
                                    </div>
                                    </div>

                                    <div class="col-sm-3">
                                     <div class="text-center">
                                      {!! Form::text('s_date', null, ['class' => 'form-control','id'=>'start_date','data-parsley-required-message' =>'Date is required','required','placeholder' => 'Start Date']) !!}
                                    </div>
                                    </div>

                                    <div class="col-sm-3">
                                     <div class="text-center">
                                      {!! Form::text('e_date', null, ['class' => 'form-control','id'=>'end_date','data-parsley-required-message' =>'Date is required','required','placeholder' => 'End Date']) !!}
                                    </div>
                                    </div>

                                    <div class="col-sm-2">
                                     <div class="text-center">
                                    <button class="btn btn-danger" type="submit" style="width: 100%;">Search</button>
                                    </div>
                                    </div>

                                </div>
                                <h3 class="text-center" style="margin-top: 5%;">My Haspatal Schedule</h3>
                                <div class="row " style="margin-left: 1%;margin-right: 1%;margin-top: 5%;border: 1px solid #b2b2b2;box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);">
                                    <div class="col-sm-12">
                                        <div class="row" style="margin-left: 40%;">
                                          <h2 class="text-center">This Year</h2>                                    
                                        </div>
                                    </div>
                                    <div class="col-sm-6" style="margin-bottom: 5%;">
                                        <h6 style="display: inline;">From</h6>  <h6 style="display: inline;" id="start_year"> <h6 style="display: inline;" > To </h6><h6 id="end_year" style="display: inline;"></h6>
                                        <select name="c_year" id="ddlYears"  class="form-control year"></select>
                                        
                                    </select>
                                      <div class="border" style="margin-left: 1%;margin-right: 1%;margin-top: 10%;border: 1px solid #b2b2b2;box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);height: 325px;">
                                            <div class="row" style="margin-left: 1%;margin-right: 1%;margin-top: 13%;">
                                                   <div class="col-sm-6">
                                                  <h1  style="color: red;" id="total"> </h1>
                                                  <br>
                                                  <h3>Total</h3> 
                                                   </div>
                                                   <div class="col-sm-6">
                                                     {{-- <a href="{{ url('/thisweek_days_list') }}" style="color: black;"> --}}
                                                     <button class="btn btn-danger" type="button" id="someButton" style="width: 100%;">View</button> </a>
                                                   </div>
                                            </div>

                                                <div class="row" style="margin-left: 1%;margin-right: 1%;margin-top: 25%;">

                                                    <div class="col-sm-6">
                                                     <div class="text-center">
                                                     <h4 class="text-center" style="color: red;" id="lastmonth"></h4>
                                                        <br>
                                                        <h6 class="text-center">Last Month</h6>
                                                    </div>
                                                    </div>

                                                    <div class="col-sm-6">
                                                     <div class="text-center">
                                                      <h4 class="text-center" style="color: red;" id="last_year"></h4>
                                                        <br>
                                                        <h6 class="text-center">Last Year</h6>
                                                    </div>
                                                    </div>

                                                </div>
                                      </div>

                                    </div>
                                    <div class="col-sm-6" style="margin-top: 5%;margin-bottom: 5%;">                               
                                        <div class="row" style="margin-top: 5%;">
                                              
                                                   
                                                
                                           </div> 
                                            
                                    </div>

                      </div>
                                

                                
                               
                              </div>
                                @include('newview.new.sidemenu.home')
                </div>
                    
            </div>

         </div>
    </div>
    {!! Form::close() !!}
@endsection

@push('scripts')
   <script type="text/javascript">
           $('#start_date').datetimepicker({
               format: 'DD-MMM-YYYY',
               useCurrent: true,
               icons: {
                   up: "icon-arrow-up-circle icons font-2xl",
                   down: "icon-arrow-down-circle icons font-2xl"
               },
               sideBySide: true
           })
       </script>
       <script type="text/javascript">
           $('#end_date').datetimepicker({
               format: 'DD-MMM-YYYY',
               useCurrent: true,
               icons: {
                   up: "icon-arrow-up-circle icons font-2xl",
                   down: "icon-arrow-down-circle icons font-2xl"
               },
               sideBySide: true
           })
       </script>

<script type="text/javascript">
    $(function () {
        //Reference the DropDownList.
        var ddlYears = $("#ddlYears");
 
        //Determine the Current Year.
        var currentYear = (new Date()).getFullYear();
 
        //Loop and add the Year values to DropDownList.
        for (var i = 2020; i <= currentYear; i++) {
            var option = $("<option />");
            option.html(i);
            option.val(i);
            ddlYears.append(option);
        }
    });
   
//     $(document).ready(function(){ 
//   $('#ddlYears').click(function(){ 
//     alert($('#ddlYears :selected').text());
//   });
// });

</script>

<script type="text/javascript">

    $('#nextyear').ready(function(){
    var customer_name = $("#ddlYears").val();   
    //alert (customer_name);
    if(customer_name){
        $.ajax({
           type:"GET",
           url:"{{url('/getyear')}}?c_year="+customer_name,
           success:function(res){
            if(res){
                console.log(res);
                 $("#newyear").val(res.c_year);
            document.getElementById("total").innerHTML = res.all_count;
            document.getElementById("lastmonth").innerHTML = res.last_month;
            document.getElementById("last_year").innerHTML = res.last_year;
            document.getElementById("start_year").innerHTML = res.start_year;
            document.getElementById("end_year").innerHTML = res.end_year;

            x = res.c_year;
            //alert (x);

            }
   
           }
        });
    }
    
    
   });

</script>

<script type="text/javascript">
 $('#nextyear').ready(function(){ 
var test = $("#ddlYears").val(); 
    $('#someButton').click(function() {
//alert (test);
        window.location = "{{url('/thisyear_month_list')}}"+'/'+test;
    });
});
</script>
@endpush