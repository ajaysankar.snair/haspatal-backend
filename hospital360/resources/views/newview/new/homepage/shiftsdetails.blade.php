@extends('layouts.new.header')
@section('title', 'Home')
@section('content')
<style type="text/css">
    /*.card:hover {
        color: white;
  background-color: red;
}*/
.card{
  height: auto; 
  border-radius: 15px; 
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
}
.card-title{
    margin-top: 5%;

}
.bio{
    border-radius: 10px !important;
    border: 1px solid red;
    height: 75px;
}
.btn-danger {
    color: #fff;
    background-color: red;
    border-color: red;
}
/*a:hover {
    color: white !important;
    text-decoration: none;
}*/
hr {
    margin-top: 1rem;
    margin-bottom: 1rem;
    border: 0;
    border-top: 1px solid red;
}
.form-control {
   box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
}
label {
    font-size: 15px;
    margin-top: 1%;
}
.pic{
    height: 84px;
    border-radius: 17px !important;
    border: 1px solid red;
}
.alert-success {
    color: white;
    background-color: green;
    border-color: #cdedd8;
}
.parsley-required{
    color: red;
}

.btn2 {
    color: red;
    background-color: white;
    border: 1px solid #b2b2b2;
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
    font-weight: bolder;
}


</style>

<?php 


$a = $_SERVER['REQUEST_URI']; 
$b = explode('/', $a);
//echo '<pre>'; print_r($b); exit();
$year = $b[2];
$start_date = $b[3];
$end_date = $b[4];
  $daydata = DB::table('booking_request')->join('patient_details','booking_request.patient_name','patient_details.id')
              ->select('booking_request.*','patient_details.first_name','patient_details.last_name','patient_details.id as patient_id')
              ->where('doctor_name', Auth::user()->userDetailsId)->whereBetween('date_time', [date($year.$start_date), date($year.$end_date)])
              ->orderBy('booking_request.date_time','ASC')
              ->get();
//$date_time = explode(' ',$daydata->date_time);
?>


   {!! Form::open(['route' => 'homepagedetails', 'files' => true,'id' => 'myForm']) !!} 
    <div class="container-fluid">
        <div class="animated fadeIn">
             <div class="container">
              
                <div class="row">
                      <div class="col-sm-8" style="margin-bottom: 1%;">
                         <div class="row" style="margin-left: 1%;margin-right: 1%;">
                                   <div class="col-sm-4">

                                    <div class="text-center">
                                     
                                    {!! Form::select('patient_id', [''=>'Select Patient'] + $patient_name->toArray(), null, ['class' => 'form-control','data-parsley-required-message' =>'Patient is required','required']) !!}
                                    </div>
                                    </div>

                                    <div class="col-sm-3">
                                     <div class="text-center">
                                      {!! Form::text('s_date', null, ['class' => 'form-control','id'=>'start_date','data-parsley-required-message' =>'Date is required','required','placeholder' => 'Start Date']) !!}
                                    </div>
                                    </div>

                                    <div class="col-sm-3">
                                     <div class="text-center">
                                      {!! Form::text('e_date', null, ['class' => 'form-control','id'=>'end_date','data-parsley-required-message' =>'Date is required','required','placeholder' => 'End Date']) !!}
                                    </div>
                                    </div>

                                    <div class="col-sm-2">
                                     <div class="text-center">
                                    <button class="btn btn-danger" type="submit" style="width: 100%;">Search</button>
                                    </div>
                                    </div>

                                </div>
                                <h3 class="text-center" style="margin-top: 5%;">My Haspatal Schedule</h3>
                                {{-- @if(count($shiftsdetailsmorning) > 0)
                                <div class="alert alert-warning">
                                    <strong>Sorry!</strong> No Data Found.
                                </div>                                      
                            @else --}}
                                <div style="overflow: scroll;height: 515px;"> 
                                @foreach($daydata as $data)
                                <div class="row bio" style="margin-left: 1%;margin-right: 1%;margin-top: 3%;margin-bottom: 2%;border: 1px solid #b2b2b2;box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);">
                                    <?php 
                                        $date_time = explode(' ',$data->date_time);
                                    ?>
                                    <div class="col-sm-2">

                                      <div class="text-center" style="margin-top: 30%;">
                                       <h6 style="color: red;">{{date('d F Y', strtotime($date_time[0]))}}</h6> 
                                      </div>
                                    </div>
                                    <div class="col-sm-2">

                                      <div class="text-center" style="margin-top: 30%;">
                                        <h6>{{$date_time[1]}}</h6>
                                      </div>
                                    </div>

                                    <div class="col-sm-3">

                                      <div class="text-center" style="margin-top: 13%;">
                                          <h6> Video Consultation Appointment</h6>
                                        </div>
                                    </div>

                                    <div class="col-sm-3">

                                      <div class="text-center" style="margin-top: 15%;">
                                      <h6 style="color: red;">{{$data->first_name}}</h6>
                                        </div>
                                    </div>

                                    <div class="col-sm-2">

                                      <div class="text-center" style="margin-top: 20%;">
                                        <button class="btn btn2" type="button" style="width: 100%;">View</button>
                                        </div>
                                    </div>

                                  
                                    <br><br>
                                </div>
                                @endforeach
                               {{-- @endif --}}
                            </div>
                              </div>
                                @include('newview.new.sidemenu.home')
                </div>
                    
            </div>

         </div>
    </div>
    {!! Form::close() !!}
@endsection

@push('scripts')
   <script type="text/javascript">
           $('#start_date').datetimepicker({
               format: 'DD-MMM-YYYY',
               useCurrent: true,
               icons: {
                   up: "icon-arrow-up-circle icons font-2xl",
                   down: "icon-arrow-down-circle icons font-2xl"
               },
               sideBySide: true
           })
       </script>
       <script type="text/javascript">
           $('#end_date').datetimepicker({
               format: 'DD-MMM-YYYY',
               useCurrent: true,
               icons: {
                   up: "icon-arrow-up-circle icons font-2xl",
                   down: "icon-arrow-down-circle icons font-2xl"
               },
               sideBySide: true
           })
       </script>
@endpush