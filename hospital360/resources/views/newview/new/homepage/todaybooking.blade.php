@extends('layouts.new.header')
@section('title', 'Home')
@section('content')
<style type="text/css">
    /*.card:hover {
        color: white;
  background-color: red;
}*/
.card{
  height: auto; 
  border-radius: 15px; 
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
}
.card-title{
    margin-top: 5%;

}
.bio{
    border-radius: 10px !important;
    border: 1px solid red;
    height: 75px;
}
.btn-danger {
    color: #fff;
    background-color: red;
    border-color: red;
}
/*a:hover {
    color: white !important;
    text-decoration: none;
}*/
hr {
    margin-top: 1rem;
    margin-bottom: 1rem;
    border: 0;
    border-top: 1px solid red;
}
.form-control {
   box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
}
label {
    font-size: 15px;
    margin-top: 1%;
}
.border{
    
    border-radius: 17px !important;
    
}
.alert-success {
    color: white;
    background-color: green;
    border-color: #cdedd8;
}
.parsley-required{
    color: red;
}

.btn2 {
    color: black;
    background-color: white;
    border-color: red;
}


</style>
</style>
<!-- <ol class="breadcrumb">
        <li class="breadcrumb-item">Doctors</li>
    </ol> -->
   {!! Form::open(['route' => 'homepagedetails', 'files' => true,'id' => 'myForm']) !!} 
    <div class="container-fluid">
        <div class="animated fadeIn">
             <div class="container">
              
                <div class="row">
                      <div class="col-sm-8">
                         <div class="row" style="margin-left: 1%;margin-right: 1%;">
                                   <div class="col-sm-4">

                                    <div class="text-center">
                                     
                                    {!! Form::select('patient_id', [''=>'Select Patient'] + $patient_name->toArray(), null, ['class' => 'form-control','data-parsley-required-message' =>'Patient is required','required']) !!}
                                    </div>
                                    </div>

                                    <div class="col-sm-3">
                                     <div class="text-center">
                                      {!! Form::text('s_date', null, ['class' => 'form-control','id'=>'start_date','data-parsley-required-message' =>'Date is required','required','placeholder' => 'Start Date']) !!}
                                    </div>
                                    </div>

                                    <div class="col-sm-3">
                                     <div class="text-center">
                                      {!! Form::text('e_date', null, ['class' => 'form-control','id'=>'end_date','data-parsley-required-message' =>'Date is required','required','placeholder' => 'End Date']) !!}
                                    </div>
                                    </div>

                                    <div class="col-sm-2">
                                     <div class="text-center">
                                    <button class="btn btn-danger" type="submit" style="width: 100%;">Search</button>
                                    </div>
                                    </div>

                                </div>
                                <h3 class="text-center" style="margin-top: 5%;">My Haspatal Schedule</h3>
                                <div class="row " style="margin-left: 1%;margin-right: 1%;margin-top: 5%;border: 1px solid #b2b2b2;box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);">
                                    <div class="col-sm-12">
                                        <div class="row" style="margin-left: 44%;margin-right: 44%;">
                                          <h2 class="text-center">Today</h2>
                                                                               
                                        </div>
                                        <div class="row" style="margin-left: 43%;">
                                          <h5 class="text-center">{{$mytime->toFormattedDateString('d-m-Y')}}</h5>
                                                                               
                                        </div>
                                    </div>
                                    <div class="col-sm-6" style="margin-bottom: 5%;">
                                      <div class="border" style="margin-left: 1%;margin-right: 1%;margin-top: 10%;border: 1px solid #b2b2b2;box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);height: 325px;">
                                            <div class="row" style="margin-left: 1%;margin-right: 1%;margin-top: 13%;">
                                                   <div class="col-sm-6">
                                                  <h1  style="color: red;">{{ $allcount }}</h1>
                                                  <br>
                                                  <h3>Total</h3> 
                                                   </div>
                                                   <div class="col-sm-6">
                                                     <a href="{{ url('/shifts') }}" style="color: black;">
                                                     <button class="btn btn-danger" type="button" style="width: 100%;">View</button> </a>
                                                   </div>
                                            </div>

                                                <div class="row" style="margin-left: 1%;margin-right: 1%;margin-top: 25%;">
                                                   <div class="col-sm-4">

                                                    <div class="text-center">
                                                     
                                                    <h4 class="text-center" style="color: red;">{{$this_weeks}}</h4>
                                                        <br>
                                                        <h6 class="text-center">This Week</h6>
                                                    </div>
                                                    </div>

                                                    <div class="col-sm-4">
                                                     <div class="text-center">
                                                     <h4 class="text-center" style="color: red;">{{$last_weeks}}</h4>
                                                        <br>
                                                        <h6 class="text-center">Last Week</h6>
                                                    </div>
                                                    </div>

                                                    <div class="col-sm-4">
                                                     <div class="text-center">
                                                      <h4 class="text-center" style="color: red;">{{$yesterday}}</h4>
                                                        <br>
                                                        <h6 class="text-center">Yesterday</h6>
                                                    </div>
                                                    </div>

                                                </div>
                                      </div>
                                     
                                    </div>
                                    <div class="col-sm-6" style="margin-top: 5%;margin-bottom: 5%;">                               
                                        <div class="row" style="margin-top: 5%;">
                                                                                           
                                           </div> 
                                            
                                    </div>

                      </div>
                                

                                
                               
                              </div>
                                @include('newview.new.sidemenu.home')
                </div>
                    
            </div>

         </div>
    </div>
    {!! Form::close() !!}
@endsection

@push('scripts')
   <script type="text/javascript">
           $('#start_date').datetimepicker({
               format: 'DD-MMM-YYYY',
               useCurrent: true,
               icons: {
                   up: "icon-arrow-up-circle icons font-2xl",
                   down: "icon-arrow-down-circle icons font-2xl"
               },
               sideBySide: true
           })
       </script>
       <script type="text/javascript">
           $('#end_date').datetimepicker({
               format: 'DD-MMM-YYYY',
               useCurrent: true,
               icons: {
                   up: "icon-arrow-up-circle icons font-2xl",
                   down: "icon-arrow-down-circle icons font-2xl"
               },
               sideBySide: true
           })
       </script>
@endpush