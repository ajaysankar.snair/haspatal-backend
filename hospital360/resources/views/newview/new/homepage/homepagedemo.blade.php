@extends('layouts.new.header')
@section('title', 'Home')
@section('content')
<style type="text/css">
    /*.card:hover {
        color: white;
  background-color: red;
}*/
.card{
  height: auto; 
  border-radius: 15px; 
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
}
.card-title{
    margin-top: 5%;

}
.bio{
    border-radius: 10px !important;
    border: 1px solid red;
    height: 75px;
}
.btn-danger {
    color: #fff;
    background-color: red;
    border-color: red;
}
/*a:hover {
    color: white !important;
    text-decoration: none;
}*/
hr {
    margin-top: 1rem;
    margin-bottom: 1rem;
    border: 0;
    border-top: 1px solid red;
}
.form-control {
   box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
}
label {
    font-size: 15px;
    margin-top: 1%;
}
.border{
    
    border-radius: 17px !important;
    
}
.alert-success {
    color: white;
    background-color: green;
    border-color: #cdedd8;
}
.parsley-required{
    color: red;
}

.btn2 {
    color: black;
    background-color: white;
    border-color: red;
}


</style>
</style>
<!-- <ol class="breadcrumb">
        <li class="breadcrumb-item">Doctors</li>
    </ol> -->
   {!! Form::open(['route' => 'homepagedetails', 'files' => true,'id' => 'myForm']) !!} 
    <div class="container-fluid">
        <div class="animated fadeIn">
             <div class="container">
              
                <div class="row">
                      <div class="col-sm-8">
                         <div class="row" style="margin-left: 1%;margin-right: 1%;">
                                   <div class="col-sm-4">

                                    <div class="text-center">
                                     
                                    {!! Form::select('patient_id', [''=>'Select Patient'] + $patient_name->toArray(), null, ['class' => 'form-control','data-parsley-required-message' =>'Patient is required','required']) !!}
                                    </div>
                                    </div>

                                    <div class="col-sm-3">
                                     <div class="text-center">
                                      {!! Form::text('s_date', null, ['class' => 'form-control','id'=>'start_date','data-parsley-required-message' =>'Date is required','required','placeholder' => 'Start Date']) !!}
                                    </div>
                                    </div>

                                    <div class="col-sm-3">
                                     <div class="text-center">
                                      {!! Form::text('e_date', null, ['class' => 'form-control','id'=>'end_date','data-parsley-required-message' =>'Date is required','required','placeholder' => 'End Date']) !!}
                                    </div>
                                    </div>

                                    <div class="col-sm-2">
                                     <div class="text-center">
                                    <button class="btn btn-danger" type="submit" style="width: 100%;">Search</button>
                                    </div>
                                    </div>

                                </div>
                                <h3 class="text-center" style="margin-top: 5%;">My Haspatal Schedule</h3>
                                <div class="row " style="margin-left: 1%;margin-right: 1%;margin-top: 5%;border: 1px solid #b2b2b2;box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);">
                                    <div class="col-sm-6" style="margin-bottom: 5%;">
                                      <div class="border" style="margin-left: 1%;margin-right: 1%;margin-top: 10%;border: 1px solid #b2b2b2;box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);height: 325px;">
                                            <div style="margin-top: 10%;">
                                                  <h1 class="text-center" style="color: red;">{{ $booking }}</h1>
                                                  <br>
                                                  <h3 class="text-center">Total</h3> 
                                            </div>

                                                <div class="row" style="margin-left: 1%;margin-right: 1%;margin-top: 25%;">
                                                   <div class="col-sm-4">

                                                    <div class="text-center">
                                                     
                                                    <h4 class="text-center" style="color: red;">{{$this_year}}</h4>
                                                        <br>
                                                        <h6 class="text-center">This Year</h6>
                                                    </div>
                                                    </div>

                                                    <div class="col-sm-4">
                                                     <div class="text-center">
                                                     <h4 class="text-center" style="color: red;">{{$lastyear}}</h4>
                                                        <br>
                                                        <h6 class="text-center">Last Year</h6>
                                                    </div>
                                                    </div>

                                                    <div class="col-sm-4">
                                                     <div class="text-center">
                                                      <h4 class="text-center" style="color: red;">{{$lastmonth}}</h4>
                                                        <br>
                                                        <h6 class="text-center">Last Month</h6>
                                                    </div>
                                                    </div>

                                                </div>
                                      </div>

                                    </div>
                                    <div class="col-sm-6" style="margin-top: 5%;margin-bottom: 5%;">                               
                                        <div class="row" style="margin-top: 5%;">
                                              <div class="col-sm-6">  
                                                    <div class="card newcard text-center border-danger" style="height: 120px;">
                                                         <a href="{{ url('/shifts') }}" style="color: black;"> 
                                                        <div class="card-block cardpadding" style="margin-top: 20%;">
                                                             <h5 style="color: red;">{{$this_today}}</h5>
                                                        <h4 class="card-title text-center">Today</h4>
                                                            
                                                        </div>
                                                        </a>
                                                    </div>

                                                </div>
                                                   
                                                <div class="col-sm-6">
                                                    <div class="card newcard text-center border-danger" style="height: 120px;">
                                                         <a href="{{ url('/thisweek_days_list') }}" style="color: black;">  
                                                        <div class="card-block cardpadding" style="margin-top: 20%;">
                                                          <h5 style="color: red;">{{$this_weeks}}</h5>
                                                           <h4 class="card-title text-center">This Week</h4>
                                                            
                                                        </div>
                                                        </a>  
                                                    </div>

                                                </div>
                                           </div> 
                                           <div class="row">
                                              <div class="col-sm-6">  
                                                    <div class="card newcard text-center border-danger" style="height: 120px;">
                                                         <a href="{{ url('/thismonth_week_list') }}" style="color: black;"> 
                                                        <div class="card-block cardpadding" style="margin-top: 20%;">
                                                            <h5 style="color: red;">{{$this_month}}</h5>
                                                        <h4 class="card-title text-center">This Month</h4>
                                                            
                                                        </div>
                                                        </a>
                                                    </div>

                                                </div>
                                                   <?php $z = date('Y'); ?>
                                                <div class="col-sm-6">
                                                    <div class="card newcard text-center border-danger" style="height: 120px;">
                                                     <a href="{{ url('/thisyear_month_list',['test' => $z]) }}" style="color: black;">
                                                        <div class="card-block cardpadding" style="margin-top: 20%;">
                                                          <h5 style="color: red;">{{$this_year}}</h5>
                                                           <h4 class="card-title text-center">This Year</h4>
                                                            
                                                        </div>
                                                        </a>  
                                                    </div>

                                                </div>
                                           </div> 
                                    </div>

                      </div>
                                

                                
                               
                              </div>
                                @include('newview.new.sidemenu.home')
                </div>
                    
            </div>

         </div>
    </div>
    {!! Form::close() !!}
@endsection

@push('scripts')
   <script type="text/javascript">
           $('#start_date').datetimepicker({
               format: 'DD-MMM-YYYY',
               useCurrent: true,
               icons: {
                   up: "icon-arrow-up-circle icons font-2xl",
                   down: "icon-arrow-down-circle icons font-2xl"
               },
               sideBySide: true
           })
       </script>
       <script type="text/javascript">
           $('#end_date').datetimepicker({
               format: 'DD-MMM-YYYY',
               useCurrent: true,
               icons: {
                   up: "icon-arrow-up-circle icons font-2xl",
                   down: "icon-arrow-down-circle icons font-2xl"
               },
               sideBySide: true
           })
       </script>
@endpush