@extends('layouts.new.header')
@section('title', 'Dr Add Fund')
@section('content')
<style type="text/css">
    /*.card:hover {
        color: white;
  background-color: red;
}*/
.card{
  height: auto; 
  border-radius: 15px; 
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
}
.card-title{
    margin-top: 5%;
}
.bio{
    border-radius: 17px !important;
    border: 1px solid red;
    margin-left: 5%;
    margin-right: 5%;
    height: 230px;
}
.btn-danger {
    color: #fff;
    background-color: red;
    border-color: red;
}
/*a:hover {
    color: white !important;
    text-decoration: none;
}*/
hr {
    margin-top: 1rem;
    margin-bottom: 1rem;
    border: 0;
    border-top: 1px solid red;
}
.form-control {
   box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
}
label {
    font-size: 15px;
    margin-top: 1%;
}
.pic{
    height: 84px;
    border-radius: 17px !important;
    border: 1px solid red;
}
.alert-success {
    color: white;
    background-color: green;
    border-color: #cdedd8;
}
.parsley-required{
    color: red;
}

.btn2 {
    color: black;
    background-color: white;
    border-color: red;
}

</style>
<!-- <ol class="breadcrumb">
        <li class="breadcrumb-item">Doctors</li>
    </ol> -->
    {!! Form::open(['route' => 'doctor_add_fund.store', 'files' => true,'id' => 'myForm']) !!}
    <div class="container-fluid">
        <div class="animated fadeIn">
             <div class="container">
               @include('flash::message')
                <div class="row">
                      <div class="col-sm-8">
<!--                          <div class="row" style="margin-left: 1%;margin-right: 1%;margin-top: 2%;">
                                   <div class="col-sm-6">

                                    <div class="text-center">
                                    {!! Form::text('search', null, ['class' => 'form-control']) !!}
                                    </div>
                                    </div>

                                    <div class="col-sm-2">
                                     <div class="text-center">
                                     <button class="btn btn2" type="button" style="width: 100%;">From</button>
                                    </div>
                                    </div>

                                    <div class="col-sm-2">
                                     <div class="text-center">
                                    <button class="btn btn2" type="button" style="width: 100%;">To</button>
                                    </div>
                                    </div>

                                    <div class="col-sm-2">
                                     <div class="text-center">
                                    <button class="btn btn2" type="button" style="width: 100%;">Search</button>
                                    </div>
                                    </div>

                                </div> -->
                                
                                <h1 class="text-center" style="margin-top: 5%;">Add Fund</h1>
                                <div class="row " style="margin-left: 1%;margin-right: 1%;margin-top: 8%;border: 1px solid #b2b2b2;box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);">
                                  <div class="col-sm-12" style="padding: 5%;">
                                        
                                       

                                        <label for="dr_amount">Current  Amount</label>
                                        <input type="text" class="form-control" name="" value="{{ (!empty($last_amount)) ? $last_amount->dr_amount : "0" }}" readonly>

                                        <label for="dr_amount">Earning  Amount</label>
                                        <input type="text" class="form-control" name="" value="{{ (!empty($amount)) ? $amount : "0" }}" readonly>

                                        <label for="dr_amount">Total Amount</label>
                                        <input type="text" class="form-control" name="" value="{{ (!empty($total_amount)) ? $total_amount : "0" }}" readonly>

                                        <label for="dr_amount">Amount</label>
                                        {!! Form::text('dr_amount', null, ['class' => 'form-control','data-parsley-required-message' =>'Amount is required','id' => 'razorpay_amount','required']) !!}  
                                        
                                    </div>
                                </div>
                                <div class="text-center" style="margin-top: 5%;margin-left: 80%;">
                                  <button class="btn btn-danger razor" type="button" style="width: 90%;">Submit</button>
                                </div>
                              </div>
                                  @include('newview.new.sidemenu.wallet')
                </div>
                    
            </div>

         </div>
    </div>
    {!! Form::close() !!}
@endsection

@push('scripts')
 <script src="https://checkout.razorpay.com/v1/checkout.js"></script>
    <script>
         var SITEURL = '{{URL::to('')}}';
         $.ajaxSetup({
           headers: {
               'X-CSRF-TOKEN': $('input[name="_token"]').attr('value')
           }
         }); 
         $(document).on('click', '.razor', function(e){          
           var totalAmount = $("#razorpay_amount").val();
           var options = {
           "key": "rzp_test_XiaDngfJzRGkOZ",
           "amount": (totalAmount*100), // 2000 paise = INR 20
           "name": "Haspatal",
           "description": "Payment",
           "image":"",
           "handler": function (response){
                 $.ajax({
                   url: SITEURL + '/doctor_add_fund',
                   type: 'post',
                   dataType: 'json',
                   data: {
                     payment_id: response.razorpay_payment_id, 
                     dr_amount : totalAmount
                     }, 
                   success: function (msg) {
                        //alert("sucess");
                       window.location.href =  SITEURL + '/dr_add_fund';
                   }
               });
             
           },
           @if(!empty(auth()->user()))
          "prefill": {
               "name":   '{{ auth()->user()->first_name }} {{ auth()->user()->last_name }}',
               "email":   '{{ auth()->user()->email }}',
           },
           @else
           "prefill": {
               "name":    billing_first_name,
               "email":   customer_email,
               "phone":   customer_phone,
           },
           @endif
           "theme": {
               "color": "#528FF0"
           }
         };
         var rzp1 = new Razorpay(options);
         rzp1.open();
         e.preventDefault();
         });
         /*document.getElementsClass('razor').onclick = function(e){
           rzp1.open();
           e.preventDefault();
         }*/
    </script>
    @endpush