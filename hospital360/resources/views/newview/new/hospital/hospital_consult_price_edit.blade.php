@extends('layouts.new.header')
@section('title', 'Consult Prices')
@section('content')
<style type="text/css">
    /*.card:hover {
        color: white;
  background-color: red;
}*/
.card{
  height: auto; 
  border-radius: 15px; 
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
}
.card-title{
    margin-top: 5%;
}
.bio{
    border-radius: 17px !important;
    border: 1px solid red;
    margin-left: 5%;
    margin-right: 5%;
    height: 230px;
}
.btn-danger {
    color: #fff;
    background-color: red;
    border-color: red;
}
/*a:hover {
    color: white !important;
    text-decoration: none;
}*/
hr {
    margin-top: 1rem;
    margin-bottom: 1rem;
    border: 0;
    border-top: 1px solid red;
}
.form-control {
   box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
}
label {
    font-size: 15px;
    margin-top: 1%;
}
.pic{
    height: 84px;
    border-radius: 17px !important;
    border: 1px solid red;
}
.alert-success {
    color: white;
    background-color: green;
    border-color: #cdedd8;
}
.parsley-required{
    color: red;
}

.btn2 {
    color: black;
    background-color: white;
    border-color: red;
}

</style>
<!-- <ol class="breadcrumb">
        <li class="breadcrumb-item">Doctors</li>
    </ol> -->
   
    <div class="container-fluid">
        <div class="animated fadeIn">
             <div class="container">
               @include('flash::message')
                <div class="row">
                  <div class="col-sm-8">
                    @if(empty($id))
                    <h1 class="text-center" style="margin-top: 5%;">Create Consult Prices</h1>
                    {!! Form::open(['route' => 'setConsultPrices.store','id' => "myForm"]) !!}
                    <div class="row " style="margin-left: 1%;margin-right: 1%;margin-top: 10%;border: 1px solid #b2b2b2;box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);">
                      <div class="col-sm-12" style="padding: 5%;">

                            <label for="main_mobile"></label>
                                         {!! Form::select('specialities', [''=>'Select Specialities'] + $specility->toArray(), null, ['class' => 'form-control','data-parsley-required-message' =>'Specialities is required','required','id'=>'specialities_id']) !!}

                                         <label for="main_mobile"></label>
                                      
                                        {!! Form::select('doctor', ['Select Doctor' => 'Select Doctor'], null, ['class' => 'form-control','id'=>'doctor','required','data-parsley-required-message'=>'Doctor is required']) !!}

                                       <label for="main_mobile"></label>
                                        {!! Form::text('consult_price', null, ['class' => 'form-control','data-parsley-required-message' =>'Consult Price is required','data-parsley-type' =>'digits','required','placeholder' => 'Consult Price']) !!}
                          

                                         <label for="main_mobile"></label>
                                        {!! Form::text('date', null, ['class' => 'form-control','data-parsley-required-message' =>'Effective Date is required','required','placeholder' => 'Effective Date','id'=>'date']) !!}

                            <div class="text-center" style="margin-top: 5%;margin-left: 80%;">
                              <button class="btn btn-danger" type="submit" style="width: 90%;">Submit</button>
                            </div>
                      </div>
                    </div>
                    {!! Form::close() !!}
                    @else
                    {!! Form::open(['route' => ['setConsultPrices.update', $id],'method' => 'patch','id' => "myForm" ]) !!}
                    <h1 class="text-center" style="margin-top: 5%;">Edit Consult Prices</h1>

                    <div class="row " style="margin-left: 1%;margin-right: 1%;margin-top: 10%;border: 1px solid #b2b2b2;box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);">
                      <div class="col-sm-12" style="padding: 5%;">
                                    <label for="main_mobile"></label>
                                         {!! Form::select('specialities', [''=>'Select Specialities'] + $specility->toArray(), $data->specialities, ['class' => 'form-control','data-parsley-required-message' =>'Specialities is required','required','id'=>'specialities_id']) !!}
                                    <label for="main_mobile"></label>
                                        
                                        {!! Form::select('doctor', ['Select Doctor' => 'Select Doctor']+ $doctors->toArray(), $data->doctor, ['class' => 'form-control','id'=>'doctor','required','data-parsley-required-message'=>'Doctor is required']) !!}

                                       <label for="main_mobile"></label>
                                        {!! Form::text('consult_price', $data->consult_price, ['class' => 'form-control','data-parsley-required-message' =>'Consult Price is required','data-parsley-type' =>'digits','required','placeholder' => 'Consult Price']) !!}
                          

                                         <label for="main_mobile"></label>
                                        {!! Form::text('date', $data->date, ['class' => 'form-control','data-parsley-required-message' =>'Effective Date is required','required','placeholder' => 'Effective Date','id'=>'date']) !!}

                            <div class="text-center" style="margin-top: 5%;margin-left: 80%;">
                              <button class="btn btn-danger" type="submit" style="width: 90%;">Submit</button>
                            </div>
                      </div>
                    </div>
                    {!! Form::close() !!}
                    @endif
                  </div>
                  @include('newview.new.sidemenu.hospital_menu.hospital')
                </div>
                    
            </div>

         </div>
    </div>
    {!! Form::close() !!}
@endsection

@push('scripts')
    <script type="text/javascript">
    
$('#specialities_id').on('load change',function(){
   var specilities = $(this).val();
  // alert(specilities);
    if(specilities){
        $.ajax({
           type:"GET",
           url:"{{url('get-hospitalservice-list')}}?specialities_id="+specilities,
           success:function(res){
            if(res){
                $("#doctor").empty();
                $("#doctor").append('<option value="">Select Doctor</option>');
                $.each(res,function(key,value){
                    $("#doctor").append('<option value="'+ key +'">'+ value +'</option>');
                });
                // $('select[name="doctor"]').empty();
                //         $.each(data, function(key, value) {
                //             $('select[name="doctor"]').append('<option value="'+ key +'">'+ value +'</option>');
                //         });
            }else{
               $("#doctor").empty();
            }
           }
        });
    }else{
        $("#doctor").empty();
    }

})
// .trigger('load');
</script>
<script type="text/javascript">
           $('#date').datetimepicker({
               format: 'YYYY-MM-DD HH:mm:ss',
               useCurrent: true,
               icons: {
                   up: "icon-arrow-up-circle icons font-2xl",
                   down: "icon-arrow-down-circle icons font-2xl"
               },
               sideBySide: true
           })
       </script>
@endpush