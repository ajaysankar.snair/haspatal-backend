@extends('layouts.new.header')
@section('title', 'Hospital Reset Password')
@section('content')
<style type="text/css">
    /*.card:hover {
        color: white;
  background-color: red;
}*/
.card{
  height: auto; 
  border-radius: 15px; 
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
}
.card-title{
    margin-top: 5%;
}
.bio{
    border-radius: 17px !important;
    border: 1px solid red;
    margin-left: 5%;
    margin-right: 5%;
    height: 230px;
}
.btn-danger {
    color: #fff;
    background-color: red;
    border-color: red;
}
/*a:hover {
    color: white !important;
    text-decoration: none;
}*/
hr {
    margin-top: 1rem;
    margin-bottom: 1rem;
    border: 0;
    border-top: 1px solid red;
}
.form-control {
   box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
}
label {
    font-size: 15px;
    margin-top: 1%;
}
.pic{
    height: 84px;
    border-radius: 17px !important;
    border: 1px solid red;
}
.alert-success {
    color: white;
    background-color: green;
    border-color: #cdedd8;
}
.parsley-required{
    color: red;
}

.btn2 {
    color: black;
    background-color: white;
    border-color: red;
}

</style>
<!-- <ol class="breadcrumb">
        <li class="breadcrumb-item">Doctors</li>
    </ol> -->
    {!! Form::model($haspatal, ['route' => ['change_password.change_password'], 'method' => 'post', 'files' => true,'id' => 'myForm']) !!}
    <div class="container-fluid">
        <div class="animated fadeIn">
             <div class="container">
              @include('flash::message')
                <div class="row">
                      <div class="col-sm-8">
                                <h1 class="text-center" style="margin-top: 5%;">Reset Password</h1>
                                <div class="row " style="margin-left: 1%;margin-right: 1%;margin-top: 8%;border: 1px solid #b2b2b2;box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);">
                                    <div class="col-sm-12" style="padding: 5%;">

                                      <label for="main_mobile"></label>
                                        <input class="form-control" data-parsley-required-message="Current Password is required" required="" name="old_password" type="password" id="main_mobile1" placeholder="Current Password">
                                        
                                        <label for="main_mobile"></label>
                                        <input class="form-control" data-parsley-required-message="New Password is required" required="" name="new_password" type="password" id="new_password" placeholder="New Password">


                                        <label for="main_mobile"></label>
                                        <input class="form-control" data-parsley-equalto="#new_password"  data-parsley-required-message="Confirm Password is required" required="" name="con_password" type="password" id="main_mobile3" placeholder="Confirm New Password">
                                    </div>
                                </div>
                                <div class="text-center" style="margin-top: 5%;margin-left: 80%;">
                                  <button class="btn btn-danger" type="submit" style="width: 90%;">Submit</button>
                                </div>
                              </div>
                          @include('newview.new.sidemenu.hospital_menu.profile')
                </div>
                    
            </div>

         </div>
    </div>
    {!! Form::close() !!}
@endsection