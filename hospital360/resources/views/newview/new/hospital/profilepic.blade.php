@extends('layouts.new.header')
@section('title', 'Dr Profile Pic')
<meta name="_token" content="{{ app('Illuminate\Encryption\Encrypter')->encrypt(csrf_token()) }}" />
@section('content')
<style type="text/css">
    /*.card:hover {
        color: white;
  background-color: red;
}*/
.card{
  height: auto; 
  border-radius: 15px; 
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
}
.card-title{
    margin-top: 5%;
}
.bio{
    border-radius: 17px !important;
    border: 1px solid red;
    margin-left: 5%;
    margin-right: 5%;
    height: 230px;
}
.btn-danger {
    color: #fff;
    background-color: red;
    border-color: red;
}
/*a:hover {
    color: white !important;
    text-decoration: none;
}*/
hr {
    margin-top: 1rem;
    margin-bottom: 1rem;
    border: 0;
    border-top: 1px solid red;
}
.form-control {
   box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
}
label {
    font-size: 15px;
    margin-top: 1%;
}
.pic{
    height: 84px;
    border-radius: 17px !important;
    border: 1px solid red;
}
.alert-success {
    color: white;
    background-color: green;
    border-color: #cdedd8;
}
.parsley-required{
    color: red;
}

.btn2 {
    color: black;
    background-color: white;
    border-color: red;
}
 input{
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
 }
</style>
<!-- <ol class="breadcrumb">
        <li class="breadcrumb-item">haspatal</li>
    </ol> -->
     {!! Form::model($haspatal, ['route' => ['hasptal.bio.update', $haspatal->id], 'method' => 'post', 'files' => true,'id' => 'myForm']) !!}
    <div class="container-fluid">
        <div class="animated fadeIn">
             <div class="container">
               @include('flash::message')
		
                <div class="row">
                      <div class="col-sm-8">
                                <h1 class="text-center" style="margin-top: 5%;">Image Pic</h1>
                                @if(Session::has('message'))
              								    <div class="alert alert-{{ Session::get('message-type') }} alert-dismissable">
              								        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
              								        <i class="glyphicon glyphicon-{{ Session::get('message-type') == 'success' ? 'ok' : 'remove'}}"></i> {{ Session::get('message') }}
              								    </div>
              								@endif
                                <div class="row " style="margin-left: 1%;margin-right: 1%;margin-top: 7%;border: 1px solid #b2b2b2;box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);">
                                 <div class="col-sm-6" style="padding: 5%;">
                                 	@if ($haspatal->hospital_profile == null)
                                 	<img  src="{{URL::asset('public/noimg.png')}}" alt="Course" style="width: 100%;margin-top: 2%;margin-left: 37%;margin-right: 30;max-width: 50%;">
                                 	 @else
                                 	<?php  $courceimage = URL::asset('public/media/hospital_profile/'.$haspatal->hospital_profile); ?>
                                       <label for="main_mobile">Current Profile Pic</label>

                                        <div class="">
                                            <img  src="<?= $courceimage ?>" alt="Course" style="width: 100%;margin-top: 2%;margin-left: 37%;margin-right: 30;max-width: 50%;">
                                        </div>
                                 	 @endif



                                    </div>
                                    <div class="col-sm-6" style="padding: 5%;">
                                    <label ></label> 

                                    <div class="text-center " style="padding: 32px;">
                                       <label >New Picture</label> 
                                      {!! Form::file('hospital_profile',[]) !!}
                                      </div>
                                    </div>

                                </div>

                                 <div class="row" style="margin-left: 10%;margin-right: 10%;margin-top: 2%;">
                                   <div class="col-sm-6" style="padding: 5%;">
                                    <input type="hidden" name="h_name" value="{{$haspatal->id}}">
                                   	<a id="done1" class="btn btn-danger text-center" role="button" style="width: 65%;margin-left: -40%;color: white;">Remove Profile Pic</a>
                                   </div>
                                   <div class="col-sm-6" style="padding: 5%;">
                                    <button class="btn btn-danger mobilebutton" type="submit" style="width: 50%;margin-left: 90%;">Submit</button>
                                   </div>
                                </div>
                              </div>



                            


                                @include('newview.new.sidemenu.hospital_menu.profile')
                </div>
                    
            </div>

         </div>
    </div>
    {!! Form::close() !!}
     @push('scripts')
    <script type="text/javascript">
  var SITEURL = '{{URL::to('')}}';
         $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
  jQuery('#done1').click(function () {
      var doctor_name =  $("input[name=h_name]").val();
      //alert(doctor_name);
       // console.log(key_point);
     crsf = $("input[name=csrf_test_name]").val();
      jQuery.ajax({
          type: "POST",
          url: SITEURL + '/h_profilepic_remove',        
          dataType: 'json',
          data: {"_token": "{{ csrf_token() }}","doctor_name":doctor_name},
          async: false,
          success: function(data) {
            window.location.href = SITEURL + '/h_profilepic' ;
            
        },
    });
 
       
     //alert(my_patient_id)

      //alert('Prescription saved successfully');
    });
</script>
@endpush
@endsection