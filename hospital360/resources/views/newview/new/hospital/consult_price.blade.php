@extends('layouts.new.header')
@section('title', 'Consult Prices')
@section('content')
<style type="text/css">
    /*.card:hover {
        color: white;
  background-color: red;
}*/
.card{
  height: auto; 
  border-radius: 15px; 
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
}
.card-title{
    margin-top: 5%;
}
.bio{
    border-radius: 17px !important;
    border: 1px solid red;
    margin-left: 5%;
    margin-right: 5%;
    height: 230px;
}
.btn-danger {
    color: #fff;
    background-color: red;
    border-color: red;
}
/*a:hover {
    color: white !important;
    text-decoration: none;
}*/
hr {
    margin-top: 1rem;
    margin-bottom: 1rem;
    border: 0;
    border-top: 1px solid red;
}
.form-control {
   box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
}
label {
    font-size: 15px;
    margin-top: 1%;
}
.pic{
    height: 84px;
    border-radius: 17px !important;
    border: 1px solid red;
}
.alert-success {
    color: white;
    background-color: green;
    border-color: #cdedd8;
}
.parsley-required{
    color: red;
}

.btn2 {
    color: black;
    background-color: white;
    border-color: red;
}

</style>
<!-- <ol class="breadcrumb">
        <li class="breadcrumb-item">Doctors</li>
    </ol> -->
   
    <div class="container-fluid">
        <div class="animated fadeIn">
             <div class="container">
               
                <div class="row">
                  <div class="col-sm-8">
                    
                    <h1 class="text-center" style="margin-top: 5%;">Set Consult Prices</h1>
                    @include('flash::message')
                    <div class="row " style="margin-left: 1%;margin-right: 1%;margin-top: 10%;border: 1px solid #b2b2b2;box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);">
                      <div class="col-sm-12" style="padding: 5%;">
                        <div style="text-align: right;margin-bottom: 2%;">
                          <a href="{{route('hospital_consult_price_create')}}" class="btn btn-danger" >Add Consult Prices</a>
                        </div>
                        <div style="height: 320px;overflow: scroll;padding: 2%;">
                        <table class="table table-striped table-bordered dataTable no-footer" width="100%" style="border: 1px solid #f86c6b;box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);">
                          <thead>
                            <tr>
                              <th class="text-center">Doctor Name</th>
                              <th class="text-center">Doctor Specialities</th>
                              <th class="text-center">Doctor Consult Price</th>
                              <th>Action</th>
                            </tr>
                          </thead>
                          <tbody>
                            @foreach($data as $value)
                            <tr>
                               <td class="text-center">{{$value->first_name}}</td>
                                <td class="text-center">{{$value->specility}}</td>
                               <td class="text-center">{{$value->consult_price}}</td>
                              <td>
                                {!! Form::open(['route' => ['setConsultPrices.destroy', $value->id], 'method' => 'delete']) !!}
                                <div class='btn-group'>
                                    
                                    <a href="{{ route('hospital_consult_price_edit',$value->id) }}" class='btn btn-ghost-info'>
                                       <i class="fa fa-edit"></i>
                                    </a>
                                    {!! Form::button('<i class="fa fa-trash"></i>', [
                                        'type' => 'submit',
                                        'class' => 'btn btn-ghost-danger',
                                        'onclick' => "return confirm('Are you sure?')"
                                    ]) !!}
                                </div>
                                {!! Form::close() !!}
                              </td>
                            </tr>
                            @endforeach
                          </tbody>
                        </table>
                          </div>  
                      </div>
                    </div>
                    
                  </div>
                  @include('newview.new.sidemenu.hospital_menu.hospital')
                </div>
                    
            </div>

         </div>
    </div>
    {!! Form::close() !!}
@endsection