@extends('layouts.new.header')
@section('title', 'Doctor Registor')
@section('content')
<style type="text/css">
    /*.card:hover {
        color: white;
  background-color: red;
}*/
.card{
  height: auto; 
  border-radius: 15px; 
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
}
.card-title{
    margin-top: 5%;
}
.bio{
    border-radius: 17px !important;
    border: 1px solid red;
    margin-left: 5%;
    margin-right: 5%;
    height: 230px;
}
.btn-danger {
    color: #fff;
    background-color: red;
    border-color: red;
}
/*a:hover {
    color: white !important;
    text-decoration: none;
}*/
hr {
    margin-top: 1rem;
    margin-bottom: 1rem;
    border: 0;
    border-top: 1px solid red;
}
.form-control {
   box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
}
label {
    font-size: 15px;
    margin-top: 1%;
}
.pic{
    height: 84px;
    border-radius: 17px !important;
    border: 1px solid red;
}
.alert-success {
    color: white;
    background-color: green;
    border-color: #cdedd8;
}
.parsley-required{
    color: red;
}

.btn2 {
    color: black;
    background-color: white;
    border-color: red;
}

</style>
<!-- <ol class="breadcrumb">
        <li class="breadcrumb-item">Doctors</li>
    </ol> -->
   
    <div class="container-fluid">
        <div class="animated fadeIn">
             <div class="container">
               
                <div class="row">
                  <div class="col-sm-8">
                    <h1 class="text-center" style="margin-top: 5%;">Register A Doctor</h1>
                    @include('flash::message')
                    
                    {!! Form::open(['route' => 'doctors.store', 'files' => true,'id' => 'myForm']) !!}
                    <div class="row " style="margin-left: 1%;margin-right: 1%;margin-top: 8%;border: 1px solid #b2b2b2;box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);">

                      
                        <div class="col-sm-6" style="padding: 5%;border-right: 1px solid gray;margin-top: 5%;">
                              <label for="speciality"></label>
                              <?php 
                              //$specilities= DB::table('specilities')->where('created_by',Auth::user()->id)->pluck('specility','id'); 
                              $specilities= DB::table('specilities')->pluck('specility','id'); 
                              ?>
                              {!! Form::select('speciality[]',[''=>'Select Specialities'] + $specilities->toArray(), null, ['class' => 'form-control','required', 'id' => 'speciality','data-parsley-required-message' =>'Speciality is required','multiple'=>'true']) !!}
                              
                              <label for="language"></label>
                              <?php 
                              //$specilities= DB::table('specilities')->where('created_by',Auth::user()->id)->pluck('specility','id'); 
                              $language= DB::table('language')->pluck('language','id'); 
                              ?>
                              {!! Form::select('language[]',[''=>'Select Language'] + $language->toArray(), null, ['class' => 'form-control','required', 'id' => 'language','data-parsley-required-message' =>'Language is required','multiple'=>'true']) !!}

                              <label for="first_name"></label>
                              {!! Form::text('first_name', null, ['class' => 'form-control','data-parsley-required-message' =>'Doctor Name is required','required','placeholder' => 'Doctor First Name']) !!}

                               <label for="last_name"></label>
                              {!! Form::text('last_name', null, ['class' => 'form-control','data-parsley-required-message' =>'Doctor Last Name is required','required','placeholder' => 'Doctor Last Name']) !!}

                              
                              <label for="experience"></label>
                              {!! Form::text('experience', null, ['class' => 'form-control', 'data-parsley-required-message' =>'Experience is required','required','placeholder' => 'Experience']) !!}

                              <label for="bio"></label>
                              {!! Form::textarea('bio', null, ['class' => 'form-control','rows' => '3','data-parsley-required-message' =>'Doctor Bio is required','required','placeholder' => 'Doctor Bio']) !!}

                              <label for="main_mobile"></label>
                              {!! Form::text('main_mobile', null, ['class' => 'form-control', 'data-parsley-required-message' =>'Mobile is required','required','placeholder' => 'Mobile']) !!}

                              <label for="email"></label>
                              {!! Form::text('email', null, ['class' => 'form-control', 'data-parsley-required-message' =>'Email is required','required','placeholder' => 'Email']) !!}
                              
                              <label for="password"></label>
                              {!! Form::password('password', ['class' => 'form-control','id' => 'password','data-parsley-required-message' =>'Password is required','required','placeholder' => 'Password']) !!}
                              
                                                            

                              <label for="confirm_password"></label>
                              {!! Form::password('confirm_password', ['class' => 'form-control','data-parsley-equalto="#password"','data-parsley-required-message' =>'Confirm Password is required','data-parsley-equalto-message'=>'Confirm password not match with password','required' ,'placeholder' => 'Confirm Password']) !!}
                          
                              
                        </div>
                        <div class="col-sm-6" style="padding: 5%;margin-top: 5%;">
                              
                             

                               

                               <label for="year_of_graduation"></label>
                              {!! Form::text('year_of_graduation', null, ['class' => 'form-control','data-parsley-required-message' =>'Year Of Graduation is required','required','placeholder' => 'Year Of Graduation']) !!}
                             

                              <label for="licence_type"></label>
                              <?php 
                            
                              $licence_type= DB::table('licence_type')->pluck('licence_name','id'); 
                              ?>
                              {!! Form::select('licence_type',[''=>'Select Licence Type'] + $licence_type->toArray(), null, ['class' => 'form-control','required', 'id' => 'licence_type','data-parsley-required-message' =>'licence_type is required']) !!}
                              <br>
                              <label for="profile_pic"> Doctor Picture</label>
                             {!! Form::file('profile_pic', null, ['class' => 'form-control','data-parsley-required-message' =>'Doctor Picture is required','required','placeholder' => 'Doctor Picture']) !!}

                              <br>
                              <label for="licence_copy">License Copy</label>
                              {!! Form::file('licence_copy', null, ['class' => 'form-control','data-parsley-required-message' =>'License Copy is required','required','placeholder' => 'License Copy']) !!}
                              <br>
                              <label for="licence_no"></label>
                              {!! Form::text('licence_no', null, ['class' => 'form-control','data-parsley-required-message' =>'License No is required','required','placeholder' => 'License No']) !!}
                              
                              <label for="valide_upto"></label>
                              {!! Form::text('valide_upto', null, ['class' => 'form-control','data-parsley-required-message' =>'Valid Upto is required','required','placeholder' => 'Valid Upto']) !!}
                             
                              <label for="issued_by"></label>
                              {!! Form::text('issued_by', null, ['class' => 'form-control','data-parsley-required-message' =>'Issued By is required','required','placeholder' => 'Issued By']) !!}

                              <label for="pan_number"></label>
                              {!! Form::text('pan_number', null, ['class' => 'form-control','data-parsley-required-message' =>'Pan Number is required','required','placeholder' => 'Pan Number']) !!}

                              <label for="account_name"></label>
                              {!! Form::text('account_name', null, ['class' => 'form-control','data-parsley-required-message' =>'Account Name is required','required','placeholder' => 'Account Name']) !!}
                              
                              <label for="bank_name"></label>
                              {!! Form::text('bank_name', null, ['class' => 'form-control','data-parsley-required-message' =>'Bank Name is required','required','placeholder' => 'Bank Name']) !!}
                             
                              <label for="ifsc_code"></label>
                              {!! Form::text('ifsc_code', null, ['class' => 'form-control','data-parsley-required-message' =>'IFSC code is required','required','placeholder' => 'IFSC code']) !!}
                              
                              <?php $id = Auth::user()->userDetailsId; ?>
                              <input type="hidden" name="hospital_id" value="{{$id}}">
                        </div>    
                       
                        <div class="col-sm-12">
                            <div class="text-center" style="margin-bottom: 5%;margin-left: 80%;">
                                <button class="btn btn-danger" type="submit" style="width: 90%;">Register</button>
                            </div>
                        </div>
                    

                    </div>
                    {!! Form::close() !!}
                  
                  </div>

                  @include('newview.new.sidemenu.hospital_menu.hospital')
                </div>
            </div>
         </div>
    </div>
    {!! Form::close() !!}
@endsection