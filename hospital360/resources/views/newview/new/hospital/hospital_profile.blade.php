@extends('layouts.new.header')
@section('title', 'Hospital Profile')
@section('content')
<style type="text/css">
    /*.card:hover {
        color: white;
  background-color: red;
}*/
.card{
  height: auto; 
  border-radius: 15px; 
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
}
.card-title{
    margin-top: 5%;
}
.bio{
    border-radius: 17px !important;
    border: 1px solid red;
    margin-left: 5%;
    margin-right: 5%;
    height: 230px;
}
.btn-danger {
    color: #fff;
    background-color: red;
    border-color: red;
}
/*a:hover {
    color: white !important;
    text-decoration: none;
}*/
hr {
    margin-top: 1rem;
    margin-bottom: 1rem;
    border: 0;
    border-top: 1px solid red;
}
.form-control {
   box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
}
label {
    font-size: 15px;
    margin-top: 1%;
}
.pic{
    height: 84px;
    border-radius: 17px !important;
    border: 1px solid red;
}
.alert-success {
    color: white;
    background-color: green;
    border-color: #cdedd8;
}
.parsley-required{
    color: red;
}

.btn2 {
    color: black;
    background-color: white;
    border-color: red;
}


</style>
<!-- <ol class="breadcrumb">
        <li class="breadcrumb-item">Doctors</li>
    </ol> -->
<div class="container-fluid">
      <div class="animated fadeIn">
             <div class="container">
               
                    <div class="row">
                            <div class="col-sm-8">
                              <h1 class="text-center" style="margin-top: 5%;">Hospital Information</h1>
                              @include('flash::message')
                                    <div class="row" style="margin-left: 1%;margin-right: 1%;margin-top: 7%;border: 1px solid #b2b2b2;box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);height: 520px;">

                                          <div class="row" style="padding: 5%">

                                              <div class="col-sm-4">  
                                                    <div class="card newcard text-center border-danger" style="height: 100px;width: 70%">
                                                        
                                                        <div class="card-block cardpadding" style="width: 100%;padding: 10px;">

                                                          @if(isset($doctors->logo)) 

                                                          <?php  $courceimage = URL::asset('public/media/hospital_logo/'.$doctors->logo);?>
                                                          <img  src="<?= $courceimage ?>" alt="Course" style="width: 70%;max-width: 70%">
                                                          
                                                            @else

                                                            <img  src="{{URL::asset('public/noimg.png')}}" alt="Course" style="width: 70%;max-width: 70%">

                                                            @endif
                                                        </div>
                                                        
                                                    </div>
                                               </div>
                                              <div class="col-md-8">
                                                    <div style="margin-top: 10%;width: 160%;"> 
                                                     <h2>{{ Auth::user()->first_name }} {{ Auth::user()->last_name }}</h2>
                                                    </div>
                                              </div>   
                                          </div>

                                          {!! Form::model($doctors, ['route' => ['hospital_profileEdit.update', $doctors->id], 'method' => 'post', 'files' => true,'id' => 'myForm']) !!}

                                            <div class="row" style="margin-bottom: 10%;">

                                                <div class="col-sm-6" style="padding: 5%;border-right: 1px solid gray;">
                                                  {!! Form::textarea('address', null, ['class' => 'form-control','rows' => '8','data-parsley-required-message' =>'Address is required','required','placeholder' => 'Hospital Address']) !!}
                                                </div>
                                                <div class="col-sm-6" style="padding: 5%;">                               
                                                  {!! Form::select('services', ['' => 'Select Services'] + $service->toArray(), null, ['class' => 'form-control']) !!}
                                                  <br><br> 

                                                  {!! Form::select('specialities', ['' => 'Select Specialities'] + $specility->toArray(), null, ['class' => 'form-control']) !!}
                                                </div>
                                            </div>
                                            <div class="row" style="float: right;margin-top: -8%;margin-right: 6%;">
                                                <button class="btn btn-danger" type="submit" style="width: 90px;">Next</button>
                                            </div>

                                           {!! Form::close() !!}

                                    </div>
                            </div>
                            @include('newview.new.sidemenu.hospital_menu.hospital')
                     </div>
              </div>
        </div>
</div>

@endsection                     