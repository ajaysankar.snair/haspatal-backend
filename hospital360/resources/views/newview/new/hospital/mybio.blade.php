@extends('layouts.new.header')
@section('title', 'Hospital Bio')
@section('content')
<style type="text/css">
    /*.card:hover {
        color: white;
  background-color: red;
}*/
.card{
  height: auto; 
  border-radius: 15px; 
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
}
.card-title{
    margin-top: 5%;
}
.bio{
    border-radius: 17px !important;
    border: 1px solid red;
    margin-left: 5%;
    margin-right: 5%;
    height: 230px;
}
.btn-danger {
    color: #fff;
    background-color: red;
    border-color: red;
}
/*a:hover {
    color: white !important;
    text-decoration: none;
}*/
hr {
    margin-top: 1rem;
    margin-bottom: 1rem;
    border: 0;
    border-top: 1px solid red;
}
.form-control {
   box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
}
label {
    font-size: 15px;
    margin-top: 1%;
}
.pic{
    height: 120px;
    border-radius: 17px !important;
    border: 1px solid red;
}
.alert-success {
    color: white;
    background-color: green;
    border-color: #cdedd8;
}
.parsley-required{
    color: red;
}

.btn2 {
    color: black;
    background-color: white;
    border-color: red;
}

</style>
<!-- <ol class="breadcrumb">
        <li class="breadcrumb-item">Doctors</li>
    </ol> -->
    {!! Form::model($haspatal, ['route' => ['hasptal.bio.update', $haspatal->id], 'method' => 'post', 'files' => true,'id' => 'myForm']) !!}
    <div class="container-fluid">
        <div class="animated fadeIn">
             <div class="container">
               @include('flash::message')
                <div class="row">
                      <div class="col-sm-8">
                         <?php 
                            $specilities = DB::table('specilities')->orderBy('specility','asc')->get();

                            ?>
                                <h3 class="text-center" style="margin-top: 5%;">My Bio</h3>
                                <div class="row " style="margin-left: 1%;margin-right: 1%;margin-top: 3%;border: 1px solid #b2b2b2;box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);">
                                    <div class="col-sm-12" style="padding: 5%;">
                                       <div class="">
                                        {!! Form::textarea('bio', null, ['class' => 'form-control','rows' =>   '9','data-parsley-required-message' =>'Bio is required','required','placeholder' => 'My Bio']) !!} 
                                      </div>
                                
                                    </div>
                                  
                                  <div class="col-sm-6" style="padding: 5%;">
                                     <div class="">
                                         <select class="form-control" data-parsley-required-message="Speciality is required" required="" name="specialities">
        
                                              @foreach($specilities as $value1)
                                                  <option value="{{$value1->id}}" @if($value1->id == $haspatal->specialities)selected="selected"@endif >{{$value1->specility}}</option>
                                              @endforeach
                                          </select>
                                      </div>
                                  </div>

                                  <div class="col-sm-6" style="padding: 5%;">
                                     <div class="">
                                         {!! Form::text('telemedicine_coordinator', null, ['class' => 'form-control','data-parsley-required-message' =>'Telemedicine Coordinator ID is required','required','placeholder' => 'Telemedicine Coordinator ID']) !!} 
                                      </div>
                                  </div>

                                  <div class="col-sm-6" style="padding: 5%;">
                                     <div class="">
                                         {!! Form::text('pancard_number', null, ['class' => 'form-control','data-parsley-required-message' =>'Pancard is required','required','placeholder' => 'Pancard number']) !!} 
                                      </div>
                                  </div>

                                  <div class="col-sm-6" style="padding: 5%;">
                                     <div class="">
                                         {!! Form::text('gst_no', null, ['class' => 'form-control','data-parsley-required-message' =>'GST No is required','required','placeholder' => 'GST No']) !!} 
                                      </div>
                                  </div>
                                </div>


                                <div class="row" style="margin-left: 10%;margin-right: 10%;margin-top: 2%;">
                                   <!-- <div class="col-sm-6" style="padding: 5%;">
                                     <button type="button" class="btn btn2" data-toggle="modal" data-target="#myModal" style="width: 50%;margin-left: -40%;">Public View</button>
                                   </div> -->
                                   <div class="col-sm-6" style="padding: 5%;">
                                    <button class="btn btn-danger mobilebutton" type="submit" style="width: 50%;margin-left: 90%;">Update</button>
                                   </div>
                                </div>
                              </div>
                                    @include('newview.new.sidemenu.hospital_menu.profile')
                </div>
                    
            </div>

         </div>
    </div>
    <div class="container">
  <div class="modal fade" id="myModal">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Hospital Profile</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <!-- Modal body -->
        <div class="modal-body">
             <div class="row">

                          <div class="col-sm-4">
                            <?php  //$courceimage = URL::asset('public/media/profile_pic/'.$doctors->profile_pic); ?>

                            
                            <div class="pic">
                                            <img  src="" alt="Course" style="width: 80%;margin-top: 5%;margin-left: 9%;margin-right: 4%;max-width: 80%;max-height: 90%;">
                                        </div>
                          </div>

                          <div class="col-sm-8">
                            <h2 style="color: red"> {{$haspatal->hospital_name}}</h2>
                            <span class="dot"></span>  <span class="dot"></span>  <span class="dot"></span>  <span class="dot"></span>
                            
                          </div>
                          <div class="border1" style="margin-top: 3%;">
                          <p style="margin-left: 3%;text-align: justify;">{{$haspatal->bio}}</p>
                          </div>
             </div>             
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
        
      </div>
    </div>
  </div>
  
</div>
    {!! Form::close() !!}
@endsection