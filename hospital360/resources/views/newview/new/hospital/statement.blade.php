@extends('layouts.new.header')
@section('title', 'Hospital Statement')
@section('content')
<style type="text/css">
    /*.card:hover {
        color: white;
  background-color: red;
}*/
.card{
  height: auto; 
  border-radius: 15px; 
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
}
.card-title{
    margin-top: 5%;
}
.bio{
    border-radius: 17px !important;
    border: 1px solid red;
    margin-left: 5%;
    margin-right: 5%;
    height: 230px;
}
.btn-danger {
    color: #fff;
    background-color: red;
    border-color: red;
}
/*a:hover {
    color: white !important;
    text-decoration: none;
}*/
hr {
    margin-top: 1rem;
    margin-bottom: 1rem;
    border: 0;
    border-top: 1px solid red;
}
.form-control {
   box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
}
label {
    font-size: 15px;
    margin-top: 1%;
}
.pic{
    height: 84px;
    border-radius: 17px !important;
    border: 1px solid red;
}
.alert-success {
    color: white;
    background-color: green;
    border-color: #cdedd8;
}
.parsley-required{
    color: red;
}

.btn2 {
    color: black;
    background-color: white;
    border-color: red;
}
.tablediv{
  margin-left: 3%;
    margin-right: 3%;
    /* margin-top: 3%; */
    border: 1px solid #b2b2b2;
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
    width: 95%;
    margin-top: 0%;
    margin-bottom: 2%;
    text-align: center;
}
</style>
<!-- <ol class="breadcrumb">
        <li class="breadcrumb-item">Doctors</li>
    </ol> -->
     {!! Form::open(['route' => 'h_account_statement_view', 'files' => true,'id' => 'myForm']) !!}
    <div class="container-fluid">
        <div class="animated fadeIn">
             <div class="container">
              
                <div class="row">
                      <div class="col-sm-8">
                                <h1 class="text-center" style="margin-top: 5%;">Statement</h1>
                                <div class="row " style="margin-left: 1%;margin-right: 1%;margin-top: 8%;border: 1px solid #b2b2b2;box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);">
                                  <div class="col-sm-12" style="padding: 5%;">
                                        
                                <div class="row">
                                  <div class="col-md-4" style="margin-left: 10%;">
                                    {!! Form::label('s_date', 'Start Date') !!}
                                      {!! Form::text('s_date', null, ['class' => 'form-control','id'=>'start_date','data-parsley-required-message' =>'Date is required','required']) !!}
                                  </div>
                                  <div class="col-md-4">
                                     {!! Form::label('e_date', 'End Date') !!}
                                      {!! Form::text('e_date', null, ['class' => 'form-control','id'=>'end_date','data-parsley-required-message' =>'Date is required','required']) !!}
                                  </div>
                                  <div class="col-md-2" style="margin-top: 4%;">
                                     <button class="btn btn-danger" type="submit" style="">Submit</button>
                                  </div>
                                </div>
                                        
                              </div>

                          <div class="row tablediv" >
                                  <div class="col-sm-12">
                                        
                                  <div class="table-responsive">          
                                        <table class="table">
                                          <thead>
                                            <tr>
                                              <th>No</th>
                                              <th>Current Balance</th>
                                              <th>Requested Amount</th>
                                              <th>Available Balance</th>
                                            </tr>
                                          </thead>
                                             <?php if(isset($dr_amount)) { ?>
                                              @if (!$dr_amount->isEmpty())
                                                @foreach($dr_amount as $value)
                                          <tbody>
                                            <tr>
                                              <td> {{$value->id}}</td>
                                              <td> {{$value->current_balance}}</td>
                                              <td> {{$value->requested_amount}}</td>
                                              <td>  {{$value->available_balance}}</td>
                                            </tr>
                                          </tbody>
                                            @endforeach
                                           @else
                                           <h3 class="text-center" style="margin-left: 35%;margin-top: 5%;"> No Records Found</h3>
                                           @endif
                                          <?php } ?>
                                        </table>
                                        </div>
                                        
                                    </div>
                                </div>


                                </div>

                              </div>
                                  @include('newview.new.sidemenu.hospital_menu.wallet')
                </div>
                    
            </div>

         </div>
    </div>
    {!! Form::close() !!}
@endsection

@push('scripts')
   <script type="text/javascript">
           $('#start_date').datetimepicker({
               format: 'YYYY-MM-DD',
               useCurrent: true,
               icons: {
                   up: "icon-arrow-up-circle icons font-2xl",
                   down: "icon-arrow-down-circle icons font-2xl"
               },
               sideBySide: true
           })
       </script>
       <script type="text/javascript">
           $('#end_date').datetimepicker({
               format: 'YYYY-MM-DD',
               useCurrent: true,
               icons: {
                   up: "icon-arrow-up-circle icons font-2xl",
                   down: "icon-arrow-down-circle icons font-2xl"
               },
               sideBySide: true
           })
       </script>
@endpush