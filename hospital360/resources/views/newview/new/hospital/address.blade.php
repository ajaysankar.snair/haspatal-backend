@extends('layouts.new.header')
@section('title', 'Haspatal Address')
@section('content')
<style type="text/css">
    /*.card:hover {
        color: white;
  background-color: red;
}*/
.card{
  height: auto; 
  border-radius: 15px; 
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
}
.card-title{
    margin-top: 5%;
}
.bio{
    border-radius: 17px !important;
    border: 1px solid red;
    margin-left: 5%;
    margin-right: 5%;
    height: 230px;
}
.btn-danger {
    color: #fff;
    background-color: red;
    border-color: red;
}
/*a:hover {
    color: white !important;
    text-decoration: none;
}*/
hr {
    margin-top: 1rem;
    margin-bottom: 1rem;
    border: 0;
    border-top: 1px solid red;
}
.form-control {
   box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
}
label {
    font-size: 15px;
    margin-top: 1%;
}
.pic{
    height: 84px;
    border-radius: 17px !important;
    border: 1px solid red;
}
.alert-success {
    color: white;
    background-color: green;
    border-color: #cdedd8;
}
.parsley-required{
    color: red;
}

.btn2 {
    color: black;
    background-color: white;
    border-color: red;
}


</style>
<!-- <ol class="breadcrumb">
        <li class="breadcrumb-item">Doctors</li>
    </ol> -->
    {!! Form::model($haspatal, ['route' => ['hasptal.bio.update', $haspatal->id], 'method' => 'post', 'files' => true,'id' => 'myForm']) !!}
    <div class="container-fluid">
        <div class="animated fadeIn">
             <div class="container">
               @include('flash::message')
                <div class="row">
                      <div class="col-sm-8">
                                <h1 class="text-center" style="margin-top: 5%;">Address</h1>
                                <div class="row " style="margin-left: 1%;margin-right: 1%;margin-top: 10%;border: 1px solid #b2b2b2;box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);">
                                    <div class="col-sm-6" style="padding: 5%;">
                                      {!! Form::text('pincode_id', null, ['class' => 'form-control','data-parsley-required-message' =>'Pincode is required','required','data-parsley-type="digits"','placeholder' => 'Pincode']) !!}

                                      <label for="country_name"></label>
                                      {!! Form::select('state_id', [''=>'Select State'] + $stateList->toArray(), null, ['class' => 'form-control','data-parsley-required-message' =>'State is required','required']) !!}

                                      <label for="country_name"></label>
                                      {!! Form::select('city_id', [''=>'Select City'] + $cityList->toArray(), null, ['class' => 'form-control','data-parsley-required-message' =>'City is required','required']) !!}
                                      </div>
                                    <div class="col-sm-6" style="padding: 5%;">                               
                                     {!! Form::textarea('address', null, ['class' => 'form-control','rows' => '8','data-parsley-required-message' =>'Address is required','required','placeholder' => 'Address']) !!}
                                    </div>
                                </div>
                                <div class="text-center" style="margin-top: 5%;margin-left: 80%;">
                                  <button class="btn btn-danger" type="submit" style="width: 90%;">Submit</button>
                                </div>
                              </div>
                              @include('newview.new.sidemenu.hospital_menu.profile')
                </div>
                    
            </div>

         </div>
    </div>
    {!! Form::close() !!}
@endsection