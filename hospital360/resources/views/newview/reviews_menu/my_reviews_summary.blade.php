@extends('layouts.nheader')
@section('title', 'My Reviews Summary')
@section('content')
<style type="text/css">
    /*.card:hover {
        color: white;
  background-color: red;
}*/
.card{
  height: 250px; 
  border-radius: 20px; 
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
}
.card-title{
    margin-top: 5%;
}
.bio{
    border-radius: 10px !important;
    border: 1px solid red;
    height: 75px;
}
.btn-danger {
    color: #fff;
    background-color: red;
    border-color: red;
}
/*a:hover {
    color: white !important;
    text-decoration: none;
}*/
hr {
    margin-top: 1rem;
    margin-bottom: 1rem;
    border: 0;
    border-top: 1px solid red;
}
.alert-success {
    color: white;
    background-color: green;
    border-color: #cdedd8;
}
.parsley-required{
    color: red;
}
</style>
<!-- <ol class="breadcrumb">
        <li class="breadcrumb-item">Doctors</li>
    </ol> -->
    <div class="container-fluid" style="margin-top: 3%;">
        <div class="animated fadeIn">
             <div class="container">
                <div class="row">
                      <div class="col-sm-12">
                         <div class="card newcard  border-danger">
                            <div class="card-block" style="margin-top: -4%;">
                                <h3 class="card-title text-center" style="margin-left: 2%;">My Reviews Summary</h3>
                                <div class="row" style="margin-left: 1%;margin-right: 1%;margin-top: 5%;">
                                   <div class="col-sm-2">

                                    <div class="text-center">
                                    <h5>Today</h5>
                                    </div>
                                    </div>

                                    <div class="col-sm-4">
                                     <div class="text-center">
                                    <h5>This Week</h5>
                                    </div>
                                    </div>

                                    <div class="col-sm-2">
                                     <div class="text-center">
                                    <h5>This Month</h5>
                                    </div>
                                    </div>

                                    <div class="col-sm-2">
                                     <div class="text-center">
                                    <h5>This Year</h5>
                                    </div>
                                    </div>

                                    <div class="col-sm-2">
                                     <div class="text-center">
                                    <h5>Total</h5>
                                    </div>
                                    </div>

                                </div>
                                <div class="row" style="margin-left: 1%;margin-right: 1%;">
                                     <h3 class="card-title text-center" style="margin-left: 40%;">Graph Design</h3>
                            </div>
                            
                        </div>
                      </div>
                </div>
                    
            </div>

         </div>
    </div>
@endsection