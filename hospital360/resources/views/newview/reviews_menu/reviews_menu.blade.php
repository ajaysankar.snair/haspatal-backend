@extends('layouts.nheader')
@section('title', 'Reviews')
@section('content')
<style type="text/css">
    .card:hover {
        color: white;
  background-color: red;
}
.card{
  height: 215px; 
  border-radius: 20px; 
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
}
.card-title{
    margin-top: 5%;
}
a:hover {
    /*color: white !important;*/
    text-decoration: none;
}
</style>
<!-- <ol class="breadcrumb">
        <li class="breadcrumb-item">Doctors</li>
    </ol> -->
    <div class="container-fluid" style="margin-top: 3%;">
        <div class="animated fadeIn">
             <div class="container">
                    <div class="row">

                        <div class="col-sm-4">
                            <div class="card  newcard text-center border-danger">
                                 <a href="{{ url('/my_reviews_summary') }}" style="color: black;"> 
                                <div class="card-block" style="padding: 50px;">
                                    <i class="fa fa-stethoscope" style="font-size: 80px;"></i>
                                    <h4 class="card-title">Reviews Summary</h4>
                                </div>
                                </a>
                                
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="card newcard text-center border-danger">
                                <a href="{{ url('/reviews') }}" style="color: black;"> 
                                <div class="card-block" style="padding: 50px;">
                                     <i class="fa fa-stethoscope" style="font-size: 80px;"></i>
                                    <h4 class="card-title">Reviews</h4>
                                    
                                </div>
                                </a>
                                
                            </div>
                         </div>
                    
                         <div class="col-sm-4">
                                <div class="card newcard text-center border-danger">
                                    <a href="{{ url('/score_card') }}" style="color: black;"> 
                                    <div class="card-block" style="padding: 50px;">
                                         <i class="fa fa-stethoscope" style="font-size: 80px;"></i>
                                        <h4 class="card-title">Score Card</h4>
                                        
                                    </div>
                                    </a>
                                    
                                </div>

                        </div>

                <div class="col-sm-4">
                    <div class="card newcard text-center border-danger">
                        <a href="{{ url('/reviews') }}" style="color: black;"> 
                        <div class="card-block" style="padding: 50px;">
                             <i class="fa fa-stethoscope" style="font-size: 80px;"></i>
                            <h4 class="card-title">My Responses</h4>
                            
                        </div>
                    </a>
                        
                    </div>
                </div>



                </div>

            </div>

         </div>
    </div>
@endsection