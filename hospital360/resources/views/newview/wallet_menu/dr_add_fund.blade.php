@extends('layouts.nheader')
@section('title', 'Add Fund')
@section('content')
<style type="text/css">
    /*.card:hover {
        color: white;
  background-color: red;
}*/
.card{
  height: 370px; 
  border-radius: 20px; 
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
}
.card-title{
    margin-top: 5%;
}
.bio{
    border-radius: 17px !important;
    border: 1px solid red;
    margin-left: 5%;
    margin-right: 5%;
    height: 230px;
}
.btn-danger {
    color: #fff;
    background-color: red;
    border-color: red;
}
/*a:hover {
    color: white !important;
    text-decoration: none;
}*/
hr {
    margin-top: 1rem;
    margin-bottom: 1rem;
    border: 0;
    border-top: 1px solid red;
}
.form-control {
    border: 1px solid red;
}
label {
    font-size: 15px;
    margin-top: 1%;
}
.parsley-required{
    color: red;
}
.alert-success {
    color: white;
    background-color: green;
    border-color: #cdedd8;
}
</style>
<!-- <ol class="breadcrumb">
        <li class="breadcrumb-item">Doctors</li>
    </ol> -->
    {!! Form::open(['route' => 'doctor_add_fund.store', 'files' => true,'id' => 'myForm']) !!}
    <div class="container-fluid" style="margin-top: 3%;">
        <div class="animated fadeIn">
             <div class="container">
                  @include('flash::message')
                <div class="row">
                      <div class="col-sm-8">
                         <div class="card newcard  border-danger">
                            <div class="card-block" style="margin-top: -4%;">
                                <h3 class="card-title text-center">Add Fund</h3>
                                <div class="row" style="margin-left: 1%;margin-right: 1%;">
                                    <div class="col-sm-12">                                       
                                       <label for="dr_amount">Current Amount</label>
                                        <input type="text" class="form-control" name="" value="<?php echo $last_amount->dr_amount; ?>" readonly>                                  
                                    </div>
                                    <div class="col-sm-12">                                       
                                       <label for="dr_amount">Amount</label>
                                        {!! Form::text('dr_amount', null, ['class' => 'form-control','data-parsley-required-message' =>'Amount is required','id' => 'razorpay_amount','required']) !!}                                        
                                    </div>

                                </div>
                                <div class="text-center" style="margin-top: 1%;">
                                  <button class="btn btn-danger razor" type="button" style="width: 30%;">Submit</button>
                                </div>
                            </div>
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="card newcard text-center border-danger">
                            <div class="card-block">
                                    <h4 class="card-title">Need Help?</h4>
                                    <hr>    
                                    <h4 class="card-title">Video Tutorial</h4>
                                    <hr>    
                                    <h4 class="card-title">Helpbook</h4>
                                    <hr>
                                    <p class="text-center">New Prices shall not be applicable on previously booked consults</p>  
                                </div>
                                
                            </div>
                      </div>
                </div>
                    
            </div>

         </div>
    </div>
  {!! Form::close() !!}     
@endsection
@push('scripts')
 <script src="https://checkout.razorpay.com/v1/checkout.js"></script>
    <script>
         var SITEURL = '{{URL::to('')}}';
         $.ajaxSetup({
           headers: {
               'X-CSRF-TOKEN': $('input[name="_token"]').attr('value')
           }
         }); 
         $(document).on('click', '.razor', function(e){          
           var totalAmount = $("#razorpay_amount").val();
           var options = {
           "key": "rzp_test_XiaDngfJzRGkOZ",
           "amount": (totalAmount*100), // 2000 paise = INR 20
           "name": "Haspatal",
           "description": "Payment",
           "image":"",
           "handler": function (response){
                 $.ajax({
                   url: SITEURL + '/doctor_add_fund',
                   type: 'post',
                   dataType: 'json',
                   data: {
                     payment_id: response.razorpay_payment_id, 
                     dr_amount : totalAmount
                     }, 
                   success: function (msg) {
                        //alert("sucess");
                       window.location.href =  SITEURL + '/add_fund';
                   }
               });
             
           },
           @if(!empty(auth()->user()))
          "prefill": {
               "name":   '{{ auth()->user()->first_name }} {{ auth()->user()->last_name }}',
               "email":   '{{ auth()->user()->email }}',
           },
           @else
           "prefill": {
               "name":    billing_first_name,
               "email":   customer_email,
               "phone":   customer_phone,
           },
           @endif
           "theme": {
               "color": "#528FF0"
           }
         };
         var rzp1 = new Razorpay(options);
         rzp1.open();
         e.preventDefault();
         });
         /*document.getElementsClass('razor').onclick = function(e){
           rzp1.open();
           e.preventDefault();
         }*/
    </script>
    @endpush