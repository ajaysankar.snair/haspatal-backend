@extends('layouts.nheader')
@section('title', 'Current Balance')
@section('content')
<style type="text/css">
    /*.card:hover {
        color: white;
  background-color: red;
}*/
.card{
  height: 330px; 
  border-radius: 20px; 
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
}
.card-title{
    margin-top: 5%;
}
.bio{
    border-radius: 17px !important;
    border: 1px solid red;
    margin-left: 5%;
    margin-right: 5%;
    height: 230px;
}
.btn-danger {
    color: #fff;
    background-color: red;
    border-color: red;
}
/*a:hover {
    color: white !important;
    text-decoration: none;
}*/
hr {
    margin-top: 1rem;
    margin-bottom: 1rem;
    border: 0;
    border-top: 1px solid red;
}
.alert-success {
    color: white;
    background-color: green;
    border-color: #cdedd8;
}
.parsley-required{
    color: red;
}
</style>
<!-- <ol class="breadcrumb">
        <li class="breadcrumb-item">Doctors</li>
    </ol> -->
   
    <div class="container-fluid" style="margin-top: 3%;">
        <div class="animated fadeIn">
             <div class="container">
              
                <div class="row">
                      <div class="col-sm-8">
                     
                         <div class="card newcard text-center border-danger">
                            <div class="card-block" style="margin-top: -4%;">
                                <h3 class="card-title">Current Balance</h3>
                                <div class="bio">

                                 <h1 style="margin-top: 15%;">RS <?php echo $amount; ?></h1>
                                </div>
                                <div style="margin-top: 1%;">
                                 <button class="btn btn-danger" type="button" style="width: 10%;">OK</button>
                                </div>
                            </div>
                            
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="card newcard text-center border-danger">
                            <div class="card-block">
                                    <h4 class="card-title">Sample Bio 1</h4>
                                    <hr>    
                                    <h4 class="card-title">Sample Bio 2</h4>
                                    <hr>    
                                    <h4 class="card-title">Sample Bio 3</h4>
                                    <hr>  
                                    
                                </div>
                                
                            </div>
                      </div>
                </div>
                    
            </div>

         </div>
    </div>
     
@endsection