@extends('layouts.nheader')
@section('title', 'Banking Info')
@section('content')
<style type="text/css">
    /*.card:hover {
        color: white;
  background-color: red;
}*/
.card{
  height: 370px; 
  border-radius: 20px; 
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
}
.card-title{
    margin-top: 5%;
}
.bio{
    border-radius: 17px !important;
    border: 1px solid red;
    margin-left: 5%;
    margin-right: 5%;
    height: 230px;
}
.btn-danger {
    color: #fff;
    background-color: red;
    border-color: red;
}
/*a:hover {
    color: white !important;
    text-decoration: none;
}*/
hr {
    margin-top: 1rem;
    margin-bottom: 1rem;
    border: 0;
    border-top: 1px solid red;
}
.form-control {
    border: 1px solid red;
}
label {
    font-size: 15px;
    margin-top: 1%;
}
.parsley-required{
    color: red;
}
.alert-success {
    color: white;
    background-color: green;
    border-color: #cdedd8;
}
</style>
<!-- <ol class="breadcrumb">
        <li class="breadcrumb-item">Doctors</li>
    </ol> -->
    {!! Form::model($doctors, ['route' => ['doctorsBio.update', $doctors->id], 'method' => 'post', 'files' => true,'id' => 'myForm']) !!} 
    <div class="container-fluid" style="margin-top: 3%;">
        <div class="animated fadeIn">
             <div class="container">
                @include('flash::message')
                <div class="row">
                      <div class="col-sm-8">
                         <div class="card newcard  border-danger">
                            <div class="card-block" style="margin-top: -4%;">
                                <h3 class="card-title text-center">Banking Info</h3>
                                <div class="row" style="margin-left: 1%;margin-right: 1%;">
                                    <div class="col-sm-6">

                                        <label for="bank_name">Bank Name</label>
                                        {!! Form::text('bank_name', null, ['class' => 'form-control','data-parsley-required-message' =>'Bank Name is required','required']) !!}


                                        <label for="ifsc_code">IFSC Code</label>
                                        {!! Form::text('ifsc_code', null, ['class' => 'form-control','data-parsley-required-message' =>'IFSC Code is required','required']) !!}


                                        
                                    </div>
                                     <div class="col-sm-6">
                                        <label for="account_name">Account Name</label>
                                        {!! Form::text('account_name', null, ['class' => 'form-control','data-parsley-required-message' =>'Account Name is required','required']) !!}

                                        <label for="account_no">Account Number</label>
                                        {!! Form::text('account_no', null, ['class' => 'form-control','data-parsley-required-message' =>'Account Number is required','required']) !!}
                                     </div>

                                </div>
                                <div class="col-sm-6">
                                <div style="margin-left: 3%;margin-right: 3%;">
                                 <label >Upload Cancelled Cheque Picture</label> 
                                        {!! Form::file('cheque_img',['class' => 'form-control']) !!}
                                 </div> 
                                 <?php  $courceimage = URL::asset('public/media/cheque_img/'.$doctors->cheque_img); ?>
                                  <div class="col-sm-offset-2 col-sm-3" style="margin-left: 113%;">
                                             <img class="" src="<?= $courceimage ?>" alt="Logo" style="width: 181%;margin-top: -96%;margin-left: 37%;">
                                        </div>
                                 </div>      
                                <div class="text-center" style="margin-top: 1%;">
                                  <button class="btn btn-danger" type="submit" style="width: 30%;">Submit</button>
                                </div>
                            </div>
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="card newcard text-center border-danger">
                            <div class="card-block">
                                    <h4 class="card-title">Need Help?</h4>
                                    <hr>    
                                    <h4 class="card-title">Video Tutorial</h4>
                                    <hr>    
                                    <h4 class="card-title">Helpbook</h4>
                                    <hr>  
                                </div>
                                
                            </div>
                      </div>
                </div>
                    
            </div>

         </div>
    </div>
    {!! Form::close() !!}     
@endsection