@extends('layouts.nheader')
@section('title', 'Account Statement')
@section('content')
<style type="text/css">
    /*.card:hover {
        color: white;
  background-color: red;
}*/
.card{
   /*height: 300px; */
  border-radius: 20px; 
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
}
.card-title{
    margin-top: 5%;
}
.bio{
    border-radius: 17px !important;
    border: 1px solid red;
    margin-left: 5%;
    margin-right: 5%;
    /*height: 230px;*/
}
.btn-danger {
    color: #fff;
    background-color: red;
    border-color: red;
}
/*a:hover {
    color: white !important;
    text-decoration: none;
}*/
hr {
    margin-top: 1rem;
    margin-bottom: 1rem;
    border: 0;
    border-top: 1px solid red;
}
.form-control {
    border: 1px solid red;
}
label {
    font-size: 15px;
    margin-top: 1%;
}
.parsley-required{
    color: red;
}
.alert-success {
    color: white;
    background-color: green;
    border-color: #cdedd8;
}
</style>
<!-- <ol class="breadcrumb">
        <li class="breadcrumb-item">Doctors</li>
    </ol> -->
     {!! Form::open(['route' => 'account_statement_view', 'files' => true,'id' => 'myForm']) !!}
    <div class="container-fluid" style="margin-top: 3%;">
        <div class="animated fadeIn">
             <div class="container">
                
                <div class="row">
                      <div class="col-sm-8">
                         <div class="card newcard  border-danger">
                            <div class="card-block" style="margin-top: -4%;">
                                <h3 class="card-title text-center">Account Statement</h3>
                                <div class="row">
                                  <div class="col-md-4" style="margin-left: 2%;">
                                    {!! Form::label('s_date', 'Start Date') !!}
                                      {!! Form::text('s_date', null, ['class' => 'form-control','id'=>'start_date','data-parsley-required-message' =>'Date is required','required']) !!}
                                  </div>
                                  <div class="col-md-4">
                                     {!! Form::label('e_date', 'End Date') !!}
                                      {!! Form::text('e_date', null, ['class' => 'form-control','id'=>'end_date','data-parsley-required-message' =>'Date is required','required']) !!}
                                  </div>
                                  <div class="col-md-2" style="margin-top: 4%;">
                                     <button class="btn btn-danger" type="submit" style="">Submit</button>
                                  </div>
                                </div>
                                <div class="row" style="margin-left: 1%;margin-right: 1%;margin-top: 5%;">
                                   <div class="col-sm-2">

                                    <div class="text-center">
                                    <h5>No</h5>
                                    </div>
                                    </div>

                                    <div class="col-sm-4">
                                     <div class="text-center">
                                    <h5>Current Balance</h5>
                                    </div>
                                    </div>

                                    <div class="col-sm-3">
                                     <div class="text-center">
                                    <h5>Requested Amount</h5>
                                    </div>
                                    </div>

                                    <div class="col-sm-3">
                                     <div class="text-center">
                                    <h5>Available Balance</h5>
                                    </div>
                                    </div>

                                </div>
                            <?php if(isset($dr_amount)) { ?>
                              @if (!$dr_amount->isEmpty())
                                @foreach($dr_amount as $value)
                                <div class="row bio" style="margin-left: 1%;margin-right: 1%;margin-top: 1%;">
                                    <div class="col-sm-2">

                                      <div class="text-center" style="margin-top: 10%;">
                                       <p>{{$value->id}}</p> 
                                      </div>
                                    </div>
                                    <div class="col-sm-4">

                                      <div class="text-center">
                                       
                                        <p>{{$value->current_balance}}</p>
                                      </div>
                                    </div>

                                    <div class="col-sm-3">

                                      <div class="text-center" >
                                       
                                        <p>{{$value->requested_amount}}</p>
                                        </div>
                                    </div>

                                    <div class="col-sm-3">

                                      <div class="text-center" >
                                        <p> {{$value->available_balance}}</p>
                                        </div>
                                    </div>

                                    <br><br>
                                </div>
                               
                                @endforeach
                                 @else
                                 <h3 class="text-center" style="margin-top: 5%;"> No Records Found</h3>
                                 @endif
                                <?php } ?>
                             </div>
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="card newcard text-center border-danger">
                            <div class="card-block">
                                    <h4 class="card-title">Need Help?</h4>
                                    <hr>    
                                    <h4 class="card-title">Video Tutorial</h4>
                                    <hr>    
                                    <h4 class="card-title">Helpbook</h4>
                                    <hr>  
                                </div>
                                
                            </div>
                      </div>
                </div>
                    
            </div>

         </div>
    </div>
{!! Form::close() !!}       
@endsection
@push('scripts')
   <script type="text/javascript">
           $('#start_date').datetimepicker({
               format: 'YYYY-MM-DD',
               useCurrent: true,
               icons: {
                   up: "icon-arrow-up-circle icons font-2xl",
                   down: "icon-arrow-down-circle icons font-2xl"
               },
               sideBySide: true
           })
       </script>
       <script type="text/javascript">
           $('#end_date').datetimepicker({
               format: 'YYYY-MM-DD',
               useCurrent: true,
               icons: {
                   up: "icon-arrow-up-circle icons font-2xl",
                   down: "icon-arrow-down-circle icons font-2xl"
               },
               sideBySide: true
           })
       </script>
@endpush