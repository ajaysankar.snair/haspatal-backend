@extends('layouts.nheader')
@section('title', 'Wallet')
@section('content')
<style type="text/css">
    .card:hover {
        color: white;
  background-color: red;
}
.card{
  height: 215px; 
  border-radius: 20px; 
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
}
.card-title{
    margin-top: 5%;
}
a:hover {
    /*color: white !important;*/
    text-decoration: none;
}
</style>
<!-- <ol class="breadcrumb">
        <li class="breadcrumb-item">Doctors</li>
    </ol> -->
    <div class="container-fluid" style="margin-top: 3%;">
        <div class="animated fadeIn">
             <div class="container">
                    <div class="row">

                        <div class="col-sm-4">
                                <div class="card  newcard text-center border-danger">
                                     <a href="{{ url('/current_balance') }}" style="color: black;"> 
                                    <div class="card-block" style="padding: 50px;">
                                        <img  src="{{ URL::asset('public/image/icons/My balance.png') }}" style="width: 30%;" alt="logo">
                                        <h4 class="card-title">Current Balance</h4>
                                    </div>
                                    </a>
                                    
                                </div>
                        </div>
                         <div class="col-sm-4">
                                <div class="card newcard text-center border-danger">
                                            <a href="{{ url('/add_fund') }}" style="color: black;"> 
                                            <div class="card-block" style="padding: 50px;">
                                                  <img  src="{{ URL::asset('public/image/icons/Statement.png') }}" style="width: 30%;" alt="logo">
                                                <h4 class="card-title">Add Fund</h4>
                                                
                                            </div>
                                            </a>
                                            
                                        </div>

                        </div>

                        <div class="col-sm-4">
                                <div class="card newcard text-center border-danger">
                                    <a href="{{ url('/refund_request') }}" style="color: black;"> 
                                    <div class="card-block" style="padding: 50px;">
                                         <img  src="{{ URL::asset('public/image/icons/Refund request.png') }}" style="width: 30%;" alt="logo">
                                        <h4 class="card-title">Withdraw</h4>
                                        
                                    </div>
                                    </a>
                                    
                                </div>
                         </div>
                    
                         <div class="col-sm-4">
                                <div class="card newcard text-center border-danger">
                                            <a href="{{ url('/account_statement') }}" style="color: black;"> 
                                            <div class="card-block" style="padding: 50px;">
                                                  <img  src="{{ URL::asset('public/image/icons/Statement.png') }}" style="width: 30%;" alt="logo">
                                                <h4 class="card-title">Statement</h4>
                                                
                                            </div>
                                            </a>
                                            
                                        </div>

                        </div>

                <div class="col-sm-4">
                            <div class="card newcard text-center border-danger">
                                <a href="{{ url('/set_prices') }}" style="color: black;"> 
                                <div class="card-block" style="padding: 50px;">
                                    <i class="fa fa-stethoscope" style="font-size: 80px;"></i>
                                    <h4 class="card-title">Set Prices</h4>
                                    
                                </div>
                                </a>
                                
                            </div>
                </div>

                <div class="col-sm-4">
                                    <div class="card newcard text-center border-danger">
                                        <a href="{{ url('/banking_info') }}" style="color: black;"> 
                                        <div class="card-block" style="padding: 50px;">
                                             <i class="fa fa-stethoscope" style="font-size: 80px;"></i>
                                            <h4 class="card-title">Banking Info</h4>
                                            
                                        </div>
                                        </a>
                                        
                                    </div>
                </div>


                </div>

            </div>

         </div>
    </div>
@endsection