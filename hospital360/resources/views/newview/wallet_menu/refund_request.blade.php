@extends('layouts.nheader')
@section('title', 'Withdraw Request')
@section('content')
<style type="text/css">
    /*.card:hover {
        color: white;
  background-color: red;
}*/
.card{
  height: 370px; 
  border-radius: 20px; 
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
}
.card-title{
    margin-top: 5%;
}
.bio{
    border-radius: 17px !important;
    border: 1px solid red;
    margin-left: 5%;
    margin-right: 5%;
    height: 230px;
}
.btn-danger {
    color: #fff;
    background-color: red;
    border-color: red;
}
/*a:hover {
    color: white !important;
    text-decoration: none;
}*/
hr {
    margin-top: 1rem;
    margin-bottom: 1rem;
    border: 0;
    border-top: 1px solid red;
}
.form-control {
    border: 1px solid red;
}
label {
    font-size: 15px;
    margin-top: 1%;
}
.parsley-required{
    color: red;
}
.alert-success {
    color: white;
    background-color: green;
    border-color: #cdedd8;
}
</style>
<!-- <ol class="breadcrumb">
        <li class="breadcrumb-item">Doctors</li>
    </ol> -->
    {!! Form::open(['route' => 'doctor_withdraw.store', 'files' => true,'id' => 'myForm']) !!}
    <div class="container-fluid" style="margin-top: 3%;">
        <div class="animated fadeIn">
             <div class="container">
                @include('flash::message')
                <div class="row">
                      <div class="col-sm-8">
                         <div class="card newcard  border-danger">
                            <div class="card-block" style="margin-top: -4%;">
                                <h3 class="card-title text-center">Withdraw Request</h3>
                                <div class="row" style="margin-left: 1%;margin-right: 1%;">
                                    <div class="col-sm-12">
                                        
                                      <label for="main_mobile">Current Available Balance</label>
                                      <input type="text" id="total_amount" name="current_balance" class="form-control" value="<? echo $total_amount; ?>" readonly>

                                        <label for="requested_amount">Requested Amount</label>
                                        {!! Form::text('requested_amount', null, ['class' => 'form-control','id' => 'r_amount','data-parsley-required-message' =>'Requested Amount is required','required']) !!}


                                        <label for="available_balance">Available Balance After Request</label>
                                        {!! Form::text('available_balance', null, ['class' => 'form-control','data-parsley-required-message' =>'Available Balance After Request is required','required','id' => 'available_balance','readonly']) !!}
                                        
                                    </div>

                                </div>
                                <div class="text-center" style="margin-top: 1%;">
                                  <button class="btn btn-danger" type="submit" style="width: 30%;">Submit</button>
                                </div>
                            </div>
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="card newcard text-center border-danger">
                            <div class="card-block">
                                    <h4 class="card-title">Need Help?</h4>
                                    <hr>    
                                    <h4 class="card-title">Video Tutorial</h4>
                                    <hr>    
                                    <h4 class="card-title">Helpbook</h4>
                                    <hr>
                                    <p>Refund Requests are processed within 2 Working days</p>  
                                </div>
                                
                            </div>
                      </div>
                </div>
                    
            </div>

         </div>
    </div>
   {!! Form::close() !!}     
@endsection

<script type="text/javascript" src="http://code.jquery.com/jquery-1.4.3.min.js" ></script>
<script type="text/javascript">
    $(document).ready(function(){
         $("#r_amount").keyup(function(){
            //alert($(this).val());
              var bla = $('#total_amount').val();
              var r_amount = $(this).val();

              var total = (bla - r_amount);
              $('#available_balance').val(total);
              
        });
    })
</script>
