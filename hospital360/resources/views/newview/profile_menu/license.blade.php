@extends('layouts.nheader')
@section('title', 'License')
@section('content')
<style type="text/css">
    /*.card:hover {
        color: white;
  background-color: red;
}*/
.card{
  height: 380px; 
  border-radius: 20px; 
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
}
.card-title{
    margin-top: 5%;
}
.bio{
    border-radius: 17px !important;
    border: 1px solid red;
    margin-left: 5%;
    margin-right: 5%;
    height: 230px;
}
.btn-danger {
    color: #fff;
    background-color: red;
    border-color: red;
}
/*a:hover {
    color: white !important;
    text-decoration: none;
}*/
hr {
    margin-top: 1rem;
    margin-bottom: 1rem;
    border: 0;
    border-top: 1px solid red;
}
.form-control {
    border: 1px solid red;
}
label {
    font-size: 15px;
    margin-top: 1%;
}
.alert-success {
    color: white;
    background-color: green;
    border-color: #cdedd8;
}
.parsley-required{
    color: red;
}
</style>
<!-- <ol class="breadcrumb">
        <li class="breadcrumb-item">Doctors</li>
    </ol> -->
    {!! Form::model($doctors, ['route' => ['doctorsBio.update', $doctors->id], 'method' => 'post', 'files' => true,'id' => 'myForm']) !!}
    <div class="container-fluid" style="margin-top: 3%;">
        <div class="animated fadeIn">
             <div class="container">
              @include('flash::message')
                <div class="row">
                      <div class="col-sm-8">
                         <div class="card newcard  border-danger">
                            <div class="card-block" style="margin-top: -4%;">
                                <h3 class="card-title text-center">License</h3>
                                <div class="row" style="margin-left: 1%;margin-right: 1%;">
                                    <div class="col-sm-6">
                                      <label for="country_name">License Type</label>
                                      {!! Form::select('licence_type', [''=>'Select Licence Type'] + $licenceTyes->toArray(), null, ['class' => 'form-control','data-parsley-required-message' =>'Licence Type is required','required']) !!}

                                      <label >License / Registrations No</label>
                                      {!! Form::text('licence_no', null, ['class' => 'form-control','data-parsley-required-message' =>'Licence No is required','required']) !!}

                                      <label for="date">Valid Up to</label>
                                      {!! Form::text('valide_upto', null, ['class' => 'form-control','id'=>'valide_upto','data-parsley-required-message' =>'Valid Upto is required', 'required']) !!}
                                    </div>
                                    <div class="col-sm-6">
                                      <label >Issued By</label>
                                     {!! Form::text('issued_by', null, ['class' => 'form-control','data-parsley-required-message' =>'Issued By is required', 'required']) !!}

                                      <label >Upload License Copy</label>
                                      {!! Form::file('licence_copy',[]) !!}

                                      <p>Kindly upload a clear picture</p>

                                      <?php  $courceimage = URL::asset('public/media/licence_copy/'.$doctors->licence_copy); ?>
                                      <label >License Copy Current Picture</label>
                                     <img  src="<?= $courceimage ?>" alt="Course" style="width: 20%;margin-left: 30%;margin-right: 30%;max-width: 40%;"> 
                                    </div>

                                </div>
                                <div class="text-center" style="margin-top: 1%;">
                                  <button class="btn btn-danger" type="submit" style="width: 30%;">Submit</button>
                                </div>
                            </div>
                            
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="card newcard text-center border-danger">
                            <div class="card-block">
                                    <h4 class="card-title">Need Help?</h4>
                                    <hr>    
                                    <h4 class="card-title">Video Tutorial</h4>
                                    <hr>    
                                    <h4 class="card-title">Helpbook</h4>
                                    <hr> 

                                </div>
                                
                            </div>
                      </div>
                </div>
                    
            </div>

         </div>
    </div>
    {!! Form::close() !!}
@endsection

@push('scripts')
<script type="text/javascript">
           $('#valide_upto').datetimepicker({
               format: 'YYYY-MM-DD',
               useCurrent: true,
               icons: {
                   up: "icon-arrow-up-circle icons font-2xl",
                   down: "icon-arrow-down-circle icons font-2xl"
               },
               sideBySide: true
           })
       </script>
@endpush
