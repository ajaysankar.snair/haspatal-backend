@extends('layouts.nheader')
@section('title', 'Profile Pic')
@section('content')
<style type="text/css">
    /*.card:hover {
        color: white;
  background-color: red;
}*/
.card{
  height: 330px; 
  border-radius: 20px; 
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
}
.card-title{
    margin-top: 5%;
}
.bio{
    border-radius: 17px !important;
    border: 1px solid red;
    margin-left: 5%;
    margin-right: 5%;
    height: 230px;
}
.btn-danger {
    color: #fff;
    background-color: red;
    border-color: red;
}
/*a:hover {
    color: white !important;
    text-decoration: none;
}*/
hr {
    margin-top: 1rem;
    margin-bottom: 1rem;
    border: 0;
    border-top: 1px solid red;
}
.form-control {
    border: 1px solid red;
}
label {
    font-size: 15px;
    margin-top: 1%;
}
.pic{
    height: 84px;
    border-radius: 17px !important;
    border: 1px solid red;
}
.alert-success {
    color: white;
    background-color: green;
    border-color: #cdedd8;
}
.parsley-required{
    color: red;
}
</style>
<!-- <ol class="breadcrumb">
        <li class="breadcrumb-item">Doctors</li>
    </ol> -->
    {!! Form::model($doctors, ['route' => ['doctorsBio.update', $doctors->id], 'method' => 'post', 'files' => true,'id' => 'myForm']) !!}
    <div class="container-fluid" style="margin-top: 3%;">
        <div class="animated fadeIn">
             <div class="container">
              @include('flash::message')
                <div class="row">
                      <div class="col-sm-8">
                         <div class="card newcard  border-danger">
                            <div class="card-block" style="margin-top: -4%;">
                                <h3 class="card-title text-center">Profile Pic</h3>
                                <div class="row" style="margin-left: 1%;margin-right: 1%;">
                                    <div class="col-sm-6">
                                      <?php  $courceimage = URL::asset('public/media/profile_pic/'.$doctors->profile_pic); ?>
                                      <label for="main_mobile">Current Picture</label>

                                        <div class="pic">
                                            <img  src="<?= $courceimage ?>" alt="Course" style="width: 27%;margin-top: 2%;margin-left: 37%;margin-right: 30;max-width: 50%;">
                                        </div>

                                    </div>
                                    <div class="col-sm-6">
                                    <label >New Picture</label> 

                                    <div class="text-center pic" style="padding: 32px;">
                                      {!! Form::file('profile_pic',[]) !!}
                                      </div>
                                    </div>

                                </div>
                                <?php if(Auth::user()->plan_id == 2){ ?>
                                 <div class="row" style="margin-left: 1%;margin-right: 1%;">
                                    <div class="col-sm-6">
                                      <?php  $courceimage = URL::asset('public/media/clinic_logo/'.$doctors->clinic_logo); ?>
                                      <label for="main_mobile">Current Clinic Logo</label>

                                        <div class="pic">
                                             <img  src="<?= $courceimage ?>" alt="Logo" style="width: 27%;margin-top: 2%;margin-left: 37%;margin-right: 30;max-width: 50%;">
                                        </div>

                                    </div>
                                    <div class="col-sm-6">
                                    <label >New Clinic Logo</label> 

                                    <div class="text-center pic" style="padding: 32px;">
                                      {!! Form::file('clinic_logo',[]) !!}
                                      </div>
                                    </div>
                                </div>
                                <?php } ?>
                                <div class="text-center" style="margin-top: 1%;">
                                  <button class="btn btn-danger" type="submit" style="width: 30%;">Submit</button>
                                </div>
                            </div>
                            
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="card newcard text-center border-danger">
                            <div class="card-block">
                                    <h4 class="card-title">Need Help?</h4>
                                    <hr>    
                                    <h4 class="card-title">Video Tutorial</h4>
                                    <hr>    
                                    <h4 class="card-title">Helpbook</h4>
                                    <hr>  
                                </div>
                                
                            </div>
                      </div>
                </div>
                    
            </div>

         </div>
    </div>
    {!! Form::close() !!}
@endsection