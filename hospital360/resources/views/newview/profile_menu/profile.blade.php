@extends('layouts.nheader')
@section('title', 'Profile')
@section('content')
<style type="text/css">
    .card:hover {
        color: white;
  background-color: red;
}
.card{
  height: 215px; 
  border-radius: 20px; 
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
}
.card-title{
    margin-top: 5%;
}
a:hover {
    color: white !important;
    text-decoration: none;
}
</style>
<!-- <ol class="breadcrumb">
        <li class="breadcrumb-item">Doctors</li>
    </ol> -->
    <div class="container-fluid" style="margin-top: 3%;">
        <div class="animated fadeIn">
             <div class="container">
    <div class="card-deck">
        <div class="card  newcard text-center border-danger">
             <a href="{{ url('/mybio') }}" style="color: black;"> 
            <div class="card-block" style="padding: 50px;">
                <i class="fa fa-stethoscope" style="font-size: 80px;"></i>
                <h4 class="card-title">My Bio</h4>
            </div>
        </a>
            
        </div>

        <div class="card newcard text-center border-danger">
            <a href="{{ url('/address') }}" style="color: black;"> 
            <div class="card-block" style="padding: 50px;">
            	<img  src="{{ URL::asset('public/image/icons/Address.png') }}" style="width: 30%;" alt="logo">
                <h4 class="card-title">Address</h4>
                
            </div>
        </a>
            
        </div>

        <div class="card newcard text-center border-danger">
            <a href="{{ url('/contact') }}" style="color: black;"> 
            <div class="card-block" style="padding: 50px;">
            	<img  src="{{ URL::asset('public/image/icons/Contact info.png') }}" style="width: 30%;" alt="logo">
                <h4 class="card-title">Contact Info</h4>
                
            </div>
        </a>
            
        </div>
    </div>
    <div class="card-deck mt-4">
        <div class="card newcard text-center border-danger">
            <a href="{{ url('/license') }}" style="color: black;"> 
            <div class="card-block" style="padding: 50px;">
            	<i class="fa fa-stethoscope" style="font-size: 80px;"></i>
                <h4 class="card-title">License</h4>
                
            </div>
            </a>
            
        </div>

        <div class="card newcard text-center border-danger">
            <a href="{{ url('/password') }}" style="color: black;"> 
            <div class="card-block" style="padding: 50px;">
            	<img  src="{{ URL::asset('public/image/icons/Password.png') }}" style="width: 30%;" alt="logo">
                <h4 class="card-title">Password</h4>
                
            </div>
        </a>
            
        </div>
        <div class="card newcard text-center border-danger">
            <a href="{{ url('/profilepic') }}" style="color: black;"> 
            <div class="card-block" style="padding: 50px;">
            	<img  src="{{ URL::asset('public/image/icons/My profile.png') }}" style="width: 30%;" alt="logo">
                <h4 class="card-title">Profile Pic</h4>
            </div>
            </a>
        </div>
    </div>
</div>

         </div>
    </div>
@endsection