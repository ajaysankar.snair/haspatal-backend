@extends('layouts.nheader')
@section('title', 'My Bio')
@section('content')
<style type="text/css">
    /*.card:hover {
        color: white;
  background-color: red;
}*/
.card{
  height: 330px; 
  border-radius: 20px; 
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
}
.card-title{
    margin-top: 5%;
}
.bio{
    border-radius: 17px !important;
    border: 1px solid red;
    margin-left: 5%;
    margin-right: 5%;
    height: 230px;
}
.border1{
  border-radius: 17px !important;
    border: 1px solid red;
    margin-left: 3%;
    margin-right: 2%;
    height: 231px;
    width: 94%;
}
.btn-danger {
    color: #fff;
    background-color: red;
    border-color: red;
}
/*a:hover {
    color: white !important;
    text-decoration: none;
}*/
hr {
    margin-top: 1rem;
    margin-bottom: 1rem;
    border: 0;
    border-top: 1px solid red;
}
.alert-success {
    color: white;
    background-color: green;
    border-color: #cdedd8;
}
.parsley-required{
    color: red;
}
.pic{
    height: 120px;
    border-radius: 17px !important;
    border: 1px solid red;
}
.dot {
    height: 20px;
    width: 20px;
  background-color: #bbb;
  border-radius: 50%;
  display: inline-block;
}
</style>
<!-- <ol class="breadcrumb">
        <li class="breadcrumb-item">Doctors</li>
    </ol> -->
   {!! Form::model($doctors, ['route' => ['doctorsBio.update', $doctors->id], 'method' => 'post', 'files' => true,'id' => 'myForm']) !!}
    <div class="container-fluid" style="margin-top: 3%;">
        <div class="animated fadeIn">
             <div class="container">
              @include('flash::message')
                <div class="row">
                      <div class="col-sm-8">
                     
                         <div class="card newcard text-center border-danger">
                            <div class="card-block" style="margin-top: -4%;">
                                <h3 class="card-title">My Bio</h3>
                                <div class="bio">
                                 {!! Form::textarea('bio', null, ['class' => 'form-control','rows' => '9','data-parsley-required-message' =>'Bio is required','required']) !!} 
                                </div>
                                <div style="margin-top: 1%;">
                                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModal" style="width: 30%;">Public View</button>
                                 
                                  <button class="btn btn-danger" type="submit" style="width: 30%;">Update</button>
                                </div>
                            </div>
                            
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="card newcard text-center border-danger">
                            <div class="card-block">
                                    <h4 class="card-title">Sample Bio 1</h4>
                                    <hr>    
                                    <h4 class="card-title">Sample Bio 2</h4>
                                    <hr>    
                                    <h4 class="card-title">Sample Bio 3</h4>
                                    <hr>  
                                   <!--  <p> Word Count 146 words</p>  --> 
                                </div>
                                
                            </div>
                      </div>
                </div>
                    
            </div>

         </div>
    </div>

<div class="container">
  <div class="modal fade" id="myModal">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Doctor Profile</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <!-- Modal body -->
        <div class="modal-body">
             <div class="row">

                          <div class="col-sm-4">
                            <?php  $courceimage = URL::asset('public/media/profile_pic/'.$doctors->profile_pic); ?>
                            <div class="pic">
                                            <img  src="<?= $courceimage ?>" alt="Course" style="width: 90%;margin-top: 8%;margin-left: 4%;margin-right: 4%;max-width: 90%;">
                                        </div>
                          </div>

                          <div class="col-sm-8">
                            <h2 style="color: red"> {{$doctors->first_name}} {{$doctors->last_name}}</h2>
                            <span class="dot"></span>  <span class="dot"></span>  <span class="dot"></span>  <span class="dot"></span>
                            <h6>Experience : {{$doctors->experience}}</h6>
                            <h6>Speciality : {{$doctors->specility_name}}</h6>
                            <h6>Language   : {{$doctors->language_name}}</h6>
                          </div>
                          <div class="border1" style="margin-top: 3%;">
                          <p style="margin-left: 3%;text-align: justify;">{{$doctors->bio}}</p>
                          </div>
             </div>             
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
        
      </div>
    </div>
  </div>
  
</div>
     {!! Form::close() !!}
@endsection