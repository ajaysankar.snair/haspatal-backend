<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{{ $prescriptionDetails->id }}</p>
</div>

<!-- Patient Id Field -->
<div class="form-group">
    {!! Form::label('patient_id', 'Patient Id:') !!}
    <p>{{ $prescriptionDetails->patient_id }}</p>
</div>

<!-- Doctor Id Field -->
<div class="form-group">
    {!! Form::label('doctor_id', 'Doctor Id:') !!}
    <p>{{ $prescriptionDetails->doctor_id }}</p>
</div>

<!-- Booking Id Field -->
<div class="form-group">
    {!! Form::label('booking_id', 'Booking Id:') !!}
    <p>{{ $prescriptionDetails->booking_id }}</p>
</div>

<!-- Prescription Field -->
<div class="form-group">
    {!! Form::label('prescription', 'Prescription:') !!}
    <p>{{ $prescriptionDetails->prescription }}</p>
</div>

<!-- Prescription Image Field -->
<div class="form-group">
    {!! Form::label('prescription_image', 'Prescription Image:') !!}
    <p>{{ $prescriptionDetails->prescription_image }}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{{ $prescriptionDetails->status }}</p>
</div>

<!-- Created By Field -->
<div class="form-group">
    {!! Form::label('created_by', 'Created By:') !!}
    <p>{{ $prescriptionDetails->created_by }}</p>
</div>

<!-- Updated By Field -->
<div class="form-group">
    {!! Form::label('updated_by', 'Updated By:') !!}
    <p>{{ $prescriptionDetails->updated_by }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $prescriptionDetails->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $prescriptionDetails->updated_at }}</p>
</div>

