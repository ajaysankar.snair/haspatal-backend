<div class="row">
<div class="form-group col-sm-6">
    {!! Form::label('patient_id', 'Patient Name:') !!}
    {!! Form::select('patient_id', [''=>'Select Patient'] + $patientList->toArray(), null, ['class' => 'form-control','data-parsley-required-message' =>'Patient Name is required','required']) !!}
</div>

<!-- Doctor Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('doctor_id', 'Doctor Name:') !!}
    {!! Form::select('doctor_id', [''=>'Select Doctor'] + $doctorList->toArray(), null, ['class' => 'form-control','data-parsley-required-message' =>'Doctor is required','required']) !!}
</div>
</div>
<div class="row">
<!-- Booking Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('booking_id', 'Booking ID:') !!}
    {!! Form::text('booking_id', null, ['class' => 'form-control','data-parsley-required-message' =>'Booking ID is required','required']) !!}
</div>

<!-- Prescription Field -->
<div class="form-group col-sm-6 ">
    {!! Form::label('prescription', 'Prescription:') !!}
    {!! Form::textarea('prescription', null, ['class' => 'form-control ckeditor', 'id' => 'messageArea', 'rows' => '7',]) !!}
</div>
</div>
<div class="row">

<!-- Prescription Image Field -->
<div class="form-group col-sm-6">
    {!! Form::label('prescription_image', 'Prescription Image:') !!}
    {!! Form::file('prescription_image',['data-parsley-required-message' =>'Prescription Image is required','required']) !!}
</div>
<div class="clearfix"></div>
</div>
<!-- Status Field -->
<!-- <div class="form-group col-sm-6">
    {!! Form::label('status', 'Status:') !!}
    {!! Form::text('status', null, ['class' => 'form-control']) !!}
</div> -->


<div class="row">

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('prescriptionDetails.index') }}" class="btn btn-secondary">Cancel</a>
</div>
</div>
