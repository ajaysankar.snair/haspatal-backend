<div class="table-responsive-sm">
    <table class="table table-striped" id="prescriptionDetails-table">
        <thead>
            <tr>
                <th>Patient Name</th>
        <th>Doctor Name</th>
        <th>Booking Id</th>
        <th>Prescription</th>
        <th>Prescription Image</th>
        
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>

        @foreach($prescriptionDetails as $prescriptionDetails)
                
            <tr>
                <td>{{ $prescriptionDetails->p_first_name }}</td>
            <td>{{ $prescriptionDetails->d_first_name }}</td>
            <td>{{ $prescriptionDetails->booking_id }}</td>
            <td>{{ $prescriptionDetails->prescription }}</td>
            <td>{{ $prescriptionDetails->prescription_image }}</td>
           
                <td>
                    {!! Form::open(['route' => ['prescriptionDetails.destroy', $prescriptionDetails->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('prescriptionDetails.show', [$prescriptionDetails->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('prescriptionDetails.edit', [$prescriptionDetails->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>