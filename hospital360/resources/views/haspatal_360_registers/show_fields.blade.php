<!-- Full Name Field -->
<div class="form-group">
    {!! Form::label('full_name', 'Full Name:') !!}
    <p>{{ $haspatal360Register->name_prefix }} {{ $haspatal360Register->full_name }}</p>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', 'Email:') !!}
    <p>{{ $haspatal360Register->email }}</p>
</div>

<!-- Mobile No Field -->
<div class="form-group">
    {!! Form::label('mobile_no', 'Mobile No:') !!}
    <p>{{ $haspatal360Register->mo_prefix }} {{ $haspatal360Register->mobile_no }}</p>
</div>


<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <?php if($haspatal360Register->status == '0') { ?>
    <a href="{{ route('h360_active',['id'=> $haspatal360Register->id]) }}">
    <span class="btn btn-xs btn-success">
        Active</a>
    </span>
    <?php }else{ ?>
        <a href="{{ route('h360_deactive',['id'=> $haspatal360Register->id]) }}">
        <span class="btn btn-xs btn-danger">
        Deactive</a>
    </span>
    <?php } ?>
</div>

<strong><label>Business Register</label></strong>

<div class="form-group">
    {!! Form::label('b_name', 'Buisness Name:') !!}
    <p>{{ $haspatal360Register->h360buisness->b_name }}</p>
</div>

<div class="form-group">
    {!! Form::label('conatact_person', 'Contact Person Name:') !!}
    <p>{{ $haspatal360Register->h360buisness->conatact_person }}</p>
</div>

<div class="form-group">
    {!! Form::label('mobile1', 'Mobile 1:') !!}
    <p>{{ $haspatal360Register->h360buisness->mobile1 }}</p>
</div>

<div class="form-group">
    {!! Form::label('mobile2', 'Mobile 2:') !!}
    <p>{{ $haspatal360Register->h360buisness->mobile2 }}</p>
</div>

<div class="form-group">
    {!! Form::label('email', 'Email:') !!}
    <p>{{ $haspatal360Register->h360buisness->email }}</p>
</div>

<div class="form-group">
    {!! Form::label('open_time', 'Open Time:') !!}
    <p>{{ $haspatal360Register->h360buisness->open_time }}</p>
</div>

<div class="form-group">
    {!! Form::label('close_time', 'Close Time:') !!}
    <p>{{ $haspatal360Register->h360buisness->close_time }}</p>
</div>

<div class="form-group">
    {!! Form::label('open_24', 'Open 24:') !!}
    @if($haspatal360Register->h360buisness->open_24 == 1)
    <p>Yes</p>
    @else
    <p>No</p>
    @endif
</div>

<div class="form-group">
    {!! Form::label('weekly_off', 'Weekly Off:') !!}
    <p>{{ $haspatal360Register->h360buisness->weekly_off }}</p>
</div>

<div class="form-group">
    {!! Form::label('all_days_open', 'All Days Open:') !!}
    @if($haspatal360Register->h360buisness->all_days_open == 1)
    <p>Yes</p>
    @else
    <p>No</p>
    @endif
</div>

<div class="form-group">
    {!! Form::label('address', 'Address:') !!}
    <p>{{ $haspatal360Register->h360buisness->address }}</p>
</div>

<div class="form-group">
    <?php $b_lic = URL::asset('public/media/b_lic/'.$haspatal360Register->h360buisness->b_lic); ?>

    {!! Form::label('b_lic', 'Buisness License:') !!}
    @if(!empty($haspatal360Register->h360buisness->b_lic))
            <p><img src = "<?php echo $b_lic; ?>" style="height:150px;width: 150px;" ></p>
    @else
            <p><img src = "{{url('public/image/default.png')}}" style="height:150px;width: 150px;" ></p>
    @endif
</div>

<div class="form-group">
    <?php $b_card_pic = URL::asset('public/media/b_card_pic/'.$haspatal360Register->h360buisness->b_card_pic); ?>

    {!! Form::label('b_card_pic', 'Buisness Card Picture:') !!}
    @if(!empty($haspatal360Register->h360buisness->b_card_pic))
            <p><img src = "<?php echo $b_card_pic; ?>" style="height:150px;width: 150px;" ></p>
    @else
            <p><img src = "{{url('public/image/default.png')}}" style="height:150px;width: 150px;" ></p>
    @endif
</div>

<div class="form-group">
    <?php $shop_pic = URL::asset('public/media/shop_pic/'.$haspatal360Register->h360buisness->shop_pic); ?>
    
    {!! Form::label('shop_pic', 'Shop Picture:') !!}
    @if(!empty($haspatal360Register->h360buisness->shop_pic))
            <p><img src = "<?php echo $shop_pic; ?>" style="height:150px;width: 150px;" ></p>
    @else
            <p><img src = "{{url('public/image/default.png')}}" style="height:150px;width: 150px;" ></p>
    @endif
</div>