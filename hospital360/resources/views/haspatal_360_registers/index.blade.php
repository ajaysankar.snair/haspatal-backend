@extends('layouts.app')
@section('title','View H360 Register')
@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">H360 Registers</li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
             @include('flash::message')
             <div class="row">
                 <div class="col-lg-12">
                     <div class="card">
                         <div class="card-header">
                             <i class="fa fa-align-justify"></i>
                             H360 Registers
                             <!-- <a class="pull-right" href="{{ route('haspatal360Registers.create') }}"><i class="fa fa-plus-square fa-lg"></i></a> -->
                         </div>
                         <div class="card-body">
                             @include('haspatal_360_registers.table')
                              <div class="pull-right mr-3">
                                     
                              </div>
                         </div>
                     </div>
                  </div>
             </div>
         </div>
    </div>
@endsection

