@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
          <li class="breadcrumb-item">
             <a href="{!! route('haspatal360Registers.index') !!}">Haspatal 360 Register</a>
          </li>
          <li class="breadcrumb-item active">Edit</li>
        </ol>
    <div class="container-fluid">
         <div class="animated fadeIn">
             @include('coreui-templates::common.errors')
             <div class="row">
                 <div class="col-lg-12">
                      <div class="card">
                          <div class="card-header">
                              <i class="fa fa-edit fa-lg"></i>
                              <strong>Edit Haspatal 360 Register</strong>
                          </div>
                          <div class="card-body">
                              {!! Form::model($haspatal360Register, ['route' => ['haspatal360Registers.update', $haspatal360Register->id], 'method' => 'patch']) !!}

                              @include('haspatal_360_registers.fields')

                              {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
         </div>
    </div>
@endsection