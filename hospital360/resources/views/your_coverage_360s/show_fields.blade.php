<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{{ $yourCoverage360->id }}</p>
</div>

<!-- C State Id Field -->
<div class="form-group">
    {!! Form::label('c_state_id', 'C State Id:') !!}
    <p>{{ $yourCoverage360->c_state_id }}</p>
</div>

<!-- C City Id Field -->
<div class="form-group">
    {!! Form::label('c_city_id', 'C City Id:') !!}
    <p>{{ $yourCoverage360->c_city_id }}</p>
</div>

<!-- C Pincode Field -->
<div class="form-group">
    {!! Form::label('c_pincode', 'C Pincode:') !!}
    <p>{{ $yourCoverage360->c_pincode }}</p>
</div>

<!-- Created By Field -->
<div class="form-group">
    {!! Form::label('created_by', 'Created By:') !!}
    <p>{{ $yourCoverage360->created_by }}</p>
</div>

<!-- Updated By Field -->
<div class="form-group">
    {!! Form::label('updated_by', 'Updated By:') !!}
    <p>{{ $yourCoverage360->updated_by }}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{{ $yourCoverage360->status }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $yourCoverage360->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $yourCoverage360->updated_at }}</p>
</div>

