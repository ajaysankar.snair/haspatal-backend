<!-- C State Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('c_state_id', 'C State Id:') !!}
    {!! Form::text('c_state_id', null, ['class' => 'form-control']) !!}
</div>

<!-- C City Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('c_city_id', 'C City Id:') !!}
    {!! Form::text('c_city_id', null, ['class' => 'form-control']) !!}
</div>

<!-- C Pincode Field -->
<div class="form-group col-sm-6">
    {!! Form::label('c_pincode', 'C Pincode:') !!}
    {!! Form::text('c_pincode', null, ['class' => 'form-control']) !!}
</div>

<!-- Created By Field -->
<div class="form-group col-sm-6">
    {!! Form::label('created_by', 'Created By:') !!}
    {!! Form::text('created_by', null, ['class' => 'form-control']) !!}
</div>

<!-- Updated By Field -->
<div class="form-group col-sm-6">
    {!! Form::label('updated_by', 'Updated By:') !!}
    {!! Form::text('updated_by', null, ['class' => 'form-control']) !!}
</div>

<!-- Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('status', 'Status:') !!}
    {!! Form::text('status', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('yourCoverage360s.index') }}" class="btn btn-secondary">Cancel</a>
</div>
