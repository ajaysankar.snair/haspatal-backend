@extends('layouts.app')
@section('title', 'Add Role')
@section('content')

<section class="content-header">
    <!-- <h1>Enter Details of the role</h1> -->
</section>
    <div class="col-md-12" >
        @include('coreui-templates::common.errors')
        <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <i class="fa fa-plus-square-o fa-lg"></i>
                                <strong>Create Role</strong>
                            </div>
                            <div class="card-body">
                                {!! Form::Open(['url' => 'user/role','id' => 'rolesform','files'=>'true','id'=>'myForm']) !!}
                                <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        {!! Form::label('name','Role Name') !!}
                                        {!! Form::text('name',null,['class'=>'form-control', 'id' => 'name','required' => 'required','data-parsley-pattern'=>"^[a-z A-Z]+$",'data-parsley-required-message' => 'Role Name is required']) !!}
                                    </div>
                                </div>

                               <div class="col-sm-6">
                                    <div class="form-group">
                                        {!! Form::label('display_name','Display Name') !!}
                                        {!! Form::text('display_name',null,['class'=>'form-control', 'id' => 'display_name','required' => 'required','data-parsley-pattern'=>"^[a-z A-Z]+$",'data-parsley-required-message' => 'Display Name is required']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        {!! Form::label('description','Description') !!}
                                        {!! Form::text('description',null,['class'=>'form-control', 'id' => 'description','required' => 'required','data-parsley-pattern'=>"^[a-z A-Z]+$",'data-parsley-required-message' => 'Description is required']) !!}
                                    </div>
                                </div>
                                <div class="form-group col-sm-12">
                                {!! Form::submit('Create', ['class' => 'btn btn-primary pull-right']) !!}
                                </div>
                            </div>
                            </div>
                        </div>

                      
                       <div class="card">
                            <div class="card-header">
                                <i class="fa fa-plus-square-o fa-lg"></i>
                                <strong>Set Permission</strong>
                            </div>
                            <div class="card-body">
                                @include('coreui-templates::common.errors')
                                <div class="box box-primary">

                                    <div class="box-body">
                                        <div class="col-md-12">
                                            @foreach($permissions->groupBy('group_key') as $permission_group)
                                                <h5><b>{{$permission_group->pluck('group_key')->pop()}}</b></h5>
                                                <div class="row">
                                                    @foreach($permission_group as $permission)
                                                        <div class="form-group col-sm-3">
                                                            <div class="checkbox checkbox-theme">
                                                                <input type="checkbox" name="permissions[]" id="permission_{{$permission->id}}" value="{{$permission->id}}">
                                                                <label for="permission_{{$permission->id}}">{{ $permission->display_name }}</label>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            <div class="row">
                                <div class="form-group col-sm-12">
                                    {!! Form::submit('Create', ['class' => 'btn btn-primary pull-right']) !!}
                                </div>
                            </div>
                            </div>

                        </div>
                    </div>
        </div> 
    </div>
    </div>
</div>



        {!! Form::Close() !!}

@stop