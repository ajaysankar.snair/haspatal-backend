@extends('layouts.app')
@section('title', 'Edit User')
@section('content')

<section class="content-header">
    <h1>Enter Details of the user</h1>
</section>
    <div class="col-md-12">
        @include('coreui-templates::common.errors')
        <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <i class="fa fa-plus-square-o fa-lg"></i>
                                <strong>Edit User</strong>
                            </div>
                            <div class="card-body">

                    {!! Form::Open(['method' => 'POST','id' => 'usersform','files'=>'true','action' => ['AclController@updateUser',$user->id]]) !!}

                    <div class="panel no-border">
                              <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        {!! Form::label('first_name','First Name') !!}
                                        {!! Form::text('first_name',$user->first_name,['class'=>'form-control', 'id' => 'name']) !!}
                                    </div>
                                </div>
                                 <div class="col-sm-6">
                                    <div class="form-group">
                                        {!! Form::label('last_name','last Name') !!}
                                        {!! Form::text('last_name',$user->last_name,['class'=>'form-control', 'id' => 'name']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        {!! Form::label('email','Email') !!}
                                        {!! Form::text('email',$user->email,['class'=>'form-control', 'id' => 'email']) !!}
                                    </div>
                                </div>
                       
                           

                                @if(isset($user))
                                    <?php
                                   // $media = $user->getMedia('staff');
                                    $image = URL::asset('assets/download.png') ;
                                    ?>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            {!! Form::label('photo','Photo') !!}
                                            {!! Form::file('photo',['class'=>'form-control', 'id' => 'photo']) !!}
                                        </div>
                                    </div>
                                  
                                @else
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            {!! Form::label('photo','Photo') !!}
                                            {!! Form::file('photo',['class'=>'form-control', 'id' => 'photo']) !!}
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>


                    <div class="panel no-border">
                        <div class="panel-title bg-white no-border">
                            <div class="panel-head">Enter Role of the user</div>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <?php
                                        //$withoutGymie = App\Role::where('name', '!=', 'Gymie')->lists('name', 'id');
                                        $withGymie = App\Role::pluck('name', 'id');
                                        ?>
                                        {!! Form::label('Role') !!}
                                        {!! Form::select('role_id',$withGymie,$user->roleUser->role_id,['class'=>'form-control selectpicker show-tick', 'id' => 'role_id']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-1">
                            <div class="form-group">
                                {!! Form::submit('Update', ['class' => 'btn btn-primary pull-right']) !!}
                            </div>
                        </div>
                    </div>
                    {!! Form::Close() !!}
                </div>
            </div>
        </div>
    </div>
</div>

@stop
