@extends('layouts.app')
@section('title', 'View Users')
@section('content')
 <ol class="breadcrumb">
        <li class="breadcrumb-item">Users</li>
        <h1 class="pull-right">
          <a href="{{ action('AclController@createUser') }}" class="btn btn-primary active pull-right" role="button"> Add</a>
        </h1>
    </ol>

      <div class="container-fluid">
        <div class="animated fadeIn">
             @include('flash::message')
             <div class="row">
                 <div class="col-lg-12">
                     <div class="card">
                         <div class="card-header">
                             <i class="fa fa-align-justify"></i>
        <!-- BEGIN PAGE HEADING -->
      <!--   <div class="page-head bg-grey-100">
            @include('flash::message')
            <h1 class="page-title">Users</h1>
            <a href="{{ action('AclController@createUser') }}" class="btn btn-primary active pull-right" role="button"> Add</a></h1>
        </div> -->

        <div class="container-fluid">
            <!-- Main row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel no-border ">
                        <div class="panel-title bg-white no-border">
                        </div>
                        <div class="panel-body no-padding-top bg-white">
                            <table id="staffs" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th class="text-center">Name</th>
                                    <th class="text-center">Email</th>
                                 <!--    <th class="text-center">Role</th> -->
                                    <th class="text-center">Actions</th>
                                </tr>
                                </thead>
                                <tbody>

                                <tr>
                                    @foreach ($users as $user)
                                        
                                        <td class="text-center">{{ $user->first_name }} {{$user->last_name}}</td>
                                        <td class="text-center">{{ $user->email}}</td>
                                      
                                        <!-- <td class="text-center">
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-info">Actions</button>
                                                <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                    <span class="caret"></span>
                                                    <span class="sr-only">Toggle Dropdown</span>
                                                </button>
                                                <ul class="dropdown-menu" role="menu">
                                                    <li>
                                                        <a href="{{ action('AclController@editUser',['id' => $user->id]) }}">
                                                            Edit details
                                                        </a>
                                                    </li>
                                                    @if(Auth::user()->id != $user->id)
                                                        <li>
                                                            <a href="#" class="delete-record" data-delete-url="{{ url('user/'.$user->id.'/delete') }}"
                                                               data-record-id="{{ $user->id }}">Delete user</a>
                                                        </li>
                                                    @endif
                                                </ul>
                                            </div>
                                        </td> -->
                                        <td class="text-center">
                                            {!! Form::open(['route' => ['user.delete', $user->id], 'method' => 'post']) !!}
                                                <div class='btn-group'>
                                                     <a href="{{ action('AclController@editUser',['id' => $user->id]) }}" class='btn btn-danger btn-xs'><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
</a>&nbsp;&nbsp;
                                                     @if(Auth::user()->id != $user->id)
                                                       {!! Form::button('<i class="fa fa-trash" aria-hidden="true"></i>', [
                                                            'type' => 'submit',
                                                            'class' => 'btn btn-danger btn-xs',
                                                            'onclick' => "return confirm('Are you sure?')"
                                                        ]) !!}
                                                       
                                                    @endif
                                                    
                                                </div>
                                            {!! Form::close() !!}
                                            
                                        </td>
                                </tr>

                                @endforeach


                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>


@stop

@section('footer_script_init')
    <script type="text/javascript">
        $(document).ready(function () {
            gymie.deleterecord();
        });
    </script>
@stop