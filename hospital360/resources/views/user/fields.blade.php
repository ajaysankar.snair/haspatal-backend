<!-- Name Field -->
<div class="row">
<div class="form-group col-sm-6">
     {!! Form::label('first_name','First Name') !!}
     {!! Form::text('first_name',null,['class'=>'form-control', 'id' => 'name','required' => 'required','data-parsley-pattern="^[a-zA-Z]+$"','data-parsley-required-message' => 'First Name is required']) !!}
</div>
<div class="form-group col-sm-6">
     {!! Form::label('last_name','last Name') !!}
     {!! Form::text('last_name',null,['class'=>'form-control', 'id' => 'name','required' => 'required','data-parsley-pattern="^[a-zA-Z]+$"','data-parsley-required-message' => 'Last Name is required']) !!}
</div>

<!-- Email Field -->
</div>
<div class="row">
    <div class="form-group col-sm-6">
        {!! Form::label('email','Email') !!}
        {!! Form::text('email',null,['class'=>'form-control', 'id' => 'email','required' => 'required','data-parsley-type="email"','data-parsley-required-message' => 'Email is required']) !!}
    </div>
    <div class="form-group col-sm-6">
        {!! Form::label('gender', 'Gender:') !!}
        {!! Form::select('gender', ['' => 'Select Gender','1'=>'Male' , '2'=>'Female'], null, ['class' => 'form-control','required' => 'required','data-parsley-required-message' => 'Gender is required']) !!}
    </div>
</div>
<!-- Email Field -->
<!-- <div class="form-group col-sm-6">
     {!! Form::label('status','Status') !!}
                                   
   {!! Form::select('status',array('1' => 'Active', '0' => 'InActive'),null,['class' => 'form-control selectpicker show-tick show-menu-arrow', 'id' => 'status']) !!}
</div> -->

<div class="row">
 @if(isset($user) && $user->photo != "")
                                    
        <div class="form-group col-sm-6">
            {!! Form::label('photo','Photo') !!}
            {!! Form::file('photo',['class'=>'form-control', 'id' => 'photo']) !!}
        </div>
  
    <div class="form-group col-sm-2">
        <img alt="staff photo" class="pull-right"
             src="{{url('/images/100x100/'.constFilePrefix::StaffPhoto . $user->id .'.jpg') }}"/>
    </div>
@else
    <div class="form-group col-sm-6">
        
            {!! Form::label('photo','Photo') !!}
            {!! Form::file('photo',['class'=>'form-control', 'id' => 'photo']) !!}
        
    </div>
@endif

<!-- Password Field -->
<div class="form-group col-sm-6">
    {!! Form::label('password', 'Password') !!}
    {!! Form::password('password', ['class' => 'form-control','required' => 'required','data-parsley-required-message' => 'Password is required']) !!}
</div>
</div>
<div class="row">

<!-- Confirmation Password Field -->
<div class="form-group col-sm-6">
     {!! Form::label('password_confirmation','Confirm Password') !!}
    {!! Form::password('password_confirmation',['class'=>'form-control', 'id' => 'password_confirmation','data-parsley-equalto="#password"','required' => 'required','data-parsley-required-message' => 'Confirm Password is required']) !!}
</div>
<div class="form-group col-sm-6">
    <?php $roles = App\Role::where('id', '!=', '1')->pluck('name', 'id'); ?>
    {!! Form::label('Role') !!}
    {!! Form::select('role_id',$roles,null,['class'=>'form-control selectpicker show-tick show-menu-arrow', 'id' => 'role_id']) !!}
</div>
</div>
<div class="row">
<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('users.index') !!}" class="btn btn-default">Cancel</a>
</div>
</div>
