@extends('layouts.app')
@section('title', 'Add Role')
@section('content')

<section class="content-header">
    <!-- <h1>Enter Details of the role</h1> -->
</section>
    <div class="col-md-12" >
        @include('coreui-templates::common.errors')
        <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <i class="fa fa-plus-square-o fa-lg"></i>
                                <strong>Edit Role</strong>
                            </div>
                            <div class="card-body">
                                 {!! Form::Open(['method' => 'POST','id' => 'rolesform','action' => ['AclController@updateRole',$role->id]]) !!}
                                <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        {!! Form::label('name','Name') !!}
                                        {!! Form::text('name',$role->name,['class'=>'form-control', 'id' => 'name']) !!}
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="form-group">
                                        {!! Form::label('display_name','Display name') !!}
                                        {!! Form::text('display_name',$role->display_name,['class'=>'form-control', 'id' => 'display_name']) !!}
                                    </div>
                                </div>

                                
                            </div>
                            <div class="row">
                               
                                  <div class="col-sm-6">
                                    <div class="form-group">
                                        {!! Form::label('description','Description') !!}
                                        {!! Form::text('description',$role->description,['class'=>'form-control', 'id' => 'description']) !!}
                                    </div>
                                </div>
                               <!-- 
                                <div class="form-group col-sm-12">
                                {!! Form::submit('Create', ['class' => 'btn btn-primary pull-right']) !!}
                                </div> -->
                            </div>
                            </div>
                        </div>

                      
                       <div class="card">
                            <div class="card-header">
                                <i class="fa fa-plus-square-o fa-lg"></i>
                                <strong>Edit Permission</strong>
                            </div>
                            <div class="card-body">
                                @include('coreui-templates::common.errors')
                                <div class="box box-primary">

                                    <div class="box-body">
                                        <div class="col-md-12">
                               
                               @foreach($permissions->groupBy('group_key') as $permission_group)
                                <h5><b>{{$permission_group->pluck('group_key')->pop()}}</b></h5>
                                <div class="row">
                                    @foreach($permission_group as $permission)
                                        <?php $status = ($permission_role->contains('permission_id', $permission->id) ? 'checked="checked"' : '') ?>
                                        <div class="col-sm-3">
                                            <div class="checkbox checkbox-theme">
                                                <input type="checkbox" name="permissions[]" id="permission_{{$permission->id}}"
                                                       value="{{$permission->id}}" {{ $status }} >
                                                <label for="permission_{{$permission->id}}">{{ $permission->display_name }}</label>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            @endforeach
                                </div>
                                    </div>
                                </div>
                            <div class="row">
                                <div class="form-group col-sm-12">
                                    {!! Form::submit('Update', ['class' => 'btn btn-primary pull-right']) !!}
                                </div>
                            </div>
                            </div>

                        </div>
                    </div>
        </div> 
    </div>
    </div>
</div>



        {!! Form::Close() !!}

@stop