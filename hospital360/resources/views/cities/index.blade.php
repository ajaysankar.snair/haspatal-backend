@extends('layouts.app')
@section('title', 'City List')
@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">Cities</li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
             @include('flash::message')
             <div class="row">
                 <div class="col-lg-12">
                     <div class="card">
                         <div class="card-header">
                             <i class="fa fa-align-justify"></i>
                             Cities
                                                     
                             @permission(['admin','add_city'])
                             <a class="pull-right" href="{{ route('cities.create') }}"><i class="fa fa-plus-square fa-lg"></i></a>
                             @endpermission
                             <span class="pull-right">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                            <a class="pull-right" href="{{ route('cityImport') }}"><i class="fa fa-file-o fa-lg"></i></a>
                         </div>
                         <div class="card-body">

                                <table class="table table-striped table-bordered dataTable no-footer" id="dataTableBuilder" width="100%">
                                    <thead>
                                        <tr role="row">
                                            <th>City</th>
                                            <th>Country Name</th>
                                            <th>State Name</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($city as $data)
                                        <tr>
                                            <td>{{$data->city}}</td>
                                            <td>{{$data->country}}</td>
                                            <td>{{$data->state}}</td>
                                            <td>
                                                {!! Form::open(['route' => ['cities.destroy', $data->id], 'method' => 'delete']) !!}
                                                <div class='btn-group'>
                                                    @permission(['admin','show_city'])
                                                    <a href="{{ route('cities.show', $data->id) }}" class='btn btn-ghost-success'>
                                                       <i class="fa fa-eye"></i>
                                                    </a>
                                                    @endpermission
                                                    @permission(['admin','edit_city'])
                                                    <a href="{{ route('cities.edit', $data->id) }}" class='btn btn-ghost-info'>
                                                       <i class="fa fa-edit"></i>
                                                    </a>
                                                    @endpermission
                                                    @permission(['admin','delete_city'])
                                                    {!! Form::button('<i class="fa fa-trash"></i>', [
                                                        'type' => 'submit',
                                                        'class' => 'btn btn-ghost-danger',
                                                        'onclick' => "return confirm('Are you sure?')"
                                                    ]) !!}
                                                    @endpermission
                                                </div>
                                                {!! Form::close() !!}
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                              <div class="pull-right mr-3">
                                     
                              </div>
                         </div>
                     </div>
                  </div>
             </div>
         </div>
    </div>
@endsection

@section('css')
    @include('layouts.datatables_css')
@endsection

@push('scripts')
    @include('layouts.datatables_js')
    <script type="text/javascript">
        $(document).ready( function () {
            $('#dataTableBuilder').DataTable();
        } );
    </script>
@endpush
