@extends('layouts.app')
@section('title', 'Create City')
@section('content')
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
         <a href="{!! route('cities.index') !!}">City</a>
      </li>
      <li class="breadcrumb-item active">Create</li>
    </ol>
     <div class="container-fluid">
          <div class="animated fadeIn">
                @include('coreui-templates::common.errors')
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <i class="fa fa-plus-square-o fa-lg"></i>
                                <strong>Create City</strong>
                            </div>
                            <div class="card-body">
                                {!! Form::open(['route' => 'cities.store','id' => 'myForm']) !!}

                                   @include('cities.fields')

                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
           </div>
    </div>
@endsection
@push('scripts')
    <script type="text/javascript">
    
$('#countryId').on('load change',function(){
   var country = $(this).val();

    if(country){
        $.ajax({
           type:"GET",
           url:"{{url('get-state-list')}}?country_id="+country,
           success:function(res){
            if(res){
                $("#state").empty();
                $("#state").append('<option value="">Select State</option>');
                $.each(res,function(key,value){
                    $("#state").append('<option value="'+key+'">'+value+'</option>');
                });

            }else{
               $("#state").empty();
            }
           }
        });
    }else{
        $("#state").empty();
    }

});
</script>
@endpush
