

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{{ $city->city }}</p>
</div>

<!-- Country Name Field -->
<div class="form-group">
    {!! Form::label('country_name', 'Country:') !!}
    <p>{{ $city->country }}</p>
</div>

<!-- State Name Field -->
<div class="form-group">
    {!! Form::label('state_name', 'State:') !!}
    <p>{{ $city->state }}</p>
</div>

<!-- Status Field -->
<!-- <div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{{ $city->status }}</p>
</div> -->

<!-- Created By Field -->
<!-- <div class="form-group">
    {!! Form::label('created_by', 'Created By:') !!}
    <p>{{ $city->created_by }}</p>
</div> -->

<!-- Updated By Field -->
<!-- <div class="form-group">
    {!! Form::label('updated_by', 'Updated By:') !!}
    <p>{{ $city->updated_by }}</p>
</div> -->

<!-- Created At Field -->
<!-- <div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $city->created_at }}</p>
</div> -->

<!-- Updated At Field -->
<!-- <div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $city->updated_at }}</p>
</div> -->

