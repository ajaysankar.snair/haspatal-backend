<!-- Name Field -->
<div class="row">
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('city', null, ['class' => 'form-control','data-parsley-required-message' =>'City Name is required','required']) !!}
</div>

<!-- Country Name Field -->
<!-- <div class="form-group col-sm-6">
    {!! Form::label('country_name', 'Country Name:') !!}
    {!! Form::select('country_name', [''=>'Select Country'] + $countryList->toArray(), null, ['class' => 'form-control','data-parsley-required-message' =>'Country is required','required']) !!}
</div> -->

<div class="form-group col-sm-6">
    {!! Form::label('country_name', 'Country Name:') !!}
    {!! Form::select('country_name', ['' => 'Select Country'] + $countryList->toArray(), null, ['class' => 'form-control','id' =>'countryId','required','data-parsley-required-message'=>'Country is required' ]) !!}                                    
</div>

</div>
<div class="row">
<!-- State Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('state_name', 'State Name:') !!}
    {!! Form::select('state_name', [''=>'Select State'] + $stateList->toArray(), null, ['class' => 'form-control','data-parsley-required-message' =>'State is required','required']) !!}
</div>

<!-- <div class="form-group col-sm-6">
    {!! Form::label('state_name', 'State:') !!}
    {!! Form::select('state_name', ['Select State' => 'Select State'], null, ['class' => 'form-control','id'=>'state','required','data-parsley-required-message'=>'State is required']) !!}
</div> -->

<!-- Status Field -->
<!-- <div class="form-group col-sm-6">
    {!! Form::label('status', 'Status:') !!}
    {!! Form::text('status', null, ['class' => 'form-control']) !!}
</div> -->
</div>
<!-- Created By Field -->
<!-- <div class="form-group col-sm-6">
    {!! Form::label('created_by', 'Created By:') !!}
    {!! Form::text('created_by', null, ['class' => 'form-control']) !!}
</div> -->

<!-- Updated By Field -->
<!-- <div class="form-group col-sm-6">
    {!! Form::label('updated_by', 'Updated By:') !!}
    {!! Form::text('updated_by', null, ['class' => 'form-control']) !!}
</div> -->

<!-- Submit Field -->
<div class="row">
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('cities.index') }}" class="btn btn-secondary">Cancel</a>
</div>
</div>