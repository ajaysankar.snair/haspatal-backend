<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
 <title>{{ $title }}</title>
</head>
<body>
  <center><img src="{{ $heading }}" style="width: 20%"></center>
  <div style="margin: 8%">
     <table width="100%" style="width:100%;text-align: center" class="table table-striped table-bordered">
      <tr>
        <td>Patient Name</td>
        <td>Doctor Name</td>
        <td>Book Id</td>
        <td>Summary</td>
      </tr>
      <tr>
        <td>{{$my_patient_id}}</td>
        <td>{{$my_doctor_id}}</td>
        <td>{{$m_book_id}}</td>
        <td>{{$my_note}}</td>
      </tr>
	    </table>
	  </div>
</body>
</html>