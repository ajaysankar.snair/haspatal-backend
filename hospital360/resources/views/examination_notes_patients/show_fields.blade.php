<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{{ $examinationNotesPatient->id }}</p>
</div>

<!-- E Patient Id Field -->
<div class="form-group">
    {!! Form::label('e_patient_id', 'E Patient Id:') !!}
    <p>{{ $examinationNotesPatient->e_patient_id }}</p>
</div>

<!-- E Doctor Id Field -->
<div class="form-group">
    {!! Form::label('e_doctor_id', 'E Doctor Id:') !!}
    <p>{{ $examinationNotesPatient->e_doctor_id }}</p>
</div>

<!-- Examination Notes Field -->
<div class="form-group">
    {!! Form::label('examination_notes', 'Examination Notes:') !!}
    <p>{{ $examinationNotesPatient->examination_notes }}</p>
</div>

<!-- Created By Field -->
<div class="form-group">
    {!! Form::label('created_by', 'Created By:') !!}
    <p>{{ $examinationNotesPatient->created_by }}</p>
</div>

<!-- Updated By Field -->
<div class="form-group">
    {!! Form::label('updated_by', 'Updated By:') !!}
    <p>{{ $examinationNotesPatient->updated_by }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $examinationNotesPatient->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $examinationNotesPatient->updated_at }}</p>
</div>

