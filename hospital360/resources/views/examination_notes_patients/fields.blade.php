<!-- E Patient Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('e_patient_id', 'E Patient Id:') !!}
    {!! Form::text('e_patient_id', null, ['class' => 'form-control']) !!}
</div>

<!-- E Doctor Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('e_doctor_id', 'E Doctor Id:') !!}
    {!! Form::text('e_doctor_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Examination Notes Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('examination_notes', 'Examination Notes:') !!}
    {!! Form::textarea('examination_notes', null, ['class' => 'form-control']) !!}
</div>

<!-- Created By Field -->
<div class="form-group col-sm-6">
    {!! Form::label('created_by', 'Created By:') !!}
    {!! Form::text('created_by', null, ['class' => 'form-control']) !!}
</div>

<!-- Updated By Field -->
<div class="form-group col-sm-6">
    {!! Form::label('updated_by', 'Updated By:') !!}
    {!! Form::text('updated_by', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('examinationNotesPatients.index') }}" class="btn btn-secondary">Cancel</a>
</div>
