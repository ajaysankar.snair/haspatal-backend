{!! Form::open(['route' => ['specialities.destroy', $id], 'method' => 'delete']) !!}
<div class='btn-group'>
    @permission(['admin','show_specialities'])
    <a href="{{ route('specialities.show', $id) }}" class='btn btn-ghost-success'>
       <i class="fa fa-eye"></i>
    </a>
    @endpermission
    @permission(['admin','edit_specialities'])
    <a href="{{ route('specialities.edit', $id) }}" class='btn btn-ghost-info'>
       <i class="fa fa-edit"></i>
    </a>
    @endpermission
    @permission(['admin','delete_specialities'])
    {!! Form::button('<i class="fa fa-trash"></i>', [
        'type' => 'submit',
        'class' => 'btn btn-ghost-danger',
        'onclick' => "return confirm('Are you sure?')"
    ]) !!}
    @endpermission
</div>
{!! Form::close() !!}
