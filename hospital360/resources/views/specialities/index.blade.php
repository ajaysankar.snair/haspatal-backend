@extends('layouts.app')
@section('title', 'Specilities List')
@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">Specialities</li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
             @include('flash::message')
             <div class="row">
                 <div class="col-lg-12">
                     <div class="card">
                         <div class="card-header">
                             <i class="fa fa-align-justify"></i>
                             Specialities
                             @permission(['admin','add_specialities'])
                             <a class="pull-right" href="{{ route('specialities.create') }}"><i class="fa fa-plus-square fa-lg"></i></a>
                             @endpermission
                         </div>
                         <div class="card-body">
                             @include('specialities.table')
                              <div class="pull-right mr-3">
                                     
                              </div>
                         </div>
                     </div>
                  </div>
             </div>
         </div>
    </div>
@endsection

