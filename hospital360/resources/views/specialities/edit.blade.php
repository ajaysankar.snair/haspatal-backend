@extends('layouts.app')
@section('title', 'Edit Specilities')
@section('content')
    <ol class="breadcrumb">
          <li class="breadcrumb-item">
             <a href="{!! route('specialities.index') !!}">Specialities</a>
          </li>
          <li class="breadcrumb-item active">Edit</li>
        </ol>
    <div class="container-fluid">
         <div class="animated fadeIn">
             @include('coreui-templates::common.errors')
             <div class="row">
                 <div class="col-lg-12">
                      <div class="card">
                          <div class="card-header">
                              <i class="fa fa-edit fa-lg"></i>
                              <strong>Edit Specialities</strong>
                          </div>
                          <div class="card-body">
                              {!! Form::model($specialities, ['route' => ['specialities.update', $specialities->id], 'method' => 'patch','id' =>"myForm"]) !!}

                              @include('specialities.fields')

                              {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
         </div>
    </div>
@endsection