<!-- Title Field -->
<div class="row">
<div class="form-group col-sm-6">
    {!! Form::label('title', 'Title:') !!}
    {!! Form::text('title', null, ['class' => 'form-control','data-parsley-type="alphanum"','data-parsley-required','required']) !!}
</div>

<!-- Discount Field -->
<div class="form-group col-sm-6">
    {!! Form::label('discount', 'Discount:') !!}
    {!! Form::text('discount', null, ['class' => 'form-control','data-parsley-type="integer"','data-parsley-required','required']) !!}
</div>
</div>
<div class="row">
<!-- Delivery Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('delivery_status', 'Delivery Status:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('delivery_status', 0) !!}
        {!! Form::checkbox('delivery_status', '1', null) !!}
    </label>
</div>
</div>



<div class="row">
<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('offers.index') }}" class="btn btn-secondary">Cancel</a>
</div>
</div>