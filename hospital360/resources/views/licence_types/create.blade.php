@extends('layouts.app')
@section('title', 'Create Licence Type')
@section('content')
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
         <a href="{!! route('licenceTypes.index') !!}">Licence Type</a>
      </li>
      <li class="breadcrumb-item active">Create</li>
    </ol>
     <div class="container-fluid">
          <div class="animated fadeIn">
                @include('coreui-templates::common.errors')
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <i class="fa fa-plus-square-o fa-lg"></i>
                                <strong>Create Licence Type</strong>
                            </div>
                            <div class="card-body">
                                {!! Form::open(['route' => 'licenceTypes.store','id'=>'myForm']) !!}

                                   @include('licence_types.fields')

                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
           </div>
    </div>
@endsection
