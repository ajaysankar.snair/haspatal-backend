@extends('layouts.app')
@section('title', 'View Licence Type')
@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">Licence Types</li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
             @include('flash::message')
             <div class="row">
                 <div class="col-lg-12">
                     <div class="card">
                         <div class="card-header">
                             <i class="fa fa-align-justify"></i>
                             Licence_types
                             @permission(['admin','add_licenceTypes'])
                             <a class="pull-right" href="{{ route('licenceTypes.create') }}"><i class="fa fa-plus-square fa-lg"></i></a>
                             @endpermission
                         </div>
                         <div class="card-body">
                             @include('licence_types.table')
                              <div class="pull-right mr-3">
                                     
                              </div>
                         </div>
                     </div>
                  </div>
             </div>
         </div>
    </div>
@endsection

