@extends('layouts.app')
@section('title', 'Create 360 Plan')
@section('content')
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
         <a href="{!! route('hPlans.index') !!}">360 Plan</a>
      </li>
      <li class="breadcrumb-item active">Create</li>
    </ol>
     <div class="container-fluid">
          <div class="animated fadeIn">
                @include('coreui-templates::common.errors')
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <i class="fa fa-plus-square-o fa-lg"></i>
                                <strong>Create H Plan</strong>
                            </div>
                            <div class="card-body">
                                {!! Form::open(['route' => 'hPlans.store','id' => 'myForm']) !!}

                                   @include('h_plans.fields')

                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
           </div>
    </div>
@endsection
