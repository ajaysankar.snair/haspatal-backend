
<!-- Plan Name Field -->
<div class="form-group">
    {!! Form::label('plan_name', 'Plan Name:') !!}
    <p>{{ $hPlan->plan_name }}</p>
</div>

<!-- Price Field -->
<div class="form-group">
    {!! Form::label('price', 'Price:') !!}
    <p>{{ $hPlan->price }}</p>
</div>

<!-- Limit Field -->
<div class="form-group">
    {!! Form::label('limit', 'Limit:') !!}
    <p>{{ $hPlan->limit }}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{{ $hPlan->description }}</p>
</div>




