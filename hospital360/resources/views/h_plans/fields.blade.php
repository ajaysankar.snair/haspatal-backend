<div class="row">
<!-- Plan Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('plan_name', 'Plan Name:') !!}
    {!! Form::text('plan_name', null, ['class' => 'form-control','data-parsley-pattern="^[a-zA-Z]+$""','data-parsley-required','required']) !!}
</div>

<!-- Price Field -->
<div class="form-group col-sm-6">
    {!! Form::label('price', 'Price:') !!}
    {!! Form::text('price', null, ['class' => 'form-control','data-parsley-type="integer"','data-parsley-required','required']) !!}
</div>
</div>
<div class="row">
<!-- Limit Field -->
<div class="form-group col-sm-6">
    {!! Form::label('limit', 'Limit:') !!}
    {!! Form::text('limit', null, ['class' => 'form-control','data-parsley-type="integer"','data-parsley-required','required']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-6">
    {!! Form::label('description', 'Description:') !!}
    {!! Form::text('description', null, ['class' => 'form-control','data-parsley-type="alphanum"','data-parsley-required','required']) !!}
</div>
</div>
<div class="row">

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('hPlans.index') }}" class="btn btn-secondary">Cancel</a>
</div>
</div>