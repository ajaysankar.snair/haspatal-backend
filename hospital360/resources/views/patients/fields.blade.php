<!-- First Name Field -->
<div class="row">
<div class="form-group col-sm-6">
    {!! Form::label('first_name', 'First Name:') !!}
    {!! Form::text('first_name', null, ['class' => 'form-control','data-parsley-pattern="^[a-z A-Z]+$"','data-parsley-required-message' =>'First Name is required','required']) !!}
</div>

<!-- Last Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('last_name', 'Last Name:') !!}
    {!! Form::text('last_name', null, ['class' => 'form-control','data-parsley-pattern="^[a-z A-Z]+$"','data-parsley-required-message' =>'Last Name is required','required']) !!}
</div>
</div>
<div class="row">
<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::email('email', null, ['class' => 'form-control','data-parsley-type="email"','data-parsley-required-message' =>'Email is required','required']) !!}
</div>

<!-- Mobile Field -->
<div class="form-group col-sm-6">
    {!! Form::label('mobile', 'Mobile:') !!}
    {!! Form::text('mobile', null, ['class' => 'form-control','data-parsley-length="[10,13]"','data-parsley-required-message' =>'Mobile is required','required']) !!}
</div>
</div>
<div class="row">
<!-- Country Field -->
<div class="form-group col-sm-6">
    {!! Form::label('country', 'Country:') !!}
    {!! Form::select('country', [''=>'Select Country'] + $countryList->toArray(), null, ['class' => 'form-control','data-parsley-required-message' =>'Country is required','required']) !!}
</div>

<!-- State Field -->
<div class="form-group col-sm-6">
    {!! Form::label('state', 'State:') !!}
    {!! Form::select('state', [''=>'Select State'] + $stateList->toArray(), null, ['class' => 'form-control','data-parsley-required-message' =>'State is required','required']) !!}
</div>
</div>
<div class="row">
<!-- City Field -->
<div class="form-group col-sm-6">
    {!! Form::label('city', 'City:') !!}
    {!! Form::select('city', [''=>'Select City'] + $cityList->toArray(), null, ['class' => 'form-control','data-parsley-required-message' =>'City is required','required']) !!}
</div>

<!-- Zipcode Field -->
<div class="form-group col-sm-6">
    {!! Form::label('zipcode', 'Zipcode:') !!}
    {!! Form::text('zipcode', null, ['class' => 'form-control','data-parsley-type="integer"','data-parsley-required-message' =>'Zipcode is required','required']) !!}
</div>
</div>
<div class="row">
<!-- Address Field -->
<div class="form-group col-sm-6 col-lg-6">
    {!! Form::label('address', 'Address:') !!}
    {!! Form::textarea('address', null, ['class' => 'form-control','rows' => '3','data-parsley-type="alphanum"','data-parsley-required-message' =>'Address is required','required']) !!}
</div>

<!-- Personal Profile Field -->
<div class="form-group col-sm-6 col-lg-6">
    {!! Form::label('personal_profile', 'Personal Profile:') !!}
    {!! Form::textarea('personal_profile', null, ['class' => 'form-control','rows' =>'3','data-parsley-type="alphanum"','data-parsley-required-message' =>'Personal Profile is required', 'required']) !!}
</div>
</div>
<div class="row">
<!-- Family Profile Field -->
<div class="form-group col-sm-6 col-lg-6">
    {!! Form::label('family_profile', 'Family Profile:') !!}
    {!! Form::textarea('family_profile', null, ['class' => 'form-control','rows' =>'3','data-parsley-type="alphanum"','data-parsley-required-message' =>'Family Profile is required','required']) !!}
</div>

<!-- Social Profile Field -->
<div class="form-group col-sm-6 col-lg-6">
    {!! Form::label('social_profile', 'Social Profile:') !!}
    {!! Form::textarea('social_profile', null, ['class' => 'form-control','rows' =>'3','data-parsley-type="alphanum"','data-parsley-required-message' =>'Social Profile is required','required']) !!}
</div>
</div>
<div class="row">
<!-- Work Profile Field -->
<div class="form-group col-sm-6 col-lg-6">
    {!! Form::label('work_profile', 'Work Profile:') !!}
    {!! Form::textarea('work_profile', null, ['class' => 'form-control','rows' =>'3','data-parsley-type="alphanum"','data-parsley-required-message' =>'Work Profile is required','required']) !!}
</div>

<!-- Contact Phone1 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('contact_phone1', 'Contact Phone1:') !!}
    {!! Form::text('contact_phone1', null, ['class' => 'form-control','data-parsley-length="[0,13]"','data-parsley-type="integer"','data-parsley-required-message' =>'Contact Phone is required','required']) !!}
</div>
</div>
<div class="row">
<!-- Contact Phone2 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('contact_phone2', 'Contact Phone2:') !!}
    {!! Form::text('contact_phone2', null, ['class' => 'form-control']) !!}
</div>

<!-- Contact Phone3 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('contact_phone3', 'Contact Phone3:') !!}
    {!! Form::text('contact_phone3', null, ['class' => 'form-control']) !!}
</div>
</div>
<div class="row">
<!-- Email1 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email1', 'Email1:') !!}
    {!! Form::email('email1', null, ['class' => 'form-control','data-parsley-type="email"','data-parsley-required-message' =>'Email is required','required']) !!}
</div>

<!-- Email2 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email2', 'Email2:') !!}
    {!! Form::email('email2', null, ['class' => 'form-control']) !!}
</div>
</div>
<div class="row">
<!-- Language Field -->
<div class="form-group col-sm-6">
    {!! Form::label('language', 'Language:') !!}
    {!! Form::select('language', [''=>'Select Language'] + $languageList->toArray(), null, ['class' => 'form-control','data-parsley-required-message' =>'Language is required','required']) !!}
</div>

<!-- Profile Pic Field -->
<div class="form-group col-sm-6">
    {!! Form::label('profile_pic', 'Profile Pic:') !!}
    {!! Form::file('profile_pic',['data-parsley-required-message' =>'Profile is required','required']) !!}
</div>
</div>
<div class="row">
<!-- Contact Phone2 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('password', 'Password:') !!}
    {!! Form::password('password', ['class' => 'form-control','id'=>'password','data-parsley-required-message' =>'Password is required','required']) !!}
</div>

<!-- Contact Phone3 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('confirm_password', 'Confirm Password:') !!}
    {!! Form::password('confirm_password', ['class' => 'form-control','data-parsley-equalto'=>'#password','data-parsley-required-message' =>'Confirm Password is required','data-parsley-equalto-message'=>'Confirm password not match with password','required']) !!}
</div>
</div>
<div class="clearfix"></div>

<!-- Status Field -->
<!-- <div class="form-group col-sm-6">
    {!! Form::label('status', 'Status:') !!}
    {!! Form::text('status', null, ['class' => 'form-control']) !!}
</div> -->

<!-- Created By Field -->
<!-- <div class="form-group col-sm-6">
    {!! Form::label('created_by', 'Created By:') !!}
    {!! Form::text('created_by', null, ['class' => 'form-control']) !!}
</div> -->

<!-- Updated By Field -->
<!-- <div class="form-group col-sm-6">
    {!! Form::label('updated_by', 'Updated By:') !!}
    {!! Form::text('updated_by', null, ['class' => 'form-control']) !!}
</div> -->

<!-- Submit Field -->
<div class="row">
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('patients.index') }}" class="btn btn-secondary">Cancel</a>
</div>
</div>