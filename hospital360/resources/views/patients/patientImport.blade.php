@extends('layouts.app')
@section('title', 'Create Patients')
@section('content')
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
         <a href="{!! route('cities.index') !!}">Patients</a>
      </li>
      <li class="breadcrumb-item active">Create</li>
    </ol>
     <div class="container-fluid">
          <div class="animated fadeIn">
                @include('coreui-templates::common.errors')
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <i class="fa fa-plus-square-o fa-lg"></i>
                                <strong>Create Patients</strong>
                            </div>
                            <div class="card-body">
                                {!! Form::open(['route' => 'patientsImportPost','id' => 'myForm','files'=>'true']) !!}
                                    <div class="row">
                                        <div class="form-group col-sm-6">
                                            {!! Form::label('name', 'Patients:') !!}
                                            <br>
                                            {!! Form::file('patient', null, ['class' => 'form-control']) !!}
                                        </div>
                                    
                                    </div>
                                    <!-- Submit Field -->
                                    <div class="row">
                                    <div class="form-group col-sm-12">
                                        {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                                    </div>
                                    </div>

                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
           </div>
    </div>
@endsection
