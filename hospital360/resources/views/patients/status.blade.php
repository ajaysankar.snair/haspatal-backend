@if($patient_login_status_contactinfo != 1 && $patient_login_status_addressinfo != 1)
<span class="badge badge-primary">Pendding (Contact Info)</span>
<span class="badge badge-dark">Pendding (Address Info)</span>
@elseif($patient_login_status_addressinfo != 1)
<span class="badge badge-dark">Pendding (Address Info)</span>
@else
<span class="badge badge-success">Completed</span>
@endif