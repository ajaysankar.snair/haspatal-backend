@extends('layouts.app')
@section('title', 'Patient List')
@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">Patients</li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
             @include('flash::message')
             <div class="row">
                 <div class="col-lg-12">
                     <div class="card">
                         <div class="card-header">
                             <i class="fa fa-align-justify"></i>
                             Patients
                             @permission(['admin','add_patient'])
                             <a class="pull-right" href="{{ route('patients.create') }}"><i class="fa fa-plus-square fa-lg"></i></a>
                             <span class="pull-right">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                            <a class="pull-right" href="{{ url('patientsImport') }}"><i class="fa fa-file-o fa-lg"></i></a>
                             @endpermission
                         </div>
                         <div class="card-body">
                             @include('patients.table')
                              <div class="pull-right mr-3">
                                     
                              </div>
                         </div>
                     </div>
                  </div>
             </div>
         </div>
    </div>
@endsection

