@extends('layouts.app')
@section('title', 'Edit Doctor Non Availability')
@section('content')
    <ol class="breadcrumb">
          <li class="breadcrumb-item">
             <a href="{!! route('doctorAvailabilities.index') !!}">Doctor Non Availability</a>
          </li>
          <li class="breadcrumb-item active">Edit</li>
        </ol>
    <div class="container-fluid">
         <div class="animated fadeIn">
             @include('coreui-templates::common.errors')
             <div class="row">
                 <div class="col-lg-12">
                      <div class="card">
                          <div class="card-header">
                              <i class="fa fa-edit fa-lg"></i>
                              <strong>Edit Doctor Non Availability</strong>
                          </div>
                          <div class="card-body">
                              {!! Form::model($doctorAvailability, ['route' => ['doctorAvailabilities.update', $doctorAvailability->id], 'method' => 'patch']) !!}

                              @include('doctor_availabilities.fields')

                              {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
         </div>
    </div>
@endsection