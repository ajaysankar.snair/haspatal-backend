<div class="row">
<!-- Doctor Id Field -->
<div class="form-group col-sm-4">
    {!! Form::label('doctor_id', 'Doctor Name:') !!}
    <?php if(Auth::user()->role_id == 3)  {?>
        {!! Form::select('doctor_id', $doctorList->toArray(), null, ['class' => 'form-control']) !!}
    <?php }else { ?>
        {!! Form::select('doctor_id', [''=>'Select Doctor'] + $doctorList->toArray(), null, ['class' => 'form-control']) !!}
    <?php } ?>
</div>

<!-- Date Field -->
<div class="form-group col-sm-4">
    {!! Form::label('date', 'Date:') !!}
    {!! Form::text('date', null, ['class' => 'form-control','id'=>'timeSlot']) !!}
    @push('scripts')
   <script type="text/javascript">
            var today=new Date();
           $('#timeSlot').datetimepicker({
               minDate: today,
               format: 'YYYY-MM-DD HH:mm:ss',
               useCurrent: true,
               icons: {
                   up: "icon-arrow-up-circle icons font-2xl",
                   down: "icon-arrow-down-circle icons font-2xl"
               },
               sideBySide: true
           })
       </script>
@endpush
</div>
<!-- <div class="form-group col-sm-4">
    {!! Form::label('date', 'Booking Time In Minute:') !!}
    {!! Form::text('booking_time_slot', null, ['class' => 'form-control']) !!}
    
</div> -->
</div>
<!-- <div class="row">

    <div class="form-group col-sm-4">
        {!! Form::label('from_time', 'Morning Time:') !!}
   
    </div>
    <div class="form-group col-sm-4">
        {!! Form::label('from_time', 'From Time:') !!}
        {!! Form::text('mrg_from_time', null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group col-sm-4">
        {!! Form::label('to_time', 'To Time:') !!}
        {!! Form::text('mrg_to_time', null, ['class' => 'form-control']) !!}
    </div>
<div class="clearfix"></div>

</div>
<div class="row">


    
    <div class="form-group col-sm-4">
        {!! Form::label('from_time', 'Afternoon Time:') !!}
     
    </div>
    <div class="form-group col-sm-4">
        {!! Form::label('from_time', 'From Time:') !!}
        {!! Form::text('aft_from_time', null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group col-sm-4">
        {!! Form::label('to_time', 'To Time:') !!}
        {!! Form::text('aft_to_time', null, ['class' => 'form-control']) !!}
    </div>
<div class="clearfix"></div>

</div>
<div class="row">


 
    <div class="form-group col-sm-4">
        {!! Form::label('from_time', 'Evening Time:') !!}
        
    </div>
    <div class="form-group col-sm-4">
        {!! Form::label('from_time', 'From Time:') !!}
        {!! Form::text('eve_from_time', null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group col-sm-4">
        {!! Form::label('to_time', 'To Time:') !!}
        {!! Form::text('eve_to_time', null, ['class' => 'form-control']) !!}
    </div>
<div class="clearfix"></div>

</div>
    <div class="clearfix"></div> -->
<!-- Created By Field -->
<!-- <div class="form-group col-sm-6">
    {!! Form::label('created_by', 'Created By:') !!}
    {!! Form::text('created_by', null, ['class' => 'form-control']) !!}
</div> -->

<!-- Updated By Field -->
<!-- <div class="form-group col-sm-6">
    {!! Form::label('updated_by', 'Updated By:') !!}
    {!! Form::text('updated_by', null, ['class' => 'form-control']) !!}
</div> -->

<!-- Submit Field -->
<div class="row">
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('doctorAvailabilities.index') }}" class="btn btn-secondary">Cancel</a>
</div>
</div>

@push('scripts')

@endpush