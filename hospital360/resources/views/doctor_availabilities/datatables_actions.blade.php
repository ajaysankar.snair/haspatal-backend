{!! Form::open(['route' => ['doctorAvailabilities.destroy', $id], 'method' => 'delete']) !!}
<div class='btn-group'>
    @permission(['admin','show_doctor_availabilities'])
    <a href="{{ route('doctorAvailabilities.show', $id) }}" class='btn btn-ghost-success'>
       <i class="fa fa-eye"></i>
    </a>
    @endpermission
    @permission(['admin','edit_doctor_availabilities'])
    <a href="{{ route('doctorAvailabilities.edit', $id) }}" class='btn btn-ghost-info'>
       <i class="fa fa-edit"></i>
    </a>
    @endpermission
    @permission(['admin','delete_doctor_availabilities'])
    {!! Form::button('<i class="fa fa-trash"></i>', [
        'type' => 'submit',
        'class' => 'btn btn-ghost-danger',
        'onclick' => "return confirm('Are you sure?')"
    ]) !!}
    @endpermission
</div>
{!! Form::close() !!}
