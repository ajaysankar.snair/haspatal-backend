<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{{ $doctorAvailability->id }}</p>
</div>

<!-- Doctor Id Field -->
<div class="form-group">
    {!! Form::label('doctor_id', 'Doctor Name:') !!}
    <p>{{ $doctorAvailability->first_name }} {{ $doctorAvailability->last_name }}</p>
</div>

<!-- Date Field -->
<div class="form-group">
    {!! Form::label('date', 'Date:') !!}
    <p>{{ $doctorAvailability->date }}</p>
</div>

<!-- Time Slot Field -->
<div class="form-group">
    {!! Form::label('time_slot', 'Time Slot:') !!}
    <p>{{ $doctorAvailability->booking_time_slot }}</p>
</div>

<!-- Status Field -->
<!-- <div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{{ $doctorAvailability->status }}</p>
</div> -->

<!-- Created By Field -->
<!-- <div class="form-group">
    {!! Form::label('created_by', 'Created By:') !!}
    <p>{{ $doctorAvailability->created_by }}</p>
</div> -->

<!-- Updated By Field -->
<!-- <div class="form-group">
    {!! Form::label('updated_by', 'Updated By:') !!}
    <p>{{ $doctorAvailability->updated_by }}</p>
</div> -->

<!-- Created At Field -->
<!-- <div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $doctorAvailability->created_at }}</p>
</div> -->

<!-- Updated At Field -->
<!-- <div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $doctorAvailability->updated_at }}</p>
</div> -->

