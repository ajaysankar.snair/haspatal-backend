@extends('layouts.app')
@section('title', 'Doctor Non Availability View')
@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">Doctor Non Availabilities</li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
             @include('flash::message')
             <div class="row">
                 <div class="col-lg-12">
                     <div class="card">
                         <div class="card-header">
                             <i class="fa fa-align-justify"></i>
                             Doctor Non Availabilities
                              @permission(['admin','add_doctor_availabilities'])
                             <a class="pull-right" href="{{ route('doctorAvailabilities.create') }}"><i class="fa fa-plus-square fa-lg"></i></a>
                             @endpermission
                         </div>
                         <div class="card-body">
                             @include('doctor_availabilities.table')
                              <div class="pull-right mr-3">
                                     
                              </div>
                         </div>
                     </div>
                  </div>
             </div>
         </div>
    </div>
@endsection

