<!-- St Patient Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('st_patient_id', 'St Patient Id:') !!}
    {!! Form::text('st_patient_id', null, ['class' => 'form-control']) !!}
</div>

<!-- St Doctor Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('st_doctor_id', 'St Doctor Id:') !!}
    {!! Form::text('st_doctor_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Suggested Therapies Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('suggested_therapies', 'Suggested Therapies:') !!}
    {!! Form::textarea('suggested_therapies', null, ['class' => 'form-control']) !!}
</div>

<!-- Created By Field -->
<div class="form-group col-sm-6">
    {!! Form::label('created_by', 'Created By:') !!}
    {!! Form::text('created_by', null, ['class' => 'form-control']) !!}
</div>

<!-- Updated By Field -->
<div class="form-group col-sm-6">
    {!! Form::label('updated_by', 'Updated By:') !!}
    {!! Form::text('updated_by', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('suggestedTherapiesPatients.index') }}" class="btn btn-secondary">Cancel</a>
</div>
