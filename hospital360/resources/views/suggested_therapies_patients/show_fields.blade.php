<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{{ $suggestedTherapiesPatient->id }}</p>
</div>

<!-- St Patient Id Field -->
<div class="form-group">
    {!! Form::label('st_patient_id', 'St Patient Id:') !!}
    <p>{{ $suggestedTherapiesPatient->st_patient_id }}</p>
</div>

<!-- St Doctor Id Field -->
<div class="form-group">
    {!! Form::label('st_doctor_id', 'St Doctor Id:') !!}
    <p>{{ $suggestedTherapiesPatient->st_doctor_id }}</p>
</div>

<!-- Suggested Therapies Field -->
<div class="form-group">
    {!! Form::label('suggested_therapies', 'Suggested Therapies:') !!}
    <p>{{ $suggestedTherapiesPatient->suggested_therapies }}</p>
</div>

<!-- Created By Field -->
<div class="form-group">
    {!! Form::label('created_by', 'Created By:') !!}
    <p>{{ $suggestedTherapiesPatient->created_by }}</p>
</div>

<!-- Updated By Field -->
<div class="form-group">
    {!! Form::label('updated_by', 'Updated By:') !!}
    <p>{{ $suggestedTherapiesPatient->updated_by }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $suggestedTherapiesPatient->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $suggestedTherapiesPatient->updated_at }}</p>
</div>

