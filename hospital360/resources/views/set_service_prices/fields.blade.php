<div class="row">
<!-- Service Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('service_name', 'Service Name:') !!}
    {!! Form::select('service_name', [''=>'Select Service'] + $serviceList->toArray(), null, ['class' => 'form-control','data-parsley-required-message' =>'Service is required','required']) !!}
</div>

<!-- Price Field -->
<div class="form-group col-sm-6">
    {!! Form::label('price', 'Price:') !!}
    {!! Form::text('price', null, ['class' => 'form-control','data-parsley-type="integer"','data-parsley-required-message' =>'Price is required','required']) !!}
</div>
</div>
<!-- Status Field -->
<!-- <div class="form-group col-sm-6">
    {!! Form::label('status', 'Status:') !!}
    {!! Form::text('status', null, ['class' => 'form-control']) !!}
</div> -->

<!-- Created By Field -->
<!-- <div class="form-group col-sm-6">
    {!! Form::label('created_by', 'Created By:') !!}
    {!! Form::text('created_by', null, ['class' => 'form-control']) !!}
</div> -->

<!-- Updated By Field -->
<!-- <div class="form-group col-sm-6">
    {!! Form::label('updated_by', 'Updated By:') !!}
    {!! Form::text('updated_by', null, ['class' => 'form-control']) !!}
</div> -->

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('setServicePrices.index') }}" class="btn btn-secondary">Cancel</a>
</div>
