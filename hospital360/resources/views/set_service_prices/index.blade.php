@extends('layouts.app')
@section('title', 'List Service Price');
@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">Set Service Prices</li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
             @include('flash::message')
             <div class="row">
                 <div class="col-lg-12">
                     <div class="card">
                         <div class="card-header">
                             <i class="fa fa-align-justify"></i>
                             Set_service_prices
                                @permission(['admin','add_service_price'])
                             <a class="pull-right" href="{{ route('setServicePrices.create') }}"><i class="fa fa-plus-square fa-lg"></i></a>
                             @endpermission
                         </div>
                         <div class="card-body">
                             @include('set_service_prices.table')
                              <div class="pull-right mr-3">
                                     
                              </div>
                         </div>
                     </div>
                  </div>
             </div>
         </div>
    </div>
@endsection

