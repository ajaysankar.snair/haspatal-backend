

<!-- First Name Field -->
<div class="form-group">
    {!! Form::label('first_name', 'First Name:') !!}
    <p>{{ $doctors->first_name }}</p>
</div>

<!-- Last Name Field -->
<div class="form-group">
    {!! Form::label('last_name', 'Last Name:') !!}
    <p>{{ $doctors->last_name }}</p>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', 'Email:') !!}
    <p>{{ $doctors->email }}</p>
</div>

<!-- Bio Field -->
<div class="form-group">
    {!! Form::label('bio', 'Bio:') !!}
    <p>{{ $doctors->bio }}</p>
</div>

<!-- Pincode Field -->
<div class="form-group">
    {!! Form::label('pincode', 'Pincode:') !!}
    <p>{{ $doctors->pincode }}</p>
</div>

<!-- State Field -->
<div class="form-group">
    {!! Form::label('state', 'State:') !!}
    <p>{{ $doctors->state }}</p>
</div>

<!-- City Field -->
<div class="form-group">
    {!! Form::label('city', 'City:') !!}
    <p>{{ $doctors->city }}</p>
</div>

<!-- Address Field -->
<div class="form-group">
    {!! Form::label('address', 'Address:') !!}
    <p>{{ $doctors->address }}</p>
</div>

<!-- Main Mobile Field -->
<div class="form-group">
    {!! Form::label('main_mobile', 'Main Mobile:') !!}
    <p>{{ $doctors->main_mobile }}</p>
</div>

<!-- Second Mobile Field -->
<div class="form-group">
    {!! Form::label('second_mobile', 'Second Mobile:') !!}
    <p>{{ $doctors->second_mobile }}</p>
</div>

<!-- Clinic Team Leader Field -->
<div class="form-group">
    {!! Form::label('clinic_team_leader', 'Clinic Team Leader:') !!}
    <p>{{ $doctors->clinic_team_leader }}</p>
</div>

<!-- Experience Field -->
<div class="form-group">
    {!! Form::label('experience', 'Experience:') !!}
    <p>{{ $doctors->experience }}</p>
</div>

<!-- Language Field -->
<div class="form-group">
    {!! Form::label('language', 'Language:') !!}
    <p>
        {{$doctors->language}}
    </p>
</div>

<!-- Speciality Field -->
<div class="form-group">
    {!! Form::label('speciality', 'Speciality:') !!}
    <p>{{ $doctors->specility }}</p>
</div>

<!-- Licence Type Field -->
<div class="form-group">
    <?php $licence = App\Models\Licence_type::where('id',$doctors->licence_type)->first(); ?>
    {!! Form::label('licence_type', 'Licence Type:') !!}
    <p>@if(isset($licence->licence_name))
        
        {{ $licence->licence_name }}
        @else
        @endif
    </p>
</div>

<!-- Licence No Field -->
<div class="form-group">
    {!! Form::label('licence_no', 'Licence No:') !!}
    <p>{{ $doctors->licence_no }}</p>
</div>

<!-- Valide Upto Field -->
<div class="form-group">
    {!! Form::label('valide_upto', 'Valide Upto:') !!}
    <p>{{ $doctors->valide_upto }}</p>
</div>

<!-- Issued By Field -->
<div class="form-group">
    {!! Form::label('issued_by', 'Issued By:') !!}
    <p>{{ $doctors->issued_by }}</p>
</div>

<!-- Licence Copy Field -->
<div class="form-group">
     <?php $licenceImg = URL::asset('public/media/licence_copy/'.$doctors->licence_copy); ?>
    {!! Form::label('licence_copy', 'Licence Copy:') !!}

    <?php 
        if (!empty($doctors->licence_copy)){
    ?>
            <p><img src = "<?php echo $licenceImg; ?>" style="height:150px;width: 150px;" ></p>
    <?php
        }else{
    ?>
            <p><img src = "{{url('public/image/default.png')}}" style="height:150px;width: 150px;" ></p>
    <?php
        }
    ?>
</div>

<!-- Profile Pic Field -->
<div class="form-group">
     <?php $profile = URL::asset('public/media/profile_pic/'.$doctors->profile_pic); ?>
    {!! Form::label('profile_pic', 'Profile Pic:') !!}
    <?php 
        if (!empty($doctors->profile_pic)){
    ?>
            <p><img src = "<?php echo $profile; ?>" style="height:150px;width: 150px;" ></p>
    <?php
        }else{
    ?>
            <p><img src = "{{url('public/image/default.png')}}" style="height:150px;width: 150px;" ></p>
    <?php
        }
    ?>
        
</div>

<!-- Status Field -->
<!-- <div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{{ $doctors->status }}</p>
</div> -->

<!-- Created By Field -->
<!-- <div class="form-group">
    {!! Form::label('created_by', 'Created By:') !!}
    <p>{{ $doctors->created_by }}</p>
</div> -->

<!-- Updated By Field -->
<!-- <div class="form-group">
    {!! Form::label('updated_by', 'Updated By:') !!}
    <p>{{ $doctors->updated_by }}</p>
</div> -->

<!-- Created At Field -->
<!-- <div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $doctors->created_at }}</p>
</div> -->

<!-- Updated At Field -->
<!-- <div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $doctors->updated_at }}</p>
</div> -->

