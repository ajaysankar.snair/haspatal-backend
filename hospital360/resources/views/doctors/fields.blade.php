<!-- First Name Field -->
<div class="row">
<div class="form-group col-sm-6">
    {!! Form::label('first_name', 'First Name:') !!}
    {!! Form::text('first_name', null, ['class' => 'form-control','data-parsley-pattern="^[a-z A-Z]+$"','data-parsley-required-message' =>'First Name is required','required']) !!}
</div>

<!-- Last Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('last_name', 'Last Name:') !!}
    {!! Form::text('last_name', null, ['class' => 'form-control','data-parsley-pattern="^[a-z A-Z]+$"','data-parsley-required-message' =>'Last Name is required','required']) !!}
</div>
</div>
<div class="row">
<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::email('email', null, ['class' => 'form-control','data-parsley-type="email"','data-parsley-required-message' =>'Email is required','required']) !!}
</div>

<!-- Bio Field -->
<div class="form-group col-sm-6 col-lg-6">
    {!! Form::label('bio', 'Bio:') !!}
    {!! Form::textarea('bio', null, ['class' => 'form-control','rows' => '3','data-parsley-required-message' =>'Bio is required','required']) !!}
</div>
</div>
<div class="row">
<!-- Pincode Field -->
<div class="form-group col-sm-6">
    {!! Form::label('pincode', 'Pincode:') !!}
    {!! Form::text('pincode', null, ['class' => 'form-control','data-parsley-type="integer"','data-parsley-length="[6,6]"','data-parsley-maxlength="6"','data-parsley-required-message' =>'Pincode is required','required']) !!}
</div>

<!-- State Field -->
<div class="form-group col-sm-6">
    {!! Form::label('state', 'State:') !!}
    {!! Form::select('state', [''=>'Select State'] + $stateList->toArray(), null, ['class' => 'form-control','data-parsley-required-message' =>'State is required','required']) !!}
</div>
</div>
<div class="row">
<!-- City Field -->
<div class="form-group col-sm-6">
    {!! Form::label('city', 'City:') !!}
    {!! Form::select('city', [''=>'Select City'] + $cityList->toArray(), null, ['class' => 'form-control','data-parsley-required-message' =>'City is required','required']) !!}
</div>

<!-- Address Field -->
<div class="form-group col-sm-6 col-lg-6">
    {!! Form::label('address', 'Address:') !!}
    {!! Form::textarea('address', null, ['class' => 'form-control','rows' => '3','data-parsley-required-message' =>'Address is required','required']) !!}
</div>
</div>
<div class="row">
<!-- Main Mobile Field -->
<div class="form-group col-sm-6">
    {!! Form::label('main_mobile', 'Main Mobile:') !!}
    {!! Form::text('main_mobile', null, ['class' => 'form-control','data-parsley-type="number"','data-parsley-length="[10,13]"','data-parsley-maxlength="13"','data-parsley-required-message' =>'Main Mobile is required','required']) !!}
</div>

<!-- Second Mobile Field -->
<div class="form-group col-sm-6">
    {!! Form::label('second_mobile', 'Second Mobile:') !!}
    {!! Form::text('second_mobile', null, ['class' => 'form-control']) !!}
</div>
</div>
<div class="row">
<!-- Clinic Team Leader Field -->
<div class="form-group col-sm-6">
    {!! Form::label('clinic_team_leader', 'Clinic Team Leader:') !!}
    {!! Form::text('clinic_team_leader', null, ['class' => 'form-control','data-parsley-pattern="^[a-zA-Z]+$"','data-parsley-required-message' =>'Clinic TL Name is required','required']) !!}
</div>

<!-- Experience Field -->
<div class="form-group col-sm-6">
    {!! Form::label('experience', 'Experience:') !!}
    {!! Form::text('experience', null, ['class' => 'form-control','data-parsley-type="integer"','data-parsley-required-message' =>'Experience is required', 'required']) !!}
</div>
</div>
<div class="row">
<!-- Language Field -->
@if(isset($doctors))
<!-- Package Category Field -->
<div class="form-group col-sm-6">
    <?php $package_category = App\Models\Language::orderBy('language','asc')->get();    

     ?>
    {!! Form::label('language', 'Language:') !!}

    <select class="form-control" id="language" name="language[]" multiple="multiple">
        
        <?php  $p_category = explode(',', $doctors->language) ?>
        @foreach($package_category as $value)
            <option value="{{$value->id}}" @foreach($p_category as $p) @if($value->id == $p)selected="selected"@endif @endforeach>{{$value->language}}</option>
        @endforeach
    </select>
</div>
@else
<!-- Package Category Field -->
<div class="form-group col-sm-6">
    <?php $cat_id = App\Models\Language::orderBy('language','asc')->pluck('language','id');    
     ?>
    {!! Form::label('language', 'Language:') !!}
    {!! Form::select('language[]', ['Select Language' => 'Select Language']+ $cat_id->toArray(), null, ['class' => 'form-control','multiple'=>true,'data-parsley-required-message' =>'Language is required','required']) !!}
</div>
@endif


<!-- Speciality Field -->
<div class="form-group col-sm-6">
    {!! Form::label('speciality', 'Speciality:') !!}
    {!! Form::select('speciality', [''=>'Select Specilities'] + $specilities->toArray(), null, ['class' => 'form-control','data-parsley-required-message' =>'Speciality is required','required']) !!}
</div>
</div>
<div class="row">
<!-- Licence Type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('licence_type', 'Licence Type:') !!}
    {!! Form::select('licence_type', [''=>'Select Licence Type'] + $licenceTyes->toArray(), null, ['class' => 'form-control','data-parsley-required-message' =>'Licence Type is required','required']) !!}
    
</div>

<!-- Licence No Field -->
<div class="form-group col-sm-6">
    {!! Form::label('licence_no', 'Licence No:') !!}
    {!! Form::text('licence_no', null, ['class' => 'form-control','data-parsley-type="alphanum"','data-parsley-required-message' =>'Licence No is required','required']) !!}
</div>
</div>
<div class="row">
<!-- Valide Upto Field -->
<div class="form-group col-sm-6">
    {!! Form::label('valide_upto', 'Valide Upto:') !!}
    {!! Form::text('valide_upto', null, ['class' => 'form-control','id'=>'valide_upto','data-parsley-required-message' =>'Valid Upto is required', 'required']) !!}
</div>

@push('scripts')
   <script type="text/javascript">
           $('#valide_upto').datetimepicker({
               format: 'YYYY-MM-DD',
               useCurrent: true,
               icons: {
                   up: "icon-arrow-up-circle icons font-2xl",
                   down: "icon-arrow-down-circle icons font-2xl"
               },
               sideBySide: true
           })
       </script>
@endpush


<!-- Issued By Field -->
<div class="form-group col-sm-6">
    {!! Form::label('issued_by', 'Issued By:') !!}
    {!! Form::text('issued_by', null, ['class' => 'form-control','data-parsley-required-message' =>'Issued By is required', 'required']) !!}
</div>
</div>
<div class="row">
<!-- Licence Copy Field -->
<!-- <div class="form-group col-sm-6">
    {!! Form::label('licence_copy', 'Licence Copy:') !!}
    {!! Form::file('licence_copy',['data-parsley-required-message' =>'Licence Copy is required']) !!}
</div> -->

@if(isset($doctors))
<!-- Sn Image Field -->
<div class="form-group col-sm-3">
    {!! Form::label('licence_copy', 'Licence Copy:') !!}
    {!! Form::file('licence_copy') !!}
</div>
<div class="form-group col-sm-3">
    <?php if ($doctors->licence_copy) {
        $img = url('public/media/licence_copy/').'/'.$doctors->licence_copy; 
    }else{
        $img = url('public/default-image.jpg'); 
    }?>
    <p><img src="{{$img}}" style="width: 30%"></p>
</div>
@else
<div class="form-group col-sm-6">
    {!! Form::label('licence_copy', 'Licence Copy:') !!}
    {!! Form::file('licence_copy',['class' => 'form-control','data-parsley-required-message' =>'Licence Copy is required', 'required']) !!}
</div>
@endif


<div class="clearfix"></div>

<!-- Profile Pic Field -->
<!-- <div class="form-group col-sm-6">
    {!! Form::label('profile_pic', 'Profile Pic:') !!}
    {!! Form::file('profile_pic',['data-parsley-required-message' =>'Profile Pic is required']) !!}
</div> -->
@if(isset($doctors))
<!-- Sn Image Field -->
<div class="form-group col-sm-3">
    {!! Form::label('profile_pic', 'Profile Pic:') !!}
    {!! Form::file('profile_pic') !!}
</div>
<div class="form-group col-sm-3">
    <?php if ($doctors->profile_pic) {
        $img = url('public/media/profile_pic/').'/'.$doctors->profile_pic; 
    }else{
        $img = url('public/default-image.jpg'); 
    }?>
    <p><img src="{{$img}}" style="width: 30%"></p>
</div>
@else
<div class="form-group col-sm-6">
    {!! Form::label('profile_pic', 'Profile Pic:') !!}
    {!! Form::file('profile_pic',['class' => 'form-control','data-parsley-required-message' =>'Profile Pic is required', 'required']) !!}
</div>
@endif
</div>
<div class="row">
    <div class="form-group col-sm-6">
    {!! Form::label('price', 'Price:') !!}
    {!! Form::text('price', null, ['class' => 'form-control','data-parsley-type="integer"','data-parsley-required-message' =>'Price is required', 'required']) !!}
</div>
<?php  $plan= DB::table('plan_master')->pluck('pl_name','pl_id'); ?>
<div class="form-group col-sm-6">
    {!! Form::label('plan_id', 'Select Plan:') !!}
    {!! Form::select('plan', [''=>'Select Plan'] + $plan->toArray(), null, ['class' => 'form-control','data-parsley-required-message' =>'Plan is required','required']) !!}
    
</div>  
</div>
@if(isset($doctors))
@else
<div class="row">
<!-- Main Mobile Field -->
<div class="form-group col-sm-6">
    {!! Form::label('password', 'Password:') !!}
    {!! Form::password('password', ['class' => 'form-control','id' => 'password','data-parsley-required-message' =>'Password is required','required']) !!}
</div>

<!-- Second Mobile Field -->
<div class="form-group col-sm-6">
    {!! Form::label('confirm_password', 'Confirm Password:') !!}
    {!! Form::password('confirm_password', ['class' => 'form-control','data-parsley-equalto="#password"','data-parsley-required-message' =>'Confirm Password is required','data-parsley-equalto-message'=>'Confirm password not match with password','required']) !!}
</div>
</div>
@endif

<div class="clearfix"></div>

<!-- Status Field -->
<!-- <div class="form-group col-sm-6">
    {!! Form::label('status', 'Status:') !!}
    {!! Form::text('status', null, ['class' => 'form-control']) !!}
</div> -->

<!-- Created By Field -->
<!-- <div class="form-group col-sm-6">
    {!! Form::label('created_by', 'Created By:') !!}
    {!! Form::text('created_by', null, ['class' => 'form-control']) !!}
</div>
 -->
<!-- Updated By Field -->
<!-- <div class="form-group col-sm-6">
    {!! Form::label('updated_by', 'Updated By:') !!}
    {!! Form::text('updated_by', null, ['class' => 'form-control']) !!}
</div> -->
<div class="row">
<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('doctors.index') }}" class="btn btn-secondary">Cancel</a>
</div>
</div>