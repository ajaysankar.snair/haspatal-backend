<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <title></title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <!-- <link rel="stylesheet" href="{{ public_path('pdf.css') }}"> -->
  
  <style>
    .override_margin{
        padding: 1px !important;
        margin: 0px !important;
    }
  * {
    box-sizing: border-box;
  }

  /* Create two equal columns that floats next to each other */
  .column {
    float: left;
    width: 50%;
    padding: 10px;
    height: 300px; /* Should be removed. Only for demonstration */
  }

  .column1 {
    float: left;
    width: 50%;
    padding: 10px;
    height: 100px; /* Should be removed. Only for demonstration */
  }

  /* Clear floats after the columns */
  .row:after {
    content: "";
    display: table;
    clear: both;
  }
  .height-100{
    min-height: 20px !important;
    max-height: 20px !important;
  }
</style>
</head>
<body style="padding: 1%">
  <div class="container" style="width: 100%;padding-right: 15px;padding-left: 15px;margin-right: auto;margin-left: auto;">
    
    <div class="row" style="background-color: lightgray;padding: 2%; -ms-flex-wrap: wrap;flex-wrap: wrap; margin-right: -15px;margin-left: -15px;">
      <div class="col-sm-6" style="-ms-flex: 0 0 50%;flex: 0 0 50%;max-width: 50%;position: relative;width: 100%;padding-right: 15px;padding-left: 15px;">
        <h3>{{$doctor->clinic_buisness_name}}</h3>
        <span>{{$doctor->bio}}</span><br>
        <span>Registration number</span><br>
        <span>{{$doctor->address}}</span><br>
        <span>{{$doctor->email}} / {{$doctor->main_mobile}}</span><br>
      </div>
    </div>
    <div style="height: 10px"></div>

    <div class="row">
      <div class="column1" >
        <div class="col-sm-5">
            <label style="position: relative;width: 100%;padding-right: 15px;padding-left: 15px;-ms-flex: 0 0 33.333333%;flex: 0 0 33.333333%;max-width: 33.333333%;padding-top: calc(0.375rem + 1px);padding-bottom: calc(0.375rem + 1px);margin-bottom: 0;font-size: inherit;line-height: 1.5;">Date of consultation</label>
            <label style="position: relative;width: 50%;padding-right: 15px;padding-left: 15px;-ms-flex: 0 0 33.333333%; flex: 0 0 33.333333%;max-width: 33.333333%;padding-top: calc(0.375rem + 1px);padding-bottom: calc(0.375rem + 1px);margin-bottom: 0;font-size: inherit;line-height: 1.5;">{{date('d-m-Y h:i',strtotime($booking_details->date_time))}}</label>

              <br>

            <label style="position: relative;width: 100%;padding-right: 15px;padding-left: 15px;-ms-flex: 0 0 33.333333%;flex: 0 0 33.333333%;max-width: 33.333333%;padding-top: calc(0.375rem + 1px);padding-bottom: calc(0.375rem + 1px);margin-bottom: 0;font-size: inherit;line-height: 1.5;">Name Of patient</label>
            
            <label style="position: relative;width: 50%;padding-right: 15px;padding-left: 15px;-ms-flex: 0 0 33.333333%; flex: 0 0 33.333333%;max-width: 33.333333%;padding-top: calc(0.375rem + 1px);padding-bottom: calc(0.375rem + 1px);margin-bottom: 0;font-size: inherit;line-height: 1.5;">{{$patient->first_name}}</label>

                <br>
                <label style="position: relative;width: 50%;padding-right: 15px;padding-left: 15px;-ms-flex: 0 0 33.333333%; flex: 0 0 33.333333%;max-width: 33.333333%;padding-top: calc(0.375rem + 1px);padding-bottom: calc(0.375rem + 1px);margin-bottom: 0;font-size: inherit;line-height: 1.5;">Address
                </label>
                <label style="position: relative;width: 50%;padding-right: 15px;padding-left: 15px;-ms-flex: 0 0 33.333333%; flex: 0 0 33.333333%;max-width: 33.333333%;padding-top: calc(0.375rem + 1px);padding-bottom: calc(0.375rem + 1px);margin-bottom: 0;font-size: inherit;line-height: 1.5;">{{$patient->address}}</label>


        </div>
      </div>
      <div class="column1">
        <label style="position: relative;width: 100%;padding-right: 15px;padding-left: 15px;-ms-flex: 0 0 33.333333%;flex: 0 0 33.333333%;max-width: 33.333333%;padding-top: calc(0.375rem + 1px);padding-bottom: calc(0.375rem + 1px);margin-bottom: 0;font-size: inherit;line-height: 1.5;">AGE
                    
          </label>
          <label style="position: relative;width: 50%;padding-right: 15px;padding-left: 15px;-ms-flex: 0 0 33.333333%; flex: 0 0 33.333333%;max-width: 33.333333%;padding-top: calc(0.375rem + 1px);padding-bottom: calc(0.375rem + 1px);margin-bottom: 0;font-size: inherit;line-height: 1.5;">{{$patient->age}}</label>
          <br>
          <label style="position: relative;width: 50%;padding-right: 15px;padding-left: 15px;-ms-flex: 0 0 33.333333%; flex: 0 0 33.333333%;max-width: 33.333333%;padding-top: calc(0.375rem + 1px);padding-bottom: calc(0.375rem + 1px);margin-bottom: 0;font-size: inherit;line-height: 1.5;">Height
          </label>
          <label style="position: relative;width: 50%;padding-right: 15px;padding-left: 15px;-ms-flex: 0 0 33.333333%; flex: 0 0 33.333333%;max-width: 33.333333%;padding-top: calc(0.375rem + 1px);padding-bottom: calc(0.375rem + 1px);margin-bottom: 0;font-size: inherit;line-height: 1.5;">{{$patient->height}}</label>
          <br>
           <label style="position: relative;width: 100%;padding-right: 15px;padding-left: 15px;-ms-flex: 0 0 33.333333%;flex: 0 0 33.333333%;max-width: 33.333333%;padding-top: calc(0.375rem + 1px);padding-bottom: calc(0.375rem + 1px);margin-bottom: 0;font-size: inherit;line-height: 1.5;">Weight
                    
            </label>
            <label style="position: relative;width: 50%;padding-right: 15px;padding-left: 15px;-ms-flex: 0 0 33.333333%; flex: 0 0 33.333333%;max-width: 33.333333%;padding-top: calc(0.375rem + 1px);padding-bottom: calc(0.375rem + 1px);margin-bottom: 0;font-size: inherit;line-height: 1.5;">{{$patient->weight}}</label>
            <br>
            <label style="position: relative;width: 100%;padding-right: 15px;padding-left: 15px;-ms-flex: 0 0 33.333333%;flex: 0 0 33.333333%;max-width: 33.333333%;padding-top: calc(0.375rem + 1px);padding-bottom: calc(0.375rem + 1px);margin-bottom: 0;font-size: inherit;line-height: 1.5;">LMP
                    
              </label>
              <label style="position: relative;width: 50%;padding-right: 15px;padding-left: 15px;-ms-flex: 0 0 33.333333%; flex: 0 0 33.333333%;max-width: 33.333333%;padding-top: calc(0.375rem + 1px);padding-bottom: calc(0.375rem + 1px);margin-bottom: 0;font-size: inherit;line-height: 1.5;">{{$patient->lmp}}</label>

      </div>
    </div>
    
   
    <hr>

    <div class="row">
      <div class="column" >
        <div class="col-sm-5">
            <b><label for="invoice_no" class="col-form-label">chief complaints</label></b>
            <div style="min-height: 50px;"></div>
            <b><label>RELEVANT Points From History</label></b>
            <div style="min-height: 50px;"></div>
            <b><label>Examination/Lab Findings</label></b>
            <div style="min-height: 50px;"></div>
            <b><label>Suggested Investigations</label></b>

        </div>
      </div>
      <div class="column">
        <div class="col-sm-6" style="-ms-flex: 0 0 50%;flex: 0 0 50%;max-width: 50%;position: relative;width: 100%;padding-right: 15px;padding-left: 15px;">
          <b><label class="col-form-label">DIAGNOSIS OR PROVISIONAL DIAGNOSIS</label></b>
          
            <br><br><br><br><br>
          

          <label>1. {{strtoupper($m_details->medicine_name)}} ({{strtoupper($m_details->generic_name)}})</label><br><label style="margin-top: -2%; margin-left: 3%; ">{{$m_details->drug_from}},  {{$m_details->drug_strength}}, {{$book->faq_administration}} & {{$book->duration}}</label>
          
        </div>
      </div>
    </div>


    <hr>
    <div class="row" style="padding: 2%;margin: 0px !important;display: -ms-flexbox;display: flex;-ms-flex-wrap: wrap;flex-wrap: wrap; margin-right: -15px;margin-left: -15px;">
        <div class="col-sm-6" style="-ms-flex: 0 0 50%;flex: 0 0 50%;max-width: 50%;position: relative;width: 100%;padding-right: 15px;padding-left: 15px;">
            <b><label for="invoice_no" class="col-sm-6 col-form-label">Special Instructions</label></b>
        </div>

        <div class="col-sm-6" style="text-align: right;-ms-flex: 0 0 50%;flex: 0 0 50%;max-width: 50%;position: relative;width: 100%;padding-right: 15px;padding-left: 15px;">
          <div style="min-height: 100px;"></div>
          <b><label>RMPs Signature & Stamp</label></b>
        </div>
        
        
    </div>
    <hr>
    <b><label style="font-size: xx-small;">Note: This prescription is generated on a teleconsultation.</label></b>
  </div>
</body>
</html>