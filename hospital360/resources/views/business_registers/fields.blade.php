<div class="row">
<!-- B Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('b_id', 'B Id:') !!}
    {!! Form::text('b_id', null, ['class' => 'form-control']) !!}
</div>

<!-- B Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('b_name', 'B Name:') !!}
    {!! Form::text('b_name', null, ['class' => 'form-control']) !!}
</div>
</div>
<div class="row">
<!-- Gst No Field -->
<div class="form-group col-sm-6">
    {!! Form::label('gst_no', 'Gst No:') !!}
    {!! Form::text('gst_no', null, ['class' => 'form-control']) !!}
</div>

<!-- Working Hr Field -->
<div class="form-group col-sm-6">
    {!! Form::label('working_hr', 'Working Hr:') !!}
    {!! Form::text('working_hr', null, ['class' => 'form-control']) !!}
</div>
</div>
<div class="row">
<!-- Address Field -->
<div class="form-group col-sm-6">
    {!! Form::label('address', 'Address:') !!}
    {!! Form::textarea('address', null, ['class' => 'form-control','rows' =>'3']) !!}
</div>

<!-- Country Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('country_id', 'Country:') !!}
    {!! Form::text('country_id', null, ['class' => 'form-control']) !!}
</div>
</div>
<div class="row">
<!-- State Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('state_id', 'State:') !!}
    {!! Form::text('state_id', null, ['class' => 'form-control']) !!}
</div>

<!-- City Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('city_id', 'City:') !!}
    {!! Form::text('city_id', null, ['class' => 'form-control']) !!}
</div>
</div>
<div class="row">
<!-- Pincode Field -->
<div class="form-group col-sm-6">
    {!! Form::label('pincode', 'Pincode:') !!}
    {!! Form::text('pincode', null, ['class' => 'form-control']) !!}
</div>

<!-- B Lic Field -->
<div class="form-group col-sm-6">
    {!! Form::label('b_lic', 'B Lic:') !!}
    {!! Form::file('b_lic') !!}
</div>
<div class="clearfix"></div>
</div>
<div class="row">
<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('businessRegisters.index') }}" class="btn btn-secondary">Cancel</a>
</div>
</div>
