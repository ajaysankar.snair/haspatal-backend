@extends('layouts.app')
@section('title', 'Edit Business Registers')
@section('content')
    <ol class="breadcrumb">
          <li class="breadcrumb-item">
             <a href="{!! route('businessRegisters.index') !!}">Business Register</a>
          </li>
          <li class="breadcrumb-item active">Edit</li>
        </ol>
    <div class="container-fluid">
         <div class="animated fadeIn">
             @include('coreui-templates::common.errors')
             <div class="row">
                 <div class="col-lg-12">
                      <div class="card">
                          <div class="card-header">
                              <i class="fa fa-edit fa-lg"></i>
                              <strong>Edit Business Register</strong>
                          </div>
                          <div class="card-body">
                              {!! Form::model($businessRegister, ['route' => ['businessRegisters.update', $businessRegister->id], 'method' => 'patch', 'files' => true]) !!}

                              @include('business_registers.fields')

                              {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
         </div>
    </div>
@endsection