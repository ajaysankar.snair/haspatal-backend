<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{{ $businessRegister->id }}</p>
</div>

<!-- B Id Field -->
<div class="form-group">
    {!! Form::label('b_id', 'B Id:') !!}
    <p>{{ $businessRegister->b_id }}</p>
</div>

<!-- B Name Field -->
<div class="form-group">
    {!! Form::label('b_name', 'B Name:') !!}
    <p>{{ $businessRegister->b_name }}</p>
</div>

<!-- Gst No Field -->
<div class="form-group">
    {!! Form::label('gst_no', 'Gst No:') !!}
    <p>{{ $businessRegister->gst_no }}</p>
</div>

<!-- Working Hr Field -->
<div class="form-group">
    {!! Form::label('working_hr', 'Working Hr:') !!}
    <p>{{ $businessRegister->working_hr }}</p>
</div>

<!-- Address Field -->
<div class="form-group">
    {!! Form::label('address', 'Address:') !!}
    <p>{{ $businessRegister->address }}</p>
</div>

<!-- Country Id Field -->
<div class="form-group">
    {!! Form::label('country_id', 'Country Id:') !!}
    <p>{{ $businessRegister->country_id }}</p>
</div>

<!-- State Id Field -->
<div class="form-group">
    {!! Form::label('state_id', 'State Id:') !!}
    <p>{{ $businessRegister->state_id }}</p>
</div>

<!-- City Id Field -->
<div class="form-group">
    {!! Form::label('city_id', 'City Id:') !!}
    <p>{{ $businessRegister->city_id }}</p>
</div>

<!-- Pincode Field -->
<div class="form-group">
    {!! Form::label('pincode', 'Pincode:') !!}
    <p>{{ $businessRegister->pincode }}</p>
</div>

<!-- B Lic Field -->
<div class="form-group">
    {!! Form::label('b_lic', 'B Lic:') !!}
    <p>{{ $businessRegister->b_lic }}</p>
</div>

<!-- Created By Field -->
<div class="form-group">
    {!! Form::label('created_by', 'Created By:') !!}
    <p>{{ $businessRegister->created_by }}</p>
</div>

<!-- Updated By Field -->
<div class="form-group">
    {!! Form::label('updated_by', 'Updated By:') !!}
    <p>{{ $businessRegister->updated_by }}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{{ $businessRegister->status }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $businessRegister->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $businessRegister->updated_at }}</p>
</div>

