<!-- Tax Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tax_name', 'Tax Name:') !!}
    {!! Form::text('tax_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Tax Percentage Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tax_percentage', 'Tax Percentage:') !!}
    {!! Form::text('tax_percentage', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('taxes.index') }}" class="btn btn-secondary">Cancel</a>
</div>
