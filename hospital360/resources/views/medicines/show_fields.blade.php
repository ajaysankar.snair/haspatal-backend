<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{{ $medicine->id }}</p>
</div>

<!-- Dr Id Field -->
<div class="form-group">
    {!! Form::label('dr_id', 'Dr Id:') !!}
    <p>{{ $medicine->dr_id }}</p>
</div>

<!-- Generic Name Field -->
<div class="form-group">
    {!! Form::label('generic_name', 'Generic Name:') !!}
    <p>{{ $medicine->generic_name }}</p>
</div>

<!-- Medicine Name Field -->
<div class="form-group">
    {!! Form::label('medicine_name', 'Medicine Name:') !!}
    <p>{{ $medicine->medicine_name }}</p>
</div>

<!-- Drug From Field -->
<div class="form-group">
    {!! Form::label('drug_from', 'Drug From:') !!}
    <p>{{ $medicine->drug_from }}</p>
</div>

<!-- Drug Strength Field -->
<div class="form-group">
    {!! Form::label('drug_strength', 'Drug Strength:') !!}
    <p>{{ $medicine->drug_strength }}</p>
</div>

<!-- Created By Field -->
<div class="form-group">
    {!! Form::label('created_by', 'Created By:') !!}
    <p>{{ $medicine->created_by }}</p>
</div>

<!-- Updated By Field -->
<div class="form-group">
    {!! Form::label('updated_by', 'Updated By:') !!}
    <p>{{ $medicine->updated_by }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $medicine->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $medicine->updated_at }}</p>
</div>

