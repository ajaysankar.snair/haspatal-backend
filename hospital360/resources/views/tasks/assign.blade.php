<?php 

$data = \App\User::where('role_id',18)->get();
?>
<input type="hidden" name="task_id" value="{{$id}}" id="task_id_{{$id}}">

<select name="category_id" class ="form-control" id="assign_id_{{$id}}">
	<option>Select Oprater</option>
	@foreach($data as $cat)
	<option value="{{$cat->id}}" @if($cat->id==$operator_id)selected @endif >{{ $cat->first_name }} {{ $cat->last_name }}</option>
	@endforeach
</select>

<script type="text/javascript">
	$('#assign_id_{{$id}}').change(function() {
		
	    var url = "{{url('/')}}";
	    var oprater_id = $(this).val();
	    var task_id = $("#task_id_{{$id}}").val();
	    $.ajax({
	        type:"GET",
	        url : url+"/taskAssign",
	        data: {oprater_id:oprater_id,task_id:task_id},
            dataType: 'json',
	        async: true,
	        success : function(response) {
	            alert('Task assign successfully!')
	        },
	        error: function() {
	            alert('Error occured');
	        }
	    });
	});
</script>