@extends('layouts.app')
@section('title','Task List')
@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">Tasks</li>
    </ol>

    <div class="container-fluid">
        <div class="animated fadeIn">
             @include('flash::message')
             <div class="row">
                 <div class="col-lg-12">
                     <div class="card">
                         <div class="card-header">
                             <i class="fa fa-align-justify"></i>
                             Tasks
                             <!-- <a class="pull-right" href="{{ route('tasks.create') }}"><i class="fa fa-plus-square fa-lg"></i></a> -->
                         </div>
                         <div class="card-body">

                            <table class="table table-striped table-bordered dataTable no-footer" id="dataTableBuilder">
                                <thead>
                                    <tr>
                                        <th>Patient Name</th>
                                        <th>Email</th>
                                        <th>Mobile</th>
                                        <!-- <th>Issue</th> -->
                                        <th>Patient Status</th>
                                        <th>Register Date</th>
                                        <!-- <th>Status</th> -->
                                    </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($tasks as $task)
                                        <tr>
                                            <td>{{$task->patient_name}}</td>
                                            <td>{{$task->email}}</td>
                                            <td>{{$task->mobile}}</td>
                                            <!-- <td>{{$task->issue}}</td> -->
                                            <td>
                                                @if($task->patient_contactinfo == 1 && $task->patient_addressinfo == 1)
                                                 <span class='badge badge-danger'>Contact Info </span><br>
                                                 <span class='badge badge-danger'>Address Info </span>
                                                @elseif($task->patient_contactinfo == 1)
                                                <span class='badge badge-danger'>Contact Info </span>
                                                @elseif($task->patient_addressinfo == 1)
                                                <span class='badge badge-danger'>Address Info </span>
                                                @else
                                                <span class='badge badge-success'>Completed</span>
                                                @endif
                                            </td>
                                            <td>{{ \Carbon\Carbon::createFromTimeStamp(strtotime($task->register_date))->diffForHumans() }}</td>
                                           <!--  <td>
                                                <input type="hidden" name="task_id" value="1" id="task_id_{{$task->id}}">

                                                <select name="category_id" class="form-control" id="assign_id_{{$task->id}}">
                                                    <option>Select Staus</option>
                                                    <option value="3" @if($task->status==3)selected @endif >Completed</option>
                                                </select>

                                                @push('scripts')
                                                <script type="text/javascript">
                                                    $('#assign_id_{{$task->id}}').change(function() {
                                                        var url = "{{url('/')}}";
                                                        var status = $(this).val();
                                                        var task_id = $("#task_id_{{$task->id}}").val();
                                                        $.ajax({
                                                            type:"GET",
                                                            url : url+"/taskUpdate",
                                                            data: {status:status,task_id:task_id},
                                                            dataType: 'json',
                                                            async: true,
                                                            success : function(response) {
                                                                alert('Task update successfully!')
                                                            },
                                                            error: function() {
                                                                alert('Error occured');
                                                            }
                                                        });
                                                    });
                                                </script>
                                                @endpush
                                            </td> -->
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                             
                              <div class="pull-right mr-3">

                              </div>
                         </div>
                     </div>
                  </div>
             </div>
         </div>
    </div>
@endsection

@section('css')
    @include('layouts.datatables_css')
@endsection
@push('scripts')
    @include('layouts.datatables_js')
    <script type="text/javascript">
        $(document).ready( function () {
            $('#dataTableBuilder').DataTable();
        } );
    </script>
@endpush


