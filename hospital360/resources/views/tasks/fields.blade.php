<!-- Patient Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('patient_id', 'Patient Id:') !!}
    {!! Form::text('patient_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Patient Contactinfo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('patient_contactinfo', 'Patient Contactinfo:') !!}
    {!! Form::text('patient_contactinfo', null, ['class' => 'form-control']) !!}
</div>

<!-- Patient Addressinfo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('patient_addressinfo', 'Patient Addressinfo:') !!}
    {!! Form::text('patient_addressinfo', null, ['class' => 'form-control']) !!}
</div>

<!-- Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('status', 'Status:') !!}
    {!! Form::text('status', null, ['class' => 'form-control']) !!}
</div>

<!-- Register Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('register_date', 'Register Date:') !!}
    {!! Form::text('register_date', null, ['class' => 'form-control','id'=>'register_date']) !!}
</div>

@push('scripts')
   <script type="text/javascript">
           $('#register_date').datetimepicker({
               format: 'YYYY-MM-DD HH:mm:ss',
               useCurrent: true,
               icons: {
                   up: "icon-arrow-up-circle icons font-2xl",
                   down: "icon-arrow-down-circle icons font-2xl"
               },
               sideBySide: true
           })
       </script>
@endpush


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('tasks.index') }}" class="btn btn-secondary">Cancel</a>
</div>
