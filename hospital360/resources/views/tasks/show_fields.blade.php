<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{{ $task->id }}</p>
</div>

<!-- Patient Id Field -->
<div class="form-group">
    {!! Form::label('patient_id', 'Patient Id:') !!}
    <p>{{ $task->patient_id }}</p>
</div>

<!-- Patient Contactinfo Field -->
<div class="form-group">
    {!! Form::label('patient_contactinfo', 'Patient Contactinfo:') !!}
    <p>{{ $task->patient_contactinfo }}</p>
</div>

<!-- Patient Addressinfo Field -->
<div class="form-group">
    {!! Form::label('patient_addressinfo', 'Patient Addressinfo:') !!}
    <p>{{ $task->patient_addressinfo }}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{{ $task->status }}</p>
</div>

<!-- Register Date Field -->
<div class="form-group">
    {!! Form::label('register_date', 'Register Date:') !!}
    <p>{{ $task->register_date }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $task->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $task->updated_at }}</p>
</div>

