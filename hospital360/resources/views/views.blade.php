<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Haspatal Admin Template</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900,900i&display=swap" rel="stylesheet">
    <script src="https://kit.fontawesome.com/fa5e54101c.js" crossorigin="anonymous"></script>

    <!-- Theme Style -->
    <!-- <link rel="stylesheet" href="css/style.css">
     -->
     <style>
     .bg_red{
    background: #db4130;
}
.logo_container{
    height: 100vh;
    padding: 20px;
    display: flex;
    width: 100%;
    justify-content: center;
    align-items: center;
}
.logo_container img{
    display: block;
    width: 250px;
}
.reg_box{
    padding: 30px;
    display: flex;
    justify-content: center;
    align-items: center;
    height: 100vh;
}
.form_container{
    width: 60%;
}
.head_txt{
    text-align: center;
    margin-bottom: 40px;
}
.login_link{
    margin: 50px 0 10px 0;
    text-align: center;
}
.login_link a{
    text-decoration: none;
}
.form-group{
    margin-bottom: 15px;
}
.form-group label{
    margin-bottom: 5px;
}
@media(max-width: 768px){
    .logo_container{
        height: auto;
        padding: 50px 20px;
    }
    .logo_container img{
        width: 180px;
    }
    .reg_box{
        height: auto;
        padding: 50px 30px;
    }
    .form_container{
        width: 80%;
    }
}
header{
    background: #db4130;
}
.logo_head_container{
    padding: 20px 30px;
}
.logo_head_container img{
    display: block;
    width: 180px;
}
.top_bx_ct{
    display: flex;
    padding: 30px;
    justify-content: space-between;
    align-items: center;
}
.top_bx_ct h5{
    margin: 0;
}
.active{
    background: #db4130;
    color: #fff !important;
}
.tabl_container{
    padding: 0 30px;
}
.tbl_contr{
    padding: 30px;
    box-shadow: 0px 0px 20px rgba(92,111,139,0.12);
    border-radius: 10px;
}
th{
    font-size: 14px;
    color: #5a5a5a;
    background: #f5f5f5 !important;
}
td{
    font-size: 13px;
    padding: .3rem .5rem !important;
}
table{
    width: 100%;
}
.acpt{
    background: rgb(15, 153, 73);
    cursor: pointer;
}
.hld{
    background: rgb(15, 157, 737);
    cursor: pointer;
}
.rjct{
    background: rgb(165, 19, 19);
    cursor: pointer;
}
.clr{
    background: rgb(111 111 236);
    cursor: pointer;
}
.dlts{
    background:  rgb(255,0,0);
    cursor: pointer;
}
.awt{
    background: rgb(216 142 8);
    cursor: pointer;
}
.aprc_cl{
    display: block;
    padding: 0px 7px;
    border-radius: 5px;
    background: rgb(15, 153, 73);
    color: #fff;
    width: fit-content;
    font-size: 12px;
}
.rjct_cl{
    display: block;
    padding: 0px 7px;
    border-radius: 5px;
    background: rgb(165, 19, 19);
    color: #fff;
    width: fit-content;
    font-size: 12px;
}
.btn_spcl_btn{
    display: inline-block;
    font-size: 16px;
    color: #db4130;
    padding: 7px;
    width: 200px;
    border: 2px solid #db4130;
    box-shadow: 0 1px 15px 1px rgb(68 68 68 / 15%);
    border-radius: 30px;
    margin-top: 20px;
}
.btn_cont{
    text-align: center;
}
.dropdown-menu.show{
    left: -37px !important;
}
.btn-slct-clr{
    color: #db4130;
    background-color: #ffffff;
    border-color: #db4130;
}
.doc{
    text-decoration: none;
    color: #db4130;
    cursor: pointer;
}
.view{
    text-decoration: none;
    cursor: pointer;
}
.df_blk{
    display: block;
}
.flx-bx{
    display: flex;
    align-items: center;
    margin-bottom: 15px;
}
.fil_lbl{
    width: 230px;
    font-size: 14px;
    color: #656565;
    font-weight: 500;
}
.p_cls{
    width: 100%;
    padding: 10px 15px;
    background: #f5f5f5;
    border-radius: 5px;
    margin:0;
    font-size: 14px;
}
.btn_group_flx{
    margin-bottom: 20px;
    display: flex;
    justify-content: flex-end;
}
.btn_grp_fl_s{
    display: block;
    margin-right: 5px;
    padding: 5px 15px;
    text-decoration: none;
    color: #fff;
    font-size: 12px;
    border-radius: 5px;
}
     </style>
  </head>
  <body>
      <section id="wrx_total">
          <header>
            <div class="logo_head_container">
                <!-- <img src="images/Logo.png"> -->
                <a  href="/allreg">  <img src="{{url('public/images/Logo.png')}}"></a>
            </div>
          </header>

          <div class="row">
              <div class="col-md-12">
                  <div class="top_bx_ct">
                      <h5>Single View</h5>
                      @if($data->admin_status =='1')  
                      <span class="df_blk">Current Status : Approved</span>
                      @elseif($data->admin_status =='2')
                      <span class="df_blk">Current Status : Rejected</span>
                      @else
                      <span class="df_blk">Current Status : Pending</span>
                      @endif
                  </div>
              </div>

              <div class="col-md-12">
                  <div class="tabl_container">
                      <div class="tbl_contr">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="btn_group_flx">
                                <form id="settings-form" action="{{ route('approve') }}" method="post">
                                    <button class="btn_grp_fl_s acpt">Approve</button>
                                    <input type="hidden" name="user_id" value="{{  $data->user_id  }}">
                                    {!! csrf_field() !!}
                                    </form>
                                    <!-- <a class="btn_grp_fl_s rjct" href="#">Reject</a> -->
                                    <form id="settings-form" action="{{ route('reject') }}" method="post">
                                    <button class="btn_grp_fl_s rjct">Reject</button>
                                    <input type="hidden" name="product_id" value="{{  $data->user_id  }}">
                                    {!! csrf_field() !!}
                                    </form>
                                    <form id="settings-form" action="{{ route('hold') }}" method="post">
                                    <button class="btn_grp_fl_s hld">Hold</button>
                                    <input type="hidden" name="product_id" value="{{  $data->user_id  }}">
                                    {!! csrf_field() !!}
                                    </form>
                                    <form id="settings-form" action="{{ route('deleteregister') }}" method="post">
                                    <button class="btn_grp_fl_s dlts">Delete</button>
                                    <input type="hidden" name="product_id" value="{{  $data->user_id  }}">
                                    {!! csrf_field() !!}
                                    </form>
                                    <a class="btn_grp_fl_s clr" href="#">Send Clear Picture</a>
                                    <a class="btn_grp_fl_s awt" style="margin-right:0;" href="#">More Documents Awaited</a>
                                </div>
                            </div>
                            <?php 
                  $temp = explode(' ',$data->created_at);
                ?>
                            <div class="col-md-6">
                                <div class="flx-bx">
                                    <label class="fil_lbl">Reg ID</label>
                                    <p class="p_cls">{{ $data->business_registerID }}</p>
                                </div>
                                <div class="flx-bx">
                                    <label class="fil_lbl">Business Name</label>
                                    <p class="p_cls">{{ $data->b_name }}</p>
                                </div>
                                <div class="flx-bx">
                                    <label class="fil_lbl">Date of Reg</label>
                                    <p class="p_cls">{{ $temp[0] }}</p>
                                </div>
                                <div class="flx-bx">
                                    <label class="fil_lbl">Time of Reg</label>
                                    <p class="p_cls">{{ $temp[1] }}</p>
                                </div>
                                <div class="flx-bx">
                                    <label class="fil_lbl">Business Type</label>
                                    <p class="p_cls">{{ $data->type }}</p>
                                </div>
                                <div class="flx-bx">
                                    <label class="fil_lbl">Email</label>
                                    <p class="p_cls">{{$data->email }}</p>
                                </div>
                                <div class="flx-bx">
                                    <label class="fil_lbl">Phone</label>
                                    <p class="p_cls">{{ $data->mobile }}</p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="flx-bx">
                                    <label class="fil_lbl">Documents</label>
                                    <p class="p_cls"><a href="{{$path['b_lic']}}">License</a></p>
                                    <p class="p_cls"><a href="{{$path['b_card_pic']}}">Card </a></p>
                                    <p class="p_cls"><a href="{{$path['shop_pic']}}">Shop</a></p>
                                </div>
                                <div class="flx-bx">
                                    <label class="fil_lbl">Business Details</label>
                                    <p class="p_cls">
                                        <!-- <span class="df_blk">lorem ipsum dolor sit amet</span>
                                        <span class="df_blk">lorem ipsum dolor sit amet</span>
                                        <span class="df_blk">lorem ipsum dolor sit amet</span> -->
                                        @if($data->description =='undefined')  
                                        <span class="df_blk">Nothing to Display</span>
                                        @else
                                        <span class="df_blk">{{$data->description}}</span>
                                        @endif
                                    </p>
                                </div>
                                <div class="flx-bx">
                                    <label class="fil_lbl">Address</label>
                                    <p class="p_cls">
                                        <!-- <span class="df_blk">lorem ipsum dolor sit amet</span>
                                        <span class="df_blk">lorem ipsum dolor sit amet</span>
                                        <span class="df_blk">lorem ipsum dolor sit amet</span> -->
                                        @if($data->address =='undefined')  
                                        <span class="df_blk">Not Selected</span>
                                        @else
                                        <span class="df_blk">{{$data->address}}</span>
                                        @endif
                                    </p>
                                </div>
                                <div class="flx-bx">
                                    <label class="fil_lbl">Location</label>
                                    <p class="p_cls">                                        
                                        <span class="df_blk">{{$data->cityname}}</span>
                                        <!-- @if($data->district =='undefined')  
                                        <span class="df_blk">Not Selected</span>
                                        @else
                                        <span class="df_blk">{{$data->district}}</span>
                                        @endif -->
                                        <span class="df_blk">{{$data->district_name}}</span>
                                        <span class="df_blk">{{$data->statename}}</span>
                                       
                                     
                                        <span class="df_blk">{{$data->pincode}}</span>
                                    </p>
                                </div>
                            </div>
                        </div>
                      </div>
                  </div>
              </div>
          </div>
      </section>
  </body>
</html>