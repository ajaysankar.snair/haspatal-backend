<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{{ $followupPeriod->id }}</p>
</div>

<!-- Dr Id Field -->
<div class="form-group">
    {!! Form::label('dr_id', 'Dr Id:') !!}
    <p>{{ $followupPeriod->dr_id }}</p>
</div>

<!-- Followup List Field -->
<div class="form-group">
    {!! Form::label('followup_list', 'Followup List:') !!}
    <p>{{ $followupPeriod->followup_list }}</p>
</div>

<!-- Created By Field -->
<div class="form-group">
    {!! Form::label('created_by', 'Created By:') !!}
    <p>{{ $followupPeriod->created_by }}</p>
</div>

<!-- Updated By Field -->
<div class="form-group">
    {!! Form::label('updated_by', 'Updated By:') !!}
    <p>{{ $followupPeriod->updated_by }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $followupPeriod->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $followupPeriod->updated_at }}</p>
</div>

