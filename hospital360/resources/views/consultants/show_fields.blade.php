<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{{ $consultant->id }}</p>
</div>

<!-- C Patient Id Field -->
<div class="form-group">
    {!! Form::label('c_patient_id', 'C Patient Id:') !!}
    <p>{{ $consultant->c_patient_id }}</p>
</div>

<!-- C Doctor Id Field -->
<div class="form-group">
    {!! Form::label('c_doctor_id', 'C Doctor Id:') !!}
    <p>{{ $consultant->c_doctor_id }}</p>
</div>

<!-- C Book Id Field -->
<div class="form-group">
    {!! Form::label('c_book_id', 'C Book Id:') !!}
    <p>{{ $consultant->c_book_id }}</p>
</div>

<!-- Consultant Note Field -->
<div class="form-group">
    {!! Form::label('consultant_note', 'Consultant Note:') !!}
    <p>{{ $consultant->consultant_note }}</p>
</div>

<!-- Created By Field -->
<div class="form-group">
    {!! Form::label('created_by', 'Created By:') !!}
    <p>{{ $consultant->created_by }}</p>
</div>

<!-- Updated By Field -->
<div class="form-group">
    {!! Form::label('updated_by', 'Updated By:') !!}
    <p>{{ $consultant->updated_by }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $consultant->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $consultant->updated_at }}</p>
</div>

