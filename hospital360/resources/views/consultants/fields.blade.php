<!-- C Patient Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('c_patient_id', 'C Patient Id:') !!}
    {!! Form::text('c_patient_id', null, ['class' => 'form-control']) !!}
</div>

<!-- C Doctor Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('c_doctor_id', 'C Doctor Id:') !!}
    {!! Form::text('c_doctor_id', null, ['class' => 'form-control']) !!}
</div>

<!-- C Book Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('c_book_id', 'C Book Id:') !!}
    {!! Form::text('c_book_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Consultant Note Field -->
<div class="form-group col-sm-6">
    {!! Form::label('consultant_note', 'Consultant Note:') !!}
    {!! Form::text('consultant_note', null, ['class' => 'form-control']) !!}
</div>

<!-- Created By Field -->
<div class="form-group col-sm-6">
    {!! Form::label('created_by', 'Created By:') !!}
    {!! Form::text('created_by', null, ['class' => 'form-control']) !!}
</div>

<!-- Updated By Field -->
<div class="form-group col-sm-6">
    {!! Form::label('updated_by', 'Updated By:') !!}
    {!! Form::text('updated_by', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('consultants.index') }}" class="btn btn-secondary">Cancel</a>
</div>
