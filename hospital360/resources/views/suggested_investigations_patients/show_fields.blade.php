<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{{ $suggestedInvestigationsPatient->id }}</p>
</div>

<!-- S Patient Id Field -->
<div class="form-group">
    {!! Form::label('s_patient_id', 'S Patient Id:') !!}
    <p>{{ $suggestedInvestigationsPatient->s_patient_id }}</p>
</div>

<!-- S Doctor Id Field -->
<div class="form-group">
    {!! Form::label('s_doctor_id', 'S Doctor Id:') !!}
    <p>{{ $suggestedInvestigationsPatient->s_doctor_id }}</p>
</div>

<!-- Suggested Investigations Field -->
<div class="form-group">
    {!! Form::label('suggested_investigations', 'Suggested Investigations:') !!}
    <p>{{ $suggestedInvestigationsPatient->suggested_investigations }}</p>
</div>

<!-- Created By Field -->
<div class="form-group">
    {!! Form::label('created_by', 'Created By:') !!}
    <p>{{ $suggestedInvestigationsPatient->created_by }}</p>
</div>

<!-- Updated By Field -->
<div class="form-group">
    {!! Form::label('updated_by', 'Updated By:') !!}
    <p>{{ $suggestedInvestigationsPatient->updated_by }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $suggestedInvestigationsPatient->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $suggestedInvestigationsPatient->updated_at }}</p>
</div>

