<!-- S Patient Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('s_patient_id', 'S Patient Id:') !!}
    {!! Form::text('s_patient_id', null, ['class' => 'form-control']) !!}
</div>

<!-- S Doctor Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('s_doctor_id', 'S Doctor Id:') !!}
    {!! Form::text('s_doctor_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Suggested Investigations Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('suggested_investigations', 'Suggested Investigations:') !!}
    {!! Form::textarea('suggested_investigations', null, ['class' => 'form-control']) !!}
</div>

<!-- Created By Field -->
<div class="form-group col-sm-6">
    {!! Form::label('created_by', 'Created By:') !!}
    {!! Form::text('created_by', null, ['class' => 'form-control']) !!}
</div>

<!-- Updated By Field -->
<div class="form-group col-sm-6">
    {!! Form::label('updated_by', 'Updated By:') !!}
    {!! Form::text('updated_by', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('suggestedInvestigationsPatients.index') }}" class="btn btn-secondary">Cancel</a>
</div>
