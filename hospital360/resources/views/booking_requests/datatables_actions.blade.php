{!! Form::open(['route' => ['bookingRequests.destroy', $id], 'method' => 'delete']) !!}
<div class='btn-group'>
    @permission(['admin','show_booking_requests'])
    <a href="{{ route('bookingRequests.show', $id) }}" class='btn btn-ghost-success'>
       <i class="fa fa-eye"></i>
    </a>
    @endpermission
    @permission(['admin','edit_booking_requests'])
    <a href="{{ route('bookingRequests.edit', $id) }}" class='btn btn-ghost-info'>
       <i class="fa fa-edit"></i>
    </a>
    @endpermission
    @permission(['admin','delete_booking_requests'])
    {!! Form::button('<i class="fa fa-trash"></i>', [
        'type' => 'submit',
        'class' => 'btn btn-ghost-danger',
        'onclick' => "return confirm('Are you sure?')"
    ]) !!}
    @endpermission
</div>
{!! Form::close() !!}
