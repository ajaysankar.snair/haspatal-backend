@extends('layouts.app')
@section('title', 'Edit Appointment')
@section('content')
    <ol class="breadcrumb">
          <li class="breadcrumb-item">
             <a href="{!! route('bookingRequests.index') !!}">Booking Request</a>
          </li>
          <li class="breadcrumb-item active">Edit</li>
        </ol>
    <div class="container-fluid">
         <div class="animated fadeIn">
             @include('coreui-templates::common.errors')
             <div class="row">
                 <div class="col-lg-12">
                      <div class="card">
                          <div class="card-header">
                              <i class="fa fa-edit fa-lg"></i>
                              <strong>Edit Booking Request</strong>
                          </div>
                          <div class="card-body">
                              {!! Form::model($bookingRequest, ['route' => ['bookingRequests.update', $bookingRequest->id], 'method' => 'patch','id' => "myForm"]) !!}

                              @include('booking_requests.fields')

                              {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
         </div>
    </div>
@endsection