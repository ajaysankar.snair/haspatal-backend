<div class="row">
<!-- Patient Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('patient_name', 'Patient Name:') !!}
    {!! Form::select('patient_name', [''=>'Select Patient'] + $patientList->toArray(), null, ['class' => 'form-control','data-parsley-required-message' =>'Patient is required','required']) !!}
</div>

<!-- Doctor Name Field -->
<!-- <div class="form-group col-sm-6">
    {!! Form::label('doctor_name', 'Doctor Name:') !!}
    {!! Form::select('doctor_name', [''=>'Select Doctor'] + $doctorList->toArray(), null, ['class' => 'form-control','data-parsley-required-message' =>'Doctor is required','required']) !!}
</div> -->
<div class="form-group col-sm-4">
    {!! Form::label('doctor_name', 'Doctor Name:') !!}
    <?php if(Auth::user()->role_id == 3)  {?>
        {!! Form::select('doctor_name', $doctorList->toArray(), null, ['class' => 'form-control']) !!}
    <?php }else { ?>
        {!! Form::select('doctor_name', [''=>'Select Doctor'] + $doctorList->toArray(), null, ['class' => 'form-control']) !!}
    <?php } ?>
</div>
</div>
<div class="row">
<!-- Date Time Field -->
<div class="form-group col-sm-6">
    {!! Form::label('date_time', 'Date Time:') !!}
    {!! Form::text('date_time', null, ['class' => 'form-control','id'=>'date_time','data-parsley-required-message' =>'Date Time is required','required']) !!}
</div>

@push('scripts')
   <script type="text/javascript">
          var today = new Date();
           $('#date_time').datetimepicker({
               
               minDate: today,
               format: 'YYYY-MM-DD HH:mm:ss',
               useCurrent: true,
               icons: {
                   up: "icon-arrow-up-circle icons font-2xl",
                   down: "icon-arrow-down-circle icons font-2xl"
               },
               sideBySide: true
           })
           
              
       </script>
@endpush
</div>
<div class="row">
<!-- Payment Status Field -->
<!-- <div class="form-group col-sm-6">
    {!! Form::label('payment_status', 'Payment Status:') !!}
    {!! Form::text('payment_status', null, ['class' => 'form-control']) !!}
</div> -->

<!-- Status Field -->
<!-- <div class="form-group col-sm-6">
    {!! Form::label('status', 'Status:') !!}
    {!! Form::text('status', null, ['class' => 'form-control']) !!}
</div> -->
</div>
<!-- Created By Field -->
<!-- <div class="form-group col-sm-6">
    {!! Form::label('created_by', 'Created By:') !!}
    {!! Form::text('created_by', null, ['class' => 'form-control']) !!}
</div> -->

<!-- Updated By Field -->
<!-- <div class="form-group col-sm-6">
    {!! Form::label('updated_by', 'Updated By:') !!}
    {!! Form::text('updated_by', null, ['class' => 'form-control']) !!}
</div> -->
<div class="row">
<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('bookingRequests.index') }}" class="btn btn-secondary">Cancel</a>
</div>
</div>