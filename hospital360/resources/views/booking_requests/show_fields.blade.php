<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{{ $bookingRequest->id }}</p>
</div>

<!-- Patient Name Field -->
<div class="form-group">
    {!! Form::label('patient_name', 'Patient Name:') !!}
    <p>{{ $bookingRequest->p_first_name }} {{ $bookingRequest->p_last_name }}</p>
</div>

<!-- Doctor Name Field -->
<div class="form-group">
    {!! Form::label('doctor_name', 'Doctor Name:') !!}
    <p>{{ $bookingRequest->first_name }} {{ $bookingRequest->last_name }}</p>
</div>

<!-- Date Time Field -->
<div class="form-group">
    {!! Form::label('date_time', 'Date Time:') !!}
    <p>{{ $bookingRequest->date_time }}</p>
</div>

<!-- Payment Status Field -->
<div class="form-group">
    {!! Form::label('payment_status', 'Payment Status:') !!}
    <p>{{ $bookingRequest->payment_status }}</p>
</div>

<!-- Status Field -->
<!-- <div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{{ $bookingRequest->status }}</p>
</div> -->

<!-- Created By Field -->
<!-- <div class="form-group">
    {!! Form::label('created_by', 'Created By:') !!}
    <p>{{ $bookingRequest->created_by }}</p>
</div> -->

<!-- Updated By Field -->
<!-- <div class="form-group">
    {!! Form::label('updated_by', 'Updated By:') !!}
    <p>{{ $bookingRequest->updated_by }}</p>
</div> -->

<!-- Created At Field -->
<!-- <div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $bookingRequest->created_at }}</p>
</div> -->

<!-- Updated At Field -->
<!-- <div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $bookingRequest->updated_at }}</p>
</div> -->

