<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{{ $referralSpecialities->id }}</p>
</div>

<!-- Dr Id Field -->
<div class="form-group">
    {!! Form::label('dr_id', 'Dr Id:') !!}
    <p>{{ $referralSpecialities->dr_id }}</p>
</div>

<!-- Specialities Name Field -->
<div class="form-group">
    {!! Form::label('specialities_name', 'Specialities Name:') !!}
    <p>{{ $referralSpecialities->specialities_name }}</p>
</div>

<!-- Created By Field -->
<div class="form-group">
    {!! Form::label('created_by', 'Created By:') !!}
    <p>{{ $referralSpecialities->created_by }}</p>
</div>

<!-- Updated By Field -->
<div class="form-group">
    {!! Form::label('updated_by', 'Updated By:') !!}
    <p>{{ $referralSpecialities->updated_by }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $referralSpecialities->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $referralSpecialities->updated_at }}</p>
</div>

