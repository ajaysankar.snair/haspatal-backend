<!-- Dr Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('dr_id', 'Dr Id:') !!}
    {!! Form::text('dr_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Specialities Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('specialities_name', 'Specialities Name:') !!}
    {!! Form::text('specialities_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Created By Field -->
<div class="form-group col-sm-6">
    {!! Form::label('created_by', 'Created By:') !!}
    {!! Form::text('created_by', null, ['class' => 'form-control']) !!}
</div>

<!-- Updated By Field -->
<div class="form-group col-sm-6">
    {!! Form::label('updated_by', 'Updated By:') !!}
    {!! Form::text('updated_by', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('referralSpecialities.index') }}" class="btn btn-secondary">Cancel</a>
</div>
