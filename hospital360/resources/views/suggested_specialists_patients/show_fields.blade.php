<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{{ $suggestedSpecialistsPatient->id }}</p>
</div>

<!-- Ss Patient Id Field -->
<div class="form-group">
    {!! Form::label('ss_patient_id', 'Ss Patient Id:') !!}
    <p>{{ $suggestedSpecialistsPatient->ss_patient_id }}</p>
</div>

<!-- Ss Doctor Id Field -->
<div class="form-group">
    {!! Form::label('ss_doctor_id', 'Ss Doctor Id:') !!}
    <p>{{ $suggestedSpecialistsPatient->ss_doctor_id }}</p>
</div>

<!-- Suggested Specialists Field -->
<div class="form-group">
    {!! Form::label('suggested_specialists', 'Suggested Specialists:') !!}
    <p>{{ $suggestedSpecialistsPatient->suggested_specialists }}</p>
</div>

<!-- Created By Field -->
<div class="form-group">
    {!! Form::label('created_by', 'Created By:') !!}
    <p>{{ $suggestedSpecialistsPatient->created_by }}</p>
</div>

<!-- Updated By Field -->
<div class="form-group">
    {!! Form::label('updated_by', 'Updated By:') !!}
    <p>{{ $suggestedSpecialistsPatient->updated_by }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $suggestedSpecialistsPatient->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $suggestedSpecialistsPatient->updated_at }}</p>
</div>

