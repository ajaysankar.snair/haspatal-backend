<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Wallpaper Management</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900,900i&display=swap" rel="stylesheet">
    <script src="https://kit.fontawesome.com/fa5e54101c.js" crossorigin="anonymous"></script>
    <!-- JavaScript Bundle with Popper -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

    <!-- Theme Style -->
    <link rel="stylesheet" href="style.css">
    <style>
    .top_box{
    padding: 30px 30px 50px 30px;
    border-bottom: 2px solid #000
}
.pdr{
    padding-right: 0;
}
.bottom_box{
    padding: 30px;
}
.bottom_top{
    text-align: center;
    padding: 20px;
    margin-bottom: 10px;
}
.bg_cls{
    background: #db4130;
}
.form-group{
    margin-bottom: 15px;
}
.border_cls{
    padding-bottom: 15px;
    border-bottom: 2px solid #000;
    margin-bottom: 30px;
}
.submit_div{
    text-align: center;
    margin-top: 30px;
}
.btn-bg-red{
    background: #db4130;
    color: #fff;
}
    </style>
  </head>
  <body>
      <section id="">
          <div class="row">
              <div class="col-9 pdr">
                <div class="top_box"></div>
                <div class="bottom_box">
                    <div class="bottom_top">
                        <h3>Wallpaper Management</h3>
                    </div>
                    <div class="bottom_bt">
                        <form>
                            <div class="border_cls">
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label col-form-label-sm">Select Client</label>
                                    <div class="col-sm-10">
                                      <select class="form-control form-control-sm">
                                          <option selected>Choose...</option>
                                          <option>...</option>
                                      </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label col-form-label-sm">Select Order</label>
                                    <div class="col-sm-10">
                                      <select class="form-control form-control-sm">
                                          <option selected>Choose...</option>
                                          <option>...</option>
                                      </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                      <label class="col-sm-2 col-form-label col-form-label-sm">Select Wallpaper</label>
                                      <div class="col-sm-10">
                                        <select class="form-control form-control-sm">
                                            <option selected>Choose...</option>
                                            <option>...</option>
                                        </select>
                                      </div>
                                </div>
                            </div>
                            <div class="rem_cls">
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label col-form-label-sm">Start Date</label>
                                    <div class="col-sm-4">
                                      <input type="date" class="form-control form-control-sm" id="">
                                    </div>
                                    <label class="col-sm-2 col-form-label col-form-label-sm">End Date</label>
                                    <div class="col-sm-4">
                                      <input type="date" class="form-control form-control-sm" id="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label col-form-label-sm">Mesure Type</label>
                                    <div class="col-sm-10">
                                      <select class="form-control form-control-sm">
                                          <option selected>Choose...</option>
                                          <option>...</option>
                                      </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label col-form-label-sm">Stop After (Number of instances)</label>
                                    <div class="col-sm-10">
                                      <select class="form-control form-control-sm">
                                          <option selected>Choose...</option>
                                          <option>...</option>
                                      </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label col-form-label-sm">Click Destination URL</label>
                                    <div class="col-sm-10">
                                      <select class="form-control form-control-sm">
                                          <option selected>Choose...</option>
                                          <option>...</option>
                                      </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label col-form-label-sm">Display Territory</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control form-control-sm" id="" placeholder="Select multiple pincodes using state,district,city,pincodes.">
                                    </div>
                                </div>
                                <div class="submit_div">
                                    <button class="btn btn-bg-red" type="submit">Submit form</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
              </div>
              <div class="col-3 bg_cls">

              </div>
          </div>
      </section>
  </body>
</html>
