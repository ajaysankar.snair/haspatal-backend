
<!-- Promo Code Field -->
<div class="form-group">
    {!! Form::label('promo_code', 'Promo Code:') !!}
    <p>{{ $coupan->promo_code }}</p>
</div>

<!-- Used Promo Code Time Field -->
<div class="form-group">
    {!! Form::label('used_promo_code_time', 'Used Promo Code Time:') !!}
    <p>{{ $coupan->used_promo_code_time }}</p>
</div>

<!-- Amount Type Field -->
<div class="form-group">
    {!! Form::label('amount_type', 'Amount Type:') !!}
    <p>
        @if($coupan->amount_type == 2)
        %
        @else
        ₹
        @endif
    </p>
</div>

<!-- Price Field -->
<div class="form-group">
    {!! Form::label('price', 'Price:') !!}
    <p>{{ $coupan->price }}</p>
</div>

<!-- Sdate Field -->
<div class="form-group">
    {!! Form::label('sdate', 'Sdate:') !!}
    <p>{{ $coupan->sdate }}</p>
</div>

<!-- Edate Field -->
<div class="form-group">
    {!! Form::label('edate', 'Edate:') !!}
    <p>{{ $coupan->edate }}</p>
</div>


