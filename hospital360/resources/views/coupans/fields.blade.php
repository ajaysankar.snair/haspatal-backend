<!-- Promo Code Field -->
<div class="row">
  <!-- Amount Type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('type', 'Type:') !!}
    {!! Form::select('type',[''=>'Select Type '] + array('1' => 'Patient', '2' => 'Doctor','11' => 'Pharmacies','12'=>'Diagnostics','13'=>'Imaging','14'=>'Homecare','16'=>'Specialist'),null,['class' => 'form-control','data-parsley-required-message' =>'Amount Type is required', 'required']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('promo_code', 'Promo Code:') !!}
    {!! Form::text('promo_code', null, ['class' => 'form-control','data-parsley-required-message' =>'Promo code is required', 'required']) !!}
</div>

<!-- Used Promo Code Time Field -->
<div class="form-group col-sm-6">
    {!! Form::label('used_promo_code_time', 'Promo Code Used Time:') !!}
    {!! Form::text('used_promo_code_time', null, ['class' => 'form-control','data-parsley-required-message' =>'Promo code Used Time is required', 'required']) !!}
</div>

<!-- Amount Type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('amount_type', 'Amount Type:') !!}
    {!! Form::select('amount_type',[''=>'Select Amount Type '] + array('1' => '₹', '2' => '%'),null,['class' => 'form-control','data-parsley-required-message' =>'Amount Type is required', 'required']) !!}
</div>

<!-- Price Field -->
<div class="form-group col-sm-6">
    {!! Form::label('price', 'Price:') !!}
    {!! Form::text('price', null, ['class' => 'form-control','data-parsley-required-message' =>'Price is required', 'required']) !!}
</div>

<!-- Sdate Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sdate', 'Start Date:') !!}
    {!! Form::text('sdate', null, ['class' => 'form-control','id'=>'sdate','data-parsley-required-message' =>'Start Date is required', 'required']) !!}
</div>

@push('scripts')
   <script type="text/javascript">
           $('#sdate').datetimepicker({
               format: 'YYYY-MM-DD',
               useCurrent: true,
               icons: {
                   up: "icon-arrow-up-circle icons font-2xl",
                   down: "icon-arrow-down-circle icons font-2xl"
               },
               sideBySide: true
           })
       </script>
@endpush


<!-- Edate Field -->
<div class="form-group col-sm-6">
    {!! Form::label('edate', 'End Date:') !!}
    {!! Form::text('edate', null, ['class' => 'form-control','id'=>'edate','data-parsley-required-message' =>'End Date is required', 'required']) !!}
</div>

@push('scripts')
   <script type="text/javascript">
           $('#edate').datetimepicker({
               format: 'YYYY-MM-DD',
               useCurrent: true,
               icons: {
                   up: "icon-arrow-up-circle icons font-2xl",
                   down: "icon-arrow-down-circle icons font-2xl"
               },
               sideBySide: true
           })
       </script>
@endpush
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('coupans.index') }}" class="btn btn-secondary">Cancel</a>
</div>
