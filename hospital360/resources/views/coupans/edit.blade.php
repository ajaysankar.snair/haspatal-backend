@extends('layouts.app')
@section('title', 'Edit Coupan')
@section('content')
    <ol class="breadcrumb">
          <li class="breadcrumb-item">
             <a href="{!! route('coupans.index') !!}">Coupan</a>
          </li>
          <li class="breadcrumb-item active">Edit</li>
        </ol>
    <div class="container-fluid">
         <div class="animated fadeIn">
             @include('coreui-templates::common.errors')
             <div class="row">
                 <div class="col-lg-12">
                      <div class="card">
                          <div class="card-header">
                              <i class="fa fa-edit fa-lg"></i>
                              <strong>Edit Coupan</strong>
                          </div>
                          <div class="card-body">
                              {!! Form::model($coupan, ['route' => ['coupans.update', $coupan->id], 'method' => 'patch','id' => 'myForm']) !!}

                              @include('coupans.fields')

                              {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
         </div>
    </div>
@endsection