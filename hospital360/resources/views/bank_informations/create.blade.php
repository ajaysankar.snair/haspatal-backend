@extends('layouts.app')
@section('title', 'Create BankInfo')
@section('content')
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
         <a href="{!! route('bankInformations.index') !!}">Bank Information</a>
      </li>
      <li class="breadcrumb-item active">Create</li>
    </ol>
     <div class="container-fluid">
          <div class="animated fadeIn">
                @include('coreui-templates::common.errors')
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <i class="fa fa-plus-square-o fa-lg"></i>
                                <strong>Create Bank Information</strong>
                            </div>
                            <div class="card-body">
                                {!! Form::open(['route' => 'bankInformations.store','id'=>'myForm']) !!}

                                   @include('bank_informations.fields')

                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
           </div>
    </div>
@endsection
