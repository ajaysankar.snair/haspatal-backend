

<!-- Bank Name Field -->
<div class="form-group">
    {!! Form::label('bank_name', 'Bank Name:') !!}
    <p>{{ $bankInformation->bank_name }}</p>
</div>

<!-- Account Name Field -->
<div class="form-group">
    {!! Form::label('account_name', 'Account Name:') !!}
    <p>{{ $bankInformation->account_name }}</p>
</div>

<!-- Ifsc Code Field -->
<div class="form-group">
    {!! Form::label('ifsc_code', 'Ifsc Code:') !!}
    <p>{{ $bankInformation->ifsc_code }}</p>
</div>

<!-- Account No Field -->
<div class="form-group">
    {!! Form::label('account_no', 'Account No:') !!}
    <p>{{ $bankInformation->account_no }}</p>
</div>

<!-- Cancle Cheque Field -->
<div class="form-group">
    <?php $licenceImg = URL::asset('public/media/cancle_cheque/'.$bankInformation->cancle_cheque); ?>
    {!! Form::label('cancle_cheque', 'Cancle Cheque:') !!}

    <?php 
        if (!empty($licenceImg)) {
    ?>
        <p><img src = "<?php echo $licenceImg; ?>" style="height:150px;width: 150px;" ></p>
    <?php
        }
        else{
    ?>
        <p><img src = "{{url('public/image/default.png')}}" style="height:150px;width: 150px;" ></p>
    <?php
        }
    ?>
</div>

<!-- Status Field -->
<!-- <div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{{ $bankInformation->status }}</p>
</div> -->

<!-- Created By Field -->
<!-- <div class="form-group">
    {!! Form::label('created_by', 'Created By:') !!}
    <p>{{ $bankInformation->created_by }}</p>
</div>
 -->
<!-- Updated By Field -->
<!-- <div class="form-group">
    {!! Form::label('updated_by', 'Updated By:') !!}
    <p>{{ $bankInformation->updated_by }}</p>
</div> -->

<!-- Created At Field -->
<!-- <div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $bankInformation->created_at }}</p>
</div> -->

<!-- Updated At Field -->
<!-- <div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $bankInformation->updated_at }}</p>
</div> -->

