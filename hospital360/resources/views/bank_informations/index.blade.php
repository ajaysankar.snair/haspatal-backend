@extends('layouts.app')
@section('title', 'List Bank Info')
@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">Bank Informations</li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
             @include('flash::message')
             <div class="row">
                 <div class="col-lg-12">
                     <div class="card">
                         <div class="card-header">
                             <i class="fa fa-align-justify"></i>
                             Bank_informations
                             @permission(['admin','add_bankinfo'])
                             <a class="pull-right" href="{{ route('bankInformations.create') }}"><i class="fa fa-plus-square fa-lg"></i></a>
                             @endpermission
                         </div>
                         <div class="card-body">
                             @include('bank_informations.table')
                              <div class="pull-right mr-3">
                                     
                              </div>
                         </div>
                     </div>
                  </div>
             </div>
         </div>
    </div>
@endsection

