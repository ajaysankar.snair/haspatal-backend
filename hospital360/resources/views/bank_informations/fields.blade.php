<div class="row">
<!-- Bank Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('bank_name', 'Bank Name:') !!}
    {!! Form::text('bank_name', null, ['class' => 'form-control','data-parsley-required-message' =>'Bank Name is required','required']) !!}
</div>

<!-- Account Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('account_name', 'Account Name:') !!}
    {!! Form::text('account_name', null, ['class' => 'form-control','data-parsley-required-message' =>'Account Name is required','required']) !!}
</div>
</div>
<div class="row">
<!-- Ifsc Code Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ifsc_code', 'Ifsc Code:') !!}
    {!! Form::text('ifsc_code', null, ['class' => 'form-control','data-parsley-pattern="^[A-Za-z]{4}0[A-Z0-9a-z]{6}$"','data-parsley-type="alphanum"','data-parsley-required-message' =>'IFSC is required','required']) !!}
</div>

<!-- Account No Field -->
<div class="form-group col-sm-6">
    {!! Form::label('account_no', 'Account No:') !!}
    {!! Form::text('account_no', null, ['class' => 'form-control','data-parsley-length="[9,18]"','data-parsley-required-message' =>'Account No is required','required']) !!}
</div>
</div>
<div class="row">
<!-- Cancle Cheque Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cancle_cheque', 'Cancle Cheque:') !!}
    {!! Form::file('cancle_cheque',['data-parsley-required-message' =>'Cancle Cheque is required','required']) !!}
    
</div>

<!-- Status Field -->
<!-- <div class="form-group col-sm-6">
    {!! Form::label('status', 'Status:') !!}
    {!! Form::text('status', null, ['class' => 'form-control']) !!}
</div> -->
</div>
<!-- Created By Field -->
<!-- <div class="form-group col-sm-6">
    {!! Form::label('created_by', 'Created By:') !!}
    {!! Form::text('created_by', null, ['class' => 'form-control']) !!}
</div>
 -->
<!-- Updated By Field -->
<!-- <div class="form-group col-sm-6">
    {!! Form::label('updated_by', 'Updated By:') !!}
    {!! Form::text('updated_by', null, ['class' => 'form-control']) !!}
</div> -->

<!-- Submit Field -->
<div class="row">
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('bankInformations.index') }}" class="btn btn-secondary">Cancel</a>
</div>
</div>