<?php
	
	ini_set('display_errors', 1);
	date_default_timezone_set("Asia/Kolkata");

	$servername = "localhost";
	$username = "haspatalapp";
	$password = "Ashish@aelius";
	$dbname = "haspatalDB";

	// Create connection
	$conn = new mysqli($servername, $username, $password, $dbname);
	// Check connection
	if ($conn->connect_error) {
	  die("Connection failed: " . $conn->connect_error);
	}

	$today = date('Y-m-d');

	$sql = "SELECT * FROM `booking_request` WHERE `status` = 1 AND STR_TO_DATE(`date_time`,'%Y-%m-%d') = '".$today."'";
	//$sql = "SELECT * FROM `booking_request` WHERE `status` = 1 AND STR_TO_DATE(`date_time`,'%Y-%m-%d') = '2020-10-30'";

	$result = $conn->query($sql);

	if ($result->num_rows > 0) {
	  // output data of each row
			
		while($row = $result->fetch_assoc()) {

			$time_input = strtotime($row['date_time']); 
			//echo date('h:i',strtotime('-30 minutes', $time_input));
			$booking_time = strtotime('-30 minutes', $time_input);
			$current_time = strtotime(date('H:i'));
			
			if($booking_time == $current_time){
				
				$sql1 = "SELECT * FROM `patient_details` WHERE id=".$row['patient_name'];	
				$result1 = $conn->query($sql1);
				$row1 = $result1->fetch_assoc();		
				$msg = "Your Appointment is after 30 minutes Appointment Time is :- ".date('h:i', $time_input)." Please join the waiting room";
				
				send_mail($row1['email'],$msg);
				send_sms($row1['mobile'],$msg);
			}
		}
	}

	function send_mail($email,$msg)
	{
		$data['personalizations'] = array("0" => array('to' => array('0' => array('email' => $email))));

        $data['from'] = array('email' => 'system@haspatal.com');

        $data['subject'] = "Appointment Reminder";
        $data['content'] = array("0" =>array('type' =>' text/html','value' => "<DOCTYPE html><html lang='en-US'>     <head><meta charset='utf-8'></head><body><h2>  ".$msg."</body></html>" ));

        //echo json_encode($data);exit();

        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://api.sendgrid.com/v3/mail/send",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => json_encode($data),
          CURLOPT_HTTPHEADER => array(
            "authorization: Bearer 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",
            "cache-control: no-cache",
            "content-type: application/json",
            "postman-token: b8c0781f-af48-90cc-1c5b-76f80e0b4be0"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          echo "cURL Error #:" . $err;
        } else {
        	echo $response;exit;
          return $response;
        }
	}
	function send_sms($mobile,$msg)
	{
		$curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://sms.bulksmsind.in/sendSMS?username=haspatal&message=".urlencode($msg)."&sendername=HSPTAL&smstype=TRANS&numbers=".$mobile."&apikey=206f20fc-d4d5-4a33-ad06-10f453b8ab4a",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => true,
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
            echo "cURL Error #:" . $err;
            //return response()->json(["status" => true,"message" => "OTP Not Send","data"=>$err]);
        } else {
            echo $response;exit();
            //return response()->json(["status" => true,"message" => "Send OTP in your mobile no"]);
        }
	}

?>