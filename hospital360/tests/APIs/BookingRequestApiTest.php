<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\BookingRequest;

class BookingRequestApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_booking_request()
    {
        $bookingRequest = factory(BookingRequest::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/booking_requests', $bookingRequest
        );

        $this->assertApiResponse($bookingRequest);
    }

    /**
     * @test
     */
    public function test_read_booking_request()
    {
        $bookingRequest = factory(BookingRequest::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/booking_requests/'.$bookingRequest->id
        );

        $this->assertApiResponse($bookingRequest->toArray());
    }

    /**
     * @test
     */
    public function test_update_booking_request()
    {
        $bookingRequest = factory(BookingRequest::class)->create();
        $editedBookingRequest = factory(BookingRequest::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/booking_requests/'.$bookingRequest->id,
            $editedBookingRequest
        );

        $this->assertApiResponse($editedBookingRequest);
    }

    /**
     * @test
     */
    public function test_delete_booking_request()
    {
        $bookingRequest = factory(BookingRequest::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/booking_requests/'.$bookingRequest->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/booking_requests/'.$bookingRequest->id
        );

        $this->response->assertStatus(404);
    }
}
