<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Service_details;

class Service_detailsApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_service_details()
    {
        $serviceDetails = factory(Service_details::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/service_details', $serviceDetails
        );

        $this->assertApiResponse($serviceDetails);
    }

    /**
     * @test
     */
    public function test_read_service_details()
    {
        $serviceDetails = factory(Service_details::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/service_details/'.$serviceDetails->id
        );

        $this->assertApiResponse($serviceDetails->toArray());
    }

    /**
     * @test
     */
    public function test_update_service_details()
    {
        $serviceDetails = factory(Service_details::class)->create();
        $editedService_details = factory(Service_details::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/service_details/'.$serviceDetails->id,
            $editedService_details
        );

        $this->assertApiResponse($editedService_details);
    }

    /**
     * @test
     */
    public function test_delete_service_details()
    {
        $serviceDetails = factory(Service_details::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/service_details/'.$serviceDetails->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/service_details/'.$serviceDetails->id
        );

        $this->response->assertStatus(404);
    }
}
