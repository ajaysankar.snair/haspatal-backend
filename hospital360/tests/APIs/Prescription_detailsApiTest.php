<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Prescription_details;

class Prescription_detailsApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_prescription_details()
    {
        $prescriptionDetails = factory(Prescription_details::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/prescription_details', $prescriptionDetails
        );

        $this->assertApiResponse($prescriptionDetails);
    }

    /**
     * @test
     */
    public function test_read_prescription_details()
    {
        $prescriptionDetails = factory(Prescription_details::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/prescription_details/'.$prescriptionDetails->id
        );

        $this->assertApiResponse($prescriptionDetails->toArray());
    }

    /**
     * @test
     */
    public function test_update_prescription_details()
    {
        $prescriptionDetails = factory(Prescription_details::class)->create();
        $editedPrescription_details = factory(Prescription_details::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/prescription_details/'.$prescriptionDetails->id,
            $editedPrescription_details
        );

        $this->assertApiResponse($editedPrescription_details);
    }

    /**
     * @test
     */
    public function test_delete_prescription_details()
    {
        $prescriptionDetails = factory(Prescription_details::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/prescription_details/'.$prescriptionDetails->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/prescription_details/'.$prescriptionDetails->id
        );

        $this->response->assertStatus(404);
    }
}
