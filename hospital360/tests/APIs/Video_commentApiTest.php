<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Video_comment;

class Video_commentApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_video_comment()
    {
        $videoComment = factory(Video_comment::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/video_comments', $videoComment
        );

        $this->assertApiResponse($videoComment);
    }

    /**
     * @test
     */
    public function test_read_video_comment()
    {
        $videoComment = factory(Video_comment::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/video_comments/'.$videoComment->id
        );

        $this->assertApiResponse($videoComment->toArray());
    }

    /**
     * @test
     */
    public function test_update_video_comment()
    {
        $videoComment = factory(Video_comment::class)->create();
        $editedVideo_comment = factory(Video_comment::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/video_comments/'.$videoComment->id,
            $editedVideo_comment
        );

        $this->assertApiResponse($editedVideo_comment);
    }

    /**
     * @test
     */
    public function test_delete_video_comment()
    {
        $videoComment = factory(Video_comment::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/video_comments/'.$videoComment->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/video_comments/'.$videoComment->id
        );

        $this->response->assertStatus(404);
    }
}
