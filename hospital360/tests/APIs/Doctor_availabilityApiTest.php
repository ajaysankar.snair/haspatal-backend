<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Doctor_availability;

class Doctor_availabilityApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_doctor_availability()
    {
        $doctorAvailability = factory(Doctor_availability::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/doctor_availabilities', $doctorAvailability
        );

        $this->assertApiResponse($doctorAvailability);
    }

    /**
     * @test
     */
    public function test_read_doctor_availability()
    {
        $doctorAvailability = factory(Doctor_availability::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/doctor_availabilities/'.$doctorAvailability->id
        );

        $this->assertApiResponse($doctorAvailability->toArray());
    }

    /**
     * @test
     */
    public function test_update_doctor_availability()
    {
        $doctorAvailability = factory(Doctor_availability::class)->create();
        $editedDoctor_availability = factory(Doctor_availability::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/doctor_availabilities/'.$doctorAvailability->id,
            $editedDoctor_availability
        );

        $this->assertApiResponse($editedDoctor_availability);
    }

    /**
     * @test
     */
    public function test_delete_doctor_availability()
    {
        $doctorAvailability = factory(Doctor_availability::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/doctor_availabilities/'.$doctorAvailability->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/doctor_availabilities/'.$doctorAvailability->id
        );

        $this->response->assertStatus(404);
    }
}
