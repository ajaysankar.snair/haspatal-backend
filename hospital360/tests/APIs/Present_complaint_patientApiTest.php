<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Present_complaint_patient;

class Present_complaint_patientApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_present_complaint_patient()
    {
        $presentComplaintPatient = factory(Present_complaint_patient::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/present_complaint_patients', $presentComplaintPatient
        );

        $this->assertApiResponse($presentComplaintPatient);
    }

    /**
     * @test
     */
    public function test_read_present_complaint_patient()
    {
        $presentComplaintPatient = factory(Present_complaint_patient::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/present_complaint_patients/'.$presentComplaintPatient->id
        );

        $this->assertApiResponse($presentComplaintPatient->toArray());
    }

    /**
     * @test
     */
    public function test_update_present_complaint_patient()
    {
        $presentComplaintPatient = factory(Present_complaint_patient::class)->create();
        $editedPresent_complaint_patient = factory(Present_complaint_patient::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/present_complaint_patients/'.$presentComplaintPatient->id,
            $editedPresent_complaint_patient
        );

        $this->assertApiResponse($editedPresent_complaint_patient);
    }

    /**
     * @test
     */
    public function test_delete_present_complaint_patient()
    {
        $presentComplaintPatient = factory(Present_complaint_patient::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/present_complaint_patients/'.$presentComplaintPatient->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/present_complaint_patients/'.$presentComplaintPatient->id
        );

        $this->response->assertStatus(404);
    }
}
