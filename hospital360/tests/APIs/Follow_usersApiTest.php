<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Follow_users;

class Follow_usersApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_follow_users()
    {
        $followUsers = factory(Follow_users::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/follow_users', $followUsers
        );

        $this->assertApiResponse($followUsers);
    }

    /**
     * @test
     */
    public function test_read_follow_users()
    {
        $followUsers = factory(Follow_users::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/follow_users/'.$followUsers->id
        );

        $this->assertApiResponse($followUsers->toArray());
    }

    /**
     * @test
     */
    public function test_update_follow_users()
    {
        $followUsers = factory(Follow_users::class)->create();
        $editedFollow_users = factory(Follow_users::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/follow_users/'.$followUsers->id,
            $editedFollow_users
        );

        $this->assertApiResponse($editedFollow_users);
    }

    /**
     * @test
     */
    public function test_delete_follow_users()
    {
        $followUsers = factory(Follow_users::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/follow_users/'.$followUsers->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/follow_users/'.$followUsers->id
        );

        $this->response->assertStatus(404);
    }
}
