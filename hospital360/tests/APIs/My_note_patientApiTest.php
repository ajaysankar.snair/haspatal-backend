<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\My_note_patient;

class My_note_patientApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_my_note_patient()
    {
        $myNotePatient = factory(My_note_patient::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/my_note_patients', $myNotePatient
        );

        $this->assertApiResponse($myNotePatient);
    }

    /**
     * @test
     */
    public function test_read_my_note_patient()
    {
        $myNotePatient = factory(My_note_patient::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/my_note_patients/'.$myNotePatient->id
        );

        $this->assertApiResponse($myNotePatient->toArray());
    }

    /**
     * @test
     */
    public function test_update_my_note_patient()
    {
        $myNotePatient = factory(My_note_patient::class)->create();
        $editedMy_note_patient = factory(My_note_patient::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/my_note_patients/'.$myNotePatient->id,
            $editedMy_note_patient
        );

        $this->assertApiResponse($editedMy_note_patient);
    }

    /**
     * @test
     */
    public function test_delete_my_note_patient()
    {
        $myNotePatient = factory(My_note_patient::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/my_note_patients/'.$myNotePatient->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/my_note_patients/'.$myNotePatient->id
        );

        $this->response->assertStatus(404);
    }
}
