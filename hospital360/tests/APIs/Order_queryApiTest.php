<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Order_query;

class Order_queryApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_order_query()
    {
        $orderQuery = factory(Order_query::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/order_queries', $orderQuery
        );

        $this->assertApiResponse($orderQuery);
    }

    /**
     * @test
     */
    public function test_read_order_query()
    {
        $orderQuery = factory(Order_query::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/order_queries/'.$orderQuery->id
        );

        $this->assertApiResponse($orderQuery->toArray());
    }

    /**
     * @test
     */
    public function test_update_order_query()
    {
        $orderQuery = factory(Order_query::class)->create();
        $editedOrder_query = factory(Order_query::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/order_queries/'.$orderQuery->id,
            $editedOrder_query
        );

        $this->assertApiResponse($editedOrder_query);
    }

    /**
     * @test
     */
    public function test_delete_order_query()
    {
        $orderQuery = factory(Order_query::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/order_queries/'.$orderQuery->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/order_queries/'.$orderQuery->id
        );

        $this->response->assertStatus(404);
    }
}
