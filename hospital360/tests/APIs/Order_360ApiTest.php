<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Order_360;

class Order_360ApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_order_360()
    {
        $order360 = factory(Order_360::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/order_360s', $order360
        );

        $this->assertApiResponse($order360);
    }

    /**
     * @test
     */
    public function test_read_order_360()
    {
        $order360 = factory(Order_360::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/order_360s/'.$order360->id
        );

        $this->assertApiResponse($order360->toArray());
    }

    /**
     * @test
     */
    public function test_update_order_360()
    {
        $order360 = factory(Order_360::class)->create();
        $editedOrder_360 = factory(Order_360::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/order_360s/'.$order360->id,
            $editedOrder_360
        );

        $this->assertApiResponse($editedOrder_360);
    }

    /**
     * @test
     */
    public function test_delete_order_360()
    {
        $order360 = factory(Order_360::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/order_360s/'.$order360->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/order_360s/'.$order360->id
        );

        $this->response->assertStatus(404);
    }
}
