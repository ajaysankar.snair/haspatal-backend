<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Patient_refund_request;

class Patient_refund_requestApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_patient_refund_request()
    {
        $patientRefundRequest = factory(Patient_refund_request::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/patient_refund_requests', $patientRefundRequest
        );

        $this->assertApiResponse($patientRefundRequest);
    }

    /**
     * @test
     */
    public function test_read_patient_refund_request()
    {
        $patientRefundRequest = factory(Patient_refund_request::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/patient_refund_requests/'.$patientRefundRequest->id
        );

        $this->assertApiResponse($patientRefundRequest->toArray());
    }

    /**
     * @test
     */
    public function test_update_patient_refund_request()
    {
        $patientRefundRequest = factory(Patient_refund_request::class)->create();
        $editedPatient_refund_request = factory(Patient_refund_request::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/patient_refund_requests/'.$patientRefundRequest->id,
            $editedPatient_refund_request
        );

        $this->assertApiResponse($editedPatient_refund_request);
    }

    /**
     * @test
     */
    public function test_delete_patient_refund_request()
    {
        $patientRefundRequest = factory(Patient_refund_request::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/patient_refund_requests/'.$patientRefundRequest->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/patient_refund_requests/'.$patientRefundRequest->id
        );

        $this->response->assertStatus(404);
    }
}
