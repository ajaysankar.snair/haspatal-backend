<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Your_coverage_360;

class Your_coverage_360ApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_your_coverage_360()
    {
        $yourCoverage360 = factory(Your_coverage_360::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/your_coverage_360s', $yourCoverage360
        );

        $this->assertApiResponse($yourCoverage360);
    }

    /**
     * @test
     */
    public function test_read_your_coverage_360()
    {
        $yourCoverage360 = factory(Your_coverage_360::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/your_coverage_360s/'.$yourCoverage360->id
        );

        $this->assertApiResponse($yourCoverage360->toArray());
    }

    /**
     * @test
     */
    public function test_update_your_coverage_360()
    {
        $yourCoverage360 = factory(Your_coverage_360::class)->create();
        $editedYour_coverage_360 = factory(Your_coverage_360::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/your_coverage_360s/'.$yourCoverage360->id,
            $editedYour_coverage_360
        );

        $this->assertApiResponse($editedYour_coverage_360);
    }

    /**
     * @test
     */
    public function test_delete_your_coverage_360()
    {
        $yourCoverage360 = factory(Your_coverage_360::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/your_coverage_360s/'.$yourCoverage360->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/your_coverage_360s/'.$yourCoverage360->id
        );

        $this->response->assertStatus(404);
    }
}
