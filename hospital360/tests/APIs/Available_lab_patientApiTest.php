<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Available_lab_patient;

class Available_lab_patientApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_available_lab_patient()
    {
        $availableLabPatient = factory(Available_lab_patient::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/available_lab_patients', $availableLabPatient
        );

        $this->assertApiResponse($availableLabPatient);
    }

    /**
     * @test
     */
    public function test_read_available_lab_patient()
    {
        $availableLabPatient = factory(Available_lab_patient::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/available_lab_patients/'.$availableLabPatient->id
        );

        $this->assertApiResponse($availableLabPatient->toArray());
    }

    /**
     * @test
     */
    public function test_update_available_lab_patient()
    {
        $availableLabPatient = factory(Available_lab_patient::class)->create();
        $editedAvailable_lab_patient = factory(Available_lab_patient::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/available_lab_patients/'.$availableLabPatient->id,
            $editedAvailable_lab_patient
        );

        $this->assertApiResponse($editedAvailable_lab_patient);
    }

    /**
     * @test
     */
    public function test_delete_available_lab_patient()
    {
        $availableLabPatient = factory(Available_lab_patient::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/available_lab_patients/'.$availableLabPatient->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/available_lab_patients/'.$availableLabPatient->id
        );

        $this->response->assertStatus(404);
    }
}
