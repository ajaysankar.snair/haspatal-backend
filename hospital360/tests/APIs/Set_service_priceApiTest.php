<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Set_service_price;

class Set_service_priceApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_set_service_price()
    {
        $setServicePrice = factory(Set_service_price::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/set_service_prices', $setServicePrice
        );

        $this->assertApiResponse($setServicePrice);
    }

    /**
     * @test
     */
    public function test_read_set_service_price()
    {
        $setServicePrice = factory(Set_service_price::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/set_service_prices/'.$setServicePrice->id
        );

        $this->assertApiResponse($setServicePrice->toArray());
    }

    /**
     * @test
     */
    public function test_update_set_service_price()
    {
        $setServicePrice = factory(Set_service_price::class)->create();
        $editedSet_service_price = factory(Set_service_price::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/set_service_prices/'.$setServicePrice->id,
            $editedSet_service_price
        );

        $this->assertApiResponse($editedSet_service_price);
    }

    /**
     * @test
     */
    public function test_delete_set_service_price()
    {
        $setServicePrice = factory(Set_service_price::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/set_service_prices/'.$setServicePrice->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/set_service_prices/'.$setServicePrice->id
        );

        $this->response->assertStatus(404);
    }
}
