<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Haspatal_360_register;

class Haspatal_360_registerApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_haspatal_360_register()
    {
        $haspatal360Register = factory(Haspatal_360_register::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/haspatal_360_registers', $haspatal360Register
        );

        $this->assertApiResponse($haspatal360Register);
    }

    /**
     * @test
     */
    public function test_read_haspatal_360_register()
    {
        $haspatal360Register = factory(Haspatal_360_register::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/haspatal_360_registers/'.$haspatal360Register->id
        );

        $this->assertApiResponse($haspatal360Register->toArray());
    }

    /**
     * @test
     */
    public function test_update_haspatal_360_register()
    {
        $haspatal360Register = factory(Haspatal_360_register::class)->create();
        $editedHaspatal_360_register = factory(Haspatal_360_register::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/haspatal_360_registers/'.$haspatal360Register->id,
            $editedHaspatal_360_register
        );

        $this->assertApiResponse($editedHaspatal_360_register);
    }

    /**
     * @test
     */
    public function test_delete_haspatal_360_register()
    {
        $haspatal360Register = factory(Haspatal_360_register::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/haspatal_360_registers/'.$haspatal360Register->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/haspatal_360_registers/'.$haspatal360Register->id
        );

        $this->response->assertStatus(404);
    }
}
