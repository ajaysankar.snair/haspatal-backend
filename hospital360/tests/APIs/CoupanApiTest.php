<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Coupan;

class CoupanApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_coupan()
    {
        $coupan = factory(Coupan::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/coupans', $coupan
        );

        $this->assertApiResponse($coupan);
    }

    /**
     * @test
     */
    public function test_read_coupan()
    {
        $coupan = factory(Coupan::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/coupans/'.$coupan->id
        );

        $this->assertApiResponse($coupan->toArray());
    }

    /**
     * @test
     */
    public function test_update_coupan()
    {
        $coupan = factory(Coupan::class)->create();
        $editedCoupan = factory(Coupan::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/coupans/'.$coupan->id,
            $editedCoupan
        );

        $this->assertApiResponse($editedCoupan);
    }

    /**
     * @test
     */
    public function test_delete_coupan()
    {
        $coupan = factory(Coupan::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/coupans/'.$coupan->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/coupans/'.$coupan->id
        );

        $this->response->assertStatus(404);
    }
}
