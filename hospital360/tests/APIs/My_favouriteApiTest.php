<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\My_favourite;

class My_favouriteApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_my_favourite()
    {
        $myFavourite = factory(My_favourite::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/my_favourites', $myFavourite
        );

        $this->assertApiResponse($myFavourite);
    }

    /**
     * @test
     */
    public function test_read_my_favourite()
    {
        $myFavourite = factory(My_favourite::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/my_favourites/'.$myFavourite->id
        );

        $this->assertApiResponse($myFavourite->toArray());
    }

    /**
     * @test
     */
    public function test_update_my_favourite()
    {
        $myFavourite = factory(My_favourite::class)->create();
        $editedMy_favourite = factory(My_favourite::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/my_favourites/'.$myFavourite->id,
            $editedMy_favourite
        );

        $this->assertApiResponse($editedMy_favourite);
    }

    /**
     * @test
     */
    public function test_delete_my_favourite()
    {
        $myFavourite = factory(My_favourite::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/my_favourites/'.$myFavourite->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/my_favourites/'.$myFavourite->id
        );

        $this->response->assertStatus(404);
    }
}
