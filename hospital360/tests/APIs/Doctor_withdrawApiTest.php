<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Doctor_withdraw;

class Doctor_withdrawApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_doctor_withdraw()
    {
        $doctorWithdraw = factory(Doctor_withdraw::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/doctor_withdraws', $doctorWithdraw
        );

        $this->assertApiResponse($doctorWithdraw);
    }

    /**
     * @test
     */
    public function test_read_doctor_withdraw()
    {
        $doctorWithdraw = factory(Doctor_withdraw::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/doctor_withdraws/'.$doctorWithdraw->id
        );

        $this->assertApiResponse($doctorWithdraw->toArray());
    }

    /**
     * @test
     */
    public function test_update_doctor_withdraw()
    {
        $doctorWithdraw = factory(Doctor_withdraw::class)->create();
        $editedDoctor_withdraw = factory(Doctor_withdraw::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/doctor_withdraws/'.$doctorWithdraw->id,
            $editedDoctor_withdraw
        );

        $this->assertApiResponse($editedDoctor_withdraw);
    }

    /**
     * @test
     */
    public function test_delete_doctor_withdraw()
    {
        $doctorWithdraw = factory(Doctor_withdraw::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/doctor_withdraws/'.$doctorWithdraw->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/doctor_withdraws/'.$doctorWithdraw->id
        );

        $this->response->assertStatus(404);
    }
}
