<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Examination_notes_patient;

class Examination_notes_patientApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_examination_notes_patient()
    {
        $examinationNotesPatient = factory(Examination_notes_patient::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/examination_notes_patients', $examinationNotesPatient
        );

        $this->assertApiResponse($examinationNotesPatient);
    }

    /**
     * @test
     */
    public function test_read_examination_notes_patient()
    {
        $examinationNotesPatient = factory(Examination_notes_patient::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/examination_notes_patients/'.$examinationNotesPatient->id
        );

        $this->assertApiResponse($examinationNotesPatient->toArray());
    }

    /**
     * @test
     */
    public function test_update_examination_notes_patient()
    {
        $examinationNotesPatient = factory(Examination_notes_patient::class)->create();
        $editedExamination_notes_patient = factory(Examination_notes_patient::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/examination_notes_patients/'.$examinationNotesPatient->id,
            $editedExamination_notes_patient
        );

        $this->assertApiResponse($editedExamination_notes_patient);
    }

    /**
     * @test
     */
    public function test_delete_examination_notes_patient()
    {
        $examinationNotesPatient = factory(Examination_notes_patient::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/examination_notes_patients/'.$examinationNotesPatient->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/examination_notes_patients/'.$examinationNotesPatient->id
        );

        $this->response->assertStatus(404);
    }
}
