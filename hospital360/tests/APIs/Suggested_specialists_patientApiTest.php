<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Suggested_specialists_patient;

class Suggested_specialists_patientApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_suggested_specialists_patient()
    {
        $suggestedSpecialistsPatient = factory(Suggested_specialists_patient::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/suggested_specialists_patients', $suggestedSpecialistsPatient
        );

        $this->assertApiResponse($suggestedSpecialistsPatient);
    }

    /**
     * @test
     */
    public function test_read_suggested_specialists_patient()
    {
        $suggestedSpecialistsPatient = factory(Suggested_specialists_patient::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/suggested_specialists_patients/'.$suggestedSpecialistsPatient->id
        );

        $this->assertApiResponse($suggestedSpecialistsPatient->toArray());
    }

    /**
     * @test
     */
    public function test_update_suggested_specialists_patient()
    {
        $suggestedSpecialistsPatient = factory(Suggested_specialists_patient::class)->create();
        $editedSuggested_specialists_patient = factory(Suggested_specialists_patient::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/suggested_specialists_patients/'.$suggestedSpecialistsPatient->id,
            $editedSuggested_specialists_patient
        );

        $this->assertApiResponse($editedSuggested_specialists_patient);
    }

    /**
     * @test
     */
    public function test_delete_suggested_specialists_patient()
    {
        $suggestedSpecialistsPatient = factory(Suggested_specialists_patient::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/suggested_specialists_patients/'.$suggestedSpecialistsPatient->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/suggested_specialists_patients/'.$suggestedSpecialistsPatient->id
        );

        $this->response->assertStatus(404);
    }
}
