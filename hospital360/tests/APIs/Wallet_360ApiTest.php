<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Wallet_360;

class Wallet_360ApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_wallet_360()
    {
        $wallet360 = factory(Wallet_360::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/wallet_360s', $wallet360
        );

        $this->assertApiResponse($wallet360);
    }

    /**
     * @test
     */
    public function test_read_wallet_360()
    {
        $wallet360 = factory(Wallet_360::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/wallet_360s/'.$wallet360->id
        );

        $this->assertApiResponse($wallet360->toArray());
    }

    /**
     * @test
     */
    public function test_update_wallet_360()
    {
        $wallet360 = factory(Wallet_360::class)->create();
        $editedWallet_360 = factory(Wallet_360::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/wallet_360s/'.$wallet360->id,
            $editedWallet_360
        );

        $this->assertApiResponse($editedWallet_360);
    }

    /**
     * @test
     */
    public function test_delete_wallet_360()
    {
        $wallet360 = factory(Wallet_360::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/wallet_360s/'.$wallet360->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/wallet_360s/'.$wallet360->id
        );

        $this->response->assertStatus(404);
    }
}
