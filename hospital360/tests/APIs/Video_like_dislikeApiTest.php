<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Video_like_dislike;

class Video_like_dislikeApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_video_like_dislike()
    {
        $videoLikeDislike = factory(Video_like_dislike::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/video_like_dislikes', $videoLikeDislike
        );

        $this->assertApiResponse($videoLikeDislike);
    }

    /**
     * @test
     */
    public function test_read_video_like_dislike()
    {
        $videoLikeDislike = factory(Video_like_dislike::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/video_like_dislikes/'.$videoLikeDislike->id
        );

        $this->assertApiResponse($videoLikeDislike->toArray());
    }

    /**
     * @test
     */
    public function test_update_video_like_dislike()
    {
        $videoLikeDislike = factory(Video_like_dislike::class)->create();
        $editedVideo_like_dislike = factory(Video_like_dislike::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/video_like_dislikes/'.$videoLikeDislike->id,
            $editedVideo_like_dislike
        );

        $this->assertApiResponse($editedVideo_like_dislike);
    }

    /**
     * @test
     */
    public function test_delete_video_like_dislike()
    {
        $videoLikeDislike = factory(Video_like_dislike::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/video_like_dislikes/'.$videoLikeDislike->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/video_like_dislikes/'.$videoLikeDislike->id
        );

        $this->response->assertStatus(404);
    }
}
