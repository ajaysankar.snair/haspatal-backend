<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\MemberDetails;

class MemberDetailsApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_member_details()
    {
        $memberDetails = factory(MemberDetails::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/member_details', $memberDetails
        );

        $this->assertApiResponse($memberDetails);
    }

    /**
     * @test
     */
    public function test_read_member_details()
    {
        $memberDetails = factory(MemberDetails::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/member_details/'.$memberDetails->id
        );

        $this->assertApiResponse($memberDetails->toArray());
    }

    /**
     * @test
     */
    public function test_update_member_details()
    {
        $memberDetails = factory(MemberDetails::class)->create();
        $editedMemberDetails = factory(MemberDetails::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/member_details/'.$memberDetails->id,
            $editedMemberDetails
        );

        $this->assertApiResponse($editedMemberDetails);
    }

    /**
     * @test
     */
    public function test_delete_member_details()
    {
        $memberDetails = factory(MemberDetails::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/member_details/'.$memberDetails->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/member_details/'.$memberDetails->id
        );

        $this->response->assertStatus(404);
    }
}
