<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\TimeSlot;

class TimeSlotApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_time_slot()
    {
        $timeSlot = factory(TimeSlot::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/time_slots', $timeSlot
        );

        $this->assertApiResponse($timeSlot);
    }

    /**
     * @test
     */
    public function test_read_time_slot()
    {
        $timeSlot = factory(TimeSlot::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/time_slots/'.$timeSlot->id
        );

        $this->assertApiResponse($timeSlot->toArray());
    }

    /**
     * @test
     */
    public function test_update_time_slot()
    {
        $timeSlot = factory(TimeSlot::class)->create();
        $editedTimeSlot = factory(TimeSlot::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/time_slots/'.$timeSlot->id,
            $editedTimeSlot
        );

        $this->assertApiResponse($editedTimeSlot);
    }

    /**
     * @test
     */
    public function test_delete_time_slot()
    {
        $timeSlot = factory(TimeSlot::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/time_slots/'.$timeSlot->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/time_slots/'.$timeSlot->id
        );

        $this->response->assertStatus(404);
    }
}
