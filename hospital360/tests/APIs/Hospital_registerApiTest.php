<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Hospital_register;

class Hospital_registerApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_hospital_register()
    {
        $hospitalRegister = factory(Hospital_register::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/hospital_registers', $hospitalRegister
        );

        $this->assertApiResponse($hospitalRegister);
    }

    /**
     * @test
     */
    public function test_read_hospital_register()
    {
        $hospitalRegister = factory(Hospital_register::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/hospital_registers/'.$hospitalRegister->id
        );

        $this->assertApiResponse($hospitalRegister->toArray());
    }

    /**
     * @test
     */
    public function test_update_hospital_register()
    {
        $hospitalRegister = factory(Hospital_register::class)->create();
        $editedHospital_register = factory(Hospital_register::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/hospital_registers/'.$hospitalRegister->id,
            $editedHospital_register
        );

        $this->assertApiResponse($editedHospital_register);
    }

    /**
     * @test
     */
    public function test_delete_hospital_register()
    {
        $hospitalRegister = factory(Hospital_register::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/hospital_registers/'.$hospitalRegister->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/hospital_registers/'.$hospitalRegister->id
        );

        $this->response->assertStatus(404);
    }
}
