<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Suggested_investigations_patient;

class Suggested_investigations_patientApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_suggested_investigations_patient()
    {
        $suggestedInvestigationsPatient = factory(Suggested_investigations_patient::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/suggested_investigations_patients', $suggestedInvestigationsPatient
        );

        $this->assertApiResponse($suggestedInvestigationsPatient);
    }

    /**
     * @test
     */
    public function test_read_suggested_investigations_patient()
    {
        $suggestedInvestigationsPatient = factory(Suggested_investigations_patient::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/suggested_investigations_patients/'.$suggestedInvestigationsPatient->id
        );

        $this->assertApiResponse($suggestedInvestigationsPatient->toArray());
    }

    /**
     * @test
     */
    public function test_update_suggested_investigations_patient()
    {
        $suggestedInvestigationsPatient = factory(Suggested_investigations_patient::class)->create();
        $editedSuggested_investigations_patient = factory(Suggested_investigations_patient::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/suggested_investigations_patients/'.$suggestedInvestigationsPatient->id,
            $editedSuggested_investigations_patient
        );

        $this->assertApiResponse($editedSuggested_investigations_patient);
    }

    /**
     * @test
     */
    public function test_delete_suggested_investigations_patient()
    {
        $suggestedInvestigationsPatient = factory(Suggested_investigations_patient::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/suggested_investigations_patients/'.$suggestedInvestigationsPatient->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/suggested_investigations_patients/'.$suggestedInvestigationsPatient->id
        );

        $this->response->assertStatus(404);
    }
}
