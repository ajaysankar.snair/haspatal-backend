<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Plan_master;

class Plan_masterApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_plan_master()
    {
        $planMaster = factory(Plan_master::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/plan_masters', $planMaster
        );

        $this->assertApiResponse($planMaster);
    }

    /**
     * @test
     */
    public function test_read_plan_master()
    {
        $planMaster = factory(Plan_master::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/plan_masters/'.$planMaster->id
        );

        $this->assertApiResponse($planMaster->toArray());
    }

    /**
     * @test
     */
    public function test_update_plan_master()
    {
        $planMaster = factory(Plan_master::class)->create();
        $editedPlan_master = factory(Plan_master::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/plan_masters/'.$planMaster->id,
            $editedPlan_master
        );

        $this->assertApiResponse($editedPlan_master);
    }

    /**
     * @test
     */
    public function test_delete_plan_master()
    {
        $planMaster = factory(Plan_master::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/plan_masters/'.$planMaster->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/plan_masters/'.$planMaster->id
        );

        $this->response->assertStatus(404);
    }
}
