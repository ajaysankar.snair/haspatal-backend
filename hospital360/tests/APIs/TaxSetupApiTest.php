<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\TaxSetup;

class TaxSetupApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_tax_setup()
    {
        $taxSetup = factory(TaxSetup::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/tax_setups', $taxSetup
        );

        $this->assertApiResponse($taxSetup);
    }

    /**
     * @test
     */
    public function test_read_tax_setup()
    {
        $taxSetup = factory(TaxSetup::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/tax_setups/'.$taxSetup->id
        );

        $this->assertApiResponse($taxSetup->toArray());
    }

    /**
     * @test
     */
    public function test_update_tax_setup()
    {
        $taxSetup = factory(TaxSetup::class)->create();
        $editedTaxSetup = factory(TaxSetup::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/tax_setups/'.$taxSetup->id,
            $editedTaxSetup
        );

        $this->assertApiResponse($editedTaxSetup);
    }

    /**
     * @test
     */
    public function test_delete_tax_setup()
    {
        $taxSetup = factory(TaxSetup::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/tax_setups/'.$taxSetup->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/tax_setups/'.$taxSetup->id
        );

        $this->response->assertStatus(404);
    }
}
