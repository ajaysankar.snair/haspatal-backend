<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Patient_add_fund;

class Patient_add_fundApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_patient_add_fund()
    {
        $patientAddFund = factory(Patient_add_fund::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/patient_add_funds', $patientAddFund
        );

        $this->assertApiResponse($patientAddFund);
    }

    /**
     * @test
     */
    public function test_read_patient_add_fund()
    {
        $patientAddFund = factory(Patient_add_fund::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/patient_add_funds/'.$patientAddFund->id
        );

        $this->assertApiResponse($patientAddFund->toArray());
    }

    /**
     * @test
     */
    public function test_update_patient_add_fund()
    {
        $patientAddFund = factory(Patient_add_fund::class)->create();
        $editedPatient_add_fund = factory(Patient_add_fund::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/patient_add_funds/'.$patientAddFund->id,
            $editedPatient_add_fund
        );

        $this->assertApiResponse($editedPatient_add_fund);
    }

    /**
     * @test
     */
    public function test_delete_patient_add_fund()
    {
        $patientAddFund = factory(Patient_add_fund::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/patient_add_funds/'.$patientAddFund->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/patient_add_funds/'.$patientAddFund->id
        );

        $this->response->assertStatus(404);
    }
}
