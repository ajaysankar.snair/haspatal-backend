<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\PropertyDetails;

class PropertyDetailsApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_property_details()
    {
        $propertyDetails = factory(PropertyDetails::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/property_details', $propertyDetails
        );

        $this->assertApiResponse($propertyDetails);
    }

    /**
     * @test
     */
    public function test_read_property_details()
    {
        $propertyDetails = factory(PropertyDetails::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/property_details/'.$propertyDetails->id
        );

        $this->assertApiResponse($propertyDetails->toArray());
    }

    /**
     * @test
     */
    public function test_update_property_details()
    {
        $propertyDetails = factory(PropertyDetails::class)->create();
        $editedPropertyDetails = factory(PropertyDetails::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/property_details/'.$propertyDetails->id,
            $editedPropertyDetails
        );

        $this->assertApiResponse($editedPropertyDetails);
    }

    /**
     * @test
     */
    public function test_delete_property_details()
    {
        $propertyDetails = factory(PropertyDetails::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/property_details/'.$propertyDetails->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/property_details/'.$propertyDetails->id
        );

        $this->response->assertStatus(404);
    }
}
