<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Consultant;

class ConsultantApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_consultant()
    {
        $consultant = factory(Consultant::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/consultants', $consultant
        );

        $this->assertApiResponse($consultant);
    }

    /**
     * @test
     */
    public function test_read_consultant()
    {
        $consultant = factory(Consultant::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/consultants/'.$consultant->id
        );

        $this->assertApiResponse($consultant->toArray());
    }

    /**
     * @test
     */
    public function test_update_consultant()
    {
        $consultant = factory(Consultant::class)->create();
        $editedConsultant = factory(Consultant::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/consultants/'.$consultant->id,
            $editedConsultant
        );

        $this->assertApiResponse($editedConsultant);
    }

    /**
     * @test
     */
    public function test_delete_consultant()
    {
        $consultant = factory(Consultant::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/consultants/'.$consultant->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/consultants/'.$consultant->id
        );

        $this->response->assertStatus(404);
    }
}
