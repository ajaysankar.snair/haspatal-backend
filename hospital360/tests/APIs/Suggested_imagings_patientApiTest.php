<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Suggested_imagings_patient;

class Suggested_imagings_patientApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_suggested_imagings_patient()
    {
        $suggestedImagingsPatient = factory(Suggested_imagings_patient::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/suggested_imagings_patients', $suggestedImagingsPatient
        );

        $this->assertApiResponse($suggestedImagingsPatient);
    }

    /**
     * @test
     */
    public function test_read_suggested_imagings_patient()
    {
        $suggestedImagingsPatient = factory(Suggested_imagings_patient::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/suggested_imagings_patients/'.$suggestedImagingsPatient->id
        );

        $this->assertApiResponse($suggestedImagingsPatient->toArray());
    }

    /**
     * @test
     */
    public function test_update_suggested_imagings_patient()
    {
        $suggestedImagingsPatient = factory(Suggested_imagings_patient::class)->create();
        $editedSuggested_imagings_patient = factory(Suggested_imagings_patient::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/suggested_imagings_patients/'.$suggestedImagingsPatient->id,
            $editedSuggested_imagings_patient
        );

        $this->assertApiResponse($editedSuggested_imagings_patient);
    }

    /**
     * @test
     */
    public function test_delete_suggested_imagings_patient()
    {
        $suggestedImagingsPatient = factory(Suggested_imagings_patient::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/suggested_imagings_patients/'.$suggestedImagingsPatient->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/suggested_imagings_patients/'.$suggestedImagingsPatient->id
        );

        $this->response->assertStatus(404);
    }
}
