<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Create_offer_360;

class Create_offer_360ApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_create_offer_360()
    {
        $createOffer360 = factory(Create_offer_360::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/create_offer_360s', $createOffer360
        );

        $this->assertApiResponse($createOffer360);
    }

    /**
     * @test
     */
    public function test_read_create_offer_360()
    {
        $createOffer360 = factory(Create_offer_360::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/create_offer_360s/'.$createOffer360->id
        );

        $this->assertApiResponse($createOffer360->toArray());
    }

    /**
     * @test
     */
    public function test_update_create_offer_360()
    {
        $createOffer360 = factory(Create_offer_360::class)->create();
        $editedCreate_offer_360 = factory(Create_offer_360::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/create_offer_360s/'.$createOffer360->id,
            $editedCreate_offer_360
        );

        $this->assertApiResponse($editedCreate_offer_360);
    }

    /**
     * @test
     */
    public function test_delete_create_offer_360()
    {
        $createOffer360 = factory(Create_offer_360::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/create_offer_360s/'.$createOffer360->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/create_offer_360s/'.$createOffer360->id
        );

        $this->response->assertStatus(404);
    }
}
