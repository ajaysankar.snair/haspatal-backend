<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\review_360;

class review_360ApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_review_360()
    {
        $review360 = factory(review_360::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/review_360s', $review360
        );

        $this->assertApiResponse($review360);
    }

    /**
     * @test
     */
    public function test_read_review_360()
    {
        $review360 = factory(review_360::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/review_360s/'.$review360->id
        );

        $this->assertApiResponse($review360->toArray());
    }

    /**
     * @test
     */
    public function test_update_review_360()
    {
        $review360 = factory(review_360::class)->create();
        $editedreview_360 = factory(review_360::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/review_360s/'.$review360->id,
            $editedreview_360
        );

        $this->assertApiResponse($editedreview_360);
    }

    /**
     * @test
     */
    public function test_delete_review_360()
    {
        $review360 = factory(review_360::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/review_360s/'.$review360->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/review_360s/'.$review360->id
        );

        $this->response->assertStatus(404);
    }
}
