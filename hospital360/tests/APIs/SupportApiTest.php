<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Support;

class SupportApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_support()
    {
        $support = factory(Support::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/supports', $support
        );

        $this->assertApiResponse($support);
    }

    /**
     * @test
     */
    public function test_read_support()
    {
        $support = factory(Support::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/supports/'.$support->id
        );

        $this->assertApiResponse($support->toArray());
    }

    /**
     * @test
     */
    public function test_update_support()
    {
        $support = factory(Support::class)->create();
        $editedSupport = factory(Support::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/supports/'.$support->id,
            $editedSupport
        );

        $this->assertApiResponse($editedSupport);
    }

    /**
     * @test
     */
    public function test_delete_support()
    {
        $support = factory(Support::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/supports/'.$support->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/supports/'.$support->id
        );

        $this->response->assertStatus(404);
    }
}
