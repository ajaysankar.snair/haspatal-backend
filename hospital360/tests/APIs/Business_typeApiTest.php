<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Business_type;

class Business_typeApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_business_type()
    {
        $businessType = factory(Business_type::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/business_types', $businessType
        );

        $this->assertApiResponse($businessType);
    }

    /**
     * @test
     */
    public function test_read_business_type()
    {
        $businessType = factory(Business_type::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/business_types/'.$businessType->id
        );

        $this->assertApiResponse($businessType->toArray());
    }

    /**
     * @test
     */
    public function test_update_business_type()
    {
        $businessType = factory(Business_type::class)->create();
        $editedBusiness_type = factory(Business_type::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/business_types/'.$businessType->id,
            $editedBusiness_type
        );

        $this->assertApiResponse($editedBusiness_type);
    }

    /**
     * @test
     */
    public function test_delete_business_type()
    {
        $businessType = factory(Business_type::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/business_types/'.$businessType->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/business_types/'.$businessType->id
        );

        $this->response->assertStatus(404);
    }
}
