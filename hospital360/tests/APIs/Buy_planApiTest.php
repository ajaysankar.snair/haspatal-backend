<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Buy_plan;

class Buy_planApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_buy_plan()
    {
        $buyPlan = factory(Buy_plan::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/buy_plans', $buyPlan
        );

        $this->assertApiResponse($buyPlan);
    }

    /**
     * @test
     */
    public function test_read_buy_plan()
    {
        $buyPlan = factory(Buy_plan::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/buy_plans/'.$buyPlan->id
        );

        $this->assertApiResponse($buyPlan->toArray());
    }

    /**
     * @test
     */
    public function test_update_buy_plan()
    {
        $buyPlan = factory(Buy_plan::class)->create();
        $editedBuy_plan = factory(Buy_plan::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/buy_plans/'.$buyPlan->id,
            $editedBuy_plan
        );

        $this->assertApiResponse($editedBuy_plan);
    }

    /**
     * @test
     */
    public function test_delete_buy_plan()
    {
        $buyPlan = factory(Buy_plan::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/buy_plans/'.$buyPlan->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/buy_plans/'.$buyPlan->id
        );

        $this->response->assertStatus(404);
    }
}
