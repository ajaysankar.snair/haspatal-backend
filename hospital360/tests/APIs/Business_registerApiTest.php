<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Business_register;

class Business_registerApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_business_register()
    {
        $businessRegister = factory(Business_register::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/business_registers', $businessRegister
        );

        $this->assertApiResponse($businessRegister);
    }

    /**
     * @test
     */
    public function test_read_business_register()
    {
        $businessRegister = factory(Business_register::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/business_registers/'.$businessRegister->id
        );

        $this->assertApiResponse($businessRegister->toArray());
    }

    /**
     * @test
     */
    public function test_update_business_register()
    {
        $businessRegister = factory(Business_register::class)->create();
        $editedBusiness_register = factory(Business_register::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/business_registers/'.$businessRegister->id,
            $editedBusiness_register
        );

        $this->assertApiResponse($editedBusiness_register);
    }

    /**
     * @test
     */
    public function test_delete_business_register()
    {
        $businessRegister = factory(Business_register::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/business_registers/'.$businessRegister->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/business_registers/'.$businessRegister->id
        );

        $this->response->assertStatus(404);
    }
}
