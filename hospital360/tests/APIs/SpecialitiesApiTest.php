<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Specialities;

class SpecialitiesApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_specialities()
    {
        $specialities = factory(Specialities::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/specialities', $specialities
        );

        $this->assertApiResponse($specialities);
    }

    /**
     * @test
     */
    public function test_read_specialities()
    {
        $specialities = factory(Specialities::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/specialities/'.$specialities->id
        );

        $this->assertApiResponse($specialities->toArray());
    }

    /**
     * @test
     */
    public function test_update_specialities()
    {
        $specialities = factory(Specialities::class)->create();
        $editedSpecialities = factory(Specialities::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/specialities/'.$specialities->id,
            $editedSpecialities
        );

        $this->assertApiResponse($editedSpecialities);
    }

    /**
     * @test
     */
    public function test_delete_specialities()
    {
        $specialities = factory(Specialities::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/specialities/'.$specialities->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/specialities/'.$specialities->id
        );

        $this->response->assertStatus(404);
    }
}
