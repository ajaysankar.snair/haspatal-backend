<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Present_medications_patient;

class Present_medications_patientApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_present_medications_patient()
    {
        $presentMedicationsPatient = factory(Present_medications_patient::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/present_medications_patients', $presentMedicationsPatient
        );

        $this->assertApiResponse($presentMedicationsPatient);
    }

    /**
     * @test
     */
    public function test_read_present_medications_patient()
    {
        $presentMedicationsPatient = factory(Present_medications_patient::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/present_medications_patients/'.$presentMedicationsPatient->id
        );

        $this->assertApiResponse($presentMedicationsPatient->toArray());
    }

    /**
     * @test
     */
    public function test_update_present_medications_patient()
    {
        $presentMedicationsPatient = factory(Present_medications_patient::class)->create();
        $editedPresent_medications_patient = factory(Present_medications_patient::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/present_medications_patients/'.$presentMedicationsPatient->id,
            $editedPresent_medications_patient
        );

        $this->assertApiResponse($editedPresent_medications_patient);
    }

    /**
     * @test
     */
    public function test_delete_present_medications_patient()
    {
        $presentMedicationsPatient = factory(Present_medications_patient::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/present_medications_patients/'.$presentMedicationsPatient->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/present_medications_patients/'.$presentMedicationsPatient->id
        );

        $this->response->assertStatus(404);
    }
}
