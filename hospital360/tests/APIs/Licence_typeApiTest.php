<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Licence_type;

class Licence_typeApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_licence_type()
    {
        $licenceType = factory(Licence_type::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/licence_types', $licenceType
        );

        $this->assertApiResponse($licenceType);
    }

    /**
     * @test
     */
    public function test_read_licence_type()
    {
        $licenceType = factory(Licence_type::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/licence_types/'.$licenceType->id
        );

        $this->assertApiResponse($licenceType->toArray());
    }

    /**
     * @test
     */
    public function test_update_licence_type()
    {
        $licenceType = factory(Licence_type::class)->create();
        $editedLicence_type = factory(Licence_type::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/licence_types/'.$licenceType->id,
            $editedLicence_type
        );

        $this->assertApiResponse($editedLicence_type);
    }

    /**
     * @test
     */
    public function test_delete_licence_type()
    {
        $licenceType = factory(Licence_type::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/licence_types/'.$licenceType->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/licence_types/'.$licenceType->id
        );

        $this->response->assertStatus(404);
    }
}
