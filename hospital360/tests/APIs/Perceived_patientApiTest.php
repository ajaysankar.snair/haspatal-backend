<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Perceived_patient;

class Perceived_patientApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_perceived_patient()
    {
        $perceivedPatient = factory(Perceived_patient::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/perceived_patients', $perceivedPatient
        );

        $this->assertApiResponse($perceivedPatient);
    }

    /**
     * @test
     */
    public function test_read_perceived_patient()
    {
        $perceivedPatient = factory(Perceived_patient::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/perceived_patients/'.$perceivedPatient->id
        );

        $this->assertApiResponse($perceivedPatient->toArray());
    }

    /**
     * @test
     */
    public function test_update_perceived_patient()
    {
        $perceivedPatient = factory(Perceived_patient::class)->create();
        $editedPerceived_patient = factory(Perceived_patient::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/perceived_patients/'.$perceivedPatient->id,
            $editedPerceived_patient
        );

        $this->assertApiResponse($editedPerceived_patient);
    }

    /**
     * @test
     */
    public function test_delete_perceived_patient()
    {
        $perceivedPatient = factory(Perceived_patient::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/perceived_patients/'.$perceivedPatient->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/perceived_patients/'.$perceivedPatient->id
        );

        $this->response->assertStatus(404);
    }
}
