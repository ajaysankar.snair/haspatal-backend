<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Booking_price_admin;

class Booking_price_adminApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_booking_price_admin()
    {
        $bookingPriceAdmin = factory(Booking_price_admin::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/booking_price_admins', $bookingPriceAdmin
        );

        $this->assertApiResponse($bookingPriceAdmin);
    }

    /**
     * @test
     */
    public function test_read_booking_price_admin()
    {
        $bookingPriceAdmin = factory(Booking_price_admin::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/booking_price_admins/'.$bookingPriceAdmin->id
        );

        $this->assertApiResponse($bookingPriceAdmin->toArray());
    }

    /**
     * @test
     */
    public function test_update_booking_price_admin()
    {
        $bookingPriceAdmin = factory(Booking_price_admin::class)->create();
        $editedBooking_price_admin = factory(Booking_price_admin::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/booking_price_admins/'.$bookingPriceAdmin->id,
            $editedBooking_price_admin
        );

        $this->assertApiResponse($editedBooking_price_admin);
    }

    /**
     * @test
     */
    public function test_delete_booking_price_admin()
    {
        $bookingPriceAdmin = factory(Booking_price_admin::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/booking_price_admins/'.$bookingPriceAdmin->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/booking_price_admins/'.$bookingPriceAdmin->id
        );

        $this->response->assertStatus(404);
    }
}
