<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Suggested_therapies_patient;

class Suggested_therapies_patientApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_suggested_therapies_patient()
    {
        $suggestedTherapiesPatient = factory(Suggested_therapies_patient::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/suggested_therapies_patients', $suggestedTherapiesPatient
        );

        $this->assertApiResponse($suggestedTherapiesPatient);
    }

    /**
     * @test
     */
    public function test_read_suggested_therapies_patient()
    {
        $suggestedTherapiesPatient = factory(Suggested_therapies_patient::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/suggested_therapies_patients/'.$suggestedTherapiesPatient->id
        );

        $this->assertApiResponse($suggestedTherapiesPatient->toArray());
    }

    /**
     * @test
     */
    public function test_update_suggested_therapies_patient()
    {
        $suggestedTherapiesPatient = factory(Suggested_therapies_patient::class)->create();
        $editedSuggested_therapies_patient = factory(Suggested_therapies_patient::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/suggested_therapies_patients/'.$suggestedTherapiesPatient->id,
            $editedSuggested_therapies_patient
        );

        $this->assertApiResponse($editedSuggested_therapies_patient);
    }

    /**
     * @test
     */
    public function test_delete_suggested_therapies_patient()
    {
        $suggestedTherapiesPatient = factory(Suggested_therapies_patient::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/suggested_therapies_patients/'.$suggestedTherapiesPatient->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/suggested_therapies_patients/'.$suggestedTherapiesPatient->id
        );

        $this->response->assertStatus(404);
    }
}
