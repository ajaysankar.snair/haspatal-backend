<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Bank_information;

class Bank_informationApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_bank_information()
    {
        $bankInformation = factory(Bank_information::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/bank_informations', $bankInformation
        );

        $this->assertApiResponse($bankInformation);
    }

    /**
     * @test
     */
    public function test_read_bank_information()
    {
        $bankInformation = factory(Bank_information::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/bank_informations/'.$bankInformation->id
        );

        $this->assertApiResponse($bankInformation->toArray());
    }

    /**
     * @test
     */
    public function test_update_bank_information()
    {
        $bankInformation = factory(Bank_information::class)->create();
        $editedBank_information = factory(Bank_information::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/bank_informations/'.$bankInformation->id,
            $editedBank_information
        );

        $this->assertApiResponse($editedBank_information);
    }

    /**
     * @test
     */
    public function test_delete_bank_information()
    {
        $bankInformation = factory(Bank_information::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/bank_informations/'.$bankInformation->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/bank_informations/'.$bankInformation->id
        );

        $this->response->assertStatus(404);
    }
}
