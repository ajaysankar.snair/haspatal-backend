<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Wallet_360_transaction;

class Wallet_360_transactionApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_wallet_360_transaction()
    {
        $wallet360Transaction = factory(Wallet_360_transaction::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/wallet_360_transactions', $wallet360Transaction
        );

        $this->assertApiResponse($wallet360Transaction);
    }

    /**
     * @test
     */
    public function test_read_wallet_360_transaction()
    {
        $wallet360Transaction = factory(Wallet_360_transaction::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/wallet_360_transactions/'.$wallet360Transaction->id
        );

        $this->assertApiResponse($wallet360Transaction->toArray());
    }

    /**
     * @test
     */
    public function test_update_wallet_360_transaction()
    {
        $wallet360Transaction = factory(Wallet_360_transaction::class)->create();
        $editedWallet_360_transaction = factory(Wallet_360_transaction::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/wallet_360_transactions/'.$wallet360Transaction->id,
            $editedWallet_360_transaction
        );

        $this->assertApiResponse($editedWallet_360_transaction);
    }

    /**
     * @test
     */
    public function test_delete_wallet_360_transaction()
    {
        $wallet360Transaction = factory(Wallet_360_transaction::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/wallet_360_transactions/'.$wallet360Transaction->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/wallet_360_transactions/'.$wallet360Transaction->id
        );

        $this->response->assertStatus(404);
    }
}
