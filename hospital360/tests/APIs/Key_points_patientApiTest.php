<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Key_points_patient;

class Key_points_patientApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_key_points_patient()
    {
        $keyPointsPatient = factory(Key_points_patient::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/key_points_patients', $keyPointsPatient
        );

        $this->assertApiResponse($keyPointsPatient);
    }

    /**
     * @test
     */
    public function test_read_key_points_patient()
    {
        $keyPointsPatient = factory(Key_points_patient::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/key_points_patients/'.$keyPointsPatient->id
        );

        $this->assertApiResponse($keyPointsPatient->toArray());
    }

    /**
     * @test
     */
    public function test_update_key_points_patient()
    {
        $keyPointsPatient = factory(Key_points_patient::class)->create();
        $editedKey_points_patient = factory(Key_points_patient::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/key_points_patients/'.$keyPointsPatient->id,
            $editedKey_points_patient
        );

        $this->assertApiResponse($editedKey_points_patient);
    }

    /**
     * @test
     */
    public function test_delete_key_points_patient()
    {
        $keyPointsPatient = factory(Key_points_patient::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/key_points_patients/'.$keyPointsPatient->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/key_points_patients/'.$keyPointsPatient->id
        );

        $this->response->assertStatus(404);
    }
}
