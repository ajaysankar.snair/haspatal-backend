<?php namespace Tests\Repositories;

use App\Models\Order_query;
use App\Repositories\Order_queryRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class Order_queryRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var Order_queryRepository
     */
    protected $orderQueryRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->orderQueryRepo = \App::make(Order_queryRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_order_query()
    {
        $orderQuery = factory(Order_query::class)->make()->toArray();

        $createdOrder_query = $this->orderQueryRepo->create($orderQuery);

        $createdOrder_query = $createdOrder_query->toArray();
        $this->assertArrayHasKey('id', $createdOrder_query);
        $this->assertNotNull($createdOrder_query['id'], 'Created Order_query must have id specified');
        $this->assertNotNull(Order_query::find($createdOrder_query['id']), 'Order_query with given id must be in DB');
        $this->assertModelData($orderQuery, $createdOrder_query);
    }

    /**
     * @test read
     */
    public function test_read_order_query()
    {
        $orderQuery = factory(Order_query::class)->create();

        $dbOrder_query = $this->orderQueryRepo->find($orderQuery->id);

        $dbOrder_query = $dbOrder_query->toArray();
        $this->assertModelData($orderQuery->toArray(), $dbOrder_query);
    }

    /**
     * @test update
     */
    public function test_update_order_query()
    {
        $orderQuery = factory(Order_query::class)->create();
        $fakeOrder_query = factory(Order_query::class)->make()->toArray();

        $updatedOrder_query = $this->orderQueryRepo->update($fakeOrder_query, $orderQuery->id);

        $this->assertModelData($fakeOrder_query, $updatedOrder_query->toArray());
        $dbOrder_query = $this->orderQueryRepo->find($orderQuery->id);
        $this->assertModelData($fakeOrder_query, $dbOrder_query->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_order_query()
    {
        $orderQuery = factory(Order_query::class)->create();

        $resp = $this->orderQueryRepo->delete($orderQuery->id);

        $this->assertTrue($resp);
        $this->assertNull(Order_query::find($orderQuery->id), 'Order_query should not exist in DB');
    }
}
