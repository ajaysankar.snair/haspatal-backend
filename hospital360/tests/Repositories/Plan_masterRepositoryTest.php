<?php namespace Tests\Repositories;

use App\Models\Plan_master;
use App\Repositories\Plan_masterRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class Plan_masterRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var Plan_masterRepository
     */
    protected $planMasterRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->planMasterRepo = \App::make(Plan_masterRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_plan_master()
    {
        $planMaster = factory(Plan_master::class)->make()->toArray();

        $createdPlan_master = $this->planMasterRepo->create($planMaster);

        $createdPlan_master = $createdPlan_master->toArray();
        $this->assertArrayHasKey('id', $createdPlan_master);
        $this->assertNotNull($createdPlan_master['id'], 'Created Plan_master must have id specified');
        $this->assertNotNull(Plan_master::find($createdPlan_master['id']), 'Plan_master with given id must be in DB');
        $this->assertModelData($planMaster, $createdPlan_master);
    }

    /**
     * @test read
     */
    public function test_read_plan_master()
    {
        $planMaster = factory(Plan_master::class)->create();

        $dbPlan_master = $this->planMasterRepo->find($planMaster->id);

        $dbPlan_master = $dbPlan_master->toArray();
        $this->assertModelData($planMaster->toArray(), $dbPlan_master);
    }

    /**
     * @test update
     */
    public function test_update_plan_master()
    {
        $planMaster = factory(Plan_master::class)->create();
        $fakePlan_master = factory(Plan_master::class)->make()->toArray();

        $updatedPlan_master = $this->planMasterRepo->update($fakePlan_master, $planMaster->id);

        $this->assertModelData($fakePlan_master, $updatedPlan_master->toArray());
        $dbPlan_master = $this->planMasterRepo->find($planMaster->id);
        $this->assertModelData($fakePlan_master, $dbPlan_master->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_plan_master()
    {
        $planMaster = factory(Plan_master::class)->create();

        $resp = $this->planMasterRepo->delete($planMaster->id);

        $this->assertTrue($resp);
        $this->assertNull(Plan_master::find($planMaster->id), 'Plan_master should not exist in DB');
    }
}
