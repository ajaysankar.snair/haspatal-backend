<?php namespace Tests\Repositories;

use App\Models\Create_offer_360;
use App\Repositories\Create_offer_360Repository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class Create_offer_360RepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var Create_offer_360Repository
     */
    protected $createOffer360Repo;

    public function setUp() : void
    {
        parent::setUp();
        $this->createOffer360Repo = \App::make(Create_offer_360Repository::class);
    }

    /**
     * @test create
     */
    public function test_create_create_offer_360()
    {
        $createOffer360 = factory(Create_offer_360::class)->make()->toArray();

        $createdCreate_offer_360 = $this->createOffer360Repo->create($createOffer360);

        $createdCreate_offer_360 = $createdCreate_offer_360->toArray();
        $this->assertArrayHasKey('id', $createdCreate_offer_360);
        $this->assertNotNull($createdCreate_offer_360['id'], 'Created Create_offer_360 must have id specified');
        $this->assertNotNull(Create_offer_360::find($createdCreate_offer_360['id']), 'Create_offer_360 with given id must be in DB');
        $this->assertModelData($createOffer360, $createdCreate_offer_360);
    }

    /**
     * @test read
     */
    public function test_read_create_offer_360()
    {
        $createOffer360 = factory(Create_offer_360::class)->create();

        $dbCreate_offer_360 = $this->createOffer360Repo->find($createOffer360->id);

        $dbCreate_offer_360 = $dbCreate_offer_360->toArray();
        $this->assertModelData($createOffer360->toArray(), $dbCreate_offer_360);
    }

    /**
     * @test update
     */
    public function test_update_create_offer_360()
    {
        $createOffer360 = factory(Create_offer_360::class)->create();
        $fakeCreate_offer_360 = factory(Create_offer_360::class)->make()->toArray();

        $updatedCreate_offer_360 = $this->createOffer360Repo->update($fakeCreate_offer_360, $createOffer360->id);

        $this->assertModelData($fakeCreate_offer_360, $updatedCreate_offer_360->toArray());
        $dbCreate_offer_360 = $this->createOffer360Repo->find($createOffer360->id);
        $this->assertModelData($fakeCreate_offer_360, $dbCreate_offer_360->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_create_offer_360()
    {
        $createOffer360 = factory(Create_offer_360::class)->create();

        $resp = $this->createOffer360Repo->delete($createOffer360->id);

        $this->assertTrue($resp);
        $this->assertNull(Create_offer_360::find($createOffer360->id), 'Create_offer_360 should not exist in DB');
    }
}
