<?php namespace Tests\Repositories;

use App\Models\Present_medications_patient;
use App\Repositories\Present_medications_patientRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class Present_medications_patientRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var Present_medications_patientRepository
     */
    protected $presentMedicationsPatientRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->presentMedicationsPatientRepo = \App::make(Present_medications_patientRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_present_medications_patient()
    {
        $presentMedicationsPatient = factory(Present_medications_patient::class)->make()->toArray();

        $createdPresent_medications_patient = $this->presentMedicationsPatientRepo->create($presentMedicationsPatient);

        $createdPresent_medications_patient = $createdPresent_medications_patient->toArray();
        $this->assertArrayHasKey('id', $createdPresent_medications_patient);
        $this->assertNotNull($createdPresent_medications_patient['id'], 'Created Present_medications_patient must have id specified');
        $this->assertNotNull(Present_medications_patient::find($createdPresent_medications_patient['id']), 'Present_medications_patient with given id must be in DB');
        $this->assertModelData($presentMedicationsPatient, $createdPresent_medications_patient);
    }

    /**
     * @test read
     */
    public function test_read_present_medications_patient()
    {
        $presentMedicationsPatient = factory(Present_medications_patient::class)->create();

        $dbPresent_medications_patient = $this->presentMedicationsPatientRepo->find($presentMedicationsPatient->id);

        $dbPresent_medications_patient = $dbPresent_medications_patient->toArray();
        $this->assertModelData($presentMedicationsPatient->toArray(), $dbPresent_medications_patient);
    }

    /**
     * @test update
     */
    public function test_update_present_medications_patient()
    {
        $presentMedicationsPatient = factory(Present_medications_patient::class)->create();
        $fakePresent_medications_patient = factory(Present_medications_patient::class)->make()->toArray();

        $updatedPresent_medications_patient = $this->presentMedicationsPatientRepo->update($fakePresent_medications_patient, $presentMedicationsPatient->id);

        $this->assertModelData($fakePresent_medications_patient, $updatedPresent_medications_patient->toArray());
        $dbPresent_medications_patient = $this->presentMedicationsPatientRepo->find($presentMedicationsPatient->id);
        $this->assertModelData($fakePresent_medications_patient, $dbPresent_medications_patient->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_present_medications_patient()
    {
        $presentMedicationsPatient = factory(Present_medications_patient::class)->create();

        $resp = $this->presentMedicationsPatientRepo->delete($presentMedicationsPatient->id);

        $this->assertTrue($resp);
        $this->assertNull(Present_medications_patient::find($presentMedicationsPatient->id), 'Present_medications_patient should not exist in DB');
    }
}
