<?php namespace Tests\Repositories;

use App\Models\Suggested_specialists_patient;
use App\Repositories\Suggested_specialists_patientRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class Suggested_specialists_patientRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var Suggested_specialists_patientRepository
     */
    protected $suggestedSpecialistsPatientRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->suggestedSpecialistsPatientRepo = \App::make(Suggested_specialists_patientRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_suggested_specialists_patient()
    {
        $suggestedSpecialistsPatient = factory(Suggested_specialists_patient::class)->make()->toArray();

        $createdSuggested_specialists_patient = $this->suggestedSpecialistsPatientRepo->create($suggestedSpecialistsPatient);

        $createdSuggested_specialists_patient = $createdSuggested_specialists_patient->toArray();
        $this->assertArrayHasKey('id', $createdSuggested_specialists_patient);
        $this->assertNotNull($createdSuggested_specialists_patient['id'], 'Created Suggested_specialists_patient must have id specified');
        $this->assertNotNull(Suggested_specialists_patient::find($createdSuggested_specialists_patient['id']), 'Suggested_specialists_patient with given id must be in DB');
        $this->assertModelData($suggestedSpecialistsPatient, $createdSuggested_specialists_patient);
    }

    /**
     * @test read
     */
    public function test_read_suggested_specialists_patient()
    {
        $suggestedSpecialistsPatient = factory(Suggested_specialists_patient::class)->create();

        $dbSuggested_specialists_patient = $this->suggestedSpecialistsPatientRepo->find($suggestedSpecialistsPatient->id);

        $dbSuggested_specialists_patient = $dbSuggested_specialists_patient->toArray();
        $this->assertModelData($suggestedSpecialistsPatient->toArray(), $dbSuggested_specialists_patient);
    }

    /**
     * @test update
     */
    public function test_update_suggested_specialists_patient()
    {
        $suggestedSpecialistsPatient = factory(Suggested_specialists_patient::class)->create();
        $fakeSuggested_specialists_patient = factory(Suggested_specialists_patient::class)->make()->toArray();

        $updatedSuggested_specialists_patient = $this->suggestedSpecialistsPatientRepo->update($fakeSuggested_specialists_patient, $suggestedSpecialistsPatient->id);

        $this->assertModelData($fakeSuggested_specialists_patient, $updatedSuggested_specialists_patient->toArray());
        $dbSuggested_specialists_patient = $this->suggestedSpecialistsPatientRepo->find($suggestedSpecialistsPatient->id);
        $this->assertModelData($fakeSuggested_specialists_patient, $dbSuggested_specialists_patient->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_suggested_specialists_patient()
    {
        $suggestedSpecialistsPatient = factory(Suggested_specialists_patient::class)->create();

        $resp = $this->suggestedSpecialistsPatientRepo->delete($suggestedSpecialistsPatient->id);

        $this->assertTrue($resp);
        $this->assertNull(Suggested_specialists_patient::find($suggestedSpecialistsPatient->id), 'Suggested_specialists_patient should not exist in DB');
    }
}
