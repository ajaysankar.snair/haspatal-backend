<?php namespace Tests\Repositories;

use App\Models\Available_lab_patient;
use App\Repositories\Available_lab_patientRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class Available_lab_patientRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var Available_lab_patientRepository
     */
    protected $availableLabPatientRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->availableLabPatientRepo = \App::make(Available_lab_patientRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_available_lab_patient()
    {
        $availableLabPatient = factory(Available_lab_patient::class)->make()->toArray();

        $createdAvailable_lab_patient = $this->availableLabPatientRepo->create($availableLabPatient);

        $createdAvailable_lab_patient = $createdAvailable_lab_patient->toArray();
        $this->assertArrayHasKey('id', $createdAvailable_lab_patient);
        $this->assertNotNull($createdAvailable_lab_patient['id'], 'Created Available_lab_patient must have id specified');
        $this->assertNotNull(Available_lab_patient::find($createdAvailable_lab_patient['id']), 'Available_lab_patient with given id must be in DB');
        $this->assertModelData($availableLabPatient, $createdAvailable_lab_patient);
    }

    /**
     * @test read
     */
    public function test_read_available_lab_patient()
    {
        $availableLabPatient = factory(Available_lab_patient::class)->create();

        $dbAvailable_lab_patient = $this->availableLabPatientRepo->find($availableLabPatient->id);

        $dbAvailable_lab_patient = $dbAvailable_lab_patient->toArray();
        $this->assertModelData($availableLabPatient->toArray(), $dbAvailable_lab_patient);
    }

    /**
     * @test update
     */
    public function test_update_available_lab_patient()
    {
        $availableLabPatient = factory(Available_lab_patient::class)->create();
        $fakeAvailable_lab_patient = factory(Available_lab_patient::class)->make()->toArray();

        $updatedAvailable_lab_patient = $this->availableLabPatientRepo->update($fakeAvailable_lab_patient, $availableLabPatient->id);

        $this->assertModelData($fakeAvailable_lab_patient, $updatedAvailable_lab_patient->toArray());
        $dbAvailable_lab_patient = $this->availableLabPatientRepo->find($availableLabPatient->id);
        $this->assertModelData($fakeAvailable_lab_patient, $dbAvailable_lab_patient->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_available_lab_patient()
    {
        $availableLabPatient = factory(Available_lab_patient::class)->create();

        $resp = $this->availableLabPatientRepo->delete($availableLabPatient->id);

        $this->assertTrue($resp);
        $this->assertNull(Available_lab_patient::find($availableLabPatient->id), 'Available_lab_patient should not exist in DB');
    }
}
