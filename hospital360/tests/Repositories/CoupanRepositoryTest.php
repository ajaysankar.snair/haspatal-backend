<?php namespace Tests\Repositories;

use App\Models\Coupan;
use App\Repositories\CoupanRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class CoupanRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var CoupanRepository
     */
    protected $coupanRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->coupanRepo = \App::make(CoupanRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_coupan()
    {
        $coupan = factory(Coupan::class)->make()->toArray();

        $createdCoupan = $this->coupanRepo->create($coupan);

        $createdCoupan = $createdCoupan->toArray();
        $this->assertArrayHasKey('id', $createdCoupan);
        $this->assertNotNull($createdCoupan['id'], 'Created Coupan must have id specified');
        $this->assertNotNull(Coupan::find($createdCoupan['id']), 'Coupan with given id must be in DB');
        $this->assertModelData($coupan, $createdCoupan);
    }

    /**
     * @test read
     */
    public function test_read_coupan()
    {
        $coupan = factory(Coupan::class)->create();

        $dbCoupan = $this->coupanRepo->find($coupan->id);

        $dbCoupan = $dbCoupan->toArray();
        $this->assertModelData($coupan->toArray(), $dbCoupan);
    }

    /**
     * @test update
     */
    public function test_update_coupan()
    {
        $coupan = factory(Coupan::class)->create();
        $fakeCoupan = factory(Coupan::class)->make()->toArray();

        $updatedCoupan = $this->coupanRepo->update($fakeCoupan, $coupan->id);

        $this->assertModelData($fakeCoupan, $updatedCoupan->toArray());
        $dbCoupan = $this->coupanRepo->find($coupan->id);
        $this->assertModelData($fakeCoupan, $dbCoupan->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_coupan()
    {
        $coupan = factory(Coupan::class)->create();

        $resp = $this->coupanRepo->delete($coupan->id);

        $this->assertTrue($resp);
        $this->assertNull(Coupan::find($coupan->id), 'Coupan should not exist in DB');
    }
}
