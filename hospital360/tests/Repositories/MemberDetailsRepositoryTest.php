<?php namespace Tests\Repositories;

use App\Models\MemberDetails;
use App\Repositories\MemberDetailsRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class MemberDetailsRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var MemberDetailsRepository
     */
    protected $memberDetailsRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->memberDetailsRepo = \App::make(MemberDetailsRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_member_details()
    {
        $memberDetails = factory(MemberDetails::class)->make()->toArray();

        $createdMemberDetails = $this->memberDetailsRepo->create($memberDetails);

        $createdMemberDetails = $createdMemberDetails->toArray();
        $this->assertArrayHasKey('id', $createdMemberDetails);
        $this->assertNotNull($createdMemberDetails['id'], 'Created MemberDetails must have id specified');
        $this->assertNotNull(MemberDetails::find($createdMemberDetails['id']), 'MemberDetails with given id must be in DB');
        $this->assertModelData($memberDetails, $createdMemberDetails);
    }

    /**
     * @test read
     */
    public function test_read_member_details()
    {
        $memberDetails = factory(MemberDetails::class)->create();

        $dbMemberDetails = $this->memberDetailsRepo->find($memberDetails->id);

        $dbMemberDetails = $dbMemberDetails->toArray();
        $this->assertModelData($memberDetails->toArray(), $dbMemberDetails);
    }

    /**
     * @test update
     */
    public function test_update_member_details()
    {
        $memberDetails = factory(MemberDetails::class)->create();
        $fakeMemberDetails = factory(MemberDetails::class)->make()->toArray();

        $updatedMemberDetails = $this->memberDetailsRepo->update($fakeMemberDetails, $memberDetails->id);

        $this->assertModelData($fakeMemberDetails, $updatedMemberDetails->toArray());
        $dbMemberDetails = $this->memberDetailsRepo->find($memberDetails->id);
        $this->assertModelData($fakeMemberDetails, $dbMemberDetails->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_member_details()
    {
        $memberDetails = factory(MemberDetails::class)->create();

        $resp = $this->memberDetailsRepo->delete($memberDetails->id);

        $this->assertTrue($resp);
        $this->assertNull(MemberDetails::find($memberDetails->id), 'MemberDetails should not exist in DB');
    }
}
