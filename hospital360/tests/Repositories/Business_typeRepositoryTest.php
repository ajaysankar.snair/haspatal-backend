<?php namespace Tests\Repositories;

use App\Models\Business_type;
use App\Repositories\Business_typeRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class Business_typeRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var Business_typeRepository
     */
    protected $businessTypeRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->businessTypeRepo = \App::make(Business_typeRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_business_type()
    {
        $businessType = factory(Business_type::class)->make()->toArray();

        $createdBusiness_type = $this->businessTypeRepo->create($businessType);

        $createdBusiness_type = $createdBusiness_type->toArray();
        $this->assertArrayHasKey('id', $createdBusiness_type);
        $this->assertNotNull($createdBusiness_type['id'], 'Created Business_type must have id specified');
        $this->assertNotNull(Business_type::find($createdBusiness_type['id']), 'Business_type with given id must be in DB');
        $this->assertModelData($businessType, $createdBusiness_type);
    }

    /**
     * @test read
     */
    public function test_read_business_type()
    {
        $businessType = factory(Business_type::class)->create();

        $dbBusiness_type = $this->businessTypeRepo->find($businessType->id);

        $dbBusiness_type = $dbBusiness_type->toArray();
        $this->assertModelData($businessType->toArray(), $dbBusiness_type);
    }

    /**
     * @test update
     */
    public function test_update_business_type()
    {
        $businessType = factory(Business_type::class)->create();
        $fakeBusiness_type = factory(Business_type::class)->make()->toArray();

        $updatedBusiness_type = $this->businessTypeRepo->update($fakeBusiness_type, $businessType->id);

        $this->assertModelData($fakeBusiness_type, $updatedBusiness_type->toArray());
        $dbBusiness_type = $this->businessTypeRepo->find($businessType->id);
        $this->assertModelData($fakeBusiness_type, $dbBusiness_type->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_business_type()
    {
        $businessType = factory(Business_type::class)->create();

        $resp = $this->businessTypeRepo->delete($businessType->id);

        $this->assertTrue($resp);
        $this->assertNull(Business_type::find($businessType->id), 'Business_type should not exist in DB');
    }
}
