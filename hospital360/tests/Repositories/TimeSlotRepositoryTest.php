<?php namespace Tests\Repositories;

use App\Models\TimeSlot;
use App\Repositories\TimeSlotRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class TimeSlotRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var TimeSlotRepository
     */
    protected $timeSlotRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->timeSlotRepo = \App::make(TimeSlotRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_time_slot()
    {
        $timeSlot = factory(TimeSlot::class)->make()->toArray();

        $createdTimeSlot = $this->timeSlotRepo->create($timeSlot);

        $createdTimeSlot = $createdTimeSlot->toArray();
        $this->assertArrayHasKey('id', $createdTimeSlot);
        $this->assertNotNull($createdTimeSlot['id'], 'Created TimeSlot must have id specified');
        $this->assertNotNull(TimeSlot::find($createdTimeSlot['id']), 'TimeSlot with given id must be in DB');
        $this->assertModelData($timeSlot, $createdTimeSlot);
    }

    /**
     * @test read
     */
    public function test_read_time_slot()
    {
        $timeSlot = factory(TimeSlot::class)->create();

        $dbTimeSlot = $this->timeSlotRepo->find($timeSlot->id);

        $dbTimeSlot = $dbTimeSlot->toArray();
        $this->assertModelData($timeSlot->toArray(), $dbTimeSlot);
    }

    /**
     * @test update
     */
    public function test_update_time_slot()
    {
        $timeSlot = factory(TimeSlot::class)->create();
        $fakeTimeSlot = factory(TimeSlot::class)->make()->toArray();

        $updatedTimeSlot = $this->timeSlotRepo->update($fakeTimeSlot, $timeSlot->id);

        $this->assertModelData($fakeTimeSlot, $updatedTimeSlot->toArray());
        $dbTimeSlot = $this->timeSlotRepo->find($timeSlot->id);
        $this->assertModelData($fakeTimeSlot, $dbTimeSlot->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_time_slot()
    {
        $timeSlot = factory(TimeSlot::class)->create();

        $resp = $this->timeSlotRepo->delete($timeSlot->id);

        $this->assertTrue($resp);
        $this->assertNull(TimeSlot::find($timeSlot->id), 'TimeSlot should not exist in DB');
    }
}
