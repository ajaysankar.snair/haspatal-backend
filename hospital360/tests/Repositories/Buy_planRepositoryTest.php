<?php namespace Tests\Repositories;

use App\Models\Buy_plan;
use App\Repositories\Buy_planRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class Buy_planRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var Buy_planRepository
     */
    protected $buyPlanRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->buyPlanRepo = \App::make(Buy_planRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_buy_plan()
    {
        $buyPlan = factory(Buy_plan::class)->make()->toArray();

        $createdBuy_plan = $this->buyPlanRepo->create($buyPlan);

        $createdBuy_plan = $createdBuy_plan->toArray();
        $this->assertArrayHasKey('id', $createdBuy_plan);
        $this->assertNotNull($createdBuy_plan['id'], 'Created Buy_plan must have id specified');
        $this->assertNotNull(Buy_plan::find($createdBuy_plan['id']), 'Buy_plan with given id must be in DB');
        $this->assertModelData($buyPlan, $createdBuy_plan);
    }

    /**
     * @test read
     */
    public function test_read_buy_plan()
    {
        $buyPlan = factory(Buy_plan::class)->create();

        $dbBuy_plan = $this->buyPlanRepo->find($buyPlan->id);

        $dbBuy_plan = $dbBuy_plan->toArray();
        $this->assertModelData($buyPlan->toArray(), $dbBuy_plan);
    }

    /**
     * @test update
     */
    public function test_update_buy_plan()
    {
        $buyPlan = factory(Buy_plan::class)->create();
        $fakeBuy_plan = factory(Buy_plan::class)->make()->toArray();

        $updatedBuy_plan = $this->buyPlanRepo->update($fakeBuy_plan, $buyPlan->id);

        $this->assertModelData($fakeBuy_plan, $updatedBuy_plan->toArray());
        $dbBuy_plan = $this->buyPlanRepo->find($buyPlan->id);
        $this->assertModelData($fakeBuy_plan, $dbBuy_plan->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_buy_plan()
    {
        $buyPlan = factory(Buy_plan::class)->create();

        $resp = $this->buyPlanRepo->delete($buyPlan->id);

        $this->assertTrue($resp);
        $this->assertNull(Buy_plan::find($buyPlan->id), 'Buy_plan should not exist in DB');
    }
}
