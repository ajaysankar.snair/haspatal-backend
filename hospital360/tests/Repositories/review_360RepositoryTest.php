<?php namespace Tests\Repositories;

use App\Models\review_360;
use App\Repositories\review_360Repository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class review_360RepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var review_360Repository
     */
    protected $review360Repo;

    public function setUp() : void
    {
        parent::setUp();
        $this->review360Repo = \App::make(review_360Repository::class);
    }

    /**
     * @test create
     */
    public function test_create_review_360()
    {
        $review360 = factory(review_360::class)->make()->toArray();

        $createdreview_360 = $this->review360Repo->create($review360);

        $createdreview_360 = $createdreview_360->toArray();
        $this->assertArrayHasKey('id', $createdreview_360);
        $this->assertNotNull($createdreview_360['id'], 'Created review_360 must have id specified');
        $this->assertNotNull(review_360::find($createdreview_360['id']), 'review_360 with given id must be in DB');
        $this->assertModelData($review360, $createdreview_360);
    }

    /**
     * @test read
     */
    public function test_read_review_360()
    {
        $review360 = factory(review_360::class)->create();

        $dbreview_360 = $this->review360Repo->find($review360->id);

        $dbreview_360 = $dbreview_360->toArray();
        $this->assertModelData($review360->toArray(), $dbreview_360);
    }

    /**
     * @test update
     */
    public function test_update_review_360()
    {
        $review360 = factory(review_360::class)->create();
        $fakereview_360 = factory(review_360::class)->make()->toArray();

        $updatedreview_360 = $this->review360Repo->update($fakereview_360, $review360->id);

        $this->assertModelData($fakereview_360, $updatedreview_360->toArray());
        $dbreview_360 = $this->review360Repo->find($review360->id);
        $this->assertModelData($fakereview_360, $dbreview_360->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_review_360()
    {
        $review360 = factory(review_360::class)->create();

        $resp = $this->review360Repo->delete($review360->id);

        $this->assertTrue($resp);
        $this->assertNull(review_360::find($review360->id), 'review_360 should not exist in DB');
    }
}
