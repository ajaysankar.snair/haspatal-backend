<?php namespace Tests\Repositories;

use App\Models\Suggested_therapies_patient;
use App\Repositories\Suggested_therapies_patientRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class Suggested_therapies_patientRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var Suggested_therapies_patientRepository
     */
    protected $suggestedTherapiesPatientRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->suggestedTherapiesPatientRepo = \App::make(Suggested_therapies_patientRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_suggested_therapies_patient()
    {
        $suggestedTherapiesPatient = factory(Suggested_therapies_patient::class)->make()->toArray();

        $createdSuggested_therapies_patient = $this->suggestedTherapiesPatientRepo->create($suggestedTherapiesPatient);

        $createdSuggested_therapies_patient = $createdSuggested_therapies_patient->toArray();
        $this->assertArrayHasKey('id', $createdSuggested_therapies_patient);
        $this->assertNotNull($createdSuggested_therapies_patient['id'], 'Created Suggested_therapies_patient must have id specified');
        $this->assertNotNull(Suggested_therapies_patient::find($createdSuggested_therapies_patient['id']), 'Suggested_therapies_patient with given id must be in DB');
        $this->assertModelData($suggestedTherapiesPatient, $createdSuggested_therapies_patient);
    }

    /**
     * @test read
     */
    public function test_read_suggested_therapies_patient()
    {
        $suggestedTherapiesPatient = factory(Suggested_therapies_patient::class)->create();

        $dbSuggested_therapies_patient = $this->suggestedTherapiesPatientRepo->find($suggestedTherapiesPatient->id);

        $dbSuggested_therapies_patient = $dbSuggested_therapies_patient->toArray();
        $this->assertModelData($suggestedTherapiesPatient->toArray(), $dbSuggested_therapies_patient);
    }

    /**
     * @test update
     */
    public function test_update_suggested_therapies_patient()
    {
        $suggestedTherapiesPatient = factory(Suggested_therapies_patient::class)->create();
        $fakeSuggested_therapies_patient = factory(Suggested_therapies_patient::class)->make()->toArray();

        $updatedSuggested_therapies_patient = $this->suggestedTherapiesPatientRepo->update($fakeSuggested_therapies_patient, $suggestedTherapiesPatient->id);

        $this->assertModelData($fakeSuggested_therapies_patient, $updatedSuggested_therapies_patient->toArray());
        $dbSuggested_therapies_patient = $this->suggestedTherapiesPatientRepo->find($suggestedTherapiesPatient->id);
        $this->assertModelData($fakeSuggested_therapies_patient, $dbSuggested_therapies_patient->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_suggested_therapies_patient()
    {
        $suggestedTherapiesPatient = factory(Suggested_therapies_patient::class)->create();

        $resp = $this->suggestedTherapiesPatientRepo->delete($suggestedTherapiesPatient->id);

        $this->assertTrue($resp);
        $this->assertNull(Suggested_therapies_patient::find($suggestedTherapiesPatient->id), 'Suggested_therapies_patient should not exist in DB');
    }
}
