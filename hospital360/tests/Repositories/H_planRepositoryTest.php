<?php namespace Tests\Repositories;

use App\Models\H_plan;
use App\Repositories\H_planRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class H_planRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var H_planRepository
     */
    protected $hPlanRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->hPlanRepo = \App::make(H_planRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_h_plan()
    {
        $hPlan = factory(H_plan::class)->make()->toArray();

        $createdH_plan = $this->hPlanRepo->create($hPlan);

        $createdH_plan = $createdH_plan->toArray();
        $this->assertArrayHasKey('id', $createdH_plan);
        $this->assertNotNull($createdH_plan['id'], 'Created H_plan must have id specified');
        $this->assertNotNull(H_plan::find($createdH_plan['id']), 'H_plan with given id must be in DB');
        $this->assertModelData($hPlan, $createdH_plan);
    }

    /**
     * @test read
     */
    public function test_read_h_plan()
    {
        $hPlan = factory(H_plan::class)->create();

        $dbH_plan = $this->hPlanRepo->find($hPlan->id);

        $dbH_plan = $dbH_plan->toArray();
        $this->assertModelData($hPlan->toArray(), $dbH_plan);
    }

    /**
     * @test update
     */
    public function test_update_h_plan()
    {
        $hPlan = factory(H_plan::class)->create();
        $fakeH_plan = factory(H_plan::class)->make()->toArray();

        $updatedH_plan = $this->hPlanRepo->update($fakeH_plan, $hPlan->id);

        $this->assertModelData($fakeH_plan, $updatedH_plan->toArray());
        $dbH_plan = $this->hPlanRepo->find($hPlan->id);
        $this->assertModelData($fakeH_plan, $dbH_plan->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_h_plan()
    {
        $hPlan = factory(H_plan::class)->create();

        $resp = $this->hPlanRepo->delete($hPlan->id);

        $this->assertTrue($resp);
        $this->assertNull(H_plan::find($hPlan->id), 'H_plan should not exist in DB');
    }
}
