<?php namespace Tests\Repositories;

use App\Models\Consultant;
use App\Repositories\ConsultantRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class ConsultantRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var ConsultantRepository
     */
    protected $consultantRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->consultantRepo = \App::make(ConsultantRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_consultant()
    {
        $consultant = factory(Consultant::class)->make()->toArray();

        $createdConsultant = $this->consultantRepo->create($consultant);

        $createdConsultant = $createdConsultant->toArray();
        $this->assertArrayHasKey('id', $createdConsultant);
        $this->assertNotNull($createdConsultant['id'], 'Created Consultant must have id specified');
        $this->assertNotNull(Consultant::find($createdConsultant['id']), 'Consultant with given id must be in DB');
        $this->assertModelData($consultant, $createdConsultant);
    }

    /**
     * @test read
     */
    public function test_read_consultant()
    {
        $consultant = factory(Consultant::class)->create();

        $dbConsultant = $this->consultantRepo->find($consultant->id);

        $dbConsultant = $dbConsultant->toArray();
        $this->assertModelData($consultant->toArray(), $dbConsultant);
    }

    /**
     * @test update
     */
    public function test_update_consultant()
    {
        $consultant = factory(Consultant::class)->create();
        $fakeConsultant = factory(Consultant::class)->make()->toArray();

        $updatedConsultant = $this->consultantRepo->update($fakeConsultant, $consultant->id);

        $this->assertModelData($fakeConsultant, $updatedConsultant->toArray());
        $dbConsultant = $this->consultantRepo->find($consultant->id);
        $this->assertModelData($fakeConsultant, $dbConsultant->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_consultant()
    {
        $consultant = factory(Consultant::class)->create();

        $resp = $this->consultantRepo->delete($consultant->id);

        $this->assertTrue($resp);
        $this->assertNull(Consultant::find($consultant->id), 'Consultant should not exist in DB');
    }
}
