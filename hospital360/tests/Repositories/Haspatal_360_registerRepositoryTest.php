<?php namespace Tests\Repositories;

use App\Models\Haspatal_360_register;
use App\Repositories\Haspatal_360_registerRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class Haspatal_360_registerRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var Haspatal_360_registerRepository
     */
    protected $haspatal360RegisterRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->haspatal360RegisterRepo = \App::make(Haspatal_360_registerRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_haspatal_360_register()
    {
        $haspatal360Register = factory(Haspatal_360_register::class)->make()->toArray();

        $createdHaspatal_360_register = $this->haspatal360RegisterRepo->create($haspatal360Register);

        $createdHaspatal_360_register = $createdHaspatal_360_register->toArray();
        $this->assertArrayHasKey('id', $createdHaspatal_360_register);
        $this->assertNotNull($createdHaspatal_360_register['id'], 'Created Haspatal_360_register must have id specified');
        $this->assertNotNull(Haspatal_360_register::find($createdHaspatal_360_register['id']), 'Haspatal_360_register with given id must be in DB');
        $this->assertModelData($haspatal360Register, $createdHaspatal_360_register);
    }

    /**
     * @test read
     */
    public function test_read_haspatal_360_register()
    {
        $haspatal360Register = factory(Haspatal_360_register::class)->create();

        $dbHaspatal_360_register = $this->haspatal360RegisterRepo->find($haspatal360Register->id);

        $dbHaspatal_360_register = $dbHaspatal_360_register->toArray();
        $this->assertModelData($haspatal360Register->toArray(), $dbHaspatal_360_register);
    }

    /**
     * @test update
     */
    public function test_update_haspatal_360_register()
    {
        $haspatal360Register = factory(Haspatal_360_register::class)->create();
        $fakeHaspatal_360_register = factory(Haspatal_360_register::class)->make()->toArray();

        $updatedHaspatal_360_register = $this->haspatal360RegisterRepo->update($fakeHaspatal_360_register, $haspatal360Register->id);

        $this->assertModelData($fakeHaspatal_360_register, $updatedHaspatal_360_register->toArray());
        $dbHaspatal_360_register = $this->haspatal360RegisterRepo->find($haspatal360Register->id);
        $this->assertModelData($fakeHaspatal_360_register, $dbHaspatal_360_register->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_haspatal_360_register()
    {
        $haspatal360Register = factory(Haspatal_360_register::class)->create();

        $resp = $this->haspatal360RegisterRepo->delete($haspatal360Register->id);

        $this->assertTrue($resp);
        $this->assertNull(Haspatal_360_register::find($haspatal360Register->id), 'Haspatal_360_register should not exist in DB');
    }
}
