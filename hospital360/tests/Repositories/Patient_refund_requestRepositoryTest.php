<?php namespace Tests\Repositories;

use App\Models\Patient_refund_request;
use App\Repositories\Patient_refund_requestRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class Patient_refund_requestRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var Patient_refund_requestRepository
     */
    protected $patientRefundRequestRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->patientRefundRequestRepo = \App::make(Patient_refund_requestRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_patient_refund_request()
    {
        $patientRefundRequest = factory(Patient_refund_request::class)->make()->toArray();

        $createdPatient_refund_request = $this->patientRefundRequestRepo->create($patientRefundRequest);

        $createdPatient_refund_request = $createdPatient_refund_request->toArray();
        $this->assertArrayHasKey('id', $createdPatient_refund_request);
        $this->assertNotNull($createdPatient_refund_request['id'], 'Created Patient_refund_request must have id specified');
        $this->assertNotNull(Patient_refund_request::find($createdPatient_refund_request['id']), 'Patient_refund_request with given id must be in DB');
        $this->assertModelData($patientRefundRequest, $createdPatient_refund_request);
    }

    /**
     * @test read
     */
    public function test_read_patient_refund_request()
    {
        $patientRefundRequest = factory(Patient_refund_request::class)->create();

        $dbPatient_refund_request = $this->patientRefundRequestRepo->find($patientRefundRequest->id);

        $dbPatient_refund_request = $dbPatient_refund_request->toArray();
        $this->assertModelData($patientRefundRequest->toArray(), $dbPatient_refund_request);
    }

    /**
     * @test update
     */
    public function test_update_patient_refund_request()
    {
        $patientRefundRequest = factory(Patient_refund_request::class)->create();
        $fakePatient_refund_request = factory(Patient_refund_request::class)->make()->toArray();

        $updatedPatient_refund_request = $this->patientRefundRequestRepo->update($fakePatient_refund_request, $patientRefundRequest->id);

        $this->assertModelData($fakePatient_refund_request, $updatedPatient_refund_request->toArray());
        $dbPatient_refund_request = $this->patientRefundRequestRepo->find($patientRefundRequest->id);
        $this->assertModelData($fakePatient_refund_request, $dbPatient_refund_request->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_patient_refund_request()
    {
        $patientRefundRequest = factory(Patient_refund_request::class)->create();

        $resp = $this->patientRefundRequestRepo->delete($patientRefundRequest->id);

        $this->assertTrue($resp);
        $this->assertNull(Patient_refund_request::find($patientRefundRequest->id), 'Patient_refund_request should not exist in DB');
    }
}
