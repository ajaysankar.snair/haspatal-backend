<?php namespace Tests\Repositories;

use App\Models\My_note_patient;
use App\Repositories\My_note_patientRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class My_note_patientRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var My_note_patientRepository
     */
    protected $myNotePatientRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->myNotePatientRepo = \App::make(My_note_patientRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_my_note_patient()
    {
        $myNotePatient = factory(My_note_patient::class)->make()->toArray();

        $createdMy_note_patient = $this->myNotePatientRepo->create($myNotePatient);

        $createdMy_note_patient = $createdMy_note_patient->toArray();
        $this->assertArrayHasKey('id', $createdMy_note_patient);
        $this->assertNotNull($createdMy_note_patient['id'], 'Created My_note_patient must have id specified');
        $this->assertNotNull(My_note_patient::find($createdMy_note_patient['id']), 'My_note_patient with given id must be in DB');
        $this->assertModelData($myNotePatient, $createdMy_note_patient);
    }

    /**
     * @test read
     */
    public function test_read_my_note_patient()
    {
        $myNotePatient = factory(My_note_patient::class)->create();

        $dbMy_note_patient = $this->myNotePatientRepo->find($myNotePatient->id);

        $dbMy_note_patient = $dbMy_note_patient->toArray();
        $this->assertModelData($myNotePatient->toArray(), $dbMy_note_patient);
    }

    /**
     * @test update
     */
    public function test_update_my_note_patient()
    {
        $myNotePatient = factory(My_note_patient::class)->create();
        $fakeMy_note_patient = factory(My_note_patient::class)->make()->toArray();

        $updatedMy_note_patient = $this->myNotePatientRepo->update($fakeMy_note_patient, $myNotePatient->id);

        $this->assertModelData($fakeMy_note_patient, $updatedMy_note_patient->toArray());
        $dbMy_note_patient = $this->myNotePatientRepo->find($myNotePatient->id);
        $this->assertModelData($fakeMy_note_patient, $dbMy_note_patient->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_my_note_patient()
    {
        $myNotePatient = factory(My_note_patient::class)->create();

        $resp = $this->myNotePatientRepo->delete($myNotePatient->id);

        $this->assertTrue($resp);
        $this->assertNull(My_note_patient::find($myNotePatient->id), 'My_note_patient should not exist in DB');
    }
}
