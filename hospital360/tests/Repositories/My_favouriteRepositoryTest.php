<?php namespace Tests\Repositories;

use App\Models\My_favourite;
use App\Repositories\My_favouriteRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class My_favouriteRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var My_favouriteRepository
     */
    protected $myFavouriteRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->myFavouriteRepo = \App::make(My_favouriteRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_my_favourite()
    {
        $myFavourite = factory(My_favourite::class)->make()->toArray();

        $createdMy_favourite = $this->myFavouriteRepo->create($myFavourite);

        $createdMy_favourite = $createdMy_favourite->toArray();
        $this->assertArrayHasKey('id', $createdMy_favourite);
        $this->assertNotNull($createdMy_favourite['id'], 'Created My_favourite must have id specified');
        $this->assertNotNull(My_favourite::find($createdMy_favourite['id']), 'My_favourite with given id must be in DB');
        $this->assertModelData($myFavourite, $createdMy_favourite);
    }

    /**
     * @test read
     */
    public function test_read_my_favourite()
    {
        $myFavourite = factory(My_favourite::class)->create();

        $dbMy_favourite = $this->myFavouriteRepo->find($myFavourite->id);

        $dbMy_favourite = $dbMy_favourite->toArray();
        $this->assertModelData($myFavourite->toArray(), $dbMy_favourite);
    }

    /**
     * @test update
     */
    public function test_update_my_favourite()
    {
        $myFavourite = factory(My_favourite::class)->create();
        $fakeMy_favourite = factory(My_favourite::class)->make()->toArray();

        $updatedMy_favourite = $this->myFavouriteRepo->update($fakeMy_favourite, $myFavourite->id);

        $this->assertModelData($fakeMy_favourite, $updatedMy_favourite->toArray());
        $dbMy_favourite = $this->myFavouriteRepo->find($myFavourite->id);
        $this->assertModelData($fakeMy_favourite, $dbMy_favourite->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_my_favourite()
    {
        $myFavourite = factory(My_favourite::class)->create();

        $resp = $this->myFavouriteRepo->delete($myFavourite->id);

        $this->assertTrue($resp);
        $this->assertNull(My_favourite::find($myFavourite->id), 'My_favourite should not exist in DB');
    }
}
