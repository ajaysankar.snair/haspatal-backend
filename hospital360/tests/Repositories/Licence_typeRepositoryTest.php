<?php namespace Tests\Repositories;

use App\Models\Licence_type;
use App\Repositories\Licence_typeRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class Licence_typeRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var Licence_typeRepository
     */
    protected $licenceTypeRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->licenceTypeRepo = \App::make(Licence_typeRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_licence_type()
    {
        $licenceType = factory(Licence_type::class)->make()->toArray();

        $createdLicence_type = $this->licenceTypeRepo->create($licenceType);

        $createdLicence_type = $createdLicence_type->toArray();
        $this->assertArrayHasKey('id', $createdLicence_type);
        $this->assertNotNull($createdLicence_type['id'], 'Created Licence_type must have id specified');
        $this->assertNotNull(Licence_type::find($createdLicence_type['id']), 'Licence_type with given id must be in DB');
        $this->assertModelData($licenceType, $createdLicence_type);
    }

    /**
     * @test read
     */
    public function test_read_licence_type()
    {
        $licenceType = factory(Licence_type::class)->create();

        $dbLicence_type = $this->licenceTypeRepo->find($licenceType->id);

        $dbLicence_type = $dbLicence_type->toArray();
        $this->assertModelData($licenceType->toArray(), $dbLicence_type);
    }

    /**
     * @test update
     */
    public function test_update_licence_type()
    {
        $licenceType = factory(Licence_type::class)->create();
        $fakeLicence_type = factory(Licence_type::class)->make()->toArray();

        $updatedLicence_type = $this->licenceTypeRepo->update($fakeLicence_type, $licenceType->id);

        $this->assertModelData($fakeLicence_type, $updatedLicence_type->toArray());
        $dbLicence_type = $this->licenceTypeRepo->find($licenceType->id);
        $this->assertModelData($fakeLicence_type, $dbLicence_type->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_licence_type()
    {
        $licenceType = factory(Licence_type::class)->create();

        $resp = $this->licenceTypeRepo->delete($licenceType->id);

        $this->assertTrue($resp);
        $this->assertNull(Licence_type::find($licenceType->id), 'Licence_type should not exist in DB');
    }
}
