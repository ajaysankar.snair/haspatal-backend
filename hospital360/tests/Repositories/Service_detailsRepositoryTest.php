<?php namespace Tests\Repositories;

use App\Models\Service_details;
use App\Repositories\Service_detailsRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class Service_detailsRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var Service_detailsRepository
     */
    protected $serviceDetailsRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->serviceDetailsRepo = \App::make(Service_detailsRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_service_details()
    {
        $serviceDetails = factory(Service_details::class)->make()->toArray();

        $createdService_details = $this->serviceDetailsRepo->create($serviceDetails);

        $createdService_details = $createdService_details->toArray();
        $this->assertArrayHasKey('id', $createdService_details);
        $this->assertNotNull($createdService_details['id'], 'Created Service_details must have id specified');
        $this->assertNotNull(Service_details::find($createdService_details['id']), 'Service_details with given id must be in DB');
        $this->assertModelData($serviceDetails, $createdService_details);
    }

    /**
     * @test read
     */
    public function test_read_service_details()
    {
        $serviceDetails = factory(Service_details::class)->create();

        $dbService_details = $this->serviceDetailsRepo->find($serviceDetails->id);

        $dbService_details = $dbService_details->toArray();
        $this->assertModelData($serviceDetails->toArray(), $dbService_details);
    }

    /**
     * @test update
     */
    public function test_update_service_details()
    {
        $serviceDetails = factory(Service_details::class)->create();
        $fakeService_details = factory(Service_details::class)->make()->toArray();

        $updatedService_details = $this->serviceDetailsRepo->update($fakeService_details, $serviceDetails->id);

        $this->assertModelData($fakeService_details, $updatedService_details->toArray());
        $dbService_details = $this->serviceDetailsRepo->find($serviceDetails->id);
        $this->assertModelData($fakeService_details, $dbService_details->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_service_details()
    {
        $serviceDetails = factory(Service_details::class)->create();

        $resp = $this->serviceDetailsRepo->delete($serviceDetails->id);

        $this->assertTrue($resp);
        $this->assertNull(Service_details::find($serviceDetails->id), 'Service_details should not exist in DB');
    }
}
