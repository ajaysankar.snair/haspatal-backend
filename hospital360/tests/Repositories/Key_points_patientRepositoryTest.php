<?php namespace Tests\Repositories;

use App\Models\Key_points_patient;
use App\Repositories\Key_points_patientRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class Key_points_patientRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var Key_points_patientRepository
     */
    protected $keyPointsPatientRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->keyPointsPatientRepo = \App::make(Key_points_patientRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_key_points_patient()
    {
        $keyPointsPatient = factory(Key_points_patient::class)->make()->toArray();

        $createdKey_points_patient = $this->keyPointsPatientRepo->create($keyPointsPatient);

        $createdKey_points_patient = $createdKey_points_patient->toArray();
        $this->assertArrayHasKey('id', $createdKey_points_patient);
        $this->assertNotNull($createdKey_points_patient['id'], 'Created Key_points_patient must have id specified');
        $this->assertNotNull(Key_points_patient::find($createdKey_points_patient['id']), 'Key_points_patient with given id must be in DB');
        $this->assertModelData($keyPointsPatient, $createdKey_points_patient);
    }

    /**
     * @test read
     */
    public function test_read_key_points_patient()
    {
        $keyPointsPatient = factory(Key_points_patient::class)->create();

        $dbKey_points_patient = $this->keyPointsPatientRepo->find($keyPointsPatient->id);

        $dbKey_points_patient = $dbKey_points_patient->toArray();
        $this->assertModelData($keyPointsPatient->toArray(), $dbKey_points_patient);
    }

    /**
     * @test update
     */
    public function test_update_key_points_patient()
    {
        $keyPointsPatient = factory(Key_points_patient::class)->create();
        $fakeKey_points_patient = factory(Key_points_patient::class)->make()->toArray();

        $updatedKey_points_patient = $this->keyPointsPatientRepo->update($fakeKey_points_patient, $keyPointsPatient->id);

        $this->assertModelData($fakeKey_points_patient, $updatedKey_points_patient->toArray());
        $dbKey_points_patient = $this->keyPointsPatientRepo->find($keyPointsPatient->id);
        $this->assertModelData($fakeKey_points_patient, $dbKey_points_patient->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_key_points_patient()
    {
        $keyPointsPatient = factory(Key_points_patient::class)->create();

        $resp = $this->keyPointsPatientRepo->delete($keyPointsPatient->id);

        $this->assertTrue($resp);
        $this->assertNull(Key_points_patient::find($keyPointsPatient->id), 'Key_points_patient should not exist in DB');
    }
}
