<?php namespace Tests\Repositories;

use App\Models\Perceived_patient;
use App\Repositories\Perceived_patientRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class Perceived_patientRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var Perceived_patientRepository
     */
    protected $perceivedPatientRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->perceivedPatientRepo = \App::make(Perceived_patientRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_perceived_patient()
    {
        $perceivedPatient = factory(Perceived_patient::class)->make()->toArray();

        $createdPerceived_patient = $this->perceivedPatientRepo->create($perceivedPatient);

        $createdPerceived_patient = $createdPerceived_patient->toArray();
        $this->assertArrayHasKey('id', $createdPerceived_patient);
        $this->assertNotNull($createdPerceived_patient['id'], 'Created Perceived_patient must have id specified');
        $this->assertNotNull(Perceived_patient::find($createdPerceived_patient['id']), 'Perceived_patient with given id must be in DB');
        $this->assertModelData($perceivedPatient, $createdPerceived_patient);
    }

    /**
     * @test read
     */
    public function test_read_perceived_patient()
    {
        $perceivedPatient = factory(Perceived_patient::class)->create();

        $dbPerceived_patient = $this->perceivedPatientRepo->find($perceivedPatient->id);

        $dbPerceived_patient = $dbPerceived_patient->toArray();
        $this->assertModelData($perceivedPatient->toArray(), $dbPerceived_patient);
    }

    /**
     * @test update
     */
    public function test_update_perceived_patient()
    {
        $perceivedPatient = factory(Perceived_patient::class)->create();
        $fakePerceived_patient = factory(Perceived_patient::class)->make()->toArray();

        $updatedPerceived_patient = $this->perceivedPatientRepo->update($fakePerceived_patient, $perceivedPatient->id);

        $this->assertModelData($fakePerceived_patient, $updatedPerceived_patient->toArray());
        $dbPerceived_patient = $this->perceivedPatientRepo->find($perceivedPatient->id);
        $this->assertModelData($fakePerceived_patient, $dbPerceived_patient->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_perceived_patient()
    {
        $perceivedPatient = factory(Perceived_patient::class)->create();

        $resp = $this->perceivedPatientRepo->delete($perceivedPatient->id);

        $this->assertTrue($resp);
        $this->assertNull(Perceived_patient::find($perceivedPatient->id), 'Perceived_patient should not exist in DB');
    }
}
