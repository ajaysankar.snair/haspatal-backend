<?php namespace Tests\Repositories;

use App\Models\Bank_information;
use App\Repositories\Bank_informationRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class Bank_informationRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var Bank_informationRepository
     */
    protected $bankInformationRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->bankInformationRepo = \App::make(Bank_informationRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_bank_information()
    {
        $bankInformation = factory(Bank_information::class)->make()->toArray();

        $createdBank_information = $this->bankInformationRepo->create($bankInformation);

        $createdBank_information = $createdBank_information->toArray();
        $this->assertArrayHasKey('id', $createdBank_information);
        $this->assertNotNull($createdBank_information['id'], 'Created Bank_information must have id specified');
        $this->assertNotNull(Bank_information::find($createdBank_information['id']), 'Bank_information with given id must be in DB');
        $this->assertModelData($bankInformation, $createdBank_information);
    }

    /**
     * @test read
     */
    public function test_read_bank_information()
    {
        $bankInformation = factory(Bank_information::class)->create();

        $dbBank_information = $this->bankInformationRepo->find($bankInformation->id);

        $dbBank_information = $dbBank_information->toArray();
        $this->assertModelData($bankInformation->toArray(), $dbBank_information);
    }

    /**
     * @test update
     */
    public function test_update_bank_information()
    {
        $bankInformation = factory(Bank_information::class)->create();
        $fakeBank_information = factory(Bank_information::class)->make()->toArray();

        $updatedBank_information = $this->bankInformationRepo->update($fakeBank_information, $bankInformation->id);

        $this->assertModelData($fakeBank_information, $updatedBank_information->toArray());
        $dbBank_information = $this->bankInformationRepo->find($bankInformation->id);
        $this->assertModelData($fakeBank_information, $dbBank_information->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_bank_information()
    {
        $bankInformation = factory(Bank_information::class)->create();

        $resp = $this->bankInformationRepo->delete($bankInformation->id);

        $this->assertTrue($resp);
        $this->assertNull(Bank_information::find($bankInformation->id), 'Bank_information should not exist in DB');
    }
}
