<?php namespace Tests\Repositories;

use App\Models\Doctors;
use App\Repositories\DoctorsRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class DoctorsRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var DoctorsRepository
     */
    protected $doctorsRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->doctorsRepo = \App::make(DoctorsRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_doctors()
    {
        $doctors = factory(Doctors::class)->make()->toArray();

        $createdDoctors = $this->doctorsRepo->create($doctors);

        $createdDoctors = $createdDoctors->toArray();
        $this->assertArrayHasKey('id', $createdDoctors);
        $this->assertNotNull($createdDoctors['id'], 'Created Doctors must have id specified');
        $this->assertNotNull(Doctors::find($createdDoctors['id']), 'Doctors with given id must be in DB');
        $this->assertModelData($doctors, $createdDoctors);
    }

    /**
     * @test read
     */
    public function test_read_doctors()
    {
        $doctors = factory(Doctors::class)->create();

        $dbDoctors = $this->doctorsRepo->find($doctors->id);

        $dbDoctors = $dbDoctors->toArray();
        $this->assertModelData($doctors->toArray(), $dbDoctors);
    }

    /**
     * @test update
     */
    public function test_update_doctors()
    {
        $doctors = factory(Doctors::class)->create();
        $fakeDoctors = factory(Doctors::class)->make()->toArray();

        $updatedDoctors = $this->doctorsRepo->update($fakeDoctors, $doctors->id);

        $this->assertModelData($fakeDoctors, $updatedDoctors->toArray());
        $dbDoctors = $this->doctorsRepo->find($doctors->id);
        $this->assertModelData($fakeDoctors, $dbDoctors->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_doctors()
    {
        $doctors = factory(Doctors::class)->create();

        $resp = $this->doctorsRepo->delete($doctors->id);

        $this->assertTrue($resp);
        $this->assertNull(Doctors::find($doctors->id), 'Doctors should not exist in DB');
    }
}
