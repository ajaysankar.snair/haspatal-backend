<?php namespace Tests\Repositories;

use App\Models\Booking_price_admin;
use App\Repositories\Booking_price_adminRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class Booking_price_adminRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var Booking_price_adminRepository
     */
    protected $bookingPriceAdminRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->bookingPriceAdminRepo = \App::make(Booking_price_adminRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_booking_price_admin()
    {
        $bookingPriceAdmin = factory(Booking_price_admin::class)->make()->toArray();

        $createdBooking_price_admin = $this->bookingPriceAdminRepo->create($bookingPriceAdmin);

        $createdBooking_price_admin = $createdBooking_price_admin->toArray();
        $this->assertArrayHasKey('id', $createdBooking_price_admin);
        $this->assertNotNull($createdBooking_price_admin['id'], 'Created Booking_price_admin must have id specified');
        $this->assertNotNull(Booking_price_admin::find($createdBooking_price_admin['id']), 'Booking_price_admin with given id must be in DB');
        $this->assertModelData($bookingPriceAdmin, $createdBooking_price_admin);
    }

    /**
     * @test read
     */
    public function test_read_booking_price_admin()
    {
        $bookingPriceAdmin = factory(Booking_price_admin::class)->create();

        $dbBooking_price_admin = $this->bookingPriceAdminRepo->find($bookingPriceAdmin->id);

        $dbBooking_price_admin = $dbBooking_price_admin->toArray();
        $this->assertModelData($bookingPriceAdmin->toArray(), $dbBooking_price_admin);
    }

    /**
     * @test update
     */
    public function test_update_booking_price_admin()
    {
        $bookingPriceAdmin = factory(Booking_price_admin::class)->create();
        $fakeBooking_price_admin = factory(Booking_price_admin::class)->make()->toArray();

        $updatedBooking_price_admin = $this->bookingPriceAdminRepo->update($fakeBooking_price_admin, $bookingPriceAdmin->id);

        $this->assertModelData($fakeBooking_price_admin, $updatedBooking_price_admin->toArray());
        $dbBooking_price_admin = $this->bookingPriceAdminRepo->find($bookingPriceAdmin->id);
        $this->assertModelData($fakeBooking_price_admin, $dbBooking_price_admin->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_booking_price_admin()
    {
        $bookingPriceAdmin = factory(Booking_price_admin::class)->create();

        $resp = $this->bookingPriceAdminRepo->delete($bookingPriceAdmin->id);

        $this->assertTrue($resp);
        $this->assertNull(Booking_price_admin::find($bookingPriceAdmin->id), 'Booking_price_admin should not exist in DB');
    }
}
