<?php namespace Tests\Repositories;

use App\Models\PropertyDetails;
use App\Repositories\PropertyDetailsRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class PropertyDetailsRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var PropertyDetailsRepository
     */
    protected $propertyDetailsRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->propertyDetailsRepo = \App::make(PropertyDetailsRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_property_details()
    {
        $propertyDetails = factory(PropertyDetails::class)->make()->toArray();

        $createdPropertyDetails = $this->propertyDetailsRepo->create($propertyDetails);

        $createdPropertyDetails = $createdPropertyDetails->toArray();
        $this->assertArrayHasKey('id', $createdPropertyDetails);
        $this->assertNotNull($createdPropertyDetails['id'], 'Created PropertyDetails must have id specified');
        $this->assertNotNull(PropertyDetails::find($createdPropertyDetails['id']), 'PropertyDetails with given id must be in DB');
        $this->assertModelData($propertyDetails, $createdPropertyDetails);
    }

    /**
     * @test read
     */
    public function test_read_property_details()
    {
        $propertyDetails = factory(PropertyDetails::class)->create();

        $dbPropertyDetails = $this->propertyDetailsRepo->find($propertyDetails->id);

        $dbPropertyDetails = $dbPropertyDetails->toArray();
        $this->assertModelData($propertyDetails->toArray(), $dbPropertyDetails);
    }

    /**
     * @test update
     */
    public function test_update_property_details()
    {
        $propertyDetails = factory(PropertyDetails::class)->create();
        $fakePropertyDetails = factory(PropertyDetails::class)->make()->toArray();

        $updatedPropertyDetails = $this->propertyDetailsRepo->update($fakePropertyDetails, $propertyDetails->id);

        $this->assertModelData($fakePropertyDetails, $updatedPropertyDetails->toArray());
        $dbPropertyDetails = $this->propertyDetailsRepo->find($propertyDetails->id);
        $this->assertModelData($fakePropertyDetails, $dbPropertyDetails->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_property_details()
    {
        $propertyDetails = factory(PropertyDetails::class)->create();

        $resp = $this->propertyDetailsRepo->delete($propertyDetails->id);

        $this->assertTrue($resp);
        $this->assertNull(PropertyDetails::find($propertyDetails->id), 'PropertyDetails should not exist in DB');
    }
}
