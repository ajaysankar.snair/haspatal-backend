<?php namespace Tests\Repositories;

use App\Models\Support;
use App\Repositories\SupportRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class SupportRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var SupportRepository
     */
    protected $supportRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->supportRepo = \App::make(SupportRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_support()
    {
        $support = factory(Support::class)->make()->toArray();

        $createdSupport = $this->supportRepo->create($support);

        $createdSupport = $createdSupport->toArray();
        $this->assertArrayHasKey('id', $createdSupport);
        $this->assertNotNull($createdSupport['id'], 'Created Support must have id specified');
        $this->assertNotNull(Support::find($createdSupport['id']), 'Support with given id must be in DB');
        $this->assertModelData($support, $createdSupport);
    }

    /**
     * @test read
     */
    public function test_read_support()
    {
        $support = factory(Support::class)->create();

        $dbSupport = $this->supportRepo->find($support->id);

        $dbSupport = $dbSupport->toArray();
        $this->assertModelData($support->toArray(), $dbSupport);
    }

    /**
     * @test update
     */
    public function test_update_support()
    {
        $support = factory(Support::class)->create();
        $fakeSupport = factory(Support::class)->make()->toArray();

        $updatedSupport = $this->supportRepo->update($fakeSupport, $support->id);

        $this->assertModelData($fakeSupport, $updatedSupport->toArray());
        $dbSupport = $this->supportRepo->find($support->id);
        $this->assertModelData($fakeSupport, $dbSupport->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_support()
    {
        $support = factory(Support::class)->create();

        $resp = $this->supportRepo->delete($support->id);

        $this->assertTrue($resp);
        $this->assertNull(Support::find($support->id), 'Support should not exist in DB');
    }
}
