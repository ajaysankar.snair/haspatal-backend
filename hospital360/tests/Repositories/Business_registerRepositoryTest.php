<?php namespace Tests\Repositories;

use App\Models\Business_register;
use App\Repositories\Business_registerRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class Business_registerRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var Business_registerRepository
     */
    protected $businessRegisterRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->businessRegisterRepo = \App::make(Business_registerRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_business_register()
    {
        $businessRegister = factory(Business_register::class)->make()->toArray();

        $createdBusiness_register = $this->businessRegisterRepo->create($businessRegister);

        $createdBusiness_register = $createdBusiness_register->toArray();
        $this->assertArrayHasKey('id', $createdBusiness_register);
        $this->assertNotNull($createdBusiness_register['id'], 'Created Business_register must have id specified');
        $this->assertNotNull(Business_register::find($createdBusiness_register['id']), 'Business_register with given id must be in DB');
        $this->assertModelData($businessRegister, $createdBusiness_register);
    }

    /**
     * @test read
     */
    public function test_read_business_register()
    {
        $businessRegister = factory(Business_register::class)->create();

        $dbBusiness_register = $this->businessRegisterRepo->find($businessRegister->id);

        $dbBusiness_register = $dbBusiness_register->toArray();
        $this->assertModelData($businessRegister->toArray(), $dbBusiness_register);
    }

    /**
     * @test update
     */
    public function test_update_business_register()
    {
        $businessRegister = factory(Business_register::class)->create();
        $fakeBusiness_register = factory(Business_register::class)->make()->toArray();

        $updatedBusiness_register = $this->businessRegisterRepo->update($fakeBusiness_register, $businessRegister->id);

        $this->assertModelData($fakeBusiness_register, $updatedBusiness_register->toArray());
        $dbBusiness_register = $this->businessRegisterRepo->find($businessRegister->id);
        $this->assertModelData($fakeBusiness_register, $dbBusiness_register->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_business_register()
    {
        $businessRegister = factory(Business_register::class)->create();

        $resp = $this->businessRegisterRepo->delete($businessRegister->id);

        $this->assertTrue($resp);
        $this->assertNull(Business_register::find($businessRegister->id), 'Business_register should not exist in DB');
    }
}
