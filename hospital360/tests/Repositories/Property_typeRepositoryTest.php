<?php namespace Tests\Repositories;

use App\Models\Property_type;
use App\Repositories\Property_typeRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class Property_typeRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var Property_typeRepository
     */
    protected $propertyTypeRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->propertyTypeRepo = \App::make(Property_typeRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_property_type()
    {
        $propertyType = factory(Property_type::class)->make()->toArray();

        $createdProperty_type = $this->propertyTypeRepo->create($propertyType);

        $createdProperty_type = $createdProperty_type->toArray();
        $this->assertArrayHasKey('id', $createdProperty_type);
        $this->assertNotNull($createdProperty_type['id'], 'Created Property_type must have id specified');
        $this->assertNotNull(Property_type::find($createdProperty_type['id']), 'Property_type with given id must be in DB');
        $this->assertModelData($propertyType, $createdProperty_type);
    }

    /**
     * @test read
     */
    public function test_read_property_type()
    {
        $propertyType = factory(Property_type::class)->create();

        $dbProperty_type = $this->propertyTypeRepo->find($propertyType->id);

        $dbProperty_type = $dbProperty_type->toArray();
        $this->assertModelData($propertyType->toArray(), $dbProperty_type);
    }

    /**
     * @test update
     */
    public function test_update_property_type()
    {
        $propertyType = factory(Property_type::class)->create();
        $fakeProperty_type = factory(Property_type::class)->make()->toArray();

        $updatedProperty_type = $this->propertyTypeRepo->update($fakeProperty_type, $propertyType->id);

        $this->assertModelData($fakeProperty_type, $updatedProperty_type->toArray());
        $dbProperty_type = $this->propertyTypeRepo->find($propertyType->id);
        $this->assertModelData($fakeProperty_type, $dbProperty_type->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_property_type()
    {
        $propertyType = factory(Property_type::class)->create();

        $resp = $this->propertyTypeRepo->delete($propertyType->id);

        $this->assertTrue($resp);
        $this->assertNull(Property_type::find($propertyType->id), 'Property_type should not exist in DB');
    }
}
