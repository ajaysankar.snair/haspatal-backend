<?php namespace Tests\Repositories;

use App\Models\Wallet_360_transaction;
use App\Repositories\Wallet_360_transactionRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class Wallet_360_transactionRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var Wallet_360_transactionRepository
     */
    protected $wallet360TransactionRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->wallet360TransactionRepo = \App::make(Wallet_360_transactionRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_wallet_360_transaction()
    {
        $wallet360Transaction = factory(Wallet_360_transaction::class)->make()->toArray();

        $createdWallet_360_transaction = $this->wallet360TransactionRepo->create($wallet360Transaction);

        $createdWallet_360_transaction = $createdWallet_360_transaction->toArray();
        $this->assertArrayHasKey('id', $createdWallet_360_transaction);
        $this->assertNotNull($createdWallet_360_transaction['id'], 'Created Wallet_360_transaction must have id specified');
        $this->assertNotNull(Wallet_360_transaction::find($createdWallet_360_transaction['id']), 'Wallet_360_transaction with given id must be in DB');
        $this->assertModelData($wallet360Transaction, $createdWallet_360_transaction);
    }

    /**
     * @test read
     */
    public function test_read_wallet_360_transaction()
    {
        $wallet360Transaction = factory(Wallet_360_transaction::class)->create();

        $dbWallet_360_transaction = $this->wallet360TransactionRepo->find($wallet360Transaction->id);

        $dbWallet_360_transaction = $dbWallet_360_transaction->toArray();
        $this->assertModelData($wallet360Transaction->toArray(), $dbWallet_360_transaction);
    }

    /**
     * @test update
     */
    public function test_update_wallet_360_transaction()
    {
        $wallet360Transaction = factory(Wallet_360_transaction::class)->create();
        $fakeWallet_360_transaction = factory(Wallet_360_transaction::class)->make()->toArray();

        $updatedWallet_360_transaction = $this->wallet360TransactionRepo->update($fakeWallet_360_transaction, $wallet360Transaction->id);

        $this->assertModelData($fakeWallet_360_transaction, $updatedWallet_360_transaction->toArray());
        $dbWallet_360_transaction = $this->wallet360TransactionRepo->find($wallet360Transaction->id);
        $this->assertModelData($fakeWallet_360_transaction, $dbWallet_360_transaction->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_wallet_360_transaction()
    {
        $wallet360Transaction = factory(Wallet_360_transaction::class)->create();

        $resp = $this->wallet360TransactionRepo->delete($wallet360Transaction->id);

        $this->assertTrue($resp);
        $this->assertNull(Wallet_360_transaction::find($wallet360Transaction->id), 'Wallet_360_transaction should not exist in DB');
    }
}
