<?php namespace Tests\Repositories;

use App\Models\Doctor_withdraw;
use App\Repositories\Doctor_withdrawRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class Doctor_withdrawRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var Doctor_withdrawRepository
     */
    protected $doctorWithdrawRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->doctorWithdrawRepo = \App::make(Doctor_withdrawRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_doctor_withdraw()
    {
        $doctorWithdraw = factory(Doctor_withdraw::class)->make()->toArray();

        $createdDoctor_withdraw = $this->doctorWithdrawRepo->create($doctorWithdraw);

        $createdDoctor_withdraw = $createdDoctor_withdraw->toArray();
        $this->assertArrayHasKey('id', $createdDoctor_withdraw);
        $this->assertNotNull($createdDoctor_withdraw['id'], 'Created Doctor_withdraw must have id specified');
        $this->assertNotNull(Doctor_withdraw::find($createdDoctor_withdraw['id']), 'Doctor_withdraw with given id must be in DB');
        $this->assertModelData($doctorWithdraw, $createdDoctor_withdraw);
    }

    /**
     * @test read
     */
    public function test_read_doctor_withdraw()
    {
        $doctorWithdraw = factory(Doctor_withdraw::class)->create();

        $dbDoctor_withdraw = $this->doctorWithdrawRepo->find($doctorWithdraw->id);

        $dbDoctor_withdraw = $dbDoctor_withdraw->toArray();
        $this->assertModelData($doctorWithdraw->toArray(), $dbDoctor_withdraw);
    }

    /**
     * @test update
     */
    public function test_update_doctor_withdraw()
    {
        $doctorWithdraw = factory(Doctor_withdraw::class)->create();
        $fakeDoctor_withdraw = factory(Doctor_withdraw::class)->make()->toArray();

        $updatedDoctor_withdraw = $this->doctorWithdrawRepo->update($fakeDoctor_withdraw, $doctorWithdraw->id);

        $this->assertModelData($fakeDoctor_withdraw, $updatedDoctor_withdraw->toArray());
        $dbDoctor_withdraw = $this->doctorWithdrawRepo->find($doctorWithdraw->id);
        $this->assertModelData($fakeDoctor_withdraw, $dbDoctor_withdraw->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_doctor_withdraw()
    {
        $doctorWithdraw = factory(Doctor_withdraw::class)->create();

        $resp = $this->doctorWithdrawRepo->delete($doctorWithdraw->id);

        $this->assertTrue($resp);
        $this->assertNull(Doctor_withdraw::find($doctorWithdraw->id), 'Doctor_withdraw should not exist in DB');
    }
}
