<?php namespace Tests\Repositories;

use App\Models\BookingRequest;
use App\Repositories\BookingRequestRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class BookingRequestRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var BookingRequestRepository
     */
    protected $bookingRequestRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->bookingRequestRepo = \App::make(BookingRequestRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_booking_request()
    {
        $bookingRequest = factory(BookingRequest::class)->make()->toArray();

        $createdBookingRequest = $this->bookingRequestRepo->create($bookingRequest);

        $createdBookingRequest = $createdBookingRequest->toArray();
        $this->assertArrayHasKey('id', $createdBookingRequest);
        $this->assertNotNull($createdBookingRequest['id'], 'Created BookingRequest must have id specified');
        $this->assertNotNull(BookingRequest::find($createdBookingRequest['id']), 'BookingRequest with given id must be in DB');
        $this->assertModelData($bookingRequest, $createdBookingRequest);
    }

    /**
     * @test read
     */
    public function test_read_booking_request()
    {
        $bookingRequest = factory(BookingRequest::class)->create();

        $dbBookingRequest = $this->bookingRequestRepo->find($bookingRequest->id);

        $dbBookingRequest = $dbBookingRequest->toArray();
        $this->assertModelData($bookingRequest->toArray(), $dbBookingRequest);
    }

    /**
     * @test update
     */
    public function test_update_booking_request()
    {
        $bookingRequest = factory(BookingRequest::class)->create();
        $fakeBookingRequest = factory(BookingRequest::class)->make()->toArray();

        $updatedBookingRequest = $this->bookingRequestRepo->update($fakeBookingRequest, $bookingRequest->id);

        $this->assertModelData($fakeBookingRequest, $updatedBookingRequest->toArray());
        $dbBookingRequest = $this->bookingRequestRepo->find($bookingRequest->id);
        $this->assertModelData($fakeBookingRequest, $dbBookingRequest->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_booking_request()
    {
        $bookingRequest = factory(BookingRequest::class)->create();

        $resp = $this->bookingRequestRepo->delete($bookingRequest->id);

        $this->assertTrue($resp);
        $this->assertNull(BookingRequest::find($bookingRequest->id), 'BookingRequest should not exist in DB');
    }
}
