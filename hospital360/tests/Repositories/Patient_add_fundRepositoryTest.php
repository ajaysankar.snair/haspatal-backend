<?php namespace Tests\Repositories;

use App\Models\Patient_add_fund;
use App\Repositories\Patient_add_fundRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class Patient_add_fundRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var Patient_add_fundRepository
     */
    protected $patientAddFundRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->patientAddFundRepo = \App::make(Patient_add_fundRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_patient_add_fund()
    {
        $patientAddFund = factory(Patient_add_fund::class)->make()->toArray();

        $createdPatient_add_fund = $this->patientAddFundRepo->create($patientAddFund);

        $createdPatient_add_fund = $createdPatient_add_fund->toArray();
        $this->assertArrayHasKey('id', $createdPatient_add_fund);
        $this->assertNotNull($createdPatient_add_fund['id'], 'Created Patient_add_fund must have id specified');
        $this->assertNotNull(Patient_add_fund::find($createdPatient_add_fund['id']), 'Patient_add_fund with given id must be in DB');
        $this->assertModelData($patientAddFund, $createdPatient_add_fund);
    }

    /**
     * @test read
     */
    public function test_read_patient_add_fund()
    {
        $patientAddFund = factory(Patient_add_fund::class)->create();

        $dbPatient_add_fund = $this->patientAddFundRepo->find($patientAddFund->id);

        $dbPatient_add_fund = $dbPatient_add_fund->toArray();
        $this->assertModelData($patientAddFund->toArray(), $dbPatient_add_fund);
    }

    /**
     * @test update
     */
    public function test_update_patient_add_fund()
    {
        $patientAddFund = factory(Patient_add_fund::class)->create();
        $fakePatient_add_fund = factory(Patient_add_fund::class)->make()->toArray();

        $updatedPatient_add_fund = $this->patientAddFundRepo->update($fakePatient_add_fund, $patientAddFund->id);

        $this->assertModelData($fakePatient_add_fund, $updatedPatient_add_fund->toArray());
        $dbPatient_add_fund = $this->patientAddFundRepo->find($patientAddFund->id);
        $this->assertModelData($fakePatient_add_fund, $dbPatient_add_fund->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_patient_add_fund()
    {
        $patientAddFund = factory(Patient_add_fund::class)->create();

        $resp = $this->patientAddFundRepo->delete($patientAddFund->id);

        $this->assertTrue($resp);
        $this->assertNull(Patient_add_fund::find($patientAddFund->id), 'Patient_add_fund should not exist in DB');
    }
}
