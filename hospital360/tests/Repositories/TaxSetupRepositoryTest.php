<?php namespace Tests\Repositories;

use App\Models\TaxSetup;
use App\Repositories\TaxSetupRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class TaxSetupRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var TaxSetupRepository
     */
    protected $taxSetupRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->taxSetupRepo = \App::make(TaxSetupRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_tax_setup()
    {
        $taxSetup = factory(TaxSetup::class)->make()->toArray();

        $createdTaxSetup = $this->taxSetupRepo->create($taxSetup);

        $createdTaxSetup = $createdTaxSetup->toArray();
        $this->assertArrayHasKey('id', $createdTaxSetup);
        $this->assertNotNull($createdTaxSetup['id'], 'Created TaxSetup must have id specified');
        $this->assertNotNull(TaxSetup::find($createdTaxSetup['id']), 'TaxSetup with given id must be in DB');
        $this->assertModelData($taxSetup, $createdTaxSetup);
    }

    /**
     * @test read
     */
    public function test_read_tax_setup()
    {
        $taxSetup = factory(TaxSetup::class)->create();

        $dbTaxSetup = $this->taxSetupRepo->find($taxSetup->id);

        $dbTaxSetup = $dbTaxSetup->toArray();
        $this->assertModelData($taxSetup->toArray(), $dbTaxSetup);
    }

    /**
     * @test update
     */
    public function test_update_tax_setup()
    {
        $taxSetup = factory(TaxSetup::class)->create();
        $fakeTaxSetup = factory(TaxSetup::class)->make()->toArray();

        $updatedTaxSetup = $this->taxSetupRepo->update($fakeTaxSetup, $taxSetup->id);

        $this->assertModelData($fakeTaxSetup, $updatedTaxSetup->toArray());
        $dbTaxSetup = $this->taxSetupRepo->find($taxSetup->id);
        $this->assertModelData($fakeTaxSetup, $dbTaxSetup->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_tax_setup()
    {
        $taxSetup = factory(TaxSetup::class)->create();

        $resp = $this->taxSetupRepo->delete($taxSetup->id);

        $this->assertTrue($resp);
        $this->assertNull(TaxSetup::find($taxSetup->id), 'TaxSetup should not exist in DB');
    }
}
