<?php namespace Tests\Repositories;

use App\Models\Suggested_investigations_patient;
use App\Repositories\Suggested_investigations_patientRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class Suggested_investigations_patientRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var Suggested_investigations_patientRepository
     */
    protected $suggestedInvestigationsPatientRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->suggestedInvestigationsPatientRepo = \App::make(Suggested_investigations_patientRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_suggested_investigations_patient()
    {
        $suggestedInvestigationsPatient = factory(Suggested_investigations_patient::class)->make()->toArray();

        $createdSuggested_investigations_patient = $this->suggestedInvestigationsPatientRepo->create($suggestedInvestigationsPatient);

        $createdSuggested_investigations_patient = $createdSuggested_investigations_patient->toArray();
        $this->assertArrayHasKey('id', $createdSuggested_investigations_patient);
        $this->assertNotNull($createdSuggested_investigations_patient['id'], 'Created Suggested_investigations_patient must have id specified');
        $this->assertNotNull(Suggested_investigations_patient::find($createdSuggested_investigations_patient['id']), 'Suggested_investigations_patient with given id must be in DB');
        $this->assertModelData($suggestedInvestigationsPatient, $createdSuggested_investigations_patient);
    }

    /**
     * @test read
     */
    public function test_read_suggested_investigations_patient()
    {
        $suggestedInvestigationsPatient = factory(Suggested_investigations_patient::class)->create();

        $dbSuggested_investigations_patient = $this->suggestedInvestigationsPatientRepo->find($suggestedInvestigationsPatient->id);

        $dbSuggested_investigations_patient = $dbSuggested_investigations_patient->toArray();
        $this->assertModelData($suggestedInvestigationsPatient->toArray(), $dbSuggested_investigations_patient);
    }

    /**
     * @test update
     */
    public function test_update_suggested_investigations_patient()
    {
        $suggestedInvestigationsPatient = factory(Suggested_investigations_patient::class)->create();
        $fakeSuggested_investigations_patient = factory(Suggested_investigations_patient::class)->make()->toArray();

        $updatedSuggested_investigations_patient = $this->suggestedInvestigationsPatientRepo->update($fakeSuggested_investigations_patient, $suggestedInvestigationsPatient->id);

        $this->assertModelData($fakeSuggested_investigations_patient, $updatedSuggested_investigations_patient->toArray());
        $dbSuggested_investigations_patient = $this->suggestedInvestigationsPatientRepo->find($suggestedInvestigationsPatient->id);
        $this->assertModelData($fakeSuggested_investigations_patient, $dbSuggested_investigations_patient->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_suggested_investigations_patient()
    {
        $suggestedInvestigationsPatient = factory(Suggested_investigations_patient::class)->create();

        $resp = $this->suggestedInvestigationsPatientRepo->delete($suggestedInvestigationsPatient->id);

        $this->assertTrue($resp);
        $this->assertNull(Suggested_investigations_patient::find($suggestedInvestigationsPatient->id), 'Suggested_investigations_patient should not exist in DB');
    }
}
