<?php namespace Tests\Repositories;

use App\Models\Suggested_imagings_patient;
use App\Repositories\Suggested_imagings_patientRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class Suggested_imagings_patientRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var Suggested_imagings_patientRepository
     */
    protected $suggestedImagingsPatientRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->suggestedImagingsPatientRepo = \App::make(Suggested_imagings_patientRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_suggested_imagings_patient()
    {
        $suggestedImagingsPatient = factory(Suggested_imagings_patient::class)->make()->toArray();

        $createdSuggested_imagings_patient = $this->suggestedImagingsPatientRepo->create($suggestedImagingsPatient);

        $createdSuggested_imagings_patient = $createdSuggested_imagings_patient->toArray();
        $this->assertArrayHasKey('id', $createdSuggested_imagings_patient);
        $this->assertNotNull($createdSuggested_imagings_patient['id'], 'Created Suggested_imagings_patient must have id specified');
        $this->assertNotNull(Suggested_imagings_patient::find($createdSuggested_imagings_patient['id']), 'Suggested_imagings_patient with given id must be in DB');
        $this->assertModelData($suggestedImagingsPatient, $createdSuggested_imagings_patient);
    }

    /**
     * @test read
     */
    public function test_read_suggested_imagings_patient()
    {
        $suggestedImagingsPatient = factory(Suggested_imagings_patient::class)->create();

        $dbSuggested_imagings_patient = $this->suggestedImagingsPatientRepo->find($suggestedImagingsPatient->id);

        $dbSuggested_imagings_patient = $dbSuggested_imagings_patient->toArray();
        $this->assertModelData($suggestedImagingsPatient->toArray(), $dbSuggested_imagings_patient);
    }

    /**
     * @test update
     */
    public function test_update_suggested_imagings_patient()
    {
        $suggestedImagingsPatient = factory(Suggested_imagings_patient::class)->create();
        $fakeSuggested_imagings_patient = factory(Suggested_imagings_patient::class)->make()->toArray();

        $updatedSuggested_imagings_patient = $this->suggestedImagingsPatientRepo->update($fakeSuggested_imagings_patient, $suggestedImagingsPatient->id);

        $this->assertModelData($fakeSuggested_imagings_patient, $updatedSuggested_imagings_patient->toArray());
        $dbSuggested_imagings_patient = $this->suggestedImagingsPatientRepo->find($suggestedImagingsPatient->id);
        $this->assertModelData($fakeSuggested_imagings_patient, $dbSuggested_imagings_patient->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_suggested_imagings_patient()
    {
        $suggestedImagingsPatient = factory(Suggested_imagings_patient::class)->create();

        $resp = $this->suggestedImagingsPatientRepo->delete($suggestedImagingsPatient->id);

        $this->assertTrue($resp);
        $this->assertNull(Suggested_imagings_patient::find($suggestedImagingsPatient->id), 'Suggested_imagings_patient should not exist in DB');
    }
}
