<?php namespace Tests\Repositories;

use App\Models\Video_like_dislike;
use App\Repositories\Video_like_dislikeRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class Video_like_dislikeRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var Video_like_dislikeRepository
     */
    protected $videoLikeDislikeRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->videoLikeDislikeRepo = \App::make(Video_like_dislikeRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_video_like_dislike()
    {
        $videoLikeDislike = factory(Video_like_dislike::class)->make()->toArray();

        $createdVideo_like_dislike = $this->videoLikeDislikeRepo->create($videoLikeDislike);

        $createdVideo_like_dislike = $createdVideo_like_dislike->toArray();
        $this->assertArrayHasKey('id', $createdVideo_like_dislike);
        $this->assertNotNull($createdVideo_like_dislike['id'], 'Created Video_like_dislike must have id specified');
        $this->assertNotNull(Video_like_dislike::find($createdVideo_like_dislike['id']), 'Video_like_dislike with given id must be in DB');
        $this->assertModelData($videoLikeDislike, $createdVideo_like_dislike);
    }

    /**
     * @test read
     */
    public function test_read_video_like_dislike()
    {
        $videoLikeDislike = factory(Video_like_dislike::class)->create();

        $dbVideo_like_dislike = $this->videoLikeDislikeRepo->find($videoLikeDislike->id);

        $dbVideo_like_dislike = $dbVideo_like_dislike->toArray();
        $this->assertModelData($videoLikeDislike->toArray(), $dbVideo_like_dislike);
    }

    /**
     * @test update
     */
    public function test_update_video_like_dislike()
    {
        $videoLikeDislike = factory(Video_like_dislike::class)->create();
        $fakeVideo_like_dislike = factory(Video_like_dislike::class)->make()->toArray();

        $updatedVideo_like_dislike = $this->videoLikeDislikeRepo->update($fakeVideo_like_dislike, $videoLikeDislike->id);

        $this->assertModelData($fakeVideo_like_dislike, $updatedVideo_like_dislike->toArray());
        $dbVideo_like_dislike = $this->videoLikeDislikeRepo->find($videoLikeDislike->id);
        $this->assertModelData($fakeVideo_like_dislike, $dbVideo_like_dislike->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_video_like_dislike()
    {
        $videoLikeDislike = factory(Video_like_dislike::class)->create();

        $resp = $this->videoLikeDislikeRepo->delete($videoLikeDislike->id);

        $this->assertTrue($resp);
        $this->assertNull(Video_like_dislike::find($videoLikeDislike->id), 'Video_like_dislike should not exist in DB');
    }
}
