<?php namespace Tests\Repositories;

use App\Models\Present_complaint_patient;
use App\Repositories\Present_complaint_patientRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class Present_complaint_patientRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var Present_complaint_patientRepository
     */
    protected $presentComplaintPatientRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->presentComplaintPatientRepo = \App::make(Present_complaint_patientRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_present_complaint_patient()
    {
        $presentComplaintPatient = factory(Present_complaint_patient::class)->make()->toArray();

        $createdPresent_complaint_patient = $this->presentComplaintPatientRepo->create($presentComplaintPatient);

        $createdPresent_complaint_patient = $createdPresent_complaint_patient->toArray();
        $this->assertArrayHasKey('id', $createdPresent_complaint_patient);
        $this->assertNotNull($createdPresent_complaint_patient['id'], 'Created Present_complaint_patient must have id specified');
        $this->assertNotNull(Present_complaint_patient::find($createdPresent_complaint_patient['id']), 'Present_complaint_patient with given id must be in DB');
        $this->assertModelData($presentComplaintPatient, $createdPresent_complaint_patient);
    }

    /**
     * @test read
     */
    public function test_read_present_complaint_patient()
    {
        $presentComplaintPatient = factory(Present_complaint_patient::class)->create();

        $dbPresent_complaint_patient = $this->presentComplaintPatientRepo->find($presentComplaintPatient->id);

        $dbPresent_complaint_patient = $dbPresent_complaint_patient->toArray();
        $this->assertModelData($presentComplaintPatient->toArray(), $dbPresent_complaint_patient);
    }

    /**
     * @test update
     */
    public function test_update_present_complaint_patient()
    {
        $presentComplaintPatient = factory(Present_complaint_patient::class)->create();
        $fakePresent_complaint_patient = factory(Present_complaint_patient::class)->make()->toArray();

        $updatedPresent_complaint_patient = $this->presentComplaintPatientRepo->update($fakePresent_complaint_patient, $presentComplaintPatient->id);

        $this->assertModelData($fakePresent_complaint_patient, $updatedPresent_complaint_patient->toArray());
        $dbPresent_complaint_patient = $this->presentComplaintPatientRepo->find($presentComplaintPatient->id);
        $this->assertModelData($fakePresent_complaint_patient, $dbPresent_complaint_patient->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_present_complaint_patient()
    {
        $presentComplaintPatient = factory(Present_complaint_patient::class)->create();

        $resp = $this->presentComplaintPatientRepo->delete($presentComplaintPatient->id);

        $this->assertTrue($resp);
        $this->assertNull(Present_complaint_patient::find($presentComplaintPatient->id), 'Present_complaint_patient should not exist in DB');
    }
}
