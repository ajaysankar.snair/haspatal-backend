<?php namespace Tests\Repositories;

use App\Models\Examination_notes_patient;
use App\Repositories\Examination_notes_patientRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class Examination_notes_patientRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var Examination_notes_patientRepository
     */
    protected $examinationNotesPatientRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->examinationNotesPatientRepo = \App::make(Examination_notes_patientRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_examination_notes_patient()
    {
        $examinationNotesPatient = factory(Examination_notes_patient::class)->make()->toArray();

        $createdExamination_notes_patient = $this->examinationNotesPatientRepo->create($examinationNotesPatient);

        $createdExamination_notes_patient = $createdExamination_notes_patient->toArray();
        $this->assertArrayHasKey('id', $createdExamination_notes_patient);
        $this->assertNotNull($createdExamination_notes_patient['id'], 'Created Examination_notes_patient must have id specified');
        $this->assertNotNull(Examination_notes_patient::find($createdExamination_notes_patient['id']), 'Examination_notes_patient with given id must be in DB');
        $this->assertModelData($examinationNotesPatient, $createdExamination_notes_patient);
    }

    /**
     * @test read
     */
    public function test_read_examination_notes_patient()
    {
        $examinationNotesPatient = factory(Examination_notes_patient::class)->create();

        $dbExamination_notes_patient = $this->examinationNotesPatientRepo->find($examinationNotesPatient->id);

        $dbExamination_notes_patient = $dbExamination_notes_patient->toArray();
        $this->assertModelData($examinationNotesPatient->toArray(), $dbExamination_notes_patient);
    }

    /**
     * @test update
     */
    public function test_update_examination_notes_patient()
    {
        $examinationNotesPatient = factory(Examination_notes_patient::class)->create();
        $fakeExamination_notes_patient = factory(Examination_notes_patient::class)->make()->toArray();

        $updatedExamination_notes_patient = $this->examinationNotesPatientRepo->update($fakeExamination_notes_patient, $examinationNotesPatient->id);

        $this->assertModelData($fakeExamination_notes_patient, $updatedExamination_notes_patient->toArray());
        $dbExamination_notes_patient = $this->examinationNotesPatientRepo->find($examinationNotesPatient->id);
        $this->assertModelData($fakeExamination_notes_patient, $dbExamination_notes_patient->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_examination_notes_patient()
    {
        $examinationNotesPatient = factory(Examination_notes_patient::class)->create();

        $resp = $this->examinationNotesPatientRepo->delete($examinationNotesPatient->id);

        $this->assertTrue($resp);
        $this->assertNull(Examination_notes_patient::find($examinationNotesPatient->id), 'Examination_notes_patient should not exist in DB');
    }
}
