<?php namespace Tests\Repositories;

use App\Models\Wallet_360;
use App\Repositories\Wallet_360Repository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class Wallet_360RepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var Wallet_360Repository
     */
    protected $wallet360Repo;

    public function setUp() : void
    {
        parent::setUp();
        $this->wallet360Repo = \App::make(Wallet_360Repository::class);
    }

    /**
     * @test create
     */
    public function test_create_wallet_360()
    {
        $wallet360 = factory(Wallet_360::class)->make()->toArray();

        $createdWallet_360 = $this->wallet360Repo->create($wallet360);

        $createdWallet_360 = $createdWallet_360->toArray();
        $this->assertArrayHasKey('id', $createdWallet_360);
        $this->assertNotNull($createdWallet_360['id'], 'Created Wallet_360 must have id specified');
        $this->assertNotNull(Wallet_360::find($createdWallet_360['id']), 'Wallet_360 with given id must be in DB');
        $this->assertModelData($wallet360, $createdWallet_360);
    }

    /**
     * @test read
     */
    public function test_read_wallet_360()
    {
        $wallet360 = factory(Wallet_360::class)->create();

        $dbWallet_360 = $this->wallet360Repo->find($wallet360->id);

        $dbWallet_360 = $dbWallet_360->toArray();
        $this->assertModelData($wallet360->toArray(), $dbWallet_360);
    }

    /**
     * @test update
     */
    public function test_update_wallet_360()
    {
        $wallet360 = factory(Wallet_360::class)->create();
        $fakeWallet_360 = factory(Wallet_360::class)->make()->toArray();

        $updatedWallet_360 = $this->wallet360Repo->update($fakeWallet_360, $wallet360->id);

        $this->assertModelData($fakeWallet_360, $updatedWallet_360->toArray());
        $dbWallet_360 = $this->wallet360Repo->find($wallet360->id);
        $this->assertModelData($fakeWallet_360, $dbWallet_360->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_wallet_360()
    {
        $wallet360 = factory(Wallet_360::class)->create();

        $resp = $this->wallet360Repo->delete($wallet360->id);

        $this->assertTrue($resp);
        $this->assertNull(Wallet_360::find($wallet360->id), 'Wallet_360 should not exist in DB');
    }
}
