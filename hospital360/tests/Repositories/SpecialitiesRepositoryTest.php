<?php namespace Tests\Repositories;

use App\Models\Specialities;
use App\Repositories\SpecialitiesRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class SpecialitiesRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var SpecialitiesRepository
     */
    protected $specialitiesRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->specialitiesRepo = \App::make(SpecialitiesRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_specialities()
    {
        $specialities = factory(Specialities::class)->make()->toArray();

        $createdSpecialities = $this->specialitiesRepo->create($specialities);

        $createdSpecialities = $createdSpecialities->toArray();
        $this->assertArrayHasKey('id', $createdSpecialities);
        $this->assertNotNull($createdSpecialities['id'], 'Created Specialities must have id specified');
        $this->assertNotNull(Specialities::find($createdSpecialities['id']), 'Specialities with given id must be in DB');
        $this->assertModelData($specialities, $createdSpecialities);
    }

    /**
     * @test read
     */
    public function test_read_specialities()
    {
        $specialities = factory(Specialities::class)->create();

        $dbSpecialities = $this->specialitiesRepo->find($specialities->id);

        $dbSpecialities = $dbSpecialities->toArray();
        $this->assertModelData($specialities->toArray(), $dbSpecialities);
    }

    /**
     * @test update
     */
    public function test_update_specialities()
    {
        $specialities = factory(Specialities::class)->create();
        $fakeSpecialities = factory(Specialities::class)->make()->toArray();

        $updatedSpecialities = $this->specialitiesRepo->update($fakeSpecialities, $specialities->id);

        $this->assertModelData($fakeSpecialities, $updatedSpecialities->toArray());
        $dbSpecialities = $this->specialitiesRepo->find($specialities->id);
        $this->assertModelData($fakeSpecialities, $dbSpecialities->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_specialities()
    {
        $specialities = factory(Specialities::class)->create();

        $resp = $this->specialitiesRepo->delete($specialities->id);

        $this->assertTrue($resp);
        $this->assertNull(Specialities::find($specialities->id), 'Specialities should not exist in DB');
    }
}
