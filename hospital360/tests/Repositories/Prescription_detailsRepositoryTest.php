<?php namespace Tests\Repositories;

use App\Models\Prescription_details;
use App\Repositories\Prescription_detailsRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class Prescription_detailsRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var Prescription_detailsRepository
     */
    protected $prescriptionDetailsRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->prescriptionDetailsRepo = \App::make(Prescription_detailsRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_prescription_details()
    {
        $prescriptionDetails = factory(Prescription_details::class)->make()->toArray();

        $createdPrescription_details = $this->prescriptionDetailsRepo->create($prescriptionDetails);

        $createdPrescription_details = $createdPrescription_details->toArray();
        $this->assertArrayHasKey('id', $createdPrescription_details);
        $this->assertNotNull($createdPrescription_details['id'], 'Created Prescription_details must have id specified');
        $this->assertNotNull(Prescription_details::find($createdPrescription_details['id']), 'Prescription_details with given id must be in DB');
        $this->assertModelData($prescriptionDetails, $createdPrescription_details);
    }

    /**
     * @test read
     */
    public function test_read_prescription_details()
    {
        $prescriptionDetails = factory(Prescription_details::class)->create();

        $dbPrescription_details = $this->prescriptionDetailsRepo->find($prescriptionDetails->id);

        $dbPrescription_details = $dbPrescription_details->toArray();
        $this->assertModelData($prescriptionDetails->toArray(), $dbPrescription_details);
    }

    /**
     * @test update
     */
    public function test_update_prescription_details()
    {
        $prescriptionDetails = factory(Prescription_details::class)->create();
        $fakePrescription_details = factory(Prescription_details::class)->make()->toArray();

        $updatedPrescription_details = $this->prescriptionDetailsRepo->update($fakePrescription_details, $prescriptionDetails->id);

        $this->assertModelData($fakePrescription_details, $updatedPrescription_details->toArray());
        $dbPrescription_details = $this->prescriptionDetailsRepo->find($prescriptionDetails->id);
        $this->assertModelData($fakePrescription_details, $dbPrescription_details->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_prescription_details()
    {
        $prescriptionDetails = factory(Prescription_details::class)->create();

        $resp = $this->prescriptionDetailsRepo->delete($prescriptionDetails->id);

        $this->assertTrue($resp);
        $this->assertNull(Prescription_details::find($prescriptionDetails->id), 'Prescription_details should not exist in DB');
    }
}
