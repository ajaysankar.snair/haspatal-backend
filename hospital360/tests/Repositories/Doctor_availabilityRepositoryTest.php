<?php namespace Tests\Repositories;

use App\Models\Doctor_availability;
use App\Repositories\Doctor_availabilityRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class Doctor_availabilityRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var Doctor_availabilityRepository
     */
    protected $doctorAvailabilityRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->doctorAvailabilityRepo = \App::make(Doctor_availabilityRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_doctor_availability()
    {
        $doctorAvailability = factory(Doctor_availability::class)->make()->toArray();

        $createdDoctor_availability = $this->doctorAvailabilityRepo->create($doctorAvailability);

        $createdDoctor_availability = $createdDoctor_availability->toArray();
        $this->assertArrayHasKey('id', $createdDoctor_availability);
        $this->assertNotNull($createdDoctor_availability['id'], 'Created Doctor_availability must have id specified');
        $this->assertNotNull(Doctor_availability::find($createdDoctor_availability['id']), 'Doctor_availability with given id must be in DB');
        $this->assertModelData($doctorAvailability, $createdDoctor_availability);
    }

    /**
     * @test read
     */
    public function test_read_doctor_availability()
    {
        $doctorAvailability = factory(Doctor_availability::class)->create();

        $dbDoctor_availability = $this->doctorAvailabilityRepo->find($doctorAvailability->id);

        $dbDoctor_availability = $dbDoctor_availability->toArray();
        $this->assertModelData($doctorAvailability->toArray(), $dbDoctor_availability);
    }

    /**
     * @test update
     */
    public function test_update_doctor_availability()
    {
        $doctorAvailability = factory(Doctor_availability::class)->create();
        $fakeDoctor_availability = factory(Doctor_availability::class)->make()->toArray();

        $updatedDoctor_availability = $this->doctorAvailabilityRepo->update($fakeDoctor_availability, $doctorAvailability->id);

        $this->assertModelData($fakeDoctor_availability, $updatedDoctor_availability->toArray());
        $dbDoctor_availability = $this->doctorAvailabilityRepo->find($doctorAvailability->id);
        $this->assertModelData($fakeDoctor_availability, $dbDoctor_availability->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_doctor_availability()
    {
        $doctorAvailability = factory(Doctor_availability::class)->create();

        $resp = $this->doctorAvailabilityRepo->delete($doctorAvailability->id);

        $this->assertTrue($resp);
        $this->assertNull(Doctor_availability::find($doctorAvailability->id), 'Doctor_availability should not exist in DB');
    }
}
