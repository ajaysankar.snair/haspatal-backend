<?php namespace Tests\Repositories;

use App\Models\Set_service_price;
use App\Repositories\Set_service_priceRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class Set_service_priceRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var Set_service_priceRepository
     */
    protected $setServicePriceRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->setServicePriceRepo = \App::make(Set_service_priceRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_set_service_price()
    {
        $setServicePrice = factory(Set_service_price::class)->make()->toArray();

        $createdSet_service_price = $this->setServicePriceRepo->create($setServicePrice);

        $createdSet_service_price = $createdSet_service_price->toArray();
        $this->assertArrayHasKey('id', $createdSet_service_price);
        $this->assertNotNull($createdSet_service_price['id'], 'Created Set_service_price must have id specified');
        $this->assertNotNull(Set_service_price::find($createdSet_service_price['id']), 'Set_service_price with given id must be in DB');
        $this->assertModelData($setServicePrice, $createdSet_service_price);
    }

    /**
     * @test read
     */
    public function test_read_set_service_price()
    {
        $setServicePrice = factory(Set_service_price::class)->create();

        $dbSet_service_price = $this->setServicePriceRepo->find($setServicePrice->id);

        $dbSet_service_price = $dbSet_service_price->toArray();
        $this->assertModelData($setServicePrice->toArray(), $dbSet_service_price);
    }

    /**
     * @test update
     */
    public function test_update_set_service_price()
    {
        $setServicePrice = factory(Set_service_price::class)->create();
        $fakeSet_service_price = factory(Set_service_price::class)->make()->toArray();

        $updatedSet_service_price = $this->setServicePriceRepo->update($fakeSet_service_price, $setServicePrice->id);

        $this->assertModelData($fakeSet_service_price, $updatedSet_service_price->toArray());
        $dbSet_service_price = $this->setServicePriceRepo->find($setServicePrice->id);
        $this->assertModelData($fakeSet_service_price, $dbSet_service_price->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_set_service_price()
    {
        $setServicePrice = factory(Set_service_price::class)->create();

        $resp = $this->setServicePriceRepo->delete($setServicePrice->id);

        $this->assertTrue($resp);
        $this->assertNull(Set_service_price::find($setServicePrice->id), 'Set_service_price should not exist in DB');
    }
}
