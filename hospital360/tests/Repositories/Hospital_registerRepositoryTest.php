<?php namespace Tests\Repositories;

use App\Models\Hospital_register;
use App\Repositories\Hospital_registerRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class Hospital_registerRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var Hospital_registerRepository
     */
    protected $hospitalRegisterRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->hospitalRegisterRepo = \App::make(Hospital_registerRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_hospital_register()
    {
        $hospitalRegister = factory(Hospital_register::class)->make()->toArray();

        $createdHospital_register = $this->hospitalRegisterRepo->create($hospitalRegister);

        $createdHospital_register = $createdHospital_register->toArray();
        $this->assertArrayHasKey('id', $createdHospital_register);
        $this->assertNotNull($createdHospital_register['id'], 'Created Hospital_register must have id specified');
        $this->assertNotNull(Hospital_register::find($createdHospital_register['id']), 'Hospital_register with given id must be in DB');
        $this->assertModelData($hospitalRegister, $createdHospital_register);
    }

    /**
     * @test read
     */
    public function test_read_hospital_register()
    {
        $hospitalRegister = factory(Hospital_register::class)->create();

        $dbHospital_register = $this->hospitalRegisterRepo->find($hospitalRegister->id);

        $dbHospital_register = $dbHospital_register->toArray();
        $this->assertModelData($hospitalRegister->toArray(), $dbHospital_register);
    }

    /**
     * @test update
     */
    public function test_update_hospital_register()
    {
        $hospitalRegister = factory(Hospital_register::class)->create();
        $fakeHospital_register = factory(Hospital_register::class)->make()->toArray();

        $updatedHospital_register = $this->hospitalRegisterRepo->update($fakeHospital_register, $hospitalRegister->id);

        $this->assertModelData($fakeHospital_register, $updatedHospital_register->toArray());
        $dbHospital_register = $this->hospitalRegisterRepo->find($hospitalRegister->id);
        $this->assertModelData($fakeHospital_register, $dbHospital_register->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_hospital_register()
    {
        $hospitalRegister = factory(Hospital_register::class)->create();

        $resp = $this->hospitalRegisterRepo->delete($hospitalRegister->id);

        $this->assertTrue($resp);
        $this->assertNull(Hospital_register::find($hospitalRegister->id), 'Hospital_register should not exist in DB');
    }
}
