<?php namespace Tests\Repositories;

use App\Models\Order_360;
use App\Repositories\Order_360Repository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class Order_360RepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var Order_360Repository
     */
    protected $order360Repo;

    public function setUp() : void
    {
        parent::setUp();
        $this->order360Repo = \App::make(Order_360Repository::class);
    }

    /**
     * @test create
     */
    public function test_create_order_360()
    {
        $order360 = factory(Order_360::class)->make()->toArray();

        $createdOrder_360 = $this->order360Repo->create($order360);

        $createdOrder_360 = $createdOrder_360->toArray();
        $this->assertArrayHasKey('id', $createdOrder_360);
        $this->assertNotNull($createdOrder_360['id'], 'Created Order_360 must have id specified');
        $this->assertNotNull(Order_360::find($createdOrder_360['id']), 'Order_360 with given id must be in DB');
        $this->assertModelData($order360, $createdOrder_360);
    }

    /**
     * @test read
     */
    public function test_read_order_360()
    {
        $order360 = factory(Order_360::class)->create();

        $dbOrder_360 = $this->order360Repo->find($order360->id);

        $dbOrder_360 = $dbOrder_360->toArray();
        $this->assertModelData($order360->toArray(), $dbOrder_360);
    }

    /**
     * @test update
     */
    public function test_update_order_360()
    {
        $order360 = factory(Order_360::class)->create();
        $fakeOrder_360 = factory(Order_360::class)->make()->toArray();

        $updatedOrder_360 = $this->order360Repo->update($fakeOrder_360, $order360->id);

        $this->assertModelData($fakeOrder_360, $updatedOrder_360->toArray());
        $dbOrder_360 = $this->order360Repo->find($order360->id);
        $this->assertModelData($fakeOrder_360, $dbOrder_360->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_order_360()
    {
        $order360 = factory(Order_360::class)->create();

        $resp = $this->order360Repo->delete($order360->id);

        $this->assertTrue($resp);
        $this->assertNull(Order_360::find($order360->id), 'Order_360 should not exist in DB');
    }
}
