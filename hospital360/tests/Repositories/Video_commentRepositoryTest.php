<?php namespace Tests\Repositories;

use App\Models\Video_comment;
use App\Repositories\Video_commentRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class Video_commentRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var Video_commentRepository
     */
    protected $videoCommentRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->videoCommentRepo = \App::make(Video_commentRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_video_comment()
    {
        $videoComment = factory(Video_comment::class)->make()->toArray();

        $createdVideo_comment = $this->videoCommentRepo->create($videoComment);

        $createdVideo_comment = $createdVideo_comment->toArray();
        $this->assertArrayHasKey('id', $createdVideo_comment);
        $this->assertNotNull($createdVideo_comment['id'], 'Created Video_comment must have id specified');
        $this->assertNotNull(Video_comment::find($createdVideo_comment['id']), 'Video_comment with given id must be in DB');
        $this->assertModelData($videoComment, $createdVideo_comment);
    }

    /**
     * @test read
     */
    public function test_read_video_comment()
    {
        $videoComment = factory(Video_comment::class)->create();

        $dbVideo_comment = $this->videoCommentRepo->find($videoComment->id);

        $dbVideo_comment = $dbVideo_comment->toArray();
        $this->assertModelData($videoComment->toArray(), $dbVideo_comment);
    }

    /**
     * @test update
     */
    public function test_update_video_comment()
    {
        $videoComment = factory(Video_comment::class)->create();
        $fakeVideo_comment = factory(Video_comment::class)->make()->toArray();

        $updatedVideo_comment = $this->videoCommentRepo->update($fakeVideo_comment, $videoComment->id);

        $this->assertModelData($fakeVideo_comment, $updatedVideo_comment->toArray());
        $dbVideo_comment = $this->videoCommentRepo->find($videoComment->id);
        $this->assertModelData($fakeVideo_comment, $dbVideo_comment->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_video_comment()
    {
        $videoComment = factory(Video_comment::class)->create();

        $resp = $this->videoCommentRepo->delete($videoComment->id);

        $this->assertTrue($resp);
        $this->assertNull(Video_comment::find($videoComment->id), 'Video_comment should not exist in DB');
    }
}
