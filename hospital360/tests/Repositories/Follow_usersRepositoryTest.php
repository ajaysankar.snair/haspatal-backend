<?php namespace Tests\Repositories;

use App\Models\Follow_users;
use App\Repositories\Follow_usersRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class Follow_usersRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var Follow_usersRepository
     */
    protected $followUsersRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->followUsersRepo = \App::make(Follow_usersRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_follow_users()
    {
        $followUsers = factory(Follow_users::class)->make()->toArray();

        $createdFollow_users = $this->followUsersRepo->create($followUsers);

        $createdFollow_users = $createdFollow_users->toArray();
        $this->assertArrayHasKey('id', $createdFollow_users);
        $this->assertNotNull($createdFollow_users['id'], 'Created Follow_users must have id specified');
        $this->assertNotNull(Follow_users::find($createdFollow_users['id']), 'Follow_users with given id must be in DB');
        $this->assertModelData($followUsers, $createdFollow_users);
    }

    /**
     * @test read
     */
    public function test_read_follow_users()
    {
        $followUsers = factory(Follow_users::class)->create();

        $dbFollow_users = $this->followUsersRepo->find($followUsers->id);

        $dbFollow_users = $dbFollow_users->toArray();
        $this->assertModelData($followUsers->toArray(), $dbFollow_users);
    }

    /**
     * @test update
     */
    public function test_update_follow_users()
    {
        $followUsers = factory(Follow_users::class)->create();
        $fakeFollow_users = factory(Follow_users::class)->make()->toArray();

        $updatedFollow_users = $this->followUsersRepo->update($fakeFollow_users, $followUsers->id);

        $this->assertModelData($fakeFollow_users, $updatedFollow_users->toArray());
        $dbFollow_users = $this->followUsersRepo->find($followUsers->id);
        $this->assertModelData($fakeFollow_users, $dbFollow_users->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_follow_users()
    {
        $followUsers = factory(Follow_users::class)->create();

        $resp = $this->followUsersRepo->delete($followUsers->id);

        $this->assertTrue($resp);
        $this->assertNull(Follow_users::find($followUsers->id), 'Follow_users should not exist in DB');
    }
}
