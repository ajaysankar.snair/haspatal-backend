<?php namespace Tests\Repositories;

use App\Models\Your_coverage_360;
use App\Repositories\Your_coverage_360Repository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class Your_coverage_360RepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var Your_coverage_360Repository
     */
    protected $yourCoverage360Repo;

    public function setUp() : void
    {
        parent::setUp();
        $this->yourCoverage360Repo = \App::make(Your_coverage_360Repository::class);
    }

    /**
     * @test create
     */
    public function test_create_your_coverage_360()
    {
        $yourCoverage360 = factory(Your_coverage_360::class)->make()->toArray();

        $createdYour_coverage_360 = $this->yourCoverage360Repo->create($yourCoverage360);

        $createdYour_coverage_360 = $createdYour_coverage_360->toArray();
        $this->assertArrayHasKey('id', $createdYour_coverage_360);
        $this->assertNotNull($createdYour_coverage_360['id'], 'Created Your_coverage_360 must have id specified');
        $this->assertNotNull(Your_coverage_360::find($createdYour_coverage_360['id']), 'Your_coverage_360 with given id must be in DB');
        $this->assertModelData($yourCoverage360, $createdYour_coverage_360);
    }

    /**
     * @test read
     */
    public function test_read_your_coverage_360()
    {
        $yourCoverage360 = factory(Your_coverage_360::class)->create();

        $dbYour_coverage_360 = $this->yourCoverage360Repo->find($yourCoverage360->id);

        $dbYour_coverage_360 = $dbYour_coverage_360->toArray();
        $this->assertModelData($yourCoverage360->toArray(), $dbYour_coverage_360);
    }

    /**
     * @test update
     */
    public function test_update_your_coverage_360()
    {
        $yourCoverage360 = factory(Your_coverage_360::class)->create();
        $fakeYour_coverage_360 = factory(Your_coverage_360::class)->make()->toArray();

        $updatedYour_coverage_360 = $this->yourCoverage360Repo->update($fakeYour_coverage_360, $yourCoverage360->id);

        $this->assertModelData($fakeYour_coverage_360, $updatedYour_coverage_360->toArray());
        $dbYour_coverage_360 = $this->yourCoverage360Repo->find($yourCoverage360->id);
        $this->assertModelData($fakeYour_coverage_360, $dbYour_coverage_360->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_your_coverage_360()
    {
        $yourCoverage360 = factory(Your_coverage_360::class)->create();

        $resp = $this->yourCoverage360Repo->delete($yourCoverage360->id);

        $this->assertTrue($resp);
        $this->assertNull(Your_coverage_360::find($yourCoverage360->id), 'Your_coverage_360 should not exist in DB');
    }
}
