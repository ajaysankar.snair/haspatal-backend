<?php

use Illuminate\Routing\Router;

Admin::routes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
    'as'            => config('admin.route.prefix') . '.',
], function (Router $router) {

    $router->get('/', 'HomeController@index')->name('home');
    $router->resource('users', UserController::class);
    $router->resource('couponrates', CouponController::class);
    $router->resource('hospital_registers', hospitalrController::class);
    $router->resource('business_registers', AcesspointController::class);
    $router->resource('coupans', coupanController::class);
    $router->resource('doctors', DoctorsController::class);
    $router->resource('specialities', variensCategoryconfigController::class);
    $router->resource('reviews', ReviewPatientController::class);
    $router->resource('usersHaspatal', haspataluserController::class);

});
