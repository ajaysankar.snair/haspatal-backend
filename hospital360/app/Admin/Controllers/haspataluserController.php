<?php

namespace App\Admin\Controllers;

use App\User;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class haspataluserController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'User';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new User());
        $grid->filter(function($filter){

            // Remove the default id filter
            $filter->disableIdFilter();
        
            // Add a column filter
            $filter->like('email', 'Email');
            $filter->like('mobile', 'Mobile');
          
        
        });
        $grid->column('id', __('Id'));
        // $grid->column('username', __('Username'));
        $grid->column('first_name', __('First name'));
        // $grid->column('last_name', __('Last name'));
        // $grid->column('uid', __('Uid'));
        // $grid->column('plan_id', __('Plan id'));
        $grid->column('email', __('Email'));
        $grid->column('mobile', __('Mobile'));
        // $grid->column('email_verified_at', __('Email verified at'));
        // $grid->column('password', __('Password'));
        // $grid->column('remember_token', __('Remember token'));
        // $grid->column('version', __('Version'));
        // $grid->column('device', __('Device'));
        // $grid->column('mobile_token', __('Mobile token'));
        // $grid->column('status', __('Status'));
        // $grid->column('role_id', __('Role id'));
        // $grid->column('user_type', __('User type'));
        // $grid->column('userDetailsId', __('UserDetailsId'));
        // $grid->column('patient_login_status', __('Patient login status'));
        // $grid->column('patient_login_status_contactinfo', __('Patient login status contactinfo'));
        // $grid->column('patient_login_status_addressinfo', __('Patient login status addressinfo'));
        // $grid->column('doctor_login_status', __('Doctor login status'));
        // $grid->column('doctor_login_status_mobile', __('Doctor login status mobile'));
        // $grid->column('doctor_plan_status', __('Doctor plan status'));
        // $grid->column('created_at', __('Created at'));
        // $grid->column('updated_at', __('Updated at'));
        // $grid->column('mobile2', __('Mobile2'));
        // $grid->column('reg_status', __('Reg status'));
        // $grid->column('OTP', __('OTP'));
        // $grid->column('updated_by', __('Updated by'));
        $grid->disableCreation();
        $grid->actions(function ($actions)
        {

            $actions->disableEdit();
        });
        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(User::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('username', __('Username'));
        $show->field('first_name', __('First name'));
        $show->field('last_name', __('Last name'));
        $show->field('uid', __('Uid'));
        $show->field('plan_id', __('Plan id'));
        $show->field('email', __('Email'));
        $show->field('mobile', __('Mobile'));
        $show->field('email_verified_at', __('Email verified at'));
        $show->field('password', __('Password'));
        $show->field('remember_token', __('Remember token'));
        $show->field('version', __('Version'));
        $show->field('device', __('Device'));
        $show->field('mobile_token', __('Mobile token'));
        $show->field('status', __('Status'));
        $show->field('role_id', __('Role id'));
        $show->field('user_type', __('User type'));
        $show->field('userDetailsId', __('UserDetailsId'));
        $show->field('patient_login_status', __('Patient login status'));
        $show->field('patient_login_status_contactinfo', __('Patient login status contactinfo'));
        $show->field('patient_login_status_addressinfo', __('Patient login status addressinfo'));
        $show->field('doctor_login_status', __('Doctor login status'));
        $show->field('doctor_login_status_mobile', __('Doctor login status mobile'));
        $show->field('doctor_plan_status', __('Doctor plan status'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));
        $show->field('mobile2', __('Mobile2'));
        $show->field('reg_status', __('Reg status'));
        $show->field('OTP', __('OTP'));
        $show->field('updated_by', __('Updated by'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new User());

        $form->text('username', __('Username'));
        $form->text('first_name', __('First name'));
        $form->text('last_name', __('Last name'));
        $form->text('uid', __('Uid'));
        $form->number('plan_id', __('Plan id'));
        $form->email('email', __('Email'));
        $form->mobile('mobile', __('Mobile'));
        $form->datetime('email_verified_at', __('Email verified at'))->default(date('Y-m-d H:i:s'));
        $form->password('password', __('Password'));
        $form->text('remember_token', __('Remember token'));
        $form->number('version', __('Version'));
        $form->text('device', __('Device'));
        $form->text('mobile_token', __('Mobile token'));
        $form->switch('status', __('Status'))->default(1);
        $form->text('role_id', __('Role id'));
        $form->text('user_type', __('User type'));
        $form->text('userDetailsId', __('UserDetailsId'));
        $form->number('patient_login_status', __('Patient login status'));
        $form->switch('patient_login_status_contactinfo', __('Patient login status contactinfo'));
        $form->switch('patient_login_status_addressinfo', __('Patient login status addressinfo'));
        $form->number('doctor_login_status', __('Doctor login status'));
        $form->switch('doctor_login_status_mobile', __('Doctor login status mobile'));
        $form->switch('doctor_plan_status', __('Doctor plan status'));
        $form->text('mobile2', __('Mobile2'));
        $form->text('reg_status', __('Reg status'));
        $form->text('OTP', __('OTP'));
        $form->textarea('updated_by', __('Updated by'));

        return $form;
    }
}
