<?php

namespace App\Admin\Controllers;

use App\Models\Specialities;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class variensCategoryconfigController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Specialities';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Specialities());

        $grid->column('id', __('Id'));
        $grid->column('specility', __('Specility'))->sortable();;
        $grid->column('description', __('Description'));
        $grid->column('status', __('Status'));
        $grid->column('created_by', __('Created by'));
        $grid->column('updated_by', __('Updated by'));
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Specialities::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('specility', __('Specility'));
        $show->field('description', __('Description'));
        $show->field('status', __('Status'));
        $show->field('created_by', __('Created by'));
        $show->field('updated_by', __('Updated by'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Specialities());

        $form->text('specility', __('Specility'));
        $form->text('description', __('Description'));
        $form->number('status', __('Status'));
        $form->text('created_by', __('Created by'));
        $form->text('updated_by', __('Updated by'));

        return $form;
    }
}
