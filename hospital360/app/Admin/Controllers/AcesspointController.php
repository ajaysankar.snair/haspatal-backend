<?php

namespace App\Admin\Controllers;

use App\Models\Business_register;
use App\Models\HaspatalRegsiter;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class AcesspointController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Business_register';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $count = Business_register::select('id')->count();
        if ($count != 0)
        {
            $timeSlotsFilter = Business_register::select('hospitalID', 'id')->latest()
                ->first()
                ->get();
            foreach ($timeSlotsFilter as & $timeSlots)
            {
                $daySlotselection = $timeSlots['hospitalID'];
                $id = $timeSlots['id'];
                $weekSlotselction = HaspatalRegsiter::where('id', $daySlotselection)->pluck('hospital_name')->implode(', ');;
            
                $result = Business_register::where('id', $id)->update(['hospitalname' => $weekSlotselction]);
            }
        }

        $grid = new Grid(new Business_register());
$grid->filter(function($filter){

    // Remove the default id filter
    $filter->disableIdFilter();

    // Add a column filter
    $filter->like('business_registerID', 'business_registerID');
  

});

$grid->disableCreation();
        $grid->actions(function ($actions)
        {

            $actions->disableDelete();
        });
        $grid->tools(function ($tools)
        {
            $tools->batch(function ($batch)
            {
                $batch->disableDelete();
            });
        });
        $grid->column('id', __('Id'));
        // $grid->column('v_uni_id', __('V uni id'));
        // $grid->column('user_id', __('User id'));
        // $grid->column('b_id', __('B id'));
        $grid->column('b_name', __('B name'));
        $grid->column('hospitalname', __('Hospital'));
        // $grid->column('acess_status', __('AcessPoint Status'));
        $grid->column('acess_status')->display(function ($title, $column) {
    
            
            If ($this->acess_status == 0) {
                return "Hold";
            }else{
                return "Approved";
            }
            
            // Otherwise it is displayed as editable
            return $column->editable();
        });
        $grid->column('admin_status')->display(function ($title, $column) {
    
            
            If ($this->admin_status == 1) {
                return "Approved";
            }else{
                return "Hold";
            }
            
            // Otherwise it is displayed as editable
            return $column->editable();
        });
        // $grid->column('conatact_person', __('Conatact person'));
        // $grid->column('mobile1', __('Mobile1'));
        // $grid->column('mobile2', __('Mobile2'));
        // $grid->column('email', __('Email'));
        // $grid->column('gst_no', __('Gst no'));
        // $grid->column('working_hr', __('Working hr'));
        // $grid->column('open_time', __('Open time'));
        // $grid->column('close_time', __('Close time'));
        // $grid->column('open_24', __('Open 24'));
        // $grid->column('weekly_off', __('Weekly off'));
        // $grid->column('all_days_open', __('All days open'));
        // $grid->column('address', __('Address'));
        // $grid->column('country_id', __('Country id'));
        // $grid->column('state_id', __('State id'));
        // $grid->column('city_id', __('City id'));
        // $grid->column('pincode', __('Pincode'));
        // $grid->column('b_lic', __('B lic'));
        // $grid->column('b_card_pic', __('B card pic'));
        // $grid->column('shop_pic', __('Shop pic'));
        // $grid->column('image', __('Image'));
        // $grid->column('description', __('Description'));
        // $grid->column('patient_select_you', __('Patient select you'));
        // $grid->column('created_by', __('Created by'));
        // $grid->column('updated_by', __('Updated by'));
        // $grid->column('status', __('Status'));
        // $grid->column('step', __('Step'));
        // $grid->column('created_at', __('Created at'));
        // $grid->column('updated_at', __('Updated at'));
        // $grid->column('deleted_at', __('Deleted at'));
        // $grid->column('district', __('District'));
        // $grid->column('admin_status', __('Admin status'));
        // $grid->column('terms_status', __('Terms status'));
        $grid->column('business_registerID', __('Business registerID'));
        // $grid->column('city_name', __('City name'));
        // $grid->column('state_name', __('State name'));
        // $grid->column('is_discard', __('Is discard'));
        // $grid->column('your_coverage_status', __('Your coverage status'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Business_register::findOrFail($id));

        // $show->field('id', __('Id'));
        // $show->field('v_uni_id', __('V uni id'));
        // $show->field('user_id', __('User id'));
        $show->field('b_id', __('B id'));
        $show->field('b_name', __('B name'));
        // $show->field('conatact_person', __('Conatact person'));
        // $show->field('mobile1', __('Mobile1'));
        // $show->field('mobile2', __('Mobile2'));
        // $show->field('email', __('Email'));
        // $show->field('gst_no', __('Gst no'));
        // $show->field('working_hr', __('Working hr'));
        // $show->field('open_time', __('Open time'));
        // $show->field('close_time', __('Close time'));
        // $show->field('open_24', __('Open 24'));
        // $show->field('weekly_off', __('Weekly off'));
        // $show->field('all_days_open', __('All days open'));
        $show->field('address', __('Address'));
        // $show->field('country_id', __('Country id'));
        // $show->field('state_id', __('State id'));
        // $show->field('city_id', __('City id'));
        $show->field('pincode', __('Pincode'));
        // $show->field('b_lic', __('B lic'));
        // $show->field('b_card_pic', __('B card pic'));
        // $show->field('shop_pic', __('Shop pic'));
        // $show->field('image', __('Image'));
        $show->field('description', __('Description'));
        // $show->field('patient_select_you', __('Patient select you'));
        // $show->field('created_by', __('Created by'));
        // $show->field('updated_by', __('Updated by'));
        // $show->field('status', __('Status'));
        // $show->field('step', __('Step'));
        // $show->field('created_at', __('Created at'));
        // $show->field('updated_at', __('Updated at'));
        // $show->field('deleted_at', __('Deleted at'));
        // $show->field('district', __('District'));
        // $show->field('admin_status', __('Admin status'));
        // $show->field('terms_status', __('Terms status'));
        // $show->field('business_registerID', __('Business registerID'));
        // $show->field('city_name', __('City name'));
        // $show->field('state_name', __('State name'));
        // $show->field('is_discard', __('Is discard'));
        // $show->field('your_coverage_status', __('Your coverage status'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Business_register());
        $parameters = request()->route()->parameters();
        // dd( $id['business_register']);
       $id1= $parameters['business_register'];
       $adminstatus = Business_register::where('id',$id1)->first();
    //    dd( $adminstatus->admin_status);
        $Weekends = HaspatalRegsiter::all()->pluck('hospital_name', 'id');
       if( $adminstatus->admin_status==1){
        $form->select('hospitalID', 'Hospital')
            ->options($Weekends);
            $form->select('acess_status', 'Status')
            ->options([1 => 'Approve', 0 => 'Hold']);
    }
           
           



        // $form->text('v_uni_id', __('V uni id'));
        // $form->number('user_id', __('User id'));
        // $form->number('b_id', __('B id'));
        // $form->text('b_name', __('B name'));
        // $form->text('conatact_person', __('Conatact person'));
        // $form->text('mobile1', __('Mobile1'));
        // $form->text('mobile2', __('Mobile2'));
        // $form->email('email', __('Email'));
        // $form->text('gst_no', __('Gst no'));
        // $form->text('working_hr', __('Working hr'));
        // $form->text('open_time', __('Open time'));
        // $form->text('close_time', __('Close time'));
        // $form->text('open_24', __('Open 24'))->default('2');
        // $form->text('weekly_off', __('Weekly off'));
        // $form->text('all_days_open', __('All days open'))->default('2');
        // $form->text('address', __('Address'));
        // $form->number('country_id', __('Country id'))->default(3);
        // $form->number('state_id', __('State id'));
        // $form->number('city_id', __('City id'));
        // $form->textarea('pincode', __('Pincode'));
        // $form->text('b_lic', __('B lic'));
        // $form->text('b_card_pic', __('B card pic'));
        // $form->text('shop_pic', __('Shop pic'));
        // $form->image('image', __('Image'));
        // $form->text('description', __('Description'));
        // $form->textarea('patient_select_you', __('Patient select you'));
        // $form->number('created_by', __('Created by'));
        // $form->number('updated_by', __('Updated by'));
        // $form->number('status', __('Status'));
        // $form->switch('step', __('Step'));
        // $form->text('district', __('District'));
        // $form->text('admin_status', __('Admin status'));
        // $form->text('terms_status', __('Terms status'));
        // $form->text('business_registerID', __('Business registerID'));
        // $form->textarea('city_name', __('City name'));
        // $form->textarea('state_name', __('State name'));
        // $form->number('is_discard', __('Is discard'));
        // $form->text('your_coverage_status', __('Your coverage status'));
        $form->footer(function ($footer)
        {
            $footer->disableViewCheck();
            $footer->disableEditingCheck();
            $footer->disableCreatingCheck();
        });
        $form->tools(function (Form\Tools $tools)
        {
            $tools->disableDelete();
        });
        return $form;
    }
}
