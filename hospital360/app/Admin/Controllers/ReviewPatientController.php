<?php

namespace App\Admin\Controllers;

use App\Models\Review;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class ReviewPatientController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Review';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Review());
        $grid->disableCreation();
        $grid->actions(function ($actions)
        {

            $actions->disableDelete();
        });
        $grid->tools(function ($tools)
        {
            $tools->batch(function ($batch)
            {
                $batch->disableDelete();
            });
        });
        $grid->column('id', __('Id'));
        // $grid->column('patient_id', __('Patient id'));
        // $grid->column('doctor_id', __('Doctor id'));
        $grid->column('book_id', __('Book id'));
        $grid->column('rating', __('Rating'));
        $grid->column('review_que', __('Review que'));
        $grid->column('doctor_f_name', __('Doctor'));
        $grid->column('patient_f_name', __('Patient'));
        // $grid->column('review_ans', __('Review ans'));
        // $grid->column('created_by', __('Created by'));
        // $grid->column('updated_by', __('Updated by'));
        // $grid->column('created_at', __('Created at'));
        // $grid->column('updated_at', __('Updated at'));
        // $grid->column('deleted_at', __('Deleted at'));
        // $grid->column('admin_status', __('Admin status'));
        $grid->column('admin_status')->display(function ($title, $column) {
    
            
            If ($this->admin_status==1) {
                return "Approved";
            }else{
                return "Hold";
            }
            
            // Otherwise it is displayed as editable
            return $column->editable();
        });
        // $grid->column('review_star', __('Review star'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Review::findOrFail($id));

        $show->field('id', __('Id'));
        // $show->field('patient_id', __('Patient id'));
        // $show->field('doctor_id', __('Doctor id'));
        $show->field('book_id', __('Book id'));
        $show->field('rating', __('Rating'));
        $show->field('review_que', __('Review que'));
        // $show->field('review_ans', __('Review ans'));
        // $show->field('created_by', __('Created by'));
        // $show->field('updated_by', __('Updated by'));
        // $show->field('created_at', __('Created at'));
        // $show->field('updated_at', __('Updated at'));
        // $show->field('deleted_at', __('Deleted at'));
        // $show->field('admin_status', __('Admin status'));
        // $show->field('review_star', __('Review star'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Review());
        $form->select('admin_status', 'Review Status')
        ->options([1 => 'Approve', 0 => 'Hold']);
        $form->text('review_que', __('Review que'));
        $form->footer(function ($footer)
        {
            $footer->disableViewCheck();
            $footer->disableEditingCheck();
            $footer->disableCreatingCheck();
        });
        $form->tools(function (Form\Tools $tools)
        {
            $tools->disableDelete();
        });
        return $form;
    }
}
