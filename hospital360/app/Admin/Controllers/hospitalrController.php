<?php

namespace App\Admin\Controllers;

use App\Models\Hospital_register;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class hospitalrController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Hospital_register';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Hospital_register());

        $grid->column('id', __('Id'));
        $grid->column('hospital_name', __('Hospital name'));
        $grid->column('email', __('Email'));
        $grid->column('country', __('Country'));
        $grid->column('state', __('State'));
        $grid->column('city', __('City'));
        $grid->column('role_id', __('Role id'));
        $grid->column('address', __('Address'));
        $grid->column('contact_person_name', __('Contact person name'));
        $grid->column('contact_details', __('Contact details'));
        $grid->column('date_time', __('Date time'));
        $grid->column('logo', __('Logo'));
        $grid->column('status', __('Status'));
        $grid->column('services', __('Services'));
        $grid->column('specialities', __('Specialities'));
        $grid->column('created_by', __('Created by'));
        $grid->column('updated_by', __('Updated by'));
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));
        $grid->column('deleted_at', __('Deleted at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Hospital_register::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('hospital_name', __('Hospital name'));
        $show->field('email', __('Email'));
        $show->field('country', __('Country'));
        $show->field('state', __('State'));
        $show->field('city', __('City'));
        $show->field('role_id', __('Role id'));
        $show->field('address', __('Address'));
        $show->field('contact_person_name', __('Contact person name'));
        $show->field('contact_details', __('Contact details'));
        $show->field('date_time', __('Date time'));
        $show->field('logo', __('Logo'));
        $show->field('status', __('Status'));
        $show->field('services', __('Services'));
        $show->field('specialities', __('Specialities'));
        $show->field('created_by', __('Created by'));
        $show->field('updated_by', __('Updated by'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));
        $show->field('deleted_at', __('Deleted at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Hospital_register());

        $form->text('hospital_name', __('Hospital name'));
        $form->email('email', __('Email'));
        $form->number('country', __('Country'));
        $form->number('state', __('State'));
        $form->number('city', __('City'));
        $form->number('role_id', __('Role id'));
        $form->text('address', __('Address'));
        $form->text('contact_person_name', __('Contact person name'));
        $form->text('contact_details', __('Contact details'));
        $form->date('date_time', __('Date time'))->default(date('Y-m-d'));
        $form->text('logo', __('Logo'));
        $form->number('status', __('Status'))->default(1);
        $form->number('services', __('Services'));
        $form->number('specialities', __('Specialities'));
        $form->number('created_by', __('Created by'));
        $form->number('updated_by', __('Updated by'));

        return $form;
    }
}
