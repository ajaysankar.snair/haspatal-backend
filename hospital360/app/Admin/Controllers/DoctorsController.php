<?php

namespace App\Admin\Controllers;

use App\Models\Doctors;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use DB;
class DoctorsController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Doctors';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Doctors());
        $grid->filter(function($filter){

            // Remove the default id filter
            $filter->disableIdFilter();
        
            // Add a column filter
            $filter->like('d_uni_id', 'business_registerID');
          
        
        });
        $grid->disableCreation();
        $grid->actions(function ($actions)
        {

            $actions->disableDelete();
        });
        $grid->tools(function ($tools)
        {
            $tools->batch(function ($batch)
            {
                $batch->disableDelete();
            });
        });
        $grid->column('id', __('Id'));
        $grid->column('d_uni_id', __('D uni id'));
        // $grid->column('plan_id', __('Plan id'));
        $grid->column('first_name', __('First name'));
        $grid->column('last_name', __('Last name'));
        $grid->column('email', __('Email'));
        $grid->column('promo_status')->display(function ($title, $column) {
    
            
            If ($this->promo_status == 0) {
                return "Hold";
            }else{
                return "Approved";
            }
            
            // Otherwise it is displayed as editable
            return $column->editable();
        });
        // $grid->column('bio', __('Bio'));
        // $grid->column('pincode', __('Pincode'));
        // $grid->column('state', __('State'));
        // $grid->column('district', __('District'));
        // $grid->column('city', __('City'));
        // $grid->column('address', __('Address'));
        // $grid->column('main_mobile', __('Main mobile'));
        // $grid->column('second_mobile', __('Second mobile'));
        // $grid->column('clinic_team_leader', __('Clinic team leader'));
        // $grid->column('clinic_admin_mobile_no', __('Clinic admin mobile no'));
        // $grid->column('clinic_email', __('Clinic email'));
        // $grid->column('experience', __('Experience'));
        // $grid->column('language', __('Language'));
        // $grid->column('speciality', __('Speciality'));
        // $grid->column('price', __('Price'));
        // $grid->column('licence_type', __('Licence type'));
        // $grid->column('licence_no', __('Licence no'));
        // $grid->column('valide_upto', __('Valide upto'));
        // $grid->column('issued_by', __('Issued by'));
        // $grid->column('licence_copy', __('Licence copy'));
        // $grid->column('profile_pic', __('Profile pic'));
        // $grid->column('clinic_logo', __('Clinic logo'));
        // $grid->column('bank_name', __('Bank name'));
        // $grid->column('account_name', __('Account name'));
        // $grid->column('ifsc_code', __('Ifsc code'));
        // $grid->column('account_no', __('Account no'));
        // $grid->column('cheque_img', __('Cheque img'));
        // $grid->column('watsapp_number', __('Watsapp number'));
        // $grid->column('heighest_qualification', __('Heighest qualification'));
        // $grid->column('youtube_link', __('Youtube link'));
        // $grid->column('teligram_number', __('Teligram number'));
        // $grid->column('status', __('Status'));
        // $grid->column('step', __('Step'));
        // $grid->column('gst_number', __('Gst number'));
        // $grid->column('hospital_id', __('Hospital id'));
        // $grid->column('created_by', __('Created by'));
        // $grid->column('updated_by', __('Updated by'));
        // $grid->column('created_at', __('Created at'));
        // $grid->column('updated_at', __('Updated at'));
        // $grid->column('year_of_graduation', __('Year of graduation'));
        // $grid->column('clinic_wallpapper', __('Clinic wallpapper'));
        // $grid->column('clinic_name', __('Clinic name'));
        // $grid->column('clinic_buisness_name', __('Clinic buisness name'));
        // $grid->column('pan_number', __('Pan number'));
        // $grid->column('co_ordinator', __('Co ordinator'));
        // $grid->column('designation', __('Designation'));
        // $grid->column('in_clinic_fee', __('In clinic fee'));
        // $grid->column('video_fee', __('Video fee'));
        // $grid->column('second_opinion_fee', __('Second opinion fee'));
        // $grid->column('primarymobile', __('Primarymobile'));
        // $grid->column('alternatemobile', __('Alternatemobile'));
        // $grid->column('appointment_type', __('Appointment type'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Doctors::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('d_uni_id', __('D uni id'));
        // $show->field('plan_id', __('Plan id'));
        $show->field('first_name', __('First name'));
        $show->field('last_name', __('Last name'));
        $show->field('email', __('Email'));
        // $show->field('bio', __('Bio'));
        // $show->field('pincode', __('Pincode'));
        // $show->field('state', __('State'));
        // $show->field('district', __('District'));
        // $show->field('city', __('City'));
        // $show->field('address', __('Address'));
        // $show->field('main_mobile', __('Main mobile'));
        // $show->field('second_mobile', __('Second mobile'));
        // $show->field('clinic_team_leader', __('Clinic team leader'));
        // $show->field('clinic_admin_mobile_no', __('Clinic admin mobile no'));
        // $show->field('clinic_email', __('Clinic email'));
        // $show->field('experience', __('Experience'));
        // $show->field('language', __('Language'));
        // $show->field('speciality', __('Speciality'));
        // $show->field('price', __('Price'));
        // $show->field('licence_type', __('Licence type'));
        // $show->field('licence_no', __('Licence no'));
        // $show->field('valide_upto', __('Valide upto'));
        // $show->field('issued_by', __('Issued by'));
        // $show->field('licence_copy', __('Licence copy'));
        // $show->field('profile_pic', __('Profile pic'));
        // $show->field('clinic_logo', __('Clinic logo'));
        // $show->field('bank_name', __('Bank name'));
        // $show->field('account_name', __('Account name'));
        // $show->field('ifsc_code', __('Ifsc code'));
        // $show->field('account_no', __('Account no'));
        // $show->field('cheque_img', __('Cheque img'));
        // $show->field('watsapp_number', __('Watsapp number'));
        // $show->field('heighest_qualification', __('Heighest qualification'));
        // $show->field('youtube_link', __('Youtube link'));
        // $show->field('teligram_number', __('Teligram number'));
        // $show->field('status', __('Status'));
        // $show->field('step', __('Step'));
        // $show->field('gst_number', __('Gst number'));
        // $show->field('hospital_id', __('Hospital id'));
        // $show->field('created_by', __('Created by'));
        // $show->field('updated_by', __('Updated by'));
        // $show->field('created_at', __('Created at'));
        // $show->field('updated_at', __('Updated at'));
        // $show->field('year_of_graduation', __('Year of graduation'));
        // $show->field('clinic_wallpapper', __('Clinic wallpapper'));
        // $show->field('clinic_name', __('Clinic name'));
        // $show->field('clinic_buisness_name', __('Clinic buisness name'));
        // $show->field('pan_number', __('Pan number'));
        // $show->field('co_ordinator', __('Co ordinator'));
        // $show->field('designation', __('Designation'));
        // $show->field('in_clinic_fee', __('In clinic fee'));
        // $show->field('video_fee', __('Video fee'));
        // $show->field('second_opinion_fee', __('Second opinion fee'));
        // $show->field('primarymobile', __('Primarymobile'));
        // $show->field('alternatemobile', __('Alternatemobile'));
        // $show->field('appointment_type', __('Appointment type'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Doctors());
        $form->select('promo_status', 'Promo tatus')
        ->options([1 => 'Approve', 0 => 'Hold']);
        // $form->text('d_uni_id', __('D uni id'));
        // // $form->number('plan_id', __('Plan id'));
        // $form->text('first_name', __('First name'));
        // $form->text('last_name', __('Last name'));
        // $form->email('email', __('Email'));
        // $form->text('bio', __('Bio'));
        // $form->text('pincode', __('Pincode'));
        // $form->text('state', __('State'));
        // $form->number('district', __('District'));
        // $form->text('city', __('City'));
        // $form->text('address', __('Address'));
        // $form->text('main_mobile', __('Main mobile'));
        // $form->text('second_mobile', __('Second mobile'));
        // $form->text('clinic_team_leader', __('Clinic team leader'));
        // $form->text('clinic_admin_mobile_no', __('Clinic admin mobile no'));
        // $form->text('clinic_email', __('Clinic email'));
        // $form->text('experience', __('Experience'));
        // $form->text('language', __('Language'));
        // $form->text('speciality', __('Speciality'));
        // $form->text('price', __('Price'));
        // $form->text('licence_type', __('Licence type'));
        // $form->text('licence_no', __('Licence no'));
        // $form->text('valide_upto', __('Valide upto'));
        // $form->text('issued_by', __('Issued by'));
        // $form->text('licence_copy', __('Licence copy'));
        // $form->text('profile_pic', __('Profile pic'));
        // $form->text('clinic_logo', __('Clinic logo'));
        // $form->text('bank_name', __('Bank name'));
        // $form->text('account_name', __('Account name'));
        // $form->text('ifsc_code', __('Ifsc code'));
        // $form->text('account_no', __('Account no'));
        // $form->text('cheque_img', __('Cheque img'));
        // $form->text('watsapp_number', __('Watsapp number'));
        // $form->text('heighest_qualification', __('Heighest qualification'));
        // $form->text('youtube_link', __('Youtube link'));
        // $form->text('teligram_number', __('Teligram number'));
        // $form->number('status', __('Status'))->default(1);
        // $form->switch('step', __('Step'));
        // $form->text('gst_number', __('Gst number'));
        // $form->number('hospital_id', __('Hospital id'));
        // $form->text('created_by', __('Created by'));
        // $form->text('updated_by', __('Updated by'));
        // $form->text('year_of_graduation', __('Year of graduation'));
        // $form->text('clinic_wallpapper', __('Clinic wallpapper'));
        // $form->text('clinic_name', __('Clinic name'));
        // $form->text('clinic_buisness_name', __('Clinic buisness name'));
        // $form->text('pan_number', __('Pan number'));
        // $form->number('co_ordinator', __('Co ordinator'));
        // $form->text('designation', __('Designation'));
        // $form->number('in_clinic_fee', __('In clinic fee'));
        // $form->number('video_fee', __('Video fee'));
        // $form->number('second_opinion_fee', __('Second opinion fee'));
        // $form->text('primarymobile', __('Primarymobile'));
        // $form->text('alternatemobile', __('Alternatemobile'));
        // $form->text('appointment_type', __('Appointment type'));
        $form->footer(function ($footer)
        {
            $footer->disableViewCheck();
            $footer->disableEditingCheck();
            $footer->disableCreatingCheck();
        });
        $form->tools(function (Form\Tools $tools)
        {
            $tools->disableDelete();
        });
        $form->saved(function (Form $form) {

            // returns a simple response
            if( $form->model()->promo_status==1){
                $speciality_name = array();
                $language_name = array();
            /*echo "<pre>";
            print_r($doctors->toArray()); exit;*/
            $d = explode(',',$form->model()->speciality);
            $d1 = explode(',',$form->model()->language);
            // $subject= $form->model()->first_name" - Doctor Registration Approved ";
            $subject =
            $form->model()->first_name.
             "- Doctor Registration Approved";
    // return response(  $subject);
            $speciality_name = DB::table('specilities')->whereIn('id',$d)->get();
            $speciltynmae="";
            $laugugaename="";
            foreach ($speciality_name as $value) {
            $speciltynmae  .='<p style="margin-bottom: 0;">'.$value->specility.'</p>';
            }
            //   return response(  $speciality_name);
            $language_name = DB::table('language')->whereIn('id',$d1)->get();
            foreach ($language_name as $values) {
                $laugugaename  .='<p style="margin-bottom: 0;">'.$values->language.'</p>';
                }

                // $user = User::findOrFail($id);
                $comtents='<!DOCTYPE html>
                <html lang="en">
                    <head>
                        <title>Dr email template</title>
                        <meta charset="utf-8">
                        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
                        <!-- CSS only -->
                        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
                        <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900,900i&display=swap" rel="stylesheet">
                        <script src="https://kit.fontawesome.com/fa5e54101c.js" crossorigin="anonymous"></script>
                        <!-- JavaScript Bundle with Popper -->
                        <style>
                            #page-wrap{
                                max-width: 800px;
                                margin: 0 auto;
                                font-family: "Roboto";
                               
                            }
                            p,h5,h6{
                                color: #000;
                            }
                        </style>
                        <!-- Theme Style -->
                        <link rel="stylesheet" href="css/print.css">
                        <!-- <link rel="stylesheet" href="/assets/css/bootstrap.min.css"> -->
                    </head>
                    <body>
                        <div id="page-wrap">
                            <div class="top">
                            
                                <h6 style="margin-bottom: 25px; font-weight:400;  font-size: 16px;">Hi '.$form->model()->first_name.' </h6>
                                <p>We find pleasure to inform you that your registration has been approved. 
                                    Now you are entitled to use Haspatal Program and Haspatal MD Application to provide your 
                                    services for patients across India.
                                </p>
                                <p style="margin-bottom: 25px;">
                                    <b>Your quick access code is '.$form->model()->d_uni_id.'.</b><br>
                                    Kindly display this quick access code at your Clinic for the convenience of your patients.
                                </p>
                                <p style="margin-bottom: 25px;">As the next step, kindly visit Doctor App Info Playlist at Haspatal Youtube Channel to know
                                     how features of Haspatal MD are helping doctors to grow their business smoothly.
                                </p>
                                <p style="margin-bottom: 0;">Your registered details are as follows:</p>
                                <p style="margin-bottom: 0;">'.$form->model()->first_name.'</p>
                                <p style="margin-bottom: 0;">'.$form->model()->address.'</p>
                                <p style="margin-bottom: 0;">'.$form->model()->main_mobile.'</p>
                                <p style="margin-bottom: 25px;">Speciality : -</p>
                                '.$speciltynmae.' 
                                <p style="margin-bottom: 25px;">Language : -</p>
                                '.$laugugaename .'

                                <p style="margin-bottom: 35px;">Price  :'.$form->model()->price.'</p>
                                <p style="margin-bottom: 25px;">With Regards,</p>
                    
                                <h5 style="font-size: 16px; margin-bottom: 0;">TEAM HASPATAL MD</h5>
                                <p style="margin-bottom: 40px;"><b>PHONE:</b>+91-124-405 56 56</p>
                    
                                <h5 style="font-size: 16px;">IMPORTANT:-</h5>
                                <p><b>We have uploaded support videos for our Doctors at following 
                                <a href="https://www.youtube.com/playlist?list=PLb9kI_cIEaQMq43XlL7Gd0ch2Ve_KQfyJ">link</a>
                                
                                    You may visit here 24*7 and find useful information</b>
                                </p>
                            </div>
                        </div>
                    </body>
                </html>
                
'                 ;
    // return response(   $form->model()->promo_status);
        
                $data['personalizations'] = array("0" => array('to' => array('0' => array('email' => $form->model()->email))));
        
                $data['from'] = array('email' => 'system@haspatal.com');
        
                $data['subject'] = $subject;
                $data['content'] = array("0" =>array('type' =>' text/html','value' => $comtents ));
        
                //echo json_encode($data);exit();
        
                $curl = curl_init();
        
                curl_setopt_array($curl, array(
                  CURLOPT_URL => "https://api.sendgrid.com/v3/mail/send",
                  CURLOPT_RETURNTRANSFER => true,
                  CURLOPT_ENCODING => "",
                  CURLOPT_MAXREDIRS => 10,
                  CURLOPT_TIMEOUT => 30,
                  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                  CURLOPT_CUSTOMREQUEST => "POST",
                  CURLOPT_POSTFIELDS => json_encode($data),
                  CURLOPT_HTTPHEADER => array(
                    "authorization: Bearer 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",
                    "cache-control: no-cache",
                    "content-type: application/json",
                    "postman-token: b8c0781f-af48-90cc-1c5b-76f80e0b4be0"
                  ),
                ));
        
                $response = curl_exec($curl);
                $err = curl_error($curl);
        
                curl_close($curl);
        
                if ($err) {
                    echo "cURL Error #:" . $err;
                } else {
                    return "Send OTP in your emailId";
                }
            }
            // return response(   $form->model()->promo_status);
        
        
        });
        return $form;
      
       
    }
}
