<?php

namespace App\Admin\Controllers;

use App\Models\Coupan;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class coupanController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Coupan';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Coupan());

        $grid->column('id', __('Id'));
        // $grid->column('type', __('Type'));
        $grid->column('promo_code', __('Promo code'));
        // $grid->column('used_promo_code_time', __('Used promo code time'));
        // $grid->column('amount_type', __('Amount type'));
        $grid->column('price', __('Price'));
        // $grid->column('sdate', __('Sdate'));
        // $grid->column('edate', __('Edate'));
        // $grid->column('created_at', __('Created at'));
        // $grid->column('updated_at', __('Updated at'));
        // $grid->column('deleted_at', __('Deleted at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Coupan::findOrFail($id));

        $show->field('id', __('Id'));
        // $show->field('type', __('Type'));
        $show->field('promo_code', __('Promo code'));
        // $show->field('used_promo_code_time', __('Used promo code time'));
        // $show->field('amount_type', __('Amount type'));
        $show->field('price', __('Price'));
        // $show->field('sdate', __('Sdate'));
        // $show->field('edate', __('Edate'));
        // $show->field('created_at', __('Created at'));
        // $show->field('updated_at', __('Updated at'));
        // $show->field('deleted_at', __('Deleted at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Coupan());

        // $form->number('type', __('Type'));
        $form->text('promo_code', __('Promo code'));
        // $form->text('used_promo_code_time', __('Used promo code time'));
        // $form->text('amount_type', __('Amount type'));
        $form->number('price', __('Price'));
        // $form->text('sdate', __('Sdate'));
        // $form->text('edate', __('Edate'));

        return $form;
    }
}
