<?php

namespace App;

use Zizaco\Entrust\Traits\EntrustUserTrait;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use Notifiable, EntrustUserTrait, HasApiTokens;
   
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'fb_id','username', 'first_name','last_name','email','password','profile_pic','version','mobile','device','signup_type','mobile_token','status','role_id','user_type','userDetailsId','uid','plan_id','doctor_login_status','patient_login_status','doctor_login_status_mobile','doctor_plan_status','patient_login_status_contactinfo','patient_login_status_addressinfo',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function roleUser()
    {
        return $this->hasOne('App\RoleUser');
    }
}
