<?php

namespace App\Repositories;

use App\Models\Plan_master;
use App\Repositories\BaseRepository;

/**
 * Class Plan_masterRepository
 * @package App\Repositories
 * @version April 28, 2020, 7:17 am UTC
*/

class Plan_masterRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'pl_name',
        'pl_price',
        'pl_desc',
        'status',
        'created_by',
        'updated_by'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Plan_master::class;
    }
}
