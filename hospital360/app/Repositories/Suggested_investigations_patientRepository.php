<?php

namespace App\Repositories;

use App\Models\Suggested_investigations_patient;
use App\Repositories\BaseRepository;

/**
 * Class Suggested_investigations_patientRepository
 * @package App\Repositories
 * @version April 18, 2020, 8:22 am UTC
*/

class Suggested_investigations_patientRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        's_patient_id',
        's_doctor_id',
        'suggested_investigations',
        'created_by',
        'updated_by'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Suggested_investigations_patient::class;
    }
}
