<?php

namespace App\Repositories;

use App\Models\Counselings_list;
use App\Repositories\BaseRepository;

/**
 * Class Counselings_listRepository
 * @package App\Repositories
 * @version October 3, 2020, 10:25 am UTC
*/

class Counselings_listRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'counseling_name',
        'created_by',
        'updated_by'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Counselings_list::class;
    }
}
