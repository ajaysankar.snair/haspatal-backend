<?php

namespace App\Repositories;

use App\Models\Your_coverage_360;
use App\Repositories\BaseRepository;

/**
 * Class Your_coverage_360Repository
 * @package App\Repositories
 * @version June 4, 2020, 5:48 am UTC
*/

class Your_coverage_360Repository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'c_state_id',
        'c_city_id',
        'c_pincode',
        'created_by',
        'updated_by',
        'status'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Your_coverage_360::class;
    }
}
