<?php

namespace App\Repositories;

use App\Models\My_favourite;
use App\Repositories\BaseRepository;

/**
 * Class My_favouriteRepository
 * @package App\Repositories
 * @version April 9, 2020, 9:54 am UTC
*/

class My_favouriteRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'patient_id',
        'doctor_id',
        'status',
        'created_by',
        'updated_by'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return My_favourite::class;
    }
}
