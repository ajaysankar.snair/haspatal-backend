<?php

namespace App\Repositories;

use App\Models\Specialities;
use App\Repositories\BaseRepository;

/**
 * Class SpecialitiesRepository
 * @package App\Repositories
 * @version April 7, 2020, 5:46 am UTC
*/

class SpecialitiesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'specility',
        'description',
        'status',
        'created_by',
        'updated_by'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Specialities::class;
    }
}
