<?php

namespace App\Repositories;

use App\Models\ReferralSpecialities;
use App\Repositories\BaseRepository;

/**
 * Class ReferralSpecialitiesRepository
 * @package App\Repositories
 * @version October 5, 2020, 10:55 am UTC
*/

class ReferralSpecialitiesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'dr_id',
        'specialities_name',
        'created_by',
        'updated_by'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ReferralSpecialities::class;
    }
}
