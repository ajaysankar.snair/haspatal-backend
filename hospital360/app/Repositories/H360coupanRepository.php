<?php

namespace App\Repositories;

use App\Models\H360coupan;
use App\Repositories\BaseRepository;

/**
 * Class H360coupanRepository
 * @package App\Repositories
 * @version November 13, 2020, 11:46 am IST
*/

class H360coupanRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'b_id',
        'coupan_code',
        'price',
        'status'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return H360coupan::class;
    }
}
