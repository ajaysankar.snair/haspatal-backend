<?php

namespace App\Repositories;

use App\Models\Support;
use App\Repositories\BaseRepository;

/**
 * Class SupportRepository
 * @package App\Repositories
 * @version May 7, 2020, 5:18 am UTC
*/

class SupportRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'description',
        'created_by',
        'updated_by'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Support::class;
    }
}
