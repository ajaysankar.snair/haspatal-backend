<?php

namespace App\Repositories;

use App\Models\Doctor_withdraw;
use App\Repositories\BaseRepository;

/**
 * Class Doctor_withdrawRepository
 * @package App\Repositories
 * @version May 6, 2020, 4:40 am UTC
*/

class Doctor_withdrawRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'doctor_id',
        'current_balance',
        'requested_amount',
        'available_balance',
        'created_by',
        'updated_by',
        'status'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Doctor_withdraw::class;
    }
}
