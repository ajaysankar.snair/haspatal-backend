<?php

namespace App\Repositories;

use App\Models\TimeSlot;
use App\Repositories\BaseRepository;

/**
 * Class TimeSlotRepository
 * @package App\Repositories
 * @version April 13, 2020, 8:28 am UTC
*/

class TimeSlotRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'doctor_id',
        'booking_time_slot',
        'mrg_from_time',
        'mrg_to_time',
        'aft_from_time',
        'aft_to_time',
        'eve_from_time',
        'eve_to_time',
        'status',
        'created_by',
        'updated_by'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return TimeSlot::class;
    }
}
