<?php

namespace App\Repositories;

use App\Models\HaspatalRegsiter;
use App\Repositories\BaseRepository;

/**
 * Class HaspatalRegsiterRepository
 * @package App\Repositories
 * @version July 13, 2020, 10:22 am UTC
*/

class HaspatalRegsiterRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'country_id',
        'state_id',
        'city_id',
        'pincode_id',
        'hospital_name',
        'address',
        'year_of_establishment',
        'gst_no',
        'license_copy',
        'website',
        'contact_person',
        'contact_person_email',
        'contact_person_phonoe_no',
        'other_phone_no',
        'support_watsapp_no'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return HaspatalRegsiter::class;
    }
}
