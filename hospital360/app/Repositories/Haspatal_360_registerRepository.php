<?php

namespace App\Repositories;

use App\Models\Haspatal_360_register;
use App\Repositories\BaseRepository;

/**
 * Class Haspatal_360_registerRepository
 * @package App\Repositories
 * @version June 1, 2020, 6:25 am UTC
*/

class Haspatal_360_registerRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name_prefix',
        'full_name',
        'email',
        'mo_prefix',
        'mobile_no',
        'created_by',
        'updated_by',
        'status'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Haspatal_360_register::class;
    }
}
