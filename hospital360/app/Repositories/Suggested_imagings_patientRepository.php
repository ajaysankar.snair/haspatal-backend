<?php

namespace App\Repositories;

use App\Models\Suggested_imagings_patient;
use App\Repositories\BaseRepository;

/**
 * Class Suggested_imagings_patientRepository
 * @package App\Repositories
 * @version April 18, 2020, 8:24 am UTC
*/

class Suggested_imagings_patientRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'si_patient_id',
        'si_doctor_id',
        'suggested_imagings',
        'created_by',
        'updated_by'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Suggested_imagings_patient::class;
    }
}
