<?php

namespace App\Repositories;

use App\Models\Suggested_specialists_patient;
use App\Repositories\BaseRepository;

/**
 * Class Suggested_specialists_patientRepository
 * @package App\Repositories
 * @version April 18, 2020, 8:26 am UTC
*/

class Suggested_specialists_patientRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'ss_patient_id',
        'ss_doctor_id',
        'suggested_specialists',
        'created_by',
        'updated_by'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Suggested_specialists_patient::class;
    }
}
