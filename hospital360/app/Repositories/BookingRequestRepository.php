<?php

namespace App\Repositories;

use App\Models\BookingRequest;
use App\Repositories\BaseRepository;

/**
 * Class BookingRequestRepository
 * @package App\Repositories
 * @version April 9, 2020, 6:34 am UTC
*/

class BookingRequestRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'patient_name',
        'doctor_name',
        'date_time',
        'payment_status',
        'status',
        'created_by',
        'updated_by'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return BookingRequest::class;
    }
}
