<?php

namespace App\Repositories;

use App\Models\Present_medications_patient;
use App\Repositories\BaseRepository;

/**
 * Class Present_medications_patientRepository
 * @package App\Repositories
 * @version April 18, 2020, 8:07 am UTC
*/

class Present_medications_patientRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'patient_id',
        'doctor_id',
        'created_by',
        'updated_by'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Present_medications_patient::class;
    }
}
