<?php

namespace App\Repositories;

use App\Models\review_360;
use App\Repositories\BaseRepository;

/**
 * Class review_360Repository
 * @package App\Repositories
 * @version August 4, 2020, 11:22 am UTC
*/

class review_360Repository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'patient_id',
        'f_id',
        'rating',
        'review_que',
        'review_ans',
        'created_by',
        'updated_by'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return review_360::class;
    }
}
