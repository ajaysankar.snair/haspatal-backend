<?php

namespace App\Repositories;

use App\Models\Examination_notes_patient;
use App\Repositories\BaseRepository;

/**
 * Class Examination_notes_patientRepository
 * @package App\Repositories
 * @version April 18, 2020, 8:15 am UTC
*/

class Examination_notes_patientRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'e_patient_id',
        'e_doctor_id',
        'examination_notes',
        'created_by',
        'updated_by'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Examination_notes_patient::class;
    }
}
