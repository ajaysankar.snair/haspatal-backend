<?php

namespace App\Repositories;

use App\Models\Coupan;
use App\Repositories\BaseRepository;

/**
 * Class CoupanRepository
 * @package App\Repositories
 * @version April 17, 2020, 11:11 am UTC
*/

class CoupanRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'promo_code',
        'used_promo_code_time',
        'amount_type',
        'price',
        'sdate',
        'edate'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Coupan::class;
    }
}
