<?php

namespace App\Repositories;

use App\Models\Hospital_register;
use App\Repositories\BaseRepository;

/**
 * Class Hospital_registerRepository
 * @package App\Repositories
 * @version May 8, 2020, 10:32 am UTC
*/

class Hospital_registerRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'hospital_name',
        'email',
        'address',
        'contact_person_name',
        'contact_details',
        'date_time',
        'logo',
        'created_by',
        'updated_by'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Hospital_register::class;
    }
}
