<?php

namespace App\Repositories;

use App\Models\Patients;
use App\Repositories\BaseRepository;

/**
 * Class PatientsRepository
 * @package App\Repositories
 * @version April 7, 2020, 10:18 am UTC
*/

class PatientsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'first_name',
        'last_name',
        'email',
        'mobile',
        'country',
        'state',
        'city',
        'zipcode',
        'address',
        'personal_profile',
        'family_profile',
        'social_profile',
        'work_profile',
        'contact_phone1',
        'contact_phone2',
        'contact_phone3',
        'email1',
        'email2',
        'language',
        'profile_pic',
        'status',
        'created_by',
        'updated_by'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Patients::class;
    }
}
