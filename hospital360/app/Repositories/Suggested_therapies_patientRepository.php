<?php

namespace App\Repositories;

use App\Models\Suggested_therapies_patient;
use App\Repositories\BaseRepository;

/**
 * Class Suggested_therapies_patientRepository
 * @package App\Repositories
 * @version April 18, 2020, 8:33 am UTC
*/

class Suggested_therapies_patientRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'st_patient_id',
        'st_doctor_id',
        'suggested_therapies',
        'created_by',
        'updated_by'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Suggested_therapies_patient::class;
    }
}
