<?php

namespace App\Repositories;

use App\Models\Doctors;
use App\Repositories\BaseRepository;

/**
 * Class DoctorsRepository
 * @package App\Repositories
 * @version April 7, 2020, 10:07 am UTC
*/

class DoctorsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'first_name',
        'last_name',
        'email',
        'bio',
        'pincode',
        'state',
        'city',
        'address',
        'main_mobile',
        'second_mobile',
        'clinic_team_leader',
        'experience',
        'language',
        'speciality',
        'licence_type',
        'licence_no',
        'valide_upto',
        'issued_by',
        'licence_copy',
        'profile_pic',
        'status',
        'created_by',
        'updated_by'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Doctors::class;
    }
}
