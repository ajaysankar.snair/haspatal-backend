<?php

namespace App\Repositories;

use App\Models\Therapies_list;
use App\Repositories\BaseRepository;

/**
 * Class Therapies_listRepository
 * @package App\Repositories
 * @version October 3, 2020, 10:24 am UTC
*/

class Therapies_listRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'therapy_name',
        'created_by',
        'updated_by'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Therapies_list::class;
    }
}
