<?php

namespace App\Repositories;

use App\Models\Set_service_price;
use App\Repositories\BaseRepository;

/**
 * Class Set_service_priceRepository
 * @package App\Repositories
 * @version April 8, 2020, 9:41 am UTC
*/

class Set_service_priceRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'service_name',
        'price',
        'status',
        'created_by',
        'updated_by'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Set_service_price::class;
    }
}
