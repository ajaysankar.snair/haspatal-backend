<?php

namespace App\Repositories;

use App\Models\Key_points_patient;
use App\Repositories\BaseRepository;

/**
 * Class Key_points_patientRepository
 * @package App\Repositories
 * @version April 18, 2020, 7:56 am UTC
*/

class Key_points_patientRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'patient_id',
        'doctor_id',
        'created_by',
        'updated_by'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Key_points_patient::class;
    }
}
