<?php

namespace App\Repositories;

use App\Models\Buy_plan_dr;
use App\Repositories\BaseRepository;

/**
 * Class Buy_plan_drRepository
 * @package App\Repositories
 * @version September 28, 2020, 10:51 am UTC
*/

class Buy_plan_drRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'dr_id',
        'plan_id',
        'payable_amount',
        'promo_code',
        'discount',
        'gst_amount',
        'final_amount',
        'gst_number',
        'payment_id',
        'payment_status'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Buy_plan_dr::class;
    }
}
