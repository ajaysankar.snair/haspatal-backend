<?php

namespace App\Repositories;

use App\Models\Imaging_list;
use App\Repositories\BaseRepository;

/**
 * Class Imaging_listRepository
 * @package App\Repositories
 * @version October 3, 2020, 10:23 am UTC
*/

class Imaging_listRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'imaging_name',
        'created_by',
        'updated_by'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Imaging_list::class;
    }
}
