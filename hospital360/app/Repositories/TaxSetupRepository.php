<?php

namespace App\Repositories;

use App\Models\TaxSetup;
use App\Repositories\BaseRepository;

/**
 * Class TaxSetupRepository
 * @package App\Repositories
 * @version March 28, 2020, 10:20 am UTC
*/

class TaxSetupRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'tax_name',
        'rate',
        'type',
        'duration',
        'status',
        'created_by',
        'updated_by'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return TaxSetup::class;
    }
}
