<?php

namespace App\Repositories;

use App\Models\Order_360;
use App\Repositories\BaseRepository;

/**
 * Class Order_360Repository
 * @package App\Repositories
 * @version June 12, 2020, 5:15 am UTC
*/

class Order_360Repository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'patient_id',
        'doctor_id',
        'book_id',
        'role_id',
        'created_by',
        'updated_by'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Order_360::class;
    }
}
