<?php

namespace App\Repositories;

use App\Models\Home_care_list;
use App\Repositories\BaseRepository;

/**
 * Class Home_care_listRepository
 * @package App\Repositories
 * @version October 3, 2020, 10:26 am UTC
*/

class Home_care_listRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'home_care_service_name',
        'created_by',
        'updated_by'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Home_care_list::class;
    }
}
