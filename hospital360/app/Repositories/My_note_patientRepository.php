<?php

namespace App\Repositories;

use App\Models\My_note_patient;
use App\Repositories\BaseRepository;

/**
 * Class My_note_patientRepository
 * @package App\Repositories
 * @version April 18, 2020, 8:49 am UTC
*/

class My_note_patientRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'my_patient_id',
        'my_doctor_id',
        'my_note',
        'created_by',
        'updated_by'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return My_note_patient::class;
    }
}
