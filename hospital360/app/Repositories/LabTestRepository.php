<?php

namespace App\Repositories;

use App\Models\LabTest;
use App\Repositories\BaseRepository;

/**
 * Class LabTestRepository
 * @package App\Repositories
 * @version October 3, 2020, 10:21 am UTC
*/

class LabTestRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'lab_test_name',
        'created_by',
        'updated_by'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return LabTest::class;
    }
}
