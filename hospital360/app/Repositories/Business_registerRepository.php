<?php

namespace App\Repositories;

use App\Models\Business_register;
use App\Repositories\BaseRepository;

/**
 * Class Business_registerRepository
 * @package App\Repositories
 * @version May 28, 2020, 9:55 am UTC
*/

class Business_registerRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'b_id',
        'b_name',
        'gst_no',
        'working_hr',
        'address',
        'country_id',
        'state_id',
        'city_id',
        'pincode',
        'b_lic',
        'created_by',
        'updated_by',
        'status'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Business_register::class;
    }
}
