<?php

namespace App\Repositories;

use App\Models\Business_type;
use App\Repositories\BaseRepository;

/**
 * Class Business_typeRepository
 * @package App\Repositories
 * @version May 28, 2020, 6:09 am UTC
*/

class Business_typeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'b_name',
        'b_desc',
        'created_by',
        'updated_by'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Business_type::class;
    }
}
