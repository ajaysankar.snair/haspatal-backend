<?php

namespace App\Repositories;

use App\Models\Present_complaint_patient;
use App\Repositories\BaseRepository;

/**
 * Class Present_complaint_patientRepository
 * @package App\Repositories
 * @version April 18, 2020, 8:40 am UTC
*/

class Present_complaint_patientRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'pc_patient_id',
        'pc_doctor_id',
        'book_id',
        'present_complaint',
        'complaint_pic',
        'complaint_pdf',
        'created_by',
        'updated_by'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Present_complaint_patient::class;
    }
}
