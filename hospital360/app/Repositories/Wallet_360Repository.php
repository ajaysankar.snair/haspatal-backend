<?php

namespace App\Repositories;

use App\Models\Wallet_360;
use App\Repositories\BaseRepository;

/**
 * Class Wallet_360Repository
 * @package App\Repositories
 * @version June 9, 2020, 10:35 am UTC
*/

class Wallet_360Repository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'payment_id',
        'wa_amount',
        'status',
        'created_by',
        'updated_by'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Wallet_360::class;
    }
}
