<?php

namespace App\Repositories;

use App\Models\Buy_plan;
use App\Repositories\BaseRepository;

/**
 * Class Buy_planRepository
 * @package App\Repositories
 * @version June 11, 2020, 6:24 am UTC
*/

class Buy_planRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'plan_id',
        'price',
        'limit',
        'payment_id',
        'order_id',
        'p_status',
        'created_by',
        'updated_by'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Buy_plan::class;
    }
}
