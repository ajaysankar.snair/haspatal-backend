<?php

namespace App\Repositories;

use App\Models\Medicine;
use App\Repositories\BaseRepository;

/**
 * Class MedicineRepository
 * @package App\Repositories
 * @version October 3, 2020, 5:16 am UTC
*/

class MedicineRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'dr_id',
        'generic_name',
        'medicine_name',
        'drug_from',
        'drug_strength',
        'created_by',
        'updated_by'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Medicine::class;
    }
}
