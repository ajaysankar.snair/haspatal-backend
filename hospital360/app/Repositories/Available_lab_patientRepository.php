<?php

namespace App\Repositories;

use App\Models\Available_lab_patient;
use App\Repositories\BaseRepository;

/**
 * Class Available_lab_patientRepository
 * @package App\Repositories
 * @version April 18, 2020, 8:17 am UTC
*/

class Available_lab_patientRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'a_patient_id',
        'a_doctor_id',
        'available_lab',
        'created_by',
        'updated_by'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Available_lab_patient::class;
    }
}
