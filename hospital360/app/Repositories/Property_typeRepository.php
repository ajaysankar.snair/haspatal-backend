<?php

namespace App\Repositories;

use App\Models\Property_type;
use App\Repositories\BaseRepository;

/**
 * Class Property_typeRepository
 * @package App\Repositories
 * @version March 27, 2020, 8:30 am UTC
*/

class Property_typeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'type',
        'status',
        'created_by',
        'updated_by'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Property_type::class;
    }
}
