<?php

namespace App\Repositories;

use App\Models\Prescription_details;
use App\Repositories\BaseRepository;

/**
 * Class Prescription_detailsRepository
 * @package App\Repositories
 * @version April 9, 2020, 11:06 am UTC
*/

class Prescription_detailsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'patient_id',
        'doctor_id',
        'booking_id',
        'prescription',
        'prescription_image',
        'status',
        'created_by',
        'updated_by'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Prescription_details::class;
    }
}
