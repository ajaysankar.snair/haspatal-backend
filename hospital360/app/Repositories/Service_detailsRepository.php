<?php

namespace App\Repositories;

use App\Models\Service_details;
use App\Repositories\BaseRepository;

/**
 * Class Service_detailsRepository
 * @package App\Repositories
 * @version April 8, 2020, 9:39 am UTC
*/

class Service_detailsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'service',
        'status',
        'created_by',
        'updated_by'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Service_details::class;
    }
}
