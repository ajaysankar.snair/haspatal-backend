<?php

namespace App\Repositories;

use App\Models\Order_query;
use App\Repositories\BaseRepository;

/**
 * Class Order_queryRepository
 * @package App\Repositories
 * @version June 13, 2020, 5:53 am UTC
*/

class Order_queryRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'book_id',
        'patient_id',
        'doctor_id',
        'query',
        'created_by',
        'updated_by'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Order_query::class;
    }
}
