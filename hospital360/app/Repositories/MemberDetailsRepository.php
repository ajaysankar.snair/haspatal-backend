<?php

namespace App\Repositories;

use App\Models\MemberDetails;
use App\Repositories\BaseRepository;

/**
 * Class MemberDetailsRepository
 * @package App\Repositories
 * @version March 28, 2020, 12:18 pm UTC
*/

class MemberDetailsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'first_name',
        'last_name',
        'email',
        'mobile',
        'floor_no',
        'street_no',
        'house_no',
        'id_proof',
        'image',
        'no_of_family_member',
        'notification_type',
        'member_type',
        'status',
        'ctreate_by',
        'updated_by'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return MemberDetails::class;
    }
}
