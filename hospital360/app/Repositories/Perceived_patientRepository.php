<?php

namespace App\Repositories;

use App\Models\Perceived_patient;
use App\Repositories\BaseRepository;

/**
 * Class Perceived_patientRepository
 * @package App\Repositories
 * @version April 18, 2020, 8:19 am UTC
*/

class Perceived_patientRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'p_patient_id',
        'p_doctor_id',
        'perceived',
        'created_by',
        'updated_by'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Perceived_patient::class;
    }
}
