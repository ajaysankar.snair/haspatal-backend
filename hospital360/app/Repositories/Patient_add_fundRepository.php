<?php

namespace App\Repositories;

use App\Models\Patient_add_fund;
use App\Repositories\BaseRepository;

/**
 * Class Patient_add_fundRepository
 * @package App\Repositories
 * @version April 10, 2020, 10:14 am UTC
*/

class Patient_add_fundRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'patient_id',
        'requested_amount',
        'balance_after_approve',
        'approved_balance',
        'transaction_type',
        'transaction_proof',
        'transaction_date',
        'status',
        'created_by',
        'updated_by'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Patient_add_fund::class;
    }
}
