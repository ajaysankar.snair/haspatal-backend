<?php

namespace App\Repositories;

use App\Models\Set_Consult_Prices;
use App\Repositories\BaseRepository;

/**
 * Class Set_Consult_PricesRepository
 * @package App\Repositories
 * @version July 25, 2020, 7:15 am UTC
*/

class Set_Consult_PricesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'specialities',
        'doctor',
        'consult_price',
        'date',
        'created_by',
        'updated_by'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Set_Consult_Prices::class;
    }
}
