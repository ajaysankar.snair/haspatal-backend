<?php

namespace App\Repositories;

use App\Models\hospitalAddFund;
use App\Repositories\BaseRepository;

/**
 * Class hospitalAddFundRepository
 * @package App\Repositories
 * @version October 19, 2020, 7:22 am UTC
*/

class hospitalAddFundRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'hospital_id',
        'payment_id',
        'h_amount',
        'created_by',
        'updated_by'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return hospitalAddFund::class;
    }
}
