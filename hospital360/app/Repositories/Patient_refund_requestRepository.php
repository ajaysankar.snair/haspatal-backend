<?php

namespace App\Repositories;

use App\Models\Patient_refund_request;
use App\Repositories\BaseRepository;

/**
 * Class Patient_refund_requestRepository
 * @package App\Repositories
 * @version April 10, 2020, 10:19 am UTC
*/

class Patient_refund_requestRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'requested_amount',
        'balance_after_approve',
        'approved_balance',
        'status',
        'created_by',
        'updated_by'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Patient_refund_request::class;
    }
}
