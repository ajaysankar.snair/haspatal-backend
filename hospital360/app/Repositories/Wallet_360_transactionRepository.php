<?php

namespace App\Repositories;

use App\Models\Wallet_360_transaction;
use App\Repositories\BaseRepository;

/**
 * Class Wallet_360_transactionRepository
 * @package App\Repositories
 * @version June 10, 2020, 5:00 am UTC
*/

class Wallet_360_transactionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'wa_amount',
        'cut_amount',
        'status',
        'created_by',
        'updated_by'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Wallet_360_transaction::class;
    }
}
