<?php

namespace App\Repositories;

use App\Models\Create_offer_360;
use App\Repositories\BaseRepository;

/**
 * Class Create_offer_360Repository
 * @package App\Repositories
 * @version June 4, 2020, 6:04 am UTC
*/

class Create_offer_360Repository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'offer_id',
        'created_by',
        'updated_by',
        'status'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Create_offer_360::class;
    }
}
