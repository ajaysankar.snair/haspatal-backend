<?php

namespace App\Repositories;

use App\Models\Consultant;
use App\Repositories\BaseRepository;

/**
 * Class ConsultantRepository
 * @package App\Repositories
 * @version May 16, 2020, 11:48 am UTC
*/

class ConsultantRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'c_patient_id',
        'c_doctor_id',
        'c_book_id',
        'consultant_note',
        'created_by',
        'updated_by'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Consultant::class;
    }
}
