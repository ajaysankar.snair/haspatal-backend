<?php

namespace App\Repositories;

use App\Models\PropertyDetails;
use App\Repositories\BaseRepository;

/**
 * Class PropertyDetailsRepository
 * @package App\Repositories
 * @version March 28, 2020, 9:21 am UTC
*/

class PropertyDetailsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'property_type',
        'propery_name',
        'no_of_floor',
        'no_of_street',
        'no_of_house',
        'serve_no',
        'area',
        'address',
        'country',
        'state',
        'city',
        'status',
        'created_by',
        'updated_by'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PropertyDetails::class;
    }
}
