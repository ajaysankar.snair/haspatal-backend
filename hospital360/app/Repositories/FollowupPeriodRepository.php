<?php

namespace App\Repositories;

use App\Models\FollowupPeriod;
use App\Repositories\BaseRepository;

/**
 * Class FollowupPeriodRepository
 * @package App\Repositories
 * @version October 5, 2020, 11:04 am UTC
*/

class FollowupPeriodRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'dr_id',
        'followup_list',
        'created_by',
        'updated_by'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return FollowupPeriod::class;
    }
}
