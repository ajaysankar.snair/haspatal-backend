<?php

namespace App\Repositories;

use App\Models\Doctor_availability;
use App\Repositories\BaseRepository;

/**
 * Class Doctor_availabilityRepository
 * @package App\Repositories
 * @version April 9, 2020, 10:48 am UTC
*/

class Doctor_availabilityRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'doctor_id',
        'date',
        'time_slot',
        'status',
        'created_by',
        'updated_by'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Doctor_availability::class;
    }
}
