<?php

namespace App\Repositories;

use App\Models\H_plan;
use App\Repositories\BaseRepository;

/**
 * Class H_planRepository
 * @package App\Repositories
 * @version May 28, 2020, 9:42 am UTC
*/

class H_planRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'plan_name',
        'price',
        'limit',
        'description',
        'status',
        'created_by',
        'updated_by'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return H_plan::class;
    }
}
