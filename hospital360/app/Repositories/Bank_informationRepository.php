<?php

namespace App\Repositories;

use App\Models\Bank_information;
use App\Repositories\BaseRepository;

/**
 * Class Bank_informationRepository
 * @package App\Repositories
 * @version April 8, 2020, 9:47 am UTC
*/

class Bank_informationRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'bank_name',
        'account_name',
        'ifsc_code',
        'account_no',
        'cancle_cheque',
        'status',
        'created_by',
        'updated_by'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Bank_information::class;
    }
}
