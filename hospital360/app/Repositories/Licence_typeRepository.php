<?php

namespace App\Repositories;

use App\Models\Licence_type;
use App\Repositories\BaseRepository;

/**
 * Class Licence_typeRepository
 * @package App\Repositories
 * @version April 9, 2020, 4:53 am UTC
*/

class Licence_typeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'licence_name',
        'status',
        'created_by',
        'updated_by'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Licence_type::class;
    }
}
