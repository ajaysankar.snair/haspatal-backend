<?php

namespace App\Repositories;

use App\Models\hospitalWithdraw;
use App\Repositories\BaseRepository;

/**
 * Class hospitalWithdrawRepository
 * @package App\Repositories
 * @version October 19, 2020, 7:54 am UTC
*/

class hospitalWithdrawRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'hospital_id',
        'current_balance',
        'requested_amount',
        'available_balance',
        'created_by',
        'updated_by'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return hospitalWithdraw::class;
    }
}
