<?php

namespace App\Repositories;

use App\Models\Booking_price_admin;
use App\Repositories\BaseRepository;

/**
 * Class Booking_price_adminRepository
 * @package App\Repositories
 * @version June 2, 2020, 11:21 am UTC
*/

class Booking_price_adminRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'book_price',
        'description',
        'created_by',
        'updated_by',
        'status'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Booking_price_admin::class;
    }
}
