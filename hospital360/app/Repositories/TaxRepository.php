<?php

namespace App\Repositories;

use App\Models\Tax;
use App\Repositories\BaseRepository;

/**
 * Class TaxRepository
 * @package App\Repositories
 * @version September 22, 2020, 5:38 am UTC
*/

class TaxRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'tax_name',
        'tax_percentage'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Tax::class;
    }
}
