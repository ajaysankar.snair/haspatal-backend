<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class LabTest
 * @package App\Models
 * @version October 3, 2020, 10:21 am UTC
 *
 * @property string $lab_test_name
 * @property integer $created_by
 * @property integer $updated_by
 */
class LabTest extends Model
{
    use SoftDeletes;

    public $table = 'lab_test';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'lab_test_name',
        'created_by',
        'updated_by'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'lab_test_name' => 'string',
        'created_by' => 'integer',
        'updated_by' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
