<?php



namespace App\Models;



use Eloquent as Model;

use Illuminate\Database\Eloquent\SoftDeletes;


/**

 * Class H360coupan

 * @package App\Models

 * @version November 13, 2020, 11:46 am IST

 *

 * @property integer $b_id
 * @property string $coupan_code
 * @property string $price
 * @property integer $status
 * @property integer $created_by
 * @property integer $updated_by
 */

class H360coupanRedeem extends Model

{

    use SoftDeletes;


    public $table = 'H360_coupan_redeem';

    


    protected $dates = ['deleted_at'];






    public $fillable = [

        'b_id',
        'user_id',
        'coupan_code',
        'price',
        'status',
        'created_by',
        'updated_by'

    ];



    /**

     * The attributes that should be casted to native types.

     *

     * @var array

     */

    protected $casts = [

        'id' => 'integer',
        'b_id' => 'integer',
        'coupan_code' => 'string',
        'price' => 'string',
        'status' => 'integer',
        'created_by' => 'integer',
        'updated_by' => 'integer'

    ];



    /**

     * Validation rules

     *

     * @var array

     */

    public static $rules = [

        

    ];



    

}

