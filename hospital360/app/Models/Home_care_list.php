<?php



namespace App\Models;



use Eloquent as Model;

use Illuminate\Database\Eloquent\SoftDeletes;


/**

 * Class Home_care_list

 * @package App\Models

 * @version October 3, 2020, 10:26 am UTC

 *

 * @property string $home_care_service_name
 * @property integer $created_by
 * @property integer $updated_by
 */

class Home_care_list extends Model

{

    use SoftDeletes;


    public $table = 'home_care_list';

    


    protected $dates = ['deleted_at'];






    public $fillable = [

        'home_care_service_name',
        'dr_id',
        'created_by',
        'updated_by'

    ];



    /**

     * The attributes that should be casted to native types.

     *

     * @var array

     */

    protected $casts = [

        'id' => 'integer',
        'home_care_service_name' => 'string',
        'created_by' => 'integer',
        'updated_by' => 'integer'

    ];



    /**

     * Validation rules

     *

     * @var array

     */

    public static $rules = [

        

    ];



    

}

