<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Bank_information",
 *      required={"bank_name", "account_name", "ifsc_code", "account_no", "cancle_cheque", "status", "created_by", "updated_by"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="bank_name",
 *          description="bank_name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="account_name",
 *          description="account_name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="ifsc_code",
 *          description="ifsc_code",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="account_no",
 *          description="account_no",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="cancle_cheque",
 *          description="cancle_cheque",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="status",
 *          description="status",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_by",
 *          description="created_by",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="updated_by",
 *          description="updated_by",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Bank_information extends Model
{
    use SoftDeletes;

    public $table = 'bank_information';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'bank_name',
        'account_name',
        'ifsc_code',
        'account_no',
        'cancle_cheque',
        'status',
        'created_by',
        'updated_by'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'bank_name' => 'string',
        'account_name' => 'string',
        'ifsc_code' => 'string',
        'account_no' => 'string',
        'cancle_cheque' => 'string',
        'status' => 'integer',
        'created_by' => 'string',
        'updated_by' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'bank_name' => 'required',
        'account_name' => 'required',
        'ifsc_code' => 'required',
        'account_no' => 'required',
        'cancle_cheque' => 'required'
       
    ];

    
}
