<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Set_Consult_Prices",
 *      required={"specialities", "doctor", "consult_price", "date"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="specialities",
 *          description="specialities",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="doctor",
 *          description="doctor",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="consult_price",
 *          description="consult_price",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="date",
 *          description="date",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_by",
 *          description="created_by",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="updated_by",
 *          description="updated_by",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Set_Consult_Prices extends Model
{
    use SoftDeletes;

    public $table = 'set_consult_prices';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'specialities',
        'doctor',
        'consult_price',
        'date',
        'created_by',
        'updated_by'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'specialities' => 'integer',
        'doctor' => 'integer',
        'consult_price' => 'string',
        'date' => 'string',
        'created_by' => 'integer',
        'updated_by' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'specialities' => 'required',
        'doctor' => 'required',
        'consult_price' => 'required',
        'date' => 'required'
    ];

    
}
