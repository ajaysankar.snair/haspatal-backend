<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Haspatal_360_register",
 *      required={"name_prefix", "full_name", "email", "mo_prefix", "mobile_no"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="name_prefix",
 *          description="name_prefix",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="full_name",
 *          description="full_name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="email",
 *          description="email",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="mo_prefix",
 *          description="mo_prefix",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="mobile_no",
 *          description="mobile_no",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_by",
 *          description="created_by",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="updated_by",
 *          description="updated_by",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="status",
 *          description="status",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Haspatal_360_register extends Model
{
    use SoftDeletes;

    public $table = 'haspatal_360_register';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'v_uni_id',
        'name_prefix',
        'full_name',
        'email',
        'mo_prefix',
        'mobile_no',
        'coupon_status',
        'your_coverage_status',
        'buisness_profile',
        'created_by',
        'updated_by',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name_prefix' => 'string',
        'full_name' => 'string',
        'email' => 'string',
        'mo_prefix' => 'string',
        'mobile_no' => 'string',
        'created_by' => 'integer',
        'updated_by' => 'integer',
        'status' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name_prefix' => 'required',
        'full_name' => 'required',
        'email' => 'required|email|unique:users,email',
        'mo_prefix' => 'required',
        'mobile_no' => 'required'
    ];

    
}
