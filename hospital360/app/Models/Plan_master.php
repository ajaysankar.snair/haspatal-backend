<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Plan_master",
 *      required={"pl_name", "pl_price", "pl_desc"},
 *      @SWG\Property(
 *          property="pl_id",
 *          description="pl_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="pl_name",
 *          description="pl_name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="pl_price",
 *          description="pl_price",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="pl_desc",
 *          description="pl_desc",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="status",
 *          description="status",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_by",
 *          description="created_by",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="updated_by",
 *          description="updated_by",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Plan_master extends Model
{
    use SoftDeletes;

    public $table = 'plan_master';
    
    protected $primaryKey = 'pl_id';

    protected $dates = ['deleted_at'];



    public $fillable = [
        'pl_name',
        'pl_price',
        'pl_validity',
        'pl_daily_sub_amount',
        'pl_deduction',
        'pl_desc',
        'status',
        'created_by',
        'updated_by'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'pl_id' => 'integer',
        'pl_name' => 'string',
        'pl_price' => 'integer',
        'pl_desc' => 'string',
        'status' => 'integer',
        'created_by' => 'integer',
        'updated_by' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'pl_name' => 'required',
        'pl_daily_sub_amount' => 'required',
        'pl_deduction' => 'required',
        'pl_validity' => 'required'
    ];

    
}
