<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Patient_add_fund",
 *      required={"patient_id", "requested_amount", "balance_after_approve", "approved_balance", "transaction_type"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="patient_id",
 *          description="patient_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="requested_amount",
 *          description="requested_amount",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="balance_after_approve",
 *          description="balance_after_approve",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="approved_balance",
 *          description="approved_balance",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="transaction_type",
 *          description="transaction_type",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="transaction_proof",
 *          description="transaction_proof",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="transaction_date",
 *          description="transaction_date",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="status",
 *          description="status",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_by",
 *          description="created_by",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="updated_by",
 *          description="updated_by",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Patient_add_fund extends Model
{
    use SoftDeletes;

    public $table = 'patient_add_fund';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'patient_id',
        'payment_id',
        'requested_amount',
        'wa_amount',
        'dr_amount',
        'dr_razorpay_payment_id',
        'doctor_id',
        'dr_w_amount',
        'balance_after_approve',
        'approved_balance',
        'transaction_type',
        'transaction_proof',
        'transaction_date',
        'status',
        'created_by',
        'updated_by'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'patient_id' => 'integer',
        'requested_amount' => 'string',
        'balance_after_approve' => 'string',
        'approved_balance' => 'string',
        'transaction_type' => 'string',
        'transaction_proof' => 'string',
        'transaction_date' => 'string',
        'status' => 'integer',
        'created_by' => 'integer',
        'updated_by' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'patient_id' => 'required'
       
    ];

    
}
