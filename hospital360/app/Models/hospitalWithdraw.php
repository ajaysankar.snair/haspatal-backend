<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class hospitalWithdraw
 * @package App\Models
 * @version October 19, 2020, 7:54 am UTC
 *
 * @property integer $hospital_id
 * @property string $current_balance
 * @property string $requested_amount
 * @property string $available_balance
 * @property integer $created_by
 * @property integer $updated_by
 */
class hospitalWithdraw extends Model
{
    use SoftDeletes;

    public $table = 'hospitalWithdraw';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'hospital_id',
        'current_balance',
        'requested_amount',
        'available_balance',
        'created_by',
        'updated_by'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'hospital_id' => 'integer',
        'current_balance' => 'string',
        'requested_amount' => 'string',
        'available_balance' => 'string',
        'created_by' => 'integer',
        'updated_by' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
