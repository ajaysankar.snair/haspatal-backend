<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Your_coverage_360",
 *      required={"c_state_id", "c_city_id", "c_pincode"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="c_state_id",
 *          description="c_state_id",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="c_city_id",
 *          description="c_city_id",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="c_pincode",
 *          description="c_pincode",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_by",
 *          description="created_by",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="updated_by",
 *          description="updated_by",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="status",
 *          description="status",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Your_coverage_360 extends Model
{
    use SoftDeletes;

    public $table = 'Your_coverage_360';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'c_state_id',
        'user_id',
        'c_city_id',
        'c_pincode',
        'created_by',
        'updated_by',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'c_state_id' => 'string',
        'c_city_id' => 'string',
        'c_pincode' => 'string',
        'created_by' => 'integer',
        'updated_by' => 'integer',
        'status' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'c_state_id' => 'required',
        'c_city_id' => 'required',
        'c_pincode' => 'required'
    ];

    
}
