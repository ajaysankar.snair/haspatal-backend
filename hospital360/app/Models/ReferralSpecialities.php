<?php



namespace App\Models;



use Eloquent as Model;



/**

 * Class ReferralSpecialities

 * @package App\Models

 * @version October 5, 2020, 10:55 am UTC

 *

 * @property integer $dr_id
 * @property integer $specialities_name
 * @property integer $created_by
 * @property integer $updated_by
 */

class ReferralSpecialities extends Model

{



    public $table = 'referral_specialities';

    







    public $fillable = [

        'dr_id',
        'specialities_name',
        'created_by',
        'updated_by'

    ];



    /**

     * The attributes that should be casted to native types.

     *

     * @var array

     */

    protected $casts = [

        'id' => 'integer',
        'dr_id' => 'integer',
        'specialities_name' => 'string',
        'created_by' => 'integer',
        'updated_by' => 'integer'

    ];



    /**

     * Validation rules

     *

     * @var array

     */

    public static $rules = [

        

    ];



    

}

