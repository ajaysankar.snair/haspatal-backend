<?php

namespace App\Models;

use Eloquent as Model;

/**
 * @SWG\Definition(
 *      definition="Patients",
 *      required={"first_name", "last_name", "email", "mobile", "country", "state", "city", "zipcode", "address", "personal_profile", "family_profile", "social_profile", "work_profile", "contact_phone1", "contact_phone2", "contact_phone3", "email1", "email2", "language", "profile_pic"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="first_name",
 *          description="first_name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="last_name",
 *          description="last_name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="email",
 *          description="email",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="mobile",
 *          description="mobile",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="country",
 *          description="country",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="state",
 *          description="state",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="city",
 *          description="city",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="zipcode",
 *          description="zipcode",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="address",
 *          description="address",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="personal_profile",
 *          description="personal_profile",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="family_profile",
 *          description="family_profile",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="social_profile",
 *          description="social_profile",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="work_profile",
 *          description="work_profile",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="contact_phone1",
 *          description="contact_phone1",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="contact_phone2",
 *          description="contact_phone2",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="contact_phone3",
 *          description="contact_phone3",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="email1",
 *          description="email1",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="email2",
 *          description="email2",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="language",
 *          description="language",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="profile_pic",
 *          description="profile_pic",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="status",
 *          description="status",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_by",
 *          description="created_by",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="updated_by",
 *          description="updated_by",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Patients extends Model
{

    public $table = 'patient_details';
    



    public $fillable = [
        'p_uni_id',
        'first_name',
        'last_name',
        'email',
        'mobile',
        'age',
        'gender',
        'height',
        'weight',
        'lmp',
        'country',
        'state',
        'city',
        'zipcode',
        'address',
        'personal_profile',
        'family_profile',
        'social_profile',
        'work_profile',
        'contact_phone1',
        'contact_phone2',
        'contact_phone3',
        'email1',
        'email2',
        'language',
        'profile_pic',
        'status',
        'created_by',
        'updated_by'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'first_name' => 'string',
        'last_name' => 'string',
        'email' => 'string',
        'mobile' => 'string',
        'country' => 'string',
        'state' => 'string',
        'city' => 'string',
        'zipcode' => 'string',
        'address' => 'string',
        'personal_profile' => 'string',
        'family_profile' => 'string',
        'social_profile' => 'string',
        'work_profile' => 'string',
        'contact_phone1' => 'string',
        'contact_phone2' => 'string',
        'contact_phone3' => 'string',
        'email1' => 'string',
        'email2' => 'string',
        'language' => 'string',
        'profile_pic' => 'string',
        'status' => 'integer',
        'created_by' => 'string',
        'updated_by' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'first_name' => 'required',
        'last_name' => 'required',
        'email' => 'required|email|unique:users,email',
        'mobile' => 'required',
        'country' => 'required',
        'state' => 'required',
        'city' => 'required',
        'zipcode' => 'required',
        'address' => 'required',
        'personal_profile' => 'required',
        'family_profile' => 'required',
        'social_profile' => 'required',
        'work_profile' => 'required',
        'contact_phone1' => 'required',
        'contact_phone2' => 'required',
        'contact_phone3' => 'required',
        'email1' => 'required',
        'email2' => 'required',
        'language' => 'required',
        'profile_pic' => 'required'
    ];

    
}
