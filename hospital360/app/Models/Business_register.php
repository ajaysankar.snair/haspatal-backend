<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Business_register",
 *      required={"b_id", "b_name", "gst_no", "working_hr"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="b_id",
 *          description="b_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="b_name",
 *          description="b_name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="gst_no",
 *          description="gst_no",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="working_hr",
 *          description="working_hr",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="address",
 *          description="address",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="country_id",
 *          description="country_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="state_id",
 *          description="state_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="city_id",
 *          description="city_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="pincode",
 *          description="pincode",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="b_lic",
 *          description="b_lic",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_by",
 *          description="created_by",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="updated_by",
 *          description="updated_by",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="status",
 *          description="status",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Business_register extends Model
{
    use SoftDeletes;

    public $table = 'business_register';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'user_id',
        'b_id',
        'b_name',
        'conatact_person',
        'mobile1',
        'mobile2',
        'email',
        'gst_no',
        'working_hr',
        'open_time',
        'close_time',
        'open_24',
        'weekly_off',
        'all_days_open',
        'address',
        'country_id',
        'state_id',
        'city_id',
        'pincode',
        'b_lic',
        'b_card_pic',
        'shop_pic',
        'patient_select_you',
        'image',
        'description',
        'created_by',
        'updated_by',
        'step',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'b_id' => 'integer',
        'b_name' => 'string',
        'gst_no' => 'string',
        'working_hr' => 'string',
        'address' => 'string',
        'country_id' => 'integer',
        'state_id' => 'integer',
        'city_id' => 'integer',
        'pincode' => 'string',
        'b_lic' => 'string',
        'created_by' => 'integer',
        'updated_by' => 'integer',
        'status' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'b_id' => 'required',
        'b_name' => 'required',
        'gst_no' => 'required',
        'working_hr' => 'required'
    ];

    
}
