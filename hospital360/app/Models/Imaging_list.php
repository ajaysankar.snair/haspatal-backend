<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Imaging_list
 * @package App\Models
 * @version October 3, 2020, 10:23 am UTC
 *
 * @property string $imaging_name
 * @property integer $created_by
 * @property integer $updated_by
 */
class Imaging_list extends Model
{
    use SoftDeletes;

    public $table = 'imaging_list';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'imaging_name',
        'created_by',
        'updated_by'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'imaging_name' => 'string',
        'created_by' => 'integer',
        'updated_by' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
