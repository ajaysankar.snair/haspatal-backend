<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Present_complaint_patient",
 *      required={"present_complaint"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="pc_patient_id",
 *          description="pc_patient_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="pc_doctor_id",
 *          description="pc_doctor_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="book_id",
 *          description="book_id",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="present_complaint",
 *          description="present_complaint",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="complaint_pic",
 *          description="complaint_pic",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="complaint_pdf",
 *          description="complaint_pdf",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_by",
 *          description="created_by",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="updated_by",
 *          description="updated_by",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Present_complaint_patient extends Model
{
    use SoftDeletes;

    public $table = 'present_complaint_patient';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'pc_patient_id',
        'pc_doctor_id',
        'book_id',
        'present_complaint',
        'complaint_pic',
        'complaint_pdf',
        'created_by',
        'updated_by'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'pc_patient_id' => 'integer',
        'pc_doctor_id' => 'integer',
        'book_id' => 'string',
        'present_complaint' => 'string',
        'complaint_pic' => 'string',
        'complaint_pdf' => 'string',
        'created_by' => 'integer',
        'updated_by' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'present_complaint' => 'required'
    ];

    
}
