<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class FollowupPeriod
 * @package App\Models
 * @version October 5, 2020, 11:04 am UTC
 *
 * @property integer $dr_id
 * @property string $followup_list
 * @property integer $created_by
 * @property integer $updated_by
 */
class FollowupPeriod extends Model
{

    public $table = 'followup_period';
    



    public $fillable = [
        'dr_id',
        'followup_list',
        'created_by',
        'updated_by'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'dr_id' => 'integer',
        'followup_list' => 'string',
        'created_by' => 'integer',
        'updated_by' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
