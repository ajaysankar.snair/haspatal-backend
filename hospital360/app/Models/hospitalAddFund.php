<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class hospitalAddFund
 * @package App\Models
 * @version October 19, 2020, 7:22 am UTC
 *
 * @property integer $hospital_id
 * @property string $payment_id
 * @property string $h_amount
 * @property integer $created_by
 * @property integer $updated_by
 */
class hospitalAddFund extends Model
{
    use SoftDeletes;

    public $table = 'hospitalAddFund';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'hospital_id',
        'payment_id',
        'h_amount',
        'created_by',
        'updated_by'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'hospital_id' => 'integer',
        'payment_id' => 'string',
        'h_amount' => 'string',
        'created_by' => 'integer',
        'updated_by' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
