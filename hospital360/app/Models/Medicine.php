<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Medicine
 * @package App\Models
 * @version October 3, 2020, 5:16 am UTC
 *
 * @property integer $dr_id
 * @property string $generic_name
 * @property string $medicine_name
 * @property string $drug_from
 * @property string $drug_strength
 * @property integer $created_by
 * @property integer $updated_by
 */
class Medicine extends Model
{
    use SoftDeletes;

    public $table = 'medicine_list';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'dr_id',
        'generic_name',
        'medicine_name',
        'drug_from',
        'drug_strength',
        'created_by',
        'updated_by'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'dr_id' => 'integer',
        'generic_name' => 'string',
        'medicine_name' => 'string',
        'drug_from' => 'string',
        'drug_strength' => 'string',
        'created_by' => 'integer',
        'updated_by' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
