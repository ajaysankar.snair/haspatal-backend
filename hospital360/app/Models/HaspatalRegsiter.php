<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class HaspatalRegsiter
 * @package App\Models
 * @version July 13, 2020, 10:22 am UTC
 *
 * @property integer country_id
 * @property integer state_id
 * @property integer city_id
 * @property integer pincode_id
 * @property string hospital_name
 * @property string address
 * @property string year_of_establishment
 * @property string gst_no
 * @property string license_copy
 * @property string website
 * @property string contact_person
 * @property string contact_person_email
 * @property string contact_person_phonoe_no
 * @property string other_phone_no
 * @property string support_watsapp_no
 */
class HaspatalRegsiter extends Model
{
    use SoftDeletes;

    public $table = 'haspatal_regsiter';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'country_id',
        'state_id',
        'h_uni_id',
        'city_id',
        'pincode_id',
        'hospital_name',
        'bio',
        'address',
        'year_of_establishment',
        'gst_no',
        'license_copy',
        'hospital_profile',
        'hospital_location',
        'email',
        'mobile',
        'telemedicine_coordinator',
        'pancard_number',
        'logo',
        'wallpaper',
        'website',
        'contact_person',
        'contact_person_email',
        'contact_person_phonoe_no',
        'other_phone_no',
        'support_watsapp_no',
        'services',
        'specialities',
        'bank_name',
        'account_name',
        'ifsc_code',
        'account_no',
        'cheque_img',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'country_id' => 'integer',
        'state_id' => 'integer',
        'city_id' => 'integer',
        'pincode_id' => 'integer',
        'hospital_name' => 'string',
        'address' => 'string',
        'year_of_establishment' => 'string',
        'gst_no' => 'string',
        'license_copy' => 'string',
        'website' => 'string',
        'contact_person' => 'string',
        'contact_person_email' => 'string',
        'contact_person_phonoe_no' => 'string',
        'other_phone_no' => 'string',
        'support_watsapp_no' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'country_id' => 'required',
        'state_id' => 'required',
        'city_id' => 'required',
        'pincode_id' => 'required',
        'hospital_name' => 'required',
        'address' => 'required',
        'year_of_establishment' => 'required',
        'gst_no' => 'required',
       // 'license_copy' => 'required',
        'website' => 'required',
        'contact_person' => 'required',
        'contact_person_email' => 'required',
        'contact_person_phonoe_no' => 'required',
        'other_phone_no' => 'required',
        'support_watsapp_no' => 'required'
    ];

    
}
