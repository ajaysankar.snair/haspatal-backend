<?php



namespace App\Models;



use Eloquent as Model;

use Illuminate\Database\Eloquent\SoftDeletes;


/**

 * Class Task

 * @package App\Models

 * @version November 3, 2020, 5:08 pm IST

 *

 * @property integer $patient_id
 * @property integer $status
 * @property string $register_date
 */

class Task extends Model

{

    use SoftDeletes;


    public $table = 'task';

    


    protected $dates = ['deleted_at'];






    public $fillable = [

        'patient_id',
        'status',
        'operator_id',
        'issue',
        'register_date'

    ];



    /**

     * The attributes that should be casted to native types.

     *

     * @var array

     */

    protected $casts = [

        'id' => 'integer',
        'patient_id' => 'integer',
        'patient_contactinfo' => 'integer',
        'patient_addressinfo' => 'integer',
        'status' => 'integer',
        'register_date' => 'datetime'

    ];



    /**

     * Validation rules

     *

     * @var array

     */

    public static $rules = [

        

    ];



    

}

