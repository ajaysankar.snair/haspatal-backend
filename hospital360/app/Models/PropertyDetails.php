<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="PropertyDetails",
 *      required={"property_type", "propery_name", "no_of_house", "serve_no", "area", "address", "country", "state", "city"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="property_type",
 *          description="property_type",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="propery_name",
 *          description="propery_name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="no_of_floor",
 *          description="no_of_floor",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="no_of_street",
 *          description="no_of_street",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="no_of_house",
 *          description="no_of_house",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="serve_no",
 *          description="serve_no",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="area",
 *          description="area",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="address",
 *          description="address",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="country",
 *          description="country",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="state",
 *          description="state",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="city",
 *          description="city",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="status",
 *          description="status",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_by",
 *          description="created_by",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="updated_by",
 *          description="updated_by",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class PropertyDetails extends Model
{
    use SoftDeletes;

    public $table = 'property_details';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'property_type',
        'propery_name',
        'no_of_floor',
        'no_of_street',
        'no_of_house',
        'serve_no',
        'area',
        'address',
        'country',
        'state',
        'city',
        'status',
        'created_by',
        'updated_by'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'property_type' => 'integer',
        'propery_name' => 'string',
        'no_of_floor' => 'string',
        'no_of_street' => 'string',
        'no_of_house' => 'string',
        'serve_no' => 'string',
        'area' => 'string',
        'address' => 'string',
        'country' => 'string',
        'state' => 'string',
        'city' => 'string',
        'status' => 'integer',
        'created_by' => 'integer',
        'updated_by' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'property_type' => 'required',
        'propery_name' => 'required',
        'no_of_house' => 'required',
        'serve_no' => 'required',
        'area' => 'required',
        'address' => 'required',
        'country' => 'required',
        'state' => 'required',
        'city' => 'required'
    ];

    
}
