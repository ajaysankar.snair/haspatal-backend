<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Prescription_details",
 *      required={"patient_id", "doctor_id", "booking_id", "prescription", "prescription_image"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="patient_id",
 *          description="patient_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="doctor_id",
 *          description="doctor_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="booking_id",
 *          description="booking_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="prescription",
 *          description="prescription",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="prescription_image",
 *          description="prescription_image",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="status",
 *          description="status",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_by",
 *          description="created_by",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="updated_by",
 *          description="updated_by",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Prescription_details extends Model
{
    use SoftDeletes;

    public $table = 'prescription_details';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'patient_id',
        'doctor_id',
        'booking_id',
        'age',
        'gender',
        'height',
        'weight',
        'lmp',
        'medicine_id',
        'faq_administration',
        'duration',
        'lab_test_id',
        'imaging_id',
        'therapy_id',
        'counseling_id',
        'home_care_id',
        'referral_specialit_id',
        'folloup_id',
        'prescription',
        'prescription_image',
        'status',
        'pdf',
        'created_by',
        'updated_by'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'patient_id' => 'integer',
        'doctor_id' => 'integer',
        'booking_id' => 'string',
        'prescription' => 'string',
        'prescription_image' => 'string',
        'pdf' => 'string',
        'status' => 'integer',
        'created_by' => 'integer',
        'updated_by' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'patient_id' => 'required',
        'doctor_id' => 'required',
        'booking_id' => 'required',
        'prescription' => 'required',
    ];

    
}
