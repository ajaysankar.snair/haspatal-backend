<?php

namespace App\Models;

use Eloquent as Model;

/**
 * @SWG\Definition(
 *      definition="Doctor_availability",
 *      required={"doctor_id", "date", "time_slot"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="doctor_id",
 *          description="doctor_id",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="date",
 *          description="date",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="time_slot",
 *          description="time_slot",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="status",
 *          description="status",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_by",
 *          description="created_by",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="updated_by",
 *          description="updated_by",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Doctor_availability extends Model
{

    public $table = 'doctor_availability';
    



    public $fillable = [
        'doctor_id',
        'date',
        'booking_time_slot',
        'time_status',
        'nonavabile_time_slot',
        'mrg_from_time',
        'mrg_to_time',
        'aft_from_time',
        'aft_to_time',
        'eve_from_time',
        'eve_to_time',
        'status',
        'doctor_status',
        'created_by',
        'updated_by'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'doctor_id' => 'string',
        'date' => 'string',
        'mrg_from_time' => 'string',
        'mrg_to_time' => 'string',
        'aft_from_time' => 'string',
        'aft_to_time' => 'string',
        'eve_from_time' => 'string',
        'eve_to_time' => 'string',
        'status' => 'integer',
        'created_by' => 'string',
        'updated_by' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'doctor_id' => 'required',
        'date' => 'required',
        
    ];
}
