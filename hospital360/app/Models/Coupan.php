<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Coupan",
 *      required={"promo_code", "used_promo_code_time", "amount_type", "price", "sdate", "edate"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="promo_code",
 *          description="promo_code",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="used_promo_code_time",
 *          description="used_promo_code_time",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="amount_type",
 *          description="amount_type",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="price",
 *          description="price",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="sdate",
 *          description="sdate",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="edate",
 *          description="edate",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Coupan extends Model
{
    use SoftDeletes;

    public $table = 'coupan';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'type',
        'promo_code',
        'used_promo_code_time',
        'amount_type',
        'price',
        'sdate',
        'edate'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'promo_code' => 'string',
        'used_promo_code_time' => 'string',
        'amount_type' => 'string',
        'price' => 'integer',
        'sdate' => 'string',
        'edate' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'promo_code' => 'required',
        'used_promo_code_time' => 'required',
        'amount_type' => 'required',
        'price' => 'required',
        'sdate' => 'required',
        'edate' => 'required'
    ];

    
}
