<?php

namespace App\Models;

use Eloquent as Model;

/**
 * @SWG\Definition(
 *      definition="TimeSlot",
 *      required={"booking_time_slot", "mrg_from_time", "mrg_to_time", "aft_from_time", "aft_to_time", "eve_from_time", "eve_to_time"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="doctor_id",
 *          description="doctor_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="booking_time_slot",
 *          description="booking_time_slot",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="mrg_from_time",
 *          description="mrg_from_time",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="mrg_to_time",
 *          description="mrg_to_time",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="aft_from_time",
 *          description="aft_from_time",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="aft_to_time",
 *          description="aft_to_time",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="eve_from_time",
 *          description="eve_from_time",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="eve_to_time",
 *          description="eve_to_time",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="status",
 *          description="status",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_by",
 *          description="created_by",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="updated_by",
 *          description="updated_by",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class TimeSlot extends Model
{

    public $table = 'time_slot';
    



    public $fillable = [
        'doctor_id',
        'booking_time_slot',
        'mrg_from_time',
        'mrg_to_time',
        'aft_from_time',
        'aft_to_time',
        'eve_from_time',
        'eve_to_time',
        'status',
        'created_by',
        'updated_by'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'doctor_id' => 'integer',
        'booking_time_slot' => 'string',
        'mrg_from_time' => 'string',
        'mrg_to_time' => 'string',
        'aft_from_time' => 'string',
        'aft_to_time' => 'string',
        'eve_from_time' => 'string',
        'eve_to_time' => 'string',
        'status' => 'integer',
        'created_by' => 'string',
        'updated_by' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'booking_time_slot' => 'required',
        'mrg_from_time' => 'required',
        'mrg_to_time' => 'required',
        'aft_from_time' => 'required',
        'aft_to_time' => 'required',
        'eve_from_time' => 'required',
        'eve_to_time' => 'required'
    ];

    
}
