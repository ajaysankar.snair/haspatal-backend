<?php



namespace App\Models;



use Eloquent as Model;

use Illuminate\Database\Eloquent\SoftDeletes;


/**

 * Class Buy_plan_dr

 * @package App\Models

 * @version September 28, 2020, 10:51 am UTC

 *

 * @property integer $dr_id
 * @property integer $plan_id
 * @property string $payable_amount
 * @property string $promo_code
 * @property string $discount
 * @property string $gst_amount
 * @property string $final_amount
 * @property string $gst_number
 * @property string $payment_id
 * @property integer $payment_status
 */

class Buy_plan_dr extends Model

{

    use SoftDeletes;


    public $table = 'buy_plan_dr';

    


    protected $dates = ['deleted_at'];






    public $fillable = [

        'user_id',
        'plan_id',
        'payable_amount',
        'promo_code',
        'discount',
        'gst_amount',
        'final_amount',
        'gst_number',
        'payment_id',
        'payment_status'

    ];



    /**

     * The attributes that should be casted to native types.

     *

     * @var array

     */

    protected $casts = [

        'id' => 'integer',
        'user_id' => 'integer',
        'plan_id' => 'integer',
        'payable_amount' => 'string',
        'promo_code' => 'string',
        'discount' => 'string',
        'gst_amount' => 'string',
        'final_amount' => 'string',
        'gst_number' => 'string',
        'payment_id' => 'string',
        'payment_status' => 'integer'

    ];



    /**

     * Validation rules

     *

     * @var array

     */

    public static $rules = [

        

    ];



    

}

