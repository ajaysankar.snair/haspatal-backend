<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Patient_refund_request",
 *      required={"requested_amount", "balance_after_approve", "approved_balance"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="requested_amount",
 *          description="requested_amount",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="balance_after_approve",
 *          description="balance_after_approve",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="approved_balance",
 *          description="approved_balance",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="status",
 *          description="status",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_by",
 *          description="created_by",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="updated_by",
 *          description="updated_by",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Patient_refund_request extends Model
{
    use SoftDeletes;

    public $table = 'patient_refund_request';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'patient_id',
        'requested_amount',
        'balance_after_approve',
        'approved_balance',
        'bank_name',
        'bank_ifsc_code',
        'account_name',
        'account_type',
        'cheque_img',
        'status',
        'created_by',
        'updated_by'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'patient_id' => 'integer',
        'requested_amount' => 'string',
        'balance_after_approve' => 'string',
        'approved_balance' => 'string',
        'status' => 'integer',
        'created_by' => 'integer',
        'updated_by' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'requested_amount' => 'required',
        'balance_after_approve' => 'required',
        'approved_balance' => 'required'
    ];

    
}
