<?php

namespace App\Models;

use Eloquent as Model;

/**
 * @SWG\Definition(
 *      definition="Doctors",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="first_name",
 *          description="first_name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="last_name",
 *          description="last_name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="email",
 *          description="email",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="bio",
 *          description="bio",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="pincode",
 *          description="pincode",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="state",
 *          description="state",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="city",
 *          description="city",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="address",
 *          description="address",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="main_mobile",
 *          description="main_mobile",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="second_mobile",
 *          description="second_mobile",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="clinic_team_leader",
 *          description="clinic_team_leader",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="experience",
 *          description="experience",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="language",
 *          description="language",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="speciality",
 *          description="speciality",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="licence_type",
 *          description="licence_type",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="licence_no",
 *          description="licence_no",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="valide_upto",
 *          description="valide_upto",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="issued_by",
 *          description="issued_by",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="licence_copy",
 *          description="licence_copy",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="profile_pic",
 *          description="profile_pic",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="status",
 *          description="status",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_by",
 *          description="created_by",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="updated_by",
 *          description="updated_by",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Doctors extends Model
{

    public $table = 'doctor_details';
    



    public $fillable = [
        'first_name',
        'last_name',
        'd_uni_id',
        'plan_id',
        'email',
        'bio',
        'pincode',
        'state',
        'city',
        'address',
        'main_mobile',
        'second_mobile',
        'clinic_team_leader',
        'clinic_admin_mobile_no',
        'clinic_email',
        'year_of_graduation',
        'clinic_wallpapper',
        'clinic_buisness_name',
        'pan_number',
        'clinic_name',
        'experience',
        'language',
        'price',
        'speciality',
        'licence_type',
        'licence_no',
        'valide_upto',
        'issued_by',
        'licence_copy',
        'profile_pic',
        'clinic_logo',        
        'bank_name',
        'account_name',
        'ifsc_code',
        'step',
        'gst_number',
        'account_no',
        'cheque_img',
        'watsapp_number',
        'youtube_link',
        'teligram_number',
        'status',
        'hospital_id',
        'created_by',
        'updated_by'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'first_name' => 'string',
        'last_name' => 'string',
        'email' => 'string',
        'bio' => 'string',
        'pincode' => 'string',
        'state' => 'string',
        'city' => 'string',
        'address' => 'string',
        'main_mobile' => 'string',
        'second_mobile' => 'string',
        'clinic_team_leader' => 'string',
        'experience' => 'string',
        'language' => 'string',
        'price' => 'string',
        'speciality' => 'string',
        'licence_type' => 'string',
        'licence_no' => 'string',
        'valide_upto' => 'string',
        'issued_by' => 'string',
        'licence_copy' => 'string',
        'profile_pic' => 'string',
        'watsapp_number' => 'string',
        'youtube_link' => 'string',
        'teligram_number' => 'string',
        'status' => 'integer',
        'created_by' => 'string',
        'updated_by' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'email' => 'required|email|unique:users,email',
        'main_mobile' => 'required|unique:doctor_details,main_mobile'
    ];

    
}
