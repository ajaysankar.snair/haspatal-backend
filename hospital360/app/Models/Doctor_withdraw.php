<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Doctor_withdraw",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="doctor_id",
 *          description="doctor_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="current_balance",
 *          description="current_balance",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="requested_amount",
 *          description="requested_amount",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="available_balance",
 *          description="available_balance",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_by",
 *          description="created_by",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="updated_by",
 *          description="updated_by",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="status",
 *          description="status",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Doctor_withdraw extends Model
{
    use SoftDeletes;

    public $table = 'Doctor_withdraw';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'doctor_id',
        'current_balance',
        'requested_amount',
        'available_balance',
        'created_by',
        'updated_by',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'doctor_id' => 'integer',
        'current_balance' => 'string',
        'requested_amount' => 'string',
        'available_balance' => 'string',
        'created_by' => 'integer',
        'updated_by' => 'integer',
        'status' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
