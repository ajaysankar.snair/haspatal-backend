<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Hospital_register",
 *      required={"hospital_name", "email", "address", "contact_person_name", "contact_details", "date_time", "logo"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="hospital_name",
 *          description="hospital_name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="email",
 *          description="email",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="address",
 *          description="address",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="contact_person_name",
 *          description="contact_person_name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="contact_details",
 *          description="contact_details",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="date_time",
 *          description="date_time",
 *          type="string",
 *          format="date"
 *      ),
 *      @SWG\Property(
 *          property="logo",
 *          description="logo",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_by",
 *          description="created_by",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="updated_by",
 *          description="updated_by",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Hospital_register extends Model
{
    use SoftDeletes;

    public $table = 'hospital_register';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'hospital_name',
        'email',
        'country',
        'state',
        'city',
        'role_id',
        'address',
        'contact_person_name',
        'contact_details',
        'date_time',
        'logo',
        'status',
        'services',
        'specialities',
        'created_by',
        'updated_by'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'hospital_name' => 'string',
        'email' => 'string',
        'address' => 'string',
        'contact_person_name' => 'string',
        'contact_details' => 'string',
        'date_time' => 'date',
        'logo' => 'string',
        'created_by' => 'integer',
        'updated_by' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'hospital_name' => 'required',
        'email' => 'required|unique:hospital_register,email',
        'address' => 'required',
        'contact_person_name' => 'required',
        'contact_details' => 'required',
        'date_time' => 'required',
        'logo' => 'required'
    ];

    
}
