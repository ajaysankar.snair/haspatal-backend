<?php

namespace App\Models;

use Eloquent as Model;

/**
 * @SWG\Definition(
 *      definition="BookingRequest",
 *      required={"patient_name", "doctor_name", "date_time", "payment_status"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="patient_name",
 *          description="patient_name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="doctor_name",
 *          description="doctor_name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="date_time",
 *          description="date_time",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="payment_status",
 *          description="payment_status",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="status",
 *          description="status",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_by",
 *          description="created_by",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="updated_by",
 *          description="updated_by",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class BookingRequest extends Model
{

    public $table = 'booking_request';
    



    public $fillable = [
        'patient_name',
        'doctor_name',
        'book_id',
        'date_time',
        'payment_id',
        'promo_code',
        'dr_price',
        'discount',
        'f_amount',
        'payment_status',
        'status',
        'consult_mode',
        'w_status',
        'slot_status',
        'w_amount',
        'created_by',
        'updated_by'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'patient_name' => 'string',
        'doctor_name' => 'string',
        'book_id' => 'string',
        'date_time' => 'string',
        'payment_status' => 'integer',
        'payment_id' => 'string',
        'status' => 'integer',
        'created_by' => 'string',
        'updated_by' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'patient_name' => 'required',
        'doctor_name' => 'required',
        'date_time' => 'required',
        
    ];

    
}
