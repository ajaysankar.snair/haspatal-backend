<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Tax
 * @package App\Models
 * @version September 22, 2020, 5:38 am UTC
 *
 * @property string $tax_name
 * @property string $tax_percentage
 */
class Tax extends Model
{
    use SoftDeletes;

    public $table = 'tax';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'tax_name',
        'tax_percentage'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'tax_name' => 'string',
        'tax_percentage' => 'string',
        'created_by' => 'integer',
        'updated_by' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
