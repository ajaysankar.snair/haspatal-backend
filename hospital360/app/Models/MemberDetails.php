<?php

namespace App\Models;

use Eloquent as Model;

/**
 * @SWG\Definition(
 *      definition="MemberDetails",
 *      required={"first_name", "last_name", "email", "mobile", "floor_no", "street_no", "house_no", "id_proof", "image", "no_of_family_member", "notification_type", "member_type", "status", "ctreate_by", "updated_by"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="first_name",
 *          description="first_name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="last_name",
 *          description="last_name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="email",
 *          description="email",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="mobile",
 *          description="mobile",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="floor_no",
 *          description="floor_no",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="street_no",
 *          description="street_no",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="house_no",
 *          description="house_no",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="id_proof",
 *          description="id_proof",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="image",
 *          description="image",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="no_of_family_member",
 *          description="no_of_family_member",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="notification_type",
 *          description="notification_type",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="member_type",
 *          description="member_type",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="status",
 *          description="status",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="ctreate_by",
 *          description="ctreate_by",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="updated_by",
 *          description="updated_by",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class MemberDetails extends Model
{

    public $table = 'member_details';
    



    public $fillable = [
        'first_name',
        'last_name',
        'email',
        'mobile',
        'floor_no',
        'street_no',
        'house_no',
        'id_proof',
        'image',
        'no_of_family_member',
        'notification_type',
        'member_type',
        'status',
        'ctreate_by',
        'updated_by'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'first_name' => 'string',
        'last_name' => 'string',
        'email' => 'string',
        'mobile' => 'string',
        'floor_no' => 'string',
        'street_no' => 'string',
        'house_no' => 'string',
        'id_proof' => 'string',
        'image' => 'string',
        'no_of_family_member' => 'string',
        'notification_type' => 'integer',
        'member_type' => 'integer',
        'status' => 'integer',
        'ctreate_by' => 'integer',
        'updated_by' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'first_name' => 'required',
        'last_name' => 'required',
        'email' => 'required',
        'mobile' => 'required',
        'floor_no' => '',
        'street_no' => '',
        'house_no' => 'required',
        'id_proof' => 'required',
        'image' => 'required',
        'no_of_family_member' => 'required',
        'notification_type' => 'required',
        'member_type' => 'required',
        'status' => '',
        'ctreate_by' => '',
        'updated_by' => ''
    ];

    
}
