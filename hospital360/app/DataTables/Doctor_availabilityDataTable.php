<?php

namespace App\DataTables;

use App\Models\Doctor_availability;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Auth;
class Doctor_availabilityDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable->addColumn('action', 'doctor_availabilities.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Doctor_availability $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Doctor_availability $model)
    {
        if(Auth::user()->role_id == 3)
        { 
            return $model->join('doctor_details','doctor_details.id','doctor_availability.doctor_id')->select('doctor_availability.*','doctor_details.first_name as doctor_name')->where('doctor_id',Auth::user()->userDetailsId);
        }
        else
        {
             return $model->join('doctor_details','doctor_details.id','doctor_availability.doctor_id')->select('doctor_availability.*','doctor_details.first_name as doctor_name');
        }

    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false])
            ->parameters([
                'dom'       => 'Bflrtip',
                'stateSave' => true,
                'order'     => [[0, 'desc']],
                'buttons'   => [
                    ['extend' => 'create', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'export', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'print', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'reset', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'reload', 'className' => 'btn btn-default btn-sm no-corner',],
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'doctor_name',
            'date',
            
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'doctor_availabilitiesdatatable_' . time();
    }
}
