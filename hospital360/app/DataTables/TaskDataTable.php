<?php



namespace App\DataTables;



use App\Models\Task;

use Yajra\DataTables\Services\DataTable;

use Yajra\DataTables\EloquentDataTable;



class TaskDataTable extends DataTable

{

    /**

     * Build DataTable class.

     *

     * @param mixed $query Results from query() method.

     * @return \Yajra\DataTables\DataTableAbstract

     */

    public function dataTable($query)

    {

       /* $dataTable = new EloquentDataTable($query);


        return $dataTable;*/

        return datatables($query)
                ->addIndexColumn()

                ->addColumn('patient_status',function($row){
                    //return view('doctors.status',['id'=>$row->id,'status' => $row->status])->render();
                    if($row->patient_contactinfo == 1 && $row->patient_addressinfo == 1){
                        return "<span class='badge badge-danger'>Contact Info </span><br><span class='badge badge-danger'>Address Info </span>";
                    }elseif($row->patient_contactinfo == 1){
                        return "<span class='badge badge-danger'>Contact Info </span>";
                    }else{
                        return "<span class='badge badge-danger'>Address Info </span>";
                    }
                    
                }) 
                ->addColumn('register_date',function($row){
                    //return view('doctors.status',['id'=>$row->id,'status' => $row->status])->render();
                    return \Carbon\Carbon::createFromTimeStamp(strtotime($row->register_date))->diffForHumans();
                }) 
                ->addColumn('assign',function($row){
                    return view('tasks.assign',['id'=>$row->id,'operator_id'=>$row->operator_id])->render();
                }) 
                ->addColumn('status',function($row){
                    return view('tasks.status',['id'=>$row->id,'status'=>$row->status])->render();
                }) 
                ->rawColumns(['status','assign', 'patient_status','register_date']);
        //return $dataTable->addColumn('action', 'tasks.datatables_actions');

    }



    /**

     * Get query source of dataTable.

     *

     * @param \App\Models\Task $model

     * @return \Illuminate\Database\Eloquent\Builder

     */

    public function query(Task $model)

    {

        return $model->newQuery()->join('patient_details','task.patient_id','patient_details.id')->select('patient_details.first_name as patient_name','patient_details.mobile','patient_details.email','task.*');

    }



    /**

     * Optional method if you want to use html builder.

     *

     * @return \Yajra\DataTables\Html\Builder

     */

    public function html()

    {

        return $this->builder()

            ->columns($this->getColumns())

            ->minifiedAjax()

            //->addAction(['width' => '120px', 'printable' => false])

            ->parameters([

                'dom'       => 'Blfrtip',

                'stateSave' => true,

                'order'     => [[0, 'desc']],
                'buttons'   => [
                ],

            ]);

    }



    /**

     * Get columns.

     *

     * @return array

     */

    protected function getColumns()

    {

        return [

            'patient_name',
            'email',
            'mobile',
            'patient_status',
            'register_date',
            'assign',
            'status',

        ];

    }



    /**

     * Get filename for export.

     *

     * @return string

     */

    protected function filename()

    {

        return 'tasks_datatable_' . time();

    }

}

