<?php

namespace App\DataTables;

use App\Models\BookingRequest;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Auth;
class BookingRequestDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable->addColumn('action', 'booking_requests.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\BookingRequest $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(BookingRequest $model)
    {
    	
        //return $model->newQuery();
        if(Auth::user()->user_type == 1)
        {
        	return $model->join('patient_details','patient_details.id','booking_request.patient_name')
        			 ->join('doctor_details','doctor_details.id','booking_request.doctor_name')
        			 ->select('booking_request.*','patient_details.first_name as patient','doctor_details.first_name as doctor')
        			 ->where('booking_request.doctor_name',Auth::user()->userDetailsId);	
        }
        else
        {
        	return $model->join('patient_details','patient_details.id','booking_request.patient_name')
        			 ->join('doctor_details','doctor_details.id','booking_request.doctor_name')
        			 ->select('booking_request.*','patient_details.first_name as patient','doctor_details.first_name as doctor');
        }
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false])
            ->parameters([
                'dom'       => 'Bflrtip',
                'stateSave' => true,
                'order'     => [[0, 'desc']],
                'buttons'   => [
                    ['extend' => 'create', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'export', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'print', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'reset', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'reload', 'className' => 'btn btn-default btn-sm no-corner',],
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'patient',
            'doctor',
            'date_time',
            'payment_status',
            
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'booking_requestsdatatable_' . time();
    }
}
