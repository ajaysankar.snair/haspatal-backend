<?php

namespace App\DataTables;

use App\Models\Patients;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

class PatientsDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return datatables($query)
                ->addIndexColumn()
              
                ->addColumn('status',function($row){
                        return view('patients.status',['id'=>$row->id,'patient_login_status_contactinfo'=>$row->patient_login_status_contactinfo,'patient_login_status_addressinfo'=>$row->patient_login_status_addressinfo])->render();
                    }) 
                ->addColumn('action', 'patients.datatables_actions')->rawColumns(['status', 'action']);

        //return $dataTable->addColumn('action', 'patients.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Patients $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Patients $model)
    {
        return $model->newQuery()->join('users','users.userDetailsId','patient_details.id')->where('user_type',2)->select('patient_details.*','users.patient_login_status_contactinfo','users.patient_login_status_addressinfo');
        // return $model->join('country','country.id','patient_details.country')->join('state','state.id','patient_details.state')->join('city','city.id','patient_details.city')->select('patient_details.*','country.country','state.state','city.city');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false])
            ->parameters([
                'dom'       => 'Bflrtip',
                'stateSave' => true,
                'order'     => [[0, 'desc']],
                'buttons'   => [
                    ['extend' => 'create', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'export', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'print', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'reset', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'reload', 'className' => 'btn btn-default btn-sm no-corner',],
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'first_name',
            'email',
            'mobile',
            'zipcode',
            'address',
            'status',
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'patientsdatatable_' . time();
    }
}
