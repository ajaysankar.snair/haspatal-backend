<?php

namespace App\DataTables;

use App\Models\Plan_master;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

class Plan_masterDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        $dataTable = new EloquentDataTable($query);

        return datatables($query)
                ->addIndexColumn()
              
                ->addColumn('status',function($row){
                        return view('plan_masters.status',['id'=>$row->pl_id,'status' => $row->status])->render();
                    }) 
                ->addColumn('action', 'plan_masters.datatables_actions')->rawColumns(['status', 'action']);

        /*return $dataTable->addColumn('action', 'plan_masters.datatables_actions');*/
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Plan_master $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Plan_master $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false])
            ->parameters([
                'dom'       => 'Bflrtip',
                'stateSave' => true,
                'order'     => [[0, 'desc']],
                'buttons'   => [
                    ['extend' => 'create', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'export', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'print', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'reset', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'reload', 'className' => 'btn btn-default btn-sm no-corner',],
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'pl_name',
            'pl_deduction',
            'pl_daily_sub_amount',
            'pl_validity',
            'status'
            /*'created_by',
            'updated_by'*/
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'plan_mastersdatatable_' . time();
    }
}
