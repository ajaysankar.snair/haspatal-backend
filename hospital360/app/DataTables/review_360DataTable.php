<?php

namespace App\DataTables;

use App\Models\review_360;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

class review_360DataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable->addColumn('action', 'review_360s.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\review_360 $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(review_360 $model)
    {
        return $model->leftJoin('patient_details','review_360s.patient_id','patient_details.id')->select('review_360s.*','patient_details.first_name as patient');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false])
            ->parameters([
                'dom'       => 'Bflrtip',
                'stateSave' => true,
                'order'     => [[0, 'desc']],
                'buttons'   => [
                    ['extend' => 'create', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'export', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'print', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'reset', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'reload', 'className' => 'btn btn-default btn-sm no-corner',],
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'patient',
            'rating',
            'review_que',
            'review_ans',
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'review_360sdatatable_' . time();
    }
}
