<?php

namespace App\DataTables;

use App\Models\Doctors;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Auth;
class DoctorsDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return datatables($query)
                ->addIndexColumn()
              
                ->addColumn('status',function($row){
                        return view('doctors.status',['id'=>$row->id,'status' => $row->status])->render();
                    }) 
                ->addColumn('action', 'doctors.datatables_actions')->rawColumns(['status', 'action']);
        
        //return $dataTable->addColumn('action', 'doctors.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Doctors $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Doctors $model)
    {
        //return $model->newQuery();
        if (Auth::user()->roleUser->role->id == 1) {
            return $model->leftJoin('state','state.id','doctor_details.state')
                    ->leftJoin('city','city.id','doctor_details.city')
                     ->leftJoin('users','doctor_details.id','users.userDetailsId')
                    ->where('users.user_type',1)
                    ->select('doctor_details.*','state.state','city.city','users.status');
        }else{
            return $model->leftJoin('state','state.id','doctor_details.state')
                    ->leftJoin('city','city.id','doctor_details.city')
                    ->where('doctor_details.created_by',Auth::user()->id)
                    ->leftJoin('users','doctor_details.id','users.userDetailsId')
                    ->where('users.user_type',1)
                    ->select('doctor_details.*','state.state','city.city','users.status');
        }
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false])
            ->parameters([
                'dom'       => 'Bflrtip',
                'stateSave' => true,
                'order'     => [[0, 'desc']],
                'buttons'   => [
                    ['extend' => 'create', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'export', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'print', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'reset', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'reload', 'className' => 'btn btn-default btn-sm no-corner',],
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'first_name',
            'last_name',
            'email',
            
            'pincode',
            'state',
            'city',
           
            'main_mobile',
            'status',
            
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'doctorsdatatable_' . time();
    }
}
