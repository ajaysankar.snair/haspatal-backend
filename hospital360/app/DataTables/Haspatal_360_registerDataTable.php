<?php

namespace App\DataTables;

use App\Models\Haspatal_360_register;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

class Haspatal_360_registerDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return datatables($query)
                ->addIndexColumn()
              
                ->addColumn('status',function($row){
                        return view('haspatal_360_registers.status',['id'=>$row->id,'status' => $row->userStatus])->render();
                    }) 
                ->addColumn('action', 'haspatal_360_registers.datatables_actions')->rawColumns(['status', 'action']);

        
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Haspatal_360_register $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Haspatal_360_register $model)
    {
        return $model->newQuery()->orderBy('id','desc')
                                ->leftJoin('users','haspatal_360_register.id','users.userDetailsId')
                                ->leftJoin('business_register','business_register.user_id','users.id')
                                ->where('users.user_type',4)
                                ->select('haspatal_360_register.*','users.status as userStatus','business_register.b_name as business_name');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false])
            ->parameters([
                'dom'       => 'Bflrtip',
                'stateSave' => true,
                'order'     => [[0, 'desc']],
                'buttons'   => [
                    ['extend' => 'export', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'print', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'reset', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'reload', 'className' => 'btn btn-default btn-sm no-corner',],
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'name_prefix',
            'full_name',
            'email',
            'mobile_no',
            'business_name',
            'status'
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'haspatal_360_registersdatatable_' . time();
    }
}
