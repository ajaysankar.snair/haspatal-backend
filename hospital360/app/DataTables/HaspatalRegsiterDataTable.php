<?php

namespace App\DataTables;

use App\Models\HaspatalRegsiter;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

class HaspatalRegsiterDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
        ->addIndexColumn()
      
        ->addColumn('status',function($row){
                return view('status',['id'=>$row->id,'status' => $row->status])->render();
            }) 

        ->addColumn('action', 'haspatal_regsiters.datatables_actions')->rawColumns(['status', 'action']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\HaspatalRegsiter $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(HaspatalRegsiter $model)
    {
        return $model->newQuery()->join('users','haspatal_regsiter.id','users.userDetailsId')->where('users.user_type',5)->select('haspatal_regsiter.*','users.status');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false])
            ->parameters([
                'dom'       => 'Bflrtip',
                'stateSave' => true,
                'order'     => [[0, 'desc']],
                'buttons'   => [
                    ['extend' => 'create', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'export', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'print', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'reset', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'reload', 'className' => 'btn btn-default btn-sm no-corner',],
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'hospital_name',
            'address',
            'year_of_establishment',
            'gst_no',
            'contact_person',
            'status'
            // 'contact_person_email',
            // 'contact_person_phonoe_no',
            // 'other_phone_no',
            // 'support_watsapp_no'
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'haspatal_regsitersdatatable_' . time();
    }
}
