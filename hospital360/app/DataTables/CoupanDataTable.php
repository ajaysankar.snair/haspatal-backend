<?php

namespace App\DataTables;

use App\Models\Coupan;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

class CoupanDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        /*$dataTable = new EloquentDataTable($query);

        return $dataTable->addColumn('action', 'coupans.datatables_actions');*/

        $dataTable = new EloquentDataTable($query);

        return datatables($query)
                ->addIndexColumn()
              
                ->addColumn('amountType',function($row){
                        if ($row->amount_type == 1) {
                            return  "₹";
                        }else{
                            return  "%";
                        }  
                }) 
                ->addColumn('type',function($row){
                        if ($row->type == 1) {
                            return  "Patient";
                        }elseif ($row->type == 2){
                            return  "Doctor";
                        }elseif ($row->type == 11){
                            return  "Pharmacies";
                        }elseif ($row->type == 12){
                            return  "Diagnostics";
                        }elseif ($row->type == 13){
                            return  "Imaging";
                        }elseif ($row->type == 14){
                            return  "Homecare";
                        }elseif ($row->type == 16){
                            return  "Specialist";
                        }

                }) 
                ->addColumn('action', 'coupans.datatables_actions')
                ->rawColumns(['amountType', 'action','type']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Coupan $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Coupan $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false])
            ->parameters([
                'dom'       => 'Bflrtip',
                'stateSave' => true,
                'order'     => [[0, 'desc']],
                'buttons'   => [
                    ['extend' => 'create', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'export', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'print', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'reset', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'reload', 'className' => 'btn btn-default btn-sm no-corner',],
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'type',
            'promo_code',
            'used_promo_code_time',
            'amountType',
            'price',
            'sdate',
            'edate'
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'coupansdatatable_' . time();
    }
}
