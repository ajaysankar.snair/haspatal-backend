<?php

namespace App\DataTables;

use App\Models\TimeSlot;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Auth;
class TimeSlotDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable->addColumn('action', 'time_slots.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\TimeSlot $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(TimeSlot $model)
    {
        //return $model->newQuery();
        if(Auth::user()->role_id == 3)
        {
            return $model->join('doctor_details','doctor_details.id','time_slot.doctor_id')->select('time_slot.*','doctor_details.first_name as doctor_name')->where('time_slot.doctor_id',Auth::user()->userDetailsId);    
        }
        else
        {

            return $model->join('doctor_details','doctor_details.id','time_slot.doctor_id')->select('time_slot.*','doctor_details.first_name as doctor_name');
        }
        
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false])
            ->parameters([
                'dom'       => 'Bflrtip',
                'stateSave' => true,
                'order'     => [[0, 'desc']],
                'buttons'   => [
                    ['extend' => 'create', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'export', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'print', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'reset', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'reload', 'className' => 'btn btn-default btn-sm no-corner',],
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'doctor_name',
            'booking_time_slot',
            'mrg_from_time',
            'mrg_to_time',
            'aft_from_time',
            'aft_to_time',
            'eve_from_time',
            'eve_to_time',
            
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'time_slotsdatatable_' . time();
    }
}
