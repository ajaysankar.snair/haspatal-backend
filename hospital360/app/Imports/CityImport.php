<?php

namespace App\Imports;

use App\Models\City;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\ToModel;
use Auth;

class CityImport implements ToModel, WithHeadingRow
{
    private $data; 

    public function __construct(array $data = [])
    {
        $this->data = $data; 
    }
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new City([
           'city'     => $row['city'],
           'country_name'     => $this->data['country'],
           'state_name'     => $this->data['state'],
           'created_by'     => Auth::user()->id,
           'updated_by'     => Auth::user()->id,
          
        ]);
    }
}
