<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Illuminate\Support\Facades\Validator;

use App\User;
use App\Models\Patients;

class PatientImport implements ToCollection, WithHeadingRow
{
	use SkipsFailures;
    /**
    * @param Collection $collection
    */
    public function collection(Collection $rows)
    {
    	Validator::make($rows->toArray(), [
             '*.email' => 'required',
         ])->validate();

        foreach ($rows as $row) 
        {
            $p = Patients::create([
                'first_name' => $row['first_name'],
                'email' => $row['email'],
                'mobile' => $row['mobile_no'],
            ]);

            User::create([
                'first_name' => $row['first_name'],
                'uid' => 'p'.rand(1111,9999).$row['first_name'],
                'email' => $row['email'],
                'mobile' => $row['mobile_no'],
                'password' => Hash::make($row['password']),
                'status' => 1,
                'role_id' => 2,
                'user_type' => 2,
                'userDetailsId' => $p->id,
            ]);
        }
    }
}
