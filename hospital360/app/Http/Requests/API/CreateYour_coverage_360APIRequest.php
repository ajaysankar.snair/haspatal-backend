<?php

namespace App\Http\Requests\API;

use App\Models\Your_coverage_360;
use InfyOm\Generator\Request\APIRequest;

class CreateYour_coverage_360APIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return Your_coverage_360::$rules;
    }
}
