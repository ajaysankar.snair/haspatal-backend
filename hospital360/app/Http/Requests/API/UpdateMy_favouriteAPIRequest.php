<?php

namespace App\Http\Requests\API;

use App\Models\My_favourite;
use InfyOm\Generator\Request\APIRequest;

class UpdateMy_favouriteAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = My_favourite::$rules;
        
        return $rules;
    }
}
