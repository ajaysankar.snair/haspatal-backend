<?php

namespace App\Http\Requests\API;

use App\Models\Booking_price_admin;
use InfyOm\Generator\Request\APIRequest;

class UpdateBooking_price_adminAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = Booking_price_admin::$rules;
        
        return $rules;
    }
}
