<?php

namespace App\Http\Requests\API;

use App\Models\Patient_refund_request;
use InfyOm\Generator\Request\APIRequest;

class UpdatePatient_refund_requestAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = Patient_refund_request::$rules;
        
        return $rules;
    }
}
