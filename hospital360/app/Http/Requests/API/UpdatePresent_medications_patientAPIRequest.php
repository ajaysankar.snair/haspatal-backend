<?php

namespace App\Http\Requests\API;

use App\Models\Present_medications_patient;
use InfyOm\Generator\Request\APIRequest;

class UpdatePresent_medications_patientAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = Present_medications_patient::$rules;
        
        return $rules;
    }
}
