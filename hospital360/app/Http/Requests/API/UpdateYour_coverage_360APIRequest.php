<?php

namespace App\Http\Requests\API;

use App\Models\Your_coverage_360;
use InfyOm\Generator\Request\APIRequest;

class UpdateYour_coverage_360APIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = Your_coverage_360::$rules;
        
        return $rules;
    }
}
