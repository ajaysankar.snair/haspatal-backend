<?php

namespace App\Http\Requests\API;

use App\Models\Examination_notes_patient;
use InfyOm\Generator\Request\APIRequest;

class UpdateExamination_notes_patientAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = Examination_notes_patient::$rules;
        
        return $rules;
    }
}
