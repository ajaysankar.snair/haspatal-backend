<?php

namespace App\Http\Requests\API;

use App\Models\Patient_add_fund;
use InfyOm\Generator\Request\APIRequest;

class UpdatePatient_add_fundAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = Patient_add_fund::$rules;
        
        return $rules;
    }
}
