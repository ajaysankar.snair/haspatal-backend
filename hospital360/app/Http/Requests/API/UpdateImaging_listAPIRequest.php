<?php

namespace App\Http\Requests\API;

use App\Models\Imaging_list;
use InfyOm\Generator\Request\APIRequest;

class UpdateImaging_listAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = Imaging_list::$rules;
        
        return $rules;
    }
}
