<?php

namespace App\Http\Requests\API;

use App\Models\Suggested_investigations_patient;
use InfyOm\Generator\Request\APIRequest;

class UpdateSuggested_investigations_patientAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = Suggested_investigations_patient::$rules;
        
        return $rules;
    }
}
