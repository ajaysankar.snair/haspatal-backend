<?php

namespace App\Http\Controllers;

use App\DataTables\Hospital_registerDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateHospital_registerRequest;
use App\Http\Requests\UpdateHospital_registerRequest;
use App\Repositories\Hospital_registerRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use App\User;
use Response;
use Auth;
use DB;
use Validator;
use Illuminate\Http\Request;

class Hospital_registerController extends AppBaseController
{
    /** @var  Hospital_registerRepository */
    private $hospitalRegisterRepository;

    public function __construct(Hospital_registerRepository $hospitalRegisterRepo)
    {
        $this->hospitalRegisterRepository = $hospitalRegisterRepo;
    }

    /**
     * Display a listing of the Hospital_register.
     *
     * @param Hospital_registerDataTable $hospitalRegisterDataTable
     * @return Response
     */
    public function index(Hospital_registerDataTable $hospitalRegisterDataTable)
    {
        return $hospitalRegisterDataTable->render('hospital_registers.index');
    }

    /**
     * Show the form for creating a new Hospital_register.
     *
     * @return Response
     */
    public function create()
    {
        $countryList = DB::table('country')->pluck('country','id');
        $stateList = DB::table('state')->pluck('state','id');
        $cityList = DB::table('city')->pluck('city','id');
        return view('hospital_registers.create',compact('countryList','stateList','cityList'));
    }

    /**
     * Store a newly created Hospital_register in storage.
     *
     * @param CreateHospital_registerRequest $request
     *
     * @return Response
     */
    public function store(CreateHospital_registerRequest $request)
    {
        $input = $request->all();

        if($request->hasfile('logo'))
        {
            $image = $request->file('logo');
            $extension = $image->getClientOriginalExtension(); // getting image extension
            $filename =time().'__'.$image->getClientOriginalName();
            $path = public_path('/media/hospital_logo/');
            $image->move($path, $filename);
            $input['logo'] = $filename;
        }

        $input['created_by'] = Auth::user()->id;
        $input['updated_by'] = Auth::user()->id;
        $hospitalRegister = $this->hospitalRegisterRepository->create($input);
        $user = User::create(['first_name' => $request['hospital_name'],
                                  'last_name' => $request['hospital_name'],
                                  'email' => $request['email'],
                                  'role_id' => $request['role_id'],
                                  'user_type' => 3,
                                  'userDetailsId'=>$hospitalRegister->id,
                                  'password' => bcrypt($request['password']),
                                  ]);
            $user->attachRole(15);
        Flash::success('Hospital Register saved successfully.');

        return redirect(route('hospitalRegisters.index'));
    }

    /**
     * Display the specified Hospital_register.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $hospitalRegister = $this->hospitalRegisterRepository->find($id);

        if (empty($hospitalRegister)) {
            Flash::error('Hospital Register not found');

            return redirect(route('hospitalRegisters.index'));
        }

        return view('hospital_registers.show')->with('hospitalRegister', $hospitalRegister);
    }

    /**
     * Show the form for editing the specified Hospital_register.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $hospitalRegister = $this->hospitalRegisterRepository->find($id);
        $countryList = DB::table('country')->pluck('country','id');
        $stateList = DB::table('state')->pluck('state','id');
        $cityList = DB::table('city')->pluck('city','id');
        if (empty($hospitalRegister)) {
            Flash::error('Hospital Register not found');

            return redirect(route('hospitalRegisters.index'));
        }

        return view('hospital_registers.edit',compact('countryList','stateList','cityList'))->with('hospitalRegister', $hospitalRegister);
    }

    /**
     * Update the specified Hospital_register in storage.
     *
     * @param  int              $id
     * @param UpdateHospital_registerRequest $request
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $hospitalRegister = $this->hospitalRegisterRepository->find($id);
        $input = $request->all();
        if($request->hasfile('logo'))
        {
            $image = $request->file('logo');
            $extension = $image->getClientOriginalExtension(); // getting image extension
            $filename =time().'__'.$image->getClientOriginalName();
            $path = public_path('/media/hospital_logo/');
            $image->move($path, $filename);
            $input['logo'] = $filename;
          }else{
            $input['logo']=$hospitalRegister->logo;
        }
        


        if (empty($hospitalRegister)) {
            Flash::error('Hospital Register not found');

            return redirect(route('hospitalRegisters.index'));
        }

        $hospitalRegister = $this->hospitalRegisterRepository->update($input, $id);

         if(Auth::user()->role_id == 1){
           Flash::success('Hospital Register updated successfully.');

            return redirect(route('hospitalRegisters.index'));
        }else{
            Flash::success('Hospital updated successfully.');
            return back();
        }


        
    }

    /**
     * Remove the specified Hospital_register from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $hospitalRegister = $this->hospitalRegisterRepository->find($id);

        if (empty($hospitalRegister)) {
            Flash::error('Hospital Register not found');

            return redirect(route('hospitalRegisters.index'));
        }

        $this->hospitalRegisterRepository->delete($id);

        Flash::success('Hospital Register deleted successfully.');

        return redirect(route('hospitalRegisters.index'));
    }
}
