<?php

namespace App\Http\Controllers;

use App\Http\Requests\API\CreateHaspatal_360_registerAPIRequest;
use App\Http\Requests\API\UpdateHaspatal_360_registerAPIRequest;
use App\Models\Haspatal_360_register;
use App\Repositories\Haspatal_360_registerRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;
use App\Models\City;
use DB;
use Auth;
use App\User;
use Mail;
use Validator;
/**
 * Class Haspatal_360_registerController
 * @package App\Http\Controllers\API
 */


class RegistrationController extends Controller
{
    //

     /** @var  Haspatal_360_registerRepository */
     private $haspatal360RegisterRepository;

     public function __construct(Haspatal_360_registerRepository $haspatal360RegisterRepo)
     {
         $this->haspatal360RegisterRepository = $haspatal360RegisterRepo;
     }
 
     /**
      * @param Request $request
      * @return Response
      *
      * @SWG\Get(
      *      path="/haspatal360Registers",
      *      summary="Get a listing of the Haspatal_360_registers.",
      *      tags={"Haspatal_360_register"},
      *      description="Get all Haspatal_360_registers",
      *      produces={"application/json"},
      *      @SWG\Response(
      *          response=200,
      *          description="successful operation",
      *          @SWG\Schema(
      *              type="object",
      *              @SWG\Property(
      *                  property="success",
      *                  type="boolean"
      *              ),
      *              @SWG\Property(
      *                  property="data",
      *                  type="array",
      *                  @SWG\Items(ref="#/definitions/Haspatal_360_register")
      *              ),
      *              @SWG\Property(
      *                  property="message",
      *                  type="string"
      *              )
      *          )
      *      )
      * )
      */
     public function index(Request $request)
     {
         $haspatal360Registers = $this->haspatal360RegisterRepository->all(
             $request->except(['skip', 'limit']),
             $request->get('skip'),
             $request->get('limit')
         );
 
         return $this->sendResponse($haspatal360Registers->toArray(), 'Haspatal 360 Registers retrieved successfully');
     }
 
     /**
      * @param CreateHaspatal_360_registerAPIRequest $request
      * @return Response
      *
      * @SWG\Post(
      *      path="/haspatal360Registers",
      *      summary="Store a newly created Haspatal_360_register in storage",
      *      tags={"Haspatal_360_register"},
      *      description="Store Haspatal_360_register",
      *      produces={"application/json"},
      *      @SWG\Parameter(
      *          name="body",
      *          in="body",
      *          description="Haspatal_360_register that should be stored",
      *          required=false,
      *          @SWG\Schema(ref="#/definitions/Haspatal_360_register")
      *      ),
      *      @SWG\Response(
      *          response=200,
      *          description="successful operation",
      *          @SWG\Schema(
      *              type="object",
      *              @SWG\Property(
      *                  property="success",
      *                  type="boolean"
      *              ),
      *              @SWG\Property(
      *                  property="data",
      *                  ref="#/definitions/Haspatal_360_register"
      *              ),
      *              @SWG\Property(
      *                  property="message",
      *                  type="string"
      *              )
      *          )
      *      )
      * )
      */
 
     public function haspatal_360_otp(Request $request)
     {
        //  $validator = Validator::make($request->all(), [
        //        'mobile_no' => 'required|unique:haspatal_360_register,mobile_no',
        //  ]);
        //  if ($validator->fails()) {
        //    return response()->json(['error'=>$validator->errors()]);
        //  }
        $otp="55";
        $message="Dear Customer, Your OTP for Registering Haspatal App is ".$otp."
        Thanks for registering.
        Regards
        Haspatal Team ";
         $curl = curl_init();
         curl_setopt_array($curl, array(
             CURLOPT_URL => "http://sms.bulksmsind.in/sendSMS?username=haspatal&message=".urlencode(  $message)."&sendername=MASUPP&smstype=TRANS&numbers=".$request['mobile_no']."&apikey=206f20fc-d4d5-4a33-ad06-10f453b8ab4a",
             CURLOPT_RETURNTRANSFER => true,
             CURLOPT_ENCODING => true,
             CURLOPT_MAXREDIRS => 10,
             CURLOPT_TIMEOUT => 30,
             CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
             CURLOPT_CUSTOMREQUEST => "GET",
         ));
         $response = curl_exec($curl);
         return  $response;
         $err = curl_error($curl);
         curl_close($curl);
         if ($err) {
             //echo "cURL Error #:" . $err;
             return response()->json(["status" => true,"message" => "OTP Not Send","data"=>$err]);
         } else {
             //echo $response;exit();
             return response()->json(["status" => true,"message" => "Send OTP in your mobile no"]);
         }
     }
 
     public function store(Request $request)
     {
         $data = $request->input();
         $email = User::where('email', '=', $data['email'])->first();
         if($email){
            return response()->json(["status" => 1,"massage" => "Email Already Exit"]);  
         }
         $mobile = User::where('mobile', '=',  $data['mo_prefix'].'-'.$data['mobile_no'])->first();

         if($mobile){
            return response()->json(["status" => 1,"massage" => "Mobile Number Already Exit"]);  
         }
         $emailmobile = User::where('email', '=', $data['email'])->where('mobile', '=',  $data['mo_prefix'].'-'.$data['mobile_no'])->first();
          if($emailmobile){
             return response()->json(["status" => 1,"massage" => "User Already Registered"]);  
          }
        
 
         $input = $request->all();
         $h360_data =  DB::table('haspatal_360_register')->orderBy('id', 'desc')->first();
 
         if($h360_data->v_uni_id != ''){
           $bookdata = explode('-', $h360_data->v_uni_id); 
           $dd = $bookdata['2'] + 1;   
           $a = str_pad($dd, 6, '0', STR_PAD_LEFT);
           $input['v_uni_id'] = $bookdata['0'].'-'.$bookdata['1'].'-'.$a;
         }else{
             $seq = '1';
             $a = str_pad($seq, 6, '0', STR_PAD_LEFT); 
             $input['v_uni_id'] = '91-V-'.$a;
         }
         $haspatal360Register = $this->haspatal360RegisterRepository->create($input);
         $user = User::create(['first_name' => $request['full_name'],
                                 'email' => $request['email'],
                                 'mobile' => $request['mo_prefix'].'-'.$request['mobile_no'],
                                 'mobile2' => $request['mo_prefix'].'-'.$request['mobile2'],
                                 'reg_status' => $request['reg_status'],
                                 'role_id' => $request['role_id'],
                                 'user_type' => 4,
                                 'userDetailsId'=>$haspatal360Register->id,
                                 //'uid'=> $result->data->uid,
                                 'password' => bcrypt($request['password']),
                               ]);
        //  $user->attachRole($request['role_id']);
        //  $user['mobile2']=$request['mo_prefix'].'-'.$request['mobile2'];
        //  $user->reg_status=$request['reg_status'];
 
        $data = User::where('id', $user->id)
        ->update(['reg_status' => '11']);
 
         if ($haspatal360Register) {
            // $data = User::where('id', $user->id)
            // ->update(['reg_status' => '11']);
             return response()->json(["status" => true,"massage"=>"Haspatal 360 Register saved successfully","data" => $haspatal360Register,"user" => $user]);
         }else{
             return response()->json(["status" => false,"data" => $haspatal360Register]);
         }
 
         //return $this->sendResponse($haspatal360Register->toArray(), 'Haspatal 360 Register saved successfully');
     }
 
     /**
      * @param int $id
      * @return Response
      *
      * @SWG\Get(
      *      path="/haspatal360Registers/{id}",
      *      summary="Display the specified Haspatal_360_register",
      *      tags={"Haspatal_360_register"},
      *      description="Get Haspatal_360_register",
      *      produces={"application/json"},
      *      @SWG\Parameter(
      *          name="id",
      *          description="id of Haspatal_360_register",
      *          type="integer",
      *          required=true,
      *          in="path"
      *      ),
      *      @SWG\Response(
      *          response=200,
      *          description="successful operation",
      *          @SWG\Schema(
      *              type="object",
      *              @SWG\Property(
      *                  property="success",
      *                  type="boolean"
      *              ),
      *              @SWG\Property(
      *                  property="data",
      *                  ref="#/definitions/Haspatal_360_register"
      *              ),
      *              @SWG\Property(
      *                  property="message",
      *                  type="string"
      *              )
      *          )
      *      )
      * )
      */
     public function show($id)
     {
         /** @var Haspatal_360_register $haspatal360Register */
         $haspatal360Register = $this->haspatal360RegisterRepository->find($id);
 
         if (empty($haspatal360Register)) {
             return $this->sendError('Haspatal 360 Register not found');
         }
 
         return $this->sendResponse($haspatal360Register->toArray(), 'Haspatal 360 Register retrieved successfully');
     }
 
     /**
      * @param int $id
      * @param UpdateHaspatal_360_registerAPIRequest $request
      * @return Response
      *
      * @SWG\Put(
      *      path="/haspatal360Registers/{id}",
      *      summary="Update the specified Haspatal_360_register in storage",
      *      tags={"Haspatal_360_register"},
      *      description="Update Haspatal_360_register",
      *      produces={"application/json"},
      *      @SWG\Parameter(
      *          name="id",
      *          description="id of Haspatal_360_register",
      *          type="integer",
      *          required=true,
      *          in="path"
      *      ),
      *      @SWG\Parameter(
      *          name="body",
      *          in="body",
      *          description="Haspatal_360_register that should be updated",
      *          required=false,
      *          @SWG\Schema(ref="#/definitions/Haspatal_360_register")
      *      ),
      *      @SWG\Response(
      *          response=200,
      *          description="successful operation",
      *          @SWG\Schema(
      *              type="object",
      *              @SWG\Property(
      *                  property="success",
      *                  type="boolean"
      *              ),
      *              @SWG\Property(
      *                  property="data",
      *                  ref="#/definitions/Haspatal_360_register"
      *              ),
      *              @SWG\Property(
      *                  property="message",
      *                  type="string"
      *              )
      *          )
      *      )
      * )
      */
     public function update($id, UpdateHaspatal_360_registerAPIRequest $request)
     {
         $input = $request->all();
 
         /** @var Haspatal_360_register $haspatal360Register */
         $haspatal360Register = $this->haspatal360RegisterRepository->find($id);
 
         if (empty($haspatal360Register)) {
             return $this->sendError('Haspatal 360 Register not found');
         }
 
         $haspatal360Register = $this->haspatal360RegisterRepository->update($input, $id);
 
         return $this->sendResponse($haspatal360Register->toArray(), 'Haspatal_360_register updated successfully');
     }
 
     /**
      * @param int $id
      * @return Response
      *
      * @SWG\Delete(
      *      path="/haspatal360Registers/{id}",
      *      summary="Remove the specified Haspatal_360_register from storage",
      *      tags={"Haspatal_360_register"},
      *      description="Delete Haspatal_360_register",
      *      produces={"application/json"},
      *      @SWG\Parameter(
      *          name="id",
      *          description="id of Haspatal_360_register",
      *          type="integer",
      *          required=true,
      *          in="path"
      *      ),
      *      @SWG\Response(
      *          response=200,
      *          description="successful operation",
      *          @SWG\Schema(
      *              type="object",
      *              @SWG\Property(
      *                  property="success",
      *                  type="boolean"
      *              ),
      *              @SWG\Property(
      *                  property="data",
      *                  type="string"
      *              ),
      *              @SWG\Property(
      *                  property="message",
      *                  type="string"
      *              )
      *          )
      *      )
      * )
      */
     public function destroy($id)
     {
         /** @var Haspatal_360_register $haspatal360Register */
         $haspatal360Register = $this->haspatal360RegisterRepository->find($id);
 
         if (empty($haspatal360Register)) {
             return $this->sendError('Haspatal 360 Register not found');
         }
 
         $haspatal360Register->delete();
 
         return $this->sendSuccess('Haspatal 360 Register deleted successfully');
     }
       public function city_list(Request $request)
    {
        $state_id=  $request->input('state_id');
        if ($state_id == 0) {
            $result = City::orderBy('city','ASC')->get();
        }else{

            $result = City::where('state_name', $state_id)->orderBy('city','ASC')->get();
        }
  
       if ($result) {
            return response()->json(["status" => true,"massage"=>"City retrieved successfully","data" => $result]);
        }else{
            return response()->json(["status" => false,"data" => $result]);
        }

    }
    public function login(Request $request){

        $countall = DB::table('users')->where('email',$request->email)->count();
  
         if($countall>0)
  {
        if(Auth::attempt(['email' => $request->email,'password'=> $request->password]))
        {
      if(Auth::attempt(['email' => $request->email, 'password' => $request->password]))
      {
  
        $result = DB::table('users')->where('email',$request->email)->update(['device' => $request->device]);
        $user = Auth::user();
        
  
        //echo "<pre>";print_r($user);exit();
        $userData = DB::table('users')->where('id',$user->id)->first();
        if($userData->reg_status=="0"){
        $userInformation = DB::table('patient_details')
                            ->leftJoin('country','country.id','patient_details.country')
                            ->leftJoin('state','state.id','patient_details.state')
                            ->leftJoin('city','city.id','patient_details.city')
                            ->where('patient_details.id',$user->userDetailsId)
                            ->select('patient_details.*','country.country as country_name','state.state  as state_name','city.city as city_name')
                            ->first();
        $success['token'] =  $user->createToken('LaraPassport')->accessToken;
        $success['user_data'] =  $userData;
        $business_register =  DB::table('business_register')->where('user_id', $user->id)->orderBy('id', 'desc')->first();
        $admin_status=$business_register->admin_status;
        $terms_status=$business_register->terms_status;
        $success['admin_status']=$admin_status;
        $success['terms_status']=$terms_status;
        $success['user_information'] =  $userInformation;
  
  
  
        return response()->json([
          //'status' => '4',
          'status'=>'100',
          'data' => $success
        ]);
    }else{
        $success['user_data'] =  $userData;
        return response()->json([
            //'status' => '4',
            'status'=>'100',
            'data' => $success
          ]); 
    }



      } else {
        return response()->json([
          'status' => '3',
          'data' => 'User Pending'
        ]);
      }
    }
    else
      {
        return response()->json([
          'status' => '2',
          'data' => 'Invalid ID or password'
        ]);
      }
  
    }
  
    else
    {
                return response()->json([
          'status' => '1',
          'data' => 'User not registered'
        ]);
    }
  
    }
    public function uploadfile(Request $request)
    {
        if($request->hasfile('b_lic'))
        {
            $image1 = $request->file('b_lic');
            $extension1 = $image1->getClientOriginalExtension(); // getting image extension
            $filename1 =time().'__'.$image1->getClientOriginalName();
            $path1 = public_path('/media/b_lic/');
            $image1->move($path1, $filename1);
           
        }
        if($request->hasfile('shop_pic'))
        {
            $image1 = $request->file('shop_pic');
            $extension1 = $image1->getClientOriginalExtension(); // getting image extension
            $filename1 =time().'__'.$image1->getClientOriginalName();
            $path1 = public_path('/media/shop_pic/');
            $image1->move($path1, $filename1);
           
        }
        if($request->hasfile('b_card_pic'))
        {
            $image1 = $request->file('b_card_pic');
            $extension1 = $image1->getClientOriginalExtension(); // getting image extension
            $filename1 =time().'__'.$image1->getClientOriginalName();
            $path1 = public_path('/media/b_card_pic/');
            $image1->move($path1, $filename1);
           
        }
        if($request->hasfile('dr_pic'))
        {
            $image1 = $request->file('dr_pic');
            $extension1 = $image1->getClientOriginalExtension(); // getting image extension
            $filename1 =time().'__'.$image1->getClientOriginalName();
            $path1 = public_path('/media/profile_pic/');
            $image1->move($path1, $filename1);
           
        }  
        if($request->hasfile('clinic_logo'))
        {
            $image1 = $request->file('clinic_logo');
            $extension1 = $image1->getClientOriginalExtension(); // getting image extension
            $filename1 =time().'__'.$image1->getClientOriginalName();
            $path1 = public_path('/media/clinic_logo/');
            $image1->move($path1, $filename1);
           
        }  
        if($request->hasfile('clinic_wallpaper'))
        {
            $image1 = $request->file('clinic_wallpaper');
            $extension1 = $image1->getClientOriginalExtension(); // getting image extension
            $filename1 =time().'__'.$image1->getClientOriginalName();
            $path1 = public_path('/media/clinic_wallpapper/');
            $image1->move($path1, $filename1);
           
        }  
        if($request->hasfile('license_pic'))
        {
            $image1 = $request->file('license_pic');
            $extension1 = $image1->getClientOriginalExtension(); // getting image extension
            $filename1 =time().'__'.$image1->getClientOriginalName();
            $path1 = public_path('/media/licence_copy/');
            $image1->move($path1, $filename1);
           
        } 
     return response()->json( $filename1,200);
    }
    public function getreedemCoupon(Request $request)
    {
        
        $users =  DB::table('users')->where('id', $request->input('user_id'))->orderBy('id', 'desc')->first();
        
       
       
        $role_id=$users->role_id;
        $role =  DB::table('couponrates')->where('category_id', $role_id)->orderBy('id', 'desc')->first();
        // dd($role);
        
     
  
      
       
        return response()->json( $role,200);

    }  
    public function district(Request $request)
    {
        
        $users =  DB::table('state')->where('id', $state_id=  $request->input('state_id'))->orderBy('id', 'desc')->first();
        
        
      
        // dd($role);
        
     $district=json_decode($users->district);
  
      
       
        return response()->json( $district->districts,200);

    } 
    public function updatePassword(Request $request)
    {
        $data = $request->input();
        
  
     
    /*  if ($request->new_password == '' || $request->con_password == '') {
        echo "string"; exit;
        unset($request->new_password);
        unset($request->con_password);
  
      } else*/
       if ($request->new_password != '' && $request->confirm_password != '') {
        //echo "string1111"; exit;
        $data['password'] = bcrypt($request->new_password);
        $user =  User::where('id', '=', $request->input('user_id'))->first();
        $user->update($data);
        $user->save();
      }
  
  
      $response_data = array();
      $response_data['status'] = true;
      $response_data['message'] = 'User details was successfully updated.';
     
      return response()->json($response_data);
  
    }
    public function HaspatalnewOTP(Request $request)
    {
        $otp = mt_rand(1000, 9999);
        $user =  User::where('id', '=', $request->input('user_id'))->first();
       
        $id=  $user->id;
       
        // $content = array('name'=>"Virat Gandhi");
     
        $updateOTP = User::where('id', $user->id)
        ->update(['OTP' =>  $otp]);
        $content = array('otp'=>$otp);
        $mobile=$request->input('mobile_no');
        $mobileResponse = $this->sendotomobile($otp,$mobile);
        $emailResponse = $this->sendotomail($id,$content);
        $emailResponsethird = $this->send_mail1( $id,$otp);
       
       

        
        return response()->json([
            //'status' => '4',
            'OTPEmail'=>$emailResponse,
            'otpmobile' =>$mobileResponse,
            'emailresponsethird' =>$emailResponsethird
          ]); 
    }
    public function sendotomail($id,$content)
    {
        $user = User::findOrFail($id);
    $sendmail=  Mail::send('mail', $content, function ($m) use ($user) {
            $m->from('system@haspatal.com', 'Haspatal');

            $m->to($user->email, $user->email)->subject('OTP From Haspatal!');
        });
        return "Otp Sended Through Mail";
    }
    public function sendotomobile($otp,$mobile)
    {
        // $otp="55";
        $message="Dear Customer, Your OTP for Registering Haspatal App is ".$otp."
        Thanks for registering.
        Regards
        Haspatal Team ";
         $curl = curl_init();
         curl_setopt_array($curl, array(
             CURLOPT_URL => "http://sms.bulksmsind.in/sendSMS?username=haspatal&message=".urlencode(  $message)."&sendername=MASUPP&smstype=TRANS&numbers=".$mobile."&apikey=206f20fc-d4d5-4a33-ad06-10f453b8ab4a",
             CURLOPT_RETURNTRANSFER => true,
             CURLOPT_ENCODING => true,
             CURLOPT_MAXREDIRS => 10,
             CURLOPT_TIMEOUT => 30,
             CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
             CURLOPT_CUSTOMREQUEST => "GET",
         ));
         $response = curl_exec($curl);
        //  return  $response;
         $err = curl_error($curl);
         curl_close($curl);
         if ($err) {
             //echo "cURL Error #:" . $err;
             return response()->json(["status" => true,"message" => "OTP Not Send","data"=>$err]);
         } else {
             //echo $response;exit();
             return response()->json(["status" => true,"message" => "Send OTP in your mobile no"]);
         }
    }
    public function hasptalverifiedOTP(Request $request)
    {
        $data = $request->input();
        $verifyOTP = User::where('id', '=', $data['user_id'])->where('OTP', '=', $data['OTP'])->first();
        if( $verifyOTP){
            $updateOTP = User::where('id', $verifyOTP->id)
        ->update(['reg_status' =>  "17"]);
            return response()->json(["status" => '1', "message" => "Otp Verified"]);
        }else{
        return response()->json(["status" => '0', "message" => "Invalid OTP"]);
        }
    }
    public function send_mail1($id,$otp)
    
    {
        $user = User::findOrFail($id);
        $mailmessage= "please verify your email id with the OTP :". $otp;
        $data['personalizations'] = array("0" => array('to' => array('0' => array('email' => $user->email))));

        $data['from'] = array('email' => 'system@haspatal.com');

        $data['subject'] = "Haspatal 
        Registration ";
        $data['content'] = array("0" =>array('type' =>' text/html','value' => "<DOCTYPE html><html lang='en-US'>     <head><meta charset='utf-8'></head><body><h2>  ".$mailmessage."</body></html>" ));

        //echo json_encode($data);exit();

        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://api.sendgrid.com/v3/mail/send",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => json_encode($data),
          CURLOPT_HTTPHEADER => array(
            "authorization: Bearer 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",
            "cache-control: no-cache",
            "content-type: application/json",
            "postman-token: b8c0781f-af48-90cc-1c5b-76f80e0b4be0"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            return "Send OTP in your emailId";
        }
    }


    public function loginAcess3600(Request $request){

        $countall = DB::table('users')->where('email',$request->email)->count();
  
         if($countall>0)
  {
        if(Auth::attempt(['email' => $request->email,'password'=> $request->password]))
        {
      if(Auth::attempt(['email' => $request->email, 'password' => $request->password]))
      {
  
        $result = DB::table('users')->where('email',$request->email)->update(['device' => $request->device]);
        $user = Auth::user();
        
  
        //echo "<pre>";print_r($user);exit();
        $userData = DB::table('users')->where('id',$user->id)->first();
        if($userData->reg_status=="0"){
        $userInformation = DB::table('patient_details')
                            ->leftJoin('country','country.id','patient_details.country')
                            ->leftJoin('state','state.id','patient_details.state')
                            ->leftJoin('city','city.id','patient_details.city')
                            ->where('patient_details.id',$user->userDetailsId)
                            ->select('patient_details.*','country.country as country_name','state.state  as state_name','city.city as city_name')
                            ->first();
        $success['token'] =  $user->createToken('LaraPassport')->accessToken;
        $success['user_data'] =  $userData;
        $business_register =  DB::table('business_register')->where('user_id', $user->id)->orderBy('id', 'desc')->first();
        $admin_status=$business_register->admin_status;
        $terms_status=$business_register->terms_status;
        $success['admin_status']=$admin_status;
        $success['terms_status']=$terms_status;
        $success['user_information'] =  $userInformation;
        $success['business_register'] =  $business_register;
  
  
  
        return response()->json([
          //'status' => '4',
          'status'=>'100',
          'data' => $success
        ]);
    }else{
        $success['user_data'] =  $userData;
        return response()->json([
            //'status' => '4',
            'status'=>'100',
            'data' => $success
          ]); 
    }



      } else {
        return response()->json([
          'status' => '3',
          'data' => 'User Pending'
        ]);
      }
    }
    else
      {
        return response()->json([
          'status' => '2',
          'data' => 'Invalid ID or password'
        ]);
      }
  
    }
  
    else
    {
                return response()->json([
          'status' => '1',
          'data' => 'User not registered'
        ]);
    }
  
    }
    
}

