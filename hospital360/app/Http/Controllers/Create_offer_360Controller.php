<?php

namespace App\Http\Controllers;

use App\DataTables\Create_offer_360DataTable;
use App\Http\Requests;
use App\Http\Requests\CreateCreate_offer_360Request;
use App\Http\Requests\UpdateCreate_offer_360Request;
use App\Repositories\Create_offer_360Repository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Auth;
use DB;

class Create_offer_360Controller extends AppBaseController
{
    /** @var  Create_offer_360Repository */
    private $createOffer360Repository;

    public function __construct(Create_offer_360Repository $createOffer360Repo)
    {
        $this->createOffer360Repository = $createOffer360Repo;
    }

    /**
     * Display a listing of the Create_offer_360.
     *
     * @param Create_offer_360DataTable $createOffer360DataTable
     * @return Response
     */
    public function index(Create_offer_360DataTable $createOffer360DataTable)
    {
        return $createOffer360DataTable->render('create_offer_360s.index');
    }

    /**
     * Show the form for creating a new Create_offer_360.
     *
     * @return Response
     */
    public function create()
    {
        return view('create_offer_360s.create');
    }

    /**
     * Store a newly created Create_offer_360 in storage.
     *
     * @param CreateCreate_offer_360Request $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $tags = $request->input('offer_id');
        $tags1 = implode(',', $tags);
        $input['offer_id'] = $tags1;

        $tags1 = implode(',', $tags);
        $input['created_by'] = Auth::user()->id;
        $input['updated_by'] = Auth::user()->id;
        $createOffer360 = $this->createOffer360Repository->create($input);

        Flash::success('Create Offer 360 saved successfully.');

        return redirect(route('createOffer360s.index'));
    }

    /**
     * Display the specified Create_offer_360.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $createOffer360 = $this->createOffer360Repository->find($id);

        if (empty($createOffer360)) {
            Flash::error('Create Offer 360 not found');

            return redirect(route('createOffer360s.index'));
        }

        return view('create_offer_360s.show')->with('createOffer360', $createOffer360);
    }

    /**
     * Show the form for editing the specified Create_offer_360.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $createOffer360 = $this->createOffer360Repository->find($id);

        if (empty($createOffer360)) {
            Flash::error('Create Offer 360 not found');

            return redirect(route('createOffer360s.index'));
        }

        return view('create_offer_360s.edit')->with('createOffer360', $createOffer360);
    }

    /**
     * Update the specified Create_offer_360 in storage.
     *
     * @param  int              $id
     * @param UpdateCreate_offer_360Request $request
     *
     * @return Response
     */
    public function update($id, UpdateCreate_offer_360Request $request)
    {
        $createOffer360 = $this->createOffer360Repository->find($id);

        if (empty($createOffer360)) {
            Flash::error('Create Offer 360 not found');

            return redirect(route('createOffer360s.index'));
        }

        $createOffer360 = $this->createOffer360Repository->update($request->all(), $id);

        Flash::success('Create Offer 360 updated successfully.');

        return redirect(route('createOffer360s.index'));
    }

    /**
     * Remove the specified Create_offer_360 from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $createOffer360 = $this->createOffer360Repository->find($id);

        if (empty($createOffer360)) {
            Flash::error('Create Offer 360 not found');

            return redirect(route('createOffer360s.index'));
        }

        $this->createOffer360Repository->delete($id);

        Flash::success('Create Offer 360 deleted successfully.');

        return redirect(route('createOffer360s.index'));
    }
}
