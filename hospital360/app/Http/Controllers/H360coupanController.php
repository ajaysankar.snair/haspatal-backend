<?php

namespace App\Http\Controllers;

use App\DataTables\H360coupanDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateH360coupanRequest;
use App\Http\Requests\UpdateH360coupanRequest;
use App\Repositories\H360coupanRepository;
use App\Http\Controllers\AppBaseController;
use Flash;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Illuminate\View\View;
use Response;

class H360coupanController extends AppBaseController
{
    /** @var  H360coupanRepository */
    private $h360coupanRepository;

    public function __construct(H360coupanRepository $h360coupanRepo)
    {
        $this->h360coupanRepository = $h360coupanRepo;
    }

    /**
     * Display a listing of the H360coupan.
     *
     * @param H360coupanDataTable $h360coupanDataTable
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function index(H360coupanDataTable $h360coupanDataTable)
    {
        return $h360coupanDataTable->render('h360coupans.index');
    }

    /**
     * Show the form for creating a new H360coupan.
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function create()
    {
        return view('h360coupans.create');
    }

    /**
     * Store a newly created H360coupan in storage.
     *
     * @param CreateH360coupanRequest $request
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function store(CreateH360coupanRequest $request)
    {
        $input = $request->all();

        $h360coupan = $this->h360coupanRepository->create($input);

        Flash::success('H360Coupan saved successfully.');

        return redirect(route('h360coupans.index'));
    }

    /**
     * Display the specified H360coupan.
     *
     * @param  int $id
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function show($id)
    {
        $h360coupan = $this->h360coupanRepository->find($id);

        if (empty($h360coupan)) {
            Flash::error('H360Coupan not found');

            return redirect(route('h360coupans.index'));
        }

        return view('h360coupans.show')->with('h360coupan', $h360coupan);
    }

    /**
     * Show the form for editing the specified H360coupan.
     *
     * @param  int $id
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function edit($id)
    {
        $h360coupan = $this->h360coupanRepository->find($id);

        if (empty($h360coupan)) {
            Flash::error('H360Coupan not found');

            return redirect(route('h360coupans.index'));
        }

        return view('h360coupans.edit')->with('h360coupan', $h360coupan);
    }

    /**
     * Update the specified H360coupan in storage.
     *
     * @param  int              $id
     * @param UpdateH360coupanRequest $request
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function update($id, UpdateH360coupanRequest $request)
    {
        $h360coupan = $this->h360coupanRepository->find($id);

        if (empty($h360coupan)) {
            Flash::error('H360Coupan not found');

            return redirect(route('h360coupans.index'));
        }

        $h360coupan = $this->h360coupanRepository->update($request->all(), $id);

        Flash::success('H360Coupan updated successfully.');

        return redirect(route('h360coupans.index'));
    }

    /**
     * Remove the specified H360coupan from storage.
     *
     * @param  int $id
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function destroy($id)
    {
        $h360coupan = $this->h360coupanRepository->find($id);

        if (empty($h360coupan)) {
            Flash::error('H360Coupan not found');

            return redirect(route('h360coupans.index'));
        }

        $this->h360coupanRepository->delete($id);

        Flash::success('H360Coupan deleted successfully.');

        return redirect(route('h360coupans.index'));
    }
}
