<?php

namespace App\Http\Controllers;

use App\DataTables\TaxSetupDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateTaxSetupRequest;
use App\Http\Requests\UpdateTaxSetupRequest;
use App\Repositories\TaxSetupRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Auth;
use DB;
class TaxSetupController extends AppBaseController
{
    /** @var  TaxSetupRepository */
    private $taxSetupRepository;

    public function __construct(TaxSetupRepository $taxSetupRepo)
    {
        $this->taxSetupRepository = $taxSetupRepo;
    }

    /**
     * Display a listing of the TaxSetup.
     *
     * @param TaxSetupDataTable $taxSetupDataTable
     * @return Response
     */
    public function index(TaxSetupDataTable $taxSetupDataTable)
    {
        return $taxSetupDataTable->render('tax_setups.index');
    }

    /**
     * Show the form for creating a new TaxSetup.
     *
     * @return Response
     */
    public function create()
    {
        return view('tax_setups.create');
    }

    /**
     * Store a newly created TaxSetup in storage.
     *
     * @param CreateTaxSetupRequest $request
     *
     * @return Response
     */
    public function store(CreateTaxSetupRequest $request)
    {
        $input = $request->all();
        $input['created_by'] = Auth::user()->id;
        $input['updated_by'] = Auth::user()->id;
        $taxSetup = $this->taxSetupRepository->create($input);

        Flash::success('Tax Setup saved successfully.');

        return redirect(route('taxSetups.index'));
    }

    /**
     * Display the specified TaxSetup.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $taxSetup = $this->taxSetupRepository->find($id);

        if (empty($taxSetup)) {
            Flash::error('Tax Setup not found');

            return redirect(route('taxSetups.index'));
        }

        return view('tax_setups.show')->with('taxSetup', $taxSetup);
    }

    /**
     * Show the form for editing the specified TaxSetup.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $taxSetup = $this->taxSetupRepository->find($id);

        if (empty($taxSetup)) {
            Flash::error('Tax Setup not found');

            return redirect(route('taxSetups.index'));
        }

        return view('tax_setups.edit')->with('taxSetup', $taxSetup);
    }

    /**
     * Update the specified TaxSetup in storage.
     *
     * @param  int              $id
     * @param UpdateTaxSetupRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTaxSetupRequest $request)
    {
        $taxSetup = $this->taxSetupRepository->find($id);

        if (empty($taxSetup)) {
            Flash::error('Tax Setup not found');

            return redirect(route('taxSetups.index'));
        }

        $taxSetup = $this->taxSetupRepository->update($request->all(), $id);

        Flash::success('Tax Setup updated successfully.');

        return redirect(route('taxSetups.index'));
    }

    /**
     * Remove the specified TaxSetup from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $taxSetup = $this->taxSetupRepository->find($id);

        if (empty($taxSetup)) {
            Flash::error('Tax Setup not found');

            return redirect(route('taxSetups.index'));
        }

        $this->taxSetupRepository->delete($id);

        Flash::success('Tax Setup deleted successfully.');

        return redirect(route('taxSetups.index'));
    }
}
