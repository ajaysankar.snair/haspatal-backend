<?php
namespace App\Http\Controllers;
use App\Http\Requests\CreateHaspatal_360_registerRequest;
use App\Http\Requests\API\CreateStateAPIRequest;
use App\Http\Requests\API\UpdateStateAPIRequest;
use App\Models\State;
use App\Repositories\StateRepository;
use App\Repositories\Business_registerRepository;
use Illuminate\Http\Request;
use App\Models\Service_details;
use App\Models\Your_coverage_360;
use App\Models\Haspatal_360_register;
use App\Models\Wallet_360;
use App\Repositories\Wallet_360Repository;
use App\coupon;
use Carbon\Carbon;
use App\User;
use App\Models\City;
use App\couponrate;
use App\Models\Business_register;
use App\Http\Controllers\AppBaseController;
use Response;
use Auth;
use DB;
use Validator;
use Session;

class AdditionalApiController extends Controller
{
       /** @var  Business_registerRepository */
       private $businessRegisterRepository;

       public function __construct(Business_registerRepository $businessRegisterRepo)
       {
           $this->businessRegisterRepository = $businessRegisterRepo;
       }
   
       /**
        * @param Request $request
        * @return Response
        *
        * @SWG\Get(
        *      path="/businessRegisters",
        *      summary="Get a listing of the Business_registers.",
        *      tags={"Business_register"},
        *      description="Get all Business_registers",
        *      produces={"application/json"},
        *      @SWG\Response(
        *          response=200,
        *          description="successful operation",
        *          @SWG\Schema(
        *              type="object",
        *              @SWG\Property(
        *                  property="success",
        *                  type="boolean"
        *              ),
        *              @SWG\Property(
        *                  property="data",
        *                  type="array",
        *                  @SWG\Items(ref="#/definitions/Business_register")
        *              ),
        *              @SWG\Property(
        *                  property="message",
        *                  type="string"
        *              )
        *          )
        *      )
        * )
        */
    //
    
                        function states(Request $r)
    {
         $state=DB::table('state')->select('id','state')->orderBy('state','ASC')->where('country_id',$r->country_id)->get();
           // var_dump($msg);
          // echo $r->id;
          if(count($state)>0)
          {
            return response()->json(['status'=>true,'data'=>$state]);
          }
          else
          {
              return response()->json(['status'=>false]);
          }
      return ;
    }
            
     function districts(Request $r)
    {
         $dis=DB::table('district')->select('id','district_name')->orderBy('district_name','ASC')->where('state_id',$r->state_id)->get();
           // var_dump($msg);
          // echo $r->id;
          if(count($dis)>0)
          {
                return response()->json(['status'=>true,'data'=>$dis]);
          }
          else
          {
              return response()->json(['status'=>false]);
          }
      return ;
    }
     
        
    function cities(Request $r)
    {
         $city=DB::table('city')->select('id','city')->orderBy('city','ASC')->where('district_id',$r->district_id)->get();
           // var_dump($msg);
          // echo $r->id;
          if(count($city)>0)
          {
            return response()->json(['status'=>true,'data'=>$city]);
          }
          else
          {
              return response()->json(['status'=>false]);
          }
      return ;
    }
    
            function pincodes(Request $r)
    {
         $pin=DB::table('pincode')->select('pincode')->orderBy('pincode','ASC')->where('city_id',$r->city_id)->get();
           // var_dump($msg);
          // echo $r->id;
          if(count($pin)>0)
          {
            return response()->json(['status'=>true,'data'=>$pin]);
          }
          else
          {
              return response()->json(['status'=>false]);
          }
      return ;
    }
    
    
    
    public function serviceType()
    {
        $data = Service_details::select('id', 'service')->get();
        if($data){
        $Service_details['success']=true;
        $Service_details['message']="Date Fetched  successful";
        $Service_details['data']=$data;
        
    }
       
        return response()->json(  $Service_details,200);
    }
    public function getsate()
    {
        $data = State::select('id', 'state')->where('country_id',3)->orderBy('state','ASC')->get();
        if($data){
        $State['success']=true;
        $State['message']="Date Fetched  successful";
        $State['state_data']=$data;
        
    }
       
        return response()->json(  $State,200);
    }
    
    
              
    
         public function insert(Request $request) {
      //$name = $request->input('stud_name');
      DB::insert('insert into country (id,country,created_by,code) values(?,?,?,?)',[$request->id,$request->country,$request->created_by,$request->code]);
      return response()->json(['status'=>true,'message'=>'Country Inserted']);
      
   }
   
            public function country_list()
            {
                $country_list=DB::table('country')->select('id','country')->orderBy('country','ASC')->get();
                if(count($country_list)>0)
                {
                    return response()->json(['status'=>true,'Message'=>'Country List retrived Successfully','data'=>$country_list]);
                }
                else
                {
                    return response()->json(['status'=>false,'Message'=>'Country List not fetched']);
                }
            }
            
            function destroy_country(Request $request)
            {
                $is_delete=DB::table('country')->where('id',$request->id)->delete();
                
                if($is_delete)
                {
                    return response()->json(['status'=>true,'Message'=>'Country deleted']);
                }
                else
                {
                    return response()->json(['status'=>false,'Message'=>'Country not deleted']);
                }
            }
            
            
            function update_country(Request $request)
            {
                $inputall=$request->all();
                $is_update=DB::table('country')->where('id',$request->id)->update($inputall);
                
                if($is_update)
                {
                    return response()->json(['status'=>true,'Message'=>'Country Updated']);
                }
                else
                {
                    return response()->json(['status'=>false,'Message'=>'Country not Updated']);
                }
            }
    
                public function add_state(Request $request) {
      $all = $request->all();
      DB::insert('insert into state (id,state,created_by,country_id) values(?,?,?,?)',[$request->id,$request->state,$request->created_by,$request->country_id]);
      return response()->json(['status'=>true,'message'=>'State Saved']);
      
   }        
             function destroy_state(Request $request)
            {
                $is_delete=DB::table('state')->where('id',$request->id)->delete();
                
                if($is_delete)
                {
                    return response()->json(['status'=>true,'Message'=>'State deleted']);
                }
                else
                {
                    return response()->json(['status'=>false,'Message'=>'State not deleted']);
                }
            }      
   
                
                     function update_state(Request $request)
            {
                $inputall=$request->all();
                $is_update=DB::table('state')->where('id',$request->id)->update($inputall);
                
                if($is_update)
                {
                    return response()->json(['status'=>true,'Message'=>'State Updated']);
                }
                else
                {
                    return response()->json(['status'=>false,'Message'=>'State not Updated']);
                }
            }
                     public function add_district(Request $request) {
      $all = $request->all();
      DB::insert('insert into district (id,district_name,state_id,created_by,country_id) values(?,?,?,?,?)',[$request->id,$request->district_name,$request->state_id,$request->created_by,$request->country_id]);
      return response()->json(['status'=>true,'message'=>'District Saved']);
      
   }        
   
   
                     function destroy_district(Request $request)
            {
                $is_delete=DB::table('district')->where('id',$request->id)->delete();
                
                if($is_delete)
                {
                    return response()->json(['status'=>true,'Message'=>'District deleted']);
                }
                else
                {
                    return response()->json(['status'=>false,'Message'=>'District not deleted']);
                }
            }      
   
                
                     function update_district(Request $request)
            {
                $inputall=$request->all();
                $is_update=DB::table('district')->where('id',$request->id)->update($inputall);
                
                if($is_update)
                {
                    return response()->json(['status'=>true,'Message'=>'District Updated']);
                }
                else
                {
                    return response()->json(['status'=>false,'Message'=>'District not Updated']);
                }
            }   
            
                      public function add_city(Request $request) {
      $all = $request->all();
      DB::insert('insert into city (id,city,state_id,created_by,country_id,district_id) values(?,?,?,?,?,?)',[$request->id,$request->city,$request->state_id,$request->created_by,$request->country_id,$request->district_id]);
      return response()->json(['status'=>true,'message'=>'City Saved']);
      
   }        
   
   
                     function destroy_city(Request $request)
            {
                $is_delete=DB::table('city')->where('id',$request->id)->delete();
                
                if($is_delete)
                {
                    return response()->json(['status'=>true,'Message'=>'City deleted']);
                }
                else
                {
                    return response()->json(['status'=>false,'Message'=>'City not deleted']);
                }
            }      
   
                
                     function update_city(Request $request)
            {
                $inputall=$request->all();
                $is_update=DB::table('city')->where('id',$request->id)->update(["city"=>$request->city,"state_id"=>$request->state_id,"created_by"=>$request->created_by,"country_id"=>$request->country_id,"district_id"=>$request->district_id]);
                
                if($is_update)
                {
                    return response()->json(['status'=>true,'Message'=>'City Updated']);
                }
                else
                {
                    return response()->json(['status'=>false,'Message'=>'City not Updated']);
                }
            }
                
     public function city_list(Request $request)
     
    {
       $district_id=  $request->input('district_id');
        
            $result = City::select('id','city')->where('district_id', $district_id)->orderBy('city','ASC')->get();
        
  
       if (count($result)>0) {
            return response()->json(["status" => true,"massage"=>"City retrieved successfully","data" => $result]);
        }else{
            return response()->json(["status" => false]);
        }
    }
    public function get_districtlist(Request $request)
    {
        $data = DB::table('district')->select('id','district_name as district')->where('state_id',$request->state_id)->orderBy('district_name','ASC')->get();
        if(count($data)>0){
        $State['success']=true;
        $State['message']="Date Fetched  successful";
        $State['state_data']=$data;
         return response()->json(  $State,200);
    }    
    else
    {
        return response()->json(['success'=>false]);
    }
       
    }
    
                
    /*pincode manage
    *CRUD for Pincode from Kizaku Panel     
    *
    *
    */
    
             public function get_pincode(Request $request)
    {
        $data = DB::table('pincode')->select('pincode')->where('city_id',$request->city_id)->orderBy('pincode','ASC')->get();
        if(count($data)>0){
        $State['success']=true;
        $State['message']="Date Fetched  successful";
        $State['pincode_data']=$data;
         return response()->json(  $State,200);
    }    
    else
    {
        return response()->json(['success'=>false]);
    }
       
    }
    
                
    public function add_pincode(Request $request) {
      $all = $request->all();
      DB::insert('insert into pincode (id,pincode,state_id,city_id,created_by,country_id,district_id) values(?,?,?,?,?,?,?)',[$request->id,$request->pincode,$request->state_id,$request->city_id,$request->created_by,$request->country_id,$request->district_id]);
      return response()->json(['status'=>true,'message'=>'Pincode Saved']);
      
   }        
             function destroy_pincode(Request $request)
            {
                $is_delete=DB::table('pincode')->where('id',$request->id)->delete();
                
                if($is_delete)
                {
                    return response()->json(['status'=>true,'Message'=>'Pincode deleted']);
                }
                else
                {
                    return response()->json(['status'=>false,'Message'=>'Pincode not deleted']);
                }
            }      
   
                
                     function update_pincode(Request $request)
            {
                $inputall=$request->all();
                $is_update=DB::table('pincode')->where('id',$request->id)->update($inputall);
                
                if($is_update)
                {
                    return response()->json(['status'=>true,'Message'=>'Pincode Updated']);
                }
                else
                {
                    return response()->json(['status'=>false,'Message'=>'Pincode not Updated']);
                }
            }
    
            //closed


          public function validateRegistration(Request $obj)
          {
                 $email = $obj->email;
                 $mo_prefix=$obj->mo_prefix;
                 $mobile_no=$obj->mobile_no;


                 $email=explode('=',$email);
                 $email= $email[1];
                 //echo$email;


                  $mo_prefix=explode('=',$mo_prefix);
                 $mo_prefix= $mo_prefix[1];
                 //echo$mo_prefix;

                  $mobile_no=explode('=',$mobile_no);
                 $mobile_no= $mobile_no[1];
                //echo$mobile_no;



                    //print_r($obj->email);
               // $checkedData=Haspatal_360_register::find(3698745210,'mobile_no');

                        $where=['mobile_no'=>$mobile_no];
                        $where2=['email'=>$email];
               // $where=['mobile_no'=>9874563210,'mo_prefix'=>'+91','email'=>'raju103@gmaili.com'];
            $results = DB::table('haspatal_360_register')->where($where)->orWhere($where2)->get();
              // print_r($results);

   // echo count($results);
                if(count($results)>0)
                {

                     //$data['results']=$results;
                $data['success']=true;
                $data['message']="User Already Registered";
                return response()->json($data,200);
               
                
               }
               else
               {
                     $data['success']=false;
                $data['message']="Not Registered";

                return response()->json($data,200);
               }

          }   


     public function newreg(Request $request)
    {
        
        $book =  DB::table('business_register')->where('user_id', $request->user_id)->orderBy('user_id', 'desc')->first();
       /* echo "<pre>";
        print_r($book); exit;*/

        if(!empty($book)){
            $input = $request->all();
            $id = $book->id;
            if($request->hasfile('b_lic'))
            {
                $image1 = $request->file('b_lic');
                $extension1 = $image1->getClientOriginalExtension(); // getting image extension
                $filename1 =time().'__'.$image1->getClientOriginalName();
                $path1 = public_path('/media/b_lic/');
                $image1->move($path1, $filename1);
                $input['b_lic'] ='public/media/b_lic/'.$filename1;
            }
            if($request->hasfile('shop_pic'))
            {
                $image1 = $request->file('shop_pic');
                $extension1 = $image1->getClientOriginalExtension(); // getting image extension
                $filename1 =time().'__'.$image1->getClientOriginalName();
                $path1 = public_path('/media/shop_pic/');
                $image1->move($path1, $filename1);
                $input['shop_pic'] = 'public/media/shop_pic/'.$filename1;
            }
            if($request->hasfile('b_card_pic'))
            {
                $image1 = $request->file('b_card_pic');
                $extension1 = $image1->getClientOriginalExtension(); // getting image extension
                $filename1 =time().'__'.$image1->getClientOriginalName();
                $path1 = public_path('/media/b_card_pic/');
                $image1->move($path1, $filename1);
                $input['b_card_pic'] = 'public/media/b_card_pic/'.$filename1;
            }
                $input['pincode']='';
                $input['b_id']=$request->b_id;
            $businessRegister = $this->businessRegisterRepository->update($input, $id);
            $businessRegister['district']=$request->input('district');

        }

        if(empty($book))
        {
            $input = $request->all();
          //  $input['user_id']=$request->input('user_id');
            $input['created_by'] = $request->user_id;
            $input['updated_by'] = $request->user_id;
            if($request->hasfile('b_lic'))
            {
                $image1 = $request->file('b_lic');
                $extension1 = $image1->getClientOriginalExtension(); // getting image extension
                $filename1 =time().'__'.$image1->getClientOriginalName();
                $path1 = public_path('/media/b_lic/');
                $image1->move($path1, $filename1);
                $input['b_lic'] = 'public/media/shop_pic/'.$filename1;
            }
            if($request->hasfile('shop_pic'))
            {
                $image1 = $request->file('shop_pic');
                $extension1 = $image1->getClientOriginalExtension(); // getting image extension
                $filename1 =time().'__'.$image1->getClientOriginalName();
                $path1 = public_path('/media/shop_pic/');
                $image1->move($path1, $filename1);
                $input['shop_pic'] = 'public/media/shop_pic/'.$filename1;
            }
            if($request->hasfile('b_card_pic'))
            {
                $image1 = $request->file('b_card_pic');
                $extension1 = $image1->getClientOriginalExtension(); // getting image extension
                $filename1 =time().'__'.$image1->getClientOriginalName();
                $path1 = public_path('/media/b_card_pic/');
                $image1->move($path1, $filename1);
                $input['b_card_pic'] = 'public/media/b_card_pic/'.$filename1;
            }
            $input['pincode']='';
            $input['b_id']=$request->b_id;
            $businessRegister = $this->businessRegisterRepository->create($input);
            $businessRegister['district']=$request->input('district');

        }
        // return $this->sendResponse($businessRegister->toArray(), 'Business Register saved successfully');
    //   return response()->json(  $businessRegister,200);
      return response()->json(["status" =>true,"message" =>'Business Register Saved successfully']);
    }

     //second registration API

           public function newreg2(Request $request)
    {
        
        $book =  DB::table('business_register')->where('user_id', $request->user_id)->orderBy('user_id', 'desc')->first();
       /* echo "<pre>";
        print_r($book); exit;*/

        if(!empty($book)){
            $input = $request->all();
            $id = $book->id;

            $new2['state_id']=$request->input('state_id');
            $new2['city_id']=$request->input('city_id');
            $new2['pincode']=$request->input('pincode');
            $new2['address']=$request->input('address');
            $new2['district']=$request->input('district');
           // $new2['state_id']=$request->input('state_id');
           // $new2['city_name']=$request->input('city_name');
            $new2['b_id']=$request->input('b_id');


                 if($request->hasfile('b_lic'))
            {
                $image1 = $request->file('b_lic');
                $extension1 = $image1->getClientOriginalExtension(); // getting image extension
                $filename1 =time().'__'.$image1->getClientOriginalName();
                $path1 = public_path('/media/b_lic/');
                $image1->move($path1, $filename1);
                $new2['b_lic'] = 'public/media/b_lic/'.$filename1;
            }
           
      

            

                        $businessRegister = $this->businessRegisterRepository->update($new2, $id);

        }

        
        // return $this->sendResponse($businessRegister->toArray(), 'Business Register saved successfully');
    //   return response()->json(  $businessRegister,200);
      return response()->json(["status"=>true,"message" => 'Business Details saved successfully']);
    }


    //closed second 






        //third1 registration API

           public function newreg3(Request $request)
    {
        
        $book =  DB::table('business_register')->where('user_id', $request->user_id)->orderBy('user_id', 'desc')->first();
       /* echo "<pre>";
        print_r($book); exit;*/

        if(!empty($book)){
            $input = $request->all();
            $id = $book->id;

            $new3['patient_select_you']=$request->input('p_data');
            $new3['b_id']=$request->input('b_id');
            
            //var_dump($new3);
     $businessRegister = $this->businessRegisterRepository->update($new3, $id);

        }

        
        // return $this->sendResponse($businessRegister->toArray(), 'Business Register saved successfully');
    //   return response()->json(  $businessRegister,200);
      return response()->json(["status" =>true,"message"=>'Business Details saved successfully']);
    }

    //closed third1 

    public function gettt_districtlist()
    {
        $data = [

            
                        'Adilabad',
                        'Anantapur',
                        'Chittoor',
                        'Kakinada',
                        'Guntur',
                        'Hyderabad',
                        'Karimnagar',
                        'Khammam',
                        'Krishna',
                        'Kurnool',
                        'Mahbubnagar',
                        'Medak',
                        'Nalgonda',
                        'Nizamabad',
                        'Ongole',
                        'Hyderabad',
                        'Srikakulam',
                        'Nellore',
                        'Visakhapatnam',
                        'Vizianagaram',
                        'Warangal',
                        'Eluru',
                        'Kadapa',
                  
                        'Anjaw',
                        'Changlang',
                        'East Siang',
                        'Kurung Kumey',
                        'Lohit',
                        'Lower Dibang Valley',
                        'Lower Subansiri',
                        'Papum Pare',
                        'Tawang',
                        'Tirap',
                        'Dibang Valley',
                        'Upper Siang',
                        'Upper Subansiri',
                        'West Kameng',
                        'West Siang',
               
                        'Baksa',
                        'Barpeta',
                        'Bongaigaon',
                        'Cachar',
                        'Chirang',
                        'Darrang',
                        'Dhemaji',
                        'Dima Hasao',
                        'Dhubri',
                        'Dibrugarh',
                        'Goalpara',
                        'Golaghat',
                        'Hailakandi',
                        'Jorhat',
                        'Kamrup',
                        'Kamrup Metropolitan',
                        'Karbi Anglong',
                        'Karimganj',
                        'Kokrajhar',
                        'Lakhimpur',
                        'Marigaon',
                        'Nagaon',
                        'Nalbari',
                        'Sibsagar',
                        'Sonitpur',
                        'Tinsukia',
                        'Udalguri',
              
                        'Araria',
                        'Arwal',
                        'Aurangabad',
                        'Banka',
                        'Begusarai',
                        'Bhagalpur',
                        'Bhojpur',
                        'Buxar',
                        'Darbhanga',
                        'East Champaran',
                        'Gaya',
                        'Gopalganj',
                        'Jamui',
                        'Jehanabad',
                        'Kaimur',
                        'Katihar',
                        'Khagaria',
                        'Kishanganj',
                        'Lakhisarai',
                        'Madhepura',
                        'Madhubani',
                        'Munger',
                        'Muzaffarpur',
                        'Nalanda',
                        'Nawada',
                        'Patna',
                        'Purnia',
                        'Rohtas',
                        'Saharsa',
                        'Samastipur',
                        'Saran',
                        'Sheikhpura',
                        'Sheohar',
                        'Sitamarhi',
                        'Siwan',
                        'Supaul',
                        'Vaishali',
                        'West Champaran',
                        'Chandigarh',
               
                        'Bastar',
                        'Bijapur',
                        'Bilaspur',
                        'Dantewada',
                        'Dhamtari',
                        'Durg',
                        'Jashpur',
                        'Janjgir-Champa',
                        'Korba',
                        'Koriya',
                        'Kanker',
                        'Kabirdham (Kawardha)',
                        'Mahasamund',
                        'Narayanpur',
                        'Raigarh',
                        'Rajnandgaon',
                        'Raipur',
                        'Surguja',
             
                        'Dadra and Nagar Haveli',
            
                        'Daman',
                        'Diu',
          
                        'Central Delhi',
                        'East Delhi',
                        'New Delhi',
                        'North Delhi',
                        'North East Delhi',
                        'North West Delhi',
                        'South Delhi',
                        'South West Delhi',
                        'West Delhi',
           
                        'North Goa',
                        'South Goa',
          
                        'Ahmedabad',
                        'Amreli district',
                        'Anand',
                        'Banaskantha',
                        'Bharuch',
                        'Bhavnagar',
                        'Dahod',
                        'The Dangs',
                        'Gandhinagar',
                        'Jamnagar',
                        'Junagadh',
                        'Kutch',
                        'Kheda',
                        'Mehsana',
                        'Narmada',
                        'Navsari',
                        'Patan',
                        'Panchmahal',
                        'Porbandar',
                        'Rajkot',
                        'Sabarkantha',
                        'Surendranagar',
                        'Surat',
                        'Vyara',
                        'Vadodara',
                        'Valsad',
           
                        'Ambala',
                        'Bhiwani',
                        'Faridabad',
                        'Fatehabad',
                        'Gurgaon',
                        'Hissar',
                        'Jhajjar',
                        'Jind',
                        'Karnal',
                        'Kaithal',
                        'Kurukshetra',
                        'Mahendragarh',
                        'Mewat',
                        'Palwal',
                        'Panchkula',
                        'Panipat',
                        'Rewari',
                        'Rohtak',
                        'Sirsa',
                        'Sonipat',
                        'Yamuna Nagar',
             
                        'Bilaspur',
                        'Chamba',
                        'Hamirpur',
                        'Kangra',
                        'Kinnaur',
                        'Kullu',
                        'Lahaul and Spiti',
                        'Mandi',
                        'Shimla',
                        'Sirmaur',
                        'Solan',
                        'Una',
              
                        'Anantnag',
                        'Badgam',
                        'Bandipora',
                        'Baramulla',
                        'Doda',
                        'Ganderbal',
                        'Jammu',
                        'Kargil',
                        'Kathua',
                        'Kishtwar',
                        'Kupwara',
                        'Kulgam',
                        'Leh',
                        'Poonch',
                        'Pulwama',
                        'Rajauri',
                        'Ramban',
                        'Reasi',
                        'Samba',
                        'Shopian',
                        'Srinagar',
                        'Udhampur',
               
                        'Bokaro',
                        'Chatra',
                        'Deoghar',
                        'Dhanbad',
                        'Dumka',
                        'East Singhbhum',
                        'Garhwa',
                        'Giridih',
                        'Godda',
                        'Gumla',
                        'Hazaribag',
                        'Jamtara',
                        'Khunti',
                        'Koderma',
                        'Latehar',
                        'Lohardaga',
                        'Pakur',
                        'Palamu',
                        'Ramgarh',
                        'Ranchi',
                        'Sahibganj',
                        'Seraikela Kharsawan',
                        'Simdega',
                        'West Singhbhum',
               
                        'Bagalkot',
                        'Bangalore Rural',
                        'Bangalore Urban',
                        'Belgaum',
                        'Bellary',
                        'Bidar',
                        'Bijapur',
                        'Chamarajnagar',
                        'Chikkamagaluru',
                        'Chikkaballapur',
                        'Chitradurga',
                        'Davanagere',
                        'Dharwad',
                        'Dakshina Kannada',
                        'Gadag',
                        'Gulbarga',
                        'Hassan',
                        'Haveri district',
                        'Kodagu',
                        'Kolar',
                        'Koppal',
                        'Mandya',
                        'Mysore',
                        'Raichur',
                        'Shimoga',
                        'Tumkur',
                        'Udupi',
                        'Uttara Kannada',
                        'Ramanagara',
                        'Yadgir',
               
                        'Alappuzha',
                        'Ernakulam',
                        'Idukki',
                        'Kannur',
                        'Kasaragod',
                        'Kollam',
                        'Kottayam',
                        'Kozhikode',
                        'Malappuram',
                        'Palakkad',
                        'Pathanamthitta',
                        'Thrissur',
                        'Thiruvananthapuram',
                        'Wayanad',
              
                        'Alirajpur',
                        'Anuppur',
                        'Ashok Nagar',
                        'Balaghat',
                        'Barwani',
                        'Betul',
                        'Bhind',
                        'Bhopal',
                        'Burhanpur',
                        'Chhatarpur',
                        'Chhindwara',
                        'Damoh',
                        'Datia',
                        'Dewas',
                        'Dhar',
                        'Dindori',
                        'Guna',
                        'Gwalior',
                        'Harda',
                        'Hoshangabad',
                        'Indore',
                        'Jabalpur',
                        'Jhabua',
                        'Katni',
                        'Khandwa (East Nimar)',
                        'Khargone (West Nimar)',
                        'Mandla',
                        'Mandsaur',
                        'Morena',
                        'Narsinghpur',
                        'Neemuch',
                        'Panna',
                        'Rewa',
                        'Rajgarh',
                        'Ratlam',
                        'Raisen',
                        'Sagar',
                        'Satna',
                        'Sehore',
                        'Seoni',
                        'Shahdol',
                        'Shajapur',
                        'Sheopur',
                        'Shivpuri',
                        'Sidhi',
                        'Singrauli',
                        'Tikamgarh',
                        'Ujjain',
                        'Umaria',
                        'Vidisha',
               
                        'Ahmednagar',
                        'Akola',
                        'Amravati',
                        'Aurangabad',
                        'Bhandara',
                        'Beed',
                        'Buldhana',
                        'Chandrapur',
                        'Dhule',
                        'Gadchiroli',
                        'Gondia',
                        'Hingoli',
                        'Jalgaon',
                        'Jalna',
                        'Kolhapur',
                        'Latur',
                        'Mumbai City',
                        'Mumbai suburban',
                        'Nandurbar',
                        'Nanded',
                        'Nagpur',
                        'Nashik',
                        'Osmanabad',
                        'Parbhani',
                        'Pune',
                        'Raigad',
                        'Ratnagiri',
                        'Sindhudurg',
                        'Sangli',
                        'Solapur',
                        'Satara',
                        'Thane',
                        'Wardha',
                        'Washim',
                        'Yavatmal',
                   
                        'Bishnupur',
                        'Churachandpur',
                        'Chandel',
                        'Imphal East',
                        'Senapati',
                        'Tamenglong',
                        'Thoubal',
                        'Ukhrul',
                        'Imphal West',
             
                        'East Garo Hills',
                        'East Khasi Hills',
                        'Jaintia Hills',
                        'Ri Bhoi',
                        'South Garo Hills',
                        'West Garo Hills',
                        'West Khasi Hills',
              
                        'Aizawl',
                        'Champhai',
                        'Kolasib',
                        'Lawngtlai',
                        'Lunglei',
                        'Mamit',
                        'Saiha',
                        'Serchhip',
               
                        'Dimapur',
                        'Kohima',
                        'Mokokchung',
                        'Mon',
                        'Phek',
                        'Tuensang',
                        'Wokha',
                        'Zunheboto',
             
                        'Angul',
                        'Boudh (Bauda)',
                        'Bhadrak',
                        'Balangir',
                        'Bargarh (Baragarh)',
                        'Balasore',
                        'Cuttack',
                        'Debagarh (Deogarh)',
                        'Dhenkanal',
                        'Ganjam',
                        'Gajapati',
                        'Jharsuguda',
                        'Jajpur',
                        'Jagatsinghpur',
                        'Khordha',
                        'Kendujhar (Keonjhar)',
                        'Kalahandi',
                        'Kandhamal',
                        'Koraput',
                        'Kendrapara',
                        'Malkangiri',
                        'Mayurbhanj',
                        'Nabarangpur',
                        'Nuapada',
                        'Nayagarh',
                        'Puri',
                        'Rayagada',
                        'Sambalpur',
                        'Subarnapur (Sonepur)',
                        'Sundergarh',
                  
                        'Karaikal',
                        'Mahe',
                        'Pondicherry',
                        'Yanam',
          
                        'Amritsar',
                        'Barnala',
                        'Bathinda',
                        'Firozpur',
                        'Faridkot',
                        'Fatehgarh Sahib',
                        'Fazilka',
                        'Gurdaspur',
                        'Hoshiarpur',
                        'Jalandhar',
                        'Kapurthala',
                        'Ludhiana',
                        'Mansa',
                        'Moga',
                        'Sri Muktsar Sahib',
                        'Pathankot',
                        'Patiala',
                        'Rupnagar',
                        'Ajitgarh (Mohali)',
                        'Sangrur',
                        'Nawanshahr',
                        'Tarn Taran',
          
                        'Ajmer',
                        'Alwar',
                        'Bikaner',
                        'Barmer',
                        'Banswara',
                        'Bharatpur',
                        'Baran',
                        'Bundi',
                        'Bhilwara',
                        'Churu',
                        'Chittorgarh',
                        'Dausa',
                        'Dholpur',
                        'Dungapur',
                        'Ganganagar',
                        'Hanumangarh',
                        'Jhunjhunu',
                        'Jalore',
                        'Jodhpur',
                        'Jaipur',
                        'Jaisalmer',
                        'Jhalawar',
                        'Karauli',
                        'Kota',
                        'Nagaur',
                        'Pali',
                        'Pratapgarh',
                        'Rajsamand',
                        'Sikar',
                        'Sawai Madhopur',
                        'Sirohi',
                        'Tonk',
                        'Udaipur',
              
                        'East Sikkim',
                        'North Sikkim',
                        'South Sikkim',
                        'West Sikkim',
              
                        'Ariyalur',
                        'Chennai',
                        'Coimbatore',
                        'Cuddalore',
                        'Dharmapuri',
                        'Dindigul',
                        'Erode',
                        'Kanchipuram',
                        'Kanyakumari',
                        'Karur',
                        'Madurai',
                        'Nagapattinam',
                        'Nilgiris',
                        'Namakkal',
                        'Perambalur',
                        'Pudukkottai',
                        'Ramanathapuram',
                        'Salem',
                        'Sivaganga',
                        'Tirupur',
                        'Tiruchirappalli',
                        'Theni',
                        'Tirunelveli',
                        'Thanjavur',
                        'Thoothukudi',
                        'Tiruvallur',
                        'Tiruvarur',
                        'Tiruvannamalai',
                        'Vellore',
                        'Viluppuram',
                        'Virudhunagar',
               
                        'Dhalai',
                        'North Tripura',
                        'South Tripura',
                        'Khowai',
                        'West Tripura',
             
                        'Agra',
                        'Allahabad',
                        'Aligarh',
                        'Ambedkar Nagar',
                        'Auraiya',
                        'Azamgarh',
                        'Barabanki',
                        'Budaun',
                        'Bagpat',
                        'Bahraich',
                        'Bijnor',
                        'Ballia',
                        'Banda',
                        'Balrampur',
                        'Bareilly',
                        'Basti',
                        'Bulandshahr',
                        'Chandauli',
                        'Chhatrapati Shahuji Maharaj Nagar',
                        'Chitrakoot',
                        'Deoria',
                        'Etah',
                        'Kanshi Ram Nagar',
                        'Etawah',
                        'Firozabad',
                        'Farrukhabad',
                        'Fatehpur',
                        'Faizabad',
                        'Gautam Buddh Nagar',
                        'Gonda',
                        'Ghazipur',
                        'Gorakhpur',
                        'Ghaziabad',
                        'Hamirpur',
                        'Hardoi',
                        'Mahamaya Nagar',
                        'Jhansi',
                        'Jalaun',
                        'Jyotiba Phule Nagar',
                        'Jaunpur district',
                        'Ramabai Nagar (Kanpur Dehat)',
                        'Kannauj',
                        'Kanpur',
                        'Kaushambi',
                        'Kushinagar',
                        'Lalitpur',
                        'Lakhimpur Kheri',
                        'Lucknow',
                        'Mau',
                        'Meerut',
                        'Maharajganj',
                        'Mahoba',
                        'Mirzapur',
                        'Moradabad',
                        'Mainpuri',
                        'Mathura',
                        'Muzaffarnagar',
                        'Panchsheel Nagar district (Hapur)',
                        'Pilibhit',
                        'Shamli',
                        'Pratapgarh',
                        'Rampur',
                        'Raebareli',
                        'Saharanpur',
                        'Sitapur',
                        'Shahjahanpur',
                        'Sant Kabir Nagar',
                        'Siddharthnagar',
                        'Sonbhadra',
                        'Sant Ravidas Nagar',
                        'Sultanpur',
                        'Shravasti',
                        'Unnao',
                        'Varanasi',
               
                        'Almora',
                        'Bageshwar',
                        'Chamoli',
                        'Champawat',
                        'Dehradun',
                        'Haridwar',
                        'Nainital',
                        'Pauri Garhwal',
                        'Pithoragarh',
                        'Rudraprayag',
                        'Tehri Garhwal',
                        'Udham Singh Nagar',
                        'Uttarkashi',
              
                        'Birbhum',
                        'Bankura',
                        'Bardhaman',
                        'Darjeeling',
                        'Dakshin Dinajpur',
                        'Hooghly',
                        'Howrah',
                        'Jalpaiguri',
                        'Cooch Behar',
                        'Kolkata',
                        'Maldah',
                        'Paschim Medinipur',
                        'Purba Medinipur',
                        'Murshidabad',
                        'Nadia',
                        'North 24 Parganas',
                        'South 24 Parganas',
                        'Purulia',
                        'Uttar Dinajpur',
               
            ];
        
    
       
        return response()->json(  $data,200);
    }
    public function couponcode()
    {
      
        $data = couponrate::select('category_id', 'name','rate','promo_code')->get();
        if($data){
        $State['success']=true;
        $State['message']="Date Fetched  successful";
        $State['coupon']=$data;
        
    }
       
        return response()->json(  $State,200);
    }

            public function your_coupons(Request $request)
    {
       
        
        
        $data = coupon::select('category_name',DB::raw('DATE_ADD(created_at, INTERVAL 1 YEAR)'), 'id','rate','promo_code','coupon_limit')->where('user_id',$request->user_id)->where('category_d',$request->role_id)->get();
        if($data){
        $coupon['success']=true;
        $coupon['message']="Date Fetched  successful";
        $coupon['coupons']=$data;
        
        
        return response()->json(['status'=>true,'data'=>$coupon]);
    }
    else
    {
        return response()->json(['status'=>false,'message'=>'Data not fetched']);
    }
    }
    

    public function saveCoupan(Request $request)
    {
        $coupon = new coupon();
        $coupon->user_id = $request->input('user_id');
        $coupon->category_d = $request->input('category_d');
        $coupon->Category_name = $request->input('Category_name');
        $coupon->Rate = $request->input('Rate');
        $coupon->save();
        if( $coupon){
        return response()->json( "Coupon Saved successfully",200);
    }
}

         function coupon_redeem(Request $request)
            {
                $data=couponrate::get()->where('promo_code',$request->promo_code)->first();
                $isused=coupon::get()->where('user_id',$request->user_id)->where('promo_code',$request->promo_code)->where('status',1)->first();
                
                    if (!empty($data)) 
                    {
     if (!empty($isused)) {
     
     
                        $coupon =coupon::find($isused->id);
                          $coupon->status =0;
  $coupon->save();

             $wallet= new wallet_360();
            $wallet->wa_amount=$request->rate;
            $wallet->user_id=$request->user_id;
            $wallet->created_by =$request->user_id;
            $wallet->updated_by =$request->user_id;
            $wallet->save();
     return  response()->json(["status"=>true,"message"=>"your coupon applied"]);

       //update status  1 to 0 in coupons table      
     }
     else
     {
   return  response()->json(["status"=>false,"message"=>"You have already used this coupon"]);
     }
                    }  
                    else{
                       return  response()->json(["status"=>false,"message"=>"Invalid Promo Code"]);
                    }                  

                
            }

    
    public function viewbusiness(Request $request)
    {
        $data = Business_register::select('*')->get();
        if( $data){
        return response()->json( $data,200);
    }


    }  

    public function business_profiles(Request $request)
    {
        $data = Business_register::where('user_id',$request->user_id)
     ->join('country', 'business_register.country_id', '=', 'country.id')
     ->join('state', 'business_register.state_id', '=', 'state.id')
     ->join('city', 'business_register.city_id', '=', 'city.id')
     ->select('business_register.*','country.country','state.state','city.city')
     ->first();

        

        $path = array("b_lic"=>env('APP_URL').'public/media/b_lic/'.$data->b_lic,
                "b_card_pic"=>env('APP_URL').'public/media/b_card_pic/'.$data->b_card_pic,
                 "shop_pic"=>env('APP_URL').'public/media/shop_pic/'.$data->shop_pic);

        return response()->json(["status" => true,"data" => $data, "message" => "Business retrieved successfully","path" => $path]);
    }
  


public function set_pincode(Request $request)
    {
        if (!empty($request->user_id) and !empty($request->pincode)) {
         


        $book =  DB::table('business_register')->where('user_id', $request->user_id)->orderBy('user_id', 'desc')->first();
       /* echo "<pre>";
        print_r($book); exit;*/

        if(!empty($book)){
        

            $your_coverage_360=new Your_coverage_360();
            $your_coverage_360->user_id=$request->user_id;
            $your_coverage_360->c_pincode=$request->pincode;
            $your_coverage_360->save();

         if( $your_coverage_360){

  return response()->json(['status'=>true,'message'=>"Coverage Area Added successfully"]);
    
                        }
                }
                else
                {
                     return response()->json(['status'=>false,'message'=>"Business not registered"]);
                }
        }
    else
         {
                 return response()->json(['keys'=>['user_id','pincode'],'method'=>'post','message'=>'Please use valid Key and method','status'=>false]);
        }

    }

             function update_app(Request $request)
        {
                    return response()->json(['status'=>true,'data'=>8,'message'=>'Please update this application']);
        }

    public function show(Request $req)
    {
        $input=$req->all();
        if ($req->input('token')=="vghfrhrweghfrjfrgfhfrhgfrfhvdewfdewu") {
        
        $data=Haspatal_360_register::select('id','email','mobile_no','created_at')->get();
        if (!empty($data)) {

            return response()->json(["status"=>true,"message"=>'Data Fetched successfully',"data"=>$data]);

        }
    }
    else
    {
                    return response()->json(["status"=>false,"message"=>'Data not Fetched Use Valid Token']);
    }

    }


                 function count_users_by_roleID(Request $request)
                {

 if($request->today)
 {
     if($request->pins)
     {
   $pins=explode(",",$request->pins);
     $data['users']=DB::table('business_register') ->leftjoin('country','country.id','business_register.country_id') ->leftjoin('state','state.id','business_register.state_id') ->leftjoin('city','city.id','business_register.city_id') ->leftjoin('district','district.id','business_register.district') ->leftjoin('users','users.id','business_register.user_id')->where('users.user_type',4)->whereIn('pincode',$pins)->where('created_at', '>=', date('Y-m-d').' 00:00:00')->count();
 $data['pharmacy']=DB::table('business_register')
 ->join('users','users.id','business_register.user_id')
 ->leftjoin('country','country.id','business_register.country_id')
 ->leftjoin('state','state.id','business_register.state_id')
 ->leftjoin('district','district.id','business_register.district')
 ->leftjoin('city','city.id','business_register.city_id')
 ->where('users.user_type',4)->whereIn('pincode',$pins)
 ->where('b_id',11)->where('created_at', '>=', date('Y-m-d').' 00:00:00')->count();
 $data['labs']=DB::table('business_register') ->leftjoin('country','country.id','business_register.country_id') ->leftjoin('state','state.id','business_register.state_id') ->leftjoin('city','city.id','business_register.city_id') ->leftjoin('district','district.id','business_register.district') ->leftjoin('users','users.id','business_register.user_id')->where('users.user_type',4)->whereIn('pincode',$pins)->where('b_id',12)->where('created_at', '>=', date('Y-m-d').' 00:00:00')->count();
 $data['imaging_center']=DB::table('business_register') ->leftjoin('country','country.id','business_register.country_id') ->leftjoin('state','state.id','business_register.state_id') ->leftjoin('city','city.id','business_register.city_id') ->leftjoin('district','district.id','business_register.district') ->leftjoin('users','users.id','business_register.user_id')->where('users.user_type',4)->whereIn('pincode',$pins)->where('b_id',13)->where('created_at', '>=', date('Y-m-d').' 00:00:00')->count();
 $data['homecare']=DB::table('business_register') ->leftjoin('country','country.id','business_register.country_id') ->leftjoin('state','state.id','business_register.state_id') ->leftjoin('city','city.id','business_register.city_id') ->leftjoin('district','district.id','business_register.district') ->leftjoin('users','users.id','business_register.user_id')->where('users.user_type',4)->whereIn('pincode',$pins)->where('b_id',14)->where('created_at', '>=', date('Y-m-d').' 00:00:00')->count();
 $data['therapy_center']=DB::table('business_register') ->leftjoin('country','country.id','business_register.country_id') ->leftjoin('state','state.id','business_register.state_id') ->leftjoin('city','city.id','business_register.city_id') ->leftjoin('district','district.id','business_register.district') ->leftjoin('users','users.id','business_register.user_id')->where('users.user_type',4)->whereIn('pincode',$pins)->where('b_id',16)->where('created_at', '>=', date('Y-m-d').' 00:00:00')->count();
 $data['councelling']=DB::table('business_register') ->leftjoin('country','country.id','business_register.country_id') ->leftjoin('state','state.id','business_register.state_id') ->leftjoin('city','city.id','business_register.city_id') ->leftjoin('district','district.id','business_register.district') ->leftjoin('users','users.id','business_register.user_id')->where('users.user_type',4)->whereIn('pincode',$pins)->where('b_id',17)->where('created_at', '>=', date('Y-m-d').' 00:00:00')->count();
                     
     }
     else
     {
     $data['users']=DB::table('business_register') ->leftjoin('country','country.id','business_register.country_id') ->leftjoin('state','state.id','business_register.state_id') ->leftjoin('city','city.id','business_register.city_id') ->leftjoin('district','district.id','business_register.district') ->leftjoin('users','users.id','business_register.user_id')->where('users.user_type',4)->where('created_at', '>=', date('Y-m-d').' 00:00:00')->get()->count();
$data['pharmacy']=DB::table('business_register')
 ->join('users','users.id','business_register.user_id')
 ->leftjoin('country','country.id','business_register.country_id')
 ->leftjoin('state','state.id','business_register.state_id')
 ->leftjoin('district','district.id','business_register.district')
 ->leftjoin('city','city.id','business_register.city_id')
 ->count(); $data['labs']=DB::table('business_register') ->leftjoin('country','country.id','business_register.country_id') ->leftjoin('state','state.id','business_register.state_id') ->leftjoin('city','city.id','business_register.city_id') ->leftjoin('district','district.id','business_register.district') ->leftjoin('users','users.id','business_register.user_id')->where('users.user_type',4)->where('created_at', '>=', date('Y-m-d').' 00:00:00')->where('b_id',12)->get()->count();
 $data['imaging_center']=DB::table('business_register') ->leftjoin('country','country.id','business_register.country_id') ->leftjoin('state','state.id','business_register.state_id') ->leftjoin('city','city.id','business_register.city_id') ->leftjoin('district','district.id','business_register.district') ->leftjoin('users','users.id','business_register.user_id')->where('users.user_type',4)->where('created_at', '>=', date('Y-m-d').' 00:00:00')->where('b_id',13)->get()->count();
 $data['homecare']=DB::table('business_register') ->leftjoin('country','country.id','business_register.country_id') ->leftjoin('state','state.id','business_register.state_id') ->leftjoin('city','city.id','business_register.city_id') ->leftjoin('district','district.id','business_register.district') ->leftjoin('users','users.id','business_register.user_id')->where('users.user_type',4)->where('created_at', '>=', date('Y-m-d').' 00:00:00')->where('b_id',14)->get()->count();
 $data['therapy_center']=DB::table('business_register') ->leftjoin('country','country.id','business_register.country_id') ->leftjoin('state','state.id','business_register.state_id') ->leftjoin('city','city.id','business_register.city_id') ->leftjoin('district','district.id','business_register.district') ->leftjoin('users','users.id','business_register.user_id')->where('users.user_type',4)->where('created_at', '>=', date('Y-m-d').' 00:00:00')->where('b_id',16)->get()->count();
 $data['councelling']=DB::table('business_register') ->leftjoin('country','country.id','business_register.country_id') ->leftjoin('state','state.id','business_register.state_id') ->leftjoin('city','city.id','business_register.city_id') ->leftjoin('district','district.id','business_register.district') ->leftjoin('users','users.id','business_register.user_id')->where('users.user_type',4)->where('created_at', '>=', date('Y-m-d').' 00:00:00')->where('b_id',17)->get()->count();    
                          //  $today_date = strtotime('-1 days', strtotime(date('Y-m-d')));
                           
     }
                        }
                        else
                        {
 
 if($request->pins)
 {
     $pins=explode(",",$request->pins);
      $data['users']=DB::table('business_register') ->leftjoin('country','country.id','business_register.country_id') ->leftjoin('state','state.id','business_register.state_id') ->leftjoin('city','city.id','business_register.city_id') ->leftjoin('district','district.id','business_register.district') ->leftjoin('users','users.id','business_register.user_id')->where('users.user_type',4)->whereIn('business_register.pincode',$pins)->get()->count();
$data['pharmacy']=DB::table('business_register')
 ->join('users','users.id','business_register.user_id')
 ->leftjoin('country','country.id','business_register.country_id')
 ->leftjoin('state','state.id','business_register.state_id')
 ->leftjoin('district','district.id','business_register.district')
 ->leftjoin('city','city.id','business_register.city_id')
 ->where('users.user_type',4)->whereIn('pincode',$pins)
 ->where('b_id',11)->where('created_at', '>=', date('Y-m-d').' 00:00:00')->count(); $data['labs']=DB::table('business_register') ->leftjoin('country','country.id','business_register.country_id') ->leftjoin('state','state.id','business_register.state_id') ->leftjoin('city','city.id','business_register.city_id') ->leftjoin('district','district.id','business_register.district') ->leftjoin('users','users.id','business_register.user_id')->where('users.user_type',4)->whereIn('business_register.pincode',$pins)->where('b_id',12)->get()->count();
 $data['imaging_center']=DB::table('business_register') ->leftjoin('country','country.id','business_register.country_id') ->leftjoin('state','state.id','business_register.state_id') ->leftjoin('city','city.id','business_register.city_id') ->leftjoin('district','district.id','business_register.district') ->leftjoin('users','users.id','business_register.user_id')->where('users.user_type',4)->whereIn('business_register.pincode',$pins)->where('b_id',13)->get()->count();
 $data['homecare']=DB::table('business_register') ->leftjoin('country','country.id','business_register.country_id') ->leftjoin('state','state.id','business_register.state_id') ->leftjoin('city','city.id','business_register.city_id') ->leftjoin('district','district.id','business_register.district') ->leftjoin('users','users.id','business_register.user_id')->where('users.user_type',4)->whereIn('business_register.pincode',$pins)->where('b_id',14)->get()->count();
 $data['therapy_center']=DB::table('business_register') ->leftjoin('country','country.id','business_register.country_id') ->leftjoin('state','state.id','business_register.state_id') ->leftjoin('city','city.id','business_register.city_id') ->leftjoin('district','district.id','business_register.district') ->leftjoin('users','users.id','business_register.user_id')->where('users.user_type',4)->whereIn('business_register.pincode',$pins)->where('b_id',16)->get()->count();
 $data['councelling']=DB::table('business_register') ->leftjoin('country','country.id','business_register.country_id') ->leftjoin('state','state.id','business_register.state_id') ->leftjoin('city','city.id','business_register.city_id') ->leftjoin('district','district.id','business_register.district') ->leftjoin('users','users.id','business_register.user_id')->where('users.user_type',4)->whereIn('business_register.pincode',$pins)->where('b_id',17)->get()->count();       
 }
 else
 {
 $data['users']=DB::table('business_register') ->leftjoin('country','country.id','business_register.country_id') ->leftjoin('state','state.id','business_register.state_id') ->leftjoin('city','city.id','business_register.city_id') ->leftjoin('district','district.id','business_register.district') ->leftjoin('users','users.id','business_register.user_id')->where('users.user_type',4)->get()->count();
 $data['pharmacy']=DB::table('business_register')
 ->join('users','users.id','business_register.user_id')
 ->leftjoin('country','country.id','business_register.country_id')
 ->leftjoin('state','state.id','business_register.state_id')
 ->leftjoin('district','district.id','business_register.district')
 ->leftjoin('city','city.id','business_register.city_id')
 ->where('users.user_type',4)->where('business_register.b_id',11)->count(); 
 $data['labs']=DB::table('business_register') ->leftjoin('country','country.id','business_register.country_id') ->leftjoin('state','state.id','business_register.state_id') ->leftjoin('city','city.id','business_register.city_id') ->leftjoin('district','district.id','business_register.district') ->leftjoin('users','users.id','business_register.user_id')->where('users.user_type',4)->where('b_id',12)->get()->count();
 $data['imaging_center']=DB::table('business_register') ->leftjoin('country','country.id','business_register.country_id') ->leftjoin('state','state.id','business_register.state_id') ->leftjoin('city','city.id','business_register.city_id') ->leftjoin('district','district.id','business_register.district') ->leftjoin('users','users.id','business_register.user_id')->where('users.user_type',4)->where('b_id',13)->get()->count();
 $data['homecare']=DB::table('business_register') ->leftjoin('country','country.id','business_register.country_id') ->leftjoin('state','state.id','business_register.state_id') ->leftjoin('city','city.id','business_register.city_id') ->leftjoin('district','district.id','business_register.district') ->leftjoin('users','users.id','business_register.user_id')->where('users.user_type',4)->where('b_id',14)->get()->count();
 $data['therapy_center']=DB::table('business_register') ->leftjoin('country','country.id','business_register.country_id') ->leftjoin('state','state.id','business_register.state_id') ->leftjoin('city','city.id','business_register.city_id') ->leftjoin('district','district.id','business_register.district') ->leftjoin('users','users.id','business_register.user_id')->where('users.user_type',4)->where('b_id',16)->get()->count();
 $data['councelling']=DB::table('business_register') ->leftjoin('country','country.id','business_register.country_id') ->leftjoin('state','state.id','business_register.state_id') ->leftjoin('city','city.id','business_register.city_id') ->leftjoin('district','district.id','business_register.district') ->leftjoin('users','users.id','business_register.user_id')->where('users.user_type',4)->where('b_id',17)->get()->count();
                           
 }
                        }

                         if (!empty($data)) {
     return response()->json(['status'=>true,'data'=>$data]);
 }
 else
 {
     return response()->json(['status'=>false,'data'=>$data]);
 }
 

                }
                
                
                
                        function get_business(Request $request)
                        {
 
 
 $pins=explode(",",$request->pins);
 if($request->pins)
 
 {
 if($request->pins and $request->today)
     {
   
                          // $data=Business_register::select('b_name','mobile1','pincode','created_at')->whereIn('pincode',$pins)->where('b_id',$request->role_id)->where('created_at', '>=', date('Y-m-d').' 00:00:00')->get();
  $data=DB::table('business_register')
      ->leftjoin('country','country.id','business_register.country_id')
      ->leftjoin('state','state.id','business_register.state_id')
      ->leftjoin('city','city.id','business_register.city_id')
      ->leftjoin('district','district.id','business_register.district')
      ->leftjoin('users','users.id','business_register.user_id')
      ->whereIn('business_register.pincode',$pins)
      ->where('created_at', '>=', date('Y-m-d'))
      ->where('business_register.b_id',$request->type)->orderBy('created_at','DESC')
 ->select('business_register.b_name','country.country','state.state','city.city','business_register.pincode','business_register.created_at')
      ->get();
     }
     else
     {
 // $data=Business_register::select('b_name','mobile1','pincode','created_at')->whereIn('pincode',$pins)->where('b_id',$request->type)->orderBy('created_at','DESC')->get();  
 $data=DB::table('business_register')
      ->leftjoin('country','country.id','business_register.country_id')
      ->leftjoin('state','state.id','business_register.state_id')
      ->leftjoin('city','city.id','business_register.city_id')
      ->leftjoin('district','district.id','business_register.district')
      ->leftjoin('users','users.id','business_register.user_id')
      ->whereIn('business_register.pincode',$pins)
      ->where('business_register.b_id',$request->type)->orderBy('created_at','DESC')
 ->select('business_register.b_name','country.country','state.state','city.city','business_register.pincode','business_register.created_at')
      ->get();
     }
     
     return response()->json(['status'=>true,'data'=>$data]);
 }
 else if($request->country)
 {
     
         
        
      $data=DB::table('business_register')
      ->leftjoin('country','country.id','business_register.country_id')
      ->leftjoin('state','state.id','business_register.state_id')
      ->leftjoin('city','city.id','business_register.city_id')
      ->leftjoin('district','district.id','business_register.district')
      ->leftjoin('users','users.id','business_register.user_id')
      ->where('business_register.country_id',3)
      ->where('users.user_type',4)
      /*->whereNotNull('business_register.state_id')
      ->whereNotNull('business_register.district')
      ->whereNotNull('business_register.city_id')*/
      ->where('business_register.b_id',$request->type)->orderBy('created_at','DESC')
 ->select('users.mobile','users.email','business_register.b_name','country.country','state.state','city.city','business_register.pincode','business_register.created_at')
      ->get();  
    return response()->json(['status'=>true,'data'=>$data]);
 }
                        }
                
                
 function get_pending_business(Request $request)
 {
      if($request->country)
 {
     
         
        
      $data=DB::table('business_register')
      ->join('country','country.id','=','business_register.country_id','left')
      ->join('state','state.id','=','business_register.state_id','left')
      ->join('city','city.id','=','business_register.city_id','left')
      ->join('users','users.id','=','business_register.user_id','left')
      ->where('business_register.country_id',3)
      ->whereNull('business_register.state_id')
      ->whereNull('business_register.patient_select_you')
      ->where('business_register.is_discard',0)
      ->where('business_register.b_id',$request->type)->orderBy('created_at','DESC')
 ->select('business_register.id','users.mobile','users.role_id as b_type','users.email','business_register.b_name','country.country','state.state','city.city','business_register.pincode','users.created_at')
      ->get();  
    return response()->json(['status'=>true,'data'=>$data]);
 }
     
 }
 
 function single_pending_business(Request $request)
 {
  
     
         
        
      $data=DB::table('business_register')
      ->join('country','country.id','=','business_register.country_id','left')
      ->join('state','state.id','=','business_register.state_id','left')
      ->join('city','city.id','=','business_register.city_id','left')
      ->join('users','users.id','=','business_register.user_id','left')
      ->where('business_register.id',$request->id)
      ->where('business_register.country_id',3)
      ->whereNull('business_register.state_id')
      ->whereNull('business_register.patient_select_you')
      ->where('business_register.b_id',$request->type)->orderBy('created_at','DESC')
 ->select('business_register.id','users.mobile','users.role_id as b_type','users.email','business_register.b_name','country.country','state.state','city.city','business_register.pincode','users.created_at')
      ->get();  
    return response()->json(['status'=>true,'data'=>$data]);
 
     
 }
 
 
   /***
     *
     *This function is  used to set the discard h360  business and then it sill be used by H360 FollowUp Unit head in Kizaku Panel
     *
     ***/
                         public function business_h360_support(Request $request) {
      
            $kiz=DB::insert('insert into h360_support_kizaku(business_id,discard_by,assign_to,b_type) values(?,?,?,?)',[$request->business_id,$request->discard_by,$request->assign_to,$request->b_type]);
           $biz=DB::table('business_register')
            ->where('id',$request->business_id)
            ->update(['is_discard'=>1]);
     if($kiz and $biz)
     {
     
     $id='dashboard/support_ticket/'.$request->b_type.'/'.$request->business_id;
    

$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => 'http://kizaku.haspatal.com/haspatal.php',
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => '',
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => 'POST',
  CURLOPT_POSTFIELDS => array('token' => $id,'title'=>'H360 Registration','unit_id'=>32,'assign_to'=>88),
));

$response = curl_exec($curl);

curl_close($curl);
//echo $response;
   
   
                     return response()->json(['status'=>true,'message'=>'Discarded this Business','b_type'=>$request->b_type]);
                     
     }
     else
     {
   
   return response()->json(['status'=>false,'message'=>'failed  to Discarded this Business']);
     }
      
      }
 
   function discarded_list()
   {
       $data=DB::table('business_register')
               ->join('h360_support_kizaku','h360_support_kizaku.business_id','=','business_register.id')
               ->join('users','users.id','=','business_register.id')
               ->select('h360_support_kizaku.discard_by','business_register.id','users.mobile','users.role_id as b_type','users.email','business_register.b_name')
                 ->get();  
                 
             
            return response()->json(['status'=>true,'data'=>$data]);
 
   }
                
                 /*function count_users_approved(Request $request)
                {

 if($request->today)
 {
           //  $today_date = strtotime('-1 days', strtotime(date('Y-m-d')));
                    $data['users']=Business_register::get()->where('admin_status',1)->where('created_at', '>=', date('Y-m-d').' 00:00:00')->count();
 $data['pharmacy']=Business_register::get()->where('admin_status',1)->where('b_id',11)->where('created_at', '>=', date('Y-m-d').' 00:00:00')->count();
 $data['labs']=Business_register::get()->where('admin_status',1)->where('b_id',12)->where('created_at', '>=', date('Y-m-d').' 00:00:00')->count();
 $data['imaging_center']=Business_register::get()->where('b_id',13)->where('admin_status',1)->where('created_at', '>=', date('Y-m-d').' 00:00:00')->count();
 $data['homecare']=Business_register::get()->where('admin_status',1)->where('b_id',14)->where('created_at', '>=', date('Y-m-d').' 00:00:00')->count();
 $data['therapy_center']=Business_register::get()->where('admin_status',1)->where('b_id',16)->where('created_at', '>=', date('Y-m-d').' 00:00:00')->count();
 $data['councelling']=Business_register::get()->where('admin_status',1)->where('b_id',17)->where('created_at', '>=', date('Y-m-d').' 00:00:00')->count();
                           
                        }
                        else
                        {
 $data['users']=Business_register::get()->where('admin_status',1)->count();
 $data['pharmacy']=Business_register::get()->where('admin_status',1)->where('b_id',11)->count();
 $data['labs']=Business_register::get()->where('admin_status',1)->where('b_id',12)->count();
 $data['imaging_center']=Business_register::get()->where('admin_status',1)->where('b_id',13)->count();
 $data['homecare']=Business_register::get()->where('admin_status',1)->where('b_id',14)->count();
 $data['therapy_center']=Business_register::get()->where('admin_status',1)->where('b_id',16)->count();
 $data['councelling']=Business_register::get()->where('admin_status',1)->where('b_id',17)->count();
 
 
                        }

                         if (!empty($data)) {
     return response()->json(['status'=>true,'data'=>$data]);
 }
 else
 {
     return response()->json(['status'=>false,'data'=>$data]);
 }
 

                }*/

                    
                    function hdashboard_change(Request $request)
                    {

$from = $request->start_date;
$to = $request->end_date;
//$title="Sales From: ".$from." To: ".$to;
//$sales = Sale::whereBetween('created_at', [$from.' 00:00:00',$to.' 23:59:59'])->get();


                           /* $data['users']=Business_register::get()->whereNotIn('b_id',2)->count();
 $data['pharmacy']=Business_register::get()->where('b_id',11)->whereBetween('created_at', [$from.' 00:00:00',$to.' 23:59:59'])->count();
 $data['labs']=Business_register::get()->where('b_id',12)->whereBetween('created_at', [$from.' 00:00:00',$to.' 23:59:59'])->count();
 $data['imaging_center']=Business_register::get()->where('b_id',13)->whereBetween('created_at', [$from.' 00:00:00',$to.' 23:59:59'])->count();
 $data['homecare']=Business_register::get()->where('b_id',14)->whereBetween('created_at', [$from.' 00:00:00',$to.' 23:59:59'])->count();
 $data['therapy_center']=Business_register::get()->where('b_id',16)->whereBetween('created_at', [$from.' 00:00:00',$to.' 23:59:59'])->count();
 $data['councelling']=Business_register::get()->where('b_id',17)->whereBetween('created_at', [$from.' 00:00:00',$to.' 23:59:59'])->count();
                    */
 $data['users']=Business_register::get()
 ->where('country_id',$request->country)
 ->where('city_id',$request->city)
 ->where('state_id',$request->state)
 ->where('pincode',$request->pincode)
 ->where('district',$request->district)
 ->count();


 $data['pharmacy']=Business_register::get()
 ->where('b_id',11)
 ->where('country_id',$request->country)
 ->where('city_id',$request->city)
 ->where('state_id',$request->state)
 ->where('pincode',$request->pincode)
 ->where('district',$request->district)
 ->whereBetween('created_at', [$from.' 00:00:00',$to.' 23:59:59'])->count();

 $data['labs']=Business_register::get()->where('b_id',12)
 ->where('country_id',$request->country)
 ->where('city_id',$request->city)
 ->where('state_id',$request->state)
 ->where('pincode',$request->pincode)
 ->where('district',$request->district)
 ->whereBetween('created_at', [$from.' 00:00:00',$to.' 23:59:59'])->count();


 $data['imaging_center']=Business_register::get()
 ->where('b_id',13)
 ->where('country_id',$request->country)
 ->where('city_id',$request->city)
 ->where('state_id',$request->state)
 ->where('pincode',$request->pincode)
 ->where('district',$request->district)
 ->whereBetween('created_at', [$from.' 00:00:00',$to.' 23:59:59'])->count();

 $data['homecare']=Business_register::get()
 ->where('country_id',$request->country)
 ->where('city_id',$request->city)
 ->where('state_id',$request->state)
 ->where('pincode',$request->pincode)
 ->where('district',$request->district)
 ->where('b_id',14)
 ->whereBetween('created_at', [$from.' 00:00:00',$to.' 23:59:59'])->count();


 $data['therapy_center']=Business_register::get()
 ->where('country_id',$request->country)
 ->where('city_id',$request->city)
 ->where('state_id',$request->state)
 ->where('pincode',$request->pincode)
 ->where('district',$request->district)
 ->where('b_id',16)
 ->whereBetween('created_at', [$from.' 00:00:00',$to.' 23:59:59'])->count();


 $data['councelling']=Business_register::get()
 ->where('b_id',17)
 ->where('country_id',$request->country)
 ->where('city_id',$request->city)
 ->where('state_id',$request->state)
 ->where('pincode',$request->pincode)
 ->where('district',$request->district)
 ->whereBetween('created_at', [$from.' 00:00:00',$to.' 23:59:59'])->count();

                         if (!empty($data)) {
     return response()->json(['status'=>true,'data'=>$data]);
 }
 else
 {
     return response()->json(['status'=>false,'data'=>$data]);
 }
                    }
                    
                    
                       function compare_haspatal($start_date,$end_date,$country,$state,$district,$city,$pincode,$label)
                    {

$from = $start_date;
$to = $end_date;
//$title="Sales From: ".$from." To: ".$to;
//$sales = Sale::whereBetween('created_at', [$from.' 00:00:00',$to.' 23:59:59'])->get();
 //$where=["country_id"=>$country,"state_id"=>$state,"city_id"=>$city,"pincode"=>$pincode];

 $data['users']=Business_register::get()
 ->where('country_id',$country)
 ->where('city_id',$city)
 ->where('state_id',$state)
 ->where('pincode',$pincode)
 ->where('district',$district)
 ->whereBetween('created_at', [$from.' 00:00:00',$to.' 23:59:59'])->count();
 


 $data['pharmacy']=Business_register::get()
 ->where('b_id',11)
 ->where('country_id',$country)
 ->where('city_id',$city)
 ->where('state_id',$state)
 ->where('pincode',$pincode)
 ->where('district',$district)
 ->whereBetween('created_at', [$from.' 00:00:00',$to.' 23:59:59'])->count();

 $data['labs']=Business_register::get()->where('b_id',12)
 ->where('country_id',$country)
 ->where('city_id',$city)
 ->where('state_id',$state)
 ->where('pincode',$pincode)
 ->where('district',$district)
 ->whereBetween('created_at', [$from.' 00:00:00',$to.' 23:59:59'])->count();


 $data['imaging_center']=Business_register::get()
 ->where('b_id',13)
 ->where('country_id',$country)
 ->where('city_id',$city)
 ->where('state_id',$state)
 ->where('pincode',$pincode)
 ->where('district',$district)
 ->whereBetween('created_at', [$from.' 00:00:00',$to.' 23:59:59'])->count();

 $data['homecare']=Business_register::get()
 ->where('country_id',$country)
 ->where('city_id',$city)
 ->where('state_id',$state)
 ->where('pincode',$pincode)
 ->where('district',$district)
 ->where('b_id',14)
 ->whereBetween('created_at', [$from.' 00:00:00',$to.' 23:59:59'])->count();


 $data['therapy_center']=Business_register::get()
 ->where('country_id',$country)
 ->where('city_id',$city)
 ->where('state_id',$state)
 ->where('pincode',$pincode)
 ->where('district',$district)
 ->where('b_id',16)
 ->whereBetween('created_at', [$from.' 00:00:00',$to.' 23:59:59'])->count();


 $data['councelling']=Business_register::get()
 ->where('b_id',17)
 ->where('country_id',$country)
 ->where('city_id',$city)
 ->where('state_id',$state)
 ->where('pincode',$pincode)
 ->where('district',$district)
 ->whereBetween('created_at', [$from.' 00:00:00',$to.' 23:59:59'])->count();
     $data['range_name']=$label;
     if(!empty($data))
     {
                         return $data;
                     }else
                     {
                        return false;
                     }
                    }

     function compare_dashboard(Request $request)
     {
   $data['status']=true;
   $data['response1']=$this->compare_haspatal($request->start_date1,$request->end_date1,$request->comp_country1,$request->comp_state1,$request->comp_district1,$request->comp_city1,$request->pincode1,$request->label1);

   $data['response2']=$this->compare_haspatal($request->start_date2,$request->end_date2,$request->comp_country2,$request->comp_state2,$request->comp_district2,$request->comp_city2,$request->pincode2,$request->label2);
   return response()->json(['data'=>$data]);
     }
     
     
     function count_users($status,$today=null)
     {
                    if(!empty($today))
 {
                   $data['users']=Business_register::get()->where('admin_status',$status)->where('created_at', '>=', date('Y-m-d').' 00:00:00')->count();
 $data['pharmacy']=Business_register::get()->where('admin_status',$status)->where('b_id',11)->where('created_at', '>=', date('Y-m-d').' 00:00:00')->count();
 $data['labs']=Business_register::get()->where('admin_status',$status)->where('b_id',12)->where('created_at', '>=', date('Y-m-d').' 00:00:00')->count();
 $data['imaging_center']=Business_register::get()->where('b_id',13)->where('admin_status',$status)->where('created_at', '>=', date('Y-m-d').' 00:00:00')->count();
 $data['homecare']=Business_register::get()->where('admin_status',$status)->where('b_id',14)->where('created_at', '>=', date('Y-m-d').' 00:00:00')->count();
 $data['therapy_center']=Business_register::get()->where('admin_status',$status)->where('b_id',16)->where('created_at', '>=', date('Y-m-d').' 00:00:00')->count();
 $data['councelling']=Business_register::get()->where('admin_status',$status)->where('b_id',17)->where('created_at', '>=', date('Y-m-d').' 00:00:00')->count();
                           
                        }
                        else
                        {
 $data['users']=Business_register::get()->where('admin_status',$status)->count();
 $data['pharmacy']=Business_register::get()->where('admin_status',$status)->where('b_id',11)->count();
 $data['labs']=Business_register::get()->where('admin_status',$status)->where('b_id',12)->count();
 $data['imaging_center']=Business_register::get()->where('admin_status',$status)->where('b_id',13)->count();
 $data['homecare']=Business_register::get()->where('admin_status',$status)->where('b_id',14)->count();
 $data['therapy_center']=Business_register::get()->where('admin_status',$status)->where('b_id',16)->count();
 $data['councelling']=Business_register::get()->where('admin_status',$status)->where('b_id',17)->count();
 
 
                        }
                        return $data;
     }
     
     
     

     function count_users_approved(Request $request)
                {

   $data['approved_user']=$this->count_users(1);
   $data['pending_user']=$this->count_users(null);
   $data['hold_user']=$this->count_users(3);


                         if (!empty($data)) {
     return response()->json(['status'=>true,'data'=>$data]);
 }
 else
 {
     return response()->json(['status'=>false,'data'=>$data]);
 }
 

                }
                
                function count_users_for_sales($status,$pins,$today=null)
     {
                    if(!empty($today))
 {
                     $data['users']=Business_register::get()->where('admin_status',$status)->whereIn('pincode',$pins)->where('created_at', '>=', date('Y-m-d').' 00:00:00')->count();
 $data['pharmacy']=Business_register::get()->where('admin_status',$status)->whereIn('pincode',$pins)->where('b_id',11)->where('created_at', '>=', date('Y-m-d').' 00:00:00')->count();
 $data['labs']=Business_register::get()->where('admin_status',$status)->where('b_id',12)->where('created_at', '>=', date('Y-m-d').' 00:00:00')->count();
 $data['imaging_center']=Business_register::get()->where('b_id',13)->whereIn('pincode',$pins)->where('admin_status',$status)->where('created_at', '>=', date('Y-m-d').' 00:00:00')->count();
 $data['homecare']=Business_register::get()->where('admin_status',$status)->whereIn('pincode',$pins)->where('b_id',14)->where('created_at', '>=', date('Y-m-d').' 00:00:00')->count();
 $data['therapy_center']=Business_register::get()->where('admin_status',$status)->whereIn('pincode',$pins)->where('b_id',16)->where('created_at', '>=', date('Y-m-d').' 00:00:00')->count();
 $data['councelling']=Business_register::get()->where('admin_status',$status)->whereIn('pincode',$pins)->where('b_id',17)->where('created_at', '>=', date('Y-m-d').' 00:00:00')->count();
                           
                        }
                        else
                        {
 
 $data['users']=Business_register::get()->where('admin_status',$status)->whereIn('pincode',$pins)->count();
 $data['pharmacy']=Business_register::get()->where('admin_status',$status)->whereIn('pincode',$pins)->where('b_id',11)->count();
 $data['labs']=Business_register::get()->where('admin_status',$status)->whereIn('pincode',$pins)->where('b_id',12)->count();
 $data['imaging_center']=Business_register::get()->where('admin_status',$status)->whereIn('pincode',$pins)->where('b_id',13)->count();
 $data['homecare']=Business_register::get()->where('admin_status',$status)->whereIn('pincode',$pins)->where('b_id',14)->count();
 $data['therapy_center']=Business_register::get()->where('admin_status',$status)->whereIn('pincode',$pins)->where('b_id',16)->count();
 $data['councelling']=Business_register::get()->where('admin_status',$status)->whereIn('pincode',$pins)->where('b_id',17)->count();
 
 
                        }
                        return $data;
     }
     
              function count_for_salesOfficer(Request $request)
                {

   if(!empty($request->today) and !empty($request->pins))
   {
       $pins=explode(',',$request->pins);
   $data['approved_user']=$this->count_users_for_sales(1,$pins,'today');
   $data['pending_user']=$this->count_users_for_sales(null,$pins,'today');
   $data['hold_user']=$this->count_users_for_sales(3,$pins,'today');
   }
   else
   {
       
       
       $pins=explode(',',$request->pins);
       $data['all']=$pins;
      
   $data['approved_user']=$this->count_users_for_sales(1,$pins);
   $data['pending_user']=$this->count_users_for_sales(null,$pins);
   $data['hold_user']=$this->count_users_for_sales(3,$pins); 
   
   
   }

                         if (!empty($data)) {
     return response()->json(['status'=>true,'data'=>$data]);
 }
 else
 {
     return response()->json(['status'=>false,'data'=>$data]);
 }
 

                }
                    //doctor for kizaku
                    
                    /**
                     * filter_by as week,today,this month,year,fortnight,yesterday
                     * 
                     * 
                     * */
                         public function doctor_count_startYear($status,$reg=null)
     {
                        $drdata=0;
   if(empty($reg))
   {
       $drdata=DB::table('doctor_details')
   ->join('users','users.userDetailsId','doctor_details.id')
   ->whereYear('users.created_at',Carbon::now()->format('Y'))
   ->where('users.status',$status)
   ->where('users.user_type',1)
   ->count();
   }
   else
   {
       $drdata=DB::table('doctor_details')
   ->join('users','users.userDetailsId','doctor_details.id')
   ->whereYear('users.created_at',Carbon::now()->format('Y'))
   ->where('users.user_type',1)
   ->count();
   }
   
   
   
   if($drdata>0)
   {
       return $drdata;
   }
   else
   {
       return false;
   }
   
   
     } 
     
     
     
      public function doctor_count_startForty($status,$reg=null)
     {
                        $drdata=0;
   if(empty($reg))
   {
    $drdata=DB::table('doctor_details')
   ->join('users','users.userDetailsId','doctor_details.id')
   ->whereBetween('users.created_at',[Carbon::now()->subDays(15),Carbon::now()])
   ->where('users.status',$status)
   ->where('users.user_type',1)
   ->count();
   }
   else
   {
   $drdata=DB::table('doctor_details')
   ->join('users','users.userDetailsId','doctor_details.id')
   ->whereBetween('users.created_at',[Carbon::now()->subDays(15),Carbon::now()])
   ->where('users.user_type',1)
   ->count();
   }

   
   
   if($drdata>0)
   {
       return $drdata;
   }
   else
   {
       return false;
   }
   
   
     } 
     
     
     public function doctor_count_startMonth($status,$reg=null)
     {
                        $drdata=0;
   if(empty($reg))
   {
       $drdata=DB::table('doctor_details')
   ->join('users','users.userDetailsId','doctor_details.id')
   ->whereMonth('users.created_at',Carbon::now()->format('m'))
   ->where('users.status',$status)
   ->where('users.user_type',1)
   ->count();
   }
   else
   {
       $drdata=DB::table('doctor_details')
   ->join('users','users.userDetailsId','doctor_details.id')
   ->whereMonth('users.created_at',Carbon::now()->format('m'))
   ->where('users.user_type',1)
   ->count();
   }
   
   
   
   if($drdata>0)
   {
       return $drdata;
   }
   else
   {
       return false;
   }
   
   
     }  
    
    
     public function doctor_count_startWeek($status,$reg=null)
     {
                        $drdata=0;
   if(empty($reg))
   {
      $drdata=DB::table('doctor_details')
   ->join('users','users.userDetailsId','doctor_details.id')
   ->whereBetween('users.created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])
   ->where('users.status',$status)
   ->where('users.user_type',1)
   ->count();
    
   }
   else
   {
        $drdata=DB::table('doctor_details')
   ->join('users','users.userDetailsId','doctor_details.id')
   ->whereBetween('users.created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])
   ->where('users.user_type',1)
   ->count();
  
   }
   
   
   if($drdata>0)
   {
       return $drdata;
   }
   else
   {
       return false;
   }
   
   
     }


      public function doctor_count_startYest($status,$reg=null)
     {
                        $drdata=0;
   if(empty($reg))
   {
   $drdata=DB::table('doctor_details')
   ->join('users','users.userDetailsId','doctor_details.id')
   ->where('users.created_at','like',Carbon::yesterday()->format('Y-m-d').'%')
   ->where('users.status',$status)
   ->where('users.user_type',1)
   ->count(); 
   }
   else
   {
   $drdata=DB::table('doctor_details')
   ->join('users','users.userDetailsId','doctor_details.id')
   ->where('users.created_at','like',Carbon::yesterday()->format('Y-m-d').'%')
   ->where('users.user_type',1)
   ->count(); 
   }
   


   
   
   if($drdata>0)
   {
       return $drdata;
   }
   else
   {
       return false;
   }
   
   
     }
     
     public function doctor_count_startToday($status,$reg=null)
     {
                        $drdata=0;
   if(empty($reg))
   {
   $drdata=DB::table('doctor_details')
   ->join('users','users.userDetailsId','doctor_details.id')
   ->where('users.created_at','like',Carbon::today()->format('Y-m-d').'%')
   ->where('users.status',$status)
   ->where('users.user_type',1)
   ->count();
   }
   else
   {
    $drdata=DB::table('doctor_details')
   ->join('users','users.userDetailsId','doctor_details.id')
   ->where('users.created_at','like',Carbon::today()->format('Y-m-d').'%')
   ->where('users.user_type',1)
   ->count();  
   }
   
   if($drdata>0)
   {
       return $drdata;
   }
   else
   {
       return false;
   }
   
   
     }
     
   public function doctor_count_startTotal($status,$reg=null)
     {
                        $drdata=0;
   if(empty($reg))
   {
   $drdata=DB::table('doctor_details')
   ->join('users','users.userDetailsId','doctor_details.id')
   ->where('users.status',$status)
   ->where('users.user_type',1)
   ->count();
   }
   else
   {
    $drdata=DB::table('doctor_details')
   ->join('users','users.userDetailsId','doctor_details.id')
   ->where('users.user_type',1)
   ->count();
   }
   
   
   if($drdata>0)
   {
       return $drdata;
   }
   else
   {
       return false;
   }
   
   
     }
     
     //approve doctors for Kizaku all blow Module
     public function doctor_count(Request $request)
     {
   
   $data=[];
   $data['today']=$this->doctor_count_startToday(0);
   $data['year']=$this->doctor_count_startYear(0);
   $data['fortnight']=$this->doctor_count_startForty(0);
   $data['week']=$this->doctor_count_startWeek(0);
   $data['yesterday']=$this->doctor_count_startYest(0);
   $data['month']=$this->doctor_count_startMonth(0);
   $data['total']=$this->doctor_count_startTotal(0);
   //$data['bearerToken']=$request->bearerToken();
   $data['status']=true;
   return response()->json($data);
     }
     
     
     public function doctor_count_pending(Request $request)
     {
   
   $data=[];
   $data['today']=$this->doctor_count_startToday(1);
   $data['year']=$this->doctor_count_startYear(1);
   $data['fortnight']=$this->doctor_count_startForty(1);
   $data['week']=$this->doctor_count_startWeek(1);
   $data['yesterday']=$this->doctor_count_startYest(1);
   $data['month']=$this->doctor_count_startMonth(1);
   $data['total']=$this->doctor_count_startTotal(1);
   //$data['bearerToken']=$request->bearerToken();
   $data['status']=true;
   return response()->json($data);
     }
     
     
     public function doctor_count_rejected(Request $request)
     {
   
            $data=[];
            $data['today']=$this->doctor_count_startToday(3);
           $data['year']=$this->doctor_count_startYear(3);
           $data['fortnight']=$this->doctor_count_startForty(3);
           $data['week']=$this->doctor_count_startWeek(3);
           $data['yesterday']=$this->doctor_count_startYest(3);
           $data['month']=$this->doctor_count_startMonth(3);
           $data['total']=$this->doctor_count_startTotal(3);
   //$data['bearerToken']=$request->bearerToken();
   $data['status']=true;
   return response()->json($data);
     }
     
     
     public function doctor_count_registration(Request $request)
     {
   //if we will pass two argumnt then only all registration not deppend on status
   $data=[];
   $data['today']=$this->doctor_count_startToday(2,1);
   $data['year']=$this->doctor_count_startYear(2,1);
   $data['fortnight']=$this->doctor_count_startForty(2,1);
   $data['week']=$this->doctor_count_startWeek(2,1);
   //$y=date("Y-m-j H:i:s", strtotime( '-1 days' ) );
      $data['yesterday']=$this->doctor_count_startYest(2,1);
   $data['month']=$this->doctor_count_startMonth(2,1);
   $data['total']=$this->doctor_count_startTotal(2,1);
   //$data['bearerToken']=$request->bearerToken();
   $data['status']=true;
   return response()->json($data);
     }
     
     public function doctor_list(Request $request)
     {
  $datalist= DB::table('doctor_details')
   ->leftjoin('users','users.userDetailsId','doctor_details.id')
   ->leftjoin('state','state.id','doctor_details.state')
   ->leftjoin('district','district.id','doctor_details.district')
   ->leftjoin('specilities','specilities.id','doctor_details.speciality')
   ->leftjoin('city','city.id','doctor_details.city')
   ->where('users.status',$request->status)
   ->orderBy('users.created_at','desc')
    ->select('doctor_details.first_name','doctor_details.last_name','doctor_details.email','doctor_details.main_mobile','doctor_details.d_uni_id','doctor_details.id','state.state','district.district_name','city.city','specilities.specility','users.created_at','doctor_details.pincode')->get();
   
   if(count($datalist)>0)
   {
       $data['total']=count($datalist);
   $data['status']=true;
   $data['data']=$datalist;
   
   }
   else
   {
   $data['status']=true;
   $data['data']=$datalist;
   }

   return response()->json([$data]);
     }
     
     /**
      * Doctor List According to Status 
      * 
      * */
      
      /**
                     * filter_by as week,today,this month,year,fortnight,yesterday
                     * 
                     * 
                     * */
                         public function doctor_listYear(Request $request)
     {
                        $drdata=0;
    if(isset($request->status))
   {
   $drdata=DB::table('doctor_details')
   ->leftjoin('users','users.userDetailsId','doctor_details.id')
   ->leftjoin('state','state.id','doctor_details.state')
   ->leftjoin('district','district.id','doctor_details.district')
   ->leftjoin('city','city.id','doctor_details.city')
   ->whereYear('users.created_at',Carbon::now()->format('Y'))
   ->leftjoin('specilities','specilities.id','doctor_details.speciality')
   ->where('users.status',$request->status)
   ->where('users.user_type',1)
    ->select('doctor_details.first_name','doctor_details.last_name','doctor_details.email','doctor_details.main_mobile','doctor_details.d_uni_id','doctor_details.id','state.state','district.district_name','city.city','specilities.specility','users.created_at','doctor_details.pincode')->get();
   }
   else
   {
   $drdata=DB::table('doctor_details')
   ->leftjoin('users','users.userDetailsId','doctor_details.id')
   ->leftjoin('state','state.id','doctor_details.state')
   ->leftjoin('district','district.id','doctor_details.district')
   ->leftjoin('specilities','specilities.id','doctor_details.speciality')
   ->leftjoin('city','city.id','doctor_details.city')
   ->whereYear('users.created_at',Carbon::now()->format('Y'))
   ->where('users.user_type',1)
    ->select('doctor_details.first_name','doctor_details.last_name','doctor_details.email','doctor_details.main_mobile','doctor_details.d_uni_id','doctor_details.id','state.state','district.district_name','city.city','specilities.specility','users.created_at','doctor_details.pincode')->get();
   }
   
   
   if(!empty($drdata))
   {
       return response()->json(['total'=>count($drdata),'status'=>true,'data'=>$drdata]);
   }
   else
   {
       return response()->json(['status'=>0,'data'=>$drdata]);
   }
   
  
   
     } 
     
     
     
      public function doctor_listForty(Request $request)
     {
                        $drdata=0;
    if(isset($request->status))
   {
   $drdata=DB::table('doctor_details')
   ->leftjoin('users','users.userDetailsId','doctor_details.id')
   ->leftjoin('state','state.id','doctor_details.state')
   ->leftjoin('district','district.id','doctor_details.district')
   ->leftjoin('specilities','specilities.id','doctor_details.speciality')
   ->leftjoin('city','city.id','doctor_details.city')
   ->whereBetween('users.created_at',[Carbon::now()->subDays(15),Carbon::now()])
   ->where('users.status',$request->status)
   ->where('users.user_type',1)
    ->select('doctor_details.first_name','doctor_details.last_name','doctor_details.email','doctor_details.main_mobile','doctor_details.d_uni_id','doctor_details.id','state.state','district.district_name','city.city','specilities.specility','users.created_at','doctor_details.pincode')->get();
   }
   else
   {
   $drdata=DB::table('doctor_details')
   ->leftjoin('users','users.userDetailsId','doctor_details.id')
   ->leftjoin('state','state.id','doctor_details.state')
   ->leftjoin('district','district.id','doctor_details.district')
   ->leftjoin('specilities','specilities.id','doctor_details.speciality')
   ->leftjoin('city','city.id','doctor_details.city')
   ->whereBetween('users.created_at',[Carbon::now()->subDays(15),Carbon::now()])
   ->where('users.user_type',1)
    ->select('doctor_details.first_name','doctor_details.last_name','doctor_details.email','doctor_details.main_mobile','doctor_details.d_uni_id','doctor_details.id','state.state','district.district_name','city.city','specilities.specility','users.created_at','doctor_details.pincode')->get();
   }
   
   
   if(!empty($drdata))
   {
       return response()->json(['total'=>count($drdata),'status'=>true,'data'=>$drdata]);
   }
   else
   {
       return response()->json(['status'=>0,'data'=>$drdata]);
   }
   
  
   
     } 
     
     
     public function doctor_listMonth(Request $request)
     {
                        $drdata=0;
                         if(isset($request->status))
   {
   $drdata=DB::table('doctor_details')
   ->leftjoin('users','users.userDetailsId','doctor_details.id')
   ->leftjoin('state','state.id','doctor_details.state')
   ->leftjoin('district','district.id','doctor_details.district')
   ->leftjoin('city','city.id','doctor_details.city')
   ->leftjoin('specilities','specilities.id','doctor_details.speciality')
   ->whereMonth('users.created_at',Carbon::now()->format('m'))
   ->where('users.status',$request->status)
   ->where('users.user_type',1)
    ->select('doctor_details.first_name','doctor_details.last_name','doctor_details.email','doctor_details.main_mobile','doctor_details.d_uni_id','doctor_details.id','state.state','district.district_name','city.city','specilities.specility','users.created_at','doctor_details.pincode')->get();
   }
   else
   {
   $drdata=DB::table('doctor_details')
   ->leftjoin('users','users.userDetailsId','doctor_details.id')
   ->leftjoin('state','state.id','doctor_details.state')
   ->leftjoin('district','district.id','doctor_details.district')
   ->leftjoin('specilities','specilities.id','doctor_details.speciality')
   ->leftjoin('city','city.id','doctor_details.city')
   ->whereMonth('users.created_at',Carbon::now()->format('m'))
   ->where('users.user_type',1)
    ->select('doctor_details.first_name','doctor_details.last_name','doctor_details.email','doctor_details.main_mobile','doctor_details.d_uni_id','doctor_details.id','state.state','district.district_name','city.city','specilities.specility','users.created_at','doctor_details.pincode')->get();
   }
   
   
   if(!empty($drdata))
   {
       return response()->json(['total'=>count($drdata),'status'=>true,'data'=>$drdata]);
   }
   else
   {
       return response()->json(['status'=>0,'data'=>$drdata]);
   }
   
  
   
   
     }  
    
    
     public function doctor_listWeek(Request $request)
     {
                        $drdata=0;
    if(isset($request->status))
   {
   $drdata=DB::table('doctor_details')
   ->leftjoin('users','users.userDetailsId','doctor_details.id')
   ->leftjoin('state','state.id','doctor_details.state')
   ->leftjoin('specilities','specilities.id','doctor_details.speciality')
   ->leftjoin('district','district.id','doctor_details.district')
   ->leftjoin('city','city.id','doctor_details.city')
   ->whereBetween('users.created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])
   ->where('users.status',$request->status)
   ->where('users.user_type',1)
    ->select('doctor_details.first_name','doctor_details.last_name','doctor_details.email','doctor_details.main_mobile','doctor_details.d_uni_id','doctor_details.id','state.state','district.district_name','city.city','specilities.specility','users.created_at','doctor_details.pincode')->get();
   }
   else
   {
   $drdata=DB::table('doctor_details')
   ->leftjoin('users','users.userDetailsId','doctor_details.id')
   ->leftjoin('state','state.id','doctor_details.state')
   ->leftjoin('district','district.id','doctor_details.district')
   ->leftjoin('specilities','specilities.id','doctor_details.speciality')
   ->leftjoin('city','city.id','doctor_details.city')
   ->whereBetween('users.created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])
   ->where('users.user_type',1)
    ->select('doctor_details.first_name','doctor_details.last_name','doctor_details.email','doctor_details.main_mobile','doctor_details.d_uni_id','doctor_details.id','state.state','district.district_name','city.city','specilities.specility','users.created_at','doctor_details.pincode')->get();
   }
   
   
   if(!empty($drdata))
   {
       return response()->json(['total'=>count($drdata),'status'=>true,'data'=>$drdata]);
   }
   else
   {
       return response()->json(['status'=>0,'data'=>$drdata]);
   }
   
  
   
   
     }
      public function doctor_listYest(Request $request)
     {
                        $drdata=0;
    if(isset($request->status))
   {
   $drdata=DB::table('doctor_details')
   ->leftjoin('users','users.userDetailsId','doctor_details.id')
   ->leftjoin('state','state.id','doctor_details.state')
   ->leftjoin('district','district.id','doctor_details.district')
   ->leftjoin('specilities','specilities.id','doctor_details.speciality')
   ->leftjoin('city','city.id','doctor_details.city')
   ->where('users.status',$request->status)
   ->where('users.created_at','like',Carbon::yesterday()->format('Y-m-d').'%')
   ->where('users.user_type',1)
    ->select('doctor_details.first_name','doctor_details.last_name','doctor_details.email','doctor_details.main_mobile','doctor_details.d_uni_id','doctor_details.id','state.state','district.district_name','city.city','specilities.specility','users.created_at','doctor_details.pincode')->get();
   }
   else
   {
   $drdata=DB::table('doctor_details')
   ->leftjoin('users','users.userDetailsId','doctor_details.id')
   ->leftjoin('state','state.id','doctor_details.state')
   ->leftjoin('district','district.id','doctor_details.district')
   ->leftjoin('specilities','specilities.id','doctor_details.speciality')
   ->leftjoin('city','city.id','doctor_details.city')
  ->where('users.created_at','like',Carbon::yesterday()->format('Y-m-d').'%')
   ->where('users.user_type',1)
    ->select('doctor_details.first_name','doctor_details.last_name','doctor_details.email','doctor_details.main_mobile','doctor_details.d_uni_id','doctor_details.id','state.state','district.district_name','city.city','specilities.specility','users.created_at','doctor_details.pincode')->get();
   }
   
   
   if(!empty($drdata))
   {
       return response()->json(['total'=>count($drdata),'status'=>true,'data'=>$drdata]);
   }
   else
   {
       return response()->json(['status'=>0,'data'=>$drdata]);
   }
   
  
   
     }
     
     public function doctor_listToday(Request $request)
     {
                        $drdata=0;
      if(isset($request->status))
   {
   $drdata=DB::table('doctor_details')
   ->leftjoin('users','users.userDetailsId','doctor_details.id')
   ->leftjoin('state','state.id','doctor_details.state')
   ->leftjoin('district','district.id','doctor_details.district')
   ->leftjoin('specilities','specilities.id','doctor_details.speciality')
   ->leftjoin('city','city.id','doctor_details.city')
   ->where('users.status',$request->status)
   ->where('users.created_at','like',Carbon::today()->format('Y-m-d').'%')
   ->where('users.user_type',1)
  ->select('doctor_details.first_name','doctor_details.last_name','doctor_details.email','doctor_details.main_mobile','doctor_details.d_uni_id','doctor_details.id','state.state','district.district_name','city.city','specilities.specility','users.created_at','doctor_details.pincode')->get();
   }
   else
   {
   $drdata=DB::table('doctor_details')
   ->leftjoin('users','users.userDetailsId','doctor_details.id')
   ->leftjoin('state','state.id','doctor_details.state')
   ->leftjoin('district','district.id','doctor_details.district')
   ->leftjoin('specilities','specilities.id','doctor_details.speciality')
   ->leftjoin('city','city.id','doctor_details.city')
   ->where('users.created_at','like',Carbon::today()->format('Y-m-d').'%')
   ->where('users.user_type',1)
    ->select('doctor_details.first_name','doctor_details.last_name','doctor_details.email','doctor_details.main_mobile','doctor_details.d_uni_id','doctor_details.id','state.state','district.district_name','city.city','specilities.specility','users.created_at','doctor_details.pincode')->get();
   }
   
   
   if(!empty($drdata))
   {
       return response()->json(['time'=>Carbon::today()->format('Y-m-d').'%','total'=>count($drdata),'status'=>true,'data'=>$drdata]);
   }
   else
   {
       return response()->json(['status'=>0,'data'=>$drdata]);
   }
   
  
   
   
     }
     
   public function doctor_listTotal(Request $request)
     {
   
   /**
    * we are using isset coz in post argument status 0 is approved and 1 is deactivate
    * 0 is value for isset else false
    * **/
        $drdata=0;
   if(isset($request->status))
   {
   $drdata=DB::table('doctor_details')
   ->leftjoin('users','users.userDetailsId','doctor_details.id')
   ->leftjoin('specilities','specilities.id','doctor_details.speciality')
   ->leftjoin('state','state.id','doctor_details.state')
   ->leftjoin('district','district.id','doctor_details.district')
   ->leftjoin('city','city.id','doctor_details.city')
   ->where('users.status',$request->status)
   ->where('users.user_type',1)
    ->select('doctor_details.first_name','doctor_details.last_name','doctor_details.email','doctor_details.main_mobile','doctor_details.d_uni_id','doctor_details.id','state.state','district.district_name','city.city','specilities.specility','users.created_at','doctor_details.pincode')->get();
   }
   else
   {
   $drdata=DB::table('doctor_details')
   ->leftjoin('users','users.userDetailsId','doctor_details.id')
   ->leftjoin('specilities','specilities.id','doctor_details.speciality')
   ->leftjoin('state','state.id','doctor_details.state')
   ->leftjoin('district','district.id','doctor_details.district')
   ->leftjoin('city','city.id','doctor_details.city')
   ->where('users.user_type',1)
   ->select('doctor_details.first_name','doctor_details.last_name','doctor_details.email','doctor_details.main_mobile','doctor_details.d_uni_id','doctor_details.id','state.state','district.district_name','city.city','specilities.specility','users.created_at','doctor_details.pincode')->get();
   }
   
   
   if(!empty($drdata))
   {
       return response()->json(['total'=>count($drdata),'status'=>true,'data'=>$drdata]);
   }
   else
   {
       return response()->json(['status'=>0,'data'=>$drdata]);
   }
   
   
     }
     
  //patients API for Kizaku


     //patient for kizaku
                    
                    /**
                     * filter_by as week,today,this month,year,fortnight,yesterday
                     * 
                     * 
                     * */
                         public function patient_count_startYear($status,$reg=null)
     {
                        $ptdata=0;
   if(empty($reg))
   {
       $ptdata=DB::table('patient_details')
   ->join('users','users.userDetailsId','patient_details.id')
   ->whereYear('users.created_at',Carbon::now()->format('Y'))
   ->where('users.status',$status)
   ->where('users.user_type',2)
   ->count();
   }
   else
   {
       $ptdata=DB::table('patient_details')
   ->join('users','users.userDetailsId','patient_details.id')
   ->whereYear('users.created_at',Carbon::now()->format('Y'))
   ->where('users.user_type',2)
   ->count();
   }
   
   
   
   if($ptdata>0)
   {
       return $ptdata;
   }
   else
   {
       return false;
   }
   
   
     } 
     
     
     
      public function patient_count_startForty($status,$reg=null)
     {
                        $ptdata=0;
   if(empty($reg))
   {
    $ptdata=DB::table('patient_details')
   ->join('users','users.userDetailsId','patient_details.id')
   ->whereBetween('users.created_at',[Carbon::now()->subDays(15),Carbon::now()])
   ->where('users.status',$status)
   ->where('users.user_type',2)
   ->count();
   }
   else
   {
   $ptdata=DB::table('patient_details')
   ->join('users','users.userDetailsId','patient_details.id')
   ->whereBetween('users.created_at',[Carbon::now()->subDays(15),Carbon::now()])
   ->where('users.user_type',2)
   ->count();
   }

   
   
   if($ptdata>0)
   {
       return $ptdata;
   }
   else
   {
       return false;
   }
   
   
     } 
     
     
     public function patient_count_startMonth($status,$reg=null)
     {
                        $ptdata=0;
   if(empty($reg))
   {
       $ptdata=DB::table('patient_details')
   ->join('users','users.userDetailsId','patient_details.id')
   ->whereMonth('users.created_at',Carbon::now()->format('m'))
   ->where('users.status',$status)
   ->where('users.user_type',2)
   ->count();
   }
   else
   {
       $ptdata=DB::table('patient_details')
   ->join('users','users.userDetailsId','patient_details.id')
   ->whereMonth('users.created_at',Carbon::now()->format('m'))
   ->where('users.user_type',2)
   ->count();
   }
   
   
   
   if($ptdata>0)
   {
       return $ptdata;
   }
   else
   {
       return false;
   }
   
   
     }  
    
    
     public function patient_count_startWeek($status,$reg=null)
     {
                        $ptdata=0;
   if(empty($reg))
   {
      $ptdata=DB::table('patient_details')
   ->join('users','users.userDetailsId','patient_details.id')
   ->whereBetween('users.created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])
   ->where('users.status',$status)
   ->where('users.user_type',2)
   ->count();
    
   }
   else
   {
        $ptdata=DB::table('patient_details')
   ->join('users','users.userDetailsId','patient_details.id')
   ->whereBetween('users.created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])
   ->where('users.user_type',2)
   ->count();
  
   }
   
   
   if($ptdata>0)
   {
       return $ptdata;
   }
   else
   {
       return false;
   }
   
   
     }
      public function patient_count_startYest($status,$reg=null)
     {
                        $ptdata=0;
   if(empty($reg))
   {
   $ptdata=DB::table('patient_details')
   ->join('users','users.userDetailsId','patient_details.id')
   ->where('users.created_at','like',Carbon::yesterday()->format('Y-m-d').'%')
   ->where('users.status',$status)
   ->where('users.user_type',2)
   ->count(); 
   }
   else
   {
   $ptdata=DB::table('patient_details')
   ->join('users','users.userDetailsId','patient_details.id')
   ->where('users.created_at','like',Carbon::yesterday()->format('Y-m-d').'%')
   ->where('users.user_type',2)
   ->count(); 
   }

   
   
   if($ptdata>0)
   {
       return $ptdata;
   }
   else
   {
       return false;
   }
   
   
     }
     
     public function patient_count_startToday($status,$reg=null)
     {
                        $ptdata=0;
   if(empty($reg))
   {
   $ptdata=DB::table('patient_details')
   ->join('users','users.userDetailsId','patient_details.id')
   ->where('users.created_at','like',Carbon::today()->format('Y-m-d').'%')
   ->where('users.status',$status)
   ->where('users.user_type',2)
   ->count();
   }
   else
   {
    $ptdata=DB::table('patient_details')
   ->join('users','users.userDetailsId','patient_details.id')
   ->where('users.created_at','like',Carbon::today()->format('Y-m-d').'%')
   ->where('users.user_type',2)
   ->count();  
   }
   
   if($ptdata>0)
   {
       return $ptdata;
   }
   else
   {
       return false;
   }
   
   
     }
     
   public function patient_count_startTotal($status,$reg=null)
     {
                        $ptdata=0;
   if(empty($reg))
   {
   $ptdata=DB::table('patient_details')
   ->join('users','users.userDetailsId','patient_details.id')
   ->where('users.status',$status)
   ->where('users.user_type',2)
   ->count();
   }
   else
   {
    $ptdata=DB::table('patient_details')
   ->join('users','users.userDetailsId','patient_details.id')
   ->where('users.user_type',2)
   ->count();
   }
   
   
   if($ptdata>0)
   {
       return $ptdata;
   }
   else
   {
       return false;
   }
   
   
     }
     
     //approve patients for Kizaku all blow Module
     public function patient_count(Request $request)
     {
   
   $data=[];
   $data['today']=$this->patient_count_startToday(0);
   $data['year']=$this->patient_count_startYear(0);
   $data['fortnight']=$this->patient_count_startForty(0);
   $data['week']=$this->patient_count_startWeek(0);
   $data['yesterday']=$this->patient_count_startYest(0);
   $data['month']=$this->patient_count_startMonth(0);
   $data['total']=$this->patient_count_startTotal(0);
   //$data['bearerToken']=$request->bearerToken();
   $data['status']=true;
   return response()->json($data);
     }
     
     
     public function patient_count_pending(Request $request)
     {
   
   $data=[];
   $data['today']=$this->patient_count_startToday(1);
   $data['year']=$this->patient_count_startYear(1);
   $data['fortnight']=$this->patient_count_startForty(1);
   $data['week']=$this->patient_count_startWeek(1);
   $data['yesterday']=$this->patient_count_startYest(1);
   $data['month']=$this->patient_count_startMonth(1);
   $data['total']=$this->patient_count_startTotal(1);
   //$data['bearerToken']=$request->bearerToken();
   $data['status']=true;
   return response()->json($data);
     }
     
     
     public function patient_count_rejected(Request $request)
     {
   
            $data=[];
            $data['today']=$this->patient_count_startToday(3);
           $data['year']=$this->patient_count_startYear(3);
           $data['fortnight']=$this->patient_count_startForty(3);
           $data['week']=$this->patient_count_startWeek(3);
           $data['yesterday']=$this->patient_count_startYest(3);
           $data['month']=$this->patient_count_startMonth(3);
           $data['total']=$this->patient_count_startTotal(3);
   //$data['bearerToken']=$request->bearerToken();
   $data['status']=true;
   return response()->json($data);
     }
     
     
     public function patient_count_registration(Request $request)
     {
   //if we will pass two argumnt then only all registration not deppend on status
   $data=[];
   $data['today']=$this->patient_count_startToday(2,1);
   $data['year']=$this->patient_count_startYear(2,1);
   $data['fortnight']=$this->patient_count_startForty(2,1);
   $data['week']=$this->patient_count_startWeek(2,1);
   $data['yesterday']=$this->patient_count_startYest(2,1);
   $data['month']=$this->patient_count_startMonth(2,1);
   $data['total']=$this->patient_count_startTotal(2,1);
   //$data['bearerToken']=$request->bearerToken();
   $data['status']=true;
   return response()->json($data);
     }
     
     public function patient_list(Request $request)
     {
  $datalist= DB::table('patient_details')
   ->leftjoin('users','users.userDetailsId','patient_details.id')
   ->leftjoin('state','state.id','patient_details.state')
   ->leftjoin('district','district.id','patient_details.district')
 
   ->leftjoin('city','city.id','patient_details.city')
   ->where('users.status',$request->status)
   ->orderBy('users.created_at','desc')
    ->select('patient_details.first_name','users.created_at','patient_details.last_name','patient_details.email','patient_details.mobile','patient_details.p_uni_id','patient_details.id','state.state','district.district_name','city.city','patient_details.pincode')->get();
   
   if(count($datalist)>0)
   {
       $data['total']=count($datalist);
   $data['status']=true;
   $data['data']=$datalist;
   
   }
   else
   {
   $data['status']=true;
   $data['data']=$datalist;
   }

   return response()->json([$data]);
     }
     
     /**
      * patient List According to Status 
      * 
      * */
      
      /**
                     * filter_by as week,today,this month,year,fortnight,yesterday
                     * 
                     * 
                     * */
                         public function patient_listYear(Request $request)
     {
                        $ptdata=0;
    if(isset($request->status))
   {
   $ptdata=DB::table('patient_details')
   ->leftjoin('users','users.userDetailsId','patient_details.id')
   ->leftjoin('state','state.id','patient_details.state')
   ->leftjoin('district','district.id','patient_details.district')
   ->leftjoin('city','city.id','patient_details.city')
   ->whereYear('users.created_at',Carbon::now()->format('Y'))
 
   ->where('users.status',$request->status)
   ->where('users.user_type',2)
    ->select('patient_details.first_name','users.created_at','patient_details.last_name','patient_details.email','patient_details.mobile','patient_details.p_uni_id','patient_details.id','state.state','district.district_name','city.city','patient_details.pincode')->get();
   }
   else
   {
   $ptdata=DB::table('patient_details')
   ->leftjoin('users','users.userDetailsId','patient_details.id')
   ->leftjoin('state','state.id','patient_details.state')
   ->leftjoin('district','district.id','patient_details.district')
 
   ->leftjoin('city','city.id','patient_details.city')
   ->whereYear('users.created_at',Carbon::now()->format('Y'))
   ->where('users.user_type',2)
    ->select('patient_details.first_name','users.created_at','patient_details.last_name','patient_details.email','patient_details.mobile','patient_details.p_uni_id','patient_details.id','state.state','district.district_name','city.city','patient_details.pincode')->get();
   }
   
   
   if(!empty($ptdata))
   {
       return response()->json(['total'=>count($ptdata),'status'=>true,'data'=>$ptdata]);
   }
   else
   {
       return response()->json(['status'=>0,'data'=>$ptdata]);
   }
   
  
   
     } 
     
     
     
      public function patient_listForty(Request $request)
     {
                        $ptdata=0;
    if(isset($request->status))
   {
   $ptdata=DB::table('patient_details')
   ->leftjoin('users','users.userDetailsId','patient_details.id')
   ->leftjoin('state','state.id','patient_details.state')
   ->leftjoin('district','district.id','patient_details.district')
 
   ->leftjoin('city','city.id','patient_details.city')
   ->whereBetween('users.created_at',[Carbon::now()->subDays(15),Carbon::now()])
   ->where('users.status',$request->status)
   ->where('users.user_type',2)
    ->select('patient_details.first_name','users.created_at','patient_details.last_name','patient_details.email','patient_details.mobile','patient_details.p_uni_id','patient_details.id','state.state','district.district_name','city.city','patient_details.pincode')->get();
   }
   else
   {
   $ptdata=DB::table('patient_details')
   ->leftjoin('users','users.userDetailsId','patient_details.id')
   ->leftjoin('state','state.id','patient_details.state')
   ->leftjoin('district','district.id','patient_details.district')
 
   ->leftjoin('city','city.id','patient_details.city')
   ->whereBetween('users.created_at',[Carbon::now()->subDays(15),Carbon::now()])
   ->where('users.user_type',2)
    ->select('patient_details.first_name','users.created_at','patient_details.last_name','patient_details.email','patient_details.mobile','patient_details.p_uni_id','patient_details.id','state.state','district.district_name','city.city','patient_details.pincode')->get();
   }
   
   
   if(!empty($ptdata))
   {
       return response()->json(['total'=>count($ptdata),'status'=>true,'data'=>$ptdata]);
   }
   else
   {
       return response()->json(['status'=>0,'data'=>$ptdata]);
   }
   
  
   
     } 
     
     
     public function patient_listMonth(Request $request)
     {
                        $ptdata=0;
                         if(isset($request->status))
   {
   $ptdata=DB::table('patient_details')
   ->leftjoin('users','users.userDetailsId','patient_details.id')
   ->leftjoin('state','state.id','patient_details.state')
   ->leftjoin('district','district.id','patient_details.district')
   ->leftjoin('city','city.id','patient_details.city')
 
   ->whereMonth('users.created_at',Carbon::now()->format('m'))
   ->where('users.status',$request->status)
   ->where('users.user_type',2)
    ->select('patient_details.first_name','users.created_at','patient_details.last_name','patient_details.email','patient_details.mobile','patient_details.p_uni_id','patient_details.id','state.state','district.district_name','city.city','patient_details.pincode')->get();
   }
   else
   {
   $ptdata=DB::table('patient_details')
   ->leftjoin('users','users.userDetailsId','patient_details.id')
   ->leftjoin('state','state.id','patient_details.state')
   ->leftjoin('district','district.id','patient_details.district')
 
   ->leftjoin('city','city.id','patient_details.city')
   ->whereMonth('users.created_at',Carbon::now()->format('m'))
   ->where('users.user_type',2)
    ->select('patient_details.first_name','users.created_at','patient_details.last_name','patient_details.email','patient_details.mobile','patient_details.p_uni_id','patient_details.id','state.state','district.district_name','city.city','patient_details.pincode')->get();
   }
   
   
   if(!empty($ptdata))
   {
       return response()->json(['total'=>count($ptdata),'status'=>true,'data'=>$ptdata]);
   }
   else
   {
       return response()->json(['status'=>0,'data'=>$ptdata]);
   }
   
  
   
   
     }  
    
    
     public function patient_listWeek(Request $request)
     {
                        $ptdata=0;
    if(isset($request->status))
   {
   $ptdata=DB::table('patient_details')
   ->leftjoin('users','users.userDetailsId','patient_details.id')
   ->leftjoin('state','state.id','patient_details.state')
 
   ->leftjoin('district','district.id','patient_details.district')
   ->leftjoin('city','city.id','patient_details.city')
   ->whereBetween('users.created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])
   ->where('users.status',$request->status)
   ->where('users.user_type',2)
    ->select('patient_details.first_name','users.created_at','patient_details.last_name','patient_details.email','patient_details.mobile','patient_details.p_uni_id','patient_details.id','state.state','district.district_name','city.city','patient_details.pincode')->get();
   }
   else
   {
   $ptdata=DB::table('patient_details')
   ->leftjoin('users','users.userDetailsId','patient_details.id')
   ->leftjoin('state','state.id','patient_details.state')
   ->leftjoin('district','district.id','patient_details.district')
 
   ->leftjoin('city','city.id','patient_details.city')
   ->whereBetween('users.created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])
   ->where('users.user_type',2)
    ->select('patient_details.first_name','users.created_at','patient_details.last_name','patient_details.email','patient_details.mobile','patient_details.p_uni_id','patient_details.id','state.state','district.district_name','city.city','patient_details.pincode')->get();
   }
   
   
   if(!empty($ptdata))
   {
       return response()->json(['total'=>count($ptdata),'status'=>true,'data'=>$ptdata]);
   }
   else
   {
       return response()->json(['status'=>0,'data'=>$ptdata]);
   }
   
  
   
   
     }
      public function patient_listYest(Request $request)
     {
                        $ptdata=0;
    if(isset($request->status))
   {
   $ptdata=DB::table('patient_details')
   ->leftjoin('users','users.userDetailsId','patient_details.id')
   ->leftjoin('state','state.id','patient_details.state')
   ->leftjoin('district','district.id','patient_details.district')
 
   ->leftjoin('city','city.id','patient_details.city')
   ->where('users.status',$request->status)
   ->where('users.created_at','like',Carbon::yesterday()->format('Y-m-d').'%')
   ->where('users.user_type',2)
    ->select('patient_details.first_name','users.created_at','patient_details.last_name','patient_details.email','patient_details.mobile','patient_details.p_uni_id','patient_details.id','state.state','district.district_name','city.city','patient_details.pincode')->get();
   }
   else
   {
   $ptdata=DB::table('patient_details')
   ->leftjoin('users','users.userDetailsId','patient_details.id')
   ->leftjoin('state','state.id','patient_details.state')
   ->leftjoin('district','district.id','patient_details.district')
 
   ->leftjoin('city','city.id','patient_details.city')
   ->where('users.created_at','like',Carbon::yesterday()->format('Y-m-d').'%')
   ->where('users.user_type',2)
    ->select('patient_details.first_name','users.created_at','patient_details.last_name','patient_details.email','patient_details.mobile','patient_details.p_uni_id','patient_details.id','state.state','district.district_name','city.city','patient_details.pincode')->get();
   }
   
   
   if(!empty($ptdata))
   {
       return response()->json(['total'=>count($ptdata),'status'=>true,'data'=>$ptdata]);
   }
   else
   {
       return response()->json(['status'=>0,'data'=>$ptdata]);
   }
   
  
   
     }
     
     public function patient_listToday(Request $request)
     {
                        $ptdata=0;
      if(isset($request->status))
   {
   $ptdata=DB::table('patient_details')
   ->leftjoin('users','users.userDetailsId','patient_details.id')
   ->leftjoin('state','state.id','patient_details.state')
   ->leftjoin('district','district.id','patient_details.district')
 
   ->leftjoin('city','city.id','patient_details.city')
   ->where('users.status',$request->status)
   ->where('users.created_at','like',Carbon::today()->format('Y-m-d').'%')
   ->where('users.user_type',2)
  ->select('patient_details.first_name','users.created_at','patient_details.last_name','patient_details.email','patient_details.mobile','patient_details.p_uni_id','patient_details.id','state.state','district.district_name','city.city','patient_details.pincode')->get();
   }
   else
   {
   $ptdata=DB::table('patient_details')
   ->leftjoin('users','users.userDetailsId','patient_details.id')
   ->leftjoin('state','state.id','patient_details.state')
   ->leftjoin('district','district.id','patient_details.district')
 
   ->leftjoin('city','city.id','patient_details.city')
   ->where('users.created_at','like',Carbon::today()->format('Y-m-d').'%')
   ->where('users.user_type',2)
    ->select('patient_details.first_name','users.created_at','patient_details.last_name','patient_details.email','patient_details.mobile','patient_details.p_uni_id','patient_details.id','state.state','district.district_name','city.city','patient_details.pincode')->get();
   }
   
   
   if(!empty($ptdata))
   {
       return response()->json(['total'=>count($ptdata),'status'=>true,'data'=>$ptdata]);
   }
   else
   {
       return response()->json(['status'=>0,'data'=>$ptdata]);
   }
   
  
   
   
     }
     
   public function patient_listTotal(Request $request)
     {
   
   /**
    * we are using isset coz in post argument status 0 is approved and 1 is deactivate
    * 0 is value for isset else false
    * **/
        $ptdata=0;
   if(isset($request->status))
   {
   $ptdata=DB::table('patient_details')
   ->leftjoin('users','users.userDetailsId','patient_details.id')
 
   ->leftjoin('state','state.id','patient_details.state')
   ->leftjoin('district','district.id','patient_details.district')
   ->leftjoin('city','city.id','patient_details.city')
   ->where('users.status',$request->status)
   ->where('users.user_type',2)
    ->select('patient_details.first_name','users.created_at','patient_details.last_name','patient_details.email','patient_details.mobile','patient_details.p_uni_id','patient_details.id','state.state','district.district_name','city.city','patient_details.pincode')->get();
   }
   else
   {
   $ptdata=DB::table('patient_details')
   ->leftjoin('users','users.userDetailsId','patient_details.id')
 
   ->leftjoin('state','state.id','patient_details.state')
   ->leftjoin('district','district.id','patient_details.district')
   ->leftjoin('city','city.id','patient_details.city')
   ->where('users.user_type',2)
   ->select('patient_details.first_name','users.created_at','patient_details.last_name','patient_details.email','patient_details.mobile','patient_details.p_uni_id','patient_details.id','state.state','district.district_name','city.city','patient_details.pincode')->get();
   }
   
   
   if(!empty($ptdata))
   {
       return response()->json(['total'=>count($ptdata),'status'=>true,'data'=>$ptdata]);
   }
   else
   {
       return response()->json(['status'=>0,'data'=>$ptdata]);
   }
   
   
     }
     

     //business filter

     function business_filter(Request $request)
                    {

       // return response()->json($request->all());exit();
       $from = Carbon::parse($request->start_date)->format('Y-m-d');
$to = Carbon::parse($request->end_date)->format('Y-m-d');

//$from = Carbon($request->start_date);
//$to = Carbon($request->end_date);

 $data['total']=DB::table('business_register')
 ->where('b_id',$request->btype)
 ->where('country_id',$request->country)
 ->where('city_id',$request->city)
 ->where('state_id',$request->state)
 ->where('pincode',$request->pincode)
 ->where('district',$request->district)
 ->whereBetween('created_at', [$from,$to])
 ->count();


 $data['today']=DB::table('business_register')
 ->where('b_id',$request->btype)
 ->where('country_id',$request->country)
 ->where('city_id',$request->city)
 ->where('state_id',$request->state)
 ->where('pincode',$request->pincode)
 ->where('district',$request->district)
 ->where('created_at','like',Carbon::today()->format('Y-m-d').'%')
 ->whereBetween('created_at', [$from,$to])->count();

 $data['yesterday']=DB::table('business_register')
 ->where('b_id',$request->btype)
 ->where('country_id',$request->country)
 ->where('city_id',$request->city)
 ->where('state_id',$request->state)
 ->where('pincode',$request->pincode)
 ->where('district',$request->district)
 ->where('created_at','like',Carbon::yesterday()->format('Y-m-d').'%')
 ->whereBetween('created_at', [$from,$to])->count();


 $data['fortnight']=DB::table('business_register')
 ->where('b_id',$request->btype)
 ->where('country_id',$request->country)
 ->where('city_id',$request->city)
 ->where('state_id',$request->state)
 ->where('pincode',$request->pincode)
 ->where('district',$request->district)
 ->whereBetween('created_at',[Carbon::now()->subDays(15),Carbon::now()])
 ->whereBetween('created_at', [$from,$to])->count();

 $data['year']=DB::table('business_register')
 ->where('country_id',$request->country)
 ->where('city_id',$request->city)
 ->where('state_id',$request->state)
 ->where('pincode',$request->pincode)
 ->where('district',$request->district)
 ->where('b_id',$request->btype)
 ->whereYear('created_at',Carbon::now()->format('Y'))
 ->whereBetween('created_at', [$from,$to])->count();


 $data['week']=DB::table('business_register')
 ->where('country_id',$request->country)
 ->where('city_id',$request->city)
 ->where('state_id',$request->state)
 ->where('pincode',$request->pincode)
 ->where('district',$request->district)
 ->where('b_id',$request->btype)
 ->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])
 ->whereBetween('created_at', [$from,$to])->count();


 $data['month']=DB::table('business_register')
 ->where('b_id',$request->btype)
 ->where('country_id',$request->country)
 ->where('city_id',$request->city)
 ->where('state_id',$request->state)
 ->where('pincode',$request->pincode)
 ->where('district',$request->district)
 ->whereMonth('created_at',Carbon::now()->format('m'))
 ->whereBetween('created_at', [$from,$to])->count();

                         if (!empty($data)) {
     return response()->json(['status'=>true,'data'=>$data]);
 }
 else
 {
     return response()->json(['status'=>false,'data'=>$data]);
 }
                    }
    
    
    

      function doctor_filter(Request $request)
                    {

       // return response()->json($request->all());exit();
       $from = Carbon::parse($request->start_date)->format('Y-m-d');
$to = Carbon::parse($request->end_date)->format('Y-m-d');

//$from = Carbon($request->start_date);
//$to = Carbon($request->end_date);

 $data['total']=DB::table('doctor_details')
 ->join('users','users.userDetailsId','doctor_details.id') 
 ->where('doctor_details.city',$request->city)->where('users.user_type',1)
 ->where('doctor_details.state',$request->state)
 ->where('doctor_details.pincode',$request->pincode)
 ->where('doctor_details.district',$request->district)
 ->whereBetween('users.created_at', [$from,$to])
 ->count();


 $data['today']=DB::table('doctor_details')
 ->join('users','users.userDetailsId','doctor_details.id')
 ->where('doctor_details.city',$request->city)->where('users.user_type',1)
 ->where('doctor_details.state',$request->state)
 ->where('doctor_details.pincode',$request->pincode)
 ->where('doctor_details.district',$request->district)
 ->where('users.created_at','like',Carbon::today()->format('Y-m-d').'%')
 ->whereBetween('users.created_at', [$from,$to])->count();

 $data['yesterday']=DB::table('doctor_details')
 ->join('users','users.userDetailsId','doctor_details.id')
 ->where('doctor_details.city',$request->city)->where('users.user_type',1)
 ->where('doctor_details.state',$request->state)
 ->where('doctor_details.pincode',$request->pincode)
 ->where('doctor_details.district',$request->district)
 ->where('users.created_at','like',Carbon::yesterday()->format('Y-m-d').'%')
 ->whereBetween('users.created_at', [$from,$to])->count();


 $data['fortnight']=DB::table('doctor_details')
 ->join('users','users.userDetailsId','doctor_details.id')
 ->where('doctor_details.city',$request->city)->where('users.user_type',1)
 ->where('doctor_details.state',$request->state)
 ->where('doctor_details.pincode',$request->pincode)
->where('doctor_details.district',$request->district)
 ->whereBetween('users.created_at',[Carbon::now()->subDays(15),Carbon::now()])
 ->whereBetween('users.created_at', [$from,$to])->count();

 $data['year']=DB::table('doctor_details')
 ->join('users','users.userDetailsId','doctor_details.id')
 ->where('doctor_details.city',$request->city)->where('users.user_type',1)
 ->where('doctor_details.state',$request->state)
 ->where('doctor_details.pincode',$request->pincode)
->where('doctor_details.district',$request->district)
 ->whereYear('users.created_at',Carbon::now()->format('Y'))
 ->whereBetween('users.created_at', [$from,$to])->count();


 $data['week']=DB::table('doctor_details')
 ->join('users','users.userDetailsId','doctor_details.id')
 ->where('doctor_details.city',$request->city)->where('users.user_type',1)
 ->where('doctor_details.state',$request->state)
 ->where('doctor_details.pincode',$request->pincode)
->where('doctor_details.district',$request->district)
 ->whereBetween('users.created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])
 ->whereBetween('users.created_at', [$from,$to])->count();


 $data['month']=DB::table('doctor_details')
 ->join('users','users.userDetailsId','doctor_details.id')
 ->where('doctor_details.city',$request->city)->where('users.user_type',1)
 ->where('doctor_details.state',$request->state)
 ->where('doctor_details.pincode',$request->pincode)
->where('doctor_details.district',$request->district)
 ->whereMonth('users.created_at',Carbon::now()->format('m'))
 ->whereBetween('users.created_at', [$from,$to])->count();

                         if (!empty($data)) {
     return response()->json(['status'=>true,'data'=>$data]);
 }
 else
 {
     return response()->json(['status'=>false,'data'=>$data]);
 }
                    }
    
          
    
 
       function patient_filter(Request $request)
                    {

       // return response()->json($request->all());exit();
       $from = Carbon::parse($request->start_date)->format('Y-m-d');
$to = Carbon::parse($request->end_date)->format('Y-m-d');

//$from = Carbon($request->start_date);
//$to = Carbon($request->end_date);

 $data['total']=DB::table('patient_details')
 ->join('users','users.userDetailsId','patient_details.id') 
 ->where('patient_details.city',$request->city)->where('users.user_type',2)
 ->where('patient_details.state',$request->state)
 ->where('patient_details.pincode',$request->pincode)
 ->where('patient_details.district',$request->district)
 ->whereBetween('users.created_at', [$from,$to])
 ->count();


 $data['today']=DB::table('patient_details')
 ->join('users','users.userDetailsId','patient_details.id')
 ->where('patient_details.city',$request->city)->where('users.user_type',2)
 ->where('patient_details.state',$request->state)
 ->where('patient_details.pincode',$request->pincode)
 ->where('patient_details.district',$request->district)
 ->where('users.created_at','like',Carbon::today()->format('Y-m-d').'%')
 ->whereBetween('users.created_at', [$from,$to])->count();

 $data['yesterday']=DB::table('patient_details')
 ->join('users','users.userDetailsId','patient_details.id')
 ->where('patient_details.city',$request->city)->where('users.user_type',2)
 ->where('patient_details.state',$request->state)
 ->where('patient_details.pincode',$request->pincode)
 ->where('patient_details.district',$request->district)
 ->where('users.created_at','like',Carbon::yesterday()->format('Y-m-d').'%')
 ->whereBetween('users.created_at', [$from,$to])->count();


 $data['fortnight']=DB::table('patient_details')
 ->join('users','users.userDetailsId','patient_details.id')
 ->where('patient_details.city',$request->city)->where('users.user_type',2)
 ->where('patient_details.state',$request->state)
 ->where('patient_details.pincode',$request->pincode)
->where('patient_details.district',$request->district)
 ->whereBetween('users.created_at',[Carbon::now()->subDays(25),Carbon::now()])
 ->whereBetween('users.created_at', [$from,$to])->count();

 $data['year']=DB::table('patient_details')
 ->join('users','users.userDetailsId','patient_details.id')
 ->where('patient_details.city',$request->city)->where('users.user_type',2)
 ->where('patient_details.state',$request->state)
 ->where('patient_details.pincode',$request->pincode)
->where('patient_details.district',$request->district)
 ->whereYear('users.created_at',Carbon::now()->format('Y'))
 ->whereBetween('users.created_at', [$from,$to])->count();


 $data['week']=DB::table('patient_details')
 ->join('users','users.userDetailsId','patient_details.id')
 ->where('patient_details.city',$request->city)->where('users.user_type',2)
 ->where('patient_details.state',$request->state)
 ->where('patient_details.pincode',$request->pincode)
->where('patient_details.district',$request->district)
 ->whereBetween('users.created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])
 ->whereBetween('users.created_at', [$from,$to])->count();


 $data['month']=DB::table('patient_details')
 ->join('users','users.userDetailsId','patient_details.id')
 ->where('patient_details.city',$request->city)->where('users.user_type',2)
 ->where('patient_details.state',$request->state)
 ->where('patient_details.pincode',$request->pincode)
->where('patient_details.district',$request->district)
 ->whereMonth('users.created_at',Carbon::now()->format('m'))
 ->whereBetween('users.created_at', [$from,$to])->count();

                         if (!empty($data)) {
     return response()->json(['status'=>true,'data'=>$data]);
 }
 else
 {
     return response()->json(['status'=>false,'data'=>$data]);
 }
                    }


//forgot password

                      public function forgot_password_otp(Request $request)
  {

    $input = $request->all();
    $rules = array(
        'email' => "required|email",
    );
    $validator = Validator::make($input, $rules);
    if ($validator->fails()) {
        $arr = array("status" => 400, "message" => $validator->errors()->first(), "data" => array());
    } else {

        $user = User::where('email',$request->email)->first();
                    if($user)
                    {
                        
                        $otp=rand(1000,9999);
                        //Session::put('otp',$otp);
                         DB::insert('insert into otp_verify (email,otp) values(?,?)',[$request->email,md5($otp)]);
                        
       $msg = "<DOCTYPE html>
                    <html lang='en-US'>         
                        <head>
                            <meta charset='utf-8'>
                        </head>
                        <body>
                            <center>
                            </center>
                                <img src='".env('APP_URL')."./image/has.png'>
                                <br><br>
                                <h4>Kindly Use this OTP and click on the link</h3>
                                <h2>'".$otp."'</h2>
                                
                                <a href='".url('/otp_verify')."'>Reset Password</a>
                                <p>Always yours</p>
                                <p>Team Hasptal</p>
                        </body>
                    </html>";


$data['personalizations'] = array("0" => array('to' => array('0' => array('email' => $request->email))));

        $data['from'] = array('email' => 'system@haspatal.com');

        $data['subject'] = "Forgot Password !";
        $data['content'] = array("0" =>array('type' =>' text/html','value' => $msg ));

        //echo json_encode($data);exit();

        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://api.sendgrid.com/v3/mail/send",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => json_encode($data),
          CURLOPT_HTTPHEADER => array(
            "authorization: Bearer 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",
            "cache-control: no-cache",
            "content-type: application/json",
            "postman-token: b8c0781f-af48-90cc-1c5b-76f80e0b4be0"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          return response()->json(['status'=>false,'data'=>$err]);
        } else {
          return response()->json(['status'=>true,'E-Mailed on your ID Check there']);
        }
                    }
                    else
                    {
                       // Session::put('error','User Not found');
                         return response()->json(['status'=>false,'User not found']);
                    }
    }
    //return redirect('/otp');
  }
  
            function verify_otp(Request $request)
            {
                $input = $request->all();
    $rules = array(
        'otp' => "required",
    );
    $validator = Validator::make($input, $rules);
    if ($validator->fails()) {
        $arr = array("status" => 400, "message" => $validator->errors()->first(), "data" => array());
    }
    
    //(int)$otp=Session::get('otp');
    
            $data=DB::table('otp_verify')->select('id','otp','email')->where('otp',md5($request->otp))->first();
            
            if(!empty($data))
            {
               // var_dump($data);
           // return response()->json(['data'=>$data]);exit();
                if(md5($request->otp)==$data->otp)
                        
                        
                        var_dump($request->all());
                        $response_data = array();
                          if ($request->password != '' && $request->c_password != '') {
      //echo "string1111"; exit;
      $pass['password'] = bcrypt($request->password);
      //$user = User::findOrFail(Auth::user()->email);
      $data1=DB::table('users')->where('email',$data->email)->update($pass);
      var_dump($data1);
      if($data1)
      {
          $userDetails=DB::table('users')->where('email',$data->email)->first();
          //trash data from otp_verify
         DB::table('otp_verify')->where('id',$data->id)->delete();
          
              
    $response_data['status'] = true;
    $response_data['message'] = 'User details was successfully updated.';
    return redirect('/success_password');
      }
      else
      {
              $response_data['status'] = false;
    $response_data['message'] = 'User details faild to uo update.';
    return redirect('/success_password');
      }
      
     
    }

                 return response()->json($response_data);

            }
            else
            
            {
                return response()->json(['status'=>false,'message'=>'OTP Invalid']);
            }
            
            }

}

