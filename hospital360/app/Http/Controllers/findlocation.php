<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateHaspatal_360_registerRequest;
use App\Http\Requests\API\CreateStateAPIRequest;
use App\Http\Requests\API\UpdateStateAPIRequest;
use App\Models\State;
use App\Repositories\StateRepository;
use App\Repositories\Business_registerRepository;
use Illuminate\Http\Request;
use App\Models\Service_details;
use App\Models\Your_coverage_360;
use App\Models\Haspatal_360_register;
use App\coupon;
use App\User;
use App\Models\City;
use App\couponrate;
use App\Models\Create_offer_360;
use App\Models\Business_register;
use App\Http\Controllers\AppBaseController;
use Response;
use DB;

/**
 * Class PatientsController
 * @package App\Http\Controllers\API
 */

class findlocation extends Controller
{
    //
    public function pharmacycodesearch(Request $request)
    {
        $data = Business_register::select('*')->where('b_id', $request->input('role_id'))->where('business_registerID', $request->input('business_registerID'))->where('admin_status',"1")->where('your_coverage_status',"1")->get();

        foreach ($data as $Business_register) {
            $user_id = $Business_register->user_id;
            $path = [
              "b_lic" => 'https://app.kizakuhaspatal.co.in/media/b_lic/' . $Business_register->b_lic,
              "b_card_pic" =>
              'https://app.kizakuhaspatal.co.in/media/b_card_pic/' . $Business_register->b_card_pic,
              "shop_pic" =>
              'https://app.kizakuhaspatal.co.in/media/shop_pic/' . $Business_register->shop_pic,
          ];
          $Business_register['path']=$path;
            $users = DB::table('users')
                ->where('id', $user_id)
                ->orderBy('id', 'desc')
                ->first();
                $create_offer_360s = Create_offer_360::select('*')->where('user_id', $user_id)
                ->get();

                $myFavourite1 = DB::table('my_favourite')->where('pharmacy_id',$Business_register->id)->where('patient_id',$request->input('patient_id'))->first();
                if($myFavourite1){
                /*echo "<pre>";
                print_r($myFavourite1->patient_id); exit;*/
                if(isset($myFavourite1->pharmacy_id) == $Business_register->id && isset($myFavourite1->patient_id) == $request->input('patient_id'))
                {
                    
                    $Business_register['myFavourite1_status']=$myFavourite1->status;
                }else{
                    //return $this->sendResponse('', 'My Favourite alredy add successfully');
                    $Business_register['myFavourite1_status']=0;
                }
            }
            else{
                   
                $Business_register['myFavourite1_status']=0;
            }
                $Business_register['user']=$users;
                $Business_register['offers']=$create_offer_360s;
            if ($users) {
                $role_id = $users->role_id;
                $role = DB::table('couponrates')
                    ->where('category_id', $role_id)
                    ->orderBy('id', 'desc')
                    ->first();
                if ($role) {
                    $Business_register['type'] = $role->name;
                  
                    
                }
            }
        }
      
        return response()->json(["data" => $data,"message"=>"Retireved Sucessfully"]);
    }
    public function pharmacylocationSearch(Request $request)
    {
        $data = Business_register::select('*')->where('b_id', $request->input('role_id'))->where('state_id', $request->input('state_id'))->where('city_id', $request->input('city_id'))->where('admin_status',"1")->where('your_coverage_status',"1")->get();

        foreach ($data as $Business_register) {
            $user_id = $Business_register->user_id;
            $path = [
                "b_lic" => 'https://app.kizakuhaspatal.co.in/media/b_lic/' . $Business_register->b_lic,
                "b_card_pic" =>
                'https://app.kizakuhaspatal.co.in/media/b_card_pic/' . $Business_register->b_card_pic,
                "shop_pic" =>
                'https://app.kizakuhaspatal.co.in/media/shop_pic/' . $Business_register->shop_pic,
            ];
          $Business_register['path']=$path;
            $users = DB::table('users')
                ->where('id', $user_id)
                ->orderBy('id', 'desc')
                ->first();
                $create_offer_360s = Create_offer_360::select('*')->where('user_id', $user_id)
                ->get();

                $myFavourite1 = DB::table('my_favourite')->where('pharmacy_id',$Business_register->id)->where('patient_id',$request->input('patient_id'))->first();
                /*echo "<pre>";
                print_r($myFavourite1->patient_id); exit;*/
                if($myFavourite1){
                    if(isset($myFavourite1->pharmacy_id) == $Business_register->id && isset($myFavourite1->patient_id) == $request->input('patient_id'))
                    {
                        
                        $Business_register['myFavourite1_status']=$myFavourite1->status;
                    }else{
                        //return $this->sendResponse('', 'My Favourite alredy add successfully');
                        $Business_register['myFavourite1_status']=0;
                    }
                }else{
                   
                    $Business_register['myFavourite1_status']=0;
                }
            

                $Business_register['user']=$users;
                $Business_register['offers']=$create_offer_360s;
            if ($users) {
                $role_id = $users->role_id;
                $role = DB::table('couponrates')
                    ->where('category_id', $role_id)
                    ->orderBy('id', 'desc')
                    ->first();
                if ($role) {
                    $Business_register['type'] = $role->name;
                  
                    
                }
            }
        }
      
        return response()->json(["data" => $data,"message"=>"Retireved Sucessfully"]);
    }
}
