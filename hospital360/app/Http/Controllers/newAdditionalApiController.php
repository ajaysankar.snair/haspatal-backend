<?php
namespace App\Http\Controllers;
use App\Http\Requests\CreateHaspatal_360_registerRequest;
use App\Http\Requests\API\CreateStateAPIRequest;
use App\Http\Requests\API\UpdateStateAPIRequest;
use App\Models\State;
use App\Repositories\StateRepository;
use App\Repositories\Business_registerRepository;
use Illuminate\Http\Request;
use App\Models\Service_details;
use App\Models\Your_coverage_360;
use App\Models\Haspatal_360_register;
use App\coupon;
use App\User;
use App\Models\City;
use App\couponrate;
use App\Models\Business_register;
use App\Http\Controllers\AppBaseController;
use Response;
use DB;

class AdditionalApiController extends Controller
{
       /** @var  Business_registerRepository */
       private $businessRegisterRepository;

       public function __construct(Business_registerRepository $businessRegisterRepo)
       {
           $this->businessRegisterRepository = $businessRegisterRepo;
       }
   
       /**
        * @param Request $request
        * @return Response
        *
        * @SWG\Get(
        *      path="/businessRegisters",
        *      summary="Get a listing of the Business_registers.",
        *      tags={"Business_register"},
        *      description="Get all Business_registers",
        *      produces={"application/json"},
        *      @SWG\Response(
        *          response=200,
        *          description="successful operation",
        *          @SWG\Schema(
        *              type="object",
        *              @SWG\Property(
        *                  property="success",
        *                  type="boolean"
        *              ),
        *              @SWG\Property(
        *                  property="data",
        *                  type="array",
        *                  @SWG\Items(ref="#/definitions/Business_register")
        *              ),
        *              @SWG\Property(
        *                  property="message",
        *                  type="string"
        *              )
        *          )
        *      )
        * )
        */
    //
    public function serviceType()
    {
        $data = Service_details::select('id', 'service')->get();
        if($data){
        $Service_details['success']=true;
        $Service_details['message']="Date Fetched  successful";
        $Service_details['data']=$data;
        
    }
       
        return response()->json(  $Service_details,200);
    }
    public function getsate()
    {
        $data = State::select('id', 'state')->get();
        if($data){
        $State['success']=true;
        $State['message']="Date Fetched  successful";
        $State['state_data']=$data;
        
    }
       
        return response()->json(  $State,200);
    }
     public function city_list(Request $request)
     
    {
       $state_id=  $request->input('state_id');
         if ($state_id == 0) {
            $result = City::orderBy('city','ASC')->get();
        }else{

            $result = City::where('state_name', $state_id)->orderBy('city','ASC')->get();
        }
  
       if ($result) {
            return response()->json(["status" => true,"massage"=>"City retrieved successfully","data" => $result]);
        }else{
            return response()->json(["status" => false,"data" => $result]);
        }
    }
    public function districtlist()
    {
        $data = State::select('id', 'state')->get();
        if($data){
        $State['success']=true;
        $State['message']="Date Fetched  successful";
        $State['state_data']=$data;
        
    }       
        return response()->json(  $State,200);
    }
    


          public function validateRegistration(Request $obj)
          {
                 $email = $obj->email;
                 $mo_prefix=$obj->mo_prefix;
                 $mobile_no=$obj->mobile_no;


                 $email=explode('=',$email);
                 $email= $email[1];
                 //echo$email;


                  $mo_prefix=explode('=',$mo_prefix);
                 $mo_prefix= $mo_prefix[1];
                 //echo$mo_prefix;

                  $mobile_no=explode('=',$mobile_no);
                 $mobile_no= $mobile_no[1];
                //echo$mobile_no;



                    //print_r($obj->email);
               // $checkedData=Haspatal_360_register::find(3698745210,'mobile_no');

                        $where=['mobile_no'=>$mobile_no];
                        $where2=['email'=>$email];
               // $where=['mobile_no'=>9874563210,'mo_prefix'=>'+91','email'=>'raju103@gmaili.com'];
            $results = DB::table('haspatal_360_register')->where($where)->orWhere($where2)->get();
              // print_r($results);

                              // echo count($results);
                if(count($results)>0)
                {

                     //$data['results']=$results;
                $data['success']=true;
                $data['message']="User Already Registered";
                return response()->json($data,200);
               
                
               }
               else
               {
                     $data['success']=false;
                $data['message']="Not Registered";

                return response()->json($data,200);
               }

          }   


     public function newreg(Request $request)
    {
        
        $book =  DB::table('business_register')->where('user_id', $request->user_id)->orderBy('user_id', 'desc')->first();
       /* echo "<pre>";
        print_r($book); exit;*/

        if(!empty($book)){
            $input = $request->all();
            $id = $book->id;
            if($request->hasfile('b_lic'))
            {
                $image1 = $request->file('b_lic');
                $extension1 = $image1->getClientOriginalExtension(); // getting image extension
                $filename1 =time().'__'.$image1->getClientOriginalName();
                $path1 = public_path('/media/b_lic/');
                $image1->move($path1, $filename1);
                $input['b_lic'] = $filename1;
            }
            if($request->hasfile('shop_pic'))
            {
                $image1 = $request->file('shop_pic');
                $extension1 = $image1->getClientOriginalExtension(); // getting image extension
                $filename1 =time().'__'.$image1->getClientOriginalName();
                $path1 = public_path('/media/shop_pic/');
                $image1->move($path1, $filename1);
                $input['shop_pic'] = $filename1;
            }
            if($request->hasfile('b_card_pic'))
            {
                $image1 = $request->file('b_card_pic');
                $extension1 = $image1->getClientOriginalExtension(); // getting image extension
                $filename1 =time().'__'.$image1->getClientOriginalName();
                $path1 = public_path('/media/b_card_pic/');
                $image1->move($path1, $filename1);
                $input['b_card_pic'] = $filename1;
            }
      
            $businessRegister = $this->businessRegisterRepository->update($input, $id);
            $businessRegister['district']=$request->input('district');

        }

        if(empty($book))
        {
            $input = $request->all();
          //  $input['user_id']=$request->input('user_id');
            $input['created_by'] = $request->user_id;
            $input['updated_by'] = $request->user_id;
            if($request->hasfile('b_lic'))
            {
                $image1 = $request->file('b_lic');
                $extension1 = $image1->getClientOriginalExtension(); // getting image extension
                $filename1 =time().'__'.$image1->getClientOriginalName();
                $path1 = public_path('/media/b_lic/');
                $image1->move($path1, $filename1);
                $input['b_lic'] = $filename1;
            }
            if($request->hasfile('shop_pic'))
            {
                $image1 = $request->file('shop_pic');
                $extension1 = $image1->getClientOriginalExtension(); // getting image extension
                $filename1 =time().'__'.$image1->getClientOriginalName();
                $path1 = public_path('/media/shop_pic/');
                $image1->move($path1, $filename1);
                $input['shop_pic'] = $filename1;
            }
            if($request->hasfile('b_card_pic'))
            {
                $image1 = $request->file('b_card_pic');
                $extension1 = $image1->getClientOriginalExtension(); // getting image extension
                $filename1 =time().'__'.$image1->getClientOriginalName();
                $path1 = public_path('/media/b_card_pic/');
                $image1->move($path1, $filename1);
                $input['b_card_pic'] = $filename1;
            }
            $businessRegister = $this->businessRegisterRepository->create($input);
            $businessRegister['district']=$request->input('district');

        }
        // return $this->sendResponse($businessRegister->toArray(), 'Business Register saved successfully');
    //   return response()->json(  $businessRegister,200);
      return response()->json(["status" =>true,"message" =>'Business Register Saved successfully']);
    }

     //second registration API

           public function newreg2(Request $request)
    {
        
        $book =  DB::table('business_register')->where('user_id', $request->user_id)->orderBy('user_id', 'desc')->first();
       /* echo "<pre>";
        print_r($book); exit;*/

        if(!empty($book)){
            $input = $request->all();
            $id = $book->id;

            $new2['state_id']=$request->input('state_id');
            $new2['city_id']=$request->input('city_id');
            $new2['pincode']=$request->input('pincode');
            $new2['address']=$request->input('address');

                 if($request->hasfile('b_lic'))
            {
                $image1 = $request->file('b_lic');
                $extension1 = $image1->getClientOriginalExtension(); // getting image extension
                $filename1 =time().'__'.$image1->getClientOriginalName();
                $path1 = public_path('/media/b_lic/');
                $image1->move($path1, $filename1);
                $new2['b_lic'] = $filename1;
            }
           
      

            $new2['district']=$request->input('district');

                        $businessRegister = $this->businessRegisterRepository->update($new2, $id);

        }

        
        // return $this->sendResponse($businessRegister->toArray(), 'Business Register saved successfully');
    //   return response()->json(  $businessRegister,200);
      return response()->json(["status"=>true,"message" => 'Business Details saved successfully']);
    }


    //closed second 






        //third1 registration API

           public function newreg3(Request $request)
    {
        
        $book =  DB::table('business_register')->where('user_id', $request->user_id)->orderBy('user_id', 'desc')->first();
       /* echo "<pre>";
        print_r($book); exit;*/

        if(!empty($book)){
            $input = $request->all();
            $id = $book->id;

            $new3['patient_select_you']=$request->input('p_data');
            
            //var_dump($new3);
     $businessRegister = $this->businessRegisterRepository->update($new3, $id);

        }

        
        // return $this->sendResponse($businessRegister->toArray(), 'Business Register saved successfully');
    //   return response()->json(  $businessRegister,200);
      return response()->json(["status" =>true,"message"=>'Business Details saved successfully']);
    }

    //closed third1 

    public function get_districtlist()
    {
        $data = [

            
                        'Adilabad',
                        'Anantapur',
                        'Chittoor',
                        'Kakinada',
                        'Guntur',
                        'Hyderabad',
                        'Karimnagar',
                        'Khammam',
                        'Krishna',
                        'Kurnool',
                        'Mahbubnagar',
                        'Medak',
                        'Nalgonda',
                        'Nizamabad',
                        'Ongole',
                        'Hyderabad',
                        'Srikakulam',
                        'Nellore',
                        'Visakhapatnam',
                        'Vizianagaram',
                        'Warangal',
                        'Eluru',
                        'Kadapa',
                  
                        'Anjaw',
                        'Changlang',
                        'East Siang',
                        'Kurung Kumey',
                        'Lohit',
                        'Lower Dibang Valley',
                        'Lower Subansiri',
                        'Papum Pare',
                        'Tawang',
                        'Tirap',
                        'Dibang Valley',
                        'Upper Siang',
                        'Upper Subansiri',
                        'West Kameng',
                        'West Siang',
               
                        'Baksa',
                        'Barpeta',
                        'Bongaigaon',
                        'Cachar',
                        'Chirang',
                        'Darrang',
                        'Dhemaji',
                        'Dima Hasao',
                        'Dhubri',
                        'Dibrugarh',
                        'Goalpara',
                        'Golaghat',
                        'Hailakandi',
                        'Jorhat',
                        'Kamrup',
                        'Kamrup Metropolitan',
                        'Karbi Anglong',
                        'Karimganj',
                        'Kokrajhar',
                        'Lakhimpur',
                        'Marigaon',
                        'Nagaon',
                        'Nalbari',
                        'Sibsagar',
                        'Sonitpur',
                        'Tinsukia',
                        'Udalguri',
              
                        'Araria',
                        'Arwal',
                        'Aurangabad',
                        'Banka',
                        'Begusarai',
                        'Bhagalpur',
                        'Bhojpur',
                        'Buxar',
                        'Darbhanga',
                        'East Champaran',
                        'Gaya',
                        'Gopalganj',
                        'Jamui',
                        'Jehanabad',
                        'Kaimur',
                        'Katihar',
                        'Khagaria',
                        'Kishanganj',
                        'Lakhisarai',
                        'Madhepura',
                        'Madhubani',
                        'Munger',
                        'Muzaffarpur',
                        'Nalanda',
                        'Nawada',
                        'Patna',
                        'Purnia',
                        'Rohtas',
                        'Saharsa',
                        'Samastipur',
                        'Saran',
                        'Sheikhpura',
                        'Sheohar',
                        'Sitamarhi',
                        'Siwan',
                        'Supaul',
                        'Vaishali',
                        'West Champaran',
                        'Chandigarh',
               
                        'Bastar',
                        'Bijapur',
                        'Bilaspur',
                        'Dantewada',
                        'Dhamtari',
                        'Durg',
                        'Jashpur',
                        'Janjgir-Champa',
                        'Korba',
                        'Koriya',
                        'Kanker',
                        'Kabirdham (Kawardha)',
                        'Mahasamund',
                        'Narayanpur',
                        'Raigarh',
                        'Rajnandgaon',
                        'Raipur',
                        'Surguja',
             
                        'Dadra and Nagar Haveli',
            
                        'Daman',
                        'Diu',
          
                        'Central Delhi',
                        'East Delhi',
                        'New Delhi',
                        'North Delhi',
                        'North East Delhi',
                        'North West Delhi',
                        'South Delhi',
                        'South West Delhi',
                        'West Delhi',
           
                        'North Goa',
                        'South Goa',
          
                        'Ahmedabad',
                        'Amreli district',
                        'Anand',
                        'Banaskantha',
                        'Bharuch',
                        'Bhavnagar',
                        'Dahod',
                        'The Dangs',
                        'Gandhinagar',
                        'Jamnagar',
                        'Junagadh',
                        'Kutch',
                        'Kheda',
                        'Mehsana',
                        'Narmada',
                        'Navsari',
                        'Patan',
                        'Panchmahal',
                        'Porbandar',
                        'Rajkot',
                        'Sabarkantha',
                        'Surendranagar',
                        'Surat',
                        'Vyara',
                        'Vadodara',
                        'Valsad',
           
                        'Ambala',
                        'Bhiwani',
                        'Faridabad',
                        'Fatehabad',
                        'Gurgaon',
                        'Hissar',
                        'Jhajjar',
                        'Jind',
                        'Karnal',
                        'Kaithal',
                        'Kurukshetra',
                        'Mahendragarh',
                        'Mewat',
                        'Palwal',
                        'Panchkula',
                        'Panipat',
                        'Rewari',
                        'Rohtak',
                        'Sirsa',
                        'Sonipat',
                        'Yamuna Nagar',
             
                        'Bilaspur',
                        'Chamba',
                        'Hamirpur',
                        'Kangra',
                        'Kinnaur',
                        'Kullu',
                        'Lahaul and Spiti',
                        'Mandi',
                        'Shimla',
                        'Sirmaur',
                        'Solan',
                        'Una',
              
                        'Anantnag',
                        'Badgam',
                        'Bandipora',
                        'Baramulla',
                        'Doda',
                        'Ganderbal',
                        'Jammu',
                        'Kargil',
                        'Kathua',
                        'Kishtwar',
                        'Kupwara',
                        'Kulgam',
                        'Leh',
                        'Poonch',
                        'Pulwama',
                        'Rajauri',
                        'Ramban',
                        'Reasi',
                        'Samba',
                        'Shopian',
                        'Srinagar',
                        'Udhampur',
               
                        'Bokaro',
                        'Chatra',
                        'Deoghar',
                        'Dhanbad',
                        'Dumka',
                        'East Singhbhum',
                        'Garhwa',
                        'Giridih',
                        'Godda',
                        'Gumla',
                        'Hazaribag',
                        'Jamtara',
                        'Khunti',
                        'Koderma',
                        'Latehar',
                        'Lohardaga',
                        'Pakur',
                        'Palamu',
                        'Ramgarh',
                        'Ranchi',
                        'Sahibganj',
                        'Seraikela Kharsawan',
                        'Simdega',
                        'West Singhbhum',
               
                        'Bagalkot',
                        'Bangalore Rural',
                        'Bangalore Urban',
                        'Belgaum',
                        'Bellary',
                        'Bidar',
                        'Bijapur',
                        'Chamarajnagar',
                        'Chikkamagaluru',
                        'Chikkaballapur',
                        'Chitradurga',
                        'Davanagere',
                        'Dharwad',
                        'Dakshina Kannada',
                        'Gadag',
                        'Gulbarga',
                        'Hassan',
                        'Haveri district',
                        'Kodagu',
                        'Kolar',
                        'Koppal',
                        'Mandya',
                        'Mysore',
                        'Raichur',
                        'Shimoga',
                        'Tumkur',
                        'Udupi',
                        'Uttara Kannada',
                        'Ramanagara',
                        'Yadgir',
               
                        'Alappuzha',
                        'Ernakulam',
                        'Idukki',
                        'Kannur',
                        'Kasaragod',
                        'Kollam',
                        'Kottayam',
                        'Kozhikode',
                        'Malappuram',
                        'Palakkad',
                        'Pathanamthitta',
                        'Thrissur',
                        'Thiruvananthapuram',
                        'Wayanad',
              
                        'Alirajpur',
                        'Anuppur',
                        'Ashok Nagar',
                        'Balaghat',
                        'Barwani',
                        'Betul',
                        'Bhind',
                        'Bhopal',
                        'Burhanpur',
                        'Chhatarpur',
                        'Chhindwara',
                        'Damoh',
                        'Datia',
                        'Dewas',
                        'Dhar',
                        'Dindori',
                        'Guna',
                        'Gwalior',
                        'Harda',
                        'Hoshangabad',
                        'Indore',
                        'Jabalpur',
                        'Jhabua',
                        'Katni',
                        'Khandwa (East Nimar)',
                        'Khargone (West Nimar)',
                        'Mandla',
                        'Mandsaur',
                        'Morena',
                        'Narsinghpur',
                        'Neemuch',
                        'Panna',
                        'Rewa',
                        'Rajgarh',
                        'Ratlam',
                        'Raisen',
                        'Sagar',
                        'Satna',
                        'Sehore',
                        'Seoni',
                        'Shahdol',
                        'Shajapur',
                        'Sheopur',
                        'Shivpuri',
                        'Sidhi',
                        'Singrauli',
                        'Tikamgarh',
                        'Ujjain',
                        'Umaria',
                        'Vidisha',
               
                        'Ahmednagar',
                        'Akola',
                        'Amravati',
                        'Aurangabad',
                        'Bhandara',
                        'Beed',
                        'Buldhana',
                        'Chandrapur',
                        'Dhule',
                        'Gadchiroli',
                        'Gondia',
                        'Hingoli',
                        'Jalgaon',
                        'Jalna',
                        'Kolhapur',
                        'Latur',
                        'Mumbai City',
                        'Mumbai suburban',
                        'Nandurbar',
                        'Nanded',
                        'Nagpur',
                        'Nashik',
                        'Osmanabad',
                        'Parbhani',
                        'Pune',
                        'Raigad',
                        'Ratnagiri',
                        'Sindhudurg',
                        'Sangli',
                        'Solapur',
                        'Satara',
                        'Thane',
                        'Wardha',
                        'Washim',
                        'Yavatmal',
                   
                        'Bishnupur',
                        'Churachandpur',
                        'Chandel',
                        'Imphal East',
                        'Senapati',
                        'Tamenglong',
                        'Thoubal',
                        'Ukhrul',
                        'Imphal West',
             
                        'East Garo Hills',
                        'East Khasi Hills',
                        'Jaintia Hills',
                        'Ri Bhoi',
                        'South Garo Hills',
                        'West Garo Hills',
                        'West Khasi Hills',
              
                        'Aizawl',
                        'Champhai',
                        'Kolasib',
                        'Lawngtlai',
                        'Lunglei',
                        'Mamit',
                        'Saiha',
                        'Serchhip',
               
                        'Dimapur',
                        'Kohima',
                        'Mokokchung',
                        'Mon',
                        'Phek',
                        'Tuensang',
                        'Wokha',
                        'Zunheboto',
             
                        'Angul',
                        'Boudh (Bauda)',
                        'Bhadrak',
                        'Balangir',
                        'Bargarh (Baragarh)',
                        'Balasore',
                        'Cuttack',
                        'Debagarh (Deogarh)',
                        'Dhenkanal',
                        'Ganjam',
                        'Gajapati',
                        'Jharsuguda',
                        'Jajpur',
                        'Jagatsinghpur',
                        'Khordha',
                        'Kendujhar (Keonjhar)',
                        'Kalahandi',
                        'Kandhamal',
                        'Koraput',
                        'Kendrapara',
                        'Malkangiri',
                        'Mayurbhanj',
                        'Nabarangpur',
                        'Nuapada',
                        'Nayagarh',
                        'Puri',
                        'Rayagada',
                        'Sambalpur',
                        'Subarnapur (Sonepur)',
                        'Sundergarh',
                  
                        'Karaikal',
                        'Mahe',
                        'Pondicherry',
                        'Yanam',
          
                        'Amritsar',
                        'Barnala',
                        'Bathinda',
                        'Firozpur',
                        'Faridkot',
                        'Fatehgarh Sahib',
                        'Fazilka',
                        'Gurdaspur',
                        'Hoshiarpur',
                        'Jalandhar',
                        'Kapurthala',
                        'Ludhiana',
                        'Mansa',
                        'Moga',
                        'Sri Muktsar Sahib',
                        'Pathankot',
                        'Patiala',
                        'Rupnagar',
                        'Ajitgarh (Mohali)',
                        'Sangrur',
                        'Nawanshahr',
                        'Tarn Taran',
          
                        'Ajmer',
                        'Alwar',
                        'Bikaner',
                        'Barmer',
                        'Banswara',
                        'Bharatpur',
                        'Baran',
                        'Bundi',
                        'Bhilwara',
                        'Churu',
                        'Chittorgarh',
                        'Dausa',
                        'Dholpur',
                        'Dungapur',
                        'Ganganagar',
                        'Hanumangarh',
                        'Jhunjhunu',
                        'Jalore',
                        'Jodhpur',
                        'Jaipur',
                        'Jaisalmer',
                        'Jhalawar',
                        'Karauli',
                        'Kota',
                        'Nagaur',
                        'Pali',
                        'Pratapgarh',
                        'Rajsamand',
                        'Sikar',
                        'Sawai Madhopur',
                        'Sirohi',
                        'Tonk',
                        'Udaipur',
              
                        'East Sikkim',
                        'North Sikkim',
                        'South Sikkim',
                        'West Sikkim',
              
                        'Ariyalur',
                        'Chennai',
                        'Coimbatore',
                        'Cuddalore',
                        'Dharmapuri',
                        'Dindigul',
                        'Erode',
                        'Kanchipuram',
                        'Kanyakumari',
                        'Karur',
                        'Madurai',
                        'Nagapattinam',
                        'Nilgiris',
                        'Namakkal',
                        'Perambalur',
                        'Pudukkottai',
                        'Ramanathapuram',
                        'Salem',
                        'Sivaganga',
                        'Tirupur',
                        'Tiruchirappalli',
                        'Theni',
                        'Tirunelveli',
                        'Thanjavur',
                        'Thoothukudi',
                        'Tiruvallur',
                        'Tiruvarur',
                        'Tiruvannamalai',
                        'Vellore',
                        'Viluppuram',
                        'Virudhunagar',
               
                        'Dhalai',
                        'North Tripura',
                        'South Tripura',
                        'Khowai',
                        'West Tripura',
             
                        'Agra',
                        'Allahabad',
                        'Aligarh',
                        'Ambedkar Nagar',
                        'Auraiya',
                        'Azamgarh',
                        'Barabanki',
                        'Budaun',
                        'Bagpat',
                        'Bahraich',
                        'Bijnor',
                        'Ballia',
                        'Banda',
                        'Balrampur',
                        'Bareilly',
                        'Basti',
                        'Bulandshahr',
                        'Chandauli',
                        'Chhatrapati Shahuji Maharaj Nagar',
                        'Chitrakoot',
                        'Deoria',
                        'Etah',
                        'Kanshi Ram Nagar',
                        'Etawah',
                        'Firozabad',
                        'Farrukhabad',
                        'Fatehpur',
                        'Faizabad',
                        'Gautam Buddh Nagar',
                        'Gonda',
                        'Ghazipur',
                        'Gorakhpur',
                        'Ghaziabad',
                        'Hamirpur',
                        'Hardoi',
                        'Mahamaya Nagar',
                        'Jhansi',
                        'Jalaun',
                        'Jyotiba Phule Nagar',
                        'Jaunpur district',
                        'Ramabai Nagar (Kanpur Dehat)',
                        'Kannauj',
                        'Kanpur',
                        'Kaushambi',
                        'Kushinagar',
                        'Lalitpur',
                        'Lakhimpur Kheri',
                        'Lucknow',
                        'Mau',
                        'Meerut',
                        'Maharajganj',
                        'Mahoba',
                        'Mirzapur',
                        'Moradabad',
                        'Mainpuri',
                        'Mathura',
                        'Muzaffarnagar',
                        'Panchsheel Nagar district (Hapur)',
                        'Pilibhit',
                        'Shamli',
                        'Pratapgarh',
                        'Rampur',
                        'Raebareli',
                        'Saharanpur',
                        'Sitapur',
                        'Shahjahanpur',
                        'Sant Kabir Nagar',
                        'Siddharthnagar',
                        'Sonbhadra',
                        'Sant Ravidas Nagar',
                        'Sultanpur',
                        'Shravasti',
                        'Unnao',
                        'Varanasi',
               
                        'Almora',
                        'Bageshwar',
                        'Chamoli',
                        'Champawat',
                        'Dehradun',
                        'Haridwar',
                        'Nainital',
                        'Pauri Garhwal',
                        'Pithoragarh',
                        'Rudraprayag',
                        'Tehri Garhwal',
                        'Udham Singh Nagar',
                        'Uttarkashi',
              
                        'Birbhum',
                        'Bankura',
                        'Bardhaman',
                        'Darjeeling',
                        'Dakshin Dinajpur',
                        'Hooghly',
                        'Howrah',
                        'Jalpaiguri',
                        'Cooch Behar',
                        'Kolkata',
                        'Maldah',
                        'Paschim Medinipur',
                        'Purba Medinipur',
                        'Murshidabad',
                        'Nadia',
                        'North 24 Parganas',
                        'South 24 Parganas',
                        'Purulia',
                        'Uttar Dinajpur',
               
            ];
        
    
       
        return response()->json(  $data,200);
    }
    public function couponcode()
    {
      
        $data = couponrate::select('category_id', 'name','rate','promo_code')->get();
        if($data){
        $State['success']=true;
        $State['message']="Date Fetched  successful";
        $State['coupon']=$data;
        
    }
       
        return response()->json(  $State,200);
    }



    public function saveCoupan(Request $request)
    {
        $coupon = new coupon();
        $coupon->user_id = $request->input('user_id');
        $coupon->category_d = $request->input('category_d');
        $coupon->Category_name = $request->input('Category_name');
        $coupon->Rate = $request->input('Rate');
        $coupon->save();
        if( $coupon){
        return response()->json( "Coupon Saved successfully",200);
    }
}

         function coupon_redeem(Request $request)
            {
                $data=couponrate::get()->where('promo_code',$request->promo_code)->first();
                $isused=coupon::get()->where('user_id',$request->user_id)->where('promo_code',$request->promo_code)->where('status',1)->first();
                
                    if (!empty($data)) 
                    {
                                if (!empty($isused)) {
                                
                                
                $coupon =coupon::find($isused->id);

        $coupon->status =0;
        $coupon->save();
                                return  response()->json(["status"=>true,"message"=>"your coupon applied"]);

                                        //update status  1 to 0 in coupons table




                                }
                                else
                                {
                                    return  response()->json(["status"=>false,"message"=>"You have already used this coupon"]);
                                }
                    }  
                    else{
                       return  response()->json(["status"=>false,"message"=>"Invalid Promo Code"]);
                    }                  

                
            }

    
    public function viewbusiness(Request $request)
    {
        $data = Business_register::select('*')->get();
        if( $data){
        return response()->json( $data,200);
    }


    }  

    public function business_profiles(Request $request)
    {
        $data = Business_register::where('user_id',$request->user_id)
                                ->join('country', 'business_register.country_id', '=', 'country.id')
                                ->join('state', 'business_register.state_id', '=', 'state.id')
                                ->join('city', 'business_register.city_id', '=', 'city.id')
                                ->select('business_register.*','country.country','state.state','city.city')
                                ->first();

        

        $path = array("b_lic"=>env('APP_URL').'public/media/b_lic/'.$data->b_lic,
                "b_card_pic"=>env('APP_URL').'public/media/b_card_pic/'.$data->b_card_pic,
                 "shop_pic"=>env('APP_URL').'public/media/shop_pic/'.$data->shop_pic);

        return response()->json(["status" => true,"data" => $data, "message" => "Business retrieved successfully","path" => $path]);
    }
  
     public function show(Request $req)
    {
      /*  $input=$req->all();
        if ($req->input('token')=="vghfrhrweghfrjfrgfhfrhgfrfhvdewfdewu") {
        
        $data=User::get();
        if (!empty($data)) {

            return response()->json(["status"=>true,"message"=>'Data Fetched successfully',"data"=>$data]);

        }
    }
    else
    {
                    return response()->json(["status"=>false,"message"=>'Data not Fetched Use Valid Token']);
    }*/
    echo "string";

    }
  
}

