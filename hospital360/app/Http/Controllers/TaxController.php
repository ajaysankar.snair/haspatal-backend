<?php



namespace App\Http\Controllers;



use App\DataTables\TaxDataTable;

use App\Http\Requests;

use App\Http\Requests\CreateTaxRequest;

use App\Http\Requests\UpdateTaxRequest;

use App\Repositories\TaxRepository;

use App\Http\Controllers\AppBaseController;

use Flash;

use Illuminate\Contracts\View\Factory;

use Illuminate\Http\RedirectResponse;

use Illuminate\Routing\Redirector;

use Illuminate\View\View;

use Response;
use Auth;


class TaxController extends AppBaseController

{

    /** @var  TaxRepository */

    private $taxRepository;



    public function __construct(TaxRepository $taxRepo)

    {

        $this->taxRepository = $taxRepo;

    }



    /**

     * Display a listing of the Tax.

     *

     * @param TaxDataTable $taxDataTable

     * @return Response|Factory|RedirectResponse|Redirector|View

     */

    public function index(TaxDataTable $taxDataTable)

    {

        return $taxDataTable->render('taxes.index');

    }



    /**

     * Show the form for creating a new Tax.

     *

     * @return Response|Factory|RedirectResponse|Redirector|View

     */

    public function create()

    {

        return view('taxes.create');

    }



    /**

     * Store a newly created Tax in storage.

     *

     * @param CreateTaxRequest $request

     *

     * @return Response|Factory|RedirectResponse|Redirector|View

     */

    public function store(CreateTaxRequest $request)

    {

        $input = $request->all();
        $input['created_by'] = Auth::user()->id;
        $input['updated_by'] = Auth::user()->id;


        $tax = $this->taxRepository->create($input);



        Flash::success('Tax saved successfully.');



        return redirect(route('taxes.index'));

    }



    /**

     * Display the specified Tax.

     *

     * @param  int $id

     *

     * @return Response|Factory|RedirectResponse|Redirector|View

     */

    public function show($id)

    {

        $tax = $this->taxRepository->find($id);



        if (empty($tax)) {

            Flash::error('Tax not found');



            return redirect(route('taxes.index'));

        }



        return view('taxes.show')->with('tax', $tax);

    }



    /**

     * Show the form for editing the specified Tax.

     *

     * @param  int $id

     *

     * @return Response|Factory|RedirectResponse|Redirector|View

     */

    public function edit($id)

    {

        $tax = $this->taxRepository->find($id);



        if (empty($tax)) {

            Flash::error('Tax not found');



            return redirect(route('taxes.index'));

        }



        return view('taxes.edit')->with('tax', $tax);

    }



    /**

     * Update the specified Tax in storage.

     *

     * @param  int              $id

     * @param UpdateTaxRequest $request

     *

     * @return Response|Factory|RedirectResponse|Redirector|View

     */

    public function update($id, UpdateTaxRequest $request)

    {

        $tax = $this->taxRepository->find($id);



        if (empty($tax)) {

            Flash::error('Tax not found');



            return redirect(route('taxes.index'));

        }



        $tax = $this->taxRepository->update($request->all(), $id);



        Flash::success('Tax updated successfully.');



        return redirect(route('taxes.index'));

    }



    /**

     * Remove the specified Tax from storage.

     *

     * @param  int $id

     *

     * @return Response|Factory|RedirectResponse|Redirector|View

     */

    public function destroy($id)

    {

        $tax = $this->taxRepository->find($id);



        if (empty($tax)) {

            Flash::error('Tax not found');



            return redirect(route('taxes.index'));

        }



        $this->taxRepository->delete($id);



        Flash::success('Tax deleted successfully.');



        return redirect(route('taxes.index'));

    }

}

