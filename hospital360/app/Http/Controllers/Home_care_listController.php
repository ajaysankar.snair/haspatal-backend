<?php

namespace App\Http\Controllers;

use App\DataTables\Home_care_listDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateHome_care_listRequest;
use App\Http\Requests\UpdateHome_care_listRequest;
use App\Repositories\Home_care_listRepository;
use App\Http\Controllers\AppBaseController;
use Flash;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Illuminate\View\View;
use Response;

class Home_care_listController extends AppBaseController
{
    /** @var  Home_care_listRepository */
    private $homeCareListRepository;

    public function __construct(Home_care_listRepository $homeCareListRepo)
    {
        $this->homeCareListRepository = $homeCareListRepo;
    }

    /**
     * Display a listing of the Home_care_list.
     *
     * @param Home_care_listDataTable $homeCareListDataTable
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function index(Home_care_listDataTable $homeCareListDataTable)
    {
        return $homeCareListDataTable->render('home_care_lists.index');
    }

    /**
     * Show the form for creating a new Home_care_list.
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function create()
    {
        return view('home_care_lists.create');
    }

    /**
     * Store a newly created Home_care_list in storage.
     *
     * @param CreateHome_care_listRequest $request
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function store(CreateHome_care_listRequest $request)
    {
        $input = $request->all();

        $homeCareList = $this->homeCareListRepository->create($input);

        Flash::success('Home Care List saved successfully.');

        return redirect(route('homeCareLists.index'));
    }

    /**
     * Display the specified Home_care_list.
     *
     * @param  int $id
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function show($id)
    {
        $homeCareList = $this->homeCareListRepository->find($id);

        if (empty($homeCareList)) {
            Flash::error('Home Care List not found');

            return redirect(route('homeCareLists.index'));
        }

        return view('home_care_lists.show')->with('homeCareList', $homeCareList);
    }

    /**
     * Show the form for editing the specified Home_care_list.
     *
     * @param  int $id
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function edit($id)
    {
        $homeCareList = $this->homeCareListRepository->find($id);

        if (empty($homeCareList)) {
            Flash::error('Home Care List not found');

            return redirect(route('homeCareLists.index'));
        }

        return view('home_care_lists.edit')->with('homeCareList', $homeCareList);
    }

    /**
     * Update the specified Home_care_list in storage.
     *
     * @param  int              $id
     * @param UpdateHome_care_listRequest $request
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function update($id, UpdateHome_care_listRequest $request)
    {
        $homeCareList = $this->homeCareListRepository->find($id);

        if (empty($homeCareList)) {
            Flash::error('Home Care List not found');

            return redirect(route('homeCareLists.index'));
        }

        $homeCareList = $this->homeCareListRepository->update($request->all(), $id);

        Flash::success('Home Care List updated successfully.');

        return redirect(route('homeCareLists.index'));
    }

    /**
     * Remove the specified Home_care_list from storage.
     *
     * @param  int $id
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function destroy($id)
    {
        $homeCareList = $this->homeCareListRepository->find($id);

        if (empty($homeCareList)) {
            Flash::error('Home Care List not found');

            return redirect(route('homeCareLists.index'));
        }

        $this->homeCareListRepository->delete($id);

        Flash::success('Home Care List deleted successfully.');

        return redirect(route('homeCareLists.index'));
    }
}
