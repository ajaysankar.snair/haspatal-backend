<?php

namespace App\Http\Controllers;

use App\DataTables\MemberDetailsDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateMemberDetailsRequest;
use App\Http\Requests\UpdateMemberDetailsRequest;
use App\Repositories\MemberDetailsRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use DB;
use Auth;

class MemberDetailsController extends AppBaseController
{
    /** @var  MemberDetailsRepository */
    private $memberDetailsRepository;

    public function __construct(MemberDetailsRepository $memberDetailsRepo)
    {
        $this->memberDetailsRepository = $memberDetailsRepo;
    }

    /**
     * Display a listing of the MemberDetails.
     *
     * @param MemberDetailsDataTable $memberDetailsDataTable
     * @return Response
     */
    public function index(MemberDetailsDataTable $memberDetailsDataTable)
    {
        return $memberDetailsDataTable->render('member_details.index');
    }

    /**
     * Show the form for creating a new MemberDetails.
     *
     * @return Response
     */
    public function create()
    {
        $member_type = DB::table('member_type')->pluck('type','id');
        $notification_type= DB::table('notification_type')->pluck('notification','id');
        return view('member_details.create',compact('member_type','notification_type'));
    }

    /**
     * Store a newly created MemberDetails in storage.
     *
     * @param CreateMemberDetailsRequest $request
     *
     * @return Response
     */
    public function store(CreateMemberDetailsRequest $request)
    {
        $input = $request->all();
        if($request->hasfile('id_proof'))
        {
            $image = $request->file('id_proof');
            $extension = $image->getClientOriginalExtension(); // getting image extension
            $filename =time().'__'.$image->getClientOriginalName();
            $path = public_path('/media/id_proof/');
            $image->move($path, $filename);
            $input['id_proof'] = $filename;
        }
        if($request->hasfile('image'))
        {
            $image1 = $request->file('image');
            $extension1 = $image1->getClientOriginalExtension(); // getting image extension
            $filename1 =time().'__'.$image1->getClientOriginalName();
            $path1 = public_path('/media/member_photo/');
            $image1->move($path1, $filename1);
            $input['image'] = $filename1;
        }
        $input['ctreate_by'] = Auth::user()->id;
        $input['updated_by'] = Auth::user()->id;
        $memberDetails = $this->memberDetailsRepository->create($input);

        Flash::success('Member Details saved successfully.');

        return redirect(route('memberDetails.index'));
    }

    /**
     * Display the specified MemberDetails.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        //$memberDetails = $this->memberDetailsRepository->join('notification_type','notification_type.id','member_details.notification_type')->join('member_type','member_type.id','member_details.member_type')->find($id);
        $memberDetails = DB::table('member_details')->join('notification_type','notification_type.id','member_details.notification_type')->
        join('member_type','member_type.id','member_details.member_type')
        ->where('member_details.id',$id)
        ->select('member_details.*','notification_type.notification','member_type.type')
        ->first();
        /*echo "<pre>";
        print_r($memberDetails);
        exit;*/
        if (empty($memberDetails)) {
            Flash::error('Member Details not found');
            return redirect(route('memberDetails.index'));
        }
        return view('member_details.show')->with('memberDetails', $memberDetails);
    }

    /**
     * Show the form for editing the specified MemberDetails.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $memberDetails = $this->memberDetailsRepository->find($id);

        if (empty($memberDetails)) {
            Flash::error('Member Details not found');

            return redirect(route('memberDetails.index'));
        }

        return view('member_details.edit')->with('memberDetails', $memberDetails);
    }

    /**
     * Update the specified MemberDetails in storage.
     *
     * @param  int              $id
     * @param UpdateMemberDetailsRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMemberDetailsRequest $request)
    {
        $memberDetails = $this->memberDetailsRepository->find($id);

        if (empty($memberDetails)) {
            Flash::error('Member Details not found');

            return redirect(route('memberDetails.index'));
        }

        $memberDetails = $this->memberDetailsRepository->update($request->all(), $id);

        Flash::success('Member Details updated successfully.');

        return redirect(route('memberDetails.index'));
    }

    /**
     * Remove the specified MemberDetails from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $memberDetails = $this->memberDetailsRepository->find($id);

        if (empty($memberDetails)) {
            Flash::error('Member Details not found');

            return redirect(route('memberDetails.index'));
        }

        $this->memberDetailsRepository->delete($id);

        Flash::success('Member Details deleted successfully.');

        return redirect(route('memberDetails.index'));
    }
}
