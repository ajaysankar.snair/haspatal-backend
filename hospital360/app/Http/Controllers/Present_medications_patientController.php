<?php

namespace App\Http\Controllers;

use App\DataTables\Present_medications_patientDataTable;
use App\Http\Requests;
use App\Http\Requests\CreatePresent_medications_patientRequest;
use App\Http\Requests\UpdatePresent_medications_patientRequest;
use App\Repositories\Present_medications_patientRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Auth;
use Illuminate\Http\Request;
class Present_medications_patientController extends AppBaseController
{
    /** @var  Present_medications_patientRepository */
    private $presentMedicationsPatientRepository;

    public function __construct(Present_medications_patientRepository $presentMedicationsPatientRepo)
    {
        $this->presentMedicationsPatientRepository = $presentMedicationsPatientRepo;
    }

    /**
     * Display a listing of the Present_medications_patient.
     *
     * @param Present_medications_patientDataTable $presentMedicationsPatientDataTable
     * @return Response
     */
    public function index(Present_medications_patientDataTable $presentMedicationsPatientDataTable)
    {
        return $presentMedicationsPatientDataTable->render('present_medications_patients.index');
    }

    /**
     * Show the form for creating a new Present_medications_patient.
     *
     * @return Response
     */
    public function create()
    {
        return view('present_medications_patients.create');
    }

    /**
     * Store a newly created Present_medications_patient in storage.
     *
     * @param CreatePresent_medications_patientRequest $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $input['created_by'] = Auth::user()->id;
        $input['updated_by'] = Auth::user()->id;
        $presentMedicationsPatient = $this->presentMedicationsPatientRepository->create($input);

        if(Auth::user()->role_id == 1){
            Flash::success('Present Medications Patient saved successfully.');
           return redirect(route('presentMedicationsPatients.index'));
        }else{
            Flash::success('Present Medications saved successfully.');
            return back();
        }
    }

    /**
     * Display the specified Present_medications_patient.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $presentMedicationsPatient = $this->presentMedicationsPatientRepository->find($id);

        if (empty($presentMedicationsPatient)) {
            Flash::error('Present Medications Patient not found');

            return redirect(route('presentMedicationsPatients.index'));
        }

        return view('present_medications_patients.show')->with('presentMedicationsPatient', $presentMedicationsPatient);
    }

    /**
     * Show the form for editing the specified Present_medications_patient.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $presentMedicationsPatient = $this->presentMedicationsPatientRepository->find($id);

        if (empty($presentMedicationsPatient)) {
            Flash::error('Present Medications Patient not found');

            return redirect(route('presentMedicationsPatients.index'));
        }

        return view('present_medications_patients.edit')->with('presentMedicationsPatient', $presentMedicationsPatient);
    }

    /**
     * Update the specified Present_medications_patient in storage.
     *
     * @param  int              $id
     * @param UpdatePresent_medications_patientRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePresent_medications_patientRequest $request)
    {
        $presentMedicationsPatient = $this->presentMedicationsPatientRepository->find($id);

        if (empty($presentMedicationsPatient)) {
            Flash::error('Present Medications Patient not found');

            return redirect(route('presentMedicationsPatients.index'));
        }

        $presentMedicationsPatient = $this->presentMedicationsPatientRepository->update($request->all(), $id);

        Flash::success('Present Medications Patient updated successfully.');

        return redirect(route('presentMedicationsPatients.index'));
    }

    /**
     * Remove the specified Present_medications_patient from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $presentMedicationsPatient = $this->presentMedicationsPatientRepository->find($id);

        if (empty($presentMedicationsPatient)) {
            Flash::error('Present Medications Patient not found');

            return redirect(route('presentMedicationsPatients.index'));
        }

        $this->presentMedicationsPatientRepository->delete($id);

        Flash::success('Present Medications Patient deleted successfully.');

        return redirect(route('presentMedicationsPatients.index'));
    }
}
