<?php

namespace App\Http\Controllers;

use App\DataTables\Wallet_360DataTable;
use App\Http\Requests;
use App\Http\Requests\CreateWallet_360Request;
use App\Http\Requests\UpdateWallet_360Request;
use App\Repositories\Wallet_360Repository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class Wallet_360Controller extends AppBaseController
{
    /** @var  Wallet_360Repository */
    private $wallet360Repository;

    public function __construct(Wallet_360Repository $wallet360Repo)
    {
        $this->wallet360Repository = $wallet360Repo;
    }

    /**
     * Display a listing of the Wallet_360.
     *
     * @param Wallet_360DataTable $wallet360DataTable
     * @return Response
     */
    public function index(Wallet_360DataTable $wallet360DataTable)
    {
        return $wallet360DataTable->render('wallet_360s.index');
    }

    /**
     * Show the form for creating a new Wallet_360.
     *
     * @return Response
     */
    public function create()
    {
        return view('wallet_360s.create');
    }

    /**
     * Store a newly created Wallet_360 in storage.
     *
     * @param CreateWallet_360Request $request
     *
     * @return Response
     */
    public function store(CreateWallet_360Request $request)
    {
        $input = $request->all();

        $wallet360 = $this->wallet360Repository->create($input);

        Flash::success('Wallet 360 saved successfully.');

        return redirect(route('wallet360s.index'));
    }

    /**
     * Display the specified Wallet_360.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $wallet360 = $this->wallet360Repository->find($id);

        if (empty($wallet360)) {
            Flash::error('Wallet 360 not found');

            return redirect(route('wallet360s.index'));
        }

        return view('wallet_360s.show')->with('wallet360', $wallet360);
    }

    /**
     * Show the form for editing the specified Wallet_360.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $wallet360 = $this->wallet360Repository->find($id);

        if (empty($wallet360)) {
            Flash::error('Wallet 360 not found');

            return redirect(route('wallet360s.index'));
        }

        return view('wallet_360s.edit')->with('wallet360', $wallet360);
    }

    /**
     * Update the specified Wallet_360 in storage.
     *
     * @param  int              $id
     * @param UpdateWallet_360Request $request
     *
     * @return Response
     */
    public function update($id, UpdateWallet_360Request $request)
    {
        $wallet360 = $this->wallet360Repository->find($id);

        if (empty($wallet360)) {
            Flash::error('Wallet 360 not found');

            return redirect(route('wallet360s.index'));
        }

        $wallet360 = $this->wallet360Repository->update($request->all(), $id);

        Flash::success('Wallet 360 updated successfully.');

        return redirect(route('wallet360s.index'));
    }

    /**
     * Remove the specified Wallet_360 from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $wallet360 = $this->wallet360Repository->find($id);

        if (empty($wallet360)) {
            Flash::error('Wallet 360 not found');

            return redirect(route('wallet360s.index'));
        }

        $this->wallet360Repository->delete($id);

        Flash::success('Wallet 360 deleted successfully.');

        return redirect(route('wallet360s.index'));
    }
}
