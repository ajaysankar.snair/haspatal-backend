<?php



namespace App\Http\Controllers;



use App\DataTables\TaskDataTable;

use App\Http\Requests;

use App\Http\Requests\CreateTaskRequest;

use App\Http\Requests\UpdateTaskRequest;

use App\Repositories\TaskRepository;

use App\Http\Controllers\AppBaseController;

use Flash;

use Illuminate\Contracts\View\Factory;

use Illuminate\Http\RedirectResponse;

use Illuminate\Routing\Redirector;

use Illuminate\View\View;

use Response;
use Illuminate\Http\Request;
use App\Models\Task;
use Auth;



class TaskController extends AppBaseController

{

    /** @var  TaskRepository */

    private $taskRepository;



    public function __construct(TaskRepository $taskRepo)

    {

        $this->taskRepository = $taskRepo;

    }



    /**

     * Display a listing of the Task.

     *

     * @param TaskDataTable $taskDataTable

     * @return Response|Factory|RedirectResponse|Redirector|View

     */

    public function index(TaskDataTable $taskDataTable)

    {

        return $taskDataTable->render('tasks.index');

    }



    /**

     * Show the form for creating a new Task.

     *

     * @return Response|Factory|RedirectResponse|Redirector|View

     */

    public function create()

    {

        return view('tasks.create');

    }



    /**

     * Store a newly created Task in storage.

     *

     * @param CreateTaskRequest $request

     *

     * @return Response|Factory|RedirectResponse|Redirector|View

     */

    public function store(CreateTaskRequest $request)

    {

        $input = $request->all();



        $task = $this->taskRepository->create($input);



        Flash::success('Task saved successfully.');



        return redirect(route('tasks.index'));

    }



    /**

     * Display the specified Task.

     *

     * @param  int $id

     *

     * @return Response|Factory|RedirectResponse|Redirector|View

     */

    public function show($id)

    {

        $task = $this->taskRepository->find($id);



        if (empty($task)) {

            Flash::error('Task not found');



            return redirect(route('tasks.index'));

        }



        return view('tasks.show')->with('task', $task);

    }



    /**

     * Show the form for editing the specified Task.

     *

     * @param  int $id

     *

     * @return Response|Factory|RedirectResponse|Redirector|View

     */

    public function edit($id)

    {

        $task = $this->taskRepository->find($id);



        if (empty($task)) {

            Flash::error('Task not found');



            return redirect(route('tasks.index'));

        }



        return view('tasks.edit')->with('task', $task);

    }



    /**

     * Update the specified Task in storage.

     *

     * @param  int              $id

     * @param UpdateTaskRequest $request

     *

     * @return Response|Factory|RedirectResponse|Redirector|View

     */

    public function update($id, UpdateTaskRequest $request)

    {

        $task = $this->taskRepository->find($id);



        if (empty($task)) {

            Flash::error('Task not found');



            return redirect(route('tasks.index'));

        }



        $task = $this->taskRepository->update($request->all(), $id);



        Flash::success('Task updated successfully.');



        return redirect(route('tasks.index'));

    }



    /**

     * Remove the specified Task from storage.

     *

     * @param  int $id

     *

     * @return Response|Factory|RedirectResponse|Redirector|View

     */

    public function destroy($id)

    {

        $task = $this->taskRepository->find($id);



        if (empty($task)) {

            Flash::error('Task not found');



            return redirect(route('tasks.index'));

        }



        $this->taskRepository->delete($id);



        Flash::success('Task deleted successfully.');



        return redirect(route('tasks.index'));

    }

    public function taskAssign(Request $request)
    {
        $task = $this->taskRepository->find($request->task_id);


        $input['status'] = 2;
        $input['operator_id'] = $request->oprater_id;
        $task = $this->taskRepository->update($input, $request->task_id);

        if ($task) {
            return response()->json(['status'=>'true']);
        }else{
            return response()->json(['status'=>'false']);
        }
    }

    public function operator_task()
    {
        $data = Task::where('operator_id',Auth::user()->id)->join('patient_details','task.patient_id','patient_details.id')->select('patient_details.first_name as patient_name','patient_details.mobile','patient_details.email','task.*')->get();

        return view('tasks.op_tasklist')->with('tasks', $data);
    }

    public function taskUpdate(Request $request)
    {
        $task = $this->taskRepository->find($request->task_id);


        $input['status'] = $request->status;
        $task = $this->taskRepository->update($input, $request->task_id);

        if ($task) {
            return response()->json(['status'=>'true']);
        }else{
            return response()->json(['status'=>'false']);
        }
    }

}

