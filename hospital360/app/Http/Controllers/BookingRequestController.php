<?php

namespace App\Http\Controllers;

use App\DataTables\BookingRequestDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateBookingRequestRequest;
use App\Http\Requests\UpdateBookingRequestRequest;
use App\Repositories\BookingRequestRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Auth;
use DB;

class BookingRequestController extends AppBaseController
{
    /** @var  BookingRequestRepository */
    private $bookingRequestRepository;

    public function __construct(BookingRequestRepository $bookingRequestRepo)
    {
        $this->middleware('auth');
        $this->bookingRequestRepository = $bookingRequestRepo;
    }

    /**
     * Display a listing of the BookingRequest.
     *
     * @param BookingRequestDataTable $bookingRequestDataTable
     * @return Response
     */
    public function index(BookingRequestDataTable $bookingRequestDataTable)
    {

        return $bookingRequestDataTable->render('booking_requests.index');
    }

    /**
     * Show the form for creating a new BookingRequest.
     *
     * @return Response
     */
    public function create()
    {
        $patientList = DB::table('patient_details')->pluck('first_name','id');
         if(Auth::user()->role_id == 3)
        {
            $doctorList = DB::table('doctor_details')->where('id',Auth::user()->userDetailsId)->pluck('first_name','id');
        }
        else
        {
            $doctorList = DB::table('doctor_details')->pluck('first_name','id');
        }
        return view('booking_requests.create',compact('patientList','doctorList'));
    }

    /**
     * Store a newly created BookingRequest in storage.
     *
     * @param CreateBookingRequestRequest $request
     *
     * @return Response
     */
    public function store(CreateBookingRequestRequest $request)
    {
        $input = $request->all();
        $input['created_by'] = Auth::user()->id;
        $input['updated_by'] = Auth::user()->id;
        
        $bookingRequest = $this->bookingRequestRepository->create($input);

        Flash::success('Booking Request saved successfully.');

        return redirect(route('bookingRequests.index'));
    }

    /**
     * Display the specified BookingRequest.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        //$bookingRequest = $this->bookingRequestRepository->find($id);
        $bookingRequest = DB::table('booking_request')
        ->join('doctor_details','doctor_details.id','booking_request.doctor_name')
        ->join('patient_details','patient_details.id','booking_request.patient_name')
        ->select('booking_request.*','patient_details.first_name as p_first_name','patient_details.last_name as p_last_name','doctor_details.first_name','doctor_details.last_name')
        ->where('booking_request.id',$id)->first();
        if (empty($bookingRequest)) {
            Flash::error('Booking Request not found');

            return redirect(route('bookingRequests.index'));
        }

        return view('booking_requests.show')->with('bookingRequest', $bookingRequest);
    }

    /**
     * Show the form for editing the specified BookingRequest.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $bookingRequest = $this->bookingRequestRepository->find($id);

        if (empty($bookingRequest)) {
            Flash::error('Booking Request not found');

            return redirect(route('bookingRequests.index'));
        }
         $patientList = DB::table('patient_details')->pluck('first_name','id');
           $doctorList =DB::table('doctor_details')->pluck('first_name','id');
        return view('booking_requests.edit',compact('patientList','doctorList'))->with('bookingRequest', $bookingRequest);
    }

    /**
     * Update the specified BookingRequest in storage.
     *
     * @param  int              $id
     * @param UpdateBookingRequestRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateBookingRequestRequest $request)
    {
        $bookingRequest = $this->bookingRequestRepository->find($id);
        if($request->hasfile('prescription_image'))
        {
            $image = $request->file('prescription_image');
            $extension = $image->getClientOriginalExtension(); // getting image extension
            $filename =time().'__'.$image->getClientOriginalName();
            $path = public_path('/media/prescription_image/');
            $image->move($path, $filename);
            $input['prescription_image'] = $filename;
        }
        if (empty($bookingRequest)) {
            Flash::error('Booking Request not found');

            return redirect(route('bookingRequests.index'));
        }

        $bookingRequest = $this->bookingRequestRepository->update($request->all(), $id);

        Flash::success('Booking Request updated successfully.');

        return redirect(route('bookingRequests.index'));
    }

    /**
     * Remove the specified BookingRequest from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $bookingRequest = $this->bookingRequestRepository->find($id);

        if (empty($bookingRequest)) {
            Flash::error('Booking Request not found');

            return redirect(route('bookingRequests.index'));
        }

        $this->bookingRequestRepository->delete($id);

        Flash::success('Booking Request deleted successfully.');

        return redirect(route('bookingRequests.index'));
    }
}
