<?php

namespace App\Http\Controllers;

use App\DataTables\Business_registerDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateBusiness_registerRequest;
use App\Http\Requests\UpdateBusiness_registerRequest;
use App\Repositories\Business_registerRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use DB;
use Auth;
class Business_registerController extends AppBaseController
{
    /** @var  Business_registerRepository */
    private $businessRegisterRepository;

    public function __construct(Business_registerRepository $businessRegisterRepo)
    {
        $this->businessRegisterRepository = $businessRegisterRepo;
    }

    /**
     * Display a listing of the Business_register.
     *
     * @param Business_registerDataTable $businessRegisterDataTable
     * @return Response
     */
    public function index(Business_registerDataTable $businessRegisterDataTable)
    {
        return $businessRegisterDataTable->render('business_registers.index');
    }

    /**
     * Show the form for creating a new Business_register.
     *
     * @return Response
     */
    public function create()
    {
        return view('business_registers.create');
    }

    /**
     * Store a newly created Business_register in storage.
     *
     * @param CreateBusiness_registerRequest $request
     *
     * @return Response
     */
    public function store(CreateBusiness_registerRequest $request)
    {
        $input = $request->all();
        $input['created_by'] = Auth::user()->id;
        $input['updated_by'] = Auth::user()->id;
        $businessRegister = $this->businessRegisterRepository->create($input);

        Flash::success('Business Register saved successfully.');

        return redirect(route('businessRegisters.index'));
    }

    /**
     * Display the specified Business_register.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $businessRegister = $this->businessRegisterRepository->find($id);

        if (empty($businessRegister)) {
            Flash::error('Business Register not found');

            return redirect(route('businessRegisters.index'));
        }

        return view('business_registers.show')->with('businessRegister', $businessRegister);
    }

    /**
     * Show the form for editing the specified Business_register.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $businessRegister = $this->businessRegisterRepository->find($id);

        if (empty($businessRegister)) {
            Flash::error('Business Register not found');

            return redirect(route('businessRegisters.index'));
        }

        return view('business_registers.edit')->with('businessRegister', $businessRegister);
    }

    /**
     * Update the specified Business_register in storage.
     *
     * @param  int              $id
     * @param UpdateBusiness_registerRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateBusiness_registerRequest $request)
    {
        $businessRegister = $this->businessRegisterRepository->find($id);

        if (empty($businessRegister)) {
            Flash::error('Business Register not found');

            return redirect(route('businessRegisters.index'));
        }

        $businessRegister = $this->businessRegisterRepository->update($request->all(), $id);

        Flash::success('Business Register updated successfully.');

        return redirect(route('businessRegisters.index'));
    }

    /**
     * Remove the specified Business_register from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $businessRegister = $this->businessRegisterRepository->find($id);

        if (empty($businessRegister)) {
            Flash::error('Business Register not found');

            return redirect(route('businessRegisters.index'));
        }

        $this->businessRegisterRepository->delete($id);

        Flash::success('Business Register deleted successfully.');

        return redirect(route('businessRegisters.index'));
    }
}
