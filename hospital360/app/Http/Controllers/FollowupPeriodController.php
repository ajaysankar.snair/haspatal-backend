<?php

namespace App\Http\Controllers;

use App\DataTables\FollowupPeriodDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateFollowupPeriodRequest;
use App\Http\Requests\UpdateFollowupPeriodRequest;
use App\Repositories\FollowupPeriodRepository;
use App\Http\Controllers\AppBaseController;
use Flash;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Illuminate\View\View;
use Response;

class FollowupPeriodController extends AppBaseController
{
    /** @var  FollowupPeriodRepository */
    private $followupPeriodRepository;

    public function __construct(FollowupPeriodRepository $followupPeriodRepo)
    {
        $this->followupPeriodRepository = $followupPeriodRepo;
    }

    /**
     * Display a listing of the FollowupPeriod.
     *
     * @param FollowupPeriodDataTable $followupPeriodDataTable
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function index(FollowupPeriodDataTable $followupPeriodDataTable)
    {
        return $followupPeriodDataTable->render('followup_periods.index');
    }

    /**
     * Show the form for creating a new FollowupPeriod.
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function create()
    {
        return view('followup_periods.create');
    }

    /**
     * Store a newly created FollowupPeriod in storage.
     *
     * @param CreateFollowupPeriodRequest $request
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function store(CreateFollowupPeriodRequest $request)
    {
        $input = $request->all();

        $followupPeriod = $this->followupPeriodRepository->create($input);

        Flash::success('Followup Period saved successfully.');

        return redirect(route('followupPeriods.index'));
    }

    /**
     * Display the specified FollowupPeriod.
     *
     * @param  int $id
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function show($id)
    {
        $followupPeriod = $this->followupPeriodRepository->find($id);

        if (empty($followupPeriod)) {
            Flash::error('Followup Period not found');

            return redirect(route('followupPeriods.index'));
        }

        return view('followup_periods.show')->with('followupPeriod', $followupPeriod);
    }

    /**
     * Show the form for editing the specified FollowupPeriod.
     *
     * @param  int $id
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function edit($id)
    {
        $followupPeriod = $this->followupPeriodRepository->find($id);

        if (empty($followupPeriod)) {
            Flash::error('Followup Period not found');

            return redirect(route('followupPeriods.index'));
        }

        return view('followup_periods.edit')->with('followupPeriod', $followupPeriod);
    }

    /**
     * Update the specified FollowupPeriod in storage.
     *
     * @param  int              $id
     * @param UpdateFollowupPeriodRequest $request
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function update($id, UpdateFollowupPeriodRequest $request)
    {
        $followupPeriod = $this->followupPeriodRepository->find($id);

        if (empty($followupPeriod)) {
            Flash::error('Followup Period not found');

            return redirect(route('followupPeriods.index'));
        }

        $followupPeriod = $this->followupPeriodRepository->update($request->all(), $id);

        Flash::success('Followup Period updated successfully.');

        return redirect(route('followupPeriods.index'));
    }

    /**
     * Remove the specified FollowupPeriod from storage.
     *
     * @param  int $id
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function destroy($id)
    {
        $followupPeriod = $this->followupPeriodRepository->find($id);

        if (empty($followupPeriod)) {
            Flash::error('Followup Period not found');

            return redirect(route('followupPeriods.index'));
        }

        $this->followupPeriodRepository->delete($id);

        Flash::success('Followup Period deleted successfully.');

        return redirect(route('followupPeriods.index'));
    }
}
