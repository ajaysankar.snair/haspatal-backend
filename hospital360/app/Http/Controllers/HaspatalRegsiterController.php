<?php

namespace App\Http\Controllers;

use App\DataTables\HaspatalRegsiterDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateHaspatalRegsiterRequest;
use App\Http\Requests\UpdateHaspatalRegsiterRequest;
use App\Repositories\HaspatalRegsiterRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use App\User;
use Auth;
use App\Models\HaspatalRegsiter;
use DB;
use Illuminate\Http\Request;
use Validator;

class HaspatalRegsiterController extends AppBaseController
{
    /** @var  HaspatalRegsiterRepository */
    private $haspatalRegsiterRepository;

    public function __construct(HaspatalRegsiterRepository $haspatalRegsiterRepo)
    {
        $this->haspatalRegsiterRepository = $haspatalRegsiterRepo;
    }

    /**
     * Display a listing of the HaspatalRegsiter.
     *
     * @param HaspatalRegsiterDataTable $haspatalRegsiterDataTable
     * @return Response
     */
    public function index(HaspatalRegsiterDataTable $haspatalRegsiterDataTable)
    {
        return $haspatalRegsiterDataTable->render('haspatal_regsiters.index');
    }

    /**
     * Show the form for creating a new HaspatalRegsiter.
     *
     * @return Response
     */
    public function create()
    {
        return view('haspatal_regsiters.create');
    }

    /**
     * Store a newly created HaspatalRegsiter in storage.
     *
     * @param CreateHaspatalRegsiterRequest $request
     *
     * @return Response
     */

    public function send_otp(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'email' => 'required|email|unique:haspatal_regsiter,email',
            'mobile' => 'required|unique:haspatal_regsiter,mobile',
        ]);

        if ($validator->fails()) {
          return redirect(route('haspatal.register.create'))
                   ->withErrors($validator)
                   ->withInput();
        }
        $input= $request->all();

        if($request->hasfile('license_copy'))
        {
            $image = $request->file('license_copy');
            $extension = $image->getClientOriginalExtension(); // getting image extension
            $license_copy =time().'__'.$image->getClientOriginalName();
            $path = public_path('/media/license_copy/');
            $image->move($path, $license_copy);
            $input['license_copy']= $license_copy;

        }

        if($request->hasfile('hospital_profile'))
        {
            $image = $request->file('hospital_profile');
            $extension = $image->getClientOriginalExtension(); // getting image extension
            $hospital_profile =time().'__'.$image->getClientOriginalName();
            $path = public_path('/media/hospital_profile/');
            $image->move($path, $hospital_profile);
            $input['hospital_profile']= $hospital_profile;

        }

        if($request->hasfile('hospital_location'))
        {
            $image = $request->file('hospital_location');
            $extension = $image->getClientOriginalExtension(); // getting image extension
            $hospital_location =time().'__'.$image->getClientOriginalName();
            $path = public_path('/media/hospital_location/');
            $image->move($path, $hospital_location);
            $input['hospital_location']= $hospital_location;

        }
        
        DB::table('temp_hasaptal_data')->insert(['otp' => $request['otp'], 'data' => json_encode($input)]);

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://sms.bulksmsind.in/sendSMS?username=haspatal&message=".$request['otp'].urlencode(' is the secret OTP for Secure Doctor Registration at Haspatal. Valid for 30 mins.')."&sendername=HSPTAL&smstype=TRANS&numbers=".$request['mobile']."&apikey=206f20fc-d4d5-4a33-ad06-10f453b8ab4a",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => true,
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
            //echo "cURL Error #:" . $err;
            Flash::success('OTP Not Send');
            return back();
        } else {
            Flash::success('Send OTP in your mobile no');
           
            return view('auth.haspatal.otp');
            return response()->json(["status" => true,"message" => "Send OTP in your mobile no"]);
        }


    }
    public function register(Request $request)
    {
        
        $hasptal = DB::table('temp_hasaptal_data')->where('otp',$request->otp)->first();

        if ($hasptal) {
            $input = (array)json_decode($hasptal->data);

            $h_data =  DB::table('haspatal_regsiter')->orderBy('id', 'desc')->first();
            
            if(!empty($h_data->h_uni_id)){
              $bookdata = explode('-', $h_data->h_uni_id); 
              $dd = $bookdata['2'] + 1;   
              $input['h_uni_id'] = $bookdata['0'].'-'.$bookdata['1'].'-'.'000000'.$dd;
            }else{
              $input['h_uni_id'] = '91-H-0000001';
            }

            
            $haspatalRegsiter = $this->haspatalRegsiterRepository->create($input);


            $user = User::create(['first_name'=>$input['contact_person'],
                                    'email' => $input['email'],
                                    'mobile' => $input['mobile'],
                                    'password' => bcrypt('12345678'),
                                    'role_id' => 15,
                                    'user_type' => 5,
                                    'status' => 1,
                                    'userDetailsId' => $haspatalRegsiter->id,
                                ]);

            $user->save();

            $user->attachRole(15);

            DB::table('temp_hasaptal_data')->where('otp', $request->otp)->delete();

            Flash::success('Haspatal Regsiter saved successfully.');
            return redirect(route('haspatal.register.create'));
        }else{
            Flash::success('Invalid OTP Try Again');
            return back();
        }
    }
    public function store(Request $request)
    {
        $input = $request->all();

        if($request->hasfile('license_copy'))
        {
            $image = $request->file('license_copy');
            $extension = $image->getClientOriginalExtension(); // getting image extension
            $license_copy =time().'__'.$image->getClientOriginalName();
            $path = public_path('/media/license_copy/');
            $image->move($path, $license_copy);
            $input['license_copy']= $license_copy;

        }

        if($request->hasfile('hospital_profile'))
        {
            $image = $request->file('hospital_profile');
            $extension = $image->getClientOriginalExtension(); // getting image extension
            $hospital_profile =time().'__'.$image->getClientOriginalName();
            $path = public_path('/media/hospital_profile/');
            $image->move($path, $hospital_profile);
            $input['hospital_profile']= $hospital_profile;

        }

        if($request->hasfile('hospital_location'))
        {
            $image = $request->file('hospital_location');
            $extension = $image->getClientOriginalExtension(); // getting image extension
            $hospital_location =time().'__'.$image->getClientOriginalName();
            $path = public_path('/media/hospital_location/');
            $image->move($path, $hospital_location);
            $input['hospital_location']= $hospital_location;

        }
        
        $h_data =  DB::table('haspatal_regsiter')->orderBy('id', 'desc')->first();
        if(!empty($h_data->h_uni_id)){
          $bookdata = explode('-', $h_data->h_uni_id); 
          $dd = $bookdata['2'] + 1;   
          $input['h_uni_id'] = $bookdata['0'].'-'.$bookdata['1'].'-'.'000000'.$dd;
        }else{
          $input['h_uni_id'] = '91-H-0000001';
        }

        
        $haspatalRegsiter = $this->haspatalRegsiterRepository->create($input);


        $user = User::create([  'first_name'=>$request['contact_person'],
                                'email' => $request['email'],
                                'mobile' => $request['mobile_no'],
                                'password' => bcrypt('12345678'),
                                'role_id' => 15,
                                'user_type' => 5,
                                'status' => 1,
                                'userDetailsId' => $haspatalRegsiter->id,
                            ]);

        $user->save();

        $user->attachRole(15);


        Flash::success('Haspatal Regsiter saved successfully.');

        if (Auth::user()) {
            
            return redirect(route('haspatalRegsiters.index'));
            
        }else{
            return back();
        }

    }

    /**
     * Display the specified HaspatalRegsiter.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $haspatalRegsiter = $this->haspatalRegsiterRepository->find($id);

        if (empty($haspatalRegsiter)) {
            Flash::error('Haspatal Regsiter not found');

            return redirect(route('haspatalRegsiters.index'));
        }

        return view('haspatal_regsiters.show')->with('haspatalRegsiter', $haspatalRegsiter);
    }

    /**
     * Show the form for editing the specified HaspatalRegsiter.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $haspatalRegsiter = $this->haspatalRegsiterRepository->find($id);

        if (empty($haspatalRegsiter)) {
            Flash::error('Haspatal Regsiter not found');

            return redirect(route('haspatalRegsiters.index'));
        }

        return view('haspatal_regsiters.edit')->with('haspatalRegsiter', $haspatalRegsiter);
    }

    /**
     * Update the specified HaspatalRegsiter in storage.
     *
     * @param  int              $id
     * @param UpdateHaspatalRegsiterRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateHaspatalRegsiterRequest $request)
    {
        
        $haspatalRegsiter = $this->haspatalRegsiterRepository->find($id);
        
        if($request->hasfile('license_copy'))
        {
            $image = $request->file('license_copy');
            $extension = $image->getClientOriginalExtension(); // getting image extension
            $license_copy =time().'__'.$image->getClientOriginalName();
            $path = public_path('/media/license_copy/');
            $image->move($path, $license_copy);
            $input['license_copy']= $license_copy;

        }else{
            $input['license_copy']=$haspatalRegsiter->license_copy;
        }

        if (empty($haspatalRegsiter)) {
            Flash::error('Haspatal Regsiter not found');

            return redirect(route('haspatalRegsiters.index'));
        }

        $haspatalRegsiter = $this->haspatalRegsiterRepository->update($input, $id);

        Flash::success('Haspatal Regsiter updated successfully.');

        return redirect(route('haspatalRegsiters.index'));
    }
    public function update1($id,Request $request)
    {
        
        $haspatalRegsiter = $this->haspatalRegsiterRepository->find($id);
        $input = $request->all();


        if($request->hasfile('hospital_profile'))
        {
            $image = $request->file('hospital_profile');
            $extension = $image->getClientOriginalExtension(); // getting image extension
            $hospital_profile =time().'__'.$image->getClientOriginalName();
            $path = public_path('/media/hospital_profile/');
            $image->move($path, $hospital_profile);
            $input['hospital_profile']= $hospital_profile;

        }else{
            $input['hospital_profile']=$haspatalRegsiter->hospital_profile;
        }

        if($request->hasfile('hospital_location'))
        {
            $image = $request->file('hospital_location');
            $extension = $image->getClientOriginalExtension(); // getting image extension
            $hospital_location =time().'__'.$image->getClientOriginalName();
            $path = public_path('/media/hospital_location/');
            $image->move($path, $hospital_location);
            $input['hospital_location']= $hospital_location;

        }else{
            $input['hospital_location']=$haspatalRegsiter->hospital_location;
        }
        if($request->hasfile('license_copy'))
        {
            $image = $request->file('license_copy');
            $extension = $image->getClientOriginalExtension(); // getting image extension
            $license_copy =time().'__'.$image->getClientOriginalName();
            $path = public_path('/media/license_copy/');
            $image->move($path, $license_copy);
            $input['license_copy']= $license_copy;

        }else{
            $input['license_copy']=$haspatalRegsiter->license_copy;
        }
        if($request->hasfile('cheque_img'))
        {
            $image = $request->file('cheque_img');
            $extension = $image->getClientOriginalExtension(); // getting image extension
            $cheque_img =time().'__'.$image->getClientOriginalName();
            $path = public_path('/media/cheque_img/');
            $image->move($path, $cheque_img);
            $input['cheque_img']= $cheque_img;

        }else{
            $input['cheque_img']=$haspatalRegsiter->cheque_img;
        }

        if (empty($haspatalRegsiter)) {
            Flash::error('Haspatal Regsiter not found');

            return redirect(route('haspatalRegsiters.index'));
        }

        $haspatalRegsiter = $this->haspatalRegsiterRepository->update($input, $id);

        Flash::success('Haspatal Regsiter updated successfully.');

        return back();
    }

    /**
     * Remove the specified HaspatalRegsiter from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $haspatalRegsiter = $this->haspatalRegsiterRepository->find($id);

        if (empty($haspatalRegsiter)) {
            Flash::error('Haspatal Regsiter not found');

            return redirect(route('haspatalRegsiters.index'));
        }

        $this->haspatalRegsiterRepository->delete($id);

        Flash::success('Haspatal Regsiter deleted successfully.');

        return redirect(route('haspatalRegsiters.index'));
    }
}
