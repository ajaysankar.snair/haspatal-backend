<?php

namespace App\Http\Controllers;

use App\DataTables\DoctorsDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateDoctorsRequest;
use App\Http\Requests\UpdateDoctorsRequest;
use App\Repositories\DoctorsRepository;
use App\User;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Auth;
use DB;
use Validator;
use Illuminate\Http\Request;

class DoctorsController extends AppBaseController
{
    /** @var  DoctorsRepository */
    private $doctorsRepository;

    public function __construct(DoctorsRepository $doctorsRepo)
    {
        $this->doctorsRepository = $doctorsRepo;
    }

    /**
     * Display a listing of the Doctors.
     *
     * @param DoctorsDataTable $doctorsDataTable
     * @return Response
     */
    public function index(DoctorsDataTable $doctorsDataTable)
    {
      
        return $doctorsDataTable->render('doctors.index');
    }

    public function dr_active($id)
    {

      $booklist =  DB::table('users')->where('userDetailsId',$id)->where('user_type',1)->update(['status' => 1]);

      $booklist =  DB::table('doctor_details')->where('id',$id)->update(['status' => 1]);

      Flash::success('Doctor updated successfully.');

      return back();

    }
    public function dr_deactive($id)
    {
      $booklist =  DB::table('users')->where('userDetailsId',$id)->where('user_type',1)->update(['status' => 0]);
      
      $booklist =  DB::table('doctor_details')->where('id',$id)->update(['status' => 0]);

      Flash::success('Doctor updated successfully.');
      return back();

    }

    /**
     * Show the form for creating a new Doctors.
     *
     * @return Response
     */
    public function create()
    {
        $countryList = \App\Models\Country::orderBy('country','asc')->pluck('country','id');
        $stateList = \App\Models\State::orderBy('state','asc')->pluck('state','id');
        $cityList = \App\Models\City::orderBy('city','asc')->pluck('city','id');
        $languageList = DB::table('language')->orderBy('language','asc')->pluck('language','id');
        $specilities= DB::table('specilities')->orderBy('specility','asc')->pluck('specility','id');
        $licenceTyes= DB::table('licence_type')->orderBy('licence_name','asc')->pluck('licence_name','id');
        return view('doctors.create',compact('countryList','stateList','cityList','languageList','specilities','licenceTyes'));
    }

    public function change_password(Request $request)
    {
          if (!password_verify($request->old_password, Auth::user()->password)) {
          return response()->json(['status'=> false, 'message'=>'Invalid current Password']);
          }

          unset($request->old_password);
          if ($request->new_password == '' || $request->con_password == '') {
            unset($request->new_password);
            unset($request->con_password);

          } else if ($request->new_password != '' && $request->con_password != '') {
            $data['password'] = bcrypt($request->new_password);
            $user = User::findOrFail(Auth::user()->id);
            $user->update($data);
            $user->save();
            Flash::success('Password Change successfully.');
            return back();
          }
    }

    /**
     * Store a newly created Doctors in storage.
     *
     * @param CreateDoctorsRequest $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
      

        $input = $request->all();
        
        if ($request->has('language')) {
          $tags = $request->input('language');
          $tags1 = implode(',', $tags);
          $input['language'] = $tags1;
        }

        if ($request->has('speciality')) {
          $speciality = $request->input('speciality');
          $speciality1 = implode(',', $speciality);
          $input['speciality'] = $speciality1;
        }


        if($request->hasfile('licence_copy'))
        {
            $image = $request->file('licence_copy');
            $extension = $image->getClientOriginalExtension(); // getting image extension
            $filename =time().'__'.$image->getClientOriginalName();
            $path = public_path('/media/licence_copy/');
            $image->move($path, $filename);
            $input['licence_copy'] = $filename;
        }
        if($request->hasfile('profile_pic'))
        {
            $image1 = $request->file('profile_pic');
            $extension1 = $image1->getClientOriginalExtension(); // getting image extension
            $filename1 =time().'__'.$image1->getClientOriginalName();
            $path1 = public_path('/media/profile_pic/');
            $image1->move($path1, $filename1);
            $input['profile_pic'] = $filename1;
        }

       
        // $input['created_by'] = Auth::user()->id;
        // $input['updated_by'] = Auth::user()->id;
        $d_data =  DB::table('doctor_details')->orderBy('id', 'desc')->first();
        if($d_data->d_uni_id != ''){
          $bookdata = explode('-', $d_data->d_uni_id); 
          $dd = $bookdata['2'] + 1;   
          $input['d_uni_id'] = $bookdata['0'].'-'.$bookdata['1'].'-'.'00000'.$dd;
        }else{
          $input['d_uni_id'] = '91-D-000001';
        }

        $input['created_by'] = Auth::user()->id;
        $input['updated_by'] = Auth::user()->id;
        $doctors = $this->doctorsRepository->create($input);

        // $doctors = implode(",",$request['language[]']);
        $user = User::create(['first_name' => $request['first_name'],
                                  'last_name' => $request['last_name'],
                                  'email' => $request['email'],
                                  'plan_id' => $request['plan_id'],
                                  'role_id' => 3,
                                  'user_type' => 1,
                                  'userDetailsId'=>$doctors->id,
                                  'doctor_login_status'=> 0,
                                  'password' => bcrypt($request['password']),
                                  ]);
            $user->attachRole(3);

        Flash::success('Doctor add successfully.');

       return back();
    }

    /**
     * Display the specified Doctors.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        //$doctors = $this->doctorsRepository->find($id);
        $doctors = DB::table('doctor_details')
        ->leftJoin('state','state.id','doctor_details.state')
        ->leftJoin('district','district.id','doctor_details.district')
        ->leftJoin('city','city.id','doctor_details.city')
        ->leftJoin('language','language.id','doctor_details.language')
        ->leftJoin('specilities','specilities.id','doctor_details.speciality')
        //->leftJoin('licence_type','licence_type.id','doctor_details.licence_type')
        ->select('doctor_details.*','state.state','district.district_name','city.city','language.language','specilities.specility')
        ->where('doctor_details.id',$id)->first();
        /*if (empty($doctors)) {
            Flash::error('Doctors not found');

            return redirect(route('doctors.index'));
        }
*/
    return view('doctors.show')->with('doctors', $doctors);
        
    }

    /**
     * Show the form for editing the specified Doctors.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
      
        $doctors = $this->doctorsRepository->find($id);
        
        $languageList = DB::table('language')->pluck('language','id');
        $specilities= DB::table('specilities')->pluck('specility','id');
        $licenceTyes= DB::table('licence_type')->pluck('licence_name','id');
        $plan= DB::table('plan_master')->pluck('pl_name','pl_id');
        if(empty($doctors)) {
            Flash::error('Doctors not found');

            return redirect(route('doctors.index'));
        }

        return view('doctors.edit',compact('countryList','stateList','cityList','languageList','specilities','licenceTyes','plan'))->with('doctors', $doctors);
    }

    /**
     * Update the specified Doctors in storage.
     *
     * @param  int              $id
     * @param UpdateDoctorsRequest $request
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        
        $doctors = $this->doctorsRepository->find($id);
        $input = $request->all();
        if (empty($doctors)) {
            Flash::error('Doctors not found');

            return redirect(route('doctors.index'));
        }
        if($request->hasfile('licence_copy'))
        {
            $image = $request->file('licence_copy');
            $extension = $image->getClientOriginalExtension(); // getting image extension
            $media_photos =time().'__'.$image->getClientOriginalName();
            $path = public_path('/media/licence_copy/');
            $image->move($path, $media_photos);
            $input['licence_copy']= $media_photos;

        }else{
            $input['licence_copy']=$doctors->licence_copy;
        }
        if($request->hasfile('profile_pic'))
        {
            $image = $request->file('profile_pic');
            $extension = $image->getClientOriginalExtension(); // getting image extension
            $media_photos =time().'__'.$image->getClientOriginalName();
            $path = public_path('/media/profile_pic/');
            $image->move($path, $media_photos);
            $input['profile_pic']= $media_photos;

        }else{
            $input['profile_pic']=$doctors->profile_pic;
        } 
        if($request->hasfile('clinic_logo'))
        {
            $image = $request->file('clinic_logo');
            $extension = $image->getClientOriginalExtension(); // getting image extension
            $media_photos =time().'__'.$image->getClientOriginalName();
            $path = public_path('/media/clinic_logo/');
            $image->move($path, $media_photos);
            $input['clinic_logo']= $media_photos;

        }else{
            $input['clinic_logo']=$doctors->clinic_logo;
        }
        if($request->hasfile('cheque_img'))
        {
            $image = $request->file('cheque_img');
            $extension = $image->getClientOriginalExtension(); // getting image extension
            $media_photos =time().'__'.$image->getClientOriginalName();
            $path = public_path('/media/cheque_img/');
            $image->move($path, $media_photos);
            $input['cheque_img']= $media_photos;

        }else{
            $input['cheque_img']=$doctors->cheque_img;
        }
        
         if ($request->has('price')) {

            $input['price']= $request->price;
            $user = DB::table('users')->where('user_type',1)->where('userDetailsId', Auth::user()->userDetailsId)->update(['doctor_login_status_mobile'=> 1]);
            
            /*$dr_old_price = DB::table('doctor_details')->where('id',Auth::user()->userDetailsId)->orderBy('id', 'desc')->first(); 
            
            if ($dr_old_price->price == $request->price) {
            }else{

                $admin_price = DB::table('booking_price_admin')->orderBy('id', 'desc')->first();
                $input['price']= $request->price + $admin_price->book_price; 
                
            }*/


        }


        if(isset($request->language))
        {
          
          $input['language'] = implode(',',$request->language);
        }
        if(isset($request->speciality))
        {
          
          $input['speciality'] = implode(',',$request->speciality);
        }
        $doctors = $this->doctorsRepository->update($input, $id);
        if(Auth::user()->role_id == 1){
            Flash::success('Updated successfully.');
            return redirect(route('doctors.index'));
        }else{
            Flash::success('Updated successfully.');
            return back();
        }
    }

    /**
     * Remove the specified Doctors from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $doctors = $this->doctorsRepository->find($id);

        if (empty($doctors)) {
            Flash::error('Doctors not found');

            return redirect(route('doctors.index'));
        }

        $u_doctor = DB::table('users')->where('userDetailsId',$id)->where('user_type',1)->delete();
        $this->doctorsRepository->delete($id);

        Flash::success('Doctors deleted successfully.');

        return redirect(route('doctors.index'));
    }
    
    //View Doctors count on Kizaku Dashboard
    
                                  public function doctor_count(Request $request)
                                {
                                    $data=array();
                                    $drdata=DB::table('doctor_details')
                                    ->join('users','users.userDetailsId','doctor_details.id')
                                    ->where('users.status',$request->status)
                                    ->where('users.user_type',1)
                                    ->select('users.status','doctor_details.*')
                                    ->get();
                                    
                                    if(count($drdata)>0)
                                    {
                                        $data['status']=true;
                                        $data['doctor_details']=$drdata;
                                    }
                                    else
                                    {
                                        $data['status']=false;
                                    }
                                    
                                    return response()->json($data);
                                }    
}
