<?php

namespace App\Http\Controllers;

use App\DataTables\Imaging_listDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateImaging_listRequest;
use App\Http\Requests\UpdateImaging_listRequest;
use App\Repositories\Imaging_listRepository;
use App\Http\Controllers\AppBaseController;
use Flash;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Illuminate\View\View;
use Response;

class Imaging_listController extends AppBaseController
{
    /** @var  Imaging_listRepository */
    private $imagingListRepository;

    public function __construct(Imaging_listRepository $imagingListRepo)
    {
        $this->imagingListRepository = $imagingListRepo;
    }

    /**
     * Display a listing of the Imaging_list.
     *
     * @param Imaging_listDataTable $imagingListDataTable
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function index(Imaging_listDataTable $imagingListDataTable)
    {
        return $imagingListDataTable->render('imaging_lists.index');
    }

    /**
     * Show the form for creating a new Imaging_list.
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function create()
    {
        return view('imaging_lists.create');
    }

    /**
     * Store a newly created Imaging_list in storage.
     *
     * @param CreateImaging_listRequest $request
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function store(CreateImaging_listRequest $request)
    {
        $input = $request->all();

        $imagingList = $this->imagingListRepository->create($input);

        Flash::success('Imaging List saved successfully.');

        return redirect(route('imagingLists.index'));
    }

    /**
     * Display the specified Imaging_list.
     *
     * @param  int $id
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function show($id)
    {
        $imagingList = $this->imagingListRepository->find($id);

        if (empty($imagingList)) {
            Flash::error('Imaging List not found');

            return redirect(route('imagingLists.index'));
        }

        return view('imaging_lists.show')->with('imagingList', $imagingList);
    }

    /**
     * Show the form for editing the specified Imaging_list.
     *
     * @param  int $id
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function edit($id)
    {
        $imagingList = $this->imagingListRepository->find($id);

        if (empty($imagingList)) {
            Flash::error('Imaging List not found');

            return redirect(route('imagingLists.index'));
        }

        return view('imaging_lists.edit')->with('imagingList', $imagingList);
    }

    /**
     * Update the specified Imaging_list in storage.
     *
     * @param  int              $id
     * @param UpdateImaging_listRequest $request
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function update($id, UpdateImaging_listRequest $request)
    {
        $imagingList = $this->imagingListRepository->find($id);

        if (empty($imagingList)) {
            Flash::error('Imaging List not found');

            return redirect(route('imagingLists.index'));
        }

        $imagingList = $this->imagingListRepository->update($request->all(), $id);

        Flash::success('Imaging List updated successfully.');

        return redirect(route('imagingLists.index'));
    }

    /**
     * Remove the specified Imaging_list from storage.
     *
     * @param  int $id
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function destroy($id)
    {
        $imagingList = $this->imagingListRepository->find($id);

        if (empty($imagingList)) {
            Flash::error('Imaging List not found');

            return redirect(route('imagingLists.index'));
        }

        $this->imagingListRepository->delete($id);

        Flash::success('Imaging List deleted successfully.');

        return redirect(route('imagingLists.index'));
    }
}
