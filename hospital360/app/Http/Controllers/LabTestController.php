<?php

namespace App\Http\Controllers;

use App\DataTables\LabTestDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateLabTestRequest;
use App\Http\Requests\UpdateLabTestRequest;
use App\Repositories\LabTestRepository;
use App\Http\Controllers\AppBaseController;
use Flash;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Illuminate\View\View;
use Response;

class LabTestController extends AppBaseController
{
    /** @var  LabTestRepository */
    private $labTestRepository;

    public function __construct(LabTestRepository $labTestRepo)
    {
        $this->labTestRepository = $labTestRepo;
    }

    /**
     * Display a listing of the LabTest.
     *
     * @param LabTestDataTable $labTestDataTable
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function index(LabTestDataTable $labTestDataTable)
    {
        return $labTestDataTable->render('lab_tests.index');
    }

    /**
     * Show the form for creating a new LabTest.
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function create()
    {
        return view('lab_tests.create');
    }

    /**
     * Store a newly created LabTest in storage.
     *
     * @param CreateLabTestRequest $request
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function store(CreateLabTestRequest $request)
    {
        $input = $request->all();

        $labTest = $this->labTestRepository->create($input);

        Flash::success('Lab Test saved successfully.');

        return redirect(route('labTests.index'));
    }

    /**
     * Display the specified LabTest.
     *
     * @param  int $id
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function show($id)
    {
        $labTest = $this->labTestRepository->find($id);

        if (empty($labTest)) {
            Flash::error('Lab Test not found');

            return redirect(route('labTests.index'));
        }

        return view('lab_tests.show')->with('labTest', $labTest);
    }

    /**
     * Show the form for editing the specified LabTest.
     *
     * @param  int $id
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function edit($id)
    {
        $labTest = $this->labTestRepository->find($id);

        if (empty($labTest)) {
            Flash::error('Lab Test not found');

            return redirect(route('labTests.index'));
        }

        return view('lab_tests.edit')->with('labTest', $labTest);
    }

    /**
     * Update the specified LabTest in storage.
     *
     * @param  int              $id
     * @param UpdateLabTestRequest $request
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function update($id, UpdateLabTestRequest $request)
    {
        $labTest = $this->labTestRepository->find($id);

        if (empty($labTest)) {
            Flash::error('Lab Test not found');

            return redirect(route('labTests.index'));
        }

        $labTest = $this->labTestRepository->update($request->all(), $id);

        Flash::success('Lab Test updated successfully.');

        return redirect(route('labTests.index'));
    }

    /**
     * Remove the specified LabTest from storage.
     *
     * @param  int $id
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function destroy($id)
    {
        $labTest = $this->labTestRepository->find($id);

        if (empty($labTest)) {
            Flash::error('Lab Test not found');

            return redirect(route('labTests.index'));
        }

        $this->labTestRepository->delete($id);

        Flash::success('Lab Test deleted successfully.');

        return redirect(route('labTests.index'));
    }
}
