<?php

namespace App\Http\Controllers;

use App\DataTables\H_planDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateH_planRequest;
use App\Http\Requests\UpdateH_planRequest;
use App\Repositories\H_planRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use DB;
use Auth;

class H_planController extends AppBaseController
{
    /** @var  H_planRepository */
    private $hPlanRepository;

    public function __construct(H_planRepository $hPlanRepo)
    {
        $this->hPlanRepository = $hPlanRepo;
    }

    /**
     * Display a listing of the H_plan.
     *
     * @param H_planDataTable $hPlanDataTable
     * @return Response
     */
    public function index(H_planDataTable $hPlanDataTable)
    {
        return $hPlanDataTable->render('h_plans.index');
    }

    /**
     * Show the form for creating a new H_plan.
     *
     * @return Response
     */
    public function create()
    {
        return view('h_plans.create');
    }

    /**
     * Store a newly created H_plan in storage.
     *
     * @param CreateH_planRequest $request
     *
     * @return Response
     */
    public function store(CreateH_planRequest $request)
    {
        $input = $request->all();
        $input['created_by'] = Auth::user()->id;
        $input['updated_by'] = Auth::user()->id;
        $hPlan = $this->hPlanRepository->create($input);

        Flash::success('H Plan saved successfully.');

        return redirect(route('hPlans.index'));
    }

    /**
     * Display the specified H_plan.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $hPlan = $this->hPlanRepository->find($id);

        if (empty($hPlan)) {
            Flash::error('H Plan not found');

            return redirect(route('hPlans.index'));
        }

        return view('h_plans.show')->with('hPlan', $hPlan);
    }

    /**
     * Show the form for editing the specified H_plan.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $hPlan = $this->hPlanRepository->find($id);

        if (empty($hPlan)) {
            Flash::error('H Plan not found');

            return redirect(route('hPlans.index'));
        }

        return view('h_plans.edit')->with('hPlan', $hPlan);
    }

    /**
     * Update the specified H_plan in storage.
     *
     * @param  int              $id
     * @param UpdateH_planRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateH_planRequest $request)
    {
        $hPlan = $this->hPlanRepository->find($id);

        if (empty($hPlan)) {
            Flash::error('H Plan not found');

            return redirect(route('hPlans.index'));
        }

        $hPlan = $this->hPlanRepository->update($request->all(), $id);

        Flash::success('H Plan updated successfully.');

        return redirect(route('hPlans.index'));
    }

    /**
     * Remove the specified H_plan from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $hPlan = $this->hPlanRepository->find($id);

        if (empty($hPlan)) {
            Flash::error('H Plan not found');

            return redirect(route('hPlans.index'));
        }

        $this->hPlanRepository->delete($id);

        Flash::success('H Plan deleted successfully.');

        return redirect(route('hPlans.index'));
    }
}
