<?php

namespace App\Http\Controllers;

use App\DataTables\Examination_notes_patientDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateExamination_notes_patientRequest;
use App\Http\Requests\UpdateExamination_notes_patientRequest;
use App\Repositories\Examination_notes_patientRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Auth;
use Illuminate\Http\Request;
class Examination_notes_patientController extends AppBaseController
{
    /** @var  Examination_notes_patientRepository */
    private $examinationNotesPatientRepository;

    public function __construct(Examination_notes_patientRepository $examinationNotesPatientRepo)
    {
        $this->examinationNotesPatientRepository = $examinationNotesPatientRepo;
    }

    /**
     * Display a listing of the Examination_notes_patient.
     *
     * @param Examination_notes_patientDataTable $examinationNotesPatientDataTable
     * @return Response
     */
    public function index(Examination_notes_patientDataTable $examinationNotesPatientDataTable)
    {
        return $examinationNotesPatientDataTable->render('examination_notes_patients.index');
    }

    /**
     * Show the form for creating a new Examination_notes_patient.
     *
     * @return Response
     */
    public function create()
    {
        return view('examination_notes_patients.create');
    }

    /**
     * Store a newly created Examination_notes_patient in storage.
     *
     * @param CreateExamination_notes_patientRequest $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $input['created_by'] = Auth::user()->id;
        $input['updated_by'] = Auth::user()->id;
        $examinationNotesPatient = $this->examinationNotesPatientRepository->create($input);

        if(Auth::user()->role_id == 1){
           Flash::success('Examination Notes Patient saved successfully.');
         return redirect(route('examinationNotesPatients.index'));
        }else{
           Flash::success('Examination Notes saved successfully.');
            return back();
        }
    }

    /**
     * Display the specified Examination_notes_patient.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $examinationNotesPatient = $this->examinationNotesPatientRepository->find($id);

        if (empty($examinationNotesPatient)) {
            Flash::error('Examination Notes Patient not found');

            return redirect(route('examinationNotesPatients.index'));
        }

        return view('examination_notes_patients.show')->with('examinationNotesPatient', $examinationNotesPatient);
    }

    /**
     * Show the form for editing the specified Examination_notes_patient.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $examinationNotesPatient = $this->examinationNotesPatientRepository->find($id);

        if (empty($examinationNotesPatient)) {
            Flash::error('Examination Notes Patient not found');

            return redirect(route('examinationNotesPatients.index'));
        }

        return view('examination_notes_patients.edit')->with('examinationNotesPatient', $examinationNotesPatient);
    }

    /**
     * Update the specified Examination_notes_patient in storage.
     *
     * @param  int              $id
     * @param UpdateExamination_notes_patientRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateExamination_notes_patientRequest $request)
    {
        $examinationNotesPatient = $this->examinationNotesPatientRepository->find($id);

        if (empty($examinationNotesPatient)) {
            Flash::error('Examination Notes Patient not found');

            return redirect(route('examinationNotesPatients.index'));
        }

        $examinationNotesPatient = $this->examinationNotesPatientRepository->update($request->all(), $id);

        Flash::success('Examination Notes Patient updated successfully.');

        return redirect(route('examinationNotesPatients.index'));
    }

    /**
     * Remove the specified Examination_notes_patient from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $examinationNotesPatient = $this->examinationNotesPatientRepository->find($id);

        if (empty($examinationNotesPatient)) {
            Flash::error('Examination Notes Patient not found');

            return redirect(route('examinationNotesPatients.index'));
        }

        $this->examinationNotesPatientRepository->delete($id);

        Flash::success('Examination Notes Patient deleted successfully.');

        return redirect(route('examinationNotesPatients.index'));
    }
}
