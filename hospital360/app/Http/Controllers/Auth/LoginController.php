<?php

namespace App\Http\Controllers\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth; 
use Flash;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */

    protected function authenticated(Request $request, $user) {
        
        if ($user->role_id == 1) {
            return redirect('/home');
        }elseif($user->status == 1){
            Flash::warning('You are not allow to login. Please contact to administrator');
            Auth::logout();
            return redirect()->back();
        }elseif($user->user_type == 5 &&$user->role_id == 15) {
            return redirect('/hospital_profile');
        }elseif($user->role_id == 17) {
            return redirect('/home');
        }elseif($user->role_id == 18) {
            return redirect('/home');
        }elseif($user->user_type == 1 && $user->doctor_login_status == 0 || $user->doctor_login_status_mobile == 0) {
            return redirect('/dr_setting');
        }elseif($user->user_type == 1 && $user->doctor_login_status == 1 || $user->doctor_login_status_mobile == 1) {
            return redirect('/homepage');
        }elseif($user->user_type == 1 && $user->doctor_login_status == null) {
            return redirect('/dr_setting');
        }elseif($user->status == 0) {
            return redirect('/home');
        } else {
            return redirect('/home');
        }
}

    // protected $redirectTo = '/dashbord';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
