<?php

namespace App\Http\Controllers;

use App\DataTables\Available_lab_patientDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateAvailable_lab_patientRequest;
use App\Http\Requests\UpdateAvailable_lab_patientRequest;
use App\Repositories\Available_lab_patientRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Auth;
class Available_lab_patientController extends AppBaseController
{
    /** @var  Available_lab_patientRepository */
    private $availableLabPatientRepository;

    public function __construct(Available_lab_patientRepository $availableLabPatientRepo)
    {
        $this->availableLabPatientRepository = $availableLabPatientRepo;
    }

    /**
     * Display a listing of the Available_lab_patient.
     *
     * @param Available_lab_patientDataTable $availableLabPatientDataTable
     * @return Response
     */
    public function index(Available_lab_patientDataTable $availableLabPatientDataTable)
    {
        return $availableLabPatientDataTable->render('available_lab_patients.index');
    }

    /**
     * Show the form for creating a new Available_lab_patient.
     *
     * @return Response
     */
    public function create()
    {
        return view('available_lab_patients.create');
    }

    /**
     * Store a newly created Available_lab_patient in storage.
     *
     * @param CreateAvailable_lab_patientRequest $request
     *
     * @return Response
     */
    public function store(CreateAvailable_lab_patientRequest $request)
    {
        $input = $request->all();
         $input['created_by'] = Auth::user()->id;
        $input['updated_by'] = Auth::user()->id;
        $availableLabPatient = $this->availableLabPatientRepository->create($input);

        if(Auth::user()->role_id == 1){

         Flash::success('Available Lab Patient saved successfully.');

        return redirect(route('availableLabPatients.index'));

        }else{

            Flash::success('Available Lab saved successfully.');

            return back();
        }

    }

    /**
     * Display the specified Available_lab_patient.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $availableLabPatient = $this->availableLabPatientRepository->find($id);

        if (empty($availableLabPatient)) {
            Flash::error('Available Lab Patient not found');

            return redirect(route('availableLabPatients.index'));
        }

        return view('available_lab_patients.show')->with('availableLabPatient', $availableLabPatient);
    }

    /**
     * Show the form for editing the specified Available_lab_patient.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $availableLabPatient = $this->availableLabPatientRepository->find($id);

        if (empty($availableLabPatient)) {
            Flash::error('Available Lab Patient not found');

            return redirect(route('availableLabPatients.index'));
        }

        return view('available_lab_patients.edit')->with('availableLabPatient', $availableLabPatient);
    }

    /**
     * Update the specified Available_lab_patient in storage.
     *
     * @param  int              $id
     * @param UpdateAvailable_lab_patientRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAvailable_lab_patientRequest $request)
    {
        $availableLabPatient = $this->availableLabPatientRepository->find($id);

        if (empty($availableLabPatient)) {
            Flash::error('Available Lab Patient not found');

            return redirect(route('availableLabPatients.index'));
        }

        $availableLabPatient = $this->availableLabPatientRepository->update($request->all(), $id);

        Flash::success('Available Lab Patient updated successfully.');

        return redirect(route('availableLabPatients.index'));
    }

    /**
     * Remove the specified Available_lab_patient from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $availableLabPatient = $this->availableLabPatientRepository->find($id);

        if (empty($availableLabPatient)) {
            Flash::error('Available Lab Patient not found');

            return redirect(route('availableLabPatients.index'));
        }

        $this->availableLabPatientRepository->delete($id);

        Flash::success('Available Lab Patient deleted successfully.');

        return redirect(route('availableLabPatients.index'));
    }
}
