<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\User;
use App\couponrate;
use App\Models\business_register;
use App\Http\Controllers\AppBaseController;
use Response;
use Auth;
use DB;

class ServicesForKizaku extends Controller
{
                //Business  for kizaku
                    
                    /**
                     * filter_by as week,today,this month,year,fortnight,yesterday
                     * 
                     * 
                     * */
                         public function singleService_count_startYear($status,$business_type=null,$reg=null)
                                {
                        $businessData=0;
                                    if(empty($reg))
                                    {
                                        $businessData=DB::table('business_register')
                                    ->join('users','users.id','business_register.user_id')
                                    ->whereYear('users.created_at',Carbon::now()->format('Y'))
                                    ->where('users.status',$status)
                                    ->where('users.user_type',4)->where('users.role_id',$business_type)
                                    ->count();
                                    }
                                    else
                                    {
                                        $businessData=DB::table('business_register')
                                    ->join('users','users.id','business_register.user_id')
                                    ->whereYear('users.created_at',Carbon::now()->format('Y'))
                                    ->where('users.user_type',4)->where('users.role_id',$business_type)
                                    ->count();
                                    }
                                    
                                    
                                    
                                    if($businessData>0)
                                    {
                                        return $businessData;
                                    }
                                    else
                                    {
                                        return false;
                                    }
                                    
                                    
                                } 
                                
                                
                                
                                 public function singleService_count_startForty($status,$business_type=null,$reg=null)
                                {
                        $businessData=0;
                                    if(empty($reg))
                                    {
                                     $businessData=DB::table('business_register')
                                    ->join('users','users.id','business_register.user_id')
                                    ->whereBetween('users.created_at',[Carbon::now()->subDays(15),Carbon::now()])
                                    ->where('users.status',$status)
                                    ->where('users.user_type',4)->where('users.role_id',$business_type)
                                    ->count();
                                    }
                                    else
                                    {
                                    $businessData=DB::table('business_register')
                                    ->join('users','users.id','business_register.user_id')
                                    ->whereBetween('users.created_at',[Carbon::now()->subDays(15),Carbon::now()])
                                    ->where('users.user_type',4)->where('users.role_id',$business_type)
                                    ->count();
                                    }

                                    
                                    
                                    if($businessData>0)
                                    {
                                        return $businessData;
                                    }
                                    else
                                    {
                                        return false;
                                    }
                                    
                                    
                                } 
                                
                                
                                public function singleService_count_startMonth( $status,$business_type=null,$reg=null)
                                {
                        $businessData=0;
                                    if(empty($reg))
                                    {
                                        $businessData=DB::table('business_register')
                                    ->join('users','users.id','business_register.user_id')
                                    ->whereMonth('users.created_at',Carbon::now()->format('m'))
                                    ->where('users.status',$status)
                                    ->where('users.user_type',4)->where('users.role_id',$business_type)
                                    ->count();
                                    }
                                    else
                                    {
                                        $businessData=DB::table('business_register')
                                    ->join('users','users.id','business_register.user_id')
                                    ->whereMonth('users.created_at',Carbon::now()->format('m'))
                                    ->where('users.user_type',4)->where('users.role_id',$business_type)
                                    ->count();
                                    }
                                    
                                    
                                    
                                    if($businessData>0)
                                    {
                                        return $businessData;
                                    }
                                    else
                                    {
                                        return false;
                                    }
                                    
                                    
                                }  
                               
                               
                                public function singleService_count_startWeek($status,$business_type=null,$reg=null)
                                {
                        $businessData=0;
                                    if(empty($reg))
                                    {
                                       $businessData=DB::table('business_register')
                                    ->join('users','users.id','business_register.user_id')
                                    ->whereBetween('users.created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])
                                    ->where('users.status',$status)
                                    ->where('users.user_type',4)->where('users.role_id',$business_type)
                                    ->count();
                                     
                                    }
                                    else
                                    {
                                         $businessData=DB::table('business_register')
                                    ->join('users','users.id','business_register.user_id')
                                    ->whereBetween('users.created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])
                                    ->where('users.user_type',4)->where('users.role_id',$business_type)
                                    ->count();
                                   
                                    }
                                    
                                    
                                    if($businessData>0)
                                    {
                                        return $businessData;
                                    }
                                    else
                                    {
                                        return false;
                                    }
                                    
                                    
                                }
                                 public function singleService_count_startYest($status,$business_type=null,$reg=null)
                                {
                        $businessData=0;
                                    if(empty($reg))
                                    {
                                    $businessData=DB::table('business_register')
                                    ->join('users','users.id','business_register.user_id')
                                    ->where('users.created_at',Carbon::yesterday())
                                    ->where('users.status',$status)
                                    ->where('users.user_type',4)->where('users.role_id',$business_type)
                                    ->count(); 
                                    }
                                    else
                                    {
                                    $businessData=DB::table('business_register')
                                    ->join('users','users.id','business_register.user_id')
                                    ->where('users.created_at',Carbon::yesterday())
                                    ->where('users.user_type',4)->where('users.role_id',$business_type)
                                    ->count(); 
                                    }

                                    
                                    
                                    if($businessData>0)
                                    {
                                        return $businessData;
                                    }
                                    else
                                    {
                                        return false;
                                    }
                                    
                                    
                                }
                                
                                public function singleService_count_startToday($status,$business_type=null,$reg=null)
                                {
                        $businessData=0;
                                    if(empty($reg))
                                    {
                                    $businessData=DB::table('business_register')
                                    ->join('users','users.id','business_register.user_id')
                                    ->where('users.created_at',Carbon::today())
                                    ->where('users.status',$status)
                                    ->where('users.user_type',4)->where('users.role_id',$business_type)
                                    ->count();
                                    }
                                    else
                                    {
                                     $businessData=DB::table('business_register')
                                    ->join('users','users.id','business_register.user_id')
                                    ->where('users.created_at',Carbon::today())
                                    ->where('users.user_type',4)->where('users.role_id',$business_type)
                                    ->count();  
                                    }
                                    
                                    if($businessData>0)
                                    {
                                        return $businessData;
                                    }
                                    else
                                    {
                                        return false;
                                    }
                                    
                                    
                                }
                                
                                    public function singleService_count_startTotal($status,$business_type,$reg=null)
                                {
                        $businessData=0;
                                    if(empty($reg))
                                    {
                                    $businessData=DB::table('business_register')
                                    ->join('users','users.id','business_register.user_id')
                                    ->where('users.status',$status)
                                    ->where('users.user_type',4)->where('users.role_id',$business_type)
                                    ->count();
                                    }
                                    else
                                    {
                                     $businessData=DB::table('business_register')
                                    ->join('users','users.id','business_register.user_id')
                                    ->where('users.user_type',4)->where('users.role_id',$business_type)
                                    ->count();
                                    }
                                    
                                    
                                    if($businessData>0)
                                    {
                                        return $businessData;
                                    }
                                    else
                                    {
                                        return false;
                                    }
                                    
                                    
                                }
                                
                                //approve Business s for Kizaku all blow Module
                                public function singleService_count(Request $request)
                                {
                                    
                                    $data=[];
                                    $data['today']=$this->singleService_count_startToday($request->status,$request->business_type);
                                    $data['year']=$this->singleService_count_startYear($request->status,$request->business_type);
                                    $data['fortnight']=$this->singleService_count_startForty($request->status,$request->business_type);
                                    $data['week']=$this->singleService_count_startWeek($request->status,$request->business_type);
                                    $data['yesterday']=$this->singleService_count_startYest($request->status,$request->business_type);
                                    $data['month']=$this->singleService_count_startMonth($request->status,$request->business_type);
                                    $data['total']=$this->singleService_count_startTotal($request->status,$request->business_type,1);
                                    //$data['bearerToken']=$request->bearerToken();
                                    $data['status']=true;
                                    return response()->json($data);
                                }
                                
                                
                                public function singleService_count_pending(Request $request)
                                {
                                    
                                    $data=[];
                                    $data['today']=$this->singleService_count_startToday(1,11);
                                    $data['year']=$this->singleService_count_startYear(1,11);
                                    $data['fortnight']=$this->singleService_count_startForty(1,11);
                                    $data['week']=$this->singleService_count_startWeek(1,11);
                                    $data['yesterday']=$this->singleService_count_startYest(1,11);
                                    $data['month']=$this->singleService_count_startMonth(1,11);
                                    $data['total']=$this->singleService_count_startTotal(1,11,1);
                                    //$data['bearerToken']=$request->bearerToken();
                                    $data['status']=true;
                                    return response()->json($data);
                                }
                                
                                
                                public function singleService_count_rejected(Request $request)
                                {
                                    
                                             $data=[];
                                             $data['today']=$this->singleService_count_startToday(3);
                                            $data['year']=$this->singleService_count_startYear(3);
                                            $data['fortnight']=$this->singleService_count_startForty(3);
                                            $data['week']=$this->singleService_count_startWeek(3);
                                            $data['yesterday']=$this->singleService_count_startYest(3);
                                            $data['month']=$this->singleService_count_startMonth(3);
                                            $data['total']=$this->singleService_count_startTotal(3);
                                    //$data['bearerToken']=$request->bearerToken();
                                    $data['status']=true;
                                    return response()->json($data);
                                }
                                
                                
                                public function singleService_count_registration(Request $request)
                                {
                                    //if we will pass two argumnt then only all registration not deppend on status
                                    $data=[];
                                    $data['today']=$this->singleService_count_startToday(2,1);
                                    $data['year']=$this->singleService_count_startYear(2,1);
                                    $data['fortnight']=$this->singleService_count_startForty(2,1);
                                    $data['week']=$this->singleService_count_startWeek(2,1);
                                    $data['yesterday']=$this->singleService_count_startYest(2,1);
                                    $data['month']=$this->singleService_count_startMonth(2,1);
                                    $data['total']=$this->singleService_count_startTotal(2,1);
                                    //$data['bearerToken']=$request->bearerToken();
                                    $data['status']=true;
                                    return response()->json($data);
                                }
                                
                                public function business_list(Request $request)
                                {
                                   $datalist= DB::table('business_register')
                                    ->join('users','users.id','business_register.user_id')
                                    ->leftjoin('state','state.id','business_register.state_id')
                                    ->leftjoin('district','district.id','business_register.district')
                                    
                                    ->leftjoin('city','city.id','business_register.city_id')
                                    ->where('users.status',$request->status)
                                    ->orderBy('users.created_at','desc')
                                     ->select('business_register.b_name','business_register.conatact_person','users.email','users.mobile','business_register.v_uni_id','business_register.user_id','state.state','district.district_name','city.city','business_register.pincode')->get();
                                    
                                    if(count($datalist)>0)
                                    {
                                        $data['total']=count($datalist);
                                    $data['status']=true;
                                    $data['data']=$datalist;
                                    
                                    }
                                    else
                                    {
                                    $data['status']=true;
                                    $data['data']=$datalist;
                                    }

                                    return response()->json([$data]);
                                }
                                
                                /**
                                 * Business List According to Status 
                                 * 
                                 * */
                                 
                                 /**
                     * filter_by as week,today,this month,year,fortnight,yesterday
                     * 
                     * 
                     * */
                     
                     
                public function business_listYear(Request $request)
                                {
                        $businessData=0;
                                     if(isset($request->status))
                                    {
                                    $businessData=DB::table('business_register')
                                    ->join('users','users.id','business_register.user_id')
                                    ->leftjoin('state','state.id','business_register.state_id')
                                    ->leftjoin('district','district.id','business_register.district')
                                    ->leftjoin('city','city.id','business_register.city_id')
                                    ->whereYear('users.created_at',Carbon::now()->format('Y'))
                                    
                                    ->where('users.status',$request->status)
                                    ->where('users.user_type',4)->where('users.role_id',$request->business_type)
                                     ->select('business_register.b_name','business_register.conatact_person','users.email','users.mobile','business_register.v_uni_id','business_register.user_id','state.state','district.district_name','city.city','business_register.pincode')->get();
                                    }
                                    else
                                    {
                                    $businessData=DB::table('business_register')
                                    ->join('users','users.id','business_register.user_id')
                                    ->leftjoin('state','state.id','business_register.state_id')
                                    ->leftjoin('district','district.id','business_register.district')
                                    
                                    ->leftjoin('city','city.id','business_register.city_id')
                                    ->whereYear('users.created_at',Carbon::now()->format('Y'))
                                    ->where('users.user_type',4)->where('users.role_id',$request->business_type)
                                     ->select('business_register.b_name','business_register.conatact_person','users.email','users.mobile','business_register.v_uni_id','business_register.user_id','state.state','district.district_name','city.city','business_register.pincode')->get();
                                    }
                                    
                                    
                                    if(!empty($businessData))
                                    {
                                        return response()->json(['total'=>count($businessData),'status'=>true,'data'=>$businessData]);
                                    }
                                    else
                                    {
                                        return response()->json(['status'=>0,'data'=>$businessData]);
                                    }
                                    
                                   
                                    
                                } 
                                
                                
                                
                                 public function business_listForty(Request $request)
                                {
                        $businessData=0;
                                     if(isset($request->status))
                                    {
                                    $businessData=DB::table('business_register')
                                    ->join('users','users.id','business_register.user_id')
                                    ->leftjoin('state','state.id','business_register.state_id')
                                    ->leftjoin('district','district.id','business_register.district')
                                    
                                    ->leftjoin('city','city.id','business_register.city_id')
                                    ->whereBetween('users.created_at',[Carbon::now()->subDays(15),Carbon::now()])
                                    ->where('users.status',$request->status)
                                    ->where('users.user_type',4)->where('users.role_id',$request->business_type)
                                     ->select('business_register.b_name','business_register.conatact_person','users.email','users.mobile','business_register.v_uni_id','business_register.user_id','state.state','district.district_name','city.city','business_register.pincode')->get();
                                    }
                                    else
                                    {
                                    $businessData=DB::table('business_register')
                                    ->join('users','users.id','business_register.user_id')
                                    ->leftjoin('state','state.id','business_register.state_id')
                                    ->leftjoin('district','district.id','business_register.district')
                                    
                                    ->leftjoin('city','city.id','business_register.city_id')
                                    ->whereBetween('users.created_at',[Carbon::now()->subDays(15),Carbon::now()])
                                    ->where('users.user_type',4)->where('users.role_id',$request->business_type)
                                     ->select('business_register.b_name','business_register.conatact_person','users.email','users.mobile','business_register.v_uni_id','business_register.user_id','state.state','district.district_name','city.city','business_register.pincode')->get();
                                    }
                                    
                                    
                                    if(!empty($businessData))
                                    {
                                        return response()->json(['total'=>count($businessData),'status'=>true,'data'=>$businessData]);
                                    }
                                    else
                                    {
                                        return response()->json(['status'=>0,'data'=>$businessData]);
                                    }
                                    
                                   
                                    
                                } 
                                
                                
                                public function business_listMonth(Request $request)
                                {
                        $businessData=0;
                         if(isset($request->status))
                                    {
                                    $businessData=DB::table('business_register')
                                    ->join('users','users.id','business_register.user_id')
                                    ->leftjoin('state','state.id','business_register.state_id')
                                    ->leftjoin('district','district.id','business_register.district')
                                    ->leftjoin('city','city.id','business_register.city_id')
                                    
                                    ->whereMonth('users.created_at',Carbon::now()->format('m'))
                                    ->where('users.status',$request->status)
                                    ->where('users.user_type',4)->where('users.role_id',$request->business_type)
                                     ->select('business_register.b_name','business_register.conatact_person','users.email','users.mobile','business_register.v_uni_id','business_register.user_id','state.state','district.district_name','city.city','business_register.pincode')->get();
                                    }
                                    else
                                    {
                                    $businessData=DB::table('business_register')
                                    ->join('users','users.id','business_register.user_id')
                                    ->leftjoin('state','state.id','business_register.state_id')
                                    ->leftjoin('district','district.id','business_register.district')
                                    
                                    ->leftjoin('city','city.id','business_register.city_id')
                                    ->whereMonth('users.created_at',Carbon::now()->format('m'))
                                    ->where('users.user_type',4)->where('users.role_id',$request->business_type)
                                     ->select('business_register.b_name','business_register.conatact_person','users.email','users.mobile','business_register.v_uni_id','business_register.user_id','state.state','district.district_name','city.city','business_register.pincode')->get();
                                    }
                                    
                                    
                                    if(!empty($businessData))
                                    {
                                        return response()->json(['total'=>count($businessData),'status'=>true,'data'=>$businessData]);
                                    }
                                    else
                                    {
                                        return response()->json(['status'=>0,'data'=>$businessData]);
                                    }
                                    
                                   
                                    
                                    
                                }  
                               
                               
                                public function business_listWeek(Request $request)
                                {
                        $businessData=0;
                                     if(isset($request->status))
                                    {
                                    $businessData=DB::table('business_register')
                                    ->join('users','users.id','business_register.user_id')
                                    ->leftjoin('state','state.id','business_register.state_id')
                                    
                                    ->leftjoin('district','district.id','business_register.district')
                                    ->leftjoin('city','city.id','business_register.city_id')
                                    ->whereBetween('users.created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])
                                    ->where('users.status',$request->status)
                                    ->where('users.user_type',4)->where('users.role_id',$request->business_type)
                                     ->select('business_register.b_name','business_register.conatact_person','users.email','users.mobile','business_register.v_uni_id','business_register.user_id','state.state','district.district_name','city.city','business_register.pincode')->get();
                                    }
                                    else
                                    {
                                    $businessData=DB::table('business_register')
                                    ->join('users','users.id','business_register.user_id')
                                    ->leftjoin('state','state.id','business_register.state_id')
                                    ->leftjoin('district','district.id','business_register.district')
                                    
                                    ->leftjoin('city','city.id','business_register.city_id')
                                    ->whereBetween('users.created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])
                                    ->where('users.user_type',4)->where('users.role_id',$request->business_type)
                                     ->select('business_register.b_name','business_register.conatact_person','users.email','users.mobile','business_register.v_uni_id','business_register.user_id','state.state','district.district_name','city.city','business_register.pincode')->get();
                                    }
                                    
                                    
                                    if(!empty($businessData))
                                    {
                                        return response()->json(['total'=>count($businessData),'status'=>true,'data'=>$businessData]);
                                    }
                                    else
                                    {
                                        return response()->json(['status'=>0,'data'=>$businessData]);
                                    }
                                    
                                   
                                    
                                    
                                }
                                 public function business_listYest(Request $request)
                                {
                        $businessData=0;
                                     if(isset($request->status))
                                    {
                                    $businessData=DB::table('business_register')
                                    ->join('users','users.id','business_register.user_id')
                                    ->leftjoin('state','state.id','business_register.state_id')
                                    ->leftjoin('district','district.id','business_register.district')
                                    
                                    ->leftjoin('city','city.id','business_register.city_id')
                                    ->where('users.status',$request->status)
                                    ->where('users.created_at',Carbon::yesterday())
                                    ->where('users.user_type',4)->where('users.role_id',$request->business_type)
                                     ->select('business_register.b_name','business_register.conatact_person','users.email','users.mobile','business_register.v_uni_id','business_register.user_id','state.state','district.district_name','city.city','business_register.pincode')->get();
                                    }
                                    else
                                    {
                                    $businessData=DB::table('business_register')
                                    ->join('users','users.id','business_register.user_id')
                                    ->leftjoin('state','state.id','business_register.state_id')
                                    ->leftjoin('district','district.id','business_register.district')
                                    
                                    ->leftjoin('city','city.id','business_register.city_id')
                                    ->where('users.created_at',Carbon::yesterday())
                                    ->where('users.user_type',4)->where('users.role_id',$request->business_type)
                                     ->select('business_register.b_name','business_register.conatact_person','users.email','users.mobile','business_register.v_uni_id','business_register.user_id','state.state','district.district_name','city.city','business_register.pincode')->get();
                                    }
                                    
                                    
                                    if(!empty($businessData))
                                    {
                                        return response()->json(['total'=>count($businessData),'status'=>true,'data'=>$businessData]);
                                    }
                                    else
                                    {
                                        return response()->json(['status'=>0,'data'=>$businessData]);
                                    }
                                    
                                   
                                    
                                }
                                
                                public function business_listToday(Request $request)
                                {
                        $businessData=0;
                                 if(isset($request->status))
                                    {
                                    $businessData=DB::table('business_register')
                                    ->join('users','users.id','business_register.user_id')
                                    ->leftjoin('state','state.id','business_register.state_id')
                                    ->leftjoin('district','district.id','business_register.district')
                                    
                                    ->leftjoin('city','city.id','business_register.city_id')
                                    ->where('users.status',$request->status)
                                    ->where('users.created_at',Carbon::today())
                                    ->where('users.user_type',4)->where('users.role_id',$request->business_type)
                             ->select('business_register.b_name','business_register.conatact_person','users.email','users.mobile','business_register.v_uni_id','business_register.user_id','state.state','district.district_name','city.city','business_register.pincode')->get();
                                    }
                                    else
                                    {
                                    $businessData=DB::table('business_register')
                                    ->join('users','users.id','business_register.user_id')
                                    ->leftjoin('state','state.id','business_register.state_id')
                                    ->leftjoin('district','district.id','business_register.district')
                                    
                                    ->leftjoin('city','city.id','business_register.city_id')
                                    ->where('users.created_at',Carbon::today())
                                    ->where('users.user_type',4)->where('users.role_id',$request->business_type)
                                     ->select('business_register.b_name','business_register.conatact_person','users.email','users.mobile','business_register.v_uni_id','business_register.user_id','state.state','district.district_name','city.city','business_register.pincode')->get();
                                    }
                                    
                                    
                                    if(!empty($businessData))
                                    {
                                        return response()->json(['total'=>count($businessData),'status'=>true,'data'=>$businessData]);
                                    }
                                    else
                                    {
                                        return response()->json(['status'=>0,'data'=>$businessData]);
                                    }
                                    
                                   
                                    
                                    
                                }
                                
                                    public function business_listTotal(Request $request)
                                {
                                    
                                    /**
                                     * we are using isset coz in post argument status 0 is approved and 1 is deactivate
                                     * 0 is value for isset else false
                                     * ***/
                                         $businessData=0;
                                    if(isset($request->status))
                                    {
                                    $businessData=DB::table('business_register')
                                    ->join('users','users.id','business_register.user_id')
                                    
                                    ->leftjoin('state','state.id','business_register.state_id')
                                    ->leftjoin('district','district.id','business_register.district')
                                    ->leftjoin('city','city.id','business_register.city_id')
                                    ->where('users.status',$request->status)
                                    ->where('users.user_type',4)->where('users.role_id',$request->business_type)
                                     ->select('business_register.b_name','business_register.conatact_person','users.email','users.mobile','business_register.v_uni_id','business_register.user_id','state.state','district.district_name','city.city','business_register.pincode')->get();
                                    }
                                    else
                                    {
                                    $businessData=DB::table('business_register')
                                    ->join('users','users.id','business_register.user_id')
                                    
                                    ->leftjoin('state','state.id','business_register.state_id')
                                    ->leftjoin('district','district.id','business_register.district')
                                    ->leftjoin('city','city.id','business_register.city_id')
                                    ->where('users.user_type',4)->where('users.role_id',$request->business_type)
                                    ->select('business_register.b_name','business_register.conatact_person','users.email','users.mobile','business_register.v_uni_id','business_register.user_id','state.state','district.district_name','city.city','business_register.pincode')->get();
                                    }
                                    
                                    
                                    if(!empty($businessData))
                                    {
                                        return response()->json(['total'=>count($businessData),'status'=>true,'data'=>$businessData]);
                                    }
                                    else
                                    {
                                        return response()->json(['status'=>0,'data'=>$businessData]);
                                    }
                                    
                                    
                                }
                            
                                

                
                                       
  
}

