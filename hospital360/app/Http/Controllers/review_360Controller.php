<?php

namespace App\Http\Controllers;

use App\DataTables\review_360DataTable;
use App\Http\Requests;
use App\Http\Requests\Createreview_360Request;
use App\Http\Requests\Updatereview_360Request;
use App\Repositories\review_360Repository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class review_360Controller extends AppBaseController
{
    /** @var  review_360Repository */
    private $review360Repository;

    public function __construct(review_360Repository $review360Repo)
    {
        $this->review360Repository = $review360Repo;
    }

    /**
     * Display a listing of the review_360.
     *
     * @param review_360DataTable $review360DataTable
     * @return Response
     */
    public function index(review_360DataTable $review360DataTable)
    {
        return $review360DataTable->render('review_360s.index');
    }

    /**
     * Show the form for creating a new review_360.
     *
     * @return Response
     */
    public function create()
    {
        return view('review_360s.create');
    }

    /**
     * Store a newly created review_360 in storage.
     *
     * @param Createreview_360Request $request
     *
     * @return Response
     */
    public function store(Createreview_360Request $request)
    {
        $input = $request->all();

        $review360 = $this->review360Repository->create($input);

        Flash::success('Review 360 saved successfully.');

        return redirect(route('review360s.index'));
    }

    /**
     * Display the specified review_360.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $review360 = $this->review360Repository->find($id);

        if (empty($review360)) {
            Flash::error('Review 360 not found');

            return redirect(route('review360s.index'));
        }

        return view('review_360s.show')->with('review360', $review360);
    }

    /**
     * Show the form for editing the specified review_360.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $review360 = $this->review360Repository->find($id);

        if (empty($review360)) {
            Flash::error('Review 360 not found');

            return redirect(route('review360s.index'));
        }

        return view('review_360s.edit')->with('review360', $review360);
    }

    /**
     * Update the specified review_360 in storage.
     *
     * @param  int              $id
     * @param Updatereview_360Request $request
     *
     * @return Response
     */
    public function update($id, Updatereview_360Request $request)
    {
        $review360 = $this->review360Repository->find($id);

        if (empty($review360)) {
            Flash::error('Review 360 not found');

            return redirect(route('review360s.index'));
        }

        $review360 = $this->review360Repository->update($request->all(), $id);

        Flash::success('Review 360 updated successfully.');

        return redirect(route('review360s.index'));
    }

    /**
     * Remove the specified review_360 from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $review360 = $this->review360Repository->find($id);

        if (empty($review360)) {
            Flash::error('Review 360 not found');

            return redirect(route('review360s.index'));
        }

        $this->review360Repository->delete($id);

        Flash::success('Review 360 deleted successfully.');

        return redirect(route('review360s.index'));
    }
}
