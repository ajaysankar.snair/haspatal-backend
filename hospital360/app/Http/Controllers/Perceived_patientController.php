<?php

namespace App\Http\Controllers;

use App\DataTables\Perceived_patientDataTable;
use App\Http\Requests;
use App\Http\Requests\CreatePerceived_patientRequest;
use App\Http\Requests\UpdatePerceived_patientRequest;
use App\Repositories\Perceived_patientRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Auth;
use Illuminate\Http\Request;
class Perceived_patientController extends AppBaseController
{
    /** @var  Perceived_patientRepository */
    private $perceivedPatientRepository;

    public function __construct(Perceived_patientRepository $perceivedPatientRepo)
    {
        $this->perceivedPatientRepository = $perceivedPatientRepo;
    }

    /**
     * Display a listing of the Perceived_patient.
     *
     * @param Perceived_patientDataTable $perceivedPatientDataTable
     * @return Response
     */
    public function index(Perceived_patientDataTable $perceivedPatientDataTable)
    {
        return $perceivedPatientDataTable->render('perceived_patients.index');
    }

    /**
     * Show the form for creating a new Perceived_patient.
     *
     * @return Response
     */
    public function create()
    {
        return view('perceived_patients.create');
    }

    /**
     * Store a newly created Perceived_patient in storage.
     *
     * @param CreatePerceived_patientRequest $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $input['created_by'] = Auth::user()->id;
        $input['updated_by'] = Auth::user()->id;
        $perceivedPatient = $this->perceivedPatientRepository->create($input);

        if(Auth::user()->role_id == 1){
            Flash::success('Perceived Patient saved successfully.');
            return redirect(route('perceivedPatients.index'));
        }else{
           Flash::success('Perceived / Diagnosis saved successfully.');
            return back();
        }
    }

    /**
     * Display the specified Perceived_patient.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $perceivedPatient = $this->perceivedPatientRepository->find($id);

        if (empty($perceivedPatient)) {
            Flash::error('Perceived Patient not found');

            return redirect(route('perceivedPatients.index'));
        }

        return view('perceived_patients.show')->with('perceivedPatient', $perceivedPatient);
    }

    /**
     * Show the form for editing the specified Perceived_patient.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $perceivedPatient = $this->perceivedPatientRepository->find($id);

        if (empty($perceivedPatient)) {
            Flash::error('Perceived Patient not found');

            return redirect(route('perceivedPatients.index'));
        }

        return view('perceived_patients.edit')->with('perceivedPatient', $perceivedPatient);
    }

    /**
     * Update the specified Perceived_patient in storage.
     *
     * @param  int              $id
     * @param UpdatePerceived_patientRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePerceived_patientRequest $request)
    {
        $perceivedPatient = $this->perceivedPatientRepository->find($id);

        if (empty($perceivedPatient)) {
            Flash::error('Perceived Patient not found');

            return redirect(route('perceivedPatients.index'));
        }

        $perceivedPatient = $this->perceivedPatientRepository->update($request->all(), $id);

        Flash::success('Perceived Patient updated successfully.');

        return redirect(route('perceivedPatients.index'));
    }

    /**
     * Remove the specified Perceived_patient from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $perceivedPatient = $this->perceivedPatientRepository->find($id);

        if (empty($perceivedPatient)) {
            Flash::error('Perceived Patient not found');

            return redirect(route('perceivedPatients.index'));
        }

        $this->perceivedPatientRepository->delete($id);

        Flash::success('Perceived Patient deleted successfully.');

        return redirect(route('perceivedPatients.index'));
    }
}
