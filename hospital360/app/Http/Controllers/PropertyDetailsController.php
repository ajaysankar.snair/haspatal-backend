<?php

namespace App\Http\Controllers;

use App\DataTables\PropertyDetailsDataTable;
use App\Http\Requests;
use App\Http\Requests\CreatePropertyDetailsRequest;
use App\Http\Requests\UpdatePropertyDetailsRequest;
use App\Repositories\PropertyDetailsRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use DB;
use Auth;
class PropertyDetailsController extends AppBaseController
{
    /** @var  PropertyDetailsRepository */
    private $propertyDetailsRepository;

    public function __construct(PropertyDetailsRepository $propertyDetailsRepo)
    {
        $this->propertyDetailsRepository = $propertyDetailsRepo;
    }

    /**
     * Display a listing of the PropertyDetails.
     *
     * @param PropertyDetailsDataTable $propertyDetailsDataTable
     * @return Response
     */
    public function index(PropertyDetailsDataTable $propertyDetailsDataTable)
    {
        return $propertyDetailsDataTable->render('property_details.index');
    }

    /**
     * Show the form for creating a new PropertyDetails.
     *
     * @return Response
     */
    public function create()
    {
        $property_type = DB::table('property_type')->pluck('type','id');
        $countryList = DB::table('country')->pluck('name','id');
        $stateList = DB::table('state')->pluck('state','id');
        $cityList = DB::table('city')->pluck('name','id');
        return view('property_details.create',compact('property_type','countryList','stateList','cityList'));
    }

    /**
     * Store a newly created PropertyDetails in storage.
     *
     * @param CreatePropertyDetailsRequest $request
     *
     * @return Response
     */
    public function store(CreatePropertyDetailsRequest $request)
    {
        $input = $request->all();
        $input['created_by'] = Auth::user()->id;
        $input['updated_by'] = Auth::user()->id;
        $propertyDetails = $this->propertyDetailsRepository->create($input);

        Flash::success('Property Details saved successfully.');

        return redirect(route('propertyDetails.index'));
    }

    /**
     * Display the specified PropertyDetails.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $propertyDetails = $this->propertyDetailsRepository->find($id);

        if (empty($propertyDetails)) {
            Flash::error('Property Details not found');

            return redirect(route('propertyDetails.index'));
        }

        return view('property_details.show')->with('propertyDetails', $propertyDetails);
    }

    /**
     * Show the form for editing the specified PropertyDetails.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $propertyDetails = $this->propertyDetailsRepository->find($id);
        $property_type = DB::table('property_type')->pluck('type','id');
        $countryList = DB::table('country')->pluck('name','id');
        $stateList = DB::table('state')->pluck('state','id');
        $cityList = DB::table('city')->pluck('name','id');
        if (empty($propertyDetails)) {
            Flash::error('Property Details not found');

            return redirect(route('propertyDetails.index'));
        }

        return view('property_details.edit',compact('property_type','countryList','stateList','cityList'))->with('propertyDetails', $propertyDetails);
    }

    /**
     * Update the specified PropertyDetails in storage.
     *
     * @param  int              $id
     * @param UpdatePropertyDetailsRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePropertyDetailsRequest $request)
    {
        $propertyDetails = $this->propertyDetailsRepository->find($id);

        if (empty($propertyDetails)) {
            Flash::error('Property Details not found');

            return redirect(route('propertyDetails.index'));
        }

        $propertyDetails = $this->propertyDetailsRepository->update($request->all(), $id);

        Flash::success('Property Details updated successfully.');

        return redirect(route('propertyDetails.index'));
    }

    /**
     * Remove the specified PropertyDetails from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $propertyDetails = $this->propertyDetailsRepository->find($id);

        if (empty($propertyDetails)) {
            Flash::error('Property Details not found');

            return redirect(route('propertyDetails.index'));
        }

        $this->propertyDetailsRepository->delete($id);

        Flash::success('Property Details deleted successfully.');

        return redirect(route('propertyDetails.index'));
    }
}
