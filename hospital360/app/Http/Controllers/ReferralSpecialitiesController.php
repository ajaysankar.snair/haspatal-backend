<?php

namespace App\Http\Controllers;

use App\DataTables\ReferralSpecialitiesDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateReferralSpecialitiesRequest;
use App\Http\Requests\UpdateReferralSpecialitiesRequest;
use App\Repositories\ReferralSpecialitiesRepository;
use App\Http\Controllers\AppBaseController;
use Flash;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Illuminate\View\View;
use Response;

class ReferralSpecialitiesController extends AppBaseController
{
    /** @var  ReferralSpecialitiesRepository */
    private $referralSpecialitiesRepository;

    public function __construct(ReferralSpecialitiesRepository $referralSpecialitiesRepo)
    {
        $this->referralSpecialitiesRepository = $referralSpecialitiesRepo;
    }

    /**
     * Display a listing of the ReferralSpecialities.
     *
     * @param ReferralSpecialitiesDataTable $referralSpecialitiesDataTable
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function index(ReferralSpecialitiesDataTable $referralSpecialitiesDataTable)
    {
        return $referralSpecialitiesDataTable->render('referral_specialities.index');
    }

    /**
     * Show the form for creating a new ReferralSpecialities.
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function create()
    {
        return view('referral_specialities.create');
    }

    /**
     * Store a newly created ReferralSpecialities in storage.
     *
     * @param CreateReferralSpecialitiesRequest $request
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function store(CreateReferralSpecialitiesRequest $request)
    {
        $input = $request->all();

        $referralSpecialities = $this->referralSpecialitiesRepository->create($input);

        Flash::success('Referral Specialities saved successfully.');

        return redirect(route('referralSpecialities.index'));
    }

    /**
     * Display the specified ReferralSpecialities.
     *
     * @param  int $id
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function show($id)
    {
        $referralSpecialities = $this->referralSpecialitiesRepository->find($id);

        if (empty($referralSpecialities)) {
            Flash::error('Referral Specialities not found');

            return redirect(route('referralSpecialities.index'));
        }

        return view('referral_specialities.show')->with('referralSpecialities', $referralSpecialities);
    }

    /**
     * Show the form for editing the specified ReferralSpecialities.
     *
     * @param  int $id
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function edit($id)
    {
        $referralSpecialities = $this->referralSpecialitiesRepository->find($id);

        if (empty($referralSpecialities)) {
            Flash::error('Referral Specialities not found');

            return redirect(route('referralSpecialities.index'));
        }

        return view('referral_specialities.edit')->with('referralSpecialities', $referralSpecialities);
    }

    /**
     * Update the specified ReferralSpecialities in storage.
     *
     * @param  int              $id
     * @param UpdateReferralSpecialitiesRequest $request
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function update($id, UpdateReferralSpecialitiesRequest $request)
    {
        $referralSpecialities = $this->referralSpecialitiesRepository->find($id);

        if (empty($referralSpecialities)) {
            Flash::error('Referral Specialities not found');

            return redirect(route('referralSpecialities.index'));
        }

        $referralSpecialities = $this->referralSpecialitiesRepository->update($request->all(), $id);

        Flash::success('Referral Specialities updated successfully.');

        return redirect(route('referralSpecialities.index'));
    }

    /**
     * Remove the specified ReferralSpecialities from storage.
     *
     * @param  int $id
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function destroy($id)
    {
        $referralSpecialities = $this->referralSpecialitiesRepository->find($id);

        if (empty($referralSpecialities)) {
            Flash::error('Referral Specialities not found');

            return redirect(route('referralSpecialities.index'));
        }

        $this->referralSpecialitiesRepository->delete($id);

        Flash::success('Referral Specialities deleted successfully.');

        return redirect(route('referralSpecialities.index'));
    }
}
