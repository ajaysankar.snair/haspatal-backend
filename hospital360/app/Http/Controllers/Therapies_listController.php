<?php

namespace App\Http\Controllers;

use App\DataTables\Therapies_listDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateTherapies_listRequest;
use App\Http\Requests\UpdateTherapies_listRequest;
use App\Repositories\Therapies_listRepository;
use App\Http\Controllers\AppBaseController;
use Flash;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Illuminate\View\View;
use Response;

class Therapies_listController extends AppBaseController
{
    /** @var  Therapies_listRepository */
    private $therapiesListRepository;

    public function __construct(Therapies_listRepository $therapiesListRepo)
    {
        $this->therapiesListRepository = $therapiesListRepo;
    }

    /**
     * Display a listing of the Therapies_list.
     *
     * @param Therapies_listDataTable $therapiesListDataTable
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function index(Therapies_listDataTable $therapiesListDataTable)
    {
        return $therapiesListDataTable->render('therapies_lists.index');
    }

    /**
     * Show the form for creating a new Therapies_list.
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function create()
    {
        return view('therapies_lists.create');
    }

    /**
     * Store a newly created Therapies_list in storage.
     *
     * @param CreateTherapies_listRequest $request
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function store(CreateTherapies_listRequest $request)
    {
        $input = $request->all();

        $therapiesList = $this->therapiesListRepository->create($input);

        Flash::success('Therapies List saved successfully.');

        return redirect(route('therapiesLists.index'));
    }

    /**
     * Display the specified Therapies_list.
     *
     * @param  int $id
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function show($id)
    {
        $therapiesList = $this->therapiesListRepository->find($id);

        if (empty($therapiesList)) {
            Flash::error('Therapies List not found');

            return redirect(route('therapiesLists.index'));
        }

        return view('therapies_lists.show')->with('therapiesList', $therapiesList);
    }

    /**
     * Show the form for editing the specified Therapies_list.
     *
     * @param  int $id
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function edit($id)
    {
        $therapiesList = $this->therapiesListRepository->find($id);

        if (empty($therapiesList)) {
            Flash::error('Therapies List not found');

            return redirect(route('therapiesLists.index'));
        }

        return view('therapies_lists.edit')->with('therapiesList', $therapiesList);
    }

    /**
     * Update the specified Therapies_list in storage.
     *
     * @param  int              $id
     * @param UpdateTherapies_listRequest $request
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function update($id, UpdateTherapies_listRequest $request)
    {
        $therapiesList = $this->therapiesListRepository->find($id);

        if (empty($therapiesList)) {
            Flash::error('Therapies List not found');

            return redirect(route('therapiesLists.index'));
        }

        $therapiesList = $this->therapiesListRepository->update($request->all(), $id);

        Flash::success('Therapies List updated successfully.');

        return redirect(route('therapiesLists.index'));
    }

    /**
     * Remove the specified Therapies_list from storage.
     *
     * @param  int $id
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function destroy($id)
    {
        $therapiesList = $this->therapiesListRepository->find($id);

        if (empty($therapiesList)) {
            Flash::error('Therapies List not found');

            return redirect(route('therapiesLists.index'));
        }

        $this->therapiesListRepository->delete($id);

        Flash::success('Therapies List deleted successfully.');

        return redirect(route('therapiesLists.index'));
    }
}
