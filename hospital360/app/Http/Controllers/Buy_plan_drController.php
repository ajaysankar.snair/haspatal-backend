<?php

namespace App\Http\Controllers;

use App\DataTables\Buy_plan_drDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateBuy_plan_drRequest;
use App\Http\Requests\UpdateBuy_plan_drRequest;
use App\Repositories\Buy_plan_drRepository;
use App\Http\Controllers\AppBaseController;
use Flash;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Illuminate\View\View;
use Response;

class Buy_plan_drController extends AppBaseController
{
    /** @var  Buy_plan_drRepository */
    private $buyPlanDrRepository;

    public function __construct(Buy_plan_drRepository $buyPlanDrRepo)
    {
        $this->buyPlanDrRepository = $buyPlanDrRepo;
    }

    /**
     * Display a listing of the Buy_plan_dr.
     *
     * @param Buy_plan_drDataTable $buyPlanDrDataTable
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function index(Buy_plan_drDataTable $buyPlanDrDataTable)
    {
        return $buyPlanDrDataTable->render('buy_plan_drs.index');
    }

    /**
     * Show the form for creating a new Buy_plan_dr.
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function create()
    {
        return view('buy_plan_drs.create');
    }

    /**
     * Store a newly created Buy_plan_dr in storage.
     *
     * @param CreateBuy_plan_drRequest $request
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function store(CreateBuy_plan_drRequest $request)
    {
        $input = $request->all();

        $buyPlanDr = $this->buyPlanDrRepository->create($input);

        Flash::success('Buy Plan Dr saved successfully.');

        return redirect(route('buyPlanDrs.index'));
    }

    /**
     * Display the specified Buy_plan_dr.
     *
     * @param  int $id
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function show($id)
    {
        $buyPlanDr = $this->buyPlanDrRepository->find($id);

        if (empty($buyPlanDr)) {
            Flash::error('Buy Plan Dr not found');

            return redirect(route('buyPlanDrs.index'));
        }

        return view('buy_plan_drs.show')->with('buyPlanDr', $buyPlanDr);
    }

    /**
     * Show the form for editing the specified Buy_plan_dr.
     *
     * @param  int $id
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function edit($id)
    {
        $buyPlanDr = $this->buyPlanDrRepository->find($id);

        if (empty($buyPlanDr)) {
            Flash::error('Buy Plan Dr not found');

            return redirect(route('buyPlanDrs.index'));
        }

        return view('buy_plan_drs.edit')->with('buyPlanDr', $buyPlanDr);
    }

    /**
     * Update the specified Buy_plan_dr in storage.
     *
     * @param  int              $id
     * @param UpdateBuy_plan_drRequest $request
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function update($id, UpdateBuy_plan_drRequest $request)
    {
        $buyPlanDr = $this->buyPlanDrRepository->find($id);

        if (empty($buyPlanDr)) {
            Flash::error('Buy Plan Dr not found');

            return redirect(route('buyPlanDrs.index'));
        }

        $buyPlanDr = $this->buyPlanDrRepository->update($request->all(), $id);

        Flash::success('Buy Plan Dr updated successfully.');

        return redirect(route('buyPlanDrs.index'));
    }

    /**
     * Remove the specified Buy_plan_dr from storage.
     *
     * @param  int $id
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function destroy($id)
    {
        $buyPlanDr = $this->buyPlanDrRepository->find($id);

        if (empty($buyPlanDr)) {
            Flash::error('Buy Plan Dr not found');

            return redirect(route('buyPlanDrs.index'));
        }

        $this->buyPlanDrRepository->delete($id);

        Flash::success('Buy Plan Dr deleted successfully.');

        return redirect(route('buyPlanDrs.index'));
    }
}
