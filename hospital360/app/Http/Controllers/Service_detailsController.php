<?php

namespace App\Http\Controllers;

use App\DataTables\Service_detailsDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateService_detailsRequest;
use App\Http\Requests\UpdateService_detailsRequest;
use App\Repositories\Service_detailsRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use DB;

class Service_detailsController extends AppBaseController
{
    /** @var  Service_detailsRepository */
    private $serviceDetailsRepository;

    public function __construct(Service_detailsRepository $serviceDetailsRepo)
    {
        $this->serviceDetailsRepository = $serviceDetailsRepo;
    }

    /**
     * Display a listing of the Service_details.
     *
     * @param Service_detailsDataTable $serviceDetailsDataTable
     * @return Response
     */
    public function index(Service_detailsDataTable $serviceDetailsDataTable)
    {
        return $serviceDetailsDataTable->render('service_details.index');
    }

    /**
     * Show the form for creating a new Service_details.
     *
     * @return Response
     */
    public function create()
    {
        return view('service_details.create');
    }

    /**
     * Store a newly created Service_details in storage.
     *
     * @param CreateService_detailsRequest $request
     *
     * @return Response
     */
    public function store(CreateService_detailsRequest $request)
    {
        $input = $request->all();

        $serviceDetails = $this->serviceDetailsRepository->create($input);

        Flash::success('Service Details saved successfully.');

        return redirect(route('serviceDetails.index'));
    }

    /**
     * Display the specified Service_details.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $serviceDetails = $this->serviceDetailsRepository->find($id);

        if (empty($serviceDetails)) {
            Flash::error('Service Details not found');

            return redirect(route('serviceDetails.index'));
        }

        return view('service_details.show')->with('serviceDetails', $serviceDetails);
    }

    /**
     * Show the form for editing the specified Service_details.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $serviceDetails = $this->serviceDetailsRepository->find($id);

        if (empty($serviceDetails)) {
            Flash::error('Service Details not found');

            return redirect(route('serviceDetails.index'));
        }

        return view('service_details.edit')->with('serviceDetails', $serviceDetails);
    }

    /**
     * Update the specified Service_details in storage.
     *
     * @param  int              $id
     * @param UpdateService_detailsRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateService_detailsRequest $request)
    {
        $serviceDetails = $this->serviceDetailsRepository->find($id);

        if (empty($serviceDetails)) {
            Flash::error('Service Details not found');

            return redirect(route('serviceDetails.index'));
        }

        $serviceDetails = $this->serviceDetailsRepository->update($request->all(), $id);

        Flash::success('Service Details updated successfully.');

        return redirect(route('serviceDetails.index'));
    }

    /**
     * Remove the specified Service_details from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $serviceDetails = $this->serviceDetailsRepository->find($id);

        if (empty($serviceDetails)) {
            Flash::error('Service Details not found');

            return redirect(route('serviceDetails.index'));
        }
                
        //$this->serviceDetailsRepository->delete($id);
        DB::table('service_details')->where('id',$id)->delete();
        Flash::success('Service Details deleted successfully.');

        return redirect(route('serviceDetails.index'));
    }
}
