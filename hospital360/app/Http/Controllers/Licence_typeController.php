<?php

namespace App\Http\Controllers;

use App\DataTables\Licence_typeDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateLicence_typeRequest;
use App\Http\Requests\UpdateLicence_typeRequest;
use App\Repositories\Licence_typeRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Auth;
use DB;
class Licence_typeController extends AppBaseController
{
    /** @var  Licence_typeRepository */
    private $licenceTypeRepository;

    public function __construct(Licence_typeRepository $licenceTypeRepo)
    {
        $this->licenceTypeRepository = $licenceTypeRepo;
    }

    /**
     * Display a listing of the Licence_type.
     *
     * @param Licence_typeDataTable $licenceTypeDataTable
     * @return Response
     */
    public function index(Licence_typeDataTable $licenceTypeDataTable)
    {
        return $licenceTypeDataTable->render('licence_types.index');
    }

    /**
     * Show the form for creating a new Licence_type.
     *
     * @return Response
     */
    public function create()
    {
        return view('licence_types.create');
    }

    /**
     * Store a newly created Licence_type in storage.
     *
     * @param CreateLicence_typeRequest $request
     *
     * @return Response
     */
    public function store(CreateLicence_typeRequest $request)
    {
        $input = $request->all();
        $input['created_by'] = Auth::user()->id;
        $input['updated_by'] = Auth::user()->id;
        $licenceType = $this->licenceTypeRepository->create($input);

        Flash::success('Licence Type saved successfully.');

        return redirect(route('licenceTypes.index'));
    }

    /**
     * Display the specified Licence_type.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $licenceType = $this->licenceTypeRepository->find($id);

        if (empty($licenceType)) {
            Flash::error('Licence Type not found');

            return redirect(route('licenceTypes.index'));
        }

        return view('licence_types.show')->with('licenceType', $licenceType);
    }

    /**
     * Show the form for editing the specified Licence_type.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $licenceType = $this->licenceTypeRepository->find($id);

        if (empty($licenceType)) {
            Flash::error('Licence Type not found');

            return redirect(route('licenceTypes.index'));
        }

        return view('licence_types.edit')->with('licenceType', $licenceType);
    }

    /**
     * Update the specified Licence_type in storage.
     *
     * @param  int              $id
     * @param UpdateLicence_typeRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateLicence_typeRequest $request)
    {
        $licenceType = $this->licenceTypeRepository->find($id);

        if (empty($licenceType)) {
            Flash::error('Licence Type not found');

            return redirect(route('licenceTypes.index'));
        }

        $licenceType = $this->licenceTypeRepository->update($request->all(), $id);

        Flash::success('Licence Type updated successfully.');

        return redirect(route('licenceTypes.index'));
    }

    /**
     * Remove the specified Licence_type from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $licenceType = $this->licenceTypeRepository->find($id);

        if (empty($licenceType)) {
            Flash::error('Licence Type not found');

            return redirect(route('licenceTypes.index'));
        }

        $this->licenceTypeRepository->delete($id);

        Flash::success('Licence Type deleted successfully.');

        return redirect(route('licenceTypes.index'));
    }
}
