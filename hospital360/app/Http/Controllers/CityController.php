<?php

namespace App\Http\Controllers;

use App\DataTables\CityDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateCityRequest;
use App\Http\Requests\UpdateCityRequest;
use App\Repositories\CityRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use DB;
use Auth;
use Illuminate\Http\Request;
use App\Imports\CityImport;
use Maatwebsite\Excel\Facades\Excel;

class CityController extends AppBaseController
{
    /** @var  CityRepository */
    private $cityRepository;

    public function __construct(CityRepository $cityRepo)
    {
        $this->cityRepository = $cityRepo;
    }

    /**
     * Display a listing of the City.
     *
     * @param CityDataTable $cityDataTable
     * @return Response
     */
    public function index(CityDataTable $cityDataTable)
    {
        $city = \App\Models\City::join('country','country.id','city.country_name')
                    ->join('state','state.id','city.state_name')
                    ->select('city.*','country.country','state.state')->get();

        return view('cities.index',compact('city'));
    }

    /**
     * Show the form for creating a new City.
     *
     * @return Response
     */
    public function create()
    {
        $countryList = \App\Models\Country::orderBy('country','asc')->pluck('country','id');
        $stateList = \App\Models\State::orderBy('state','asc')->pluck('state','id');   
        return view('cities.create',compact('countryList','stateList'));
    }

    /**
     * Store a newly created City in storage.
     *
     * @param CreateCityRequest $request
     *
     * @return Response
     */
    public function store(CreateCityRequest $request)
    {
        $input = $request->all();
        $input['created_by'] = Auth::user()->id;
        $input['updated_by'] = Auth::user()->id;
        $city = $this->cityRepository->create($input);

        Flash::success('City saved successfully.');

        return redirect(route('cities.index'));
    }

    /**
     * Display the specified City.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        //$city = $this->cityRepository->find($id);
        $city = DB::table('city')->join('country','country.id','city.country_name')
                    ->join('state','state.id','city.state_name')
                    ->select('city.*','country.country','state.state')->where('city.id',$id)->first();
        if (empty($city)) {
            Flash::error('City not found');

            return redirect(route('cities.index'));
        }

        return view('cities.show')->with('city', $city);
    }

    /**
     * Show the form for editing the specified City.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $city = $this->cityRepository->find($id);
        $countryList = DB::table('country')->pluck('country','id');
        $stateList = DB::table('state')->pluck('state','id');
        if (empty($city)) {
            Flash::error('City not found');

            return redirect(route('cities.index'));
        }

        return view('cities.edit',compact('countryList','stateList'))->with('city', $city);
    }

    /**
     * Update the specified City in storage.
     *
     * @param  int              $id
     * @param UpdateCityRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCityRequest $request)
    {
        $city = $this->cityRepository->find($id);

        if (empty($city)) {
            Flash::error('City not found');

            return redirect(route('cities.index'));
        }

        $city = $this->cityRepository->update($request->all(), $id);

        Flash::success('City updated successfully.');

        return redirect(route('cities.index'));
    }

    /**
     * Remove the specified City from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $city = $this->cityRepository->find($id);

        if (empty($city)) {
            Flash::error('City not found');

            return redirect(route('cities.index'));
        }

        $this->cityRepository->delete($id);

        Flash::success('City deleted successfully.');

        return redirect(route('cities.index'));
    }
    
    public function cityImport()
    {
         $countryList = \App\Models\Country::orderBy('country','asc')->pluck('country','id');
        $stateList = \App\Models\State::orderBy('state','asc')->pluck('state','id');  
        return view('cities.import',compact('countryList','stateList'));
    }
    
    public function cityImportPost(Request $request) 
    {
        //echo"<pre>";print_r($request->city);exit;
        $data['country']=$request->country_name;
        $data['state']=$request->state_name;
        //$path = $request->file('city')->getRealPath();
        Excel::import(new CityImport($data), request()->file('city'));
        
        return redirect(route('cities.index'))->with('success', 'All good!');
    }
}
