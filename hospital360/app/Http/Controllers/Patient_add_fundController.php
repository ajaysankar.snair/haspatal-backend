<?php

namespace App\Http\Controllers;

use App\DataTables\Patient_add_fundDataTable;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Requests\CreatePatient_add_fundRequest;
use App\Http\Requests\UpdatePatient_add_fundRequest;
use App\Repositories\Patient_add_fundRepository;
use Flash;
use App\Models\Patient_add_fund;
use App\Http\Controllers\AppBaseController;
use Response;
use Auth;
use DB;

class Patient_add_fundController extends AppBaseController
{
    /** @var  Patient_add_fundRepository */
    private $patientAddFundRepository;

    public function __construct(Patient_add_fundRepository $patientAddFundRepo)
    {
        $this->patientAddFundRepository = $patientAddFundRepo;
    }

    /**
     * Display a listing of the Patient_add_fund.
     *
     * @param Patient_add_fundDataTable $patientAddFundDataTable
     * @return Response
     */
    public function index(Patient_add_fundDataTable $patientAddFundDataTable)
    {
        return $patientAddFundDataTable->render('patient_add_funds.index');
    }

    /**
     * Show the form for creating a new Patient_add_fund.
     *
     * @return Response
     */
    public function create()
    {
        return view('patient_add_funds.create');
    }

    /**
     * Store a newly created Patient_add_fund in storage.
     *
     * @param CreatePatient_add_fundRequest $request
     *
     * @return Response
     */
    public function store(request $request)
    {
        //echo "string"; exit;
        $input = $request->all();
        $input['created_by'] = Auth::user()->id;
        $input['updated_by'] = Auth::user()->id;
        $input['doctor_id'] = Auth::user()->userDetailsId; 
        $last_amount = Patient_add_fund::where('doctor_id',Auth::user()->userDetailsId)->first();
        if(!empty($last_amount)){
          $amount = $last_amount->dr_amount + $request->dr_amount;
          $patientAddFund = DB::table('patient_add_fund')->where('doctor_id',Auth::user()->userDetailsId)->update(['dr_amount' => $amount,'payment_id' => $request->payment_id]);
           if(Auth::user()->role_id == 1){            
             Flash::success('Doctor Add Fund saved successfully.');
            
             return redirect(route('patientAddFunds.index'));
            }else{

             Flash::success('Doctor Add Fund saved successfully.');
           
              return response()->json(['data' => 'true']);
            }
        }else{
             $patientAddFund = $this->patientAddFundRepository->create($input);
              if(Auth::user()->role_id == 1){
            
           Flash::success('Doctor Add Fund saved successfully.');
             return redirect(route('patientAddFunds.index'));
            }else{

                 Flash::success('Doctor Add Fund saved successfully.');
                
                
                 return response()->json(['data' => 'true']);
            }
          }  


        
    }

    /**
     * Display the specified Patient_add_fund.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $patientAddFund = $this->patientAddFundRepository->find($id);

        if (empty($patientAddFund)) {
            Flash::error('Patient Add Fund not found');

            return redirect(route('patientAddFunds.index'));
        }

        return view('patient_add_funds.show')->with('patientAddFund', $patientAddFund);
    }

    /**
     * Show the form for editing the specified Patient_add_fund.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $patientAddFund = $this->patientAddFundRepository->find($id);

        if (empty($patientAddFund)) {
            Flash::error('Patient Add Fund not found');

            return redirect(route('patientAddFunds.index'));
        }

        return view('patient_add_funds.edit')->with('patientAddFund', $patientAddFund);
    }

    /**
     * Update the specified Patient_add_fund in storage.
     *
     * @param  int              $id
     * @param UpdatePatient_add_fundRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePatient_add_fundRequest $request)
    {
        $patientAddFund = $this->patientAddFundRepository->find($id);

        if (empty($patientAddFund)) {
            Flash::error('Patient Add Fund not found');

            return redirect(route('patientAddFunds.index'));
        }

        $patientAddFund = $this->patientAddFundRepository->update($request->all(), $id);

        Flash::success('Patient Add Fund updated successfully.');

        return redirect(route('patientAddFunds.index'));
    }

    /**
     * Remove the specified Patient_add_fund from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $patientAddFund = $this->patientAddFundRepository->find($id);

        if (empty($patientAddFund)) {
            Flash::error('Patient Add Fund not found');

            return redirect(route('patientAddFunds.index'));
        }

        $this->patientAddFundRepository->delete($id);

        Flash::success('Patient Add Fund deleted successfully.');

        return redirect(route('patientAddFunds.index'));
    }
}
