<?php

namespace App\Http\Controllers;
use App\DataTables\hospitalAddFundDataTable;
use App\Http\Requests;
use App\Http\Requests\CreatehospitalAddFundRequest;
use App\Http\Requests\UpdatehospitalAddFundRequest;
use App\Repositories\hospitalAddFundRepository;
use App\Http\Controllers\AppBaseController;
use Flash;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Illuminate\View\View;
use Response;
use App\Models\hospitalAddFund;
use Auth;

class hospitalAddFundController extends AppBaseController
{
    /** @var  hospitalAddFundRepository */
    private $hospitalAddFundRepository;

    public function __construct(hospitalAddFundRepository $hospitalAddFundRepo)
    {
        $this->hospitalAddFundRepository = $hospitalAddFundRepo;
    }

    /**

     * Display a listing of the hospitalAddFund.

     *

     * @param hospitalAddFundDataTable $hospitalAddFundDataTable

     * @return Response|Factory|RedirectResponse|Redirector|View

     */

    public function index(hospitalAddFundDataTable $hospitalAddFundDataTable)
    {
        return $hospitalAddFundDataTable->render('hospital_add_funds.index');
    }

    /**

     * Show the form for creating a new hospitalAddFund.

     *

     * @return Response|Factory|RedirectResponse|Redirector|View

     */

    public function create()
    {
        return view('hospital_add_funds.create');
    }
    /**

     * Store a newly created hospitalAddFund in storage.

     *

     * @param CreatehospitalAddFundRequest $request

     *

     * @return Response|Factory|RedirectResponse|Redirector|View

     */

    public function store(CreatehospitalAddFundRequest $request)
    {
        $input = $request->all();
        $input = $request->all();
        $input['created_by'] = Auth::user()->id;
        $input['updated_by'] = Auth::user()->id;
        $input['hospital_id'] = Auth::user()->userDetailsId; 

        $last_amount = hospitalAddFund::where('hospital_id',Auth::user()->userDetailsId)->first();

        if(!empty($last_amount)){

            $amount = $last_amount->h_amount + $request->h_amount;
            $patientAddFund = DB::table('hospitalAddFund')->where('hospital_id',Auth::user()->userDetailsId)->update(['h_amount' => $amount,'payment_id' => $request->payment_id]);

            if(Auth::user()->role_id == 1){            
                Flash::success('Doctor Add Fund saved successfully.');
            
                return redirect(route('patientAddFunds.index'));
            }else{

                Flash::success('Doctor Add Fund saved successfully.');
                return back();
                return response()->json(['data' => 'true']);
            }
        }else{

            $hospitalAddFund = $this->hospitalAddFundRepository->create($input);
            Flash::success('Hospital Add Fund saved successfully.');

            return back();
            //return redirect(route('hospitalAddFunds.index'));
        }





    }



    /**

     * Display the specified hospitalAddFund.

     *

     * @param  int $id

     *

     * @return Response|Factory|RedirectResponse|Redirector|View

     */

    public function show($id)

    {

        $hospitalAddFund = $this->hospitalAddFundRepository->find($id);



        if (empty($hospitalAddFund)) {

            Flash::error('Hospital Add Fund not found');



            return redirect(route('hospitalAddFunds.index'));

        }



        return view('hospital_add_funds.show')->with('hospitalAddFund', $hospitalAddFund);

    }



    /**

     * Show the form for editing the specified hospitalAddFund.

     *

     * @param  int $id

     *

     * @return Response|Factory|RedirectResponse|Redirector|View

     */

    public function edit($id)

    {

        $hospitalAddFund = $this->hospitalAddFundRepository->find($id);



        if (empty($hospitalAddFund)) {

            Flash::error('Hospital Add Fund not found');



            return redirect(route('hospitalAddFunds.index'));

        }



        return view('hospital_add_funds.edit')->with('hospitalAddFund', $hospitalAddFund);

    }



    /**

     * Update the specified hospitalAddFund in storage.

     *

     * @param  int              $id

     * @param UpdatehospitalAddFundRequest $request

     *

     * @return Response|Factory|RedirectResponse|Redirector|View

     */

    public function update($id, UpdatehospitalAddFundRequest $request)

    {

        $hospitalAddFund = $this->hospitalAddFundRepository->find($id);



        if (empty($hospitalAddFund)) {

            Flash::error('Hospital Add Fund not found');



            return redirect(route('hospitalAddFunds.index'));

        }



        $hospitalAddFund = $this->hospitalAddFundRepository->update($request->all(), $id);



        Flash::success('Hospital Add Fund updated successfully.');



        return redirect(route('hospitalAddFunds.index'));

    }



    /**

     * Remove the specified hospitalAddFund from storage.

     *

     * @param  int $id

     *

     * @return Response|Factory|RedirectResponse|Redirector|View

     */

    public function destroy($id)

    {

        $hospitalAddFund = $this->hospitalAddFundRepository->find($id);



        if (empty($hospitalAddFund)) {

            Flash::error('Hospital Add Fund not found');



            return redirect(route('hospitalAddFunds.index'));

        }



        $this->hospitalAddFundRepository->delete($id);



        Flash::success('Hospital Add Fund deleted successfully.');



        return redirect(route('hospitalAddFunds.index'));

    }

}

