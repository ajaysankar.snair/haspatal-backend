<?php

namespace App\Http\Controllers;

use App\DataTables\Your_coverage_360DataTable;
use App\Http\Requests;
use App\Http\Requests\CreateYour_coverage_360Request;
use App\Http\Requests\UpdateYour_coverage_360Request;
use App\Repositories\Your_coverage_360Repository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Auth;
use DB;

class Your_coverage_360Controller extends AppBaseController
{
    /** @var  Your_coverage_360Repository */
    private $yourCoverage360Repository;

    public function __construct(Your_coverage_360Repository $yourCoverage360Repo)
    {
        $this->yourCoverage360Repository = $yourCoverage360Repo;
    }

    /**
     * Display a listing of the Your_coverage_360.
     *
     * @param Your_coverage_360DataTable $yourCoverage360DataTable
     * @return Response
     */
    public function index(Your_coverage_360DataTable $yourCoverage360DataTable)
    {
        return $yourCoverage360DataTable->render('your_coverage_360s.index');
    }

    /**
     * Show the form for creating a new Your_coverage_360.
     *
     * @return Response
     */
    public function create()
    {
        return view('your_coverage_360s.create');
    }

    /**
     * Store a newly created Your_coverage_360 in storage.
     *
     * @param CreateYour_coverage_360Request $request
     *
     * @return Response
     */
    public function store(CreateYour_coverage_360Request $request)
    {
        $input = $request->all();
          $input['created_by'] = Auth::user()->id;
        $input['updated_by'] = Auth::user()->id;
        $yourCoverage360 = $this->yourCoverage360Repository->create($input);

        Flash::success('Your Coverage 360 saved successfully.');

        return redirect(route('yourCoverage360s.index'));
    }

    /**
     * Display the specified Your_coverage_360.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $yourCoverage360 = $this->yourCoverage360Repository->find($id);

        if (empty($yourCoverage360)) {
            Flash::error('Your Coverage 360 not found');

            return redirect(route('yourCoverage360s.index'));
        }

        return view('your_coverage_360s.show')->with('yourCoverage360', $yourCoverage360);
    }

    /**
     * Show the form for editing the specified Your_coverage_360.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $yourCoverage360 = $this->yourCoverage360Repository->find($id);

        if (empty($yourCoverage360)) {
            Flash::error('Your Coverage 360 not found');

            return redirect(route('yourCoverage360s.index'));
        }

        return view('your_coverage_360s.edit')->with('yourCoverage360', $yourCoverage360);
    }

    /**
     * Update the specified Your_coverage_360 in storage.
     *
     * @param  int              $id
     * @param UpdateYour_coverage_360Request $request
     *
     * @return Response
     */
    public function update($id, UpdateYour_coverage_360Request $request)
    {
        $yourCoverage360 = $this->yourCoverage360Repository->find($id);

        if (empty($yourCoverage360)) {
            Flash::error('Your Coverage 360 not found');

            return redirect(route('yourCoverage360s.index'));
        }

        $yourCoverage360 = $this->yourCoverage360Repository->update($request->all(), $id);

        Flash::success('Your Coverage 360 updated successfully.');

        return redirect(route('yourCoverage360s.index'));
    }

    /**
     * Remove the specified Your_coverage_360 from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $yourCoverage360 = $this->yourCoverage360Repository->find($id);

        if (empty($yourCoverage360)) {
            Flash::error('Your Coverage 360 not found');

            return redirect(route('yourCoverage360s.index'));
        }

        $this->yourCoverage360Repository->delete($id);

        Flash::success('Your Coverage 360 deleted successfully.');

        return redirect(route('yourCoverage360s.index'));
    }
}
