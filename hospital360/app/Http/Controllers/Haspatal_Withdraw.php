<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\CreatehospitalWithdrawRequest;

use App\Http\Requests\UpdatehospitalWithdrawRequest;

use App\Repositories\hospitalWithdrawRepository;

use App\Http\Controllers\AppBaseController;

use Flash;

use Illuminate\Contracts\View\Factory;

use Illuminate\Http\RedirectResponse;

use Illuminate\Routing\Redirector;

use Illuminate\View\View;

use Response;
use Validator;
use Auth;
use DB;


class Haspatal_Withdraw extends Controller
{
    
     public function create()

    {
                    
                    $data=DB::table('haspatal_regsiter')->select('wallet_amount')->where('id',Auth::user()->userDetailsId)->get();
                    
                    $wallet_amount=$data[0]->wallet_amount;
        return view('hospital_withdraws.create',compact('wallet_amount'));

    }



    /**

     * Store a newly created hospitalWithdraw in storage.

     *

     * @param CreatehospitalWithdrawRequest $request

     *

     * @return Response|Factory|RedirectResponse|Redirector|View

     */

    public function store(Request $request)

    {
        
                $validator = Validator::make($request->all(), [
            'requested_amount' => 'required|numeric',
            'bank_name' => 'required',
            'ifsc_code'=>'required',
            'account_name'=>'required',
            'account_type'=>'required',
            'cheque_img'=>'required|image'
            
        ]);
        if ($validator->fails()) {
          return redirect('/hospitalWithdraws/create')
                   ->withErrors($validator)
                   ->withInput();
        }
        $chimg="";
        if($request->hasfile('cheque_img'))
            {
                $image1 = $request->file('cheque_img');
                $extension1 = $image1->getClientOriginalExtension(); // getting image extension
                $filename1 =time().'__'.$image1->getClientOriginalName();
                $path1 = public_path('/media/cheque_img/');
                $image1->move($path1, $filename1);
                $chimg ='public/media/cheque_img/'.$filename1;
            }
                        
                        $amtdata=DB::table('haspatal_regsiter')->select('wallet_amount')->where('id',Auth::user()->userDetailsId)->get();
                        if((int)$request->requested_amount>(int)$amtdata[0]->wallet_amount)
                        {
                            Flash::error('Requested amount is greater than Wallet Amount');
                         
                          return redirect('/hospitalWithdraws/create')
                   ->withErrors($validator)
                   ->withInput();
                        }
        
        //$all=$request->all();
       // insert('insert into buy_plan_haspatal (orderId,user_id,plan_id,payable_amount,payment_id,payment_status) values(?,?,?,?,?,?)',[$req->razorpay_order_id,Auth::user()->userDetailsId,$req->plan_id,$req->amount,$req->razorpay_payment_id,1])
        $data=DB::insert('insert into haspatal_withdraw(requested_amount,bank_name,ifsc_code,haspatal_id,account_name,account_type,cheque_img) values(?,?,?,?,?,?,?)',[$request->requested_amount,$request->bank_name,$request->ifsc_code,Auth::user()->userDetailsId,$request->account_name,$request->account_type,$chimg]);
        //return response()->json([$all]);
        if($data)
        {
            Flash::success("Withdraw Requested Successfully");
        }
        else
        {
            Flash::errors('Failed to request withdraw');
        }
        return redirect('/hospitalWithdraws');
    }

        public function index()
        {
                
            $data=DB::table('haspatal_withdraw')
            ->select('id','bank_name','account_name','ifsc_code','requested_amount','account_type','cheque_img','created_at','status')
            ->get();
            $section="wallet";
          return  view('hospital_withdraws.index',compact('data','section'));
        }

    /**

     * Display the specified hospitalWithdraw.

     *

     * @param  int $id

     *

     * @return Response|Factory|RedirectResponse|Redirector|View

     */

    public function show($id)

    {


    }



    /**

     * Show the form for editing the specified hospitalWithdraw.

     *

     * @param  int $id

     *

     * @return Response|Factory|RedirectResponse|Redirector|View

     */

    public function edit($id)

    {
        $hospitalWithdraw=DB::table('haspatal_withdraw')->find($id);
         $data=DB::table('haspatal_regsiter')->select('wallet_amount')->where('id',Auth::user()->userDetailsId)->get();
                    
                    $wallet_amount=$data[0]->wallet_amount;
        if(!$hospitalWithdraw)
        {
           Flash::errors('Hospital Withdraws not found'); 
        }
        
       // $hospitalWithdraw=DB::table('haspatal_withdraw')->find($id);
            return view('hospital_withdraws.edit',compact('wallet_amount'))->with('hospitalWithdraw', $hospitalWithdraw);
    }



    /**

     * Update the specified hospitalWithdraw in storage.

     *

     * @param  int              $id

     * @param UpdatehospitalWithdrawRequest $request

     *

     * @return Response|Factory|RedirectResponse|Redirector|View

     */

    public function update($id,Request $request)

    {
                    $all=$request->all();
         return   response()->json([$all]);
                    
    }



    /**

     * Remove the specified hospitalWithdraw from storage.

     *

     * @param  int $id

     *

     * @return Response|Factory|RedirectResponse|Redirector|View

     */

    public function destroy($id)

    {

    }
    //ajax data return
    
     public function viewStatement(Request $request)
                                {
                                    $wallet="wallet";
                                    $startDate = \Carbon\Carbon::createFromFormat('Y-m-d',$request->from);
                                    $endDate = \Carbon\Carbon::createFromFormat('Y-m-d',$request->to);

                                $data=DB::table('haspatal_withdraw')
                                ->select('id','bank_name','account_name','ifsc_code','requested_amount','account_type','cheque_img','created_at','status')
                                ->where('haspatal_id',Auth::user()->userDetailsId)
                                    ->whereBetween('created_at',[$startDate,$endDate])
                                    ->orderBy('created_at')
                                    ->get();
                                                       
                                        ?>
                                            
                           <div class="table-responsive-lg">
                        <div class="alert alert-success text-center"><?= ' View Statement Between   '.$request->from.'  To Date '.$request->to ?></div>
                    <table class="table">
                        <th>Sr.</th><th>Requested Amount</th><th>Bank Name</th><th>IFSC Code</th><th>Account Type</th><th>Cheque Image</th><th>Time</th><th>Status</th>
                        <?php
                         
                        if(count($data)>0)
                        {
                        
                        ?>
                        
                                    <?php $sr=1; 
                                    foreach($data as $row)
                                    {
                                        ?>
                                    
                                    <tr>
                                        <td><?= $sr?></td>
                                        <td><?= $row->requested_amount?></td>
                                        <td><?= $row->bank_name?></td>
                                        <td><?= $row->ifsc_code?></td>
                                        <td><?= $row->account_type?></td>
                                        <td><a class="badge badge-info" href="{{url($row->cheque_img)}}">View</a></td>
                                        <td><?= $row->created_at?></td>
                                        <td><?= $row->status?'Activated':'Pending'?></td>
                                    </tr>
                                <?php    $sr++; 
                                    }
                                    ?>
                                    

                                    <?php } else { echo"<div class='alert alert-danger'>Data not available !!</div>";} ?>
                        
                    </table>
                    
                </div>   
                                        
                                        <?php
                               
                                }
}
