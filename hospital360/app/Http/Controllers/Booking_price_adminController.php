<?php

namespace App\Http\Controllers;

use App\DataTables\Booking_price_adminDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateBooking_price_adminRequest;
use App\Http\Requests\UpdateBooking_price_adminRequest;
use App\Repositories\Booking_price_adminRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Auth;
use DB;

class Booking_price_adminController extends AppBaseController
{
    /** @var  Booking_price_adminRepository */
    private $bookingPriceAdminRepository;

    public function __construct(Booking_price_adminRepository $bookingPriceAdminRepo)
    {
        $this->bookingPriceAdminRepository = $bookingPriceAdminRepo;
    }

    /**
     * Display a listing of the Booking_price_admin.
     *
     * @param Booking_price_adminDataTable $bookingPriceAdminDataTable
     * @return Response
     */
    public function index(Booking_price_adminDataTable $bookingPriceAdminDataTable)
    {
        return $bookingPriceAdminDataTable->render('booking_price_admins.index');
    }

    /**
     * Show the form for creating a new Booking_price_admin.
     *
     * @return Response
     */
    public function create()
    {
        return view('booking_price_admins.create');
    }

    /**
     * Store a newly created Booking_price_admin in storage.
     *
     * @param CreateBooking_price_adminRequest $request
     *
     * @return Response
     */
    public function store(CreateBooking_price_adminRequest $request)
    {
        $input = $request->all();
        $input['created_by'] = Auth::user()->id;
        $input['updated_by'] = Auth::user()->id;
        $bookingPriceAdmin = $this->bookingPriceAdminRepository->create($input);

        $result = \App\Models\Doctors::all();

        foreach ($result as $value) {

            $n_price = $value->price + $input['book_price'];

            DB::table('doctor_details')->where('id',$value->id)->update(['price' => $n_price,'updated_at' => \Carbon\Carbon::now()]);
        }

        Flash::success('Booking Price Admin saved successfully.');

        return redirect(route('bookingPriceAdmins.index'));
    }

    /**
     * Display the specified Booking_price_admin.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $bookingPriceAdmin = $this->bookingPriceAdminRepository->find($id);

        if (empty($bookingPriceAdmin)) {
            Flash::error('Booking Price Admin not found');

            return redirect(route('bookingPriceAdmins.index'));
        }

        return view('booking_price_admins.show')->with('bookingPriceAdmin', $bookingPriceAdmin);
    }

    /**
     * Show the form for editing the specified Booking_price_admin.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $bookingPriceAdmin = $this->bookingPriceAdminRepository->find($id);

        if (empty($bookingPriceAdmin)) {
            Flash::error('Booking Price Admin not found');

            return redirect(route('bookingPriceAdmins.index'));
        }

        return view('booking_price_admins.edit')->with('bookingPriceAdmin', $bookingPriceAdmin);
    }

    /**
     * Update the specified Booking_price_admin in storage.
     *
     * @param  int              $id
     * @param UpdateBooking_price_adminRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateBooking_price_adminRequest $request)
    {
        $bookingPriceAdmin = $this->bookingPriceAdminRepository->find($id);

        if (empty($bookingPriceAdmin)) {
            Flash::error('Booking Price Admin not found');

            return redirect(route('bookingPriceAdmins.index'));
        }

        $bookingPriceAdmin = $this->bookingPriceAdminRepository->update($request->all(), $id);

        //$result = \App\Models\Doctors::all();

        /*foreach ($result as $value) {

            $n_price = $value->price + $request->book_price;

            DB::table('doctor_details')->where('id',$value->id)->update(['price' => $n_price,'updated_at' => \Carbon\Carbon::now()]);
        }*/

        Flash::success('Booking Price Admin updated successfully.');

        return redirect(route('bookingPriceAdmins.edit',["id"=>7]));
    }

    /**
     * Remove the specified Booking_price_admin from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $bookingPriceAdmin = $this->bookingPriceAdminRepository->find($id);

        if (empty($bookingPriceAdmin)) {
            Flash::error('Booking Price Admin not found');

            return redirect(route('bookingPriceAdmins.index'));
        }

        $this->bookingPriceAdminRepository->delete($id);

        Flash::success('Booking Price Admin deleted successfully.');

        return redirect(route('bookingPriceAdmins.index'));
    }
}
