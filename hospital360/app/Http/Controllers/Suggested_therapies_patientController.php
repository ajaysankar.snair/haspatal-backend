<?php

namespace App\Http\Controllers;

use App\DataTables\Suggested_therapies_patientDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateSuggested_therapies_patientRequest;
use App\Http\Requests\UpdateSuggested_therapies_patientRequest;
use App\Repositories\Suggested_therapies_patientRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Auth;
use Illuminate\Http\Request;
class Suggested_therapies_patientController extends AppBaseController
{
    /** @var  Suggested_therapies_patientRepository */
    private $suggestedTherapiesPatientRepository;

    public function __construct(Suggested_therapies_patientRepository $suggestedTherapiesPatientRepo)
    {
        $this->suggestedTherapiesPatientRepository = $suggestedTherapiesPatientRepo;
    }

    /**
     * Display a listing of the Suggested_therapies_patient.
     *
     * @param Suggested_therapies_patientDataTable $suggestedTherapiesPatientDataTable
     * @return Response
     */
    public function index(Suggested_therapies_patientDataTable $suggestedTherapiesPatientDataTable)
    {
        return $suggestedTherapiesPatientDataTable->render('suggested_therapies_patients.index');
    }

    /**
     * Show the form for creating a new Suggested_therapies_patient.
     *
     * @return Response
     */
    public function create()
    {
        return view('suggested_therapies_patients.create');
    }

    /**
     * Store a newly created Suggested_therapies_patient in storage.
     *
     * @param CreateSuggested_therapies_patientRequest $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $input['created_by'] = Auth::user()->id;
        $input['updated_by'] = Auth::user()->id;
        $suggestedTherapiesPatient = $this->suggestedTherapiesPatientRepository->create($input);

        if(Auth::user()->role_id == 1){
            Flash::success('Suggested Therapies Patient saved successfully.');

            return redirect(route('suggestedTherapiesPatients.index'));
        }else{
            Flash::success('Suggested Therapies saved successfully.');
            return back();
        }
    }

    /**
     * Display the specified Suggested_therapies_patient.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $suggestedTherapiesPatient = $this->suggestedTherapiesPatientRepository->find($id);

        if (empty($suggestedTherapiesPatient)) {
            Flash::error('Suggested Therapies Patient not found');

            return redirect(route('suggestedTherapiesPatients.index'));
        }

        return view('suggested_therapies_patients.show')->with('suggestedTherapiesPatient', $suggestedTherapiesPatient);
    }

    /**
     * Show the form for editing the specified Suggested_therapies_patient.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $suggestedTherapiesPatient = $this->suggestedTherapiesPatientRepository->find($id);

        if (empty($suggestedTherapiesPatient)) {
            Flash::error('Suggested Therapies Patient not found');

            return redirect(route('suggestedTherapiesPatients.index'));
        }

        return view('suggested_therapies_patients.edit')->with('suggestedTherapiesPatient', $suggestedTherapiesPatient);
    }

    /**
     * Update the specified Suggested_therapies_patient in storage.
     *
     * @param  int              $id
     * @param UpdateSuggested_therapies_patientRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSuggested_therapies_patientRequest $request)
    {
        $suggestedTherapiesPatient = $this->suggestedTherapiesPatientRepository->find($id);

        if (empty($suggestedTherapiesPatient)) {
            Flash::error('Suggested Therapies Patient not found');

            return redirect(route('suggestedTherapiesPatients.index'));
        }

        $suggestedTherapiesPatient = $this->suggestedTherapiesPatientRepository->update($request->all(), $id);

        Flash::success('Suggested Therapies Patient updated successfully.');

        return redirect(route('suggestedTherapiesPatients.index'));
    }

    /**
     * Remove the specified Suggested_therapies_patient from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $suggestedTherapiesPatient = $this->suggestedTherapiesPatientRepository->find($id);

        if (empty($suggestedTherapiesPatient)) {
            Flash::error('Suggested Therapies Patient not found');

            return redirect(route('suggestedTherapiesPatients.index'));
        }

        $this->suggestedTherapiesPatientRepository->delete($id);

        Flash::success('Suggested Therapies Patient deleted successfully.');

        return redirect(route('suggestedTherapiesPatients.index'));
    }
}
