<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\State_master;
use App\Models\City_master;
use App\Models\Doctors;
use App\Models\Hospital_register;
use App\Models\Doctor_availability;
use App\Models\Patients;
use App\Models\TimeSlot;
use App\User;
use App\Models\Set_Consult_Prices;
use App\Models\BookingRequest;
use App\Models\Present_complaint_patient;
use App\Models\My_note_patient;
use App\Models\Patient_add_fund;
use App\Models\Doctor_withdraw;
use App\Models\Prescription_details;
use App\Models\Booking_price_admin;
use Auth;
use DB;
use Carbon\Carbon;
use DateTime;
use Flash;
use App\Models\HaspatalRegsiter;
use App\Models\hospitalAddFund;
use App\Models\hospitalWithdraw;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $total_patients =  DB::table('patient_details')->count();
        $total_doctors =  DB::table('doctor_details')->count();
        $total_haspatal_regsiter =  DB::table('haspatal_regsiter')->count();

        return view('home',compact('total_doctors','total_haspatal_regsiter','total_patients'));
    }
    public function new()
    {
        return view('new');
    }
    public function user_active($id)
    {

      $booklist =  DB::table('users')->where('userDetailsId',$id)->where('user_type',5)->update(['status' => 1]);
      Flash::success('Haspatal Regsiter updated successfully.');

      return redirect(route('haspatalRegsiters.index'));

    }
    public function user_deactive($id)
    {
      $booklist =  DB::table('users')->where('userDetailsId',$id)->where('user_type',5)->update(['status' => 0]);
      Flash::success('Haspatal Regsiter updated successfully.');
      return redirect(route('haspatalRegsiters.index'));

    }


    public function support()
    {
        return view('newview.support.support');
    }

    public function profile()
    {
        return view('newview.profile_menu.profile');
    }

    public function mybio()
    {
        $doctor_id = Auth::user()->userDetailsId;
        //echo $doctor_id; exit;
        $doctors = Doctors::join('specilities', 'doctor_details.speciality', '=', 'specilities.id')
        ->join('language', 'doctor_details.language', '=', 'language.id')
        ->select('doctor_details.*', 'specilities.specility as specility_name', 'language.language as language_name')
        ->where('doctor_details.id',$doctor_id)
        ->first();

         // $users = DB::table('doctor_details')
         //    ->join('specilities', 'doctor_details.speciality', '=', 'specilities.id')
         //    ->join('language', 'doctor_details.language', '=', 'language.id')
         //    ->select('doctor_details.*', 'specilities.*', 'language.*')
         //    ->where('id',$doctor_id)->first();

          //echo "<pre>"; print_r($doctors); exit();

        return view('newview.profile_menu.mybio',compact('doctors','doctor_id'));
    }

    public function address()
    {

        $doctor_id = Auth::user()->userDetailsId;
        $countryList = DB::table('country')->pluck('country','id');
        $stateList = DB::table('state')->pluck('state','id');
        $cityList = DB::table('city')->pluck('city','id');
        $doctors = Doctors::where('id',$doctor_id)->first();

         return view('newview.profile_menu.address',compact('doctors','doctor_id','countryList','stateList','cityList'));
    }
    public function contact()
    {
    	$doctor_id = Auth::user()->userDetailsId;
        $doctors = Doctors::where('id',$doctor_id)->first();
        return view('newview.profile_menu.contact',compact('doctors','doctor_id'));
    } 
    public function license(Request $request)
    {
    	  $doctor_id = Auth::user()->userDetailsId;
        $doctors = Doctors::where('id',$doctor_id)->first();
        $licenceTyes= DB::table('licence_type')->pluck('licence_name','id');
        if($request->hasfile('licence_copy'))
        {
            $image = $request->file('licence_copy');
            $extension = $image->getClientOriginalExtension(); // getting image extension
            $media_photos =time().'__'.$image->getClientOriginalName();
            $path = public_path('/media/licence_copy/');
            $image->move($path, $media_photos);
            $doctors['licence_copy']= $media_photos;

        }
        return view('newview.profile_menu.license',compact('doctors','doctor_id','licenceTyes'));
    }
    public function password(Request $request)
    {
  	  $doctor_id = Auth::user()->userDetailsId;
      $doctors = Doctors::where('id',$doctor_id)->first();
     
          return view('newview.profile_menu.password',compact('doctors','doctor_id'));
      }
    public function profilepic(Request $request)
    {
    	$doctor_id = Auth::user()->userDetailsId;
        $doctors = Doctors::where('id',$doctor_id)->first();
        if($request->hasfile('profile_pic'))
        {
            $image = $request->file('profile_pic');
            $extension = $image->getClientOriginalExtension(); // getting image extension
            $media_photos =time().'__'.$image->getClientOriginalName();
            $path = public_path('/media/profile_pic/');
            $image->move($path, $media_photos);
            $doctors['profile_pic']= $media_photos;

        }
        //print_r($doctors); exit();
        return view('newview.profile_menu.profilepic',compact('doctors','doctor_id','input'));
    }

    public function consults()
    {
        return view('newview.consults_menu.consults');
    }
     public function setting()
    {
        return view('newview.consults_menu.consults');
    }

    public function patients_queue()
    {
    	
        $allcount = DB::table('booking_request')->where('doctor_name', Auth::user()->userDetailsId)->where('status',1)->whereDate('booking_request.updated_at', Carbon::today())->count();
        $check_count = DB::table('booking_request')->where('doctor_name', Auth::user()->userDetailsId)->where('status',3)->whereDate('booking_request.updated_at', Carbon::today())->count();
        $waiting_count = DB::table('booking_request')->where('doctor_name', Auth::user()->userDetailsId)->where('status',2)->whereDate('booking_request.updated_at', Carbon::today())->count();
        return view('newview.consults_menu.patients_queue',compact('allcount','check_count','waiting_count'));
    } 
    public function tostart()
    {
        $doctor_id = Auth::user()->userDetailsId;
        $patients = BookingRequest::join('patient_details','booking_request.patient_name','patient_details.id')
                              ->select('booking_request.*','patient_details.first_name','patient_details.last_name','patient_details.id as patient_id')
                              ->where('booking_request.status',2)
                              ->whereDate('booking_request.updated_at', Carbon::today())
                              ->where('booking_request.doctor_name', $doctor_id)
                              ->get();
    /*echo "<pre>";
    print_r($patients); exit;*/
	   return view('newview.consults_menu.tostart',compact('patients'));
    }
    public function tocomplete()
    {

        $doctor_id = Auth::user()->userDetailsId;
       // $patients = Patients::join('booking_request','', 'booking_request.patient_name')
        $patients = BookingRequest::join('patient_details','booking_request.patient_name','patient_details.id')
                              ->select('booking_request.*','patient_details.first_name','patient_details.last_name','patient_details.id as patient_id')
                              ->where('booking_request.status',3)
                              ->whereDate('booking_request.updated_at', Carbon::today())
                              ->where('booking_request.doctor_name', $doctor_id)
                              ->get();
 
     return view('newview.consults_menu.tocomplete',compact('patients'));
    }
    public function reschedule()
    {

        $doctor_id = Auth::user()->userDetailsId;
       // $patients = Patients::join('booking_request','', 'booking_request.patient_name')
        $patients = BookingRequest::join('patient_details','booking_request.patient_name','patient_details.id')
                              ->select('booking_request.*','patient_details.first_name','patient_details.last_name','patient_details.id as patient_id')
                              ->where('booking_request.status',4)
                              ->whereDate('booking_request.updated_at', Carbon::today())
                              ->where('booking_request.doctor_name', $doctor_id)
                              ->get();
 
     return view('newview.consults_menu.reschedule',compact('patients'));
    }

     public function cancel()
    {

        $doctor_id = Auth::user()->userDetailsId;
       // $patients = Patients::join('booking_request','', 'booking_request.patient_name')
        $patients = BookingRequest::join('patient_details','booking_request.patient_name','patient_details.id')
                              ->select('booking_request.*','patient_details.first_name','patient_details.last_name','patient_details.id as patient_id')
                              ->where('booking_request.status',5)
                              ->whereDate('booking_request.updated_at', Carbon::today())
                              ->where('booking_request.doctor_name', $doctor_id)
                              ->get();
 
     return view('newview.consults_menu.cancel',compact('patients'));
    }
    
    
    public function patients_profile($id,$b_id)
    {
        $doctor = Auth::user()->uid; 
        $patient = User::where('userDetailsId',$id)->select('uid as patient_cuid')->first(); 
        $book_id_data = BookingRequest::where('book_id',$b_id)->select('book_id')->first(); 
        $complaint_patient = Present_complaint_patient::where('book_id',$b_id)->first(); 
        $my_note_patient = My_note_patient::where('m_book_id',$b_id)->get(); 
       // $booklist =  DB::table('booking_request')->where('book_id',$b_id)->update(['status' => 2]);

        // echo "<pre>";
        // print_r($complaint_patient); exit;
        $detail = Patients::where('id',$id)->first();
       // echo $patients_uid; exit;,
        return view('newview.consults_menu.patients_profile',compact('doctor','patient','detail','book_id_data','complaint_patient','my_note_patient'));
    }
//      public function patients_complaint($id,$b_id)
//     {
//         $doctor = Auth::user()->uid; 
//         $patient = User::where('userDetailsId',$id)->select('uid as patient_cuid')->first(); 
//         $book_id_data = BookingRequest::where('book_id',$b_id)->select('book_id')->first(); 
//         $complaint_patient = Present_complaint_patient::where('book_id',$b_id)->first(); 
//         $my_note_patient = My_note_patient::where('m_book_id',$b_id)->get(); 
//        // $booklist =  DB::table('booking_request')->where('book_id',$b_id)->update(['status' => 2]);
// /*
//         echo "<pre>";
//         print_r($my_note_patient); exit;*/
//         $detail = Patients::where('id',$id)->first();
//        // echo $patients_uid; exit;,
//         return view('newview.consults_menu.patients_complaint',compact('doctor','patient','detail','book_id_data','complaint_patient','my_note_patient'));
//     } 
   
    

    
    public function wallet()
    {
        return view('newview.wallet_menu.wallet');
    }
    public function current_balance()
    {
        $doctor = Auth::user()->userDetailsId; 
      
        $amount = BookingRequest::where('doctor_name',$doctor)->sum('f_amount');
       
        return view('newview.wallet_menu.current_balance',compact('amount'));
    }
    public function refund_request()
    {
        $doctor = Auth::user()->userDetailsId;       
        $amount = BookingRequest::where('doctor_name',$doctor)->sum('f_amount');
        $total_fund =  DB::table('patient_add_fund')->where('doctor_id', $doctor)->first();
        $dr_amount = Doctor_withdraw::where('doctor_id',$doctor)->sum('requested_amount');
        $total_amount = $amount + $total_fund->dr_amount - $dr_amount;
        // $total_amount = $amount + $total_fund->dr_amount;
        //echo $total_amount; exit;
         return view('newview.wallet_menu.refund_request',compact('total_amount'));
    }
    public function set_prices()
    {
         $doctor_id = Auth::user()->userDetailsId;

         $doctors = Doctors::join('specilities', 'doctor_details.speciality', '=', 'specilities.id')
                    ->join('language', 'doctor_details.language', '=', 'language.id')
                    ->select('doctor_details.*', 'specilities.specility as specility_name', 'language.language as language_name')
                    ->where('doctor_details.id',$doctor_id)
                    ->first();

         return view('newview.wallet_menu.set_prices',compact('doctor_id','doctors'));
     }
   public function banking_info()
   {
        $doctor_id = Auth::user()->userDetailsId;
        //echo $doctor_id; exit;
        $doctors = Doctors::join('specilities', 'doctor_details.speciality', '=', 'specilities.id')
                  ->join('language', 'doctor_details.language', '=', 'language.id')
                  ->select('doctor_details.*', 'specilities.specility as specility_name', 'language.language as language_name')
                  ->where('doctor_details.id',$doctor_id)
                  ->first();
    
    return view('newview.wallet_menu.banking_info',compact('doctor_id','doctors'));
   }
   public function add_fund()
   {
     $doctor_id = Auth::user()->userDetailsId;
     $last_amount = Patient_add_fund::where('doctor_id',$doctor_id)->first();
     return view('newview.wallet_menu.dr_add_fund',compact('last_amount'));
   }

   public function account_statement()
   {
     return view('newview.wallet_menu.account_statement');
   } 
    public function account_statement_view(Request $request)
   {
     if(!empty($request->s_date)){
       $startDate =  Carbon::parse($request->s_date)->startOfDay(); 
       $endDate =  Carbon::now()->endOfDay();
       $doctor_id = Auth::user()->userDetailsId;
       $dr_amount = Doctor_withdraw::where('doctor_id',$doctor_id)
                    ->whereBetween('Doctor_withdraw.created_at', [$startDate, $endDate]) 
                    ->get();
      }
     return view('newview.wallet_menu.account_statement',compact('dr_amount'));
   } 

      public function records()
     {
           return view('newview.records_menu.records');
      }
      public function myconsults_records()
     {
           return view('newview.records_menu.myconsults_records');
      }
    public function myconsults_records2(Request $request)
     {
        $doctor_id = Auth::user()->userDetailsId;
        $startDate =  Carbon::parse($request->s_date)->startOfDay(); 
        $endDate =  Carbon::now()->endOfDay();
        $patients = BookingRequest::join('patient_details','booking_request.patient_name','patient_details.id')
                              ->select('booking_request.*','patient_details.first_name','patient_details.last_name','patient_details.id as patient_id')
                              ->where('booking_request.status',3)
                              ->whereBetween('booking_request.created_at', [$startDate, $endDate]) 
                              ->where('booking_request.doctor_name', $doctor_id)
                              ->get();
      
           return view('newview.records_menu.myconsults_records2',compact('patients'));
      }
    public function myvisit_summaries()
         {
               return view('newview.records_menu.myvisit_summaries');
          }
    public function myvisit_summaries2(Request $request)
    {
       $doctor_id = Auth::user()->userDetailsId;
        $startDate =  Carbon::parse($request->s_date)->startOfDay(); 
        $endDate =  Carbon::now()->endOfDay();
       $myNotePatients = My_note_patient::where('my_doctor_id',$doctor_id)
                         ->join('patient_details', 'my_note_patient.my_patient_id', '=', 'patient_details.id')
                         ->join('doctor_details', 'my_note_patient.my_doctor_id', '=', 'doctor_details.id')
                         ->whereBetween('my_note_patient.created_at', [$startDate, $endDate])
                         ->select('my_note_patient.*','patient_details.first_name as patient','doctor_details.first_name','doctor_details.last_name')
                        ->get();
      /* echo "<pre>";
        print_r($myNotePatients); exit;*/
      return view('newview.records_menu.myvisit_summaries2',compact('myNotePatients'));
    }
    public function mybookings()
   {
         return view('newview.records_menu.mybookings');
    }
    public function mybookings2()
   {
         return view('newview.records_menu.mybookings2');
    }
    public function my_prescriptions()
     {
           return view('newview.records_menu.my_prescriptions');
      }
    public function my_prescriptions2(Request $request)
    {
        $doctor_id = Auth::user()->userDetailsId;
        $startDate =  Carbon::parse($request->s_date)->startOfDay(); 
        $endDate =  Carbon::now()->endOfDay();
        $prescription_details = Prescription_details::where('prescription_details.doctor_id', $doctor_id)
                                        ->join('patient_details', 'prescription_details.patient_id', '=', 'patient_details.id')
                                        ->join('booking_request', 'prescription_details.doctor_id', '=', 'booking_request.doctor_name')
                                        ->whereBetween('prescription_details.created_at', [$startDate, $endDate])
                                        ->select('prescription_details.*','patient_details.first_name','patient_details.last_name','booking_request.date_time')
                                        ->get();
      /*  echo "<pre>";
        print_r($result); exit;*/
         return view('newview.records_menu.my_prescriptions2',compact($prescription_details));
    }
    public function reviews_menu()
         {
               return view('newview.reviews_menu.reviews_menu');
          }
    public function my_reviews_summary()
         {
               return view('newview.reviews_menu.my_reviews_summary');
          }
    public function reviews()
         {
               return view('newview.reviews_menu.reviews');
          }
    public function score_card()
         {
               return view('newview.reviews_menu.score_card');
          }
    public function responce()
         {
               return view('newview.reviews_menu.responce');
          }

    public function tiemslots()
         {
          $doctor_id = Auth::user()->userDetailsId;
          $doctors = TimeSlot::where('doctor_id',$doctor_id)->first();
          return view('newview.settings_menu.timeslots',compact('doctors','doctor_id'));
          }


      public function setting_doctor_availability()
         {  
          $doctor_id = Auth::user()->userDetailsId;
          $doctors = Doctor_availability::where('id',$doctor_id)->first();

         // echo "<pre>"; print_r($doctors); exit();
          return view('newview.settings_menu.doctor_availability',compact('doctors','doctor_id'));
          }

    public function stateList(Request $request)
    {
        $states = \App\Models\State::where("country_id",$request->country_id)->orderBy('state','asc')->pluck("state","id");
        //echo "<pre>";print_r($states);exit;
        return response()->json($states);
    }
      public function cityList(Request $request)
    {
        $city = \App\Models\City::where("state_name",$request->state_id)->orderBy('city','desc')->pluck("city","id");
        return response()->json($city);
    }
    
     public function appoiment_done(Request $request)
    {
        $booklist =  DB::table('booking_request')->where('book_id',$request->book_id)->update(['status' => 3,'updated_at' => Carbon::now()]);
        return response()->json($booklist);
       // return redirect(route('tostart')); 
    }

    public function settings_menu()
      {
         return view('newview.settings_menu.settings');
      }



    // New Design Start 


    public function design()
      {
        $doctor_id = Auth::user()->userDetailsId;
        $patient_name = Patients::join('booking_request','patient_details.id','booking_request.patient_name')
                          ->where('booking_request.doctor_name',Auth::user()->userDetailsId)
                           ->groupBy('booking_request.patient_name')
                           ->select('booking_request.*','patient_details.first_name','patient_details.id')
                           ->pluck('first_name','patient_details.id');

         return view('newview.new.home',compact('patient_name'));
      }

      public function homepagedetails(Request $request)
        {
         
          $startDate =  Carbon::parse($request->s_date)->startOfDay();
          $endDate =  Carbon::now()->endOfDay();
          if($request->s_date != ''){
              $patient_name = Patients::join('booking_request','patient_details.id','booking_request.patient_name')
                          ->where('booking_request.doctor_name',Auth::user()->userDetailsId)
                           ->groupBy('booking_request.patient_name')
                           ->select('booking_request.*','patient_details.first_name','patient_details.id')
                           ->pluck('first_name','patient_details.id');

           $patients_data = BookingRequest::where('booking_request.patient_name',$request->patient_id)
                         ->whereBetween('booking_request.created_at', [$startDate, $endDate])
                        ->join('patient_details', 'booking_request.patient_name', '=', 'patient_details.id')
                         ->orderBy('booking_request.id','DESC')
                         ->select('booking_request.*','patient_details.first_name','patient_details.last_name','patient_details.id as patient_id')
                         ->get();
                //echo "<pre>"; print_r($patients); exit();
          }else{
              $patient_name = Patients::join('booking_request','patient_details.id','booking_request.patient_name')
                          ->where('booking_request.doctor_name',Auth::user()->userDetailsId)
                           ->groupBy('booking_request.patient_name')
                           ->select('booking_request.*','patient_details.first_name','patient_details.id')
                           ->pluck('first_name','patient_details.id');
             $patients_data = BookingRequest::where('booking_request.patient_name',$request->patient_id)
                        ->join('patient_details', 'booking_request.patient_name', '=', 'patient_details.id')
                         ->orderBy('booking_request.id','DESC')
                         ->select('booking_request.*','patient_details.first_name','patient_details.last_name','patient_details.id as patient_id')
                         ->get();
          }
          //echo "<pre>";print_r($patient_name);exit;
          return view('newview.new.home',compact('patient_name','patients_data'));
   }


    public function dr_address()
      {
      	$doctor_id = Auth::user()->userDetailsId;
        $countryList = DB::table('country')->pluck('country','id');
        $stateList = \App\Models\State::orderBy('state','asc')->pluck('state','id');
        $cityList = \App\Models\City::orderBy('city','asc')->pluck('city','id');
        $doctors = Doctors::where('id',$doctor_id)->first();

         return view('newview.new.address',compact('doctors','doctor_id','countryList','stateList','cityList'));
      }

   public function dr_contact()
      {
      	$doctor_id = Auth::user()->userDetailsId;
        $doctors = Doctors::where('id',$doctor_id)->first();
        return view('newview.new.contact',compact('doctors','doctor_id'));
      }

  public function dr_profilepic(Request $request)
      {
      	$doctor_id = Auth::user()->userDetailsId;
        $doctors = Doctors::where('id',$doctor_id)->first();
        if($request->hasfile('profile_pic'))
        {
            $image = $request->file('profile_pic');
            $extension = $image->getClientOriginalExtension(); // getting image extension
            $media_photos =time().'__'.$image->getClientOriginalName();
            $path = public_path('/media/profile_pic/');
            $image->move($path, $media_photos);
            $doctors['profile_pic']= $media_photos;

        }
        //print_r($doctors); exit();
        return view('newview.new.profilepic',compact('doctors','doctor_id'));
      }


  // public function dr_profilepic_remove(Request $request)
  //   {
  //     echo "string"; exit();
      
  //     $doctor_id = Auth::user()->userDetailsId;
  //     $doctors = Doctors::where('id',$doctor_id)->first();
  //     $dr_profilepic_remove =  DB::table('doctor_details')->where('id',$id)->update(['profile_pic' => null]);

  //     Flash::success('Haspatal Regsiter updated successfully.');

  //     return back();

  //   }
           public function dr_profilepic_remove(Request $request)
    {
        $booklist =  DB::table('doctor_details')->where('id',$request->doctor_name)->update(['profile_pic' => null]);
        $request->session()->flash('message', 'Profile Pic Remove Successfully.');
        $request->session()->flash('message-type', 'success');
        return response()->json($booklist);
       // return redirect(route('tostart')); 
    }


  public function dr_license(Request $request)
      {
      	 $doctor_id = Auth::user()->userDetailsId;
        $doctors = Doctors::where('id',$doctor_id)->first();
        $licenceTyes= DB::table('licence_type')->orderBy('licence_name','asc')->pluck('licence_name','id');
        if($request->hasfile('licence_copy'))
        {
            $image = $request->file('licence_copy');
            $extension = $image->getClientOriginalExtension(); // getting image extension
            $media_photos =time().'__'.$image->getClientOriginalName();
            $path = public_path('/media/licence_copy/');
            $image->move($path, $media_photos);
            $doctors['licence_copy']= $media_photos;

        }
        return view('newview.new.license',compact('doctors','doctor_id','licenceTyes'));
      }
  public function dr_mybio()
      {
      	$doctor_id = Auth::user()->userDetailsId;
        //echo $doctor_id; exit;
        $doctors = Doctors::join('specilities', 'doctor_details.speciality', '=', 'specilities.id')
        ->join('language', 'doctor_details.language', '=', 'language.id')
        ->select('doctor_details.*', 'specilities.specility as specility_name', 'language.language as language_name')
        ->where('doctor_details.id',$doctor_id)
        ->first();

         // $users = DB::table('doctor_details')
         //    ->join('specilities', 'doctor_details.speciality', '=', 'specilities.id')
         //    ->join('language', 'doctor_details.language', '=', 'language.id')
         //    ->select('doctor_details.*', 'specilities.*', 'language.*')
         //    ->where('id',$doctor_id)->first();

          //echo "<pre>"; print_r($doctors); exit();

        return view('newview.new.mybio',compact('doctors','doctor_id'));
      }
  public function dr_password()
      {
      	 $doctor_id = Auth::user()->userDetailsId;
      	$doctors = Doctors::where('id',$doctor_id)->first();
     
        return view('newview.new.passowrd',compact('doctors','doctor_id'));
      }
  public function dr_bankinginfo()
      {
      	$doctor_id = Auth::user()->userDetailsId;
        //echo $doctor_id; exit;
        $doctors = Doctors::join('specilities', 'doctor_details.speciality', '=', 'specilities.id')
                  ->join('language', 'doctor_details.language', '=', 'language.id')
                  ->select('doctor_details.*', 'specilities.specility as specility_name', 'language.language as language_name')
                  ->where('doctor_details.id',$doctor_id)
                  ->first();
    
    	return view('newview.new.bankinginfo',compact('doctor_id','doctors'));
      }
  public function dr_current_balance()
      {
      	$doctor = Auth::user()->userDetailsId; 
      
        $amount = BookingRequest::where('doctor_name',$doctor)->sum('f_amount');
       
        return view('newview.new.current_balance',compact('amount'));
      }

  public function dr_refund()
      {
      	$doctor = Auth::user()->userDetailsId;       
        $amount = BookingRequest::where('doctor_name',$doctor)->sum('f_amount');
        $total_fund =  DB::table('patient_add_fund')->where('doctor_id', $doctor)->first();
        $dr_amount = Doctor_withdraw::where('doctor_id',$doctor)->sum('requested_amount');
        $total_amount = $amount + $total_fund->dr_amount - $dr_amount;
        // $total_amount = $amount + $total_fund->dr_amount;
        //echo $total_amount; exit;
         return view('newview.new.refund',compact('total_amount'));

      }
  public function dr_setprice()
      {
      	$doctor_id = Auth::user()->userDetailsId;

        $doctors = Doctors::join('specilities', 'doctor_details.speciality', '=', 'specilities.id')
                    ->join('language', 'doctor_details.language', '=', 'language.id')
                    ->select('doctor_details.*', 'specilities.specility as specility_name', 'language.language as language_name')
                    ->where('doctor_details.id',$doctor_id)
                    ->first();
        $admin_price =  DB::table('booking_price_admin')->orderBy('id', 'desc')->first();

         return view('newview.new.setprice',compact('doctor_id','doctors','admin_price'));
      }

      public function dr_patients_queue()
      {
         $allcount = DB::table('booking_request')->where('doctor_name', Auth::user()->userDetailsId)->where('status',1)->whereDate('booking_request.updated_at', Carbon::today())->count();
        $check_count = DB::table('booking_request')->where('doctor_name', Auth::user()->userDetailsId)->where('status',3)->whereDate('booking_request.updated_at', Carbon::today())->count();
        $waiting_count = DB::table('booking_request')->where('doctor_name', Auth::user()->userDetailsId)->where('status',2)->whereDate('booking_request.updated_at', Carbon::today())->count();

        return view('newview.new.patients_queue',compact('allcount','check_count','waiting_count'));


      }

      public function dr_tostart()
      {
         $doctor_id = Auth::user()->userDetailsId;
          $patients = BookingRequest::join('patient_details','booking_request.patient_name','patient_details.id')
                              ->select('booking_request.*','patient_details.first_name','patient_details.last_name','patient_details.id as patient_id')
                              ->where('booking_request.status',2)
                              ->whereDate('booking_request.updated_at', Carbon::today())
                              ->where('booking_request.doctor_name', $doctor_id)
                              ->get();
    /*echo "<pre>";
    print_r($patients); exit;*/
     return view('newview.new.tostart',compact('patients'));

      }

      public function dr_tocomplete()

      {
        $doctor_id = Auth::user()->userDetailsId;
       // $patients = Patients::join('booking_request','', 'booking_request.patient_name')
        $patients = BookingRequest::join('patient_details','booking_request.patient_name','patient_details.id')
                              ->select('booking_request.*','patient_details.first_name','patient_details.last_name','patient_details.id as patient_id')
                              ->where('booking_request.status',3)
                              ->whereDate('booking_request.updated_at', Carbon::today())
                              ->where('booking_request.doctor_name', $doctor_id)
                              ->get();
 
     return view('newview.new.tocomplete',compact('patients'));

      }

      public function dr_reschedule_booking()

      {
         $doctor_id = Auth::user()->userDetailsId;
       // $patients = Patients::join('booking_request','', 'booking_request.patient_name')
        $patients = BookingRequest::join('patient_details','booking_request.patient_name','patient_details.id')
                              ->select('booking_request.*','patient_details.first_name','patient_details.last_name','patient_details.id as patient_id')
                              ->where('booking_request.status',4)
                              ->whereDate('booking_request.updated_at', Carbon::today())
                              ->where('booking_request.doctor_name', $doctor_id)
                              ->get();
 
     return view('newview.new.reschedule_booking',compact('patients'));

      }

      public function dr_cancle_refund_booking()

      {
         $doctor_id = Auth::user()->userDetailsId;
       // $patients = Patients::join('booking_request','', 'booking_request.patient_name')
        $patients = BookingRequest::join('patient_details','booking_request.patient_name','patient_details.id')
                              ->select('booking_request.*','patient_details.first_name','patient_details.last_name','patient_details.id as patient_id')
                              ->where('booking_request.status',5)
                              ->whereDate('booking_request.updated_at', Carbon::today())
                              ->where('booking_request.doctor_name', $doctor_id)
                              ->get();
 
          return view('newview.new.cancle_refund_booking',compact('patients'));

      }

      public function dr_patients_profile($id,$b_id)
    {
        $doctor = Auth::user()->uid; 
        $patient = User::where('userDetailsId',$id)->select('uid as patient_cuid')->first(); 
        $book_id_data = BookingRequest::where('book_id',$b_id)->select('book_id')->first(); 
        $complaint_patient = Present_complaint_patient::where('book_id',$b_id)->first(); 
        $my_note_patient = My_note_patient::where('m_book_id',$b_id)->get(); 
       // $booklist =  DB::table('booking_request')->where('book_id',$b_id)->update(['status' => 2]);

        // echo "<pre>";
        // print_r($complaint_patient); exit;
        $detail = Patients::where('id',$id)->first();
       // echo $patients_uid; exit;,
        return view('newview.new.patients_profile',compact('doctor','patient','detail','book_id_data','complaint_patient','my_note_patient'));
    }

    public function patients_complaint($id,$b_id)
    {
        $doctor = Auth::user()->userDetailsId; 
        $patient = User::where('userDetailsId',$id)->select('uid as patient_cuid')->first(); 
        $book_id_data = BookingRequest::where('book_id',$b_id)->select('book_id')->first(); 
        $complaint_patient = Present_complaint_patient::where('book_id',$b_id)->first(); 
        $my_note_patient = My_note_patient::where('m_book_id',$b_id)->get(); 
        // echo "<pre>";
        // print_r($id); exit;
       // $booklist =  DB::table('booking_request')->where('book_id',$b_id)->update(['status' => 2]);

        $detail = Patients::where('id',$id)->first();
       // echo $patients_uid; exit;,
        return view('newview.new.newpatients_complaint',compact('doctor','patient','detail','book_id_data','complaint_patient','my_note_patient'));
    }
    
     public function dr_score_card()
    {     
        $doctor_id = Auth::user()->userDetailsId;

        $totalreviewsum = DB::table('review')
                  ->where('review.doctor_id', $doctor_id)->sum('review.rating');


        $total_review = DB::table('review')
                  ->where('review.doctor_id', $doctor_id)->count();        
        
        if ($totalreviewsum != 0 && $total_review != 0) {
          $final_total =   $totalreviewsum / $total_review * 5;
        }else{
          $final_total = 0;
        }


        $final_total1 =  number_format($final_total, 2);

        $totalreviewsumcount = DB::table('review')
                  ->where('review.doctor_id', $doctor_id)->sum('review.rating');

        $final_totalreviewsumcount =  number_format($totalreviewsumcount, 2);

                  //echo "<pre>"; print_r($final_total1); exit();

        return view('newview.new.score_card',compact('final_total1','final_totalreviewsumcount'));
    }
 
    public function dr_statement()
    {
        return view('newview.new.statement');
    }
   public function dr_account_statement_view(Request $request)
   {
     if(!empty($request->s_date)){
       $startDate =  Carbon::parse($request->s_date)->startOfDay(); 
       $endDate =  Carbon::now()->endOfDay();
       $doctor_id = Auth::user()->userDetailsId;
       $dr_amount = Doctor_withdraw::where('doctor_id',$doctor_id)
                    ->whereBetween('Doctor_withdraw.created_at', [$startDate, $endDate]) 
                    ->get();
      }
     return view('newview.new.statement',compact('dr_amount'));
   } 

    public function dr_add_fund()
    {
        $doctor_id = Auth::user()->userDetailsId;

        $amount = BookingRequest::where('doctor_name',$doctor_id)->sum('f_amount');

        $last_amount = Patient_add_fund::where('doctor_id',$doctor_id)->first();

        $withdraw = Doctor_withdraw::where('doctor_id',$doctor_id)->sum('requested_amount');

        $total_amount = $amount + $last_amount->dr_amount - $withdraw;

        return view('newview.new.dr_add_fund',compact('last_amount','amount','total_amount'));

    }
    public function dr_review_summery()
    {   
          $mytime = Carbon::now();
          $today = new \DateTime(); 
          $year  = (int) $today->format('Y');
          $week  = (int) $today->format('W'); // Week of the year
          $day   = (int) $today->format('w'); // Day of the week (0 = sunday)
          $month   = (int) $today->format('m');
          $doctor_id = Auth::user()->userDetailsId;

          $today = DB::table('review')->join('patient_details', 'review.patient_id', '=', 'patient_details.id')
                  ->join('doctor_details', 'review.doctor_id', '=', 'doctor_details.id')
                  ->where('review.doctor_id', $doctor_id)->whereBetween('review.rating', [4.5, 5.0])->whereDate('review.created_at', Carbon::today())
                  ->select('review.*','patient_details.first_name as patientname' ,'doctor_details.first_name')
                  ->count();

         $today_4 = DB::table('review')->join('patient_details', 'review.patient_id', '=', 'patient_details.id')
                  ->join('doctor_details', 'review.doctor_id', '=', 'doctor_details.id')
                  ->where('review.doctor_id', $doctor_id)->whereBetween('review.rating', [3.5, 4.0])->whereDate('review.created_at', Carbon::today())

                  ->select('review.*','patient_details.first_name as patientname' ,'doctor_details.first_name')
                  ->count();    

         $today_3 = DB::table('review')->join('patient_details', 'review.patient_id', '=', 'patient_details.id')
                  ->join('doctor_details', 'review.doctor_id', '=', 'doctor_details.id')
                  ->where('review.doctor_id', $doctor_id)->whereBetween('review.rating', [2.5, 3.0])->whereDate('review.created_at', Carbon::today())
                  ->select('review.*','patient_details.first_name as patientname' ,'doctor_details.first_name')
                  ->count();

          $today_2 = DB::table('review')->join('patient_details', 'review.patient_id', '=', 'patient_details.id')
                  ->join('doctor_details', 'review.doctor_id', '=', 'doctor_details.id')
                  ->where('review.doctor_id', $doctor_id)->whereBetween('review.rating', [1.5, 2.0])->whereDate('review.created_at', Carbon::today())
                  ->select('review.*','patient_details.first_name as patientname' ,'doctor_details.first_name')
                  ->count();                   

          $today_1 = DB::table('review')->join('patient_details', 'review.patient_id', '=', 'patient_details.id')
                  ->join('doctor_details', 'review.doctor_id', '=', 'doctor_details.id')
                  ->where('review.doctor_id', $doctor_id)->where('review.rating', 1.0)->whereDate('review.created_at', Carbon::today())
                  ->select('review.*','patient_details.first_name as patientname' ,'doctor_details.first_name')
                  ->count();

          $data = [
            $today,$today_4,$today_3,$today_2,$today_1
          ];

          $subtotal = $today+$today_4+$today_3+$today_2+$today_1;

          $today_consults = DB::table('booking_request')->where('doctor_name', $doctor_id)->whereDate('date_time', Carbon::today())->count();
          
          $allcount = DB::table('booking_request')->where('doctor_name', $doctor_id)->count();


          $this_week = DB::table('review')->join('patient_details', 'review.patient_id', '=', 'patient_details.id')
                  ->join('doctor_details', 'review.doctor_id', '=', 'doctor_details.id')
                  ->where('review.doctor_id', $doctor_id)->whereBetween('review.rating', [4.5, 5.0])->whereBetween('review.created_at', [Carbon::now()-> startOfWeek(), Carbon::now()->endOfWeek()])
                  ->select('review.*','patient_details.first_name as patientname' ,'doctor_details.first_name')
                  ->count();

          $this_week_4 = DB::table('review')->join('patient_details', 'review.patient_id', '=', 'patient_details.id')
                  ->join('doctor_details', 'review.doctor_id', '=', 'doctor_details.id')
                  ->where('review.doctor_id', $doctor_id)->whereBetween('review.rating', [3.5, 4.0])->whereBetween('review.created_at', [Carbon::now()-> startOfWeek(), Carbon::now()->endOfWeek()])

                  ->select('review.*','patient_details.first_name as patientname' ,'doctor_details.first_name')
                  ->count();    

           $this_week_3 = DB::table('review')->join('patient_details', 'review.patient_id', '=', 'patient_details.id')
                  ->join('doctor_details', 'review.doctor_id', '=', 'doctor_details.id')
                  ->where('review.doctor_id', $doctor_id)->whereBetween('review.rating', [2.5, 3.0])->whereBetween('review.created_at', [Carbon::now()-> startOfWeek(), Carbon::now()->endOfWeek()])
                  ->select('review.*','patient_details.first_name as patientname' ,'doctor_details.first_name')
                  ->count();

          $this_week_2 = DB::table('review')->join('patient_details', 'review.patient_id', '=', 'patient_details.id')
                  ->join('doctor_details', 'review.doctor_id', '=', 'doctor_details.id')
                  ->where('review.doctor_id', $doctor_id)->whereBetween('review.rating', [1.5, 2.0])->whereBetween('review.created_at', [Carbon::now()-> startOfWeek(), Carbon::now()->endOfWeek()])
                  ->select('review.*','patient_details.first_name as patientname' ,'doctor_details.first_name')
                  ->count();                   

          $this_week_1 = DB::table('review')->join('patient_details', 'review.patient_id', '=', 'patient_details.id')
                  ->join('doctor_details', 'review.doctor_id', '=', 'doctor_details.id')
                  ->where('review.doctor_id', $doctor_id)->where('review.rating', 1.0)->whereBetween('review.created_at', [Carbon::now()-> startOfWeek(), Carbon::now()->endOfWeek()])
                  ->select('review.*','patient_details.first_name as patientname' ,'doctor_details.first_name')
                  ->count();

          $subtotal_this_week = $this_week+$this_week_4+$this_week_3+$this_week_2+$this_week_1;

          $this_week_consults = DB::table('booking_request')->where('doctor_name', $doctor_id)->whereBetween('date_time', [Carbon::now()-> startOfWeek(), Carbon::now()->endOfWeek()])->count();


          $this_month = DB::table('review')->join('patient_details', 'review.patient_id', '=', 'patient_details.id')
                    ->join('doctor_details', 'review.doctor_id', '=', 'doctor_details.id')
                    ->where('review.doctor_id', $doctor_id)->whereBetween('review.rating', [4.5, 5.0])->whereDate('review.created_at', '>=', [Carbon::now()->startOfMonth(), Carbon::now()->endOfMonth()])
                    ->select('review.*','patient_details.first_name as patientname' ,'doctor_details.first_name')
                    ->count();

            $this_month_4 = DB::table('review')->join('patient_details', 'review.patient_id', '=', 'patient_details.id')
                      ->join('doctor_details', 'review.doctor_id', '=', 'doctor_details.id')
                      ->where('review.doctor_id', $doctor_id)->whereBetween('review.rating', [3.5, 4.0])->whereDate('review.created_at', '>=', [Carbon::now()->startOfMonth(), Carbon::now()->endOfMonth()])

                      ->select('review.*','patient_details.first_name as patientname' ,'doctor_details.first_name')
                      ->count();    

             $this_month_3 = DB::table('review')->join('patient_details', 'review.patient_id', '=', 'patient_details.id')
                      ->join('doctor_details', 'review.doctor_id', '=', 'doctor_details.id')
                      ->where('review.doctor_id', $doctor_id)->whereBetween('review.rating', [2.5, 3.0])->whereDate('review.created_at', '>=', [Carbon::now()->startOfMonth(), Carbon::now()->endOfMonth()])
                      ->select('review.*','patient_details.first_name as patientname' ,'doctor_details.first_name')
                      ->count();

            $this_month_2 = DB::table('review')->join('patient_details', 'review.patient_id', '=', 'patient_details.id')
                      ->join('doctor_details', 'review.doctor_id', '=', 'doctor_details.id')
                      ->where('review.doctor_id', $doctor_id)->whereBetween('review.rating', [1.5, 2.0])->whereDate('review.created_at', '>=', [Carbon::now()->startOfMonth(), Carbon::now()->endOfMonth()])
                      ->select('review.*','patient_details.first_name as patientname' ,'doctor_details.first_name')
                      ->count();                   

             $this_month_1 = DB::table('review')->join('patient_details', 'review.patient_id', '=', 'patient_details.id')
                      ->join('doctor_details', 'review.doctor_id', '=', 'doctor_details.id')
                      ->where('review.doctor_id', $doctor_id)->where('review.rating', 1.0)->whereDate('review.created_at', '>=', [Carbon::now()->startOfMonth(), Carbon::now()->endOfMonth()])
                      ->select('review.*','patient_details.first_name as patientname' ,'doctor_details.first_name')
                      ->count();

            $subtotal_this_month = $this_month+$this_month_4+$this_month_3+$this_month_2+$this_month_1;

            $this_month_consults = DB::table('booking_request')->where('doctor_name', $doctor_id)->whereDate('date_time', '>=', [Carbon::now()->startOfMonth(), Carbon::now()->endOfMonth()])->count();


            $this_year = DB::table('review')->join('patient_details', 'review.patient_id', '=', 'patient_details.id')
                      ->join('doctor_details', 'review.doctor_id', '=', 'doctor_details.id')
                      ->where('review.doctor_id', $doctor_id)->whereBetween('review.rating', [4.5, 5.0])->whereYear('review.created_at', $year)
                      ->select('review.*','patient_details.first_name as patientname' ,'doctor_details.first_name')
                      ->count();

            $this_year_4 = DB::table('review')->join('patient_details', 'review.patient_id', '=', 'patient_details.id')
                      ->join('doctor_details', 'review.doctor_id', '=', 'doctor_details.id')
                      ->where('review.doctor_id', $doctor_id)->whereBetween('review.rating', [3.5, 4.0])->whereYear('review.created_at', $year)

                      ->select('review.*','patient_details.first_name as patientname' ,'doctor_details.first_name')
                      ->count();    

             $this_year_3 = DB::table('review')->join('patient_details', 'review.patient_id', '=', 'patient_details.id')
                      ->join('doctor_details', 'review.doctor_id', '=', 'doctor_details.id')
                      ->where('review.doctor_id', $doctor_id)->whereBetween('review.rating', [2.5, 3.0])->whereYear('review.created_at', $year)
                      ->select('review.*','patient_details.first_name as patientname' ,'doctor_details.first_name')
                      ->count();

            $this_year_2 = DB::table('review')->join('patient_details', 'review.patient_id', '=', 'patient_details.id')
                      ->join('doctor_details', 'review.doctor_id', '=', 'doctor_details.id')
                      ->where('review.doctor_id', $doctor_id)->whereBetween('review.rating', [1.5, 2.0])->whereYear('review.created_at', $year)
                      ->select('review.*','patient_details.first_name as patientname' ,'doctor_details.first_name')
                      ->count();                   

             $this_year_1 = DB::table('review')->join('patient_details', 'review.patient_id', '=', 'patient_details.id')
                      ->join('doctor_details', 'review.doctor_id', '=', 'doctor_details.id')
                      ->where('review.doctor_id', $doctor_id)->where('review.rating', 1.0)->whereYear('review.created_at', $year)
                      ->select('review.*','patient_details.first_name as patientname' ,'doctor_details.first_name')
                      ->count();

             $subtotal_this_year = $this_year+$this_year_4+$this_year_3+$this_year_2+$this_year_1;

             $this_year_consults = DB::table('booking_request')->where('doctor_name', $doctor_id)->whereYear('date_time', $year)->count();


                      
            $this_total = DB::table('review')->join('patient_details', 'review.patient_id', '=', 'patient_details.id')
                      ->join('doctor_details', 'review.doctor_id', '=', 'doctor_details.id')
                      ->where('review.doctor_id', $doctor_id)->whereBetween('review.rating', [4.5, 5.0])
                      ->select('review.*','patient_details.first_name as patientname' ,'doctor_details.first_name')
                      ->count();

            $this_total_4 = DB::table('review')->join('patient_details', 'review.patient_id', '=', 'patient_details.id')
                      ->join('doctor_details', 'review.doctor_id', '=', 'doctor_details.id')
                      ->where('review.doctor_id', $doctor_id)->whereBetween('review.rating', [3.5, 4.0])

                      ->select('review.*','patient_details.first_name as patientname' ,'doctor_details.first_name')
                      ->count();    

             $this_total_3 = DB::table('review')->join('patient_details', 'review.patient_id', '=', 'patient_details.id')
                      ->join('doctor_details', 'review.doctor_id', '=', 'doctor_details.id')
                      ->where('review.doctor_id', $doctor_id)->whereBetween('review.rating', [2.5, 3.0])
                      ->select('review.*','patient_details.first_name as patientname' ,'doctor_details.first_name')
                      ->count();

            $this_total_2 = DB::table('review')->join('patient_details', 'review.patient_id', '=', 'patient_details.id')
                      ->join('doctor_details', 'review.doctor_id', '=', 'doctor_details.id')
                      ->where('review.doctor_id', $doctor_id)->whereBetween('review.rating', [1.5, 2.0])
                      ->select('review.*','patient_details.first_name as patientname' ,'doctor_details.first_name')
                      ->count();                   

             $this_total_1 = DB::table('review')->join('patient_details', 'review.patient_id', '=', 'patient_details.id')
                      ->join('doctor_details', 'review.doctor_id', '=', 'doctor_details.id')
                      ->where('review.doctor_id', $doctor_id)->where('review.rating', 1.0)
                      ->select('review.*','patient_details.first_name as patientname' ,'doctor_details.first_name')
                      ->count();          

                $subtotal_this_total = $this_total+$this_total_4+$this_total_3+$this_total_2+$this_total_1;

             $this_total_consults = DB::table('booking_request')->where('doctor_name', $doctor_id)->count();


          // $subtotal->array_sum($data);

                //echo "<pre>"; print_r($today_consults); exit();


        return view('newview.new.review_summery',compact('today','today_4','today_3','today_2','today_1','subtotal','this_week','this_week_4','this_week_3','this_week_2','this_week_1','this_month','this_month_4','this_month_3','this_month_2','this_month_1','this_year','this_year_4','this_year_3','this_year_2','this_year_1','this_total','this_total_4','this_total_3','this_total_2','this_total_1','today_consults','subtotal_this_week','this_week_consults','subtotal_this_month','this_month_consults','subtotal_this_year','this_year_consults','subtotal_this_total','this_total_consults'));

    }
    public function dr_myreview()
    { 

          $mytime = Carbon::now();
          $today = new \DateTime(); 
          $year  = (int) $today->format('Y');
          $week  = (int) $today->format('W'); // Week of the year
          $day   = (int) $today->format('w'); // Day of the week (0 = sunday)
          $month   = (int) $today->format('m');

          $doctor_id = Auth::user()->userDetailsId;

          $today = DB::table('review')->join('patient_details', 'review.patient_id', '=', 'patient_details.id')
                  ->join('doctor_details', 'review.doctor_id', '=', 'doctor_details.id')
                  ->where('review.doctor_id', $doctor_id)->whereDate('review.created_at', Carbon::today())
                  ->select('review.*','patient_details.first_name as patientname' ,'doctor_details.first_name')
                  ->get();

          $this_year = DB::table('review')->join('patient_details', 'review.patient_id', '=', 'patient_details.id')
                   ->join('doctor_details', 'review.doctor_id', '=', 'doctor_details.id')
                  ->where('review.doctor_id', $doctor_id)->whereYear('review.created_at', $year)
                   ->select('review.*','patient_details.first_name as patientname' ,'doctor_details.first_name')
                  ->select('review.*','patient_details.first_name as patientname' ,'doctor_details.first_name')
                  ->get();     

          $this_weeks = DB::table('review')->join('patient_details', 'review.patient_id', '=', 'patient_details.id')
                  ->join('doctor_details', 'review.doctor_id', '=', 'doctor_details.id')
                  ->where('review.doctor_id', $doctor_id)->whereBetween('review.created_at', [Carbon::now()-> startOfWeek(), Carbon::now()->endOfWeek()])
                   ->select('review.*','patient_details.first_name as patientname' ,'doctor_details.first_name')
                  ->get();

          $thismonth = DB::table('review')->join('patient_details', 'review.patient_id', '=', 'patient_details.id')
                   ->join('doctor_details', 'review.doctor_id', '=', 'doctor_details.id')
                  ->where('review.doctor_id', $doctor_id)->whereDate('review.created_at', '>=', [Carbon::now()->startOfMonth(), Carbon::now()->endOfMonth()])
                  ->select('review.*','patient_details.first_name as patientname' ,'doctor_details.first_name')
                  ->get();

          $all_total = DB::table('review')->join('patient_details', 'review.patient_id', '=', 'patient_details.id')
                   ->join('doctor_details', 'review.doctor_id', '=', 'doctor_details.id')
                  ->where('review.doctor_id', $doctor_id)
                  ->select('review.*','patient_details.first_name as patientname' ,'doctor_details.first_name')
                  ->get();

          //echo "<pre>"; print_r($all_total); exit();
        return view('newview.new.myreview',compact('today','this_year','this_weeks','thismonth','all_total'));

    }
    public function dr_myresponce()
    {
        $mytime = Carbon::now();
          $today = new \DateTime(); 
          $year  = (int) $today->format('Y');
          $week  = (int) $today->format('W'); // Week of the year
          $day   = (int) $today->format('w'); // Day of the week (0 = sunday)
          $month   = (int) $today->format('m');

          $doctor_id = Auth::user()->userDetailsId;

          $today = DB::table('review')->join('patient_details', 'review.patient_id', '=', 'patient_details.id')
                  ->join('doctor_details', 'review.doctor_id', '=', 'doctor_details.id')
                  ->where('review.doctor_id', $doctor_id)->whereDate('review.created_at', Carbon::today())
                  ->select('review.*','patient_details.first_name as patientname' ,'doctor_details.first_name')
                  ->get();

          $this_year = DB::table('review')->join('patient_details', 'review.patient_id', '=', 'patient_details.id')
                   ->join('doctor_details', 'review.doctor_id', '=', 'doctor_details.id')
                  ->where('review.doctor_id', $doctor_id)->whereYear('review.created_at', $year)
                   ->select('review.*','patient_details.first_name as patientname' ,'doctor_details.first_name')
                  ->select('review.*','patient_details.first_name as patientname' ,'doctor_details.first_name')
                  ->get();     

          $this_weeks = DB::table('review')->join('patient_details', 'review.patient_id', '=', 'patient_details.id')
                  ->join('doctor_details', 'review.doctor_id', '=', 'doctor_details.id')
                  ->where('review.doctor_id', $doctor_id)->whereBetween('review.created_at', [Carbon::now()-> startOfWeek(), Carbon::now()->endOfWeek()])
                   ->select('review.*','patient_details.first_name as patientname' ,'doctor_details.first_name')
                  ->get();

          $thismonth = DB::table('review')->join('patient_details', 'review.patient_id', '=', 'patient_details.id')
                   ->join('doctor_details', 'review.doctor_id', '=', 'doctor_details.id')
                  ->where('review.doctor_id', $doctor_id)->whereDate('review.created_at', '>=', [Carbon::now()->startOfMonth(), Carbon::now()->endOfMonth()])
                  ->select('review.*','patient_details.first_name as patientname' ,'doctor_details.first_name')
                  ->get();

          $all_total = DB::table('review')->join('patient_details', 'review.patient_id', '=', 'patient_details.id')
                   ->join('doctor_details', 'review.doctor_id', '=', 'doctor_details.id')
                  ->where('review.doctor_id', $doctor_id)
                  ->select('review.*','patient_details.first_name as patientname' ,'doctor_details.first_name')
                  ->get();

          //echo "<pre>"; print_r($all_total); exit();
        return view('newview.new.myresponce',compact('today','this_year','this_weeks','thismonth','all_total'));

    }

    public function review_answer(Request $request)
    {
                      
      $review_answer = DB::table('review')->where('book_id',$request->book_id)->update(['review_ans' => $request->review_ans]); 
         Flash::success('Your Review Sent Successfully.');         
         return back();
      //return view('newview.new.myresponce',compact('review_answer'));

    }

     public function dr_my_consults_records()
     {
           return view('newview.new.my_consults_records');
      }
    public function dr_my_consults_records2( Request $request)
     {
        $doctor_id = Auth::user()->userDetailsId;
        $startDate =  Carbon::parse($request->s_date)->startOfDay(); 
        $endDate =  Carbon::now()->endOfDay();
        $patients = BookingRequest::join('patient_details','booking_request.patient_name','patient_details.id')
                              ->select('booking_request.*','patient_details.first_name','patient_details.last_name','patient_details.id as patient_id')
                              ->where('booking_request.status',3)
                              ->whereBetween('booking_request.created_at', [$startDate, $endDate]) 
                              ->where('booking_request.doctor_name', $doctor_id)
                              ->get();
      
           return view('newview.new.my_consults_records2',compact('patients'));
      }

    public function dr_my_visit_summaries()
         {
               return view('newview.new.my_visit_summaries');
          }
    public function dr_my_visit_summaries2(Request $request)
    {
       $doctor_id = Auth::user()->userDetailsId;
        $startDate =  Carbon::parse($request->s_date)->startOfDay(); 
        $endDate =  Carbon::now()->endOfDay();
       $myNotePatients = My_note_patient::where('my_doctor_id',$doctor_id)
                         ->join('patient_details', 'my_note_patient.my_patient_id', '=', 'patient_details.id')
                         ->join('doctor_details', 'my_note_patient.my_doctor_id', '=', 'doctor_details.id')
                         ->whereBetween('my_note_patient.created_at', [$startDate, $endDate])
                         ->select('my_note_patient.*','patient_details.first_name as patient','doctor_details.first_name','doctor_details.last_name')
                        ->get();
      /* echo "<pre>";
        print_r($myNotePatients); exit;*/
      return view('newview.new.my_visit_summaries2',compact('myNotePatients'));
    }

    public function dr_my_prescriptions()
     {
           return view('newview.new.my_prescriptions');
      }
    public function dr_my_prescriptions2(Request $request)
    {
        $doctor_id = Auth::user()->userDetailsId;
        $startDate =  Carbon::parse($request->s_date)->startOfDay(); 
        $endDate =  Carbon::now()->endOfDay();
        $prescription_details = Prescription_details::where('prescription_details.doctor_id', $doctor_id)
                                        ->join('patient_details', 'prescription_details.patient_id', '=', 'patient_details.id')
                                        ->join('booking_request', 'prescription_details.doctor_id', '=', 'booking_request.doctor_name')
                                        ->whereBetween('prescription_details.created_at', [$startDate, $endDate])
                                        ->select('prescription_details.*','patient_details.first_name','patient_details.last_name','booking_request.date_time')
                                        ->get();
      /*  echo "<pre>";
        print_r($result); exit;*/
         return view('newview.new.my_prescriptions2',compact($prescription_details));
    }

    public function dr_setting()
     {  
     // echo "string"; exit;
         $doctor_id = Auth::user()->userDetailsId;
         $doctors = Doctor_availability::where('id',$doctor_id)->first();
         $doctorstime = TimeSlot::where('doctor_id',$doctor_id)->first();
         $doctorsnon = Doctor_availability::where('doctor_id',$doctor_id)->orderBy('id', 'desc')->get();

        $doctorsdetails = Doctors::join('specilities', 'doctor_details.speciality', '=', 'specilities.id')
        ->join('language', 'doctor_details.language', '=', 'language.id')
        ->select('doctor_details.*', 'specilities.specility as specility_name', 'language.language as language_name')
        ->where('doctor_details.id',$doctor_id)
        ->first();

         return view('newview.new.setting',compact('doctorsdetails','doctors','doctor_id','doctorstime','doctorsnon'));
      }

       public function homepagedemo()
      {

      	// echo "string"; exit;
         
        $doctor_id = Auth::user()->userDetailsId;
        $patient_name = Patients::join('booking_request','patient_details.id','booking_request.patient_name')
                          ->where('booking_request.doctor_name',Auth::user()->userDetailsId)
                           ->groupBy('booking_request.patient_name')
                           ->select('booking_request.*','patient_details.first_name','patient_details.id')
                           ->pluck('first_name','patient_details.id');
          
          

          //print_r($sameDayLastYear); exit();
                             
        $booking = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->count();

          $today = new \DateTime(); 
          $year  = (int) $today->format('Y');
          $week  = (int) $today->format('W'); // Week of the year
          $day   = (int) $today->format('w'); // Day of the week (0 = sunday)
          $month   = (int) $today->format('m');
          $today_day   = (int) $today->format('d');

        //$lastyear = new \DateTime();

        $lastmonth = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereMonth('created_at', $month - 1)->count();

        $lastyear = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereYear('created_at', $year - 1)->count();

        $this_year = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereYear('created_at', $year)->count();

        $this_month = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereMonth('date_time', $month)->count();

        $this_today = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereDay('date_time', $today_day)->count();

        $this_weeks = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereBetween('date_time', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->count();


         return view('newview.new.homepage.homepagedemo',compact('patient_name','booking','this_year','lastyear','lastmonth','this_month','this_today','this_weeks'));
      }
      
      public function homepagetodaybooking()
      {

        $mytime = Carbon::now();
           $today = new \DateTime(); 
          $year  = (int) $today->format('Y');
          $week  = (int) $today->format('W'); // Week of the year
          $day   = (int) $today->format('w'); // Day of the week (0 = sunday)
          $month   = (int) $today->format('m');

          $doctor_id = Auth::user()->userDetailsId;

          $allcount = DB::table('booking_request')->where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereDate('date_time', Carbon::today())->count();
          $this_weeks = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereBetween('date_time', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->count();
          
          $last_weeks = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereDate('date_time', date('Y-m-d H:i:s',strtotime('-7 days')) )->count();
                     

                     //echo "<pre>"; print_r($last_weeks); exit();


          $yesterday = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereDate('date_time', date('Y-m-d', strtotime("-1 days")) )->count();
          
          //  $mytime = Carbon::now();
          //  $today = new \DateTime(); 
          // $year  = (int) $today->format('Y');
          // $week  = (int) $today->format('W'); // Week of the year
          // $day   = (int) $today->format('w'); // Day of the week (0 = sunday)
          // $month   = (int) $today->format('m');



        // $doctor_id = Auth::user()->userDetailsId;
        $patient_name = Patients::join('booking_request','patient_details.id','booking_request.patient_name')
                          ->where('booking_request.doctor_name',Auth::user()->userDetailsId)
                           ->groupBy('booking_request.patient_name')
                           ->select('booking_request.*','patient_details.first_name','patient_details.id')
                           ->pluck('first_name','patient_details.id');

        //   	$allcount = DB::table('booking_request')->where('doctor_name', Auth::user()->userDetailsId)->whereDate('created_at', Carbon::today())->count();

         	// $this_weeks = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->
          // 	whereMonth('created_at', $week)->count();
          //	$this_weeks = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->count();

        	//echo "<pre>"; print_r($current_week); exit();

           // $this_weeks = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->
         	 // whereDate('created_at', '<=', date('Y-m-d H:i:s',strtotime('7 days')) )->count();

          	// echo "<pre>"; print_r($todayMinusOneWeekAgo); die();

          //  $last_weeks = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->
        	//  whereDate('created_at', '>=', date('Y-m-d H:i:s',strtotime('-7 days')) )->count();

          	// $yesterday = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->
          	// whereDate('created_at', $day - 1)->get();

          //  $yesterday = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->
          // 	whereDate('created_at', date('Y-m-d', strtotime("-1 days")) )->count();

         return view('newview.new.homepage.todaybooking',compact('patient_name','allcount','this_weeks','last_weeks','yesterday','mytime'));
      }

      public function homepageshifts()
      {
        $mytime = Carbon::now();
        $today = new \DateTime(); 
        $year  = (int) $today->format('Y');
        $week  = (int) $today->format('W'); // Week of the year
        $day   = (int) $today->format('w'); // Day of the week (0 = sunday)
        $month   = (int) $today->format('m');

        $doctor_id = Auth::user()->userDetailsId;

         $mor = DB::table('booking_request')->where('slot_status','1')->where('status','!=' , 5)->where('doctor_name', Auth::user()->userDetailsId)->whereDate('date_time', Carbon::today())->count();
          $aft = DB::table('booking_request')->where('slot_status','2')->where('booking_request.status','!=' , 5)->where('doctor_name', Auth::user()->userDetailsId)->whereDate('date_time', Carbon::today())->count();
          $eve = DB::table('booking_request')->where('slot_status','3')->where('booking_request.status','!=' , 5)->where('doctor_name', Auth::user()->userDetailsId)->whereDate('date_time', Carbon::today())->count();

        $patient_name = Patients::join('booking_request','patient_details.id','booking_request.patient_name')
                          ->where('booking_request.doctor_name',Auth::user()->userDetailsId)
                           ->groupBy('booking_request.patient_name')
                           ->select('booking_request.*','patient_details.first_name','patient_details.id')
                           ->pluck('first_name','patient_details.id');

          $t_amount = BookingRequest::where('doctor_name',Auth::user()->userDetailsId)->whereDate('date_time', Carbon::today())->sum('f_amount');


         return view('newview.new.homepage.todayshifts',compact('patient_name','mor','aft','eve','mytime','t_amount'));
      }
     
      public function homepageshiftsdetailsmorning()
      {
        $mytime = Carbon::now();
        $today = new \DateTime(); 
        $year  = (int) $today->format('Y');
        $week  = (int) $today->format('W'); // Week of the year
        $day   = (int) $today->format('w'); // Day of the week (0 = sunday)
        $month   = (int) $today->format('m');

        $doctor_id = Auth::user()->userDetailsId;
        $patient_name = Patients::join('booking_request','patient_details.id','booking_request.patient_name')
                          ->where('booking_request.doctor_name',Auth::user()->userDetailsId)
                           ->groupBy('booking_request.patient_name')
                           ->select('booking_request.*','patient_details.first_name','patient_details.id')
                           ->pluck('first_name','patient_details.id');

        $m = DB::table('booking_request')->where('slot_status','1')->where('booking_request.status','!=' , 5)->where('doctor_name', Auth::user()->userDetailsId)->whereDate('date_time', Carbon::today())->count();

      // echo '<pre>'; print_r($shiftsdetailsmorning); exit();
        
       
        $shiftsdetailsmorning = DB::table('booking_request')->join('patient_details','booking_request.patient_name','patient_details.id')
                              ->select('booking_request.*','patient_details.first_name','patient_details.last_name','patient_details.id as patient_id')
                              ->where('slot_status','1')->where('booking_request.status','!=' , 5)->where('doctor_name', Auth::user()->userDetailsId)->whereDate('date_time', Carbon::today())
                              ->get();

                              //echo '<pre>'; print_r($shiftsdetailsmorning); exit();

         return view('newview.new.homepage.today_shiftsdetails_morning',compact('patient_name','shiftsdetailsmorning'));
      }
      
      public function homepageshiftsdetailsafternoon()
      {
        $mytime = Carbon::now();
        $today = new \DateTime(); 
        $year  = (int) $today->format('Y');
        $week  = (int) $today->format('W'); // Week of the year
        $day   = (int) $today->format('w'); // Day of the week (0 = sunday)
        $month   = (int) $today->format('m');

        $doctor_id = Auth::user()->userDetailsId;
        $patient_name = Patients::join('booking_request','patient_details.id','booking_request.patient_name')
                          ->where('booking_request.doctor_name',Auth::user()->userDetailsId)
                           ->groupBy('booking_request.patient_name')
                           ->select('booking_request.*','patient_details.first_name','patient_details.id')
                           ->pluck('first_name','patient_details.id');

        $m = DB::table('booking_request')->where('slot_status','1')->where('status','!=' , 5)->where('doctor_name', Auth::user()->userDetailsId)->whereDate('date_time', Carbon::today())->count();
       
        $shiftsdetailsafternoon = DB::table('booking_request')->join('patient_details','booking_request.patient_name','patient_details.id')
                              ->select('booking_request.*','patient_details.first_name','patient_details.last_name','patient_details.id as patient_id')
                              ->where('slot_status','2')->where('booking_request.status','!=' , 5)->where('doctor_name', Auth::user()->userDetailsId)->whereDate('date_time', Carbon::today())
                              ->get();

      //  echo '<pre>'; print_r($shiftsdetailsafternoon); exit();

         return view('newview.new.homepage.today_shiftsdetails_afternoon',compact('patient_name','shiftsdetailsafternoon'));
      }


      public function homepageshiftsdetailsevening()
      {
        $mytime = Carbon::now();
        $today = new \DateTime(); 
        $year  = (int) $today->format('Y');
        $week  = (int) $today->format('W'); // Week of the year
        $day   = (int) $today->format('w'); // Day of the week (0 = sunday)
        $month   = (int) $today->format('m');

        $doctor_id = Auth::user()->userDetailsId;
        $patient_name = Patients::join('booking_request','patient_details.id','booking_request.patient_name')
                          ->where('booking_request.doctor_name',Auth::user()->userDetailsId)
                           ->groupBy('booking_request.patient_name')
                           ->select('booking_request.*','patient_details.first_name','patient_details.id')
                           ->pluck('first_name','patient_details.id');

        $m = DB::table('booking_request')->where('slot_status','1')->where('status','!=' , 5)->where('doctor_name', Auth::user()->userDetailsId)->whereDate('date_time', Carbon::today())->count();
       
        $shiftsdetailsevening = DB::table('booking_request')->join('patient_details','booking_request.patient_name','patient_details.id')
                              ->select('booking_request.*','patient_details.first_name','patient_details.last_name','patient_details.id as patient_id')
                              ->where('slot_status','3')->where('booking_request.status','!=' , 5)->where('doctor_name', Auth::user()->userDetailsId)->whereDate('date_time', Carbon::today())
                              ->get();

      //  echo '<pre>'; print_r($shiftsdetailsafternoon); exit();

         return view('newview.new.homepage.today_shiftsdetails_evening',compact('patient_name','shiftsdetailsevening'));
      }



      public function homepagethisweek()
      {
        $mytime = Carbon::now();
        $today = new \DateTime(); 
        $year  = (int) $today->format('Y');
        $week  = (int) $today->format('W'); // Week of the year
        $day   = (int) $today->format('w'); // Day of the week (0 = sunday)
        $month   = (int) $today->format('m');


        $doctor_id = Auth::user()->userDetailsId;
        $patient_name = Patients::join('booking_request','patient_details.id','booking_request.patient_name')
                          ->where('booking_request.doctor_name',Auth::user()->userDetailsId)
                           ->groupBy('booking_request.patient_name')
                           ->select('booking_request.*','patient_details.first_name','patient_details.id')
                           ->pluck('first_name','patient_details.id');

          $allcount = DB::table('booking_request')->where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereDate('booking_request.updated_at', Carbon::today())->count();
          $this_weeksall = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereBetween('date_time', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->count();

           $this_weeks = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->
           whereDate('created_at', '<=', date('Y-m-d H:i:s',strtotime('7 days')) )->count();
          //  $last_weeks = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->
          //  whereDate('created_at', '>=', date('Y-m-d H:i:s',strtotime('-7 days')) )->count();
          $last_weeks = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereDate('date_time', date('Y-m-d H:i:s',strtotime('-7 days')) )->count();
          // $lastmonth = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->
          // whereDate('created_at', '>=', date('Y-m-d H:i:s',strtotime('-30 days')) )->count();
          $lastmonth = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereMonth('date_time', $month - 1)->count();
          // $this_year = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->
          //  whereDate('created_at', '<=', date('Y-m-d H:i:s',strtotime(' 365 days')) )->count();
          $this_year = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereYear('date_time', $year)->count();

          //  $date = \Carbon\Carbon::today()->subDays(7);
           $lastdate = $mytime->copy()->addDays(6);
           $weekstart = Carbon::now()->startOfWeek()->toFormattedDateString('d-m-Y');
           $weekend = Carbon::now()->endOfWeek()->toFormattedDateString('d-m-Y');

          
           
          //echo '<pre>'; print_r($weekend); exit();
         return view('newview.new.homepage.thisweek',compact('patient_name','allcount','this_weeks','last_weeks','lastmonth','mytime','this_year','lastdate','this_weeksall','weekstart','weekend'));
      }

      public function homepagethisweek_day_list()
      {
        $mytime = Carbon::now();
        $today = new \DateTime(); 
        $year  = (int) $today->format('Y');
        $week  = (int) $today->format('W'); // Week of the year
        $day   = (int) $today->format('w'); // Day of the week (0 = sunday)
        $month   = (int) $today->format('m');

        $doctor_id = Auth::user()->userDetailsId;
        $patient_name = Patients::join('booking_request','patient_details.id','booking_request.patient_name')
                          ->where('booking_request.doctor_name',Auth::user()->userDetailsId)
                           ->groupBy('booking_request.patient_name')
                           ->select('booking_request.*','patient_details.first_name','patient_details.id')
                           ->pluck('first_name','patient_details.id');
       
      $date0 = Carbon::now()->startOfWeek()->format('l, d F, Y');
      $new0 = Carbon::now()->startOfWeek()->format('Y-m-d');
      // echo date('Y-m-d', strtotime('Monday + 1 day')); exit;
      $day0 = Carbon::now()->startOfWeek();
      //$tomorrow = date('Y-m-d',strtotime($day0 . "+2 days"));
      //echo '<pre>'; print_r ($tomorrow); exit;
      $mon = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereDate('date_time',$day0)->count();

      $date1 =  date('l, d F, Y',strtotime($day0 . "+1 days"));
      $new1 = date('Y-m-d',strtotime($day0 . "+1 days"));
      // $day1 =  Carbon::parse(date('Y-m-d H:i:s',strtotime('-2 days')))->startOfDay(); 
      $day1 = date('Y-m-d',strtotime($day0 . "+1 days"));
      $tue = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereDate('date_time',$day1)->count();
       
      $date2 =  date('l, d F, Y',strtotime($day0 . "+2 days"));
      $new2 =  date('Y-m-d',strtotime($day0 . "+2 days"));
      //$day2 =  Carbon::parse(date('Y-m-d H:i:s',strtotime('-1 days')))->startOfDay(); 
      $day2 = date('Y-m-d',strtotime($day0 . "+2 days"));
      $wed = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereDate('date_time',$day2)->count();
       
      $date3 =  $day3 = date('l, d F, Y',strtotime($day0 . "+3 days")); 
      $new3 =  $day3 = date('Y-m-d',strtotime($day0 . "+3 days"));
      // $day3 =  Carbon::parse(date('Y-m-d H:i:s',strtotime('0 days')))->startOfDay(); 
      $day3 = date('Y-m-d',strtotime($day0 . "+3 days"));
      $thr = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereDate('date_time',$day3)->count();
     
      $date4 =  date('l, d F, Y',strtotime($day0 . "+4 days")); 
      $new4 =  date('Y-m-d',strtotime($day0 . "+4 days"));
      // $day4 =  Carbon::parse(date('Y-m-d H:i:s',strtotime('+1 days')))->startOfDay(); 
      $day4 = date('Y-m-d',strtotime($day0 . "+4 days"));
      $fri = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereDate('date_time',$day4)->count();
      
      $date5 =  date('l, d F, Y',strtotime($day0 . "+5 days")); 
      $new5 =  date('Y-m-d',strtotime($day0 . "+5 days"));
      //$day5 =  Carbon::parse(date('Y-m-d H:i:s',strtotime('+2 days')))->startOfDay(); 
      $day5 = date('Y-m-d',strtotime($day0 . "+5 days"));
      $sat = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereDate('date_time', $day5)->count();
     
      $date6 =  Carbon::now()->endOfWeek()->format('l, d F, Y');
      $new6 =  Carbon::now()->endOfWeek()->format('Y-m-d');
      $day6 =  Carbon::now()->endOfWeek();
      $sun = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereDate('date_time', $day6)->count();

      $weekstart = Carbon::now()->startOfWeek()->toFormattedDateString('d-m-Y');
      $weekend = Carbon::now()->endOfWeek()->toFormattedDateString('d-m-Y');
      //echo '<pre>'; print_r($day6); exit();
        // $day7 = $mytime->copy()->addDays(7)->format('l, d F, Y');
        //echo '<pre>'; print_r($wed); exit();

      $t_amount = BookingRequest::where('doctor_name',Auth::user()->userDetailsId)->whereBetween('date_time', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->sum('f_amount');

         return view('newview.new.homepage.thisweek_day_list',compact('patient_name','date0','date1','date2','date3','date4','date5','date6','mon','tue','wed','thr','fri','sat','sun','new1','new2','new3','new4','new5','new6','new0','weekstart','weekend','t_amount'));
      }

      public function homepagethisweeklist()
      {
        $mytime = Carbon::now();

       

        $doctor_id = Auth::user()->userDetailsId;
        $patient_name = Patients::join('booking_request','patient_details.id','booking_request.patient_name')
                          ->where('booking_request.doctor_name',Auth::user()->userDetailsId)
                           ->groupBy('booking_request.patient_name')
                           ->select('booking_request.*','patient_details.first_name','patient_details.id')
                           ->pluck('first_name','patient_details.id');

         return view('newview.new.homepage.thisweeklist',compact('patient_name'));
      }

      public function homepagethismonth()
      {
        $mytime = Carbon::now();
        $today = new \DateTime(); 
        $year  = (int) $today->format('Y');
        $week  = (int) $today->format('W'); // Week of the year
        $day   = (int) $today->format('w'); // Day of the week (0 = sunday)
        $month   = (int) $today->format('m');


        $doctor_id = Auth::user()->userDetailsId;
        $patient_name = Patients::join('booking_request','patient_details.id','booking_request.patient_name')
                          ->where('booking_request.doctor_name',Auth::user()->userDetailsId)
                           ->groupBy('booking_request.patient_name')
                           ->select('booking_request.*','patient_details.first_name','patient_details.id')
                           ->pluck('first_name','patient_details.id');

          $allcount = DB::table('booking_request')->where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereDate('booking_request.updated_at', Carbon::today())->count();
          $thismonth = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereDate('date_time', '>=', [Carbon::now()->startOfMonth(), Carbon::now()->endOfMonth()])->count();
          //  $thismonth = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->
          // whereDate('created_at', '<=', date('Y-m-d H:i:s',strtotime('30 days')) )->count();
          //  $last_weeks = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->
          //  whereDate('created_at', '>=', date('Y-m-d H:i:s',strtotime('-7 days')) )->count();
          $start = Carbon::now()->subWeek()->startOfWeek();
           $end = Carbon::now()->subWeek()->endOfWeek();
          $last_weeks = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereBetween('date_time', [$start, $end])->count();
          // $lastmonth = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->
          // whereDate('created_at', '>=', date('Y-m-d H:i:s',strtotime('-30 days')) )->count();
          $m_start = new Carbon('first day of this month');
          // echo '<pre>'; print_r($m_start); exit();
          $m_end = new Carbon('last day of this month');
          $lastmonth = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereBetween('date_time', [$m_start, $m_end])->count();
          $this_year = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereDate('date_time', '<=', date('Y-m-d H:i:s',strtotime(' 365 days')) )->count();
          // $this_year = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->
          //  whereDate('created_at', '<=', date('Y-m-d H:i:s',strtotime(' 365 days')) )->count();
         return view('newview.new.homepage.thismonth',compact('patient_name','allcount','thismonth','last_weeks','lastmonth','mytime','this_year','m_start','m_end'));
      }

       public function homepagethismonth_week_list()
      {
        $mytime = Carbon::now();
        $today = new \DateTime(); 
        $year  = (int) $today->format('Y');
        $week  = (int) $today->format('W'); // Week of the year
        $day   = (int) $today->format('w'); // Day of the week (0 = sunday)
        $month   = (int) $today->format('m');
        // $currentWeek = ceil((date("d",strtotime($mytime)) - date("w",strtotime($mytime)) - 1) / 7) + 1;
        $date = new DateTime('first Monday of this month');
        $thisMonth = $date->format('m');
        $mondays_arr = [];

        // Get all the Mondays in the current month and store in array
        while ($date->format('m') === $thisMonth) {
            //echo $date->format('Y-m-d'), "\n";
            $mondays_arr[] = $date->format('Y-m-d');
            $date->modify('next Monday');
        }

        // Get the day of the week (1-7 from monday to sunday)
        $day_of_week = date('N') - 1;

        // Get the day of month (1 to 31) 
        $current_week_monday_date = date('j') - $day_of_week;

        /*$day_of_week = date('N',mktime(0, 0, 0, 2, 11, 2020)) - 1;
        $current_week_monday_date = date('j',mktime(0, 0, 0, 2, 11, 2020)) - $day_of_week;*/

        $week_no = array_search($current_week_monday_date,$mondays_arr) + 1;
          //echo "Week No: ". $week_no;
          //$z = $mondays_arr[4];
       

        $date = new DateTime('first Sunday of this month');
        $thisMonth = $date->format('m');
        $sunday_arr = [];
        while ($date->format('m') === $thisMonth) {
            $sunday_arr[] = $date->format('Y-m-d');
            $date->modify('next Sunday');
        }
        $day_of_week = date('N') - 1;
        $current_week_monday_date = date('j') - $day_of_week;
        $week_no = array_search($current_week_monday_date,$sunday_arr) + 1;
          //$z = $sunday_arr[4];
          //echo '<pre>'; print_r($sunday_arr); exit;

        $doctor_id = Auth::user()->userDetailsId;
        $patient_name = Patients::join('booking_request','patient_details.id','booking_request.patient_name')
                          ->where('booking_request.doctor_name',Auth::user()->userDetailsId)
                           ->groupBy('booking_request.patient_name')
                           ->select('booking_request.*','patient_details.first_name','patient_details.id')
                           ->pluck('first_name','patient_details.id');
        
        $week_sdate = date('Y-m-01');   
        $week_date =  date('Y-m-07');
        $week_snew = date('01 F, Y');
        $week_enew = date('07 F, Y');
       $mon1 =  $mondays_arr[0];
       $sun1 =  $sunday_arr[0];

       $mon2 =  $mondays_arr[1];
       $sun2 =  $sunday_arr[1];

       $mon3 =  $mondays_arr[2];
       $sun3 =  $sunday_arr[2];

       $mon4 =  $mondays_arr[3];
       $sun4 =  $sunday_arr[3];
      
      // $mon5 =  $mondays_arr[4];
      // $sun5 =  $sunday_arr[4];

      
       if(!empty($mon5) ){
        $mon5 =  $mondays_arr[4];
      }
      else{
         $mondays_arr[4] = null;
         $mon5 =  $mondays_arr[4];
      }

      if(!empty($sun5) ){
        $sun5 =  $sunday_arr[4];
      }
      else{
         $sunday_arr[4] = null;
         $sun5 =  $sunday_arr[4];
      }

        // $sunday_arr[0];

        $week1 = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereBetween('date_time', [$week_sdate, $week_date])->count();
        
        $week_sdate2 = date('Y-m-08');   
        $week_date2 =  date('Y-m-14'); 
        $week_snew2 = date('08 F, Y');
        $week_enew2 = date('14 F, Y');        
        $week2 = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereBetween('date_time', [$week_sdate2, $week_date2])->count();
        
        $week_sdate3 = date('Y-m-15');   
        $week_date3 =  date('Y-m-21'); 
        $week_snew3 = date('15 F, Y');
        $week_enew3 = date('21 F, Y');      
        $week3 = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereBetween('date_time', [$week_sdate3, $week_date3])->count();
        
        $week_sdate4 = date('Y-m-22');
        $week_date4 =  date('Y-m-31');
        $week_snew4 = date('22 F, Y');
        $week_enew4 = date('31 F, Y');
        
          $end = new Carbon('last day of this month');

          $week4 = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereBetween('date_time', [$week_sdate4, $week_date4])->count();
       
          $week5 = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereBetween('date_time', [$mon5, $end])->count();         
        
        $end = new Carbon('last day of this month');

        $m_start = new Carbon('first day of this month');
          // echo '<pre>'; print_r($m_start); exit();
          $m_end = new Carbon('last day of this month');

          $t_amount = BookingRequest::where('doctor_name',Auth::user()->userDetailsId)->whereBetween('date_time', [Carbon::now()->startOfMonth(), Carbon::now()->endOfMonth()])->sum('f_amount');
       
         return view('newview.new.homepage.thismonth_week_list',compact('patient_name','week1','week2','week3','week4','week_sdate','week_date','week_sdate2','week_date2','week_sdate3','week_date3','week_sdate4','week_date4','week_snew','week_enew','week_snew2','week_enew2','week_snew3','week_enew3','week_snew4','week_enew4','end','week5','mon1','mon2','mon3','mon4','mon5','sun1','sun2','sun3','sun4','sun5','m_start','m_end','t_amount'));
      }

       public function homepagethismonth_day_list()
      {
        $mytime = Carbon::now();
        $today = new \DateTime(); 
        $year  = (int) $today->format('Y');
        $week  = (int) $today->format('W'); // Week of the year
        $day   = (int) $today->format('w'); // Day of the week (0 = sunday)
        $month   = (int) $today->format('m');



        $doctor_id = Auth::user()->userDetailsId;
        $patient_name = Patients::join('booking_request','patient_details.id','booking_request.patient_name')
                          ->where('booking_request.doctor_name',Auth::user()->userDetailsId)
                           ->groupBy('booking_request.patient_name')
                           ->select('booking_request.*','patient_details.first_name','patient_details.id')
                           ->pluck('first_name','patient_details.id');


        
        // $week_snew = date('23 F, Y');
        // $week_date =  date('Y-m-07');
        //$day0 = $week_sdate->startOfWeek();

        //$mon = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->whereDate('date_time',$day0)->count();
        // $searchDay = 'Thursday';
        // $searchDate = new Carbon(); //or whatever Carbon instance you're using
        // $lastThursday = Carbon::createFromFormat(strtotime("last $searchDay", $searchDate->timestamp));

          //$start = $mytime->startOfWeek(Carbon::TUESDAY);

       // $day4 =  Carbon::parse(date('Y-m-d H:i:s',strtotime('+1 days',$week_sdate4)))->startOfDay(); 
       //$end = new Carbon('last day of this month');
         //echo '<pre>'; print_r($end); exit;

         return view('newview.new.homepage.thismoth_day_wise_list',compact('patient_name'));
      }

      public function homepagethismonthweek_one_list()
      {
        $mytime = Carbon::now();
        $today = new \DateTime(); 
        $year  = (int) $today->format('Y');
        $week  = (int) $today->format('W'); // Week of the year
        $day   = (int) $today->format('w'); // Day of the week (0 = sunday)
        $month   = (int) $today->format('m');
        // $currentWeek = ceil((date("d",strtotime($mytime)) - date("w",strtotime($mytime)) - 1) / 7) + 1;
        $date = new DateTime('first Monday of this month');
        $thisMonth = $date->format('m');
        $mondays_arr = [];

        // Get all the Mondays in the current month and store in array
        while ($date->format('m') === $thisMonth) {
            //echo $date->format('Y-m-d'), "\n";
            $mondays_arr[] = $date->format('Y-m-d');
            $date->modify('next Monday');
        }

        // Get the day of the week (1-7 from monday to sunday)
        $day_of_week = date('N') - 1;

        // Get the day of month (1 to 31) 
        $current_week_monday_date = date('j') - $day_of_week;

        /*$day_of_week = date('N',mktime(0, 0, 0, 2, 11, 2020)) - 1;
        $current_week_monday_date = date('j',mktime(0, 0, 0, 2, 11, 2020)) - $day_of_week;*/

        $week_no = array_search($current_week_monday_date,$mondays_arr) + 1;
          //echo "Week No: ". $week_no;
          //$z = $mondays_arr[4];
       

        $date = new DateTime('first Sunday of this month');
        $thisMonth = $date->format('m');
        $sunday_arr = [];
        while ($date->format('m') === $thisMonth) {
            $sunday_arr[] = $date->format('Y-m-d');
            $date->modify('next Sunday');
        }
        $day_of_week = date('N') - 1;
        $current_week_monday_date = date('j') - $day_of_week;
        $week_no = array_search($current_week_monday_date,$sunday_arr) + 1;
          //$z = $sunday_arr[4];
          //echo '<pre>'; print_r($sunday_arr); exit;

        $doctor_id = Auth::user()->userDetailsId;
        $patient_name = Patients::join('booking_request','patient_details.id','booking_request.patient_name')
                          ->where('booking_request.doctor_name',Auth::user()->userDetailsId)
                           ->groupBy('booking_request.patient_name')
                           ->select('booking_request.*','patient_details.first_name','patient_details.id')
                           ->pluck('first_name','patient_details.id');
        
       $mon1 =  $mondays_arr[0];
       $sun1 =  $sunday_arr[0];

       $mon2 =  $mondays_arr[1];
       $sun2 =  $sunday_arr[1];

       $mon3 =  $mondays_arr[2];
       $sun3 =  $sunday_arr[2];

       $mon4 =  $mondays_arr[3];
       $sun4 =  $sunday_arr[3];
      
      // $mon5 =  $mondays_arr[4];
      // $sun5 =  $sunday_arr[4];

      
       if(!empty($mon5) ){
        $mon5 =  $mondays_arr[4];
      }
      else{
         $mondays_arr[4] = null;
         $mon5 =  $mondays_arr[4];
      }

      if(!empty($sun5) ){
        $sun5 =  $sunday_arr[4];
      }
      else{
         $sunday_arr[4] = null;
         $sun5 =  $sunday_arr[4];
      }

        // $sunday_arr[0];

        $week_sdate = date('Y-m-01');   
        $week_date =  date('Y-m-07');

        //$day0 = $mondays_arr[0];
        $day0 = date('Y-m-01');
        $date0 = date('l, d F, Y',strtotime($day0 . "+0 days"));
        $new0 = date('Y-m-d',strtotime($day0 . "+0 days"));
        // echo date('Y-m-d', strtotime('Monday + 1 day')); exit;
        //$tomorrow = date('Y-m-d',strtotime($day0 . "+2 days"));
        
        $mon = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereDate('date_time',date('Y-m-01'))->count();
  
        $date1 =  date('l, d F, Y',strtotime($day0 . "+1 days"));
        //echo '<pre>'; print_r ($date1); exit;
        $new1 = date('Y-m-d',strtotime($day0 . "+1 days"));
        // $day1 =  Carbon::parse(date('Y-m-d H:i:s',strtotime('-2 days')))->startOfDay(); 
        $day1 = date('Y-m-d',strtotime($day0 . "+1 days"));
        $tue = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereDate('date_time',date('Y-m-02'))->count();
         
        $date2 =  date('l, d F, Y',strtotime($day0 . "+2 days"));
        $new2 =  date('Y-m-d',strtotime($day0 . "+2 days"));
        //$day2 =  Carbon::parse(date('Y-m-d H:i:s',strtotime('-1 days')))->startOfDay(); 
        $day2 = date('Y-m-d',strtotime($day0 . "+2 days"));
        $wed = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereDate('date_time',date('Y-m-03'))->count();
         
        $date3 =  $day3 = date('l, d F, Y',strtotime($day0 . "+3 days")); 
        $new3 =  $day3 = date('Y-m-d',strtotime($day0 . "+3 days"));
        // $day3 =  Carbon::parse(date('Y-m-d H:i:s',strtotime('0 days')))->startOfDay(); 
        $day3 = date('Y-m-d',strtotime($day0 . "+3 days"));
        $thr = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereDate('date_time',date('Y-m-04'))->count();
       
        $date4 =  date('l, d F, Y',strtotime($day0 . "+4 days")); 
        $new4 =  date('Y-m-d',strtotime($day0 . "+4 days"));
        // $day4 =  Carbon::parse(date('Y-m-d H:i:s',strtotime('+1 days')))->startOfDay(); 
        $day4 = date('Y-m-d',strtotime($day0 . "+4 days"));
        $fri = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereDate('date_time',date('Y-m-05'))->count();
        
        $date5 =  date('l, d F, Y',strtotime($day0 . "+5 days")); 
        $new5 =  date('Y-m-d',strtotime($day0 . "+5 days"));
        //$day5 =  Carbon::parse(date('Y-m-d H:i:s',strtotime('+2 days')))->startOfDay(); 
        $day5 = date('Y-m-d',strtotime($day0 . "+5 days"));
        $sat = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereDate('date_time', date('Y-m-06'))->count();
       
        $date6 =  date('l, d F, Y',strtotime($day0 . "+6 days")); 
        $new6 =  date('Y-m-d',strtotime($day0 . "+6 days"));
        $day6 =  date('Y-m-d',strtotime($day0 . "+6 days"));
        //echo '<pre>'; print_r($date6); exit;
        $sun = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereDate('date_time', date('Y-m-07'))->count();
        
        
        return view('newview.new.homepage.month_weeks.week_one_list',compact('patient_name','date0','date1','date2','date3','date4','date5','date6','mon','tue','wed','thr','fri','sat','sun','new1','new2','new3','new4','new5','new6','new0'));
      }

      public function homepagethismonthweek_two_list()
      {
        $mytime = Carbon::now();
        $today = new \DateTime(); 
        $year  = (int) $today->format('Y');
        $week  = (int) $today->format('W'); // Week of the year
        $day   = (int) $today->format('w'); // Day of the week (0 = sunday)
        $month   = (int) $today->format('m');
        // $currentWeek = ceil((date("d",strtotime($mytime)) - date("w",strtotime($mytime)) - 1) / 7) + 1;
        $date = new DateTime('first Monday of this month');
        $thisMonth = $date->format('m');
        $mondays_arr = [];

        // Get all the Mondays in the current month and store in array
        while ($date->format('m') === $thisMonth) {
            //echo $date->format('Y-m-d'), "\n";
            $mondays_arr[] = $date->format('Y-m-d');
            $date->modify('next Monday');
        }

        // Get the day of the week (1-7 from monday to sunday)
        $day_of_week = date('N') - 1;

        // Get the day of month (1 to 31) 
        $current_week_monday_date = date('j') - $day_of_week;

        /*$day_of_week = date('N',mktime(0, 0, 0, 2, 11, 2020)) - 1;
        $current_week_monday_date = date('j',mktime(0, 0, 0, 2, 11, 2020)) - $day_of_week;*/

        $week_no = array_search($current_week_monday_date,$mondays_arr) + 1;
          //echo "Week No: ". $week_no;
          //$z = $mondays_arr[4];
       

        $date = new DateTime('first Sunday of this month');
        $thisMonth = $date->format('m');
        $sunday_arr = [];
        while ($date->format('m') === $thisMonth) {
            $sunday_arr[] = $date->format('Y-m-d');
            $date->modify('next Sunday');
        }
        $day_of_week = date('N') - 1;
        $current_week_monday_date = date('j') - $day_of_week;
        $week_no = array_search($current_week_monday_date,$sunday_arr) + 1;
          //$z = $sunday_arr[4];
          //echo '<pre>'; print_r($sunday_arr); exit;

        $doctor_id = Auth::user()->userDetailsId;
        $patient_name = Patients::join('booking_request','patient_details.id','booking_request.patient_name')
                          ->where('booking_request.doctor_name',Auth::user()->userDetailsId)
                           ->groupBy('booking_request.patient_name')
                           ->select('booking_request.*','patient_details.first_name','patient_details.id')
                           ->pluck('first_name','patient_details.id');
        
       $mon1 =  $mondays_arr[0];
       $sun1 =  $sunday_arr[0];

       $mon2 =  $mondays_arr[1];
       $sun2 =  $sunday_arr[1];

       $mon3 =  $mondays_arr[2];
       $sun3 =  $sunday_arr[2];

       $mon4 =  $mondays_arr[3];
       $sun4 =  $sunday_arr[3];
      
      // $mon5 =  $mondays_arr[4];
      // $sun5 =  $sunday_arr[4];

      
       if(!empty($mon5) ){
        $mon5 =  $mondays_arr[4];
      }
      else{
         $mondays_arr[4] = null;
         $mon5 =  $mondays_arr[4];
      }

      if(!empty($sun5) ){
        $sun5 =  $sunday_arr[4];
      }
      else{
         $sunday_arr[4] = null;
         $sun5 =  $sunday_arr[4];
      }

        // $sunday_arr[0];
        $week_sdate = date('Y-m-08');   
        $week_date =  date('Y-m-14');

        //$day0 = $mondays_arr[1];
        $day0 = $week_sdate;
        $date0 = date('l, d F, Y',strtotime($day0 . "+0 days"));
        $new0 = date('Y-m-d',strtotime($day0 . "+0 days"));
        // echo date('Y-m-d', strtotime('Monday + 1 day')); exit;
        //$tomorrow = date('Y-m-d',strtotime($day0 . "+2 days"));
        
        $mon = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereDate('date_time',$day0)->count();
  
        $date1 =  date('l, d F, Y',strtotime($day0 . "+1 days"));
        //echo '<pre>'; print_r ($date1); exit;
        $new1 = date('Y-m-d',strtotime($day0 . "+1 days"));
        // $day1 =  Carbon::parse(date('Y-m-d H:i:s',strtotime('-2 days')))->startOfDay(); 
        $day1 = date('Y-m-d',strtotime($day0 . "+1 days"));
        $tue = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereDate('date_time',$day1)->count();
         
        $date2 =  date('l, d F, Y',strtotime($day0 . "+2 days"));
        $new2 =  date('Y-m-d',strtotime($day0 . "+2 days"));
        //$day2 =  Carbon::parse(date('Y-m-d H:i:s',strtotime('-1 days')))->startOfDay(); 
        $day2 = date('Y-m-d',strtotime($day0 . "+2 days"));
        $wed = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereDate('date_time',$day2)->count();
         
        $date3 =  $day3 = date('l, d F, Y',strtotime($day0 . "+3 days")); 
        $new3 =  $day3 = date('Y-m-d',strtotime($day0 . "+3 days"));
        // $day3 =  Carbon::parse(date('Y-m-d H:i:s',strtotime('0 days')))->startOfDay(); 
        $day3 = date('Y-m-d',strtotime($day0 . "+3 days"));
        $thr = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereDate('date_time',$day3)->count();
       
        $date4 =  date('l, d F, Y',strtotime($day0 . "+4 days")); 
        $new4 =  date('Y-m-d',strtotime($day0 . "+4 days"));
        // $day4 =  Carbon::parse(date('Y-m-d H:i:s',strtotime('+1 days')))->startOfDay(); 
        $day4 = date('Y-m-d',strtotime($day0 . "+4 days"));
        $fri = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereDate('date_time',$day4)->count();
        
        $date5 =  date('l, d F, Y',strtotime($day0 . "+5 days")); 
        $new5 =  date('Y-m-d',strtotime($day0 . "+5 days"));
        //$day5 =  Carbon::parse(date('Y-m-d H:i:s',strtotime('+2 days')))->startOfDay(); 
        $day5 = date('Y-m-d',strtotime($day0 . "+5 days"));
        $sat = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereDate('date_time', $day5)->count();
       
        $date6 =  date('l, d F, Y',strtotime($day0 . "+6 days")); 
        $new6 =  date('Y-m-d',strtotime($day0 . "+6 days"));
        $day6 =  date('Y-m-d',strtotime($day0 . "+6 days"));
        //echo '<pre>'; print_r($date6); exit;
        $sun = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereDate('date_time', $day6)->count();
        
        
        return view('newview.new.homepage.month_weeks.week_two_list',compact('patient_name','date0','date1','date2','date3','date4','date5','date6','mon','tue','wed','thr','fri','sat','sun','new1','new2','new3','new4','new5','new6','new0'));
      }

      public function homepagethismonthweek_three_list()
      {
        $mytime = Carbon::now();
        $today = new \DateTime(); 
        $year  = (int) $today->format('Y');
        $week  = (int) $today->format('W'); // Week of the year
        $day   = (int) $today->format('w'); // Day of the week (0 = sunday)
        $month   = (int) $today->format('m');
        // $currentWeek = ceil((date("d",strtotime($mytime)) - date("w",strtotime($mytime)) - 1) / 7) + 1;
        $date = new DateTime('first Monday of this month');
        $thisMonth = $date->format('m');
        $mondays_arr = [];

        // Get all the Mondays in the current month and store in array
        while ($date->format('m') === $thisMonth) {
            //echo $date->format('Y-m-d'), "\n";
            $mondays_arr[] = $date->format('Y-m-d');
            $date->modify('next Monday');
        }

        // Get the day of the week (1-7 from monday to sunday)
        $day_of_week = date('N') - 1;

        // Get the day of month (1 to 31) 
        $current_week_monday_date = date('j') - $day_of_week;

        /*$day_of_week = date('N',mktime(0, 0, 0, 2, 11, 2020)) - 1;
        $current_week_monday_date = date('j',mktime(0, 0, 0, 2, 11, 2020)) - $day_of_week;*/

        $week_no = array_search($current_week_monday_date,$mondays_arr) + 1;
          //echo "Week No: ". $week_no;
          //$z = $mondays_arr[4];
       

        $date = new DateTime('first Sunday of this month');
        $thisMonth = $date->format('m');
        $sunday_arr = [];
        while ($date->format('m') === $thisMonth) {
            $sunday_arr[] = $date->format('Y-m-d');
            $date->modify('next Sunday');
        }
        $day_of_week = date('N') - 1;
        $current_week_monday_date = date('j') - $day_of_week;
        $week_no = array_search($current_week_monday_date,$sunday_arr) + 1;
          //$z = $sunday_arr[4];
          //echo '<pre>'; print_r($sunday_arr); exit;

        $doctor_id = Auth::user()->userDetailsId;
        $patient_name = Patients::join('booking_request','patient_details.id','booking_request.patient_name')
                          ->where('booking_request.doctor_name',Auth::user()->userDetailsId)
                           ->groupBy('booking_request.patient_name')
                           ->select('booking_request.*','patient_details.first_name','patient_details.id')
                           ->pluck('first_name','patient_details.id');
        
       $mon1 =  $mondays_arr[0];
       $sun1 =  $sunday_arr[0];

       $mon2 =  $mondays_arr[1];
       $sun2 =  $sunday_arr[1];

       $mon3 =  $mondays_arr[2];
       $sun3 =  $sunday_arr[2];

       $mon4 =  $mondays_arr[3];
       $sun4 =  $sunday_arr[3];
      
      // $mon5 =  $mondays_arr[4];
      // $sun5 =  $sunday_arr[4];

      
       if(!empty($mon5) ){
        $mon5 =  $mondays_arr[4];
      }
      else{
         $mondays_arr[4] = null;
         $mon5 =  $mondays_arr[4];
      }

      if(!empty($sun5) ){
        $sun5 =  $sunday_arr[4];
      }
      else{
         $sunday_arr[4] = null;
         $sun5 =  $sunday_arr[4];
      }

        // $sunday_arr[0];
        $week_sdate = date('Y-m-15');   
        $week_date =  date('Y-m-21');

        //$day0 = $mondays_arr[2];
        $day0 = $week_sdate;
        $date0 = date('l, d F, Y',strtotime($day0 . "+0 days"));
        $new0 = date('Y-m-d',strtotime($day0 . "+0 days"));
        // echo date('Y-m-d', strtotime('Monday + 1 day')); exit;
        //$tomorrow = date('Y-m-d',strtotime($day0 . "+2 days"));
        
        $mon = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereDate('date_time',$day0)->count();
  
        $date1 =  date('l, d F, Y',strtotime($day0 . "+1 days"));
        //echo '<pre>'; print_r ($date1); exit;
        $new1 = date('Y-m-d',strtotime($day0 . "+1 days"));
        // $day1 =  Carbon::parse(date('Y-m-d H:i:s',strtotime('-2 days')))->startOfDay(); 
        $day1 = date('Y-m-d',strtotime($day0 . "+1 days"));
        $tue = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereDate('date_time',$day1)->count();
         
        $date2 =  date('l, d F, Y',strtotime($day0 . "+2 days"));
        $new2 =  date('Y-m-d',strtotime($day0 . "+2 days"));
        //$day2 =  Carbon::parse(date('Y-m-d H:i:s',strtotime('-1 days')))->startOfDay(); 
        $day2 = date('Y-m-d',strtotime($day0 . "+2 days"));
        $wed = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereDate('date_time',$day2)->count();
         
        $date3 =  $day3 = date('l, d F, Y',strtotime($day0 . "+3 days")); 
        $new3 =  $day3 = date('Y-m-d',strtotime($day0 . "+3 days"));
        // $day3 =  Carbon::parse(date('Y-m-d H:i:s',strtotime('0 days')))->startOfDay(); 
        $day3 = date('Y-m-d',strtotime($day0 . "+3 days"));
        $thr = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereDate('date_time',$day3)->count();
       
        $date4 =  date('l, d F, Y',strtotime($day0 . "+4 days")); 
        $new4 =  date('Y-m-d',strtotime($day0 . "+4 days"));
        // $day4 =  Carbon::parse(date('Y-m-d H:i:s',strtotime('+1 days')))->startOfDay(); 
        $day4 = date('Y-m-d',strtotime($day0 . "+4 days"));
        $fri = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereDate('date_time',$day4)->count();
        
        $date5 =  date('l, d F, Y',strtotime($day0 . "+5 days")); 
        $new5 =  date('Y-m-d',strtotime($day0 . "+5 days"));
        //$day5 =  Carbon::parse(date('Y-m-d H:i:s',strtotime('+2 days')))->startOfDay(); 
        $day5 = date('Y-m-d',strtotime($day0 . "+5 days"));
        $sat = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereDate('date_time', $day5)->count();
       
        $date6 =  date('l, d F, Y',strtotime($day0 . "+6 days")); 
        $new6 =  date('Y-m-d',strtotime($day0 . "+6 days"));
        $day6 =  date('Y-m-d',strtotime($day0 . "+6 days"));
        //echo '<pre>'; print_r($date6); exit;
        $sun = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereDate('date_time', $day6)->count();
        
        
        return view('newview.new.homepage.month_weeks.week_three_list',compact('patient_name','date0','date1','date2','date3','date4','date5','date6','mon','tue','wed','thr','fri','sat','sun','new1','new2','new3','new4','new5','new6','new0'));
      }

      public function homepagethismonthweek_four_list()
      {
        $mytime = Carbon::now();
        $today = new \DateTime(); 
        $year  = (int) $today->format('Y');
        $week  = (int) $today->format('W'); // Week of the year
        $day   = (int) $today->format('w'); // Day of the week (0 = sunday)
        $month   = (int) $today->format('m');
        // $currentWeek = ceil((date("d",strtotime($mytime)) - date("w",strtotime($mytime)) - 1) / 7) + 1;
        $date = new DateTime('first Monday of this month');
        $thisMonth = $date->format('m');
        $mondays_arr = [];

        // Get all the Mondays in the current month and store in array
        while ($date->format('m') === $thisMonth) {
            //echo $date->format('Y-m-d'), "\n";
            $mondays_arr[] = $date->format('Y-m-d');
            $date->modify('next Monday');
        }

        // Get the day of the week (1-7 from monday to sunday)
        $day_of_week = date('N') - 1;

        // Get the day of month (1 to 31) 
        $current_week_monday_date = date('j') - $day_of_week;

        /*$day_of_week = date('N',mktime(0, 0, 0, 2, 11, 2020)) - 1;
        $current_week_monday_date = date('j',mktime(0, 0, 0, 2, 11, 2020)) - $day_of_week;*/

        $week_no = array_search($current_week_monday_date,$mondays_arr) + 1;
          //echo "Week No: ". $week_no;
          //$z = $mondays_arr[4];
       

        $date = new DateTime('first Sunday of this month');
        $thisMonth = $date->format('m');
        $sunday_arr = [];
        while ($date->format('m') === $thisMonth) {
            $sunday_arr[] = $date->format('Y-m-d');
            $date->modify('next Sunday');
        }
        $day_of_week = date('N') - 1;
        $current_week_monday_date = date('j') - $day_of_week;
        $week_no = array_search($current_week_monday_date,$sunday_arr) + 1;
          //$z = $sunday_arr[4];
          //echo '<pre>'; print_r($sunday_arr); exit;

        $doctor_id = Auth::user()->userDetailsId;
        $patient_name = Patients::join('booking_request','patient_details.id','booking_request.patient_name')
                          ->where('booking_request.doctor_name',Auth::user()->userDetailsId)
                           ->groupBy('booking_request.patient_name')
                           ->select('booking_request.*','patient_details.first_name','patient_details.id')
                           ->pluck('first_name','patient_details.id');
        
       $mon1 =  $mondays_arr[0];
       $sun1 =  $sunday_arr[0];

       $mon2 =  $mondays_arr[1];
       $sun2 =  $sunday_arr[1];

       $mon3 =  $mondays_arr[2];
       $sun3 =  $sunday_arr[2];

       $mon4 =  $mondays_arr[3];
       $sun4 =  $sunday_arr[3];
      
      // $mon5 =  $mondays_arr[4];
      // $sun5 =  $sunday_arr[4];

      
       if(!empty($mon5) ){
        $mon5 =  $mondays_arr[4];
      }
      else{
         $mondays_arr[4] = null;
         $mon5 =  $mondays_arr[4];
      }

      if(!empty($sun5) ){
        $sun5 =  $sunday_arr[4];
      }
      else{
         $sunday_arr[4] = null;
         $sun5 =  $sunday_arr[4];
      }

        // $sunday_arr[0];
        $week_sdate = date('Y-m-22');   
        $week_date =  date('Y-m-28');

        //$day0 = $mondays_arr[3];
        $day0 = $week_sdate;

        $date0 = date('l, d F, Y',strtotime($day0 . "+0 days"));
        $new0 = date('Y-m-d',strtotime($day0 . "+0 days"));
        // echo date('Y-m-d', strtotime('Monday + 1 day')); exit;
        //$tomorrow = date('Y-m-d',strtotime($day0 . "+2 days"));
        
        $mon = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereDate('date_time',$day0)->count();
  
        $date1 =  date('l, d F, Y',strtotime($day0 . "+1 days"));
        //echo '<pre>'; print_r ($date1); exit;
        $new1 = date('Y-m-d',strtotime($day0 . "+1 days"));
        // $day1 =  Carbon::parse(date('Y-m-d H:i:s',strtotime('-2 days')))->startOfDay(); 
        $day1 = date('Y-m-d',strtotime($day0 . "+1 days"));
        $tue = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereDate('date_time',$day1)->count();
         
        $date2 =  date('l, d F, Y',strtotime($day0 . "+2 days"));
        $new2 =  date('Y-m-d',strtotime($day0 . "+2 days"));
        //$day2 =  Carbon::parse(date('Y-m-d H:i:s',strtotime('-1 days')))->startOfDay(); 
        $day2 = date('Y-m-d',strtotime($day0 . "+2 days"));
        $wed = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereDate('date_time',$day2)->count();
         
        $date3 =  $day3 = date('l, d F, Y',strtotime($day0 . "+3 days")); 
        $new3 =  $day3 = date('Y-m-d',strtotime($day0 . "+3 days"));
        // $day3 =  Carbon::parse(date('Y-m-d H:i:s',strtotime('0 days')))->startOfDay(); 
        $day3 = date('Y-m-d',strtotime($day0 . "+3 days"));
        $thr = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereDate('date_time',$day3)->count();
       
        $date4 =  date('l, d F, Y',strtotime($day0 . "+4 days")); 
        $new4 =  date('Y-m-d',strtotime($day0 . "+4 days"));
        // $day4 =  Carbon::parse(date('Y-m-d H:i:s',strtotime('+1 days')))->startOfDay(); 
        $day4 = date('Y-m-d',strtotime($day0 . "+4 days"));
        $fri = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereDate('date_time',$day4)->count();
        
        $date5 =  date('l, d F, Y',strtotime($day0 . "+5 days")); 
        $new5 =  date('Y-m-d',strtotime($day0 . "+5 days"));
        //$day5 =  Carbon::parse(date('Y-m-d H:i:s',strtotime('+2 days')))->startOfDay(); 
        $day5 = date('Y-m-d',strtotime($day0 . "+5 days"));
        $sat = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereDate('date_time', $day5)->count();
       
        $date6 =  date('l, d F, Y',strtotime($day0 . "+6 days")); 
        $new6 =  date('Y-m-d',strtotime($day0 . "+6 days"));
        $day6 =  date('Y-m-d',strtotime($day0 . "+6 days"));
        //echo '<pre>'; print_r($date6); exit;
        $sun = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereDate('date_time', $day6)->count();

        $date7 =  date('l, d F, Y',strtotime($day0 . "+7 days")); 
        $new7 =  date('Y-m-d',strtotime($day0 . "+7 days"));
        $day7 =  date('Y-m-d',strtotime($day0 . "+7 days"));
        //echo '<pre>'; print_r($date6); exit;
        $sun1 = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereDate('date_time', $day7)->count();

        $date8 =  date('l, d F, Y',strtotime($day0 . "+8 days")); 
        $new8 =  date('Y-m-d',strtotime($day0 . "+8 days"));
        $day8 =  date('Y-m-d',strtotime($day0 . "+8 days"));
        //echo '<pre>'; print_r($date6); exit;
        $sun2 = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereDate('date_time', $day8)->count();

        $date9 =  date('l, d F, Y',strtotime($day0 . "+9 days")); 
        $new9 =  date('Y-m-d',strtotime($day0 . "+9 days"));
        $day9 =  date('Y-m-d',strtotime($day0 . "+9 days"));
        //echo '<pre>'; print_r($date6); exit;
        $sun3 = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereDate('date_time', $day9)->count();
        
        
        return view('newview.new.homepage.month_weeks.week_four_list',compact('patient_name','date0','date1','date2','date3','date4','date5','date6','date7','date8','date9','mon','tue','wed','thr','fri','sat','sun','sun1','sun2','sun3','new1','new2','new3','new4','new5','new6','new7','new8','new9','new0'));
      }

      public function homepagethismonthweek_five_list()
      {
        $mytime = Carbon::now();
        $today = new \DateTime(); 
        $year  = (int) $today->format('Y');
        $week  = (int) $today->format('W'); // Week of the year
        $day   = (int) $today->format('w'); // Day of the week (0 = sunday)
        $month   = (int) $today->format('m');
        // $currentWeek = ceil((date("d",strtotime($mytime)) - date("w",strtotime($mytime)) - 1) / 7) + 1;
        $date = new DateTime('first Monday of this month');
        $thisMonth = $date->format('m');
        $mondays_arr = [];

        // Get all the Mondays in the current month and store in array
        while ($date->format('m') === $thisMonth) {
            //echo $date->format('Y-m-d'), "\n";
            $mondays_arr[] = $date->format('Y-m-d');
            $date->modify('next Monday');
        }

        // Get the day of the week (1-7 from monday to sunday)
        $day_of_week = date('N') - 1;

        // Get the day of month (1 to 31) 
        $current_week_monday_date = date('j') - $day_of_week;

        /*$day_of_week = date('N',mktime(0, 0, 0, 2, 11, 2020)) - 1;
        $current_week_monday_date = date('j',mktime(0, 0, 0, 2, 11, 2020)) - $day_of_week;*/

        $week_no = array_search($current_week_monday_date,$mondays_arr) + 1;
          //echo "Week No: ". $week_no;
          //$z = $mondays_arr[4];
       

        $date = new DateTime('first Sunday of this month');
        $thisMonth = $date->format('m');
        $sunday_arr = [];
        while ($date->format('m') === $thisMonth) {
            $sunday_arr[] = $date->format('Y-m-d');
            $date->modify('next Sunday');
        }
        $day_of_week = date('N') - 1;
        $current_week_monday_date = date('j') - $day_of_week;
        $week_no = array_search($current_week_monday_date,$sunday_arr) + 1;
          //$z = $sunday_arr[4];
          //echo '<pre>'; print_r($sunday_arr); exit;

        $doctor_id = Auth::user()->userDetailsId;
        $patient_name = Patients::join('booking_request','patient_details.id','booking_request.patient_name')
                          ->where('booking_request.doctor_name',Auth::user()->userDetailsId)
                           ->groupBy('booking_request.patient_name')
                           ->select('booking_request.*','patient_details.first_name','patient_details.id')
                           ->pluck('first_name','patient_details.id');
        
       $mon1 =  $mondays_arr[0];
       $sun1 =  $sunday_arr[0];

       $mon2 =  $mondays_arr[1];
       $sun2 =  $sunday_arr[1];

       $mon3 =  $mondays_arr[2];
       $sun3 =  $sunday_arr[2];

       $mon4 =  $mondays_arr[3];
       $sun4 =  $sunday_arr[3];
      
      // $mon5 =  $mondays_arr[4];
      // $sun5 =  $sunday_arr[4];

      
       if(!empty($mon5) ){
        $mon5 =  $mondays_arr[4];
      }
      else{
         $mondays_arr[4] = null;
         $mon5 =  $mondays_arr[4];
      }

      if(!empty($sun5) ){
        $sun5 =  $sunday_arr[4];
      }
      else{
         $sunday_arr[4] = null;
         $sun5 =  $sunday_arr[4];
      }

        // $sunday_arr[0];
        $week_sdate = date('Y-m-29');   
        $week_date =  date('Y-m-31');
        
        //$day0 = $mondays_arr[4];
        $day0 = $week_sdate;
        
        $date0 = date('l, d F, Y',strtotime($day0 . "+0 days"));
        $new0 = date('Y-m-d',strtotime($day0 . "+0 days"));
        // echo date('Y-m-d', strtotime('Monday + 1 day')); exit;
        //$tomorrow = date('Y-m-d',strtotime($day0 . "+2 days"));
        
        $mon = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereDate('date_time',$day0)->count();
  
        $date1 =  date('l, d F, Y',strtotime($day0 . "+1 days"));
        //echo '<pre>'; print_r ($date1); exit;
        $new1 = date('Y-m-d',strtotime($day0 . "+1 days"));
        // $day1 =  Carbon::parse(date('Y-m-d H:i:s',strtotime('-2 days')))->startOfDay(); 
        $day1 = date('Y-m-d',strtotime($day0 . "+1 days"));
        $tue = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereDate('date_time',$day1)->count();
         
        $date2 =  date('l, d F, Y',strtotime($day0 . "+2 days"));
        $new2 =  date('Y-m-d',strtotime($day0 . "+2 days"));
        //$day2 =  Carbon::parse(date('Y-m-d H:i:s',strtotime('-1 days')))->startOfDay(); 
        $day2 = date('Y-m-d',strtotime($day0 . "+2 days"));
        $wed = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereDate('date_time',$day2)->count();
         
        $date3 =  $day3 = date('l, d F, Y',strtotime($day0 . "+3 days")); 
        $new3 =  $day3 = date('Y-m-d',strtotime($day0 . "+3 days"));
        // $day3 =  Carbon::parse(date('Y-m-d H:i:s',strtotime('0 days')))->startOfDay(); 
        $day3 = date('Y-m-d',strtotime($day0 . "+3 days"));
        $thr = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereDate('date_time',$day3)->count();
       
        $date4 =  date('l, d F, Y',strtotime($day0 . "+4 days")); 
        $new4 =  date('Y-m-d',strtotime($day0 . "+4 days"));
        // $day4 =  Carbon::parse(date('Y-m-d H:i:s',strtotime('+1 days')))->startOfDay(); 
        $day4 = date('Y-m-d',strtotime($day0 . "+4 days"));
        $fri = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereDate('date_time',$day4)->count();
        
        $date5 =  date('l, d F, Y',strtotime($day0 . "+5 days")); 
        $new5 =  date('Y-m-d',strtotime($day0 . "+5 days"));
        //$day5 =  Carbon::parse(date('Y-m-d H:i:s',strtotime('+2 days')))->startOfDay(); 
        $day5 = date('Y-m-d',strtotime($day0 . "+5 days"));
        $sat = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereDate('date_time', $day5)->count();
       
        $date6 =  date('l, d F, Y',strtotime($day0 . "+6 days")); 
        $new6 =  date('Y-m-d',strtotime($day0 . "+6 days"));
        $day6 =  date('Y-m-d',strtotime($day0 . "+6 days"));
        //echo '<pre>'; print_r($date6); exit;
        $sun = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereDate('date_time', $day6)->count();
        
        
        return view('newview.new.homepage.month_weeks.week_five_list',compact('patient_name','date0','date1','date2','date3','date4','date5','date6','mon','tue','wed','thr','fri','sat','sun','new1','new2','new3','new4','new5','new6','new0'));
      }

      public function homepagethismonthlist()
      {
        $mytime = Carbon::now();

        $doctor_id = Auth::user()->userDetailsId;
        $patient_name = Patients::join('booking_request','patient_details.id','booking_request.patient_name')
                          ->where('booking_request.doctor_name',Auth::user()->userDetailsId)
                           ->groupBy('booking_request.patient_name')
                           ->select('booking_request.*','patient_details.first_name','patient_details.id')
                           ->pluck('first_name','patient_details.id');

         return view('newview.new.homepage.thismonthlist',compact('patient_name'));
      }

      public function homepagethisyear_month_wise_list()
      {

        $mytime = Carbon::now();

        $doctor_id = Auth::user()->userDetailsId;
        $patient_name = Patients::join('booking_request','patient_details.id','booking_request.patient_name')
                          ->where('booking_request.doctor_name',Auth::user()->userDetailsId)
                           ->groupBy('booking_request.patient_name')
                           ->select('booking_request.*','patient_details.first_name','patient_details.id')
                           ->pluck('first_name','patient_details.id');

         return view('newview.new.homepage.thisyear_month_wise_list',compact('patient_name'));
      }

       public function homepagethisyear()
      {
        
        $mytime = Carbon::now();
        $today = new \DateTime(); 
        $year  = (int) $today->format('Y');
        $week  = (int) $today->format('W'); // Week of the year
        $day   = (int) $today->format('w'); // Day of the week (0 = sunday)
        $month   = (int) $today->format('m');



        $doctor_id = Auth::user()->userDetailsId;
        $patient_name = Patients::join('booking_request','patient_details.id','booking_request.patient_name')
                          ->where('booking_request.doctor_name',Auth::user()->userDetailsId)
                           ->groupBy('booking_request.patient_name')
                           ->select('booking_request.*','patient_details.first_name','patient_details.id')
                           ->pluck('first_name','patient_details.id');

          $allcount = DB::table('booking_request')->where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereDate('booking_request.updated_at', Carbon::today())->count();

          // $allcount = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->whereBetween('date_time', [date($c_year.'-01-01 H:i:s'),date($c_year.'-12-31 H:i:s')])->count();
          $l_start = Carbon::now()->startOfYear();
          //echo '<pre>'; print_r($l_start); exit;
          $l_end = Carbon::now()->endOfYear();

          $m_start = Carbon::now()->subMonth()->startOfMonth();
          $m_end = Carbon::now()->subMonth()->endOfMonth();

          //  $last_year = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->
          //  whereDate('created_at', '>=', date('Y-m-d H:i:s',strtotime('-365 days')) )->count();

           $last_year = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereBetween('date_time', [$l_start, $l_end])->count();

          // $lastmonth = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->
          // whereDate('created_at', '>=', date('Y-m-d H:i:s',strtotime('-30 days')) )->count();
          $lastmonth = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereBetween('date_time', [$m_start, $m_end])->count();

          $this_year = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->
           whereDate('created_at', '<=', date('Y-m-d H:i:s',strtotime(' 365 days')) )->count();
         return view('newview.new.homepage.thisyear',compact('patient_name','allcount','last_year','lastmonth','mytime','this_year','l_start','l_end'));
      }
      
      public function home_year(Request $request)
      {

          $c_year = $request->c_year;
          $y_start = date($c_year.'-01-01 H:i:s');
          $y_end = date($c_year.'-12-31 H:i:s');
          //echo $y_end; exit;
           $m_end = Carbon::now()->subMonth()->endOfMonth();

           $start_year = Carbon::now()->startOfYear()->format('d F, Y');
           $end_year =  Carbon::now()->endOfYear()->format('d F, Y');

           $all_count = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereBetween('date_time', [date($c_year.'-01-01 H:i:s'),date($c_year.'-12-31 H:i:s')])->count();
           $l_start = Carbon::now()->subYear()->startOfYear();
           $l_end = Carbon::now()->subYear()->endOfYear();
           $last_year = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereBetween('date_time', [$l_start, $l_end])->count();

           $m_start = Carbon::now()->subMonth()->startOfMonth();
           $m_end = Carbon::now()->subMonth()->endOfMonth();


           $last_month = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereBetween('date_time', [$m_start, $m_end])->count();

           $this_year = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereBetween('date_time', [date($c_year.'-01-01 H:i:s'),date($c_year.'-12-31 H:i:s')])->count();
           
          $date1 = date($c_year.'-01-01');
          $date2 = date($c_year.'-01-31');
        
           $jan = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereBetween('date_time', [date($c_year.'-01-01'), date($c_year.'-01-31')])->count();
           $feb = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereBetween('date_time', [date($c_year.'-02-01'), date($c_year.'-02-29')])->count();
           $mar = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereBetween('date_time', [date($c_year.'-03-01'), date($c_year.'-03-31')])->count();
           $apr = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereBetween('date_time', [date($c_year.'-04-01'), date($c_year.'-04-31')])->count();
           $may = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereBetween('date_time', [date($c_year.'-05-01'), date($c_year.'-05-31')])->count();
           $jun = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereBetween('date_time', [date($c_year.'-06-01'), date($c_year.'-06-31')])->count();
           $jul = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereBetween('date_time', [date($c_year.'-07-01'), date($c_year.'-07-31')])->count();
           $aug = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereBetween('date_time', [date($c_year.'-08-01'), date($c_year.'-08-31')])->count();
           $sep = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereBetween('date_time', [date($c_year.'-09-01'), date($c_year.'-09-31')])->count();
           $oct = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereBetween('date_time', [date($c_year.'-10-01'), date($c_year.'-10-31')])->count();
           $nov = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereBetween('date_time', [date($c_year.'-11-01'), date($c_year.'-11-31')])->count();
           $dec = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereBetween('date_time', [date($c_year.'-12-01'), date($c_year.'-12-31')])->count();
          

          // $jundata = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->whereBetween('date_time', [date($c_year.'-06-01'), date($c_year.'-06-31')])->get();
          
          $t_amount = BookingRequest::where('doctor_name',Auth::user()->userDetailsId)->whereBetween('date_time', [date($c_year.'-01-01 H:i:s'),date($c_year.'-12-31 H:i:s')])->sum('f_amount');
           
           if ($all_count || $last_month || $last_year || $this_year) {
            return response()->json(["status" => true,"massage"=>"Year Count Data retrieved successfully","all_count" => $all_count,"last_month" => $last_month,"last_year" => $last_year,"this_year" => $this_year,"jan" => $jan,"feb" => $feb,"mar" => $mar,"apr" => $apr,"may" => $may,"jun" => $jun,"jul" => $jul,"aug" => $aug,"sep" => $sep,"oct" => $oct,"nov" => $nov,"dec" => $dec,"c_year" => $c_year,"start_year" => $start_year,"end_year" => $end_year,"date1"=>$date1,"date2"=>$date2,'earnings'=>$t_amount]);
       }else{
           return response()->json(["status" => false,"data" => '']);
       }
      //  return view('newview.new.homepage.thisyear',compact('patient_name','allcount','last_year','lastmonth','mytime','this_year','l_start','l_end'));
      }
       
       public function homepagethisyear_week_list()
      {
        $mytime = Carbon::now();
        $today = new \DateTime(); 
        $year  = (int) $today->format('Y');
        $week  = (int) $today->format('W'); // Week of the year
        $day   = (int) $today->format('w'); // Day of the week (0 = sunday)
        $month   = (int) $today->format('m');
        // $currentWeek = ceil((date("d",strtotime($mytime)) - date("w",strtotime($mytime)) - 1) / 7) + 1;
        $date = new DateTime('first Monday of this month');
        $thisMonth = $date->format('m');
        $mondays_arr = [];

        // Get all the Mondays in the current month and store in array
        while ($date->format('m') === $thisMonth) {
            //echo $date->format('Y-m-d'), "\n";
            $mondays_arr[] = $date->format('Y-m-d');
            $date->modify('next Monday');
        }

        // Get the day of the week (1-7 from monday to sunday)
        $day_of_week = date('N') - 1;

        // Get the day of month (1 to 31) 
        $current_week_monday_date = date('j') - $day_of_week;

        /*$day_of_week = date('N',mktime(0, 0, 0, 2, 11, 2020)) - 1;
        $current_week_monday_date = date('j',mktime(0, 0, 0, 2, 11, 2020)) - $day_of_week;*/

        $week_no = array_search($current_week_monday_date,$mondays_arr) + 1;
          //echo "Week No: ". $week_no;
          //$z = $mondays_arr[4];
       

        $date = new DateTime('first Sunday of this month');
        $thisMonth = $date->format('m');
        $sunday_arr = [];
        while ($date->format('m') === $thisMonth) {
            $sunday_arr[] = $date->format('Y-m-d');
            $date->modify('next Sunday');
        }
        $day_of_week = date('N') - 1;
        $current_week_monday_date = date('j') - $day_of_week;
        $week_no = array_search($current_week_monday_date,$sunday_arr) + 1;
          //$z = $sunday_arr[4];
          //echo '<pre>'; print_r($sunday_arr); exit;

        $doctor_id = Auth::user()->userDetailsId;
        $patient_name = Patients::join('booking_request','patient_details.id','booking_request.patient_name')
                          ->where('booking_request.doctor_name',Auth::user()->userDetailsId)
                           ->groupBy('booking_request.patient_name')
                           ->select('booking_request.*','patient_details.first_name','patient_details.id')
                           ->pluck('first_name','patient_details.id');
        
        $week_sdate = date('Y-m-01');   
        $week_date =  date('Y-m-07');
        $week_snew = date('01 F, Y');
        $week_enew = date('07 F, Y');
        $week1 = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereBetween('date_time', [$mondays_arr[0], $sunday_arr[0]])->count();
        
        $week_sdate2 = date('Y-m-08');   
        $week_date2 =  date('Y-m-14'); 
        $week_snew2 = date('08 F, Y');
        $week_enew2 = date('14 F, Y');        
        $week2 = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereBetween('date_time', [$mondays_arr[1], $sunday_arr[1]])->count();
        
        $week_sdate3 = date('Y-m-15');   
        $week_date3 =  date('Y-m-21'); 
        $week_snew3 = date('15 F, Y');
        $week_enew3 = date('21 F, Y');      
        $week3 = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereBetween('date_time', [$mondays_arr[2], $sunday_arr[2]])->count();
        
        $week_sdate4 = date('Y-m-22');
        $week_date4 =  date('Y-m-31');
        $week_snew4 = date('22 F, Y');
        $week_enew4 = date('31 F, Y');
        
        $end = new Carbon('last day of this month');

        $week4 = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereBetween('date_time', [$mondays_arr[3], $sunday_arr[3]])->count();
        $week5 = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereDate('date_time', $mondays_arr[4])->count();
        
         return view('newview.new.homepage.thisyear_week_list',compact('patient_name','week1','week2','week3','week4','week_sdate','week_date','week_sdate2','week_date2','week_sdate3','week_date3','week_sdate4','week_date4','week_snew','week_enew','week_snew2','week_enew2','week_snew3','week_enew3','week_snew4','week_enew4','end','week5'));
      }
      public function homepageshiftsdetails()
      {
        $mytime = Carbon::now();

        $doctor_id = Auth::user()->userDetailsId;
        $patient_name = Patients::join('booking_request','patient_details.id','booking_request.patient_name')
                          ->where('booking_request.doctor_name',Auth::user()->userDetailsId)
                           ->groupBy('booking_request.patient_name')
                           ->select('booking_request.*','patient_details.first_name','patient_details.id')
                           ->pluck('first_name','patient_details.id');

          
         return view('newview.new.homepage.shiftsdetails',compact('patient_name'));
      }

       public function homepagethisyear_day_wise_list()
      {
        $mytime = Carbon::now();

        $doctor_id = Auth::user()->userDetailsId;
        $patient_name = Patients::join('booking_request','patient_details.id','booking_request.patient_name')
                          ->where('booking_request.doctor_name',Auth::user()->userDetailsId)
                           ->groupBy('booking_request.patient_name')
                           ->select('booking_request.*','patient_details.first_name','patient_details.id')
                           ->pluck('first_name','patient_details.id');

         return view('newview.new.homepage.thisyear_day_wise_list',compact('patient_name'));
      }
       public function homepagethisyearlist()
      {
        $mytime = Carbon::now();

        $doctor_id = Auth::user()->userDetailsId;
        $patient_name = Patients::join('booking_request','patient_details.id','booking_request.patient_name')
                          ->where('booking_request.doctor_name',Auth::user()->userDetailsId)
                           ->groupBy('booking_request.patient_name')
                           ->select('booking_request.*','patient_details.first_name','patient_details.id')
                           ->pluck('first_name','patient_details.id');

         return view('newview.new.homepage.thisyearlist',compact('patient_name'));
      }


      public function hospital_profile()
      { 

        $doctor_id = Auth::user()->userDetailsId;
        $doctors = HaspatalRegsiter::where('id',$doctor_id)->first();
         
         $service = DB::table('service_details')->pluck('service','id');
        $specility = DB::table('specilities')->pluck('specility','id');
        

        

         return view('newview.new.hospital.hospital_profile',compact('doctor_id','doctors','service','specility'));
      }

      public function hospital_consult_price()
      {
        $doctor_id = Auth::user()->userDetailsId;
        $doctors = Hospital_register::first();
         
        $service = DB::table('service_details')->pluck('service','id');
        $specility = DB::table('specilities')->where('created_by',Auth::user()->id)->pluck('specility','id');

        $data = DB::table('set_consult_prices')->join('specilities', 'set_consult_prices.specialities', '=', 'specilities.id')->join('doctor_details', 'set_consult_prices.doctor', '=', 'doctor_details.id')->select('set_consult_prices.*','specilities.specility','doctor_details.first_name','doctor_details.last_name')->where('deleted_at','=',null)->get();

         // $setConsultPrices = Set_Consult_Prices::first($id);

        return view('newview.new.hospital.consult_price',compact('doctor_id','doctors','service','specility','data'));
      }


      public function hospital_consult_price_create()
      {
        $id = '';
         $service = DB::table('service_details')->pluck('service','id');

        $specility = DB::table('specilities')->where('created_by',Auth::user()->id)->pluck('specility','id');
        return view('newview.new.hospital.hospital_consult_price_edit',compact('id','specility','service'));
      }

      public function hospital_consult_price_edit($id)
      {
        $data = DB::table('set_consult_prices')->where('id',$id)->first();

         $service = DB::table('service_details')->pluck('service','id');
         $doctors = DB::table('doctor_details')->pluck('first_name','id');
        $specility = DB::table('specilities')->where('created_by',Auth::user()->id)->pluck('specility','id');
        return view('newview.new.hospital.hospital_consult_price_edit',compact('data','id','specility','service','doctors'));
      } 


      public function hospital_register_doctor()
      {
         return view('newview.new.hospital.register_doctor');
      }


      public function hospital_specialities_create()
      {
        $id = '';
        return view('newview.new.hospital.specialiti_create',compact('id'));
      }

      public function hospital_specialities_edit($id)
      {
        $data = DB::table('specilities')->where('id',$id)->first();

        return view('newview.new.hospital.specialiti_create',compact('data','id'));
      }

      public function hospital_specialities_list()
      {
        $data = DB::table('specilities')->where('created_by',Auth::user()->id)->get();

        return view('newview.new.hospital.specialiti_list',compact('data'));
      }

      public function hospital_doctor_list()
      {
        $data = DB::table('doctor_details')->where('created_by',Auth::user()->id)->get();

        return view('newview.new.hospital.doctor_list',compact('data'));
      }



    public function hospitalservicelist(Request $request)
    {
        // echo "<pre>"; print_r($request->specialities_id); exit();
        $states = Doctors::where("speciality",$request->specialities_id)->pluck('first_name','id');
        return response()->json($states);
    }

    public function hasptal_bio()
    {
        $h_id = Auth::user()->userDetailsId;
        //echo $doctor_id; exit;
        $haspatal = HaspatalRegsiter::where('id',$h_id)->first();

        //echo "<pre>";print_r($haspatal);exit;

        return view('newview.new.hospital.mybio',compact('haspatal','h_id'));
    }

    public function h_address()
    {
        $h_id = Auth::user()->userDetailsId;
        $countryList = DB::table('country')->pluck('country','id');
        $stateList = \App\Models\State::orderBy('state','asc')->pluck('state','id');
        $cityList = \App\Models\City::orderBy('city','asc')->pluck('city','id');
        $haspatal = HaspatalRegsiter::where('id',$h_id)->first();

        return view('newview.new.hospital.address',compact('haspatal','h_id','countryList','stateList','cityList'));
    }

    public function h_password()
    {
        $h_id = Auth::user()->userDetailsId;
        $haspatal = HaspatalRegsiter::where('id',$h_id)->first();
     
        return view('newview.new.hospital.passowrd',compact('haspatal','h_id'));
    }

    public function h_contact()
    {
        $h_id = Auth::user()->userDetailsId;
        $haspatal = HaspatalRegsiter::where('id',$h_id)->first();
     
        return view('newview.new.hospital.contact',compact('haspatal','h_id'));
    }
    public function h_profilepic(Request $request)
    {
        $h_id = Auth::user()->userDetailsId;
        $haspatal = HaspatalRegsiter::where('id',$h_id)->first();
        if($request->hasfile('hospital_profile'))
        {
            $image = $request->file('hospital_profile');
            $extension = $image->getClientOriginalExtension(); // getting image extension
            $media_photos =time().'__'.$image->getClientOriginalName();
            $path = public_path('/media/hospital_profile/');
            $image->move($path, $media_photos);
            $haspatal['hospital_profile']= $media_photos;

        } 
        //print_r($doctors); exit();
        return view('newview.new.hospital.profilepic',compact('haspatal','h_id'));
    }
    public function h_profilepic_remove(Request $request)
    {
      
        $booklist =  DB::table('haspatal_regsiter')->where('id',$request->doctor_name)->update(['hospital_profile' => null]);
        $request->session()->flash('message', 'Profile Pic Remove Successfully.');
        $request->session()->flash('message-type', 'success');
        return response()->json($booklist);
       // return redirect(route('tostart')); 
    }

    public function h_license(Request $request)
    {
        $h_id = Auth::user()->userDetailsId;
        $haspatal = HaspatalRegsiter::where('id',$h_id)->first();
        $licenceTyes= DB::table('licence_type')->orderBy('licence_name','asc')->pluck('licence_name','id');
        if($request->hasfile('licence_copy'))
        {
            $image = $request->file('licence_copy');
            $extension = $image->getClientOriginalExtension(); // getting image extension
            $media_photos =time().'__'.$image->getClientOriginalName();
            $path = public_path('/media/licence_copy/');
            $image->move($path, $media_photos);
            $doctors['licence_copy']= $media_photos;

        }
        return view('newview.new.hospital.license',compact('haspatal','h_id','licenceTyes'));
    }

    public function h_current_balance()
      {
        $hospital = Auth::user()->userDetailsId; 
      
        $amount = BookingRequest::where('doctor_name',$hospital)->sum('f_amount');
       
        return view('newview.new.hospital.current_balance',compact('amount'));
      }

    public function h_add_fund()
    {
        $h_id = Auth::user()->userDetailsId;
        $last_amount = hospitalAddFund::where('hospital_id',$h_id)->first();

        return view('newview.new.hospital.h_add_fund',compact('last_amount'));

    }
    public function h_refund()
    {
        $h_id = Auth::user()->userDetailsId;    

        $amount = BookingRequest::where('doctor_name',$h_id)->sum('f_amount');

        $total_fund =  DB::table('hospitalAddFund')->where('hospital_id', $h_id)->first();
        $h_amount = hospitalWithdraw::where('hospital_id',$h_id)->sum('requested_amount');
        $total_amount = $amount + $total_fund->h_amount - $h_amount;
        // $total_amount = $amount + $total_fund->dr_amount;
        //echo $total_amount; exit;
         return view('newview.new.hospital.refund',compact('total_amount'));

    }

    public function h_statement()
    {
      return view('newview.new.hospital.statement');
    }

    public function h_account_statement_view(Request $request)
   {
     if(!empty($request->s_date)){
       $startDate =  Carbon::parse($request->s_date)->startOfDay(); 
       $endDate =  Carbon::now()->endOfDay();
       $h_id = Auth::user()->userDetailsId;
       $dr_amount = hospitalWithdraw::where('hospital_id',$h_id)
                    ->whereBetween('created_at', [$startDate, $endDate]) 
                    ->get();
      }
     return view('newview.new.hospital.statement',compact('dr_amount'));
   } 
    public function h_bankinginfo()
    {
        $h_id = Auth::user()->userDetailsId;
        //echo $h_id; exit;
        $hospital = HaspatalRegsiter::where('id',$h_id)->first();
    
        return view('newview.new.hospital.bankinginfo',compact('h_id','hospital'));
    }
}