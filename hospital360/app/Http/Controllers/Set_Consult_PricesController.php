<?php

namespace App\Http\Controllers;

use App\DataTables\Set_Consult_PricesDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateSet_Consult_PricesRequest;
use App\Http\Requests\UpdateSet_Consult_PricesRequest;
use App\Repositories\Set_Consult_PricesRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Auth;
class Set_Consult_PricesController extends AppBaseController
{
    /** @var  Set_Consult_PricesRepository */
    private $setConsultPricesRepository;

    public function __construct(Set_Consult_PricesRepository $setConsultPricesRepo)
    {
        $this->setConsultPricesRepository = $setConsultPricesRepo;
    }

    /**
     * Display a listing of the Set_Consult_Prices.
     *
     * @param Set_Consult_PricesDataTable $setConsultPricesDataTable
     * @return Response
     */
    public function index(Set_Consult_PricesDataTable $setConsultPricesDataTable)
    {
        return $setConsultPricesDataTable->render('set__consult__prices.index');
    }

    /**
     * Show the form for creating a new Set_Consult_Prices.
     *
     * @return Response
     */
    public function create()
    {
        return view('set__consult__prices.create');
    }

    /**
     * Store a newly created Set_Consult_Prices in storage.
     *
     * @param CreateSet_Consult_PricesRequest $request
     *
     * @return Response
     */
    public function store(CreateSet_Consult_PricesRequest $request)
    {
        $input = $request->all();
        $input['created_by'] = Auth::user()->id;
        $input['updated_by'] = Auth::user()->id;

        $setConsultPrices = $this->setConsultPricesRepository->create($input);

        


        if(Auth::user()->role_id == 1){
            Flash::success('Consult Prices saved successfully.');

            return redirect(route('setConsultPrices.index'));
        }else{
             Flash::success('Consult Prices saved successfully.');
           return redirect(route('hospital_consult_price'));
        }

    }

    /**
     * Display the specified Set_Consult_Prices.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $setConsultPrices = $this->setConsultPricesRepository->find($id);

        if (empty($setConsultPrices)) {
            Flash::error('Set  Consult  Prices not found');

            return redirect(route('setConsultPrices.index'));
        }

        return view('set__consult__prices.show')->with('setConsultPrices', $setConsultPrices);
    }

    /**
     * Show the form for editing the specified Set_Consult_Prices.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $setConsultPrices = $this->setConsultPricesRepository->find($id);

        if (empty($setConsultPrices)) {
            Flash::error('Set  Consult  Prices not found');

            return redirect(route('setConsultPrices.index'));
        }

        return view('set__consult__prices.edit')->with('setConsultPrices', $setConsultPrices);
    }

    /**
     * Update the specified Set_Consult_Prices in storage.
     *
     * @param  int              $id
     * @param UpdateSet_Consult_PricesRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSet_Consult_PricesRequest $request)
    {
        $setConsultPrices = $this->setConsultPricesRepository->find($id);

        if (empty($setConsultPrices)) {
            Flash::error('Set  Consult  Prices not found');

            return redirect(route('setConsultPrices.index'));
        }

        $setConsultPrices = $this->setConsultPricesRepository->update($request->all(), $id);

       

        if(Auth::user()->role_id == 1){
             Flash::success('Consult Prices updated successfully.');

        return redirect(route('setConsultPrices.index')); 
        }else{
             Flash::success('Consult Prices updated successfully.');
        
        return redirect(route('hospital_consult_price'));
        }

    }

    /**
     * Remove the specified Set_Consult_Prices from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $setConsultPrices = $this->setConsultPricesRepository->find($id);

        if (empty($setConsultPrices)) {
            Flash::error('Set  Consult  Prices not found');

            return redirect(route('setConsultPrices.index'));
        }

        $this->setConsultPricesRepository->delete($id);

        


        if(Auth::user()->role_id == 1){
           
        Flash::success('Consult Prices deleted successfully.');
        return redirect(route('setConsultPrices.index'));
        }else{
             Flash::success('Consult Prices deleted successfully.');
            return back();
        }

    }
}
