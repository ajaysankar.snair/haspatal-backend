<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use App\Role;
use App\User;
use App\RoleUser;
use App\Permission;
use App\PermissionRole;
use App\Models\Plan_master;
use Illuminate\Http\Request;
use App\Exports\UsersExport;
use App\Imports\UsersImport;
use Maatwebsite\Excel\Facades\Excel;



class AclController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * CRUD functions for Users.
     */
    public function userIndex()
    {
        $users = User::where('user_type',0)->get();

        return view('user.userIndex', compact('users'));
    }

    public function export() 
    {
        return Excel::download(new UsersExport, 'users.xlsx');
    }

    public function import() 
    {
        Excel::import(new UsersImport, 'users.xlsx');
        
        return redirect('/')->with('success', 'All good!');
    }
    
    public function createUser()
    {
        return view('user.createUser');
    }

    public function storeUser(Request $request)
    {
        /*echo "<pre>";
        print_r($request->all()); exit;*/
        $this->validate($request, ['first_name' => 'required|max:255',
                                   'last_name' => 'required|max:255',
                                   'email' => 'required|email|max:255|unique:users',
                                   'password' => 'required|confirmed|min:8', ]);

        $user = User::create(['first_name' => $request['first_name'],
                                'last_name' => $request['last_name'],
                                'gender' => $request['gender'],
                                'email' => $request['email'],
                                'role_id' => $request['role_id'],
                                'password' => bcrypt($request['password']),
                                'status' => 0,
                                  ]);

        $user->save();

        // Adding Photo
        if ($request->hasFile('photo')) {
            $user->addMedia($request->file('photo'))->usingFileName('staff_'.$user->id.$request->photo->getClientOriginalExtension())->toCollection('staff');
        }
        $user->save();

        $user->attachRole($request->role_id);

        flash()->success('User was successfully created');

        return redirect('user');
    }

    public function editUser($id)
    {
        $user = User::findOrFail($id);

        return view('user.editUser', compact('user'));
    }

    public function updateUser($id, Request $request)
    {
        $user = User::findOrFail($id);

        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->email = $request->email;

        if (! empty($request->password)) {
            $this->validate($request, ['password' => 'required|string|min:8|confirmed']);
            $user->password = bcrypt($request->password);
        }

        //$user->status = $request->status;
       /* echo "<pre>";
        print_r($user); exit;*/

        $user->update();

        /*if ($request->hasFile('photo')) {
          //  $user->clearMediaCollection('staff');
            $user->addMedia($request->file('photo'))->usingFileName('staff_'.$user->id.$request->photo->getClientOriginalExtension())->toCollection('staff');
        }*/
         if($request->hasfile('photo'))
        {
            $image = $request->file('photo');
            $extension = $image->getClientOriginalExtension(); // getting image extension
            $bn_samaj =time().'__'.$image->getClientOriginalName();
            $path = public_path('/media/staff/');
            $image->move($path, $bn_samaj);
            $input['photo']= $bn_samaj;

        }
        $user->save();

        if ($user->roleUser->role->id != $request->role_id) {
            RoleUser::where('user_id', $user->id)->where('role_id', $user->roleUser->role_id)->delete();
            $user->attachRole($request->role_id);
        }

        flash()->success('User details were successfully updated');

        return redirect('user');
    }

    public function deleteUser($id)
    {
        DB::beginTransaction();
        try {
            RoleUser::where('user_id', $id)->delete();
            $user = User::findOrFail($id);
            DB::table('users')->where('id',$id)->delete();
           // $user->clearMediaCollection('staff');
            //$user->status = \constStatus::Archive;
            $user->save();

            DB::commit();
            flash()->success('User was successfully deleted');

            return redirect('user');
        } catch (Exception $e) {
            DB::rollback();
            flash()->error('User was not deleted');

            return redirect('user');
        }
    }

    /**
     * CRUD functions for Roles.
     */
    public function roleIndex()
    {
        $roles = Role::excludeGymie()->get();

        return view('user.roleIndex', compact('roles'));
    }

    public function createRole()
    {
        $permissions = Permission::all();

        return view('user.createRole', compact('permissions'));
    }

    public function storeRole(Request $request)
    {
        //echo "<pre>";print_r($request->all());exit;
        DB::beginTransaction();
        try {
            $role = Role::create(['name' => $request->name,
                                  'display_name' => $request->display_name,
                                  'description' => $request->description,
                                 ]);

            if ($request->has('plan_id')) {
                //$role->attachPermissions($request->permissions);
                $module = Plan_master::where('id',$request->plan_id)->select('module_id')->first();
                $data = json_decode($module->module_id);
                foreach ($data as $value) {
                    $permission_key[] = Permission::where('id',$value)->select('group_key')->first();
                }

                foreach ($permission_key as $value1) {
                    $get_permissions = Permission::where('group_key',$value1->group_key)->select('id')->get();
                }

                foreach ($get_permissions as $value2) {
                    $permissions[] = $value2->id;
                }
                $role->attachPermissions($permissions);

            }
            //echo "<pre>";print_r($permissions);exit;

            DB::commit();
            flash()->success('Role was successfully created');

            return redirect('user/role');
        } catch (Exception $e) {
            DB::rollback();
            flash()->error('Role was not created');

            return redirect('user/role');
        }
    }

    public function editRole($id)
    {
        $role = Role::findOrFail($id);
        $permissions = Permission::all();
        $permission_role = PermissionRole::where('role_id', $id)->get();

        return view('user.editRole', compact('role', 'permissions', 'permission_role'));
    }

    public function updateRole($id, Request $request)
    {
        DB::beginTransaction();
        try {
            //Updating Role
            $role = Role::findOrFail($id);

            $role->update(['name' => $request->name,
                           'display_name' => $request->display_name,
                           'description' => $request->description,
                          ]);

            //Updating permissions for the role
            $DBpermissions = PermissionRole::where('role_id', $id)->select('permission_id')->pluck('permission_id');
            $ClientPermissions = collect($request->permissions);

            $addPermissions = $ClientPermissions->diff($DBpermissions);
            $deletePermissions = $DBpermissions->diff($ClientPermissions);

            if ($addPermissions->count()) {
                $role->attachPermissions($addPermissions);
            }

            if ($deletePermissions->count()) {
                foreach ($deletePermissions as $deletePermission) {
                    PermissionRole::where('role_id', $id)->where('permission_id', $deletePermission)->delete();
                }
            }

            DB::commit();
            flash()->success('Role was successfully updated');

            return redirect('user/role');
        } catch (Exception $e) {
            DB::rollback();
            flash()->error('Role was not updated');

            return redirect('user/role');
        }
    }

    public function deleteRole($id)
    {
        DB::beginTransaction();
        try {
            PermissionRole::where('role_id', $id)->delete();
            Role::where('id', $id)->delete();

            DB::commit();
            flash()->success('Role was successfully deleted');

            return redirect('user/role');
        } catch (Exception $e) {
            DB::rollback();
            flash()->error('Role was not deleted');

            return redirect('user/role');
        }
    }

    /**
     * CRUD functions for Permissions.
     */
    public function permissionIndex()
    {
        $permissions = Permission::all();

        return view('user.permissionIndex', compact('permissions'));
    }

    public function createPermission()
    {
        return view('user.createPermission');
    }

    public function storePermission(Request $request)
    {
        Permission::create(['name' => $request->name,
                            'display_name' => $request->display_name,
                            'description' => $request->description,
                            'group_key' => $request->group_key,
                           ]);

        flash()->success('Permission was successfully created');

        return redirect('user/permission');
    }

    public function editPermission($id)
    {
        $permission = Permission::findOrFail($id);

        return view('user.editPermission', compact('permission'));
    }

    public function updatePermission($id, Request $request)
    {
        $permission = Permission::findOrFail($id);

        $permission->update(['name' => $request->name,
                            'display_name' => $request->display_name,
                            'description' => $request->description,
                            'group_key' => $request->group_key,
                            ]);

        flash()->success('Permission was successfully updated');

        return redirect('user/permission');
    }

    public function deletePermission($id)
    {
        Permission::findOrFail($id)->delete();

        flash()->success('Permission was successfully deleted');

        return redirect('user/permission');
    }
}
