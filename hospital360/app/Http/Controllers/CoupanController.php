<?php

namespace App\Http\Controllers;

use App\DataTables\CoupanDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateCoupanRequest;
use App\Http\Requests\UpdateCoupanRequest;
use App\Repositories\CoupanRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class CoupanController extends AppBaseController
{
    /** @var  CoupanRepository */
    private $coupanRepository;

    public function __construct(CoupanRepository $coupanRepo)
    {
        $this->coupanRepository = $coupanRepo;
    }

    /**
     * Display a listing of the Coupan.
     *
     * @param CoupanDataTable $coupanDataTable
     * @return Response
     */
    public function index(CoupanDataTable $coupanDataTable)
    {
        return $coupanDataTable->render('coupans.index');
    }

    /**
     * Show the form for creating a new Coupan.
     *
     * @return Response
     */
    public function create()
    {
        return view('coupans.create');
    }

    /**
     * Store a newly created Coupan in storage.
     *
     * @param CreateCoupanRequest $request
     *
     * @return Response
     */
    public function store(CreateCoupanRequest $request)
    {
        $input = $request->all();

        $coupan = $this->coupanRepository->create($input);

        Flash::success('Coupan saved successfully.');

        return redirect(route('coupans.index'));
    }

    /**
     * Display the specified Coupan.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $coupan = $this->coupanRepository->find($id);

        if (empty($coupan)) {
            Flash::error('Coupan not found');

            return redirect(route('coupans.index'));
        }

        return view('coupans.show')->with('coupan', $coupan);
    }

    /**
     * Show the form for editing the specified Coupan.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $coupan = $this->coupanRepository->find($id);

        if (empty($coupan)) {
            Flash::error('Coupan not found');

            return redirect(route('coupans.index'));
        }

        return view('coupans.edit')->with('coupan', $coupan);
    }

    /**
     * Update the specified Coupan in storage.
     *
     * @param  int              $id
     * @param UpdateCoupanRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCoupanRequest $request)
    {
        $coupan = $this->coupanRepository->find($id);

        if (empty($coupan)) {
            Flash::error('Coupan not found');

            return redirect(route('coupans.index'));
        }

        $coupan = $this->coupanRepository->update($request->all(), $id);

        Flash::success('Coupan updated successfully.');

        return redirect(route('coupans.index'));
    }

    /**
     * Remove the specified Coupan from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $coupan = $this->coupanRepository->find($id);

        if (empty($coupan)) {
            Flash::error('Coupan not found');

            return redirect(route('coupans.index'));
        }

        $this->coupanRepository->delete($id);

        Flash::success('Coupan deleted successfully.');

        return redirect(route('coupans.index'));
    }
}
