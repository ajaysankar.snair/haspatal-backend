<?php

namespace App\Http\Controllers;

use App\DataTables\Order_queryDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateOrder_queryRequest;
use App\Http\Requests\UpdateOrder_queryRequest;
use App\Repositories\Order_queryRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class Order_queryController extends AppBaseController
{
    /** @var  Order_queryRepository */
    private $orderQueryRepository;

    public function __construct(Order_queryRepository $orderQueryRepo)
    {
        $this->orderQueryRepository = $orderQueryRepo;
    }

    /**
     * Display a listing of the Order_query.
     *
     * @param Order_queryDataTable $orderQueryDataTable
     * @return Response
     */
    public function index(Order_queryDataTable $orderQueryDataTable)
    {
        return $orderQueryDataTable->render('order_queries.index');
    }

    /**
     * Show the form for creating a new Order_query.
     *
     * @return Response
     */
    public function create()
    {
        return view('order_queries.create');
    }

    /**
     * Store a newly created Order_query in storage.
     *
     * @param CreateOrder_queryRequest $request
     *
     * @return Response
     */
    public function store(CreateOrder_queryRequest $request)
    {
        $input = $request->all();

        $orderQuery = $this->orderQueryRepository->create($input);

        Flash::success('Order Query saved successfully.');

        return redirect(route('orderQueries.index'));
    }

    /**
     * Display the specified Order_query.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $orderQuery = $this->orderQueryRepository->find($id);

        if (empty($orderQuery)) {
            Flash::error('Order Query not found');

            return redirect(route('orderQueries.index'));
        }

        return view('order_queries.show')->with('orderQuery', $orderQuery);
    }

    /**
     * Show the form for editing the specified Order_query.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $orderQuery = $this->orderQueryRepository->find($id);

        if (empty($orderQuery)) {
            Flash::error('Order Query not found');

            return redirect(route('orderQueries.index'));
        }

        return view('order_queries.edit')->with('orderQuery', $orderQuery);
    }

    /**
     * Update the specified Order_query in storage.
     *
     * @param  int              $id
     * @param UpdateOrder_queryRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateOrder_queryRequest $request)
    {
        $orderQuery = $this->orderQueryRepository->find($id);

        if (empty($orderQuery)) {
            Flash::error('Order Query not found');

            return redirect(route('orderQueries.index'));
        }

        $orderQuery = $this->orderQueryRepository->update($request->all(), $id);

        Flash::success('Order Query updated successfully.');

        return redirect(route('orderQueries.index'));
    }

    /**
     * Remove the specified Order_query from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $orderQuery = $this->orderQueryRepository->find($id);

        if (empty($orderQuery)) {
            Flash::error('Order Query not found');

            return redirect(route('orderQueries.index'));
        }

        $this->orderQueryRepository->delete($id);

        Flash::success('Order Query deleted successfully.');

        return redirect(route('orderQueries.index'));
    }
}
