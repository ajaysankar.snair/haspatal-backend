<?php
namespace App\Http\Controllers;
use Carbon\Carbon;
use App\Http\Requests\API\CreateStateAPIRequest;
use App\Http\Requests\API\UpdateStateAPIRequest;
use App\Models\State;
use App\Repositories\StateRepository;
use App\Repositories\Business_registerRepository;
use Illuminate\Http\Request;
use App\Models\Service_details;
use App\Models\Your_coverage_360;
use App\Models\City;
use App\coupon;
use App\User;
use App\Models\Doctors;
use App\couponrate;
use App\Models\Order_360;
use App\Models\Business_register;
use App\Http\Controllers\AppBaseController;
use Response;
use DB;

class adminsController extends Controller
{
    //
    public function allbus()
    {
        $data = Business_register::select('*')->orderBy('id','desc')->get();

        foreach ($data as $Business_register) {
            $user_id = $Business_register->user_id;
            // $district = $Business_register->district;
            // if( $district=="Gurgaon"){
            //     $result = Business_register::where(
            //         'user_id',
            //         $user_id
            //     )->update(['district' => '11']);
            // }

            $users = DB::table('users')
                ->where('id', $user_id)
                ->orderBy('id', 'desc')
                ->first();
            if ($users) {
                $role_id = $users->role_id;
                $role = DB::table('couponrates')
                    ->where('category_id', $role_id)
                    ->orderBy('id', 'desc')
                    ->first();
                if ($role) {
                    $Business_register['type'] = $role->name;
                    // create regid
                    $firstLetter = $role->RegCode;
                    // if ($Business_register->state_id) {
                    //     $state_id = $Business_register->state_id;
                    //     if (strlen($state_id) == 1) {
                    //         $SecondLetter = "0" . $state_id;
                    //     } else {
                    //         $SecondLetter = $state_id;
                    //     }
                    // }
                    // if ($Business_register->district) {
                    //     $thirdletter = $Business_register->district[0];
                    // }
                    // $lastletter = substr($Business_register->pincode, -1);
                    $businssID=$Business_register->id;
                    // if (strlen($businssID) == 1) {
                    //     $BusCode = "00" . $businssID;
                    // } elseif(strlen($businssID) == 2) {
                    //     $BusCode = "0" . $businssID;
                    // }else{
                    //     $BusCode =  $businssID; 
                    // }


                    //////////////

                    // $regID =
                    //     $firstLetter ."91".
                    //     $SecondLetter .
                    //     $thirdletter .
                    //     $lastletter.$BusCode;
                    // dd( $regID);
                     $regID =
                       "91". $firstLetter.
                        "0000".
                        $businssID;
                    $result = Business_register::where(
                        'user_id',
                        $user_id
                    )->update(['business_registerID' => $regID]);
                    $Business_register['regID'] = $regID;
                }
            }
        }
        // dd($data);
        return view('indexs', compact('data'));
    }
    public function alldoctor()
    {
        $data = Doctors::select('*')->get();
        // dd( $data[0]);
        foreach ($data as $doctor) {
            // $id=$doctor->d_uni_id;
            // if( $id){
            //     dd($id);
            // }
           
        }

        
        return view('doctorlist', compact('data'));
    }
    public function allorders()
    {
        // $data = Order_360::select('*')->get();
        // foreach ($data as $orders) {
        //     $user_id = $orders->user_id;
          
        //     if( $user_id){

        //     $users = DB::table('users')
        //     ->where('id', $user_id)
        //     ->orderBy('id', 'desc')
        //     ->first();
        //     if($users ){
        //     $orders['first_name']= $users->first_name;
        //     $orders['mobile']= $users->mobile;
        // }
        // }
        // }
        $order_view = Order_360::leftJoin('doctor_details', 'Order_360.doctor_id', '=', 'doctor_details.id')
        ->join('patient_details', 'Order_360.patient_id', '=', 'patient_details.id')
        // ->where('Order_360.user_id',$request->user_id)
       
        ->select('Order_360.*','doctor_details.first_name','doctor_details.last_name','patient_details.first_name as patient_name','patient_details.mobile as mobile')
        ->orderBy('updated_at','DESC')->get();

        $path =  env('APP_URL') . 'media/prescription_image/';
        $data= $order_view;
    
         foreach ($data as $orders) {
            $user_id = $orders->user_id;
          
            if( $user_id){
              $Business_register=  Business_register::select('*')    ->where('user_id', $user_id)
                ->orderBy('id', 'desc')
                ->first();
                if($Business_register){
                    $orders['b_name'] =$Business_register->b_name;
                    // dd($Business_register->b_name);
                }
            $users = DB::table('users')
            ->where('id', $user_id)
            ->orderBy('id', 'desc')
            ->first();
            if($users ){
            $orders['first_name']= $users->first_name;
            $orders['mobile']= $users->mobile;
        }
        }
        }
        return view('orders', compact('data'));

    }
    public function todays()
    {
        $mytime = Carbon::now();
        $startDate =  Carbon::parse($mytime->toDateTimeString())->startOfDay(); 
        $endDate =  Carbon::now()->endOfDay();
    
        $today_order_list = Order_360::leftJoin('doctor_details', 'Order_360.doctor_id', '=', 'doctor_details.id')
                                   ->join('patient_details', 'Order_360.patient_id', '=', 'patient_details.id')
                                   ->whereBetween('Order_360.created_at', [$startDate, $endDate])
                                    //  ->where('Order_360.user_id',$user_id)
                                     ->where('Order_360.order_status','=','0')
                                      ->select('Order_360.*','doctor_details.first_name','doctor_details.last_name','patient_details.first_name as patient_name','patient_details.mobile as mobile')
                                     ->get();
                                     $path =  env('APP_URL') . '/media/prescription_image/';
        // $data = Order_360::select('*')->where('created_at', $mytime->toDateTimeString())->get();
        // dd($today_order_list) ;
        $data=$today_order_list;
        foreach ($data as $orders) {
            $user_id = $orders->user_id;
          
            if( $user_id){
                $Business_register=  Business_register::select('*')    ->where('user_id', $user_id)
                ->orderBy('id', 'desc')
                ->first();
                if($Business_register){
                    $orders['b_name'] =$Business_register->b_name;
                    // dd($Business_register->b_name);
                }
            $users = DB::table('users')
            ->where('id', $user_id)
            ->orderBy('id', 'desc')
            ->first();
            if($users ){
            $orders['first_name']= $users->first_name;
            $orders['mobile']= $users->mobile;
        }
        }
        }

        return view('todays', compact('data'));

    }
    public function pending()
    {
        $order_view = Order_360::leftJoin('doctor_details', 'Order_360.doctor_id', '=', 'doctor_details.id')
        ->join('patient_details', 'Order_360.patient_id', '=', 'patient_details.id')
        // ->where('Order_360.user_id',$request->user_id)
        ->where('Order_360.order_status','=','0')
        ->select('Order_360.*','doctor_details.first_name','doctor_details.last_name','patient_details.first_name as patient_name','patient_details.mobile as mobile')
        ->orderBy('updated_at','DESC')->get();

        $path =  env('APP_URL') . 'media/prescription_image/';
        // $data = Order_360::select('*')->where('created_at', $mytime->toDateTimeString())->get();
        // dd($today_order_list) ;
        $data=$order_view;
        foreach ($data as $orders) {
            $user_id = $orders->user_id;
          
            if( $user_id){
                $Business_register=  Business_register::select('*')    ->where('user_id', $user_id)
                ->orderBy('id', 'desc')
                ->first();
                if($Business_register){
                    $orders['b_name'] =$Business_register->b_name;
                    // dd($Business_register->b_name);
                }

            $users = DB::table('users')
            ->where('id', $user_id)
            ->orderBy('id', 'desc')
            ->first();
            if($users ){
            $orders['first_name']= $users->first_name;
            $orders['mobile']= $users->mobile;
        }
        }
        }

        return view('pending', compact('data'));

    }
    public function complete()
    {
        $order_view = Order_360::leftJoin('doctor_details', 'Order_360.doctor_id', '=', 'doctor_details.id')
        ->join('patient_details', 'Order_360.patient_id', '=', 'patient_details.id')
        // ->where('Order_360.user_id',$request->user_id)
        ->where('Order_360.order_status','=','1')
        ->select('Order_360.*','doctor_details.first_name','doctor_details.last_name','patient_details.first_name as patient_name','patient_details.mobile as mobile')
        ->orderBy('updated_at','DESC')->get();

        $path =  env('APP_URL') . 'media/prescription_image/';
        // $data = Order_360::select('*')->where('created_at', $mytime->toDateTimeString())->get();
        // dd($today_order_list) ;
        $data=$order_view;
        foreach ($data as $orders) {
            $user_id = $orders->user_id;
          
            if( $user_id){
                $Business_register=  Business_register::select('*')    ->where('user_id', $user_id)
                ->orderBy('id', 'desc')
                ->first();
                if($Business_register){
                    $orders['b_name'] =$Business_register->b_name;
                    // dd($Business_register->b_name);
                }

            $users = DB::table('users')
            ->where('id', $user_id)
            ->orderBy('id', 'desc')
            ->first();
            if($users ){
            $orders['first_name']= $users->first_name;
            $orders['mobile']= $users->mobile;
        }
        }
        }

        return view('complete', compact('data'));

    }
    public function doctorsingle(Request $request)
    {
        $data =Doctors::select('*')
        ->where('id', $request->input('product_id'))
        ->orderBy('id', 'desc')
        ->first();
        return view('singledoctor', compact('data'));
    }
    public function doctorapprove(Request $request)
    {
        $Doctors = Doctors::where(
            'id',
            $request->input('user_id')
        )->update(['status' => 0]);
        $data = Doctors::select('*')->get();
        // dd( $data[0]);
        foreach ($data as $doctor) {
            // $id=$doctor->d_uni_id;
            // if( $id){
            //     dd($id);
            // }
           
        }

        
        return view('doctorlist', compact('data'));
    }
    public function doctorhold(Request $request)
    {
        $Doctors = Doctors::where(
            'id',
            $request->input('user_id')
        )->update(['status' => '1']);
        $data = Doctors::select('*')->get();
        // dd( $data[0]);
        foreach ($data as $doctor) {
            // $id=$doctor->d_uni_id;
            // if( $id){
            //     dd($id);
            // }
           
        }

        
        return view('doctorlist', compact('data'));
    }
    public function settings(Request $request)
    {
        // $data=$request->input('product_id');
        $data = DB::table('business_register')
            ->where('id', $request->input('product_id'))
            ->orderBy('user_id', 'desc')
            ->first();
        $user_id = $data->user_id;

        $users = DB::table('users')
            ->where('id', $user_id)
            ->orderBy('id', 'desc')
            ->first();

        // $user =  User::select('*')->where(['id' => $request->input('product_id')]);
        $role_id = $users->role_id;
        $data->email= $users->email;
        $data->mobile= $users->mobile;
        $role = DB::table('couponrates')
            ->where('category_id', $role_id)
            ->orderBy('id', 'desc')
            ->first();
        // dd($role);
        $type = $role->name;
        // $data = Business_register::where('user_id',$request->user_id)
        //                         ->join('country', 'business_register.country_id', '=', 'country.id')
        //                         ->join('state', 'business_register.state_id', '=', 'state.id')
        //                         ->join('city', 'business_register.city_id', '=', 'city.id')
        //                         ->select('business_register.*','country.country','state.state','city.city')
        //                         ->first();
        if($data->district){
          $district = DB::table('district')
            ->where('id', $data->district)
            ->orderBy('id', 'desc')
            ->first();
            if($district){
            $data->district_name = $district->district_name;
        }else{
            $data->district_name="Processing";
        }
    }else{
        $data->district_name="Not Selected"  ;
    }

//////
if($data->city_id){
    $city = DB::table('city')
      ->where('id', $data->city_id)
      ->orderBy('id', 'desc')
      ->first();
      if($city){
      $data->cityname = $city->city;
  }else{
      $data->cityname="Processing";
  }
}else{
  $data->cityname="Not Selected"  ;
}

////


    if($data->state_id){
        $state = DB::table('state')
            ->where('id', $data->state_id)
            ->orderBy('id', 'desc')
            ->first();
       
        $data->statename = $state->state;
    }else{
        $data->statename="Not Selected"  ;
    }
        $data->type = $type;
        // dd($data);
        $path = [
            "b_lic" => env('APP_URL') . 'media/b_lic/' . $data->b_lic,
            "b_card_pic" =>
                env('APP_URL') . 'media/b_card_pic/' . $data->b_card_pic,
            "shop_pic" =>
                env('APP_URL') . 'media/shop_pic/' . $data->shop_pic,
        ];
        //  dd($path);
        if ($data) {
            return view('views', compact('data', 'path'));
        }
    }
    public function aprroveregs(Request $request)
    {
        // $data=$request->input('product_id');
        $data = Business_register::select('*')
            ->where(['admin_status' => '1'])
            ->get();
        foreach ($data as $Business_register) {
            $user_id = $Business_register->user_id;

            $users = DB::table('users')
                ->where('id', $user_id)
                ->orderBy('id', 'desc')
                ->first();
            if ($users) {
                $role_id = $users->role_id;
                $role = DB::table('couponrates')
                    ->where('category_id', $role_id)
                    ->orderBy('id', 'desc')
                    ->first();

                $Business_register['type'] = $role->name;
            }
        }
        // dd($data);
        if ($data) {
            return view('approvereg', compact('data'));
        }
    }
    public function rjcts(Request $request)
    {
        // $data=$request->input('product_id');
        $data = Business_register::select('*')
            ->where(['admin_status' => '2'])
            ->get();
        foreach ($data as $Business_register) {
            $user_id = $Business_register->user_id;

            $users = DB::table('users')
                ->where('id', $user_id)
                ->orderBy('id', 'desc')
                ->first();
            if ($users) {
                $role_id = $users->role_id;
                $role = DB::table('couponrates')
                    ->where('category_id', $role_id)
                    ->orderBy('id', 'desc')
                    ->first();

                $Business_register['type'] = $role->name;
            }
        }
        // dd($data);
        if ($data) {
            return view('rjct', compact('data'));
        }
    }
    public function approve(Request $request)
    {
        // $data=$request->input('product_id');
        // $data =  DB::table('business_register')->where('user_id', $request->input('product_id'))->orderBy('user_id', 'desc')->first();
        // dd($data);
        $data = Business_register::where(
            'user_id',
            $request->input('user_id')
        )->update(['admin_status' => '1']);
        // dd($request->input('product_id'));
        $users = DB::table('users')
            ->where('id', $request->input('user_id'))
            ->orderBy('id', 'desc')
            ->first();
        
        // $user =  User::select('*')->where(['id' => $request->input('product_id')]);
        $role_id = $users->role_id;
        $role = DB::table('couponrates')
            ->where('category_id', $role_id)
            ->orderBy('id', 'desc')
            ->first();
            if($users){
                $role_name=$role->name;
                $business_register =  Business_register::where('user_id', $request->input('user_id'))
            ->orderBy('id', 'desc')
            ->first();
            $b_name=$business_register->b_name;
            $b_id=$business_register->business_registerID;
            $adress=$business_register->address;




            $emailResponsethird = $this->send_mail1($request->input('user_id'),$b_name,$b_id,$adress,$role_name);
            }
        // dd($role);
        $CouponUser = coupon::where(
            'user_id',
            '=',
            $request->input('user_id')
        )->first();
        if ($CouponUser) {
            return redirect('/aprroveregs');
        } else {
            $coupon = new coupon();
            $coupon->user_id = $request->input('user_id');
            $coupon->category_d = $role->category_id;
            $coupon->Category_name = $role->name;
            $coupon->Rate = $role->rate;
            $coupon->promo_code = $role->promo_code;
            $coupon->save();
        }

        if ($data) {
            // return view('aprrovereg', compact('data'));
            return redirect('/aprroveregs');
        }
    }
    public function reject(Request $request)
    {
        $data = Business_register::where(
            'user_id',
            $request->input('product_id')
        )->update(['admin_status' => '2']);
        if ($data) {
            return redirect('/rjcts');
        }
    }
    public function send_mail1($id,$b_name,$b_id,$adress,$role_name)
    
    {
        $user = User::findOrFail($id);
        $subject =
        $b_name.
         "-" . $role_name. " - Registration Approved ";
        // dd( $user);
        $mailmessage= '<!DOCTYPE html>
        <html lang="en">
            <head>
                <title>Dr email template</title>
                <meta charset="utf-8">
                <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
                <!-- CSS only -->
                <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
                <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900,900i&display=swap" rel="stylesheet">
                <script src="https://kit.fontawesome.com/fa5e54101c.js" crossorigin="anonymous"></script>
                <!-- JavaScript Bundle with Popper -->
                <style>
                    #page-wrap{
                        max-width: 800px;
                        margin: 0 auto;
                        font-family: "Roboto";
                       
                    }
                    p,h5,h6{
                        color: #000;
                    }
                </style>
                <!-- Theme Style -->
                <link rel="stylesheet" href="css/print.css">
                <!-- <link rel="stylesheet" href="/assets/css/bootstrap.min.css"> -->
            </head>
            <body>
                <div id="page-wrap">
                    <div class="top">
                    
                        <h6 style="margin-bottom: 25px; font-weight:400;  font-size: 16px;">Hi , '.$b_name.'  !.</h6>
                        <p>We find pleasure to inform you that your registration has been approved. Now you are entitled to use Haspatal Program and Haspatal 360 Application to provide your services for Haspatal Patients & your Local Customers at zero commission.

                        </p>
                        <p style="margin-bottom: 25px;">
                            <b>Your quick access code is '.$b_id.'.</b><br>
                            Kindly display this quick access code at your Pharmacy for the convenience of your customers.

                        </p>
                    
                        <p style="margin-bottom: 0;">Your registered details are as follows:</p>
                        <p style="margin-bottom: 0;">'.$b_name.'</p>
                        <p style="margin-bottom: 0;">'.$adress.'</p>
                        <p style="margin-bottom: 0;">'.$user->mobile.'</p>
                        <p style="margin-bottom: 25px;">With Regards,</p>
            
                        <h5 style="font-size: 16px; margin-bottom: 0;">TEAM HASPATAL MD</h5>
                        <p style="margin-bottom: 40px;"><b>PHONE:</b>+91-124-405 56 56</p>
            
                        <h5 style="font-size: 16px;">IMPORTANT:-</h5>
                        <p><b>We have uploaded support videos for our Doctors at following 
                        <a href="https://www.youtube.com/playlist?list=PLb9kI_cIEaQMq43XlL7Gd0ch2Ve_KQfyJ">link</a>
                        
                            You may visit here 24*7 and find useful information</b>
                        </p>
                    </div>
                </div>
            </body>
        </html>
        
'                 ;
        $data['personalizations'] = array("0" => array('to' => array('0' => array('email' => $user->email))));

        $data['from'] = array('email' => 'system@haspatal.com');

        $data['subject'] = $subject;
        $data['content'] = array("0" =>array('type' =>' text/html','value' => $mailmessage ));

        //echo json_encode($data);exit();

        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://api.sendgrid.com/v3/mail/send",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => json_encode($data),
          CURLOPT_HTTPHEADER => array(
            "authorization: Bearer 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",
            "cache-control: no-cache",
            "content-type: application/json",
            "postman-token: b8c0781f-af48-90cc-1c5b-76f80e0b4be0"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            return "Send OTP in your emailId";
        }
    }
    public function hold(Request $request)
    {
        $data = Business_register::where(
            'user_id',
            $request->input('product_id')
        )->update(['admin_status' => '3']);
        if ($data) {
            return redirect('/allreg');
        }
    }
    public function deleteregister(Request $request)
    {
        $data=User::where('id', $request->input('product_id'))->delete();
        if ($data) {
            $datas=Business_register::where('user_id', $request->input('product_id'))->delete();
            return redirect('/allreg');
        }
    }
    public function adminregister(Request $request)
    {
        // $data=$request->input('product_id');
        // $data =   $request->input('email');
        // dd($data);
        $user = new User();
        $data = $request->input();
        $otp = mt_rand(1000, 9999);
        $user->email = $data['email'];
        $user->username = $data['fname'];
        $user->password = $data['password'];
        $user->mobile = $data['phone'];
        $user->role_id = $otp;
        $user->save();

        $mailmessage =
            $data['fname'] .
            "please verify your email id with the otp :" .
            $otp;
        $year = date('Y');
        $data['personalizations'] = [
            "0" => [
                'to' => ['0' => ['email' => $data['email']]],
                "dynamic_template_data" => [
                    "name" => $mailmessage,
                    "year" => "2021",
                ],
            ],
        ];

        $data['from'] = ['email' => 'system@haspatal.com'];

        //$data['subject'] = "Registration successful";
        $data['template_id'] = "d-c19ea64f41ea4c48ae28b3625202ed9e";
        //$data['content'] = array("0" =>array('type' =>' text/html','value' => "<DOCTYPE html><html lang='en-US'><head><meta charset='utf-8'></head><body><h2>Hi ".$user->first_name.", we’re glad you’re here! Following are your account details: <br> </h3> <h3>Email: </h3><p>".$user->email."</p> </body> </html>" ));

        //echo json_encode($data);exit();

        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => "https://api.sendgrid.com/v3/mail/send",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($data),
            CURLOPT_HTTPHEADER => [
                "authorization: Bearer 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",
                "cache-control: no-cache",
                "content-type: application/json",
                "postman-token: b8c0781f-af48-90cc-1c5b-76f80e0b4be0",
            ],
        ]);

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            // return response()->json(["Your_coverage_360" => $response,"massage"=>"updated  successfully"]);
        }
        if ($user) {
            return view('otp', compact('user'));
        }
    }
    public function validateRegistration(Request $request)
    {
        $data = $request->input();
        $users = User::where('email', '=', $data['email'])
            ->where(
                'mobile',
                '=',
                $data['mo_prefix'] . '-' . $data['mobile_no']
            )
            ->first();
        if ($users) {
            return response()->json(["status" => 1]);
        } else {
            return response()->json(["status" => 0]);
        }
    }
    public function loginregister(Request $request)
    {
        // dd("dd");
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
        ]);
        $data = $request->input();
        $users = User::where('email', '=', $data['email'])
            ->where('password', '=', $data['password'])
            ->where('role_id', '=', '21')
            ->first();
        // if (\Auth::attempt([
        //     'email' => $request->email,
        //     'password' => $request->password])
        // ){
        //     return redirect('/allreg');
        if ($users) {
            return redirect('/allreg');
        }
        // }
        return redirect('/loginadmin')->with(
            'error',
            'Invalid Email address or Password'
        );
    }
    public function otpVerification(Request $request)
    {
        $data = $request->input();
        // dd( $data);
        $otp = $data['otp'];
        $user = User::where('id', '=', $data['user_id'])->first();
        //   dd( $user);
        if ($user) {
            $otpd = $user->role_id;
            if ($otpd == $otp) {
                $data = User::where('id', $data['user_id'])->update([
                    'role_id' => '21',
                ]);
                return view('loginadmin', compact('user'));
            }
        }
    }
}
