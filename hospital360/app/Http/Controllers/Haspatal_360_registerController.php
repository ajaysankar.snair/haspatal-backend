<?php

namespace App\Http\Controllers;

use App\DataTables\Haspatal_360_registerDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateHaspatal_360_registerRequest;
use App\Http\Requests\UpdateHaspatal_360_registerRequest;
use App\Repositories\Haspatal_360_registerRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Illuminate\Http\Request;
use App\Repositories\Business_registerRepository;
use Validator;
use DB;
use App\User;

class Haspatal_360_registerController extends AppBaseController
{
    /** @var  Haspatal_360_registerRepository */
    private $haspatal360RegisterRepository;

    public function __construct(Haspatal_360_registerRepository $haspatal360RegisterRepo,Business_registerRepository $businessRegisterRepo)
    {
        $this->haspatal360RegisterRepository = $haspatal360RegisterRepo;
        $this->businessRegisterRepository = $businessRegisterRepo;
    }

    public function h360_active($id)
    {

      $booklist =  DB::table('users')->where('userDetailsId',$id)->where('user_type',4)->update(['status' => 1]);

      $booklist =  DB::table('haspatal_360_register')->where('id',$id)->update(['status' => 1]);

      Flash::success('Doctor updated successfully.');

      return back();

    }
    public function h360_deactive($id)
    {
      $booklist =  DB::table('users')->where('userDetailsId',$id)->where('user_type',4)->update(['status' => 0]);
      
      $booklist =  DB::table('haspatal_360_register')->where('id',$id)->update(['status' => 0]);

      Flash::success('Doctor updated successfully.');
      return back();

    }

    /**
     * Display a listing of the Haspatal_360_register.
     *
     * @param Haspatal_360_registerDataTable $haspatal360RegisterDataTable
     * @return Response
     */
    public function index(Haspatal_360_registerDataTable $haspatal360RegisterDataTable)
    {
        return $haspatal360RegisterDataTable->render('haspatal_360_registers.index');
    }

    /**
     * Show the form for creating a new Haspatal_360_register.
     *
     * @return Response
     */
    public function create()
    {
        return view('haspatal_360_registers.create');
    }

    /**
     * Store a newly created Haspatal_360_register in storage.
     *
     * @param CreateHaspatal_360_registerRequest $request
     *
     * @return Response
     */
    public function store(CreateHaspatal_360_registerRequest $request)
    {
        $input = $request->all();

        $haspatal360Register = $this->haspatal360RegisterRepository->create($input);

        Flash::success('Haspatal 360 Register saved successfully.');

        return redirect(route('haspatal360Registers.index'));
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|unique:haspatal_360_register,email',
            'mobile_no' => 'required|unique:haspatal_360_register,mobile_no',
        ]);

        if ($validator->fails()) {
          return redirect(route('haspatal.register360.create'))
                   ->withErrors($validator)
                   ->withInput();
        }


        $h_data =  DB::table('haspatal_360_register')->orderBy('id', 'desc')->first();
        if($h_data->v_uni_id != ''){
            $bookdata = explode('-', $h_data->v_uni_id); 
            $dd = $bookdata['2'] + 1;   
            $a = str_pad($dd, 6, '0', STR_PAD_LEFT); 
            $input['v_uni_id'] = $bookdata['0'].'-'.$bookdata['1'].'-'.$a;
        }else{
            $seq = '1';
            $a = str_pad($seq, 6, '0', STR_PAD_LEFT); 
            $ssn = '91-V-'.$a;
            $input['v_uni_id'] = $ssn;
        }

        $input['name_prefix'] = $request->name_prefix;
        $input['full_name'] = $request->full_name;
        $input['email']     = $request->email;
        $input['mo_prefix'] = $request->mo_prefix;
        $input['mobile_no'] = $request->mobile_no;

        $haspatal360Register = $this->haspatal360RegisterRepository->create($input);

        $user = User::create(['first_name' => $request['full_name'],
                              'email' => $request['email'],
                              'mobile' => $request['mo_prefix'].'-'.$request['mobile_no'],
                              'role_id' => $request['role_id'],
                              'user_type' => 4,
                              'userDetailsId'=>$haspatal360Register->id,
                              'password' => bcrypt($request['password']),
                            ]);
        $user->save();
        $user->attachRole($request['role_id']);

        if($request->hasfile('b_lic'))
        {
            $image1 = $request->file('b_lic');
            $extension1 = $image1->getClientOriginalExtension(); // getting image extension
            $filename1 =time().'__'.$image1->getClientOriginalName();
            $path1 = public_path('/media/b_lic/');
            $image1->move($path1, $filename1);
            $input1['b_lic'] = $filename1;
        }
        if($request->hasfile('shop_pic'))
        {
            $image1 = $request->file('shop_pic');
            $extension1 = $image1->getClientOriginalExtension(); // getting image extension
            $filename1 =time().'__'.$image1->getClientOriginalName();
            $path1 = public_path('/media/shop_pic/');
            $image1->move($path1, $filename1);
            $input1['shop_pic'] = $filename1;
        }
        if($request->hasfile('b_card_pic'))
        {
            $image1 = $request->file('b_card_pic');
            $extension1 = $image1->getClientOriginalExtension(); // getting image extension
            $filename1 =time().'__'.$image1->getClientOriginalName();
            $path1 = public_path('/media/b_card_pic/');
            $image1->move($path1, $filename1);
            $input1['b_card_pic'] = $filename1;
        }

        $input1['user_id'] = $user->id;
        $input1['b_id'] = $request['role_id'];
        $input1['b_name'] = $request->b_name;
        $input1['conatact_person'] = $request->conatact_person;
        $input1['mobile1'] = $request->mobile1;
        $input1['mobile2'] = $request->mobile2;
        $input1['email'] = $request->email;
        $input1['open_time'] = $request->open_time;
        $input1['close_time'] = $request->close_time;
        $input1['open_24'] = isset($request->open_24) ? 1 : 2;
        $input1['weekly_off'] = $request->weekly_off;
        $input1['all_days_open'] = isset($request->all_days_open) ? 1 : 2;
        $input1['address'] = $request->address;
        $input1['country_id'] = $request->country_id;
        $input1['state_id'] = $request->state_id;
        $input1['city_id'] = $request->city_id;
        $input1['pincode'] = $request->pincode;
        $input1['patient_select_you'] = $request->patient_select_you;


        $businessRegister = $this->businessRegisterRepository->create($input1);

        Flash::success('H360 Regsiter successfully.');
        return redirect(route('haspatal.register360.create'));
    }

    /**
     * Display the specified Haspatal_360_register.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $haspatal360Register = $this->haspatal360RegisterRepository->find($id);

        $h_360user = User::where('user_type',4)->where('userDetailsId',$id)->first();

        $haspatal360Register['h360buisness'] = DB::table('business_register')->where('user_id',$h_360user->id)->first();


        if (empty($haspatal360Register)) {
            Flash::error('Haspatal 360 Register not found');

            return redirect(route('haspatal360Registers.index'));
        }

        return view('haspatal_360_registers.show')->with('haspatal360Register', $haspatal360Register);
    }

    /**
     * Show the form for editing the specified Haspatal_360_register.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $haspatal360Register = $this->haspatal360RegisterRepository->find($id);

        if (empty($haspatal360Register)) {
            Flash::error('Haspatal 360 Register not found');

            return redirect(route('haspatal360Registers.index'));
        }

        return view('haspatal_360_registers.edit')->with('haspatal360Register', $haspatal360Register);
    }

    /**
     * Update the specified Haspatal_360_register in storage.
     *
     * @param  int              $id
     * @param UpdateHaspatal_360_registerRequest $request
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $haspatal360Register = $this->haspatal360RegisterRepository->find($id);
         $validator = Validator::make($request->all(), [
            'email' => 'required',
            'mobile_no' => 'required',
        ]);

        if ($validator->fails()) {
          return redirect('haspatal360Registers/'.$id.'/edit')
                   ->withErrors($validator)
                   ->withInput();
        }

        if (empty($haspatal360Register)) {
            Flash::error('Haspatal 360 Register not found');

            return redirect(route('haspatal360Registers.index'));
        }

        $haspatal360Register = $this->haspatal360RegisterRepository->update($request->all(), $id);

        Flash::success('Haspatal 360 Register updated successfully.');

        return redirect(route('haspatal360Registers.index'));
    }

    /**
     * Remove the specified Haspatal_360_register from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $haspatal360Register = $this->haspatal360RegisterRepository->find($id);

        if (empty($haspatal360Register)) {
            Flash::error('Haspatal 360 Register not found');

            return redirect(route('haspatal360Registers.index'));
        }

        $u_doctor = DB::table('users')->where('userDetailsId',$id)->where('user_type',4)->delete();

        $this->haspatal360RegisterRepository->delete($id);

        Flash::success('Haspatal 360 Register deleted successfully.');

        return redirect(route('haspatal360Registers.index'));
    }
}
