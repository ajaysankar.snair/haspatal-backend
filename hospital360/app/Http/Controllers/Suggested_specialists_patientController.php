<?php

namespace App\Http\Controllers;

use App\DataTables\Suggested_specialists_patientDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateSuggested_specialists_patientRequest;
use App\Http\Requests\UpdateSuggested_specialists_patientRequest;
use App\Repositories\Suggested_specialists_patientRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Auth;
use Illuminate\Http\Request;
class Suggested_specialists_patientController extends AppBaseController
{
    /** @var  Suggested_specialists_patientRepository */
    private $suggestedSpecialistsPatientRepository;

    public function __construct(Suggested_specialists_patientRepository $suggestedSpecialistsPatientRepo)
    {
        $this->suggestedSpecialistsPatientRepository = $suggestedSpecialistsPatientRepo;
    }

    /**
     * Display a listing of the Suggested_specialists_patient.
     *
     * @param Suggested_specialists_patientDataTable $suggestedSpecialistsPatientDataTable
     * @return Response
     */
    public function index(Suggested_specialists_patientDataTable $suggestedSpecialistsPatientDataTable)
    {
        return $suggestedSpecialistsPatientDataTable->render('suggested_specialists_patients.index');
    }

    /**
     * Show the form for creating a new Suggested_specialists_patient.
     *
     * @return Response
     */
    public function create()
    {
        return view('suggested_specialists_patients.create');
    }

    /**
     * Store a newly created Suggested_specialists_patient in storage.
     *
     * @param CreateSuggested_specialists_patientRequest $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
         $input['created_by'] = Auth::user()->id;
        $input['updated_by'] = Auth::user()->id;
        $suggestedSpecialistsPatient = $this->suggestedSpecialistsPatientRepository->create($input);

        if(Auth::user()->role_id == 1){
        Flash::success('Suggested Specialists Patient saved successfully.');

        return redirect(route('suggestedSpecialistsPatients.index'));
        }else{
            Flash::success('Suggested Specialists saved successfully.');
            return back();
        }
    }

    /**
     * Display the specified Suggested_specialists_patient.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $suggestedSpecialistsPatient = $this->suggestedSpecialistsPatientRepository->find($id);

        if (empty($suggestedSpecialistsPatient)) {
            Flash::error('Suggested Specialists Patient not found');

            return redirect(route('suggestedSpecialistsPatients.index'));
        }

        return view('suggested_specialists_patients.show')->with('suggestedSpecialistsPatient', $suggestedSpecialistsPatient);
    }

    /**
     * Show the form for editing the specified Suggested_specialists_patient.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $suggestedSpecialistsPatient = $this->suggestedSpecialistsPatientRepository->find($id);

        if (empty($suggestedSpecialistsPatient)) {
            Flash::error('Suggested Specialists Patient not found');

            return redirect(route('suggestedSpecialistsPatients.index'));
        }

        return view('suggested_specialists_patients.edit')->with('suggestedSpecialistsPatient', $suggestedSpecialistsPatient);
    }

    /**
     * Update the specified Suggested_specialists_patient in storage.
     *
     * @param  int              $id
     * @param UpdateSuggested_specialists_patientRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSuggested_specialists_patientRequest $request)
    {
        $suggestedSpecialistsPatient = $this->suggestedSpecialistsPatientRepository->find($id);

        if (empty($suggestedSpecialistsPatient)) {
            Flash::error('Suggested Specialists Patient not found');

            return redirect(route('suggestedSpecialistsPatients.index'));
        }

        $suggestedSpecialistsPatient = $this->suggestedSpecialistsPatientRepository->update($request->all(), $id);

        Flash::success('Suggested Specialists Patient updated successfully.');

        return redirect(route('suggestedSpecialistsPatients.index'));
    }

    /**
     * Remove the specified Suggested_specialists_patient from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $suggestedSpecialistsPatient = $this->suggestedSpecialistsPatientRepository->find($id);

        if (empty($suggestedSpecialistsPatient)) {
            Flash::error('Suggested Specialists Patient not found');

            return redirect(route('suggestedSpecialistsPatients.index'));
        }

        $this->suggestedSpecialistsPatientRepository->delete($id);

        Flash::success('Suggested Specialists Patient deleted successfully.');

        return redirect(route('suggestedSpecialistsPatients.index'));
    }
}
