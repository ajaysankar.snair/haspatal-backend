<?php

namespace App\Http\Controllers;

use App\DataTables\Wallet_360_transactionDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateWallet_360_transactionRequest;
use App\Http\Requests\UpdateWallet_360_transactionRequest;
use App\Repositories\Wallet_360_transactionRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class Wallet_360_transactionController extends AppBaseController
{
    /** @var  Wallet_360_transactionRepository */
    private $wallet360TransactionRepository;

    public function __construct(Wallet_360_transactionRepository $wallet360TransactionRepo)
    {
        $this->wallet360TransactionRepository = $wallet360TransactionRepo;
    }

    /**
     * Display a listing of the Wallet_360_transaction.
     *
     * @param Wallet_360_transactionDataTable $wallet360TransactionDataTable
     * @return Response
     */
    public function index(Wallet_360_transactionDataTable $wallet360TransactionDataTable)
    {
        return $wallet360TransactionDataTable->render('wallet_360_transactions.index');
    }

    /**
     * Show the form for creating a new Wallet_360_transaction.
     *
     * @return Response
     */
    public function create()
    {
        return view('wallet_360_transactions.create');
    }

    /**
     * Store a newly created Wallet_360_transaction in storage.
     *
     * @param CreateWallet_360_transactionRequest $request
     *
     * @return Response
     */
    public function store(CreateWallet_360_transactionRequest $request)
    {
        $input = $request->all();

        $wallet360Transaction = $this->wallet360TransactionRepository->create($input);

        Flash::success('Wallet 360 Transaction saved successfully.');

        return redirect(route('wallet360Transactions.index'));
    }

    /**
     * Display the specified Wallet_360_transaction.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $wallet360Transaction = $this->wallet360TransactionRepository->find($id);

        if (empty($wallet360Transaction)) {
            Flash::error('Wallet 360 Transaction not found');

            return redirect(route('wallet360Transactions.index'));
        }

        return view('wallet_360_transactions.show')->with('wallet360Transaction', $wallet360Transaction);
    }

    /**
     * Show the form for editing the specified Wallet_360_transaction.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $wallet360Transaction = $this->wallet360TransactionRepository->find($id);

        if (empty($wallet360Transaction)) {
            Flash::error('Wallet 360 Transaction not found');

            return redirect(route('wallet360Transactions.index'));
        }

        return view('wallet_360_transactions.edit')->with('wallet360Transaction', $wallet360Transaction);
    }

    /**
     * Update the specified Wallet_360_transaction in storage.
     *
     * @param  int              $id
     * @param UpdateWallet_360_transactionRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateWallet_360_transactionRequest $request)
    {
        $wallet360Transaction = $this->wallet360TransactionRepository->find($id);

        if (empty($wallet360Transaction)) {
            Flash::error('Wallet 360 Transaction not found');

            return redirect(route('wallet360Transactions.index'));
        }

        $wallet360Transaction = $this->wallet360TransactionRepository->update($request->all(), $id);

        Flash::success('Wallet 360 Transaction updated successfully.');

        return redirect(route('wallet360Transactions.index'));
    }

    /**
     * Remove the specified Wallet_360_transaction from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $wallet360Transaction = $this->wallet360TransactionRepository->find($id);

        if (empty($wallet360Transaction)) {
            Flash::error('Wallet 360 Transaction not found');

            return redirect(route('wallet360Transactions.index'));
        }

        $this->wallet360TransactionRepository->delete($id);

        Flash::success('Wallet 360 Transaction deleted successfully.');

        return redirect(route('wallet360Transactions.index'));
    }
}
