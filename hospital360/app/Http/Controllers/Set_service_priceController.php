<?php

namespace App\Http\Controllers;

use App\DataTables\Set_service_priceDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateSet_service_priceRequest;
use App\Http\Requests\UpdateSet_service_priceRequest;
use App\Repositories\Set_service_priceRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Auth;
use DB;
class Set_service_priceController extends AppBaseController
{
    /** @var  Set_service_priceRepository */
    private $setServicePriceRepository;

    public function __construct(Set_service_priceRepository $setServicePriceRepo)
    {
        $this->setServicePriceRepository = $setServicePriceRepo;
    }

    /**
     * Display a listing of the Set_service_price.
     *
     * @param Set_service_priceDataTable $setServicePriceDataTable
     * @return Response
     */
    public function index(Set_service_priceDataTable $setServicePriceDataTable)
    {
        return $setServicePriceDataTable->render('set_service_prices.index');
    }

    /**
     * Show the form for creating a new Set_service_price.
     *
     * @return Response
     */
    public function create()
    {
        $serviceList = DB::table('service_details')->pluck('service','id');
        return view('set_service_prices.create',compact('serviceList'));
    }

    /**
     * Store a newly created Set_service_price in storage.
     *
     * @param CreateSet_service_priceRequest $request
     *
     * @return Response
     */
    public function store(CreateSet_service_priceRequest $request)
    {
        $input = $request->all();
        $input['created_by'] = Auth::user()->id;
        $input['updated_by'] = Auth::user()->id;

        $setServicePrice = $this->setServicePriceRepository->create($input);

        Flash::success('Set Service Price saved successfully.');

        return redirect(route('setServicePrices.index'));
    }

    /**
     * Display the specified Set_service_price.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        //$setServicePrice = $this->setServicePriceRepository->find($id);
        $setServicePrice = DB::table('set_service_price')->join('service_details','service_details.id','set_service_price.service_name')->select('set_service_price.*','service_details.service')->where('set_service_price.id',$id)->first();
        if (empty($setServicePrice)) {
            Flash::error('Set Service Price not found');

            return redirect(route('setServicePrices.index'));
        }

        return view('set_service_prices.show')->with('setServicePrice', $setServicePrice);
    }

    /**
     * Show the form for editing the specified Set_service_price.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $setServicePrice = $this->setServicePriceRepository->find($id);

        if (empty($setServicePrice)) {
            Flash::error('Set Service Price not found');

            return redirect(route('setServicePrices.index'));
        }
        $serviceList = DB::table('service_details')->pluck('service','id');
        return view('set_service_prices.edit',compact('serviceList'))->with('setServicePrice', $setServicePrice);
    }

    /**
     * Update the specified Set_service_price in storage.
     *
     * @param  int              $id
     * @param UpdateSet_service_priceRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSet_service_priceRequest $request)
    {
        $setServicePrice = $this->setServicePriceRepository->find($id);

        if (empty($setServicePrice)) {
            Flash::error('Set Service Price not found');

            return redirect(route('setServicePrices.index'));
        }

        $setServicePrice = $this->setServicePriceRepository->update($request->all(), $id);

        Flash::success('Set Service Price updated successfully.');

        return redirect(route('setServicePrices.index'));
    }

    /**
     * Remove the specified Set_service_price from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $setServicePrice = $this->setServicePriceRepository->find($id);

        if (empty($setServicePrice)) {
            Flash::error('Set Service Price not found');

            return redirect(route('setServicePrices.index'));
        }

        $this->setServicePriceRepository->delete($id);

        Flash::success('Set Service Price deleted successfully.');

        return redirect(route('setServicePrices.index'));
    }
}
