<?php

namespace App\Http\Controllers;

use App\DataTables\Patient_refund_requestDataTable;
use App\Http\Requests;
use App\Http\Requests\CreatePatient_refund_requestRequest;
use App\Http\Requests\UpdatePatient_refund_requestRequest;
use App\Repositories\Patient_refund_requestRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class Patient_refund_requestController extends AppBaseController
{
    /** @var  Patient_refund_requestRepository */
    private $patientRefundRequestRepository;

    public function __construct(Patient_refund_requestRepository $patientRefundRequestRepo)
    {
        $this->patientRefundRequestRepository = $patientRefundRequestRepo;
    }

    /**
     * Display a listing of the Patient_refund_request.
     *
     * @param Patient_refund_requestDataTable $patientRefundRequestDataTable
     * @return Response
     */
    public function index(Patient_refund_requestDataTable $patientRefundRequestDataTable)
    {
        return $patientRefundRequestDataTable->render('patient_refund_requests.index');
    }

    /**
     * Show the form for creating a new Patient_refund_request.
     *
     * @return Response
     */
    public function create()
    {
        return view('patient_refund_requests.create');
    }

    /**
     * Store a newly created Patient_refund_request in storage.
     *
     * @param CreatePatient_refund_requestRequest $request
     *
     * @return Response
     */
    public function store(CreatePatient_refund_requestRequest $request)
    {
        $input = $request->all();

        $patientRefundRequest = $this->patientRefundRequestRepository->create($input);

        Flash::success('Patient Refund Request saved successfully.');

        return redirect(route('patientRefundRequests.index'));
    }

    /**
     * Display the specified Patient_refund_request.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $patientRefundRequest = $this->patientRefundRequestRepository->find($id);

        if (empty($patientRefundRequest)) {
            Flash::error('Patient Refund Request not found');

            return redirect(route('patientRefundRequests.index'));
        }

        return view('patient_refund_requests.show')->with('patientRefundRequest', $patientRefundRequest);
    }

    /**
     * Show the form for editing the specified Patient_refund_request.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $patientRefundRequest = $this->patientRefundRequestRepository->find($id);

        if (empty($patientRefundRequest)) {
            Flash::error('Patient Refund Request not found');

            return redirect(route('patientRefundRequests.index'));
        }

        return view('patient_refund_requests.edit')->with('patientRefundRequest', $patientRefundRequest);
    }

    /**
     * Update the specified Patient_refund_request in storage.
     *
     * @param  int              $id
     * @param UpdatePatient_refund_requestRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePatient_refund_requestRequest $request)
    {
        $patientRefundRequest = $this->patientRefundRequestRepository->find($id);

        if (empty($patientRefundRequest)) {
            Flash::error('Patient Refund Request not found');

            return redirect(route('patientRefundRequests.index'));
        }

        $patientRefundRequest = $this->patientRefundRequestRepository->update($request->all(), $id);

        Flash::success('Patient Refund Request updated successfully.');

        return redirect(route('patientRefundRequests.index'));
    }

    /**
     * Remove the specified Patient_refund_request from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $patientRefundRequest = $this->patientRefundRequestRepository->find($id);

        if (empty($patientRefundRequest)) {
            Flash::error('Patient Refund Request not found');

            return redirect(route('patientRefundRequests.index'));
        }

        $this->patientRefundRequestRepository->delete($id);

        Flash::success('Patient Refund Request deleted successfully.');

        return redirect(route('patientRefundRequests.index'));
    }
}
