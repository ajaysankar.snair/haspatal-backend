<?php

namespace App\Http\Controllers;

use App\DataTables\MedicineDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateMedicineRequest;
use App\Http\Requests\UpdateMedicineRequest;
use App\Repositories\MedicineRepository;
use App\Http\Controllers\AppBaseController;
use Flash;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Illuminate\View\View;
use Response;

class MedicineController extends AppBaseController
{
    /** @var  MedicineRepository */
    private $medicineRepository;

    public function __construct(MedicineRepository $medicineRepo)
    {
        $this->medicineRepository = $medicineRepo;
    }

    /**
     * Display a listing of the Medicine.
     *
     * @param MedicineDataTable $medicineDataTable
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function index(MedicineDataTable $medicineDataTable)
    {
        return $medicineDataTable->render('medicines.index');
    }

    /**
     * Show the form for creating a new Medicine.
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function create()
    {
        return view('medicines.create');
    }

    /**
     * Store a newly created Medicine in storage.
     *
     * @param CreateMedicineRequest $request
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function store(CreateMedicineRequest $request)
    {
        $input = $request->all();

        $medicine = $this->medicineRepository->create($input);

        Flash::success('Medicine saved successfully.');

        return redirect(route('medicines.index'));
    }

    /**
     * Display the specified Medicine.
     *
     * @param  int $id
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function show($id)
    {
        $medicine = $this->medicineRepository->find($id);

        if (empty($medicine)) {
            Flash::error('Medicine not found');

            return redirect(route('medicines.index'));
        }

        return view('medicines.show')->with('medicine', $medicine);
    }

    /**
     * Show the form for editing the specified Medicine.
     *
     * @param  int $id
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function edit($id)
    {
        $medicine = $this->medicineRepository->find($id);

        if (empty($medicine)) {
            Flash::error('Medicine not found');

            return redirect(route('medicines.index'));
        }

        return view('medicines.edit')->with('medicine', $medicine);
    }

    /**
     * Update the specified Medicine in storage.
     *
     * @param  int              $id
     * @param UpdateMedicineRequest $request
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function update($id, UpdateMedicineRequest $request)
    {
        $medicine = $this->medicineRepository->find($id);

        if (empty($medicine)) {
            Flash::error('Medicine not found');

            return redirect(route('medicines.index'));
        }

        $medicine = $this->medicineRepository->update($request->all(), $id);

        Flash::success('Medicine updated successfully.');

        return redirect(route('medicines.index'));
    }

    /**
     * Remove the specified Medicine from storage.
     *
     * @param  int $id
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function destroy($id)
    {
        $medicine = $this->medicineRepository->find($id);

        if (empty($medicine)) {
            Flash::error('Medicine not found');

            return redirect(route('medicines.index'));
        }

        $this->medicineRepository->delete($id);

        Flash::success('Medicine deleted successfully.');

        return redirect(route('medicines.index'));
    }
}
