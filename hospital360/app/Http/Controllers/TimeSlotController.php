<?php

namespace App\Http\Controllers;

use App\DataTables\TimeSlotDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateTimeSlotRequest;
use App\Http\Requests\UpdateTimeSlotRequest;
use App\Repositories\TimeSlotRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Auth;
use DB;
use User;

class TimeSlotController extends AppBaseController
{
    /** @var  TimeSlotRepository */
    private $timeSlotRepository;

    public function __construct(TimeSlotRepository $timeSlotRepo)
    {
        $this->timeSlotRepository = $timeSlotRepo;
    }

    /**
     * Display a listing of the TimeSlot.
     *
     * @param TimeSlotDataTable $timeSlotDataTable
     * @return Response
     */
    public function index(TimeSlotDataTable $timeSlotDataTable)
    {
        return $timeSlotDataTable->render('time_slots.index');
    }

    /**
     * Show the form for creating a new TimeSlot.
     *
     * @return Response
     */
    public function create()
    {
        return view('time_slots.create');
    }

    /**
     * Store a newly created TimeSlot in storage.
     *
     * @param CreateTimeSlotRequest $request
     *
     * @return Response
     */
    public function store(CreateTimeSlotRequest $request)
    {
        $input = $request->all();
        $input['doctor_id'] = Auth::user()->userDetailsId;
        $input['created_by'] = Auth::user()->id;
        $input['updated_by'] = Auth::user()->id;

        $timeSlot = $this->timeSlotRepository->create($input);

        $user = DB::table('users')->where('user_type',1)->where('userDetailsId', Auth::user()->userDetailsId)->update(['doctor_login_status'=> 1]);
         if(Auth::user()->role_id == 1){
            Flash::success('Time Slot saved successfully.');
            return redirect(route('doctors.index'));
        }else{
            Flash::success('Time Slot saved successfully.');
            return back();
        }

        /*Flash::success('Time Slot saved successfully.');

        return redirect(route('timeSlots.index'));*/
    }

    /**
     * Display the specified TimeSlot.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        //$timeSlot = $this->timeSlotRepository->find($id);
        $timeSlot = DB::table('time_slot')->join('doctor_details','doctor_details.id','time_slot.doctor_id')->select('time_slot.*','doctor_details.first_name','doctor_details.last_name')->where('time_slot.id',$id)->first();
        if (empty($timeSlot)) {
            Flash::error('Time Slot not found');

            return redirect(route('timeSlots.index'));
        }

        return view('time_slots.show')->with('timeSlot', $timeSlot);
    }

    /**
     * Show the form for editing the specified TimeSlot.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $timeSlot = $this->timeSlotRepository->find($id);

        if (empty($timeSlot)) {
            Flash::error('Time Slot not found');

            return redirect(route('timeSlots.index'));
        }

        return view('time_slots.edit')->with('timeSlot', $timeSlot);
    }

    /**
     * Update the specified TimeSlot in storage.
     *
     * @param  int              $id
     * @param UpdateTimeSlotRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTimeSlotRequest $request)
    {
        $timeSlot = $this->timeSlotRepository->find($id);
        $input = $request->all();
        if (empty($timeSlot)) {
            Flash::error('Time Slot not found');

            return redirect(route('timeSlots.index'));
        }

        $timeSlot = $this->timeSlotRepository->update($input, $id);
        $user = DB::table('users')->where('user_type',1)->where('userDetailsId', Auth::user()->userDetailsId)->update(['doctor_login_status'=> 1]);

        //echo"<pre>";print_r($user);exit;

        if(Auth::user()->role_id == 1){
        Flash::success('Time Slot updated successfully.');

        return redirect(route('timeSlots.index'));
        }else{
            Flash::success('Time Slot updated successfully.');
            return back();
        }
    }

    /**
     * Remove the specified TimeSlot from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $timeSlot = $this->timeSlotRepository->find($id);

        if (empty($timeSlot)) {
            Flash::error('Time Slot not found');

            return redirect(route('timeSlots.index'));
        }

        $this->timeSlotRepository->delete($id);

        Flash::success('Time Slot deleted successfully.');

        return redirect(route('timeSlots.index'));
    }
}
