<?php

namespace App\Http\Controllers;

use App\DataTables\Plan_masterDataTable;
use App\Http\Requests;
use App\Http\Requests\CreatePlan_masterRequest;
use App\Http\Requests\UpdatePlan_masterRequest;
use App\Repositories\Plan_masterRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Auth;
use DB;

class Plan_masterController extends AppBaseController
{
    /** @var  Plan_masterRepository */
    private $planMasterRepository;

    public function __construct(Plan_masterRepository $planMasterRepo)
    {
        $this->planMasterRepository = $planMasterRepo;
    }

    public function plan_active($id)
    {

      /*$booklist =  DB::table('users')->where('userDetailsId',$id)->where('user_type',1)->update(['status' => 1]);*/

      $booklist =  DB::table('plan_master')->where('pl_id',$id)->update(['status' => 2]);

      Flash::success('Doctor updated successfully.');

      return back();

    }
    public function plan_deactive($id)
    {
      /*$booklist =  DB::table('users')->where('userDetailsId',$id)->where('user_type',1)->update(['status' => 0]);*/
      
      $booklist =  DB::table('plan_master')->where('pl_id',$id)->update(['status' => 1]);

      Flash::success('Doctor updated successfully.');
      return back();

    }

    /**
     * Display a listing of the Plan_master.
     *
     * @param Plan_masterDataTable $planMasterDataTable
     * @return Response
     */
    public function index(Plan_masterDataTable $planMasterDataTable)
    {
        return $planMasterDataTable->render('plan_masters.index');
    }

    /**
     * Show the form for creating a new Plan_master.
     *
     * @return Response
     */
    public function create()
    {
        return view('plan_masters.create');
    }

    /**
     * Store a newly created Plan_master in storage.
     *
     * @param CreatePlan_masterRequest $request
     *
     * @return Response
     */
    public function store(CreatePlan_masterRequest $request)
    {
        $input = $request->all();
        $input['created_by'] = Auth::user()->id;
        $input['updated_by'] = Auth::user()->id;
        $planMaster = $this->planMasterRepository->create($input);

        Flash::success('Plan Master saved successfully.');

        return redirect(route('planMasters.index'));
    }

    /**
     * Display the specified Plan_master.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $planMaster = $this->planMasterRepository->find($id);

        if (empty($planMaster)) {
            Flash::error('Plan Master not found');

            return redirect(route('planMasters.index'));
        }

        return view('plan_masters.show')->with('planMaster', $planMaster);
    }

    /**
     * Show the form for editing the specified Plan_master.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $planMaster = $this->planMasterRepository->find($id);

        if (empty($planMaster)) {
            Flash::error('Plan Master not found');

            return redirect(route('planMasters.index'));
        }

        return view('plan_masters.edit')->with('planMaster', $planMaster);
    }

    /**
     * Update the specified Plan_master in storage.
     *
     * @param  int              $id
     * @param UpdatePlan_masterRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePlan_masterRequest $request)
    {
        $planMaster = $this->planMasterRepository->find($id);

        if (empty($planMaster)) {
            Flash::error('Plan Master not found');

            return redirect(route('planMasters.index'));
        }

        $planMaster = $this->planMasterRepository->update($request->all(), $id);

        Flash::success('Plan Master updated successfully.');

        return redirect(route('planMasters.index'));
    }

    /**
     * Remove the specified Plan_master from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $planMaster = $this->planMasterRepository->find($id);

        if (empty($planMaster)) {
            Flash::error('Plan Master not found');

            return redirect(route('planMasters.index'));
        }

        $this->planMasterRepository->delete($id);

        Flash::success('Plan Master deleted successfully.');

        return redirect(route('planMasters.index'));
    }
}
