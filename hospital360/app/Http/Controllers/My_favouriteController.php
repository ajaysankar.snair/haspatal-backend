<?php

namespace App\Http\Controllers;

use App\DataTables\My_favouriteDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateMy_favouriteRequest;
use App\Http\Requests\UpdateMy_favouriteRequest;
use App\Repositories\My_favouriteRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class My_favouriteController extends AppBaseController
{
    /** @var  My_favouriteRepository */
    private $myFavouriteRepository;

    public function __construct(My_favouriteRepository $myFavouriteRepo)
    {
        $this->myFavouriteRepository = $myFavouriteRepo;
    }

    /**
     * Display a listing of the My_favourite.
     *
     * @param My_favouriteDataTable $myFavouriteDataTable
     * @return Response
     */
    public function index(My_favouriteDataTable $myFavouriteDataTable)
    {
        return $myFavouriteDataTable->render('my_favourites.index');
    }

    /**
     * Show the form for creating a new My_favourite.
     *
     * @return Response
     */
    public function create()
    {
        return view('my_favourites.create');
    }

    /**
     * Store a newly created My_favourite in storage.
     *
     * @param CreateMy_favouriteRequest $request
     *
     * @return Response
     */
    public function store(CreateMy_favouriteRequest $request)
    {
        $input = $request->all();

        $myFavourite = $this->myFavouriteRepository->create($input);

        Flash::success('My Favourite saved successfully.');

        return redirect(route('myFavourites.index'));
    }

    /**
     * Display the specified My_favourite.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $myFavourite = $this->myFavouriteRepository->find($id);

        if (empty($myFavourite)) {
            Flash::error('My Favourite not found');

            return redirect(route('myFavourites.index'));
        }

        return view('my_favourites.show')->with('myFavourite', $myFavourite);
    }

    /**
     * Show the form for editing the specified My_favourite.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $myFavourite = $this->myFavouriteRepository->find($id);

        if (empty($myFavourite)) {
            Flash::error('My Favourite not found');

            return redirect(route('myFavourites.index'));
        }

        return view('my_favourites.edit')->with('myFavourite', $myFavourite);
    }

    /**
     * Update the specified My_favourite in storage.
     *
     * @param  int              $id
     * @param UpdateMy_favouriteRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMy_favouriteRequest $request)
    {
        $myFavourite = $this->myFavouriteRepository->find($id);

        if (empty($myFavourite)) {
            Flash::error('My Favourite not found');

            return redirect(route('myFavourites.index'));
        }

        $myFavourite = $this->myFavouriteRepository->update($request->all(), $id);

        Flash::success('My Favourite updated successfully.');

        return redirect(route('myFavourites.index'));
    }

    /**
     * Remove the specified My_favourite from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $myFavourite = $this->myFavouriteRepository->find($id);

        if (empty($myFavourite)) {
            Flash::error('My Favourite not found');

            return redirect(route('myFavourites.index'));
        }

        $this->myFavouriteRepository->delete($id);

        Flash::success('My Favourite deleted successfully.');

        return redirect(route('myFavourites.index'));
    }
}
