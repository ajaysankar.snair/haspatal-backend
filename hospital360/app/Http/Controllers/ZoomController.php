<?php

namespace App\Http\Controllers;

use App\Http\Requests\API\CreateBookingRequestAPIRequest;
use App\Http\Requests\API\UpdateBookingRequestAPIRequest;
use App\Models\BookingRequest;
use App\Repositories\BookingRequestRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;
use Carbon\Carbon;
use DB;
class ZoomController extends Controller
{
    //

    public function zoomtoken()
    {
   
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.zoom.us/v2/users/8eIxTHlqTny9h2IS6xm7gQ/token?type=zak",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => true,
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
          
            CURLOPT_HTTPHEADER => array(
                "authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOm51bGwsImlzcyI6ImJZRmhLNVFtUjlpeWd0ZW9UZ0Z5OUEiLCJleHAiOjE2NzMzNTQ1MjAsImlhdCI6MTYxNzkzNjc3NX0.ilN7HdBpCZK-hXut7kvokOVj1ePXTsABzmJU5sLJrJM",
                "content-type: application/json"
              ),
            
        ));
        $response = curl_exec($curl);
      
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
            //echo "cURL Error #:" . $err;
            return "OTP Not Send";
        } else {
            //echo $response;exit();
            return $response;
        }
    }
    public function zoommeeting(Request $request)
    {
       $request_id=$request->input('request_id');
      //  return $request_id;
       $request_url = "https://api.zoom.us/v2/users/8eIxTHlqTny9h2IS6xm7gQ/meetings";




        $postFields =  '{
        "topic": "New Meeting",
        "type": 2,
        "start_time": "2021-04-01T12:00:00Z",
        "duration": 45,
        "timezone": "America/Anchorage",
        "password": "1234",
        "agenda": "Zoom WordPress",
        "tracking_fields": [
          {
            "field": "string",
            "value": "string"
          }
        ],
        "settings": {
          "host_video": true,
          "participant_video": true,
          "cn_meeting": false,
          "in_meeting": false,
          "join_before_host": false,
          "mute_upon_entry": true,
          "watermark": false,
          "use_pmi": false,
          "approval_type": 0,
          "registration_type": 1,
          "audio":"voip", 
          "enforce_login": false,
          "enforce_login_domains": "",
          "alternative_hosts": "",
          "registrants_email_notification": false
        }
      }';



        $ch = curl_init();





        curl_setopt_array($ch, array(
            CURLOPT_URL => $request_url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $postFields,
            CURLOPT_HTTPHEADER => array(
                "authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOm51bGwsImlzcyI6ImJZRmhLNVFtUjlpeWd0ZW9UZ0Z5OUEiLCJleHAiOjE2NzMzNTQ1MjAsImlhdCI6MTYxNzkzNjc3NX0.ilN7HdBpCZK-hXut7kvokOVj1ePXTsABzmJU5sLJrJM",
                "content-type: application/json",
                // "Accept: application/json",
            ),
        ));


        $response = curl_exec($ch);
        // return response()->json($response);
        // return $response;
        $err = curl_error($ch);
        curl_close($ch);
        if (!$response) {
            return $err;
        }
        $meeting = json_decode($response, true);
        // $meeting_id=$meeting['id'];
        // $meeting_password=$meeting['password'];
        // $result = BookingRequest::where('id', $request_id)->update(['meeting_id' => $meeting_id, 'password_meeting' => $meeting_password]);
        return $meeting;
    }



    public function updatemeeting(Request $request)
    {
    // return $request;
      $request_id=$request->input('request_id');
      $meeting_id=(string)$request->input('meetingNumber');
      $meeting_password=$request->input('meetingPassword');
        //  return $meeting_id;
      $result = BookingRequest::where('id', $request_id)->update(['meeting_id' => $meeting_id, 'password_meeting' => $meeting_password,'call_status' => 1]);
      $meeting = BookingRequest::where('id', $request_id)->orderBy('id', 'desc')
       ->first();
       return $meeting;
    }

    public function callended_status(Request $request)
    {
    // return $request;
      $request_id=$request->input('request_id');
      
        //  return $meeting_id;
      $result = BookingRequest::where('id', $request_id)->update(['call_status' => 9]);
      $meeting = BookingRequest::where('id', $request_id)->orderBy('id', 'desc')
       ->first();
       return $meeting;
    }




    public function joinMeeting(Request $request)
    {
       $request_id=$request->input('request_id');
      
       $meeting = BookingRequest::where('id', $request_id)->orderBy('id', 'desc')
       ->first();
        // $meeting = BookingRequest::select('*')->where('id',  $request_id);
        return $meeting;
    }
    function sendFCM($token=null,$title=null,$message=null,$user_id=null)
{
    $path_to_firebase_cm = 'https://fcm.googleapis.com/fcm/send';
    $fields = array(
        'to' => 'ccz0W77OTsKzdJSUqpUueo:APA91bF1obTtREGM8TEs-AuQdO7xqkVmFKQDXO8bV6OD0bGvkst4tFUIZ0Z8jnPAVALX70HF4-k04WjW6lb3-Va83SynoyIum3PVzMqwixaFYxe5wIFEi2oPh0e-3zwIgJL8sOOXZtF5',

        'notification' => array('title' => 'Order Placed', 'body' => 'Order Number C1RR3 has been placed for you at 17-10-2021, Kindly complete the order at the earliest and confirm using Haspatal 360 App', 'sound' => 'default', 'click_action' => 'FCM_PLUGIN_ACTIVITY', 'icon' => 'fcm_push_icon'),

        'data' => array('title' => 'title', 'Order Placed' => 'Order Number C1RR3 has been placed for you at 17-10-2021
        Kindly complete the order at the earliest and confirm using Haspatal 360 App', 'sound' => 'default', 'icon' => 'fcm_push_icon'),
    );
    $headers = array(
        'Authorization:key=' . ' AAAAENvHI2Q:APA91bGZeZW1jpHGLP7Pp9Dpf9-T7d5LoiMOgpUaJJITzjXhvbHLOEH2gfAiIZNIQEy_9iZ8saL6Qmhq-hM_ChWbnfC3pTXui5nVbQVtRy2PGFcqo6lYy9aCUxvQPuG1YKWmIJtftpgY  ',
        'Content-Type:application/json'
    );
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $path_to_firebase_cm); 
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); 
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
    $result = curl_exec($ch);
    curl_close($ch);
    return $result;
}
public function enablex(Request $request)
    {
      $kAppId    = "60e3e32720afd110643ba984";
      $kAppkey   = "7aBujeGyaa9uAeLyuu5y2yzyryYuhuMe5yae";
      //  return $request_id;
       $request_url = "https://api.enablex.io/video/v1/rooms";




        $postFields =  '{
          "name": "Hapatal",
          "owner_ref": "dooctor",
          "settings": {
              "description": "Descriptive text",
              "mode": "group",
              "scheduled": false,
              "adhoc": false,
              "duration": 30,
        "moderators": "1",
              "participants": "5",
              "billing_code": "",
              "auto_recording": false,
              "quality": "SD",
              "canvas": false,
        "screen_share": false,
              "abwd": true,
        "max_active_talkers": 4,
        "knock": false,
        "wait_for_moderator": false,
      
        "single_file_recording": false
          },
          "sip" : {
              "enabled": false
          },
          "data": {
              "custom_key": ""
          }              
     }';



        $ch = curl_init();





        curl_setopt_array($ch, array(
            CURLOPT_URL => $request_url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $postFields,
            CURLOPT_HTTPHEADER => array(
              'Authorization: Basic ' . base64_encode( $kAppId  . ":" .  $kAppkey ),
              'Content-Type: application/json',
                // "Accept: application/json",
            ),
        ));


        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }
    public function enablextoken(Request $request)
    {
   
      $kAppId    = "60e3e32720afd110643ba984";
      $kAppkey   = "7aBujeGyaa9uAeLyuu5y2yzyryYuhuMe5yae";
      //  return $request_id;
      $room_ID=$request->input('room_ID');
    //  return base64_encode( $kAppId  . ":" .  $kAppkey );
      //  return $request_id;
       $request_url = "https://api.enablex.io/video/v1/rooms/".$room_ID."/tokens";
    //  return  $request_url;
       $postFields =  '{
        "name": "Patient",
        "role": "participant",
        "user_ref": "XXX"
   }';



        $ch = curl_init();





        curl_setopt_array($ch, array(
            CURLOPT_URL => $request_url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $postFields,
            CURLOPT_HTTPHEADER => array(
              'Authorization: Basic ' . base64_encode( $kAppId  . ":" .  $kAppkey ),
              'Content-Type: application/json',
                // "Accept: application/json",
            ),
        ));


        $response = curl_exec($ch);
        $err = curl_error($ch);
        curl_close($ch);
        if (!$response) {
            return $err;
        }
        return $response;
    }
}
