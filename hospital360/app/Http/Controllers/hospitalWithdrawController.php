<?php



namespace App\Http\Controllers;



use App\DataTables\hospitalWithdrawDataTable;

use App\Http\Requests;

use App\Http\Requests\CreatehospitalWithdrawRequest;

use App\Http\Requests\UpdatehospitalWithdrawRequest;

use App\Repositories\hospitalWithdrawRepository;

use App\Http\Controllers\AppBaseController;

use Flash;

use Illuminate\Contracts\View\Factory;

use Illuminate\Http\RedirectResponse;

use Illuminate\Routing\Redirector;

use Illuminate\View\View;

use Response;

use Auth;
use DB;

class hospitalWithdrawController extends AppBaseController

{

    /** @var  hospitalWithdrawRepository */

    private $hospitalWithdrawRepository;



    public function __construct(hospitalWithdrawRepository $hospitalWithdrawRepo)

    {

        $this->hospitalWithdrawRepository = $hospitalWithdrawRepo;

    }



    /**

     * Display a listing of the hospitalWithdraw.

     *

     * @param hospitalWithdrawDataTable $hospitalWithdrawDataTable

     * @return Response|Factory|RedirectResponse|Redirector|View

     */

    public function index(hospitalWithdrawDataTable $hospitalWithdrawDataTable)

    {

        return $hospitalWithdrawDataTable->render('hospital_withdraws.index');

    }



    /**

     * Show the form for creating a new hospitalWithdraw.

     *

     * @return Response|Factory|RedirectResponse|Redirector|View

     */

    public function create()

    {

        return view('hospital_withdraws.create');

    }



    /**

     * Store a newly created hospitalWithdraw in storage.

     *

     * @param CreatehospitalWithdrawRequest $request

     *

     * @return Response|Factory|RedirectResponse|Redirector|View

     */

    public function store(CreatehospitalWithdrawRequest $request)

    {

        $input = $request->all();
        $input['created_by'] = Auth::user()->id;
        $input['updated_by'] = Auth::user()->id;
        $input['hospital_id'] = Auth::user()->userDetailsId;


        $hospitalWithdraw = $this->hospitalWithdrawRepository->create($input);



        Flash::success('Hospital Withdraw saved successfully.');

        return back();

        return redirect(route('hospitalWithdraws.index'));

    }



    /**

     * Display the specified hospitalWithdraw.

     *

     * @param  int $id

     *

     * @return Response|Factory|RedirectResponse|Redirector|View

     */

    public function show($id)

    {

       // $hospitalWithdraw = $this->hospitalWithdrawRepository->find($id);

            $hospitalWithdraw=  DB::table('haspatal_withdraw')
            ->join('haspatal_regsiter','haspatal_regsiter.id','haspatal_withdraw.haspatal_id')
            ->where('haspatal_withdraw.id',$id)
            ->select('haspatal_withdraw.status','haspatal_withdraw.cheque_img','haspatal_withdraw.id','haspatal_regsiter.hospital_name','haspatal_withdraw.bank_name','haspatal_withdraw.ifsc_code','haspatal_withdraw.requested_amount','haspatal_withdraw.status','haspatal_withdraw.account_name','haspatal_withdraw.created_at')->get();
    

        if (empty($hospitalWithdraw)) {

            Flash::error('Hospital Withdraw not found');



            return redirect(route('hospitalWithdraws.index'));

        }



        return view('hospital_withdraws.show')->with('hospitalWithdraw', $hospitalWithdraw[0]);

    }



    /**

     * Show the form for editing the specified hospitalWithdraw.

     *

     * @param  int $id

     *

     * @return Response|Factory|RedirectResponse|Redirector|View

     */

    public function edit($id)

    {

        $hospitalWithdraw = $this->hospitalWithdrawRepository->find($id);



        if (empty($hospitalWithdraw)) {

            Flash::error('Hospital Withdraw not found');



            return redirect(route('hospitalWithdraws.index'));

        }



        return view('hospital_withdraws.edit')->with('hospitalWithdraw', $hospitalWithdraw);

    }



    /**

     * Update the specified hospitalWithdraw in storage.

     *

     * @param  int              $id

     * @param UpdatehospitalWithdrawRequest $request

     *

     * @return Response|Factory|RedirectResponse|Redirector|View

     */

    public function update($id, UpdatehospitalWithdrawRequest $request)

    {

        $hospitalWithdraw = $this->hospitalWithdrawRepository->find($id);



        if (empty($hospitalWithdraw)) {

            Flash::error('Hospital Withdraw not found');



            return redirect(route('hospitalWithdraws.index'));

        }



        $hospitalWithdraw = $this->hospitalWithdrawRepository->update($request->all(), $id);



        Flash::success('Hospital Withdraw updated successfully.');



        return redirect(route('hospitalWithdraws.index'));

    }



    /**

     * Remove the specified hospitalWithdraw from storage.

     *

     * @param  int $id

     *

     * @return Response|Factory|RedirectResponse|Redirector|View

     */

    public function destroy($id)

    {

        $hospitalWithdraw = $this->hospitalWithdrawRepository->find($id);



        if (empty($hospitalWithdraw)) {

            Flash::error('Hospital Withdraw not found');



            return redirect(route('hospitalWithdraws.index'));

        }



        $this->hospitalWithdrawRepository->delete($id);



        Flash::success('Hospital Withdraw deleted successfully.');



        return redirect(route('hospitalWithdraws.index'));

    }

}

