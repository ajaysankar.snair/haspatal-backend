<?php

namespace App\Http\Controllers;

use App\DataTables\Present_complaint_patientDataTable;
use App\Http\Requests;
use App\Http\Requests\CreatePresent_complaint_patientRequest;
use App\Http\Requests\UpdatePresent_complaint_patientRequest;
use App\Repositories\Present_complaint_patientRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Auth;

class Present_complaint_patientController extends AppBaseController
{
    /** @var  Present_complaint_patientRepository */
    private $presentComplaintPatientRepository;

    public function __construct(Present_complaint_patientRepository $presentComplaintPatientRepo)
    {
        $this->presentComplaintPatientRepository = $presentComplaintPatientRepo;
    }

    /**
     * Display a listing of the Present_complaint_patient.
     *
     * @param Present_complaint_patientDataTable $presentComplaintPatientDataTable
     * @return Response
     */
    public function index(Present_complaint_patientDataTable $presentComplaintPatientDataTable)
    {
        return $presentComplaintPatientDataTable->render('present_complaint_patients.index');
    }

    /**
     * Show the form for creating a new Present_complaint_patient.
     *
     * @return Response
     */
    public function create()
    {
        return view('present_complaint_patients.create');
    }

    /**
     * Store a newly created Present_complaint_patient in storage.
     *
     * @param CreatePresent_complaint_patientRequest $request
     *
     * @return Response
     */
    public function store(CreatePresent_complaint_patientRequest $request)
    {
        $input = $request->all();
       /* echo "<pre>";
        print_r($request->complaint_pic); exit;*/
        $complaint_pic=array();
        if($files=$request->file('complaint_pic')){
           foreach($files as $file){
               $name=$file->getClientOriginalName();
               $path = public_path('/media/complaint_pic/');
               $file->move($path,$name);
               $complaint_pic[]=$name;
           }
            $input['complaint_pic'] = implode("|",$complaint_pic);

       }
        /* if($request->hasfile('complaint_pic'))
        {
            $image = $request->file('complaint_pic');
            $extension = $image->getClientOriginalExtension(); // getting image extension
            $filename =time().'__'.$image->getClientOriginalName();
            $path = public_path('/media/complaint_pic/');
            $image->move($path, $filename);
            $input['complaint_pic'] = $filename;
        }*/
         if($request->hasfile('complaint_pdf'))
        {
            $image = $request->file('complaint_pdf');
            $extension = $image->getClientOriginalExtension(); // getting image extension
            $filename =time().'__'.$image->getClientOriginalName();
            $path = public_path('/media/complaint_pdf/');
            $image->move($path, $filename);
            $input['complaint_pdf'] = $filename;
        }
         
        $input['created_by'] = Auth::user()->id;
        $input['updated_by'] = Auth::user()->id;
     
        $presentComplaintPatient = $this->presentComplaintPatientRepository->create($input);

        Flash::success('Present Complaint Patient saved successfully.');

        return redirect(route('presentComplaintPatients.index'));
    }

    /**
     * Display the specified Present_complaint_patient.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $presentComplaintPatient = $this->presentComplaintPatientRepository->find($id);

        if (empty($presentComplaintPatient)) {
            Flash::error('Present Complaint Patient not found');

            return redirect(route('presentComplaintPatients.index'));
        }

        return view('present_complaint_patients.show')->with('presentComplaintPatient', $presentComplaintPatient);
    }

    /**
     * Show the form for editing the specified Present_complaint_patient.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $presentComplaintPatient = $this->presentComplaintPatientRepository->find($id);

        if (empty($presentComplaintPatient)) {
            Flash::error('Present Complaint Patient not found');

            return redirect(route('presentComplaintPatients.index'));
        }

        return view('present_complaint_patients.edit')->with('presentComplaintPatient', $presentComplaintPatient);
    }

    /**
     * Update the specified Present_complaint_patient in storage.
     *
     * @param  int              $id
     * @param UpdatePresent_complaint_patientRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePresent_complaint_patientRequest $request)
    {
        $presentComplaintPatient = $this->presentComplaintPatientRepository->find($id);

        if (empty($presentComplaintPatient)) {
            Flash::error('Present Complaint Patient not found');

            return redirect(route('presentComplaintPatients.index'));
        }

        $presentComplaintPatient = $this->presentComplaintPatientRepository->update($request->all(), $id);

        Flash::success('Present Complaint Patient updated successfully.');

        return redirect(route('presentComplaintPatients.index'));
    }

    /**
     * Remove the specified Present_complaint_patient from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $presentComplaintPatient = $this->presentComplaintPatientRepository->find($id);

        if (empty($presentComplaintPatient)) {
            Flash::error('Present Complaint Patient not found');

            return redirect(route('presentComplaintPatients.index'));
        }

        $this->presentComplaintPatientRepository->delete($id);

        Flash::success('Present Complaint Patient deleted successfully.');

        return redirect(route('presentComplaintPatients.index'));
    }
}
