<?php

namespace App\Http\Controllers;

use App\DataTables\SpecialitiesDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateSpecialitiesRequest;
use App\Http\Requests\UpdateSpecialitiesRequest;
use App\Repositories\SpecialitiesRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Auth;

class SpecialitiesController extends AppBaseController
{
    /** @var  SpecialitiesRepository */
    private $specialitiesRepository;

    public function __construct(SpecialitiesRepository $specialitiesRepo)
    {
        $this->specialitiesRepository = $specialitiesRepo;
    }

    /**
     * Display a listing of the Specialities.
     *
     * @param SpecialitiesDataTable $specialitiesDataTable
     * @return Response
     */
    public function index(SpecialitiesDataTable $specialitiesDataTable)
    {
        return $specialitiesDataTable->render('specialities.index');
    }

    /**
     * Show the form for creating a new Specialities.
     *
     * @return Response
     */
    public function create()
    {
        return view('specialities.create');
    }

    /**
     * Store a newly created Specialities in storage.
     *
     * @param CreateSpecialitiesRequest $request
     *
     * @return Response
     */
    public function store(CreateSpecialitiesRequest $request)
    {
        $input = $request->all();
        $input['status'] = 1; 
        $input['created_by'] = Auth::user()->id;
        $input['updated_by'] = Auth::user()->id;
        $specialities = $this->specialitiesRepository->create($input);

        Flash::success('Specialities saved successfully.');

        if (Auth::user()->role_id == 15) {
            return redirect(route('hospital_specialities_list'));
        }
        return redirect(route('specialities.index'));
    }

    /**
     * Display the specified Specialities.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $specialities = $this->specialitiesRepository->find($id);

        if (empty($specialities)) {
            Flash::error('Specialities not found');

            return redirect(route('specialities.index'));
        }

        return view('specialities.show')->with('specialities', $specialities);
    }

    /**
     * Show the form for editing the specified Specialities.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $specialities = $this->specialitiesRepository->find($id);

        if (empty($specialities)) {
            Flash::error('Specialities not found');

            return redirect(route('specialities.index'));
        }

        return view('specialities.edit')->with('specialities', $specialities);
    }

    /**
     * Update the specified Specialities in storage.
     *
     * @param  int              $id
     * @param UpdateSpecialitiesRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSpecialitiesRequest $request)
    {
        $specialities = $this->specialitiesRepository->find($id);

        if (empty($specialities)) {
            Flash::error('Specialities not found');

            return redirect(route('specialities.index'));
        }

        $specialities = $this->specialitiesRepository->update($request->all(), $id);

        Flash::success('Specialities updated successfully.');

        if (Auth::user()->role_id == 15) {
            return redirect(route('hospital_specialities_list'));
        }

        return redirect(route('specialities.index'));
    }

    /**
     * Remove the specified Specialities from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $specialities = $this->specialitiesRepository->find($id);

        if (empty($specialities)) {
            Flash::error('Specialities not found');

            return redirect(route('specialities.index'));
        }

        $this->specialitiesRepository->delete($id);

        Flash::success('Specialities deleted successfully.');

        
        if (Auth::user()->role_id == 15) {
            return redirect(route('hospital_specialities_list'));
        }
        return redirect(route('specialities.index'));
    }
}
