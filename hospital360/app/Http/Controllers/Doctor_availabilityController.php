<?php

namespace App\Http\Controllers;

use App\DataTables\Doctor_availabilityDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateDoctor_availabilityRequest;
use App\Http\Requests\UpdateDoctor_availabilityRequest;
use App\Repositories\Doctor_availabilityRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Auth;
use DB;

class Doctor_availabilityController extends AppBaseController
{
    /** @var  Doctor_availabilityRepository */
    private $doctorAvailabilityRepository;

    public function __construct(Doctor_availabilityRepository $doctorAvailabilityRepo)
    {
        $this->doctorAvailabilityRepository = $doctorAvailabilityRepo;
    }

    /**
     * Display a listing of the Doctor_availability.
     *
     * @param Doctor_availabilityDataTable $doctorAvailabilityDataTable
     * @return Response
     */
    public function index(Doctor_availabilityDataTable $doctorAvailabilityDataTable)
    {
        return $doctorAvailabilityDataTable->render('doctor_availabilities.index');
    }

    /**
     * Show the form for creating a new Doctor_availability.
     *
     * @return Response
     */
    public function create()
    {
        if(Auth::user()->role_id == 3)
        {
            $doctorList = DB::table('doctor_details')->where('id',Auth::user()->userDetailsId)->pluck('first_name','id');
        }
        else
        {
            $doctorList = DB::table('doctor_details')->pluck('first_name','id');
        }
        return view('doctor_availabilities.create',compact('doctorList'));
    }

    /**
     * Store a newly created Doctor_availability in storage.
     *
     * @param CreateDoctor_availabilityRequest $request
     *
     * @return Response
     */
    public function store(CreateDoctor_availabilityRequest $request)
    {
        $input = $request->all();
        
        $input['created_by'] = Auth::user()->id;
        $input['updated_by'] = Auth::user()->id;

        $doctorAvailability = $this->doctorAvailabilityRepository->create($input);

        if(Auth::user()->role_id == 1){
            
           Flash::success('Doctor Non Availability saved successfully.');

        return redirect(route('doctorAvailabilities.index'));
        
        }else{

             Flash::success('Doctor Non Availability saved successfully.');
            return back();
        }
    }

    /**
     * Display the specified Doctor_availability.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        //$doctorAvailability = $this->doctorAvailabilityRepository->find($id);
        $doctorAvailability = DB::table('doctor_availability')->join('doctor_details','doctor_details.id','doctor_availability.doctor_id')->select('doctor_availability.*','doctor_details.first_name','doctor_details.last_name')->where('doctor_availability.id',$id)->first();
        if (empty($doctorAvailability)) {
            Flash::error('Doctor Non Availability not found');

            return redirect(route('doctorAvailabilities.index'));
        }

        return view('doctor_availabilities.show')->with('doctorAvailability', $doctorAvailability);
    }

    /**
     * Show the form for editing the specified Doctor_availability.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {

        $doctorAvailability = $this->doctorAvailabilityRepository->find($id);

        if(Auth::user()->role_id == 3)
        {
            $doctorList = DB::table('doctor_details')->where('id',Auth::user()->userDetailsId)->pluck('first_name','id');
        }
        else
        {
            $doctorList = DB::table('doctor_details')->pluck('first_name','id');
        }

        if (empty($doctorAvailability)) {
            Flash::error('Doctor Non Availability not found');

            return redirect(route('doctorAvailabilities.index'));
        }

        return view('doctor_availabilities.edit',compact('doctorList'))->with('doctorAvailability', $doctorAvailability);
    }

    /**
     * Update the specified Doctor_availability in storage.
     *
     * @param  int              $id
     * @param UpdateDoctor_availabilityRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDoctor_availabilityRequest $request)
    {
        $doctorAvailability = $this->doctorAvailabilityRepository->find($id);
        $input = $request->all();
        if (empty($doctorAvailability)) {
            Flash::error('Doctor Non Availability not found');

            return redirect(route('doctorAvailabilities.index'));
        }

        $doctorAvailability = $this->doctorAvailabilityRepository->update($input, $id);

        

        if(Auth::user()->role_id == 1){
            
           Flash::success('Doctor Non Availability updated successfully.');

        return redirect(route('doctorAvailabilities.index'));
        }else{

             Flash::success('Doctor Non Availability updated successfully.');
            return back();
        }
    }

    /**
     * Remove the specified Doctor_availability from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $doctorAvailability = $this->doctorAvailabilityRepository->find($id);

        if (empty($doctorAvailability)) {
            Flash::error('Doctor Non Availability not found');

            return redirect(route('doctorAvailabilities.index'));
        }

        $this->doctorAvailabilityRepository->delete($id);
        
        if(Auth::user()->role_id == 1){
            
          Flash::success('Doctor Non Availability deleted successfully.');

        return redirect(route('doctorAvailabilities.index'));
        
        }else{

             Flash::success('Doctor Non Availability deleted successfully.');
            return back();
        }

        
    }
}
