<?php

namespace App\Http\Controllers;

use App\DataTables\Order_360DataTable;
use App\Http\Requests;
use App\Http\Requests\CreateOrder_360Request;
use App\Http\Requests\UpdateOrder_360Request;
use App\Repositories\Order_360Repository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class Order_360Controller extends AppBaseController
{
    /** @var  Order_360Repository */
    private $order360Repository;

    public function __construct(Order_360Repository $order360Repo)
    {
        $this->order360Repository = $order360Repo;
    }

    /**
     * Display a listing of the Order_360.
     *
     * @param Order_360DataTable $order360DataTable
     * @return Response
     */
    public function index(Order_360DataTable $order360DataTable)
    {
        return $order360DataTable->render('order_360s.index');
    }

    /**
     * Show the form for creating a new Order_360.
     *
     * @return Response
     */
    public function create()
    {
        return view('order_360s.create');
    }

    /**
     * Store a newly created Order_360 in storage.
     *
     * @param CreateOrder_360Request $request
     *
     * @return Response
     */
    public function store(CreateOrder_360Request $request)
    {
        $input = $request->all();

        $order360 = $this->order360Repository->create($input);

        Flash::success('Order 360 saved successfully.');

        return redirect(route('order360s.index'));
    }

    /**
     * Display the specified Order_360.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $order360 = $this->order360Repository->find($id);

        if (empty($order360)) {
            Flash::error('Order 360 not found');

            return redirect(route('order360s.index'));
        }

        return view('order_360s.show')->with('order360', $order360);
    }

    /**
     * Show the form for editing the specified Order_360.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $order360 = $this->order360Repository->find($id);

        if (empty($order360)) {
            Flash::error('Order 360 not found');

            return redirect(route('order360s.index'));
        }

        return view('order_360s.edit')->with('order360', $order360);
    }

    /**
     * Update the specified Order_360 in storage.
     *
     * @param  int              $id
     * @param UpdateOrder_360Request $request
     *
     * @return Response
     */
    public function update($id, UpdateOrder_360Request $request)
    {
        $order360 = $this->order360Repository->find($id);

        if (empty($order360)) {
            Flash::error('Order 360 not found');

            return redirect(route('order360s.index'));
        }

        $order360 = $this->order360Repository->update($request->all(), $id);

        Flash::success('Order 360 updated successfully.');

        return redirect(route('order360s.index'));
    }

    /**
     * Remove the specified Order_360 from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $order360 = $this->order360Repository->find($id);

        if (empty($order360)) {
            Flash::error('Order 360 not found');

            return redirect(route('order360s.index'));
        }

        $this->order360Repository->delete($id);

        Flash::success('Order 360 deleted successfully.');

        return redirect(route('order360s.index'));
    }
}
