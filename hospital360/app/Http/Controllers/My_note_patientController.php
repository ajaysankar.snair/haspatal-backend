<?php

namespace App\Http\Controllers;

use App\DataTables\My_note_patientDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateMy_note_patientRequest;
use App\Http\Requests\UpdateMy_note_patientRequest;
use App\Repositories\My_note_patientRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Auth;
use Illuminate\Http\Request;
use DB;
use PDF;
class My_note_patientController extends AppBaseController
{
    /** @var  My_note_patientRepository */
    private $myNotePatientRepository;

    public function __construct(My_note_patientRepository $myNotePatientRepo)
    {
        $this->myNotePatientRepository = $myNotePatientRepo;
    }

    /**
     * Display a listing of the My_note_patient.
     *
     * @param My_note_patientDataTable $myNotePatientDataTable
     * @return Response
     */
    public function index(My_note_patientDataTable $myNotePatientDataTable)
    {
        return $myNotePatientDataTable->render('my_note_patients.index');
    }

    /**
     * Show the form for creating a new My_note_patient.
     *
     * @return Response
     */
    public function create()
    {
        return view('my_note_patients.create');
    }

    /**
     * Store a newly created My_note_patient in storage.
     *
     * @param CreateMy_note_patientRequest $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $patient = DB::table('patient_details')->where('id',$input['my_patient_id'])->select('first_name')->first();
        $doctor = DB::table('doctor_details')->where('id',$input['my_doctor_id'])->select('first_name')->first();
      
        $data = [
          'title' => 'Summary',
          'heading' => env('APP_URL').'/public/image/haspatallogo.png',
          'my_patient_id' => $patient->first_name,       
          'my_doctor_id' => $doctor->first_name,       
          'm_book_id' => $input['m_book_id'],       
          'my_note' => $input['my_note'],       
            ];
        
        $pdf = PDF::loadView('summary_pdf_view', $data); 
        //return view('pdf_view', compact('data'));
        
        $path = public_path('summary/');
        $fileName1 =   time() . '__'. 'summary.' . 'pdf' ;
        $pdf->save($path . '/' . $fileName1); 

        $input['s_pdf'] = 'public/summary/'.$fileName1;

        $input['created_by'] = Auth::user()->id;
        $input['updated_by'] = Auth::user()->id;
        
        $myNotePatient = $this->myNotePatientRepository->create($input);

       

        if(Auth::user()->role_id == 1){
             Flash::success('My Note Patient saved successfully.');

            return redirect(route('myNotePatients.index'));
        }else{
            Flash::success('My Note saved successfully.');
            return back();
        }
    }

    /**
     * Display the specified My_note_patient.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $myNotePatient = $this->myNotePatientRepository->find($id);

        if (empty($myNotePatient)) {
            Flash::error('My Note Patient not found');

            return redirect(route('myNotePatients.index'));
        }

        return view('my_note_patients.show')->with('myNotePatient', $myNotePatient);
    }

    /**
     * Show the form for editing the specified My_note_patient.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $myNotePatient = $this->myNotePatientRepository->find($id);

        if (empty($myNotePatient)) {
            Flash::error('My Note Patient not found');

            return redirect(route('myNotePatients.index'));
        }

        return view('my_note_patients.edit')->with('myNotePatient', $myNotePatient);
    }

    /**
     * Update the specified My_note_patient in storage.
     *
     * @param  int              $id
     * @param UpdateMy_note_patientRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMy_note_patientRequest $request)
    {
        $myNotePatient = $this->myNotePatientRepository->find($id);

        if (empty($myNotePatient)) {
            Flash::error('My Note Patient not found');

            return redirect(route('myNotePatients.index'));
        }

        $myNotePatient = $this->myNotePatientRepository->update($request->all(), $id);

        Flash::success('My Note Patient updated successfully.');

        return redirect(route('myNotePatients.index'));
    }

    /**
     * Remove the specified My_note_patient from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $myNotePatient = $this->myNotePatientRepository->find($id);

        if (empty($myNotePatient)) {
            Flash::error('My Note Patient not found');

            return redirect(route('myNotePatients.index'));
        }

        $this->myNotePatientRepository->delete($id);

        Flash::success('My Note Patient deleted successfully.');

        return redirect(route('myNotePatients.index'));
    }
}
