<?php

namespace App\Http\Controllers;

use App\DataTables\DoctorsDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateDoctorsRequest;
use App\Http\Requests\UpdateDoctorsRequest;
use App\Repositories\DoctorsRepository;
use App\User;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Auth;
use DB;
use Validator;
use Illuminate\Http\Request;
class commoncontroller extends Controller
{
    //
    
          /** @var  DoctorsRepository */
    private $doctorsRepository;

    public function __construct(DoctorsRepository $doctorsRepo)
    {
        $this->doctorsRepository = $doctorsRepo;
    }

    
    public function dr_active($id)
    {

      $booklist =  DB::table('users')->where('userDetailsId',$id)->where('user_type',1)->update(['status' => 1]);

      $booklist =  DB::table('doctor_details')->where('id',$id)->update(['status' => 1]);

      Flash::success('Doctor updated successfully.');

      return back();

    }
    public function dr_deactive($id)
    {
      $booklist =  DB::table('users')->where('userDetailsId',$id)->where('user_type',1)->update(['status' => 0]);
      
      $booklist =  DB::table('doctor_details')->where('id',$id)->update(['status' => 0]);

      Flash::success('Doctor updated successfully.');
      return back();

    }
    
    /**
     * Display the specified Doctors.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        //$doctors = $this->doctorsRepository->find($id);
        $doctors = DB::table('doctor_details')
        ->leftJoin('state','state.id','doctor_details.state')
        ->leftJoin('district','district.id','doctor_details.district')
        ->leftJoin('city','city.id','doctor_details.city')
        ->leftJoin('language','language.id','doctor_details.language')
        ->leftJoin('specilities','specilities.id','doctor_details.speciality')
        //->leftJoin('licence_type','licence_type.id','doctor_details.licence_type')
        ->select('doctor_details.*','state.state','district.district_name','city.city','language.language','specilities.specility')
        ->where('doctor_details.id',$id)->first();
        /*if (empty($doctors)) {
            Flash::error('Doctors not found');

            return redirect(route('doctors.index'));
        }
*/
    return view('doctors.show')->with('doctors', $doctors);
        
    }
    
    
        /**
     * Show the form for editing the specified Doctors.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
      
        $doctors = $this->doctorsRepository->find($id);
        $countryList = DB::table('country')->pluck('country','id');
        $stateList = DB::table('state')->pluck('state','id');
        $cityList = DB::table('city')->pluck('city','id');
        $languageList = DB::table('language')->pluck('language','id');
        $specilities= DB::table('specilities')->pluck('specility','id');
        $licenceTyes= DB::table('licence_type')->pluck('licence_name','id');
        $plan= DB::table('plan_master')->pluck('pl_name','pl_id');
        if (empty($doctors)) {
            Flash::error('Doctors not found');

            return redirect(route('doctors.index'));
        }

        return view('doctors.edit',compact('countryList','stateList','cityList','languageList','specilities','licenceTyes','plan'))->with('doctors', $doctors);
    }

    /**
     * Update the specified Doctors in storage.
     *
     * @param  int              $id
     * @param UpdateDoctorsRequest $request
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        
        $doctors = $this->doctorsRepository->find($id);
        $input = $request->all();
        if (empty($doctors)) {
            Flash::error('Doctors not found');

            return redirect(route('doctors.index'));
        }
        if($request->hasfile('licence_copy'))
        {
            $image = $request->file('licence_copy');
            $extension = $image->getClientOriginalExtension(); // getting image extension
            $media_photos =time().'__'.$image->getClientOriginalName();
            $path = public_path('/media/licence_copy/');
            $image->move($path, $media_photos);
            $input['licence_copy']= $media_photos;

        }else{
            $input['licence_copy']=$doctors->licence_copy;
        }
        if($request->hasfile('profile_pic'))
        {
            $image = $request->file('profile_pic');
            $extension = $image->getClientOriginalExtension(); // getting image extension
            $media_photos =time().'__'.$image->getClientOriginalName();
            $path = public_path('/media/profile_pic/');
            $image->move($path, $media_photos);
            $input['profile_pic']= $media_photos;

        }else{
            $input['profile_pic']=$doctors->profile_pic;
        } 
        if($request->hasfile('clinic_logo'))
        {
            $image = $request->file('clinic_logo');
            $extension = $image->getClientOriginalExtension(); // getting image extension
            $media_photos =time().'__'.$image->getClientOriginalName();
            $path = public_path('/media/clinic_logo/');
            $image->move($path, $media_photos);
            $input['clinic_logo']= $media_photos;

        }else{
            $input['clinic_logo']=$doctors->clinic_logo;
        }
        if($request->hasfile('cheque_img'))
        {
            $image = $request->file('cheque_img');
            $extension = $image->getClientOriginalExtension(); // getting image extension
            $media_photos =time().'__'.$image->getClientOriginalName();
            $path = public_path('/media/cheque_img/');
            $image->move($path, $media_photos);
            $input['cheque_img']= $media_photos;

        }else{
            $input['cheque_img']=$doctors->cheque_img;
        }
        
         if ($request->has('price')) {

            $input['price']= $request->price;
            $user = DB::table('users')->where('user_type',1)->where('userDetailsId', Auth::user()->userDetailsId)->update(['doctor_login_status_mobile'=> 1]);
            
            /*$dr_old_price = DB::table('doctor_details')->where('id',Auth::user()->userDetailsId)->orderBy('id', 'desc')->first(); 
            
            if ($dr_old_price->price == $request->price) {
            }else{

                $admin_price = DB::table('booking_price_admin')->orderBy('id', 'desc')->first();
                $input['price']= $request->price + $admin_price->book_price; 
                
            }*/


        }


        if(isset($request->language))
        {
          
          $input['language'] = implode(',',$request->language);
        }
        if(isset($request->speciality))
        {
          
          $input['speciality'] = implode(',',$request->speciality);
        }
        $doctors = $this->doctorsRepository->update($input, $id);
        if(Auth::user()->role_id == 1){
            Flash::success('Updated successfully.');
            return redirect(route('doctors.index'));
        }else{
            Flash::success('Updated successfully.');
            return back();
        }
    }
}
