<?php

namespace App\Http\Controllers;

use App\DataTables\PatientsDataTable;
use App\Http\Requests;
use App\Http\Requests\CreatePatientsRequest;
use App\Http\Requests\UpdatePatientsRequest;
use App\Repositories\PatientsRepository;
use App\User;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Auth;
use DB;
use Illuminate\Http\Request;
use App\Imports\PatientImport;
use Maatwebsite\Excel\Facades\Excel;

class PatientsController extends AppBaseController
{
    /** @var  PatientsRepository */
    private $patientsRepository;

    public function __construct(PatientsRepository $patientsRepo)
    {
        $this->patientsRepository = $patientsRepo;
    }

    /**
     * Display a listing of the Patients.
     *
     * @param PatientsDataTable $patientsDataTable
     * @return Response
     */
    public function index(PatientsDataTable $patientsDataTable)
    {
        return $patientsDataTable->render('patients.index');
    }

    /**
     * Show the form for creating a new Patients.
     *
     * @return Response
     */
    public function create()
    {
        $countryList = \App\Models\Country::orderBy('country','asc')->pluck('country','id');
        $stateList = \App\Models\State::orderBy('state','asc')->pluck('state','id');
        //$cityList = \App\Models\City::orderBy('city','asc')->pluck('city','id');
        $languageList = DB::table('language')->orderBy('language','asc')->pluck('language','id');
        $districtList = DB::table('district')->orderBy('district_name','asc')->pluck('district_name','id');
        $cityList = DB::table('city')->orderBy('city','asc')->pluck('city','id');
        $pincodeList = DB::table('pincode')->orderBy('pincode','asc')->pluck('pincode','id');

        return view('patients.create',compact('countryList','stateList','cityList','languageList','districtList'))->with('districtList',$districtList);
    }

    /**
     * Store a newly created Patients in storage.
     *
     * @param CreatePatientsRequest $request
     *
     * @return Response
     */
    public function store(CreatePatientsRequest $request)
    {
        $input = $request->all();
        if($request->hasfile('profile_pic'))
        {
            $image = $request->file('profile_pic');
            $extension = $image->getClientOriginalExtension(); // getting image extension
            $filename =time().'__'.$image->getClientOriginalName();
            $path = public_path('/media/profile_pic/');
            $image->move($path, $filename);
            $input['profile_pic'] = $filename;
        }
         $p_data =  DB::table('patient_details')->orderBy('id', 'desc')->first();
        if($p_data->p_uni_id != ''){
          $bookdata = explode('-', $p_data->p_uni_id); 
          $dd = $bookdata['2'] + 1;   
          $input['p_uni_id'] = $bookdata['0'].'-'.$bookdata['1'].'-'.'000000'.$dd;
        }else{
          $input['p_uni_id'] = '91-P-0000001';
        }
        $input['created_by'] = Auth::user()->id;
        $input['updated_by'] = Auth::user()->id;
        
        $patients = $this->patientsRepository->create($input);

        $user = User::create(['first_name' => $request['first_name'],
                                  'last_name' => $request['last_name'],
                                  'email' => $request['email'],
                                  'role_id' => 2,
                                  'user_type' => 2,
                                  'userDetailsId'=>$patients->id,
                                  'patient_login_status_addressinfo'=>1,
                                  'password' => bcrypt($request['password']),
                                  ]);
            $user->attachRole(2);

        Flash::success('Patients saved successfully.');

        return redirect(route('patients.index'));
    }

    /**
     * Display the specified Patients.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        //$patients = $this->patientsRepository->find($id);

        $patients = DB::table('patient_details')->leftJoin('country','country.id','patient_details.country')
        ->leftJoin('state','state.id','patient_details.state')
        ->leftJoin('city','city.id','patient_details.city')
        ->leftJoin('language','language.id','patient_details.language')
        ->select('patient_details.*','country.country','state.state','city.city','language.language')
        ->where('patient_details.id',$id)->first();

        //echo "<pre>";print_r($patients);exit();

        if (empty($patients)) {
            Flash::error('Patients not found');

            return redirect(route('patients.index'));
        }

        return view('patients.show')->with('patients', $patients);
    }

    /**
     * Show the form for editing the specified Patients.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $patients = $this->patientsRepository->find($id);
        $countryList = DB::table('country')->pluck('country','id');
        $stateList = DB::table('state')->pluck('state','id');
        $cityList = DB::table('city')->pluck('city','id');
        $languageList = DB::table('language')->pluck('language','id');
        if (empty($patients)) {
            Flash::error('Patients not found');

            return redirect(route('patients.index'));
        }

        return view('patients.edit',compact('countryList','stateList','cityList','languageList'))->with('patients', $patients);
    }

    /**
     * Update the specified Patients in storage.
     *
     * @param  int              $id
     * @param UpdatePatientsRequest $request
     *
     * @return Response
     */
    public function update($id,Request $request)
    {
        $patients = $this->patientsRepository->find($id);

        if (empty($patients)) {
            Flash::error('Patients not found');

            return redirect(route('patients.index'));
        }

        $patients = $this->patientsRepository->update($request->all(), $id);

        Flash::success('Patients updated successfully.');

        return redirect(route('patients.index'));
    }

    /**
     * Remove the specified Patients from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $patients = $this->patientsRepository->find($id);
        $u_patients = DB::table('users')->where('userDetailsId',$id)->delete();

        if (empty($patients)) {
            Flash::error('Patients not found');

            return redirect(route('patients.index'));
        }

        $this->patientsRepository->delete($id);

        Flash::success('Patients deleted successfully.');

        return redirect(route('patients.index'));
    }

    public function patientsImportPost(Request $request)
    {
        Excel::import(new PatientImport, request()->file('patient'));
        
        return redirect(route('patients.index'))->with('success', 'All good!');
    }
}
