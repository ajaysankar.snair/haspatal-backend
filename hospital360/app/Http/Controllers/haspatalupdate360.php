<?php

namespace App\Http\Controllers;
use App\Http\Requests\CreateHaspatal_360_registerRequest;
use App\Http\Requests\API\CreateStateAPIRequest;
use App\Http\Requests\API\UpdateStateAPIRequest;
use App\Models\State;
use App\Repositories\StateRepository;
use App\Repositories\Business_registerRepository;
use Illuminate\Http\Request;
use App\Models\Service_details;
use App\Models\Your_coverage_360;
use App\Models\Haspatal_360_register;
use App\coupon;
use App\User;
use App\Models\City;
use App\couponrate;
use App\Models\Coupan;
use App\Models\Create_offer_360;
use App\Models\Business_register;
use App\Http\Controllers\AppBaseController;
use Response;
use App\Models\Doctors;
use DB;




class haspatalupdate360 extends Controller
{
    //
    
    public function haspatal360Coverage(Request $request)
    {
        $Your_coverage_360 = new Your_coverage_360();
        $Your_coverage_360->user_id = $request->input('user_id');
        $Your_coverage_360->c_pincode = $request->input('pincode');
        
        $Your_coverage_360->save();
         $pincodes = Your_coverage_360::select('c_pincode')->where('user_id', '=', $request->input('user_id'))->get();
         $affected = DB::table('business_register')->where('user_id',$request->input('user_id'))->update(['your_coverage_status' => 1]);
        if( $Your_coverage_360){
         return response()->json(["Your_coverage_360" => $Your_coverage_360,"massage"=>"updated  successfully","data" => $pincodes]);
    }


    }
    public function singleDoctor(Request $request)
    {
        $data = Doctors::select('*')->where('id', $request->dr)->first();
      

        
        return response()->json(["Doctor" => $data]);
    }
    public function haspatalType(Request $request)
    {
      
        $data = Doctors::where('id',$request->dr)->update(['appointment_type' => $request->appointment_type]);

        
        return response()->json(["Doctor" => $data]);
    }
    public function updationAvailable(Request $request)
    {
         $version=12;
         return response()->json(["UpdateVersion" => $version,"message"=>"New Update Available"]);
    



    }
    public function updationAvailableDoctor(Request $request)
    {
         $version=2;
         return response()->json(["UpdateVersion" => $version,"message"=>"New Update Available"]);
    



    }
    public function otpverification(Request $request)
    {
        $users = User::where('id', $request->user_id)->orderBy('id', 'desc')
        ->first();
        if( $users){
          $users->reg_status="17";
          $users->save();  
      }
         return response()->json(["users" => $users,"message"=>"Otp Validated"]);
    


    }
    public function sendmai(Request $request)
    {
      $data['fname']="ajay";
        $year = date('Y');
        $otp = mt_rand(1000,9999);
        $mailmessage= $data['fname']."please verify your email id with the OTP :". $otp;
       
        $data['personalizations'] = array("0" => array('to' => array('0' => array('email' => "ajaysankar.snair@gmail.com")),"dynamic_template_data"=> array("name"=>$mailmessage,"year"=> "2021")));


        $data['from'] = array('email' => 'system@haspatal.com');

        //$data['subject'] = "Registration successful";
        $data['template_id'] = "d-c19ea64f41ea4c48ae28b3625202ed9e";
        //$data['content'] = array("0" =>array('type' =>' text/html','value' => "<DOCTYPE html><html lang='en-US'><head><meta charset='utf-8'></head><body><h2>Hi ".$user->first_name.", we’re glad you’re here! Following are your account details: <br> </h3> <h3>Email: </h3><p>".$user->email."</p> </body> </html>" ));

        //echo json_encode($data);exit();

        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://api.sendgrid.com/v3/mail/send",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => json_encode($data),
          CURLOPT_HTTPHEADER => array(
            "authorization: Bearer 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",
            "cache-control: no-cache",
            "content-type: application/json",
            "postman-token: b8c0781f-af48-90cc-1c5b-76f80e0b4be0"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          echo "cURL Error #:" . $err;
        } else {
            return response()->json(["Your_coverage_360" => $response,"massage"=>"updated  successfully"]);
        }
    }
    public function pharmacysearch(Request $request)
    {
        $data = Your_coverage_360::leftJoin('business_register', 'Your_coverage_360.user_id', '=', 'business_register.user_id')->join('users', 'users.id', '=', 'business_register.user_id')
       
        ->where('Your_coverage_360.c_pincode',$request->input('pincode'))
        ->where('business_register.admin_status',1)
        ->where('business_register.b_id',$request->input('role_id'))
        ->where('business_register.your_coverage_status',1)
        ->select('business_register.*')
        ->groupBy('Your_coverage_360.user_id')
        ->orderBy('updated_at','DESC')->get();
        // $data = Business_register::select('*')->where('b_id', $request->input('role_id'))->where('pincode', $request->input('pincode'))->where('admin_status',"1")->get();

        foreach ($data as $Business_register) {
            $user_id = $Business_register->user_id;
            $path = [
              "b_lic" => 'https://app.kizakuhaspatal.co.in/media/b_lic/' . $Business_register->b_lic,
              "b_card_pic" =>
              'https://app.kizakuhaspatal.co.in/media/b_card_pic/' . $Business_register->b_card_pic,
              "shop_pic" =>
              'https://app.kizakuhaspatal.co.in/media/shop_pic/' . $Business_register->shop_pic,
          ];
          $Business_register['path']=$path;
            $users = DB::table('users')
                ->where('id', $user_id)
                ->orderBy('id', 'desc')
                ->first();
                $create_offer_360s = Create_offer_360::select('*')->where('user_id', $user_id)
                ->get();
                
                $myFavourite1 = DB::table('my_favourite')->where('pharmacy_id',$Business_register->id)->where('patient_id',$request->input('patient_id'))->first();
                /*echo "<pre>";
                print_r($myFavourite1->patient_id); exit;*/
                if($myFavourite1){
                if(isset($myFavourite1->pharmacy_id) == $Business_register->id && isset($myFavourite1->patient_id) == $request->input('patient_id'))
                {
                    
                    $Business_register['myFavourite1_status']=$myFavourite1->status;
                }else{
                    //return $this->sendResponse('', 'My Favourite alredy add successfully');
                    $Business_register['myFavourite1_status']=0;
                }

            }
            else{
                   
                $Business_register['myFavourite1_status']=0;
            }
                $Business_register['user']=$users;
                $Business_register['offers']=$create_offer_360s;
            if ($users) {
                $role_id = $users->role_id;
                $role = DB::table('couponrates')
                    ->where('category_id', $role_id)
                    ->orderBy('id', 'desc')
                    ->first();
                if ($role) {
                    $Business_register['type'] = $role->name;
                  
                    
                }
            }
        }
      
        return response()->json(["data" => $data,"message"=>"Retireved Sucessfully"]);
    }

    public function singleBusiness(Request $request){
            // $data=$request->input('product_id');
            $data = DB::table('business_register')
            ->where('id', $request->input('business_id'))
            ->orderBy('user_id', 'desc')
            ->first();
        $user_id = $data->user_id;

        $users = DB::table('users')
            ->where('id', $user_id)
            ->orderBy('id', 'desc')
            ->first();
            $create_offer_360s = Create_offer_360::select('*')->where('user_id', $user_id)
            ->get();
            $data->offers=$create_offer_360s;
            $data->users=$users;
        // $user =  User::select('*')->where(['id' => $request->input('product_id')]);
        $role_id = $users->role_id;
        $role = DB::table('couponrates')
            ->where('category_id', $role_id)
            ->orderBy('id', 'desc')
            ->first();
        // dd($role);
        $type = $role->name;
        // $data = Business_register::where('user_id',$request->user_id)
        //                         ->join('country', 'business_register.country_id', '=', 'country.id')
        //                         ->join('state', 'business_register.state_id', '=', 'state.id')
        //                         ->join('city', 'business_register.city_id', '=', 'city.id')
        //                         ->select('business_register.*','country.country','state.state','city.city')
        //                         ->first();
        if($data->city_id){
          $city = DB::table('city')
            ->where('id', $data->city_id)
            ->orderBy('id', 'desc')
            ->first();
            if( $city){
                $data->cityname = $city->city;
            }
            
    }else{
        $data->cityname="Not Selected"  ;
    }
    if($data->state_id){
        $state = DB::table('state')
            ->where('id', $data->state_id)
            ->orderBy('id', 'desc')
            ->first();
       
        $data->statename = $state->state;
    }else{
        $data->statename="Not Selected"  ;
    }
        $data->type = $type;
        // dd($data);
        $path = [
            "b_lic" => 'https://app.kizakuhaspatal.co.in/media/b_lic/' . $data->b_lic,
            "b_card_pic" =>
            'https://app.kizakuhaspatal.co.in/media/b_card_pic/' . $data->b_card_pic,
            "shop_pic" =>
            'https://app.kizakuhaspatal.co.in/media/shop_pic/' . $data->shop_pic,
        ];
        //  dd($path);
        if ($data) {
            return response()->json(["data" => $data,"message"=>"Retireved Sucessfully","path" => $path]);
        }
    }


    public function uploadfiledoctorprofile(Request $request)
    {
        $input = $request->all();

        if($request->hasfile('dr_pic'))
        {
            $image = $request->file('dr_pic');
            $extension = $image->getClientOriginalExtension(); // getting image extension
            $filename =time().'__'.$image->getClientOriginalName();
            $path = public_path('/media/profile_pic/');
            $image->move($path, $filename);
            $filename1 = $filename;
        }else
        {
            $image=$input['dr_pic'];  
            $image = str_replace(' ', '+', $image);
            $imageName = str_random(10) . '.png';
            $data = base64_decode($image);
            $path = public_path('/media/profile_pic/' . $imageName);
            file_put_contents($path, $data);
            $filename1 = $imageName;
        
        }
     return response()->json( $filename1,200);
    }
    public function uploadfiledoctorclinic(Request $request)
    {
        $input = $request->all();

        if($request->hasfile('clinic_logo'))
        {
            $image = $request->file('clinic_logo');
            $extension = $image->getClientOriginalExtension(); // getting image extension
            $filename =time().'__'.$image->getClientOriginalName();
            $path = public_path('/media/clinic_logo/');
            $image->move($path, $filename);
            $filename1 = $filename;
        }else
        {
            $image=$input['clinic_logo'];  
            $image = str_replace(' ', '+', $image);
            $imageName = str_random(10) . '.png';
            $data = base64_decode($image);
            $path = public_path('/media/clinic_logo/' . $imageName);
            file_put_contents($path, $data);
            $filename1 = $imageName;
        
        }
     return response()->json( $filename1,200);
    }
    public function uploadfilewallpaper(Request $request)
    {
        $input = $request->all();

        if($request->hasfile('clinic_wallpaper'))
        {
            $image = $request->file('clinic_wallpaper');
            $extension = $image->getClientOriginalExtension(); // getting image extension
            $filename =time().'__'.$image->getClientOriginalName();
            $path = public_path('/media/clinic_wallpapper/');
            $image->move($path, $filename);
            $filename1 = $filename;
        }else
        {
            $image=$input['clinic_wallpaper'];  
            $image = str_replace(' ', '+', $image);
            $imageName = str_random(10) . '.png';
            $data = base64_decode($image);
            $path = public_path('/media/clinic_wallpapper/' . $imageName);
            file_put_contents($path, $data);
            $filename1 = $imageName;
        
        }
     return response()->json( $filename1,200);
    }
    public function uploadfilelicense(Request $request)
    {
        $input = $request->all();

        if($request->hasfile('license_pic'))
        {
            $image = $request->file('license_pic');
            $extension = $image->getClientOriginalExtension(); // getting image extension
            $filename =time().'__'.$image->getClientOriginalName();
            $path = public_path('/media/licence_copy/');
            $image->move($path, $filename);
            $filename1 = $filename;
        }else
        {
            $image=$input['license_pic'];  
            $image = str_replace(' ', '+', $image);
            $imageName = str_random(10) . '.png';
            $data = base64_decode($image);
            $path = public_path('/media/licence_copy/' . $imageName);
            file_put_contents($path, $data);
            $filename1 = $imageName;
        
        }
     return response()->json( $filename1,200);
    }
    public function uploadfileb_card_pic(Request $request)
    {
        $input = $request->all();

        if($request->hasfile('b_card_pic'))
        {
            $image1 = $request->file('b_card_pic');
            $extension = $image->getClientOriginalExtension(); // getting image extension
            $filename =time().'__'.$image->getClientOriginalName();
            $path = public_path('/media/b_card_pic/');
            $image->move($path, $filename);
            $filename1 = $filename;
        }else
        {
            $image=$input['b_card_pic'];  
            $image = str_replace(' ', '+', $image);
            $imageName = str_random(10) . '.png';
            $data = base64_decode($image);
            $path = public_path('/media/b_card_pic/' . $imageName);
            file_put_contents($path, $data);
            $filename1 = $imageName;
        
        }
     return response()->json( $filename1,200);
    }
    public function uploadfilebillCopy(Request $request)
    {
        $input = $request->all();

        if($request->hasfile('biil_copy'))
        {
            $image1 = $request->file('biil_copy');
            $extension = $image->getClientOriginalExtension(); // getting image extension
            $filename =time().'__'.$image->getClientOriginalName();
            $path = public_path('/media/prescription_image/');
            $image->move($path, $filename);
            $filename1 = $filename;
        }else
        {
            $image=$input['biil_copy'];  
            $image = str_replace(' ', '+', $image);
            $imageName = str_random(10) . '.png';
            $data = base64_decode($image);
            $path = public_path('/media/prescription_image/' . $imageName);
            file_put_contents($path, $data);
            $filename1 = $imageName;
        
        }
     return response()->json( $filename1,200);
    }
    public function uploadfileb_lic(Request $request)
    {
        $input = $request->all();
      
        if($request->hasfile('b_lic'))
        {
            $image1 = $request->file('b_lic');
            $extension = $image->getClientOriginalExtension(); // getting image extension
            $filename =time().'__'.$image->getClientOriginalName();
            $path1 = public_path('/media/b_lic/');
            $image->move($path, $filename);
            $filename1 = $filename;
        }else
        {
            $image=$input['b_lic'];  
            $image = str_replace(' ', '+', $image);
            $imageName = str_random(10) . '.png';
            $data = base64_decode($image);
            $path = public_path('/media/b_lic/' . $imageName);
            file_put_contents($path, $data);
            $filename1 = $imageName;
        
        }
     return response()->json( $filename1,200);
    }
    public function uploadfileshop_pic(Request $request)
    {
        $input = $request->all();
      
        if($request->hasfile('shop_pic'))
        {
            $image1 = $request->file('shop_pic');
            $extension = $image->getClientOriginalExtension(); // getting image extension
            $filename =time().'__'.$image->getClientOriginalName();
            $path1 = public_path('/media/shop_pic/');
            $image->move($path, $filename);
            $filename1 = $filename;
        }else
        {
            $image=$input['shop_pic'];  
            $image = str_replace(' ', '+', $image);
            $imageName = str_random(10) . '.png';
            $data = base64_decode($image);
            $path = public_path('/media/shop_pic/' . $imageName);
            file_put_contents($path, $data);
            $filename1 = $imageName;
        
        }
     return response()->json( $filename1,200);
    }
    public function uploadfiledocuments(Request $request)
    {
    //     $input = $request->all();
      
    //    if($input['documents']){
    //         $image=$input['documents'];  
    //         $image = str_replace(' ', '+', $image);
    //         $imageName = str_random(10) . '.png';
    //         $data = base64_decode($image);
    //         $path = public_path('/media/documents_patient/' . $imageName);
    //         file_put_contents($path, $data);
    //         $filename1 = $imageName;
    //         return response()->json( $filename1,200);
    //    }else{
    //     return response()->json( "Something wrong",200);
    //    }
       $input = $request->all();
      
       if($request->hasfile('patient_document'))
       {
           $image1 = $request->file('patient_document');
           $extension = $image->getClientOriginalExtension(); // getting image extension
           $filename =time().'__'.$image->getClientOriginalName();
           $path1 = public_path('/media/prescription_image/');
           $image->move($path, $filename);
           $filename1 = $filename;
       }else
       {
           $image=$input['patient_document'];  
           $image = str_replace(' ', '+', $image);
           $imageName = str_random(10) . '.png';
           $data = base64_decode($image);
           $path = public_path('/media/prescription_image/' . $imageName);
           file_put_contents($path, $data);
           $filename1 = $imageName;
       
       }
    return response()->json( $filename1,200);
    
    }
    public function patient_documents_details(Request $request)
    {
        $input = $request->all();
      
       $patient_documets= DB::table('Prescription_document')->insert($input);
       if($patient_documets==true){
        $Prescription_document =  DB::table('Prescription_document')
        ->where('doctor_id',$request->doctor_id) ->where('Patient_id',$request->Patient_id)->get();
        $path = env('APP_URL').'media/prescription_image';
     
    
        // return response()->json( $Prescription_document,200);
        return response()->json(["path" => $path,"data" => $Prescription_document]);
       }
       
    }
    public function patient_documen_doctor(Request $request)
    {
       
        $Prescription_document =  DB::table('Prescription_document')
        ->where('doctor_id',$request->doctor_id) ->where('Patient_id',$request->Patient_id)->get();
        $path = env('APP_URL').'media/prescription_image';
     
    
        // return response()->json( $Prescription_document,200);
        return response()->json(["path" => $path,"data" => $Prescription_document]);
      
       
    }
    public function promcodeverify(Request $request)
    {
        // $data = $request->input();
        $coupon = coupan::where('promo_code', '=', $request->promo_code)->first();
        if($coupon){
          $applied_coupon=  DB::table('applied_coupan')
            ->where('dr_id',$request->doctor_id) ->where('patient_id',$request->Patient_id) ->where('promo_code',$request->promo_code)->first();  
            if($applied_coupon){
                return response()->json(["status" => 0,"massage" => "promo_code Already Applied"]);
            }
            $applied_coupon_save =  DB::table('applied_coupan')->insert(
                ['patient_id' => $request->Patient_id, 'dr_id' => $request->doctor_id, 'promo_code' => $request->promo_code, 'price' => $coupon->price, 'updated_at' =>date('Y-m-d H:i:s')]
            );
            return response()->json(["status" => 1,"massage" => "Applied Sucessfully","result" =>$applied_coupon_save,"coupon" =>$coupon,]);
        }else{
            return response()->json(["status" => 0,"massage" => "promo_code not Found"]);

        }

      
       
    }
}
