<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\User;
use Validator;
use App\Models\BookingRequest;
use App\coupon;
use App\Models\Your_coverage_360;
use DB;
use App\Models\student;
use Illuminate\Support\Facades\Password;

class Elicinc extends Controller
{
    //

    public function patient_login_Elinic(Request $request){

  
      
        $userData= User::select('*')->where('mobile', $request->mobile)->first();
        if($userData){
        $userInformation = DB::table('patient_details')
                            ->leftJoin('country','country.id','patient_details.country')
                            ->leftJoin('state','state.id','patient_details.state')
                            ->leftJoin('city','city.id','patient_details.city')
                            ->where('patient_details.id',$userData->userDetailsId)
                            ->select('patient_details.*','country.country as country_name','state.state  as state_name','city.city as city_name')
                            ->first();
       
        $success['user_data'] =  $userData;
        $success['user_information'] =  $userInformation;
        return response()->json([
          //'status' => '4',
          'status'=>'success',
          'data' => $success
        ]);
      }else{
        return response()->json([
          //'status' => '4',
          'status'=>'failed',
          'data' => "0"
        ]);
      }
  
    }

    public function check_date(Request $request){

  
      $BookingRequest = BookingRequest::where('date_time', '=', $request->date_time)->where('pharmacy_id', '=',$request->pharmacy_id)->first();
      if( $BookingRequest){
        return response()->json(["status" => true,"message" => "Slot Already Booked From H360"]);
      }else{
        return response()->json(["status" => false,"message" => "Slot Selected Sucessfully"]);
      }
     
  

  }
}
