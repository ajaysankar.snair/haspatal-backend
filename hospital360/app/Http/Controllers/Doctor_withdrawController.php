<?php

namespace App\Http\Controllers;

use App\DataTables\Doctor_withdrawDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateDoctor_withdrawRequest;
use App\Http\Requests\UpdateDoctor_withdrawRequest;
use App\Repositories\Doctor_withdrawRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Auth;
use DB;
class Doctor_withdrawController extends AppBaseController
{
    /** @var  Doctor_withdrawRepository */
    private $doctorWithdrawRepository;

    public function __construct(Doctor_withdrawRepository $doctorWithdrawRepo)
    {
        $this->doctorWithdrawRepository = $doctorWithdrawRepo;
    }

    /**
     * Display a listing of the Doctor_withdraw.
     *
     * @param Doctor_withdrawDataTable $doctorWithdrawDataTable
     * @return Response
     */
    public function index(Doctor_withdrawDataTable $doctorWithdrawDataTable)
    {
        return $doctorWithdrawDataTable->render('doctor_withdraws.index');
    }

    /**
     * Show the form for creating a new Doctor_withdraw.
     *
     * @return Response
     */
    public function create()
    {
        return view('doctor_withdraws.create');
    }

    /**
     * Store a newly created Doctor_withdraw in storage.
     *
     * @param CreateDoctor_withdrawRequest $request
     *
     * @return Response
     */
    public function store(CreateDoctor_withdrawRequest $request)
    {
        $input = $request->all();
        $input['created_by'] = Auth::user()->id;
        $input['updated_by'] = Auth::user()->id;
        $input['doctor_id'] = Auth::user()->userDetailsId; 

        $doctorWithdraw = $this->doctorWithdrawRepository->create($input);
         if(Auth::user()->role_id == 1){            
             Flash::success('Doctor Withdraw saved successfully.');

        return redirect(route('doctorWithdraws.index'));
        }else{

             Flash::success('Doctor Withdraw saved successfully.');
            return back();
        }
       
    }

    /**
     * Display the specified Doctor_withdraw.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $doctorWithdraw = $this->doctorWithdrawRepository->find($id);

        if (empty($doctorWithdraw)) {
            Flash::error('Doctor Withdraw not found');

            return redirect(route('doctorWithdraws.index'));
        }

        return view('doctor_withdraws.show')->with('doctorWithdraw', $doctorWithdraw);
    }

    /**
     * Show the form for editing the specified Doctor_withdraw.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $doctorWithdraw = $this->doctorWithdrawRepository->find($id);

        if (empty($doctorWithdraw)) {
            Flash::error('Doctor Withdraw not found');

            return redirect(route('doctorWithdraws.index'));
        }

        return view('doctor_withdraws.edit')->with('doctorWithdraw', $doctorWithdraw);
    }

    /**
     * Update the specified Doctor_withdraw in storage.
     *
     * @param  int              $id
     * @param UpdateDoctor_withdrawRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDoctor_withdrawRequest $request)
    {
        $doctorWithdraw = $this->doctorWithdrawRepository->find($id);

        if (empty($doctorWithdraw)) {
            Flash::error('Doctor Withdraw not found');

            return redirect(route('doctorWithdraws.index'));
        }

        $doctorWithdraw = $this->doctorWithdrawRepository->update($request->all(), $id);

        Flash::success('Doctor Withdraw updated successfully.');

        return redirect(route('doctorWithdraws.index'));
    }

    /**
     * Remove the specified Doctor_withdraw from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $doctorWithdraw = $this->doctorWithdrawRepository->find($id);

        if (empty($doctorWithdraw)) {
            Flash::error('Doctor Withdraw not found');

            return redirect(route('doctorWithdraws.index'));
        }

        $this->doctorWithdrawRepository->delete($id);

        Flash::success('Doctor Withdraw deleted successfully.');

        return redirect(route('doctorWithdraws.index'));
    }
}
