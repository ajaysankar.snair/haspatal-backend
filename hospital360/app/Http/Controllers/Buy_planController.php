<?php

namespace App\Http\Controllers;

use App\DataTables\Buy_planDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateBuy_planRequest;
use App\Http\Requests\UpdateBuy_planRequest;
use App\Repositories\Buy_planRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class Buy_planController extends AppBaseController
{
    /** @var  Buy_planRepository */
    private $buyPlanRepository;

    public function __construct(Buy_planRepository $buyPlanRepo)
    {
        $this->buyPlanRepository = $buyPlanRepo;
    }

    /**
     * Display a listing of the Buy_plan.
     *
     * @param Buy_planDataTable $buyPlanDataTable
     * @return Response
     */
    public function index(Buy_planDataTable $buyPlanDataTable)
    {
        return $buyPlanDataTable->render('buy_plans.index');
    }

    /**
     * Show the form for creating a new Buy_plan.
     *
     * @return Response
     */
    public function create()
    {
        return view('buy_plans.create');
    }

    /**
     * Store a newly created Buy_plan in storage.
     *
     * @param CreateBuy_planRequest $request
     *
     * @return Response
     */
    public function store(CreateBuy_planRequest $request)
    {
        $input = $request->all();

        $buyPlan = $this->buyPlanRepository->create($input);

        Flash::success('Buy Plan saved successfully.');

        return redirect(route('buyPlans.index'));
    }

    /**
     * Display the specified Buy_plan.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $buyPlan = $this->buyPlanRepository->find($id);

        if (empty($buyPlan)) {
            Flash::error('Buy Plan not found');

            return redirect(route('buyPlans.index'));
        }

        return view('buy_plans.show')->with('buyPlan', $buyPlan);
    }

    /**
     * Show the form for editing the specified Buy_plan.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $buyPlan = $this->buyPlanRepository->find($id);

        if (empty($buyPlan)) {
            Flash::error('Buy Plan not found');

            return redirect(route('buyPlans.index'));
        }

        return view('buy_plans.edit')->with('buyPlan', $buyPlan);
    }

    /**
     * Update the specified Buy_plan in storage.
     *
     * @param  int              $id
     * @param UpdateBuy_planRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateBuy_planRequest $request)
    {
        $buyPlan = $this->buyPlanRepository->find($id);

        if (empty($buyPlan)) {
            Flash::error('Buy Plan not found');

            return redirect(route('buyPlans.index'));
        }

        $buyPlan = $this->buyPlanRepository->update($request->all(), $id);

        Flash::success('Buy Plan updated successfully.');

        return redirect(route('buyPlans.index'));
    }

    /**
     * Remove the specified Buy_plan from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $buyPlan = $this->buyPlanRepository->find($id);

        if (empty($buyPlan)) {
            Flash::error('Buy Plan not found');

            return redirect(route('buyPlans.index'));
        }

        $this->buyPlanRepository->delete($id);

        Flash::success('Buy Plan deleted successfully.');

        return redirect(route('buyPlans.index'));
    }
}
