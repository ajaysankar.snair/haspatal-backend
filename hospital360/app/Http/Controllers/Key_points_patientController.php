<?php

namespace App\Http\Controllers;

use App\DataTables\Key_points_patientDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateKey_points_patientRequest;
use App\Http\Requests\UpdateKey_points_patientRequest;
use App\Repositories\Key_points_patientRepository;
use Illuminate\Http\Request;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Auth;
class Key_points_patientController extends AppBaseController
{
    /** @var  Key_points_patientRepository */
    private $keyPointsPatientRepository;

    public function __construct(Key_points_patientRepository $keyPointsPatientRepo)
    {
        $this->keyPointsPatientRepository = $keyPointsPatientRepo;
    }

    /**
     * Display a listing of the Key_points_patient.
     *
     * @param Key_points_patientDataTable $keyPointsPatientDataTable
     * @return Response
     */
    public function index(Key_points_patientDataTable $keyPointsPatientDataTable)
    {
        return $keyPointsPatientDataTable->render('key_points_patients.index');
    }

    /**
     * Show the form for creating a new Key_points_patient.
     *
     * @return Response
     */
    public function create()
    {
        return view('key_points_patients.create');
    }

    /**
     * Store a newly created Key_points_patient in storage.
     *
     * @param CreateKey_points_patientRequest $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $input['created_by'] = Auth::user()->id;
        $input['updated_by'] = Auth::user()->id;
        $keyPointsPatient = $this->keyPointsPatientRepository->create($input);

        if(Auth::user()->role_id == 1){
            Flash::success('Key Points Patient saved successfully.');
           return redirect(route('keyPointsPatients.index'));
        }else{
            Flash::success('Key Points saved successfully.');
            return back();
        }

        
    }

    /**
     * Display the specified Key_points_patient.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $keyPointsPatient = $this->keyPointsPatientRepository->find($id);

        if (empty($keyPointsPatient)) {
            Flash::error('Key Points Patient not found');

            return redirect(route('keyPointsPatients.index'));
        }

        return view('key_points_patients.show')->with('keyPointsPatient', $keyPointsPatient);
    }

    /**
     * Show the form for editing the specified Key_points_patient.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $keyPointsPatient = $this->keyPointsPatientRepository->find($id);

        if (empty($keyPointsPatient)) {
            Flash::error('Key Points Patient not found');

            return redirect(route('keyPointsPatients.index'));
        }

        return view('key_points_patients.edit')->with('keyPointsPatient', $keyPointsPatient);
    }

    /**
     * Update the specified Key_points_patient in storage.
     *
     * @param  int              $id
     * @param UpdateKey_points_patientRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateKey_points_patientRequest $request)
    {
        $keyPointsPatient = $this->keyPointsPatientRepository->find($id);

        if (empty($keyPointsPatient)) {
            Flash::error('Key Points Patient not found');

            return redirect(route('keyPointsPatients.index'));
        }

        $keyPointsPatient = $this->keyPointsPatientRepository->update($request->all(), $id);

        Flash::success('Key Points Patient updated successfully.');

        return redirect(route('keyPointsPatients.index'));
    }

    /**
     * Remove the specified Key_points_patient from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $keyPointsPatient = $this->keyPointsPatientRepository->find($id);

        if (empty($keyPointsPatient)) {
            Flash::error('Key Points Patient not found');

            return redirect(route('keyPointsPatients.index'));
        }

        $this->keyPointsPatientRepository->delete($id);

        Flash::success('Key Points Patient deleted successfully.');

        return redirect(route('keyPointsPatients.index'));
    }
}
