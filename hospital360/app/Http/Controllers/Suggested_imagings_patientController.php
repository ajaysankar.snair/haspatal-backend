<?php

namespace App\Http\Controllers;

use App\DataTables\Suggested_imagings_patientDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateSuggested_imagings_patientRequest;
use App\Http\Requests\UpdateSuggested_imagings_patientRequest;
use App\Repositories\Suggested_imagings_patientRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Illuminate\Http\Request;
use Auth;
class Suggested_imagings_patientController extends AppBaseController
{
    /** @var  Suggested_imagings_patientRepository */
    private $suggestedImagingsPatientRepository;

    public function __construct(Suggested_imagings_patientRepository $suggestedImagingsPatientRepo)
    {
        $this->suggestedImagingsPatientRepository = $suggestedImagingsPatientRepo;
    }

    /**
     * Display a listing of the Suggested_imagings_patient.
     *
     * @param Suggested_imagings_patientDataTable $suggestedImagingsPatientDataTable
     * @return Response
     */
    public function index(Suggested_imagings_patientDataTable $suggestedImagingsPatientDataTable)
    {
        return $suggestedImagingsPatientDataTable->render('suggested_imagings_patients.index');
    }

    /**
     * Show the form for creating a new Suggested_imagings_patient.
     *
     * @return Response
     */
    public function create()
    {
        return view('suggested_imagings_patients.create');
    }

    /**
     * Store a newly created Suggested_imagings_patient in storage.
     *
     * @param CreateSuggested_imagings_patientRequest $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $input['created_by'] = Auth::user()->id;
        $input['updated_by'] = Auth::user()->id;
        $suggestedImagingsPatient = $this->suggestedImagingsPatientRepository->create($input);

        if(Auth::user()->role_id == 1){
        Flash::success('Suggested Imagings Patient saved successfully.');
        return redirect(route('suggestedImagingsPatients.index'));
        }else{
        Flash::success('Suggested Imagings saved successfully.');
            return back();
        }
    }

    /**
     * Display the specified Suggested_imagings_patient.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $suggestedImagingsPatient = $this->suggestedImagingsPatientRepository->find($id);

        if (empty($suggestedImagingsPatient)) {
            Flash::error('Suggested Imagings Patient not found');

            return redirect(route('suggestedImagingsPatients.index'));
        }

        return view('suggested_imagings_patients.show')->with('suggestedImagingsPatient', $suggestedImagingsPatient);
    }

    /**
     * Show the form for editing the specified Suggested_imagings_patient.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $suggestedImagingsPatient = $this->suggestedImagingsPatientRepository->find($id);

        if (empty($suggestedImagingsPatient)) {
            Flash::error('Suggested Imagings Patient not found');

            return redirect(route('suggestedImagingsPatients.index'));
        }

        return view('suggested_imagings_patients.edit')->with('suggestedImagingsPatient', $suggestedImagingsPatient);
    }

    /**
     * Update the specified Suggested_imagings_patient in storage.
     *
     * @param  int              $id
     * @param UpdateSuggested_imagings_patientRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSuggested_imagings_patientRequest $request)
    {
        $suggestedImagingsPatient = $this->suggestedImagingsPatientRepository->find($id);

        if (empty($suggestedImagingsPatient)) {
            Flash::error('Suggested Imagings Patient not found');

            return redirect(route('suggestedImagingsPatients.index'));
        }

        $suggestedImagingsPatient = $this->suggestedImagingsPatientRepository->update($request->all(), $id);

        Flash::success('Suggested Imagings Patient updated successfully.');

        return redirect(route('suggestedImagingsPatients.index'));
    }

    /**
     * Remove the specified Suggested_imagings_patient from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $suggestedImagingsPatient = $this->suggestedImagingsPatientRepository->find($id);

        if (empty($suggestedImagingsPatient)) {
            Flash::error('Suggested Imagings Patient not found');

            return redirect(route('suggestedImagingsPatients.index'));
        }

        $this->suggestedImagingsPatientRepository->delete($id);

        Flash::success('Suggested Imagings Patient deleted successfully.');

        return redirect(route('suggestedImagingsPatients.index'));
    }
}
