<?php

namespace App\Http\Controllers;

use App\DataTables\ConsultantDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateConsultantRequest;
use App\Http\Requests\UpdateConsultantRequest;
use App\Repositories\ConsultantRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class ConsultantController extends AppBaseController
{
    /** @var  ConsultantRepository */
    private $consultantRepository;

    public function __construct(ConsultantRepository $consultantRepo)
    {
        $this->consultantRepository = $consultantRepo;
    }

    /**
     * Display a listing of the Consultant.
     *
     * @param ConsultantDataTable $consultantDataTable
     * @return Response
     */
    public function index(ConsultantDataTable $consultantDataTable)
    {
        return $consultantDataTable->render('consultants.index');
    }

    /**
     * Show the form for creating a new Consultant.
     *
     * @return Response
     */
    public function create()
    {
        return view('consultants.create');
    }

    /**
     * Store a newly created Consultant in storage.
     *
     * @param CreateConsultantRequest $request
     *
     * @return Response
     */
    public function store(CreateConsultantRequest $request)
    {
        $input = $request->all();

        $consultant = $this->consultantRepository->create($input);

        Flash::success('Consultant saved successfully.');

        return redirect(route('consultants.index'));
    }

    /**
     * Display the specified Consultant.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $consultant = $this->consultantRepository->find($id);

        if (empty($consultant)) {
            Flash::error('Consultant not found');

            return redirect(route('consultants.index'));
        }

        return view('consultants.show')->with('consultant', $consultant);
    }

    /**
     * Show the form for editing the specified Consultant.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $consultant = $this->consultantRepository->find($id);

        if (empty($consultant)) {
            Flash::error('Consultant not found');

            return redirect(route('consultants.index'));
        }

        return view('consultants.edit')->with('consultant', $consultant);
    }

    /**
     * Update the specified Consultant in storage.
     *
     * @param  int              $id
     * @param UpdateConsultantRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateConsultantRequest $request)
    {
        $consultant = $this->consultantRepository->find($id);

        if (empty($consultant)) {
            Flash::error('Consultant not found');

            return redirect(route('consultants.index'));
        }

        $consultant = $this->consultantRepository->update($request->all(), $id);

        Flash::success('Consultant updated successfully.');

        return redirect(route('consultants.index'));
    }

    /**
     * Remove the specified Consultant from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $consultant = $this->consultantRepository->find($id);

        if (empty($consultant)) {
            Flash::error('Consultant not found');

            return redirect(route('consultants.index'));
        }

        $this->consultantRepository->delete($id);

        Flash::success('Consultant deleted successfully.');

        return redirect(route('consultants.index'));
    }
}
