<?php

namespace App\Http\Controllers;

use App\DataTables\Business_typeDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateBusiness_typeRequest;
use App\Http\Requests\UpdateBusiness_typeRequest;
use App\Repositories\Business_typeRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Auth;
use DB;

class Business_typeController extends AppBaseController
{
    /** @var  Business_typeRepository */
    private $businessTypeRepository;

    public function __construct(Business_typeRepository $businessTypeRepo)
    {
        $this->businessTypeRepository = $businessTypeRepo;
    }

    /**
     * Display a listing of the Business_type.
     *
     * @param Business_typeDataTable $businessTypeDataTable
     * @return Response
     */
    public function index(Business_typeDataTable $businessTypeDataTable)
    {
        return $businessTypeDataTable->render('business_types.index');
    }

    /**
     * Show the form for creating a new Business_type.
     *
     * @return Response
     */
    public function create()
    {
        return view('business_types.create');
    }

    /**
     * Store a newly created Business_type in storage.
     *
     * @param CreateBusiness_typeRequest $request
     *
     * @return Response
     */
    public function store(CreateBusiness_typeRequest $request)
    {
        $input = $request->all();
        $input['created_by'] = Auth::user()->id;
        $input['updated_by'] = Auth::user()->id;
        
        $businessType = $this->businessTypeRepository->create($input);

        Flash::success('Business Type saved successfully.');

        return redirect(route('businessTypes.index'));
    }

    /**
     * Display the specified Business_type.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $businessType = $this->businessTypeRepository->find($id);

        if (empty($businessType)) {
            Flash::error('Business Type not found');

            return redirect(route('businessTypes.index'));
        }

        return view('business_types.show')->with('businessType', $businessType);
    }

    /**
     * Show the form for editing the specified Business_type.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $businessType = $this->businessTypeRepository->find($id);

        if (empty($businessType)) {
            Flash::error('Business Type not found');

            return redirect(route('businessTypes.index'));
        }

        return view('business_types.edit')->with('businessType', $businessType);
    }

    /**
     * Update the specified Business_type in storage.
     *
     * @param  int              $id
     * @param UpdateBusiness_typeRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateBusiness_typeRequest $request)
    {
        $businessType = $this->businessTypeRepository->find($id);

        if (empty($businessType)) {
            Flash::error('Business Type not found');

            return redirect(route('businessTypes.index'));
        }

        $businessType = $this->businessTypeRepository->update($request->all(), $id);

        Flash::success('Business Type updated successfully.');

        return redirect(route('businessTypes.index'));
    }

    /**
     * Remove the specified Business_type from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $businessType = $this->businessTypeRepository->find($id);

        if (empty($businessType)) {
            Flash::error('Business Type not found');

            return redirect(route('businessTypes.index'));
        }

        $this->businessTypeRepository->delete($id);

        Flash::success('Business Type deleted successfully.');

        return redirect(route('businessTypes.index'));
    }
}
