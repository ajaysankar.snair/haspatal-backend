<?php

namespace App\Http\Controllers;

use App\DataTables\Counselings_listDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateCounselings_listRequest;
use App\Http\Requests\UpdateCounselings_listRequest;
use App\Repositories\Counselings_listRepository;
use App\Http\Controllers\AppBaseController;
use Flash;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Illuminate\View\View;
use Response;

class Counselings_listController extends AppBaseController
{
    /** @var  Counselings_listRepository */
    private $counselingsListRepository;

    public function __construct(Counselings_listRepository $counselingsListRepo)
    {
        $this->counselingsListRepository = $counselingsListRepo;
    }

    /**
     * Display a listing of the Counselings_list.
     *
     * @param Counselings_listDataTable $counselingsListDataTable
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function index(Counselings_listDataTable $counselingsListDataTable)
    {
        return $counselingsListDataTable->render('counselings_lists.index');
    }

    /**
     * Show the form for creating a new Counselings_list.
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function create()
    {
        return view('counselings_lists.create');
    }

    /**
     * Store a newly created Counselings_list in storage.
     *
     * @param CreateCounselings_listRequest $request
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function store(CreateCounselings_listRequest $request)
    {
        $input = $request->all();

        $counselingsList = $this->counselingsListRepository->create($input);

        Flash::success('Counselings List saved successfully.');

        return redirect(route('counselingsLists.index'));
    }

    /**
     * Display the specified Counselings_list.
     *
     * @param  int $id
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function show($id)
    {
        $counselingsList = $this->counselingsListRepository->find($id);

        if (empty($counselingsList)) {
            Flash::error('Counselings List not found');

            return redirect(route('counselingsLists.index'));
        }

        return view('counselings_lists.show')->with('counselingsList', $counselingsList);
    }

    /**
     * Show the form for editing the specified Counselings_list.
     *
     * @param  int $id
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function edit($id)
    {
        $counselingsList = $this->counselingsListRepository->find($id);

        if (empty($counselingsList)) {
            Flash::error('Counselings List not found');

            return redirect(route('counselingsLists.index'));
        }

        return view('counselings_lists.edit')->with('counselingsList', $counselingsList);
    }

    /**
     * Update the specified Counselings_list in storage.
     *
     * @param  int              $id
     * @param UpdateCounselings_listRequest $request
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function update($id, UpdateCounselings_listRequest $request)
    {
        $counselingsList = $this->counselingsListRepository->find($id);

        if (empty($counselingsList)) {
            Flash::error('Counselings List not found');

            return redirect(route('counselingsLists.index'));
        }

        $counselingsList = $this->counselingsListRepository->update($request->all(), $id);

        Flash::success('Counselings List updated successfully.');

        return redirect(route('counselingsLists.index'));
    }

    /**
     * Remove the specified Counselings_list from storage.
     *
     * @param  int $id
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function destroy($id)
    {
        $counselingsList = $this->counselingsListRepository->find($id);

        if (empty($counselingsList)) {
            Flash::error('Counselings List not found');

            return redirect(route('counselingsLists.index'));
        }

        $this->counselingsListRepository->delete($id);

        Flash::success('Counselings List deleted successfully.');

        return redirect(route('counselingsLists.index'));
    }
}
