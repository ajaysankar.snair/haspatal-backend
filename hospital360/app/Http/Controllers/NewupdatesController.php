<?php

namespace App\Http\Controllers;
use App\Http\Requests\CreateHaspatal_360_registerRequest;
use App\Http\Requests\API\CreateStateAPIRequest;
use App\Http\Requests\API\UpdateStateAPIRequest;
use App\Models\State;
use App\Repositories\StateRepository;
use App\Repositories\Business_registerRepository;
use Illuminate\Http\Request;
use App\Models\Service_details;
use App\Models\Your_coverage_360;
use App\Models\Haspatal_360_register;
use App\coupon;
use App\Models\Create_offer_360;
use App\User;
use App\Models\City;
use App\Models\Wallet_360;
use App\couponrate;
use App\Models\Business_register;
use App\Http\Controllers\AppBaseController;
use Response;
use DB;



class NewupdatesController extends Controller
{
 
       /** @var  Business_registerRepository */
       private $businessRegisterRepository;

       public function __construct(Business_registerRepository $businessRegisterRepo)
       {
           $this->businessRegisterRepository = $businessRegisterRepo;
       }
   
       /**
        * @param Request $request
        * @return Response
        *
        * @SWG\Get(
        *      path="/businessRegisters",
        *      summary="Get a listing of the Business_registers.",
        *      tags={"Business_register"},
        *      description="Get all Business_registers",
        *      produces={"application/json"},
        *      @SWG\Response(
        *          response=200,
        *          description="successful operation",
        *          @SWG\Schema(
        *              type="object",
        *              @SWG\Property(
        *                  property="success",
        *                  type="boolean"
        *              ),
        *              @SWG\Property(
        *                  property="data",
        *                  type="array",
        *                  @SWG\Items(ref="#/definitions/Business_register")
        *              ),
        *              @SWG\Property(
        *                  property="message",
        *                  type="string"
        *              )
        *          )
        *      )
        * )
        */
    //
    public function serviceType()
    {
        $data = Service_details::select('id', 'service')->get();
        if($data){
        $Service_details['success']=true;
        $Service_details['message']="Date Fetched  successful";
        $Service_details['data']=$data;
        
    }
       
        return response()->json(  $Service_details,200);
    }
    public function getsate()
    {
        $data = State::select('id', 'state')->get();
        if($data){
        $State['success']=true;
        $State['message']="Date Fetched  successful";
        $State['state_data']=$data;
        
    }
       
        return response()->json(  $State,200);
    }
     public function city_list(Request $request)
     
    {
       $state_id=  $request->input('state_id');
         if ($state_id == 0) {
            $result = City::orderBy('city','ASC')->get();
        }else{

            $result = City::where('state_name', $state_id)->orderBy('city','ASC')->get();
        }
  
       if ($result) {
            return response()->json(["status" => true,"massage"=>"City retrieved successfully","data" => $result]);
        }else{
            return response()->json(["status" => false,"data" => $result]);
        }
    }
    public function districtlist()
    {
        $data = State::select('id', 'state')->get();
        if($data){
        $State['success']=true;
        $State['message']="Date Fetched  successful";
        $State['state_data']=$data;
        
    }       
        return response()->json(  $State,200);
    }
    


          public function validateRegistration(Request $obj)
          {
                 $email = $obj->email;
                 $mo_prefix=$obj->mo_prefix;
                 $mobile_no=$obj->mobile_no;


                 $email=explode('=',$email);
                 $email= $email[1];
                 //echo$email;


                  $mo_prefix=explode('=',$mo_prefix);
                 $mo_prefix= $mo_prefix[1];
                 //echo$mo_prefix;

                  $mobile_no=explode('=',$mobile_no);
                 $mobile_no= $mobile_no[1];
                //echo$mobile_no;



                    //print_r($obj->email);
               // $checkedData=Haspatal_360_register::find(3698745210,'mobile_no');

                        $where=['mobile_no'=>$mobile_no];
                        $where2=['email'=>$email];
               // $where=['mobile_no'=>9874563210,'mo_prefix'=>'+91','email'=>'raju103@gmaili.com'];
            $results = DB::table('haspatal_360_register')->where($where)->orWhere($where2)->get();
              // print_r($results);

                              // echo count($results);
                if(count($results)>0)
                {

                     //$data['results']=$results;
                $data['success']=true;
                $data['message']="User Already Registered";
                return response()->json($data,200);
               
                
               }
               else
               {
                     $data['success']=false;
                $data['message']="Not Registered";

                return response()->json($data,200);
               }

          }   


     public function newreg(Request $request)
    {
        
        $book =  DB::table('business_register')->where('user_id', $request->user_id)->orderBy('user_id', 'desc')->first();
        $users = User::where('id', $request->user_id)->orderBy('id', 'desc')
        ->first();
        if( $users){
            $users->reg_status=$request->reg_status;
            $users->save();  
        }

    // $user =  User::select('*')->where(['id' => $request->input('product_id')]);
    $role_id = $users->role_id;
        /* echo "<pre>";
        print_r($book); exit;*/

        if(!empty($book)){
            $input = $request->all();
            $id = $book->id;
            if($request->hasfile('b_lic'))
            {
                $image1 = $request->file('b_lic');
                $extension1 = $image1->getClientOriginalExtension(); // getting image extension
                $filename1 =time().'__'.$image1->getClientOriginalName();
                $path1 = public_path('/media/b_lic/');
                $image1->move($path1, $filename1);
                $input['b_lic'] = $filename1;
            }
            if($request->hasfile('shop_pic'))
            {
                $image1 = $request->file('shop_pic');
                $extension1 = $image1->getClientOriginalExtension(); // getting image extension
                $filename1 =time().'__'.$image1->getClientOriginalName();
                $path1 = public_path('/media/shop_pic/');
                $image1->move($path1, $filename1);
                $input['shop_pic'] = $filename1;
            }
            if($request->hasfile('b_card_pic'))
            {
                $image1 = $request->file('b_card_pic');
                $extension1 = $image1->getClientOriginalExtension(); // getting image extension
                $filename1 =time().'__'.$image1->getClientOriginalName();
                $path1 = public_path('/media/b_card_pic/');
                $image1->move($path1, $filename1);
                $input['b_card_pic'] = $filename1;
            }
      
            $businessRegister = $this->businessRegisterRepository->update($input, $id);
           
            // $businessRegister['district']=$request->input('district');

        }

        if(empty($book))
        {
            $input = $request->all();
          //  $input['user_id']=$request->input('user_id');
            $input['created_by'] = $request->user_id;
            $input['updated_by'] = $request->user_id;
            if($request->hasfile('b_lic'))
            {
                $image1 = $request->file('b_lic');
                $extension1 = $image1->getClientOriginalExtension(); // getting image extension
                $filename1 =time().'__'.$image1->getClientOriginalName();
                $path1 = public_path('/media/b_lic/');
                $image1->move($path1, $filename1);
                $input['b_lic'] = $filename1;
            }
            if($request->hasfile('shop_pic'))
            {
                $image1 = $request->file('shop_pic');
                $extension1 = $image1->getClientOriginalExtension(); // getting image extension
                $filename1 =time().'__'.$image1->getClientOriginalName();
                $path1 = public_path('/media/shop_pic/');
                $image1->move($path1, $filename1);
                $input['shop_pic'] = $filename1;
            }
            if($request->hasfile('b_card_pic'))
            {
                $image1 = $request->file('b_card_pic');
                $extension1 = $image1->getClientOriginalExtension(); // getting image extension
                $filename1 =time().'__'.$image1->getClientOriginalName();
                $path1 = public_path('/media/b_card_pic/');
                $image1->move($path1, $filename1);
                $input['b_card_pic'] = $filename1;
            }
            $businessRegister = $this->businessRegisterRepository->create($input);
            $businessRegister->terms_status='0';
            $businessRegister->b_id=$role_id;
            //////////////////////////////////////////////////////////////////////////////////
            // $role_id = $users->role_id;
            $role = DB::table('couponrates')
                    ->where('category_id', $role_id)
                    ->orderBy('id', 'desc')
                    ->first();
            if ($role) 
            {
                $firstLetter = $role->RegCode;
                $businssID=$businessRegister->id;
                $regID =
                "91". $firstLetter.
                    "0000".
                    $businssID;
            $businessRegister->business_registerID=$regID;
            }
            $businessRegister->save();
            // $businessRegister['district']=$request->input('district');
            

        }
        if( $businessRegister->district==null){
            $businessRegister->district=$request->input('district');
    
            // $businessRegister->terms_status='0';
            $businessRegister->save();
        }
        if($businessRegister->city_id)
        {
            $city = DB::table('city')
              ->where('id', $businessRegister->city_id)
              ->orderBy('id', 'desc')
              ->first();
              if($city)
              {
              $businessRegister->cityname = $city->city;
              }
               else
              {
              $businessRegister->cityname="Processing";
              }
          }
          else
          {
            $businessRegister->cityname="Not Selected"  ;
          }
          if($businessRegister->state_id){
            $state = DB::table('state')
                ->where('id', $businessRegister->state_id)
                ->orderBy('id', 'desc')
                ->first();
                if($city)
                {
                    $businessRegister->statename = $state->state;
                }
                 else
                {
                    $businessRegister->statename="Processing";
                }
           
        }else{
            $businessRegister->statename="Not Selected"  ;
        }
        if ($businessRegister->b_id) {
            $role_id = $businessRegister->b_id;
            $role = DB::table('couponrates')
                ->where('category_id', $role_id)
                ->orderBy('id', 'desc')
                ->first();

                $businessRegister->type = $role->name;
        }
        if ($businessRegister->user_id) {
            $user_id = $businessRegister->user_id;
            $users = User::where('id', $user_id)->orderBy('id', 'desc')
            ->first();

                $businessRegister->user_mobile = $users->mobile;
                if($users->reg_status==0){
                   
                    $beforeapproval = $this->send_mailbeforeapproval($businessRegister,$users->email);
                    // return $beforeapproval;
                }
        }
        //////////////////////////////////////////////////////////////////////////////////////////////////
        // return $this->sendResponse($businessRegister->toArray(), 'Business Register saved successfully');
    //   return response()->json(  $businessRegister,200);
      return response()->json(["status" => 'Business Register saved successfully',"data" => $businessRegister]);
    }


    public function send_mailbeforeapproval($businessRegister,$email)
    
    {
       
               $subject =
               "Your Haspatal 360 registration for ".$businessRegister->type." is under Approval Process ";
        // $mailmessage= $content;
        $mailmessage= '<!DOCTYPE html>
        <html lang="en">
            <head>
                <title>Dr email template</title>
                <meta charset="utf-8">
                <!-- CSS only -->
                <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
                <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
                <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900,900i&display=swap" rel="stylesheet">
                <script src="https://kit.fontawesome.com/fa5e54101c.js" crossorigin="anonymous"></script>
                <!-- JavaScript Bundle with Popper -->
        
                <!-- Theme Style -->
                <!-- <link rel="stylesheet" href="/assets/css/bootstrap.min.css"> -->
                <style>
                    #page-wrap{
                        max-width: 800px; 
                        margin: 0 auto;
                        font-family: "Roboto";
                        /* padding: 25px 0; */
                    }
                    p,h5,h6{
                        color: #000;
                        font-family: "Roboto";
                        margin: 0;
                    }
                    p span{
                        display: inline-block;
                    }
                </style>
            </head>
            <body>
                <div id="page-wrap">
                    <div class="top">
                    
                    <h6 style="margin-bottom: 25px; font-weight:400; font-size: 16px;">Dear '.$businessRegister->b_name.', </h6>
                        <p style="margin-bottom: 25px;">We thank you for registering on Haspatal 360 App. Your application is under approval process. </p>
                        <p style="margin-bottom: 25px; margin-top: 25px;">Our approval team may contact you for additional information if needed.
                        </p>
                        <p style="margin-bottom: 25px; margin-top: 25px;"">For any query, please contact Haspatal Support Team at: +91-124-405 56 56 or email us at network@haspatal.com</p>
                        <p style="margin-bottom: 25px;  margin-top: 25px;"">Team Haspatal 360</p>
                    </div>
                </div>
            </body>
        </html>
        
        
'                 ;
        $data['personalizations'] = array("0" => array('to' => array('0' => array('email' => $email))));

        $data['from'] = array('email' => 'system@haspatal.com');

        $data['subject'] = $subject;
        $data['content'] = array("0" =>array('type' =>' text/html','value' => $mailmessage ));

        //echo json_encode($data);exit();

        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://api.sendgrid.com/v3/mail/send",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => json_encode($data),
          CURLOPT_HTTPHEADER => array(
            "authorization: Bearer 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",
            "cache-control: no-cache",
            "content-type: application/json",
            "postman-token: b8c0781f-af48-90cc-1c5b-76f80e0b4be0"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            return "Send OTP in your emailId";
        }
    }




          //second registration API

           public function newreg2(Request $request)
    {
        
        $book =  DB::table('business_register')->where('user_id', $request->user_id)->orderBy('user_id', 'desc')->first();
       /* echo "<pre>";
        print_r($book); exit;*/

        if(!empty($book)){
            $input = $request->all();
            $id = $book->id;

            $new2['state_id']=$request->input('state_id');
            $new2['city_id']=$request->input('city_id');
            $new2['pincode']=$request->input('pincode');
            $new2['address']=$request->input('address');

                 if($request->hasfile('b_lic'))
            {
                $image1 = $request->file('b_lic');
                $extension1 = $image1->getClientOriginalExtension(); // getting image extension
                $filename1 =time().'__'.$image1->getClientOriginalName();
                $path1 = public_path('/media/b_lic/');
                $image1->move($path1, $filename1);
                $new2['b_lic'] = $filename1;
            }
           
      

            $new2['district']=$request->input('district');

                        $businessRegister = $this->businessRegisterRepository->update($new2, $id);

        }

        
        // return $this->sendResponse($businessRegister->toArray(), 'Business Register saved successfully');
    //   return response()->json(  $businessRegister,200);
      return response()->json(["status" => 'Business Details saved successfully',"data" => $new2]);
    }


    //closed second 






        //third1 registration API

           public function newreg3(Request $request)
    {
        
        $book =  DB::table('business_register')->where('user_id', $request->user_id)->orderBy('user_id', 'desc')->first();
       /* echo "<pre>";
        print_r($book); exit;*/

        if(!empty($book)){
            $input = $request->all();
            $id = $book->id;

$new3['patient_select_you']=$request->input('a').'-'.$request->input('b').'-'.$request->input('c').'-'.$request->input('d').'-'.$request->input('e').'-'.$request->input('f');
            
            //var_dump($new3);
     $businessRegister = $this->businessRegisterRepository->update($new3, $id);

        }

        
        // return $this->sendResponse($businessRegister->toArray(), 'Business Register saved successfully');
    //   return response()->json(  $businessRegister,200);
      return response()->json(["status" => 'Business Details saved successfully',"data" => $businessRegister]);
    }


    //closed third1 

    public function get_districtlist()
    {
        $data = [

            
                        'Adilabad',
                        'Anantapur',
                        'Chittoor',
                        'Kakinada',
                        'Guntur',
                        'Hyderabad',
                        'Karimnagar',
                        'Khammam',
                        'Krishna',
                        'Kurnool',
                        'Mahbubnagar',
                        'Medak',
                        'Nalgonda',
                        'Nizamabad',
                        'Ongole',
                        'Hyderabad',
                        'Srikakulam',
                        'Nellore',
                        'Visakhapatnam',
                        'Vizianagaram',
                        'Warangal',
                        'Eluru',
                        'Kadapa',
                  
                        'Anjaw',
                        'Changlang',
                        'East Siang',
                        'Kurung Kumey',
                        'Lohit',
                        'Lower Dibang Valley',
                        'Lower Subansiri',
                        'Papum Pare',
                        'Tawang',
                        'Tirap',
                        'Dibang Valley',
                        'Upper Siang',
                        'Upper Subansiri',
                        'West Kameng',
                        'West Siang',
               
                        'Baksa',
                        'Barpeta',
                        'Bongaigaon',
                        'Cachar',
                        'Chirang',
                        'Darrang',
                        'Dhemaji',
                        'Dima Hasao',
                        'Dhubri',
                        'Dibrugarh',
                        'Goalpara',
                        'Golaghat',
                        'Hailakandi',
                        'Jorhat',
                        'Kamrup',
                        'Kamrup Metropolitan',
                        'Karbi Anglong',
                        'Karimganj',
                        'Kokrajhar',
                        'Lakhimpur',
                        'Marigaon',
                        'Nagaon',
                        'Nalbari',
                        'Sibsagar',
                        'Sonitpur',
                        'Tinsukia',
                        'Udalguri',
              
                        'Araria',
                        'Arwal',
                        'Aurangabad',
                        'Banka',
                        'Begusarai',
                        'Bhagalpur',
                        'Bhojpur',
                        'Buxar',
                        'Darbhanga',
                        'East Champaran',
                        'Gaya',
                        'Gopalganj',
                        'Jamui',
                        'Jehanabad',
                        'Kaimur',
                        'Katihar',
                        'Khagaria',
                        'Kishanganj',
                        'Lakhisarai',
                        'Madhepura',
                        'Madhubani',
                        'Munger',
                        'Muzaffarpur',
                        'Nalanda',
                        'Nawada',
                        'Patna',
                        'Purnia',
                        'Rohtas',
                        'Saharsa',
                        'Samastipur',
                        'Saran',
                        'Sheikhpura',
                        'Sheohar',
                        'Sitamarhi',
                        'Siwan',
                        'Supaul',
                        'Vaishali',
                        'West Champaran',
                        'Chandigarh',
               
                        'Bastar',
                        'Bijapur',
                        'Bilaspur',
                        'Dantewada',
                        'Dhamtari',
                        'Durg',
                        'Jashpur',
                        'Janjgir-Champa',
                        'Korba',
                        'Koriya',
                        'Kanker',
                        'Kabirdham (Kawardha)',
                        'Mahasamund',
                        'Narayanpur',
                        'Raigarh',
                        'Rajnandgaon',
                        'Raipur',
                        'Surguja',
             
                        'Dadra and Nagar Haveli',
            
                        'Daman',
                        'Diu',
          
                        'Central Delhi',
                        'East Delhi',
                        'New Delhi',
                        'North Delhi',
                        'North East Delhi',
                        'North West Delhi',
                        'South Delhi',
                        'South West Delhi',
                        'West Delhi',
           
                        'North Goa',
                        'South Goa',
          
                        'Ahmedabad',
                        'Amreli district',
                        'Anand',
                        'Banaskantha',
                        'Bharuch',
                        'Bhavnagar',
                        'Dahod',
                        'The Dangs',
                        'Gandhinagar',
                        'Jamnagar',
                        'Junagadh',
                        'Kutch',
                        'Kheda',
                        'Mehsana',
                        'Narmada',
                        'Navsari',
                        'Patan',
                        'Panchmahal',
                        'Porbandar',
                        'Rajkot',
                        'Sabarkantha',
                        'Surendranagar',
                        'Surat',
                        'Vyara',
                        'Vadodara',
                        'Valsad',
           
                        'Ambala',
                        'Bhiwani',
                        'Faridabad',
                        'Fatehabad',
                        'Gurgaon',
                        'Hissar',
                        'Jhajjar',
                        'Jind',
                        'Karnal',
                        'Kaithal',
                        'Kurukshetra',
                        'Mahendragarh',
                        'Mewat',
                        'Palwal',
                        'Panchkula',
                        'Panipat',
                        'Rewari',
                        'Rohtak',
                        'Sirsa',
                        'Sonipat',
                        'Yamuna Nagar',
             
                        'Bilaspur',
                        'Chamba',
                        'Hamirpur',
                        'Kangra',
                        'Kinnaur',
                        'Kullu',
                        'Lahaul and Spiti',
                        'Mandi',
                        'Shimla',
                        'Sirmaur',
                        'Solan',
                        'Una',
              
                        'Anantnag',
                        'Badgam',
                        'Bandipora',
                        'Baramulla',
                        'Doda',
                        'Ganderbal',
                        'Jammu',
                        'Kargil',
                        'Kathua',
                        'Kishtwar',
                        'Kupwara',
                        'Kulgam',
                        'Leh',
                        'Poonch',
                        'Pulwama',
                        'Rajauri',
                        'Ramban',
                        'Reasi',
                        'Samba',
                        'Shopian',
                        'Srinagar',
                        'Udhampur',
               
                        'Bokaro',
                        'Chatra',
                        'Deoghar',
                        'Dhanbad',
                        'Dumka',
                        'East Singhbhum',
                        'Garhwa',
                        'Giridih',
                        'Godda',
                        'Gumla',
                        'Hazaribag',
                        'Jamtara',
                        'Khunti',
                        'Koderma',
                        'Latehar',
                        'Lohardaga',
                        'Pakur',
                        'Palamu',
                        'Ramgarh',
                        'Ranchi',
                        'Sahibganj',
                        'Seraikela Kharsawan',
                        'Simdega',
                        'West Singhbhum',
               
                        'Bagalkot',
                        'Bangalore Rural',
                        'Bangalore Urban',
                        'Belgaum',
                        'Bellary',
                        'Bidar',
                        'Bijapur',
                        'Chamarajnagar',
                        'Chikkamagaluru',
                        'Chikkaballapur',
                        'Chitradurga',
                        'Davanagere',
                        'Dharwad',
                        'Dakshina Kannada',
                        'Gadag',
                        'Gulbarga',
                        'Hassan',
                        'Haveri district',
                        'Kodagu',
                        'Kolar',
                        'Koppal',
                        'Mandya',
                        'Mysore',
                        'Raichur',
                        'Shimoga',
                        'Tumkur',
                        'Udupi',
                        'Uttara Kannada',
                        'Ramanagara',
                        'Yadgir',
               
                        'Alappuzha',
                        'Ernakulam',
                        'Idukki',
                        'Kannur',
                        'Kasaragod',
                        'Kollam',
                        'Kottayam',
                        'Kozhikode',
                        'Malappuram',
                        'Palakkad',
                        'Pathanamthitta',
                        'Thrissur',
                        'Thiruvananthapuram',
                        'Wayanad',
              
                        'Alirajpur',
                        'Anuppur',
                        'Ashok Nagar',
                        'Balaghat',
                        'Barwani',
                        'Betul',
                        'Bhind',
                        'Bhopal',
                        'Burhanpur',
                        'Chhatarpur',
                        'Chhindwara',
                        'Damoh',
                        'Datia',
                        'Dewas',
                        'Dhar',
                        'Dindori',
                        'Guna',
                        'Gwalior',
                        'Harda',
                        'Hoshangabad',
                        'Indore',
                        'Jabalpur',
                        'Jhabua',
                        'Katni',
                        'Khandwa (East Nimar)',
                        'Khargone (West Nimar)',
                        'Mandla',
                        'Mandsaur',
                        'Morena',
                        'Narsinghpur',
                        'Neemuch',
                        'Panna',
                        'Rewa',
                        'Rajgarh',
                        'Ratlam',
                        'Raisen',
                        'Sagar',
                        'Satna',
                        'Sehore',
                        'Seoni',
                        'Shahdol',
                        'Shajapur',
                        'Sheopur',
                        'Shivpuri',
                        'Sidhi',
                        'Singrauli',
                        'Tikamgarh',
                        'Ujjain',
                        'Umaria',
                        'Vidisha',
               
                        'Ahmednagar',
                        'Akola',
                        'Amravati',
                        'Aurangabad',
                        'Bhandara',
                        'Beed',
                        'Buldhana',
                        'Chandrapur',
                        'Dhule',
                        'Gadchiroli',
                        'Gondia',
                        'Hingoli',
                        'Jalgaon',
                        'Jalna',
                        'Kolhapur',
                        'Latur',
                        'Mumbai City',
                        'Mumbai suburban',
                        'Nandurbar',
                        'Nanded',
                        'Nagpur',
                        'Nashik',
                        'Osmanabad',
                        'Parbhani',
                        'Pune',
                        'Raigad',
                        'Ratnagiri',
                        'Sindhudurg',
                        'Sangli',
                        'Solapur',
                        'Satara',
                        'Thane',
                        'Wardha',
                        'Washim',
                        'Yavatmal',
                   
                        'Bishnupur',
                        'Churachandpur',
                        'Chandel',
                        'Imphal East',
                        'Senapati',
                        'Tamenglong',
                        'Thoubal',
                        'Ukhrul',
                        'Imphal West',
             
                        'East Garo Hills',
                        'East Khasi Hills',
                        'Jaintia Hills',
                        'Ri Bhoi',
                        'South Garo Hills',
                        'West Garo Hills',
                        'West Khasi Hills',
              
                        'Aizawl',
                        'Champhai',
                        'Kolasib',
                        'Lawngtlai',
                        'Lunglei',
                        'Mamit',
                        'Saiha',
                        'Serchhip',
               
                        'Dimapur',
                        'Kohima',
                        'Mokokchung',
                        'Mon',
                        'Phek',
                        'Tuensang',
                        'Wokha',
                        'Zunheboto',
             
                        'Angul',
                        'Boudh (Bauda)',
                        'Bhadrak',
                        'Balangir',
                        'Bargarh (Baragarh)',
                        'Balasore',
                        'Cuttack',
                        'Debagarh (Deogarh)',
                        'Dhenkanal',
                        'Ganjam',
                        'Gajapati',
                        'Jharsuguda',
                        'Jajpur',
                        'Jagatsinghpur',
                        'Khordha',
                        'Kendujhar (Keonjhar)',
                        'Kalahandi',
                        'Kandhamal',
                        'Koraput',
                        'Kendrapara',
                        'Malkangiri',
                        'Mayurbhanj',
                        'Nabarangpur',
                        'Nuapada',
                        'Nayagarh',
                        'Puri',
                        'Rayagada',
                        'Sambalpur',
                        'Subarnapur (Sonepur)',
                        'Sundergarh',
                  
                        'Karaikal',
                        'Mahe',
                        'Pondicherry',
                        'Yanam',
          
                        'Amritsar',
                        'Barnala',
                        'Bathinda',
                        'Firozpur',
                        'Faridkot',
                        'Fatehgarh Sahib',
                        'Fazilka',
                        'Gurdaspur',
                        'Hoshiarpur',
                        'Jalandhar',
                        'Kapurthala',
                        'Ludhiana',
                        'Mansa',
                        'Moga',
                        'Sri Muktsar Sahib',
                        'Pathankot',
                        'Patiala',
                        'Rupnagar',
                        'Ajitgarh (Mohali)',
                        'Sangrur',
                        'Nawanshahr',
                        'Tarn Taran',
          
                        'Ajmer',
                        'Alwar',
                        'Bikaner',
                        'Barmer',
                        'Banswara',
                        'Bharatpur',
                        'Baran',
                        'Bundi',
                        'Bhilwara',
                        'Churu',
                        'Chittorgarh',
                        'Dausa',
                        'Dholpur',
                        'Dungapur',
                        'Ganganagar',
                        'Hanumangarh',
                        'Jhunjhunu',
                        'Jalore',
                        'Jodhpur',
                        'Jaipur',
                        'Jaisalmer',
                        'Jhalawar',
                        'Karauli',
                        'Kota',
                        'Nagaur',
                        'Pali',
                        'Pratapgarh',
                        'Rajsamand',
                        'Sikar',
                        'Sawai Madhopur',
                        'Sirohi',
                        'Tonk',
                        'Udaipur',
              
                        'East Sikkim',
                        'North Sikkim',
                        'South Sikkim',
                        'West Sikkim',
              
                        'Ariyalur',
                        'Chennai',
                        'Coimbatore',
                        'Cuddalore',
                        'Dharmapuri',
                        'Dindigul',
                        'Erode',
                        'Kanchipuram',
                        'Kanyakumari',
                        'Karur',
                        'Madurai',
                        'Nagapattinam',
                        'Nilgiris',
                        'Namakkal',
                        'Perambalur',
                        'Pudukkottai',
                        'Ramanathapuram',
                        'Salem',
                        'Sivaganga',
                        'Tirupur',
                        'Tiruchirappalli',
                        'Theni',
                        'Tirunelveli',
                        'Thanjavur',
                        'Thoothukudi',
                        'Tiruvallur',
                        'Tiruvarur',
                        'Tiruvannamalai',
                        'Vellore',
                        'Viluppuram',
                        'Virudhunagar',
               
                        'Dhalai',
                        'North Tripura',
                        'South Tripura',
                        'Khowai',
                        'West Tripura',
             
                        'Agra',
                        'Allahabad',
                        'Aligarh',
                        'Ambedkar Nagar',
                        'Auraiya',
                        'Azamgarh',
                        'Barabanki',
                        'Budaun',
                        'Bagpat',
                        'Bahraich',
                        'Bijnor',
                        'Ballia',
                        'Banda',
                        'Balrampur',
                        'Bareilly',
                        'Basti',
                        'Bulandshahr',
                        'Chandauli',
                        'Chhatrapati Shahuji Maharaj Nagar',
                        'Chitrakoot',
                        'Deoria',
                        'Etah',
                        'Kanshi Ram Nagar',
                        'Etawah',
                        'Firozabad',
                        'Farrukhabad',
                        'Fatehpur',
                        'Faizabad',
                        'Gautam Buddh Nagar',
                        'Gonda',
                        'Ghazipur',
                        'Gorakhpur',
                        'Ghaziabad',
                        'Hamirpur',
                        'Hardoi',
                        'Mahamaya Nagar',
                        'Jhansi',
                        'Jalaun',
                        'Jyotiba Phule Nagar',
                        'Jaunpur district',
                        'Ramabai Nagar (Kanpur Dehat)',
                        'Kannauj',
                        'Kanpur',
                        'Kaushambi',
                        'Kushinagar',
                        'Lalitpur',
                        'Lakhimpur Kheri',
                        'Lucknow',
                        'Mau',
                        'Meerut',
                        'Maharajganj',
                        'Mahoba',
                        'Mirzapur',
                        'Moradabad',
                        'Mainpuri',
                        'Mathura',
                        'Muzaffarnagar',
                        'Panchsheel Nagar district (Hapur)',
                        'Pilibhit',
                        'Shamli',
                        'Pratapgarh',
                        'Rampur',
                        'Raebareli',
                        'Saharanpur',
                        'Sitapur',
                        'Shahjahanpur',
                        'Sant Kabir Nagar',
                        'Siddharthnagar',
                        'Sonbhadra',
                        'Sant Ravidas Nagar',
                        'Sultanpur',
                        'Shravasti',
                        'Unnao',
                        'Varanasi',
               
                        'Almora',
                        'Bageshwar',
                        'Chamoli',
                        'Champawat',
                        'Dehradun',
                        'Haridwar',
                        'Nainital',
                        'Pauri Garhwal',
                        'Pithoragarh',
                        'Rudraprayag',
                        'Tehri Garhwal',
                        'Udham Singh Nagar',
                        'Uttarkashi',
              
                        'Birbhum',
                        'Bankura',
                        'Bardhaman',
                        'Darjeeling',
                        'Dakshin Dinajpur',
                        'Hooghly',
                        'Howrah',
                        'Jalpaiguri',
                        'Cooch Behar',
                        'Kolkata',
                        'Maldah',
                        'Paschim Medinipur',
                        'Purba Medinipur',
                        'Murshidabad',
                        'Nadia',
                        'North 24 Parganas',
                        'South 24 Parganas',
                        'Purulia',
                        'Uttar Dinajpur',
               
            ];
        
    
       
        return response()->json(  $data,200);
    }
    public function couponcode()
    {
      
        $data = couponrate::select('category_id', 'name','rate')->get();
        if($data){
        $State['success']=true;
        $State['message']="Date Fetched  successful";
        $State['coupon']=$data;
        
    }
       
        return response()->json(  $State,200);
    }



    public function saveCoupan(Request $request)
    {
        $coupon = new coupon();
        $coupon->user_id = $request->input('user_id');
        $coupon->category_d = $request->input('category_d');
        $coupon->Category_name = $request->input('Category_name');
        $coupon->Rate = $request->input('Rate');
        $coupon->save();
        if( $coupon){
        return response()->json( "Coupon Saved successfully",200);
    }
}

         function coupon_redeem(Request $request)
            {
                $data=couponrate::get()->where('promo_code',$request->promo_code)->first();
                $isused=coupon::get()->where('user_id',$request->user_id)->where('promo_code',$request->promo_code)->first();
                
                    if (!empty($data)) 
                    {
                                if (empty($isused)) {
                                
                                return  response()->json(["status"=>true,"message"=>"you can use this coupon"]);
                                }
                                else
                                {
                                    return  response()->json(["status"=>false,"message"=>"You have already used this coupon"]);
                                }
                    }  
                    else{
                       return  response()->json(["status"=>false,"message"=>"Invalid Promo Code"]);
                    }                  

                
            }

    
    public function viewbusiness(Request $request)
    {
        $data = Business_register::select('*')->get();
        if( $data){
        return response()->json( $data,200);
    }


    }  

    public function business_profiles(Request $request)
    {
        $data = Business_register::where('user_id',$request->user_id)
                                ->join('country', 'business_register.country_id', '=', 'country.id')
                                ->join('state', 'business_register.state_id', '=', 'state.id')
                                ->join('city', 'business_register.city_id', '=', 'city.id')
                                ->select('business_register.*','country.country','state.state','city.city')
                                ->first();

        

        $path = array("b_lic"=>env('APP_URL').'media/b_lic/'.$data->b_lic,
                "b_card_pic"=>env('APP_URL').'media/b_card_pic/'.$data->b_card_pic,
                 "shop_pic"=>env('APP_URL').'media/shop_pic/'.$data->shop_pic);

        return response()->json(["status" => true,"data" => $data, "message" => "Business retrieved successfully","path" => $path]);
    }
  
     public function show(Request $req)
    {
      /*  $input=$req->all();
        if ($req->input('token')=="vghfrhrweghfrjfrgfhfrhgfrfhvdewfdewu") {
        
        $data=User::get();
        if (!empty($data)) {

            return response()->json(["status"=>true,"message"=>'Data Fetched successfully',"data"=>$data]);

        }
    }
    else
    {
                    return response()->json(["status"=>false,"message"=>'Data not Fetched Use Valid Token']);
    }*/
    echo "string";

    }
  
    public function acceptTerms(Request $request)
    {
        $data = Business_register::where('user_id', $request->input('user_id'))->update(['terms_status' => '1']);
        return response()->json( $data,200);
        
    }
    public function profile360(Request $request)
    {
        // $data=$request->input('product_id');
        $data = DB::table('business_register')->where('user_id', $request->input('user_id'))
            ->orderBy('user_id', 'desc')
            ->first();
        $user_id =$request->input('user_id');

        $users = DB::table('users')->where('id', $user_id)->orderBy('id', 'desc')
            ->first();

        // $user =  User::select('*')->where(['id' => $request->input('product_id')]);
        $role_id = $users->role_id;
        $role = DB::table('couponrates')->where('category_id', $role_id)->orderBy('id', 'desc')
            ->first();
        // dd($role);
        $type = $role->name;
        // $data = Business_register::where('user_id',$request->user_id)
        //                         ->join('country', 'business_register.country_id', '=', 'country.id')
        //                         ->join('state', 'business_register.state_id', '=', 'state.id')
        //                         ->join('city', 'business_register.city_id', '=', 'city.id')
        //                         ->select('business_register.*','country.country','state.state','city.city')
        //                         ->first();
        $city = DB::table('city')->where('id', $data->city_id)
            ->orderBy('id', 'desc')
            ->first();
        $state = DB::table('state')->where('id', $data->state_id)
            ->orderBy('id', 'desc')
            ->first();
        $district = DB::table('district')
        ->where('id', $data->district)
        ->orderBy('id', 'desc')
        ->first();
        $data->cityname = $city->city;
        $data->district_name = $district->district_name;
        $data->statename = $state->state;
        $data->role_ids = $role_id;
        $data->type = $role->name;

        // dd($data);
        $path = array(
            "b_lic" => env('APP_URL') . 'media/b_lic/' . $data->b_lic,
            "b_card_pic" => env('APP_URL') . 'media/b_card_pic/' . $data->b_card_pic,
            "shop_pic" => env('APP_URL') . 'media/shop_pic/' . $data->shop_pic
        );
        //  dd($path);
        if ($data)
        {
            return response()->json(["status" => true,"data" => $data, "message" => "Business retrieved successfully","path" => $path]);
        }

    }
    public function createOffer(Request $request){
        $Create_offer_360 = new Create_offer_360();
        $Create_offer_360->user_id = $request->input('user_id');
        $Create_offer_360->offer_id = $request->input('offer_id');
        $Create_offer_360->created_by = $request->input('user_id');
        $Create_offer_360->updated_by = $request->input('user_id');
        $Create_offer_360->offers = $request->input('offers');
        $Create_offer_360->adoffer = $request->input('adoffer');
        
        $Create_offer_360->save();  
        return response()->json(["Create_offer_360" => $Create_offer_360,"massage"=>"updated  successfully"]);

    }
    public function Wstore(Request $request)
    {
         $book = Wallet_360::where('user_id', $request->user_id)->orderBy('user_id', 'desc')->first();
        if(!empty($book)){
            $book->user_id = $request->input('user_id');
            $book->payment_id = $request->input('payment_id');
            // $book->payment_id = $request->input('payment_id');
            $book->wa_amount =  $request->input('wa_amount') + $book->wa_amount;
            $book->created_by = $request->input('user_id');
            $book->updated_by = $request->input('user_id');
            $book->save();
                

        }else
        {
            $book = new Wallet_360();
            $book->user_id = $request->input('user_id');
            $book->payment_id = $request->input('payment_id');
            $book->created_by = $request->input('user_id');
            $book->wa_amount = $request->input('wa_amount');
            $book->updated_by = $request->input('user_id');
            $book->save();  
        }

        

        return response()->json(["Create_offer_360" => $book,"massage"=>"updated  successfully"]);
    }
    public function wallet_balance_360(Request $request)
    {
        
        $balance =  Wallet_360::where('user_id', $request->user_id)->orderBy('user_id', 'desc')->first();
      
       if ($balance) {
            return response()->json(["status" => true,"massage"=>"Balance retrieved successfully","balance" => $balance]);
        }else{
            return response()->json(["status" => false,"balance" => $balance]);
        }
    }
    public function updateToken(Request $request)
    {
        
        $result = User::where('id', $request->user_id)->update(['mobile_token' =>$request->mobile_token]);
      
       if ($result) {
            return response()->json(["status" => true,"massage"=>"Token Updated","result" => $result]);
        }else{
            return response()->json(["status" => false,"result" => $result]);
        }
    }
    
}
