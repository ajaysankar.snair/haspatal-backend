<?php

namespace App\Http\Controllers;

use App\DataTables\Property_typeDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateProperty_typeRequest;
use App\Http\Requests\UpdateProperty_typeRequest;
use App\Repositories\Property_typeRepository;
use Flash;
use Auth;
use App\Http\Controllers\AppBaseController;
use Response;

class Property_typeController extends AppBaseController
{
    /** @var  Property_typeRepository */
    private $propertyTypeRepository;

    public function __construct(Property_typeRepository $propertyTypeRepo)
    {
        $this->propertyTypeRepository = $propertyTypeRepo;
    }

    /**
     * Display a listing of the Property_type.
     *
     * @param Property_typeDataTable $propertyTypeDataTable
     * @return Response
     */
    public function index(Property_typeDataTable $propertyTypeDataTable)
    {
        return $propertyTypeDataTable->render('property_types.index');
    }

    /**
     * Show the form for creating a new Property_type.
     *
     * @return Response
     */
    public function create()
    {
        return view('property_types.create');
    }

    /**
     * Store a newly created Property_type in storage.
     *
     * @param CreateProperty_typeRequest $request
     *
     * @return Response
     */
    public function store(CreateProperty_typeRequest $request)
    {
        
        $input = $request->all();
        $input['created_by'] = Auth::user()->id;
        $input['updated_by'] = Auth::user()->id;
        $propertyType = $this->propertyTypeRepository->create($input);

        Flash::success('Property Type saved successfully.');

        return redirect(route('propertyTypes.index'));
    }

    /**
     * Display the specified Property_type.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $propertyType = $this->propertyTypeRepository->find($id);

        if (empty($propertyType)) {
            Flash::error('Property Type not found');

            return redirect(route('propertyTypes.index'));
        }

        return view('property_types.show')->with('propertyType', $propertyType);
    }

    /**
     * Show the form for editing the specified Property_type.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $propertyType = $this->propertyTypeRepository->find($id);

        if (empty($propertyType)) {
            Flash::error('Property Type not found');

            return redirect(route('propertyTypes.index'));
        }

        return view('property_types.edit')->with('propertyType', $propertyType);
    }

    /**
     * Update the specified Property_type in storage.
     *
     * @param  int              $id
     * @param UpdateProperty_typeRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateProperty_typeRequest $request)
    {
        $propertyType = $this->propertyTypeRepository->find($id);

        if (empty($propertyType)) {
            Flash::error('Property Type not found');

            return redirect(route('propertyTypes.index'));
        }

        $propertyType = $this->propertyTypeRepository->update($request->all(), $id);

        Flash::success('Property Type updated successfully.');

        return redirect(route('propertyTypes.index'));
    }

    /**
     * Remove the specified Property_type from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $propertyType = $this->propertyTypeRepository->find($id);

        if (empty($propertyType)) {
            Flash::error('Property Type not found');

            return redirect(route('propertyTypes.index'));
        }

        $this->propertyTypeRepository->delete($id);

        Flash::success('Property Type deleted successfully.');

        return redirect(route('propertyTypes.index'));
    }
}
