<?php

namespace App\Http\Controllers;

use App\DataTables\SupportDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateSupportRequest;
use App\Http\Requests\UpdateSupportRequest;
use App\Repositories\SupportRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Auth;


class SupportController extends AppBaseController
{
    /** @var  SupportRepository */
    private $supportRepository;

    public function __construct(SupportRepository $supportRepo)
    {
        $this->supportRepository = $supportRepo;
    }

    /**
     * Display a listing of the Support.
     *
     * @param SupportDataTable $supportDataTable
     * @return Response
     */
    public function index(SupportDataTable $supportDataTable)
    {
        return $supportDataTable->render('supports.index');
    }

    /**
     * Show the form for creating a new Support.
     *
     * @return Response
     */
    public function create()
    {
        return view('supports.create');
    }

    /**
     * Store a newly created Support in storage.
     *
     * @param CreateSupportRequest $request
     *
     * @return Response
     */
    public function store(CreateSupportRequest $request)
    {
        $input = $request->all();
        $input['doctor_id'] = Auth::user()->userDetailsId;
        $input['created_by'] = Auth::user()->id;
        $input['updated_by'] = Auth::user()->id;

        $support = $this->supportRepository->create($input);

         if(Auth::user()->role_id == 1){            
             Flash::success('Support saved successfully.');
             return redirect(route('supports.index'));
        }else{

            Flash::success('Support saved successfully.');
            return back();
        }

        
    }

    /**
     * Display the specified Support.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $support = $this->supportRepository->find($id);

        if (empty($support)) {
            Flash::error('Support not found');

            return redirect(route('supports.index'));
        }

        return view('supports.show')->with('support', $support);
    }

    /**
     * Show the form for editing the specified Support.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $support = $this->supportRepository->find($id);

        if (empty($support)) {
            Flash::error('Support not found');

            return redirect(route('supports.index'));
        }

        return view('supports.edit')->with('support', $support);
    }

    /**
     * Update the specified Support in storage.
     *
     * @param  int              $id
     * @param UpdateSupportRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSupportRequest $request)
    {
        $support = $this->supportRepository->find($id);

        if (empty($support)) {
            Flash::error('Support not found');

            return redirect(route('supports.index'));
        }

        $support = $this->supportRepository->update($request->all(), $id);

        Flash::success('Support updated successfully.');

        return redirect(route('supports.index'));
    }

    /**
     * Remove the specified Support from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $support = $this->supportRepository->find($id);

        if (empty($support)) {
            Flash::error('Support not found');

            return redirect(route('supports.index'));
        }

        $this->supportRepository->delete($id);

        Flash::success('Support deleted successfully.');

        return redirect(route('supports.index'));
    }
}
