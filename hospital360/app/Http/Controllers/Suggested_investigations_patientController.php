<?php

namespace App\Http\Controllers;

use App\DataTables\Suggested_investigations_patientDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateSuggested_investigations_patientRequest;
use App\Http\Requests\UpdateSuggested_investigations_patientRequest;
use App\Repositories\Suggested_investigations_patientRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Auth;
use Illuminate\Http\Request;
class Suggested_investigations_patientController extends AppBaseController
{
    /** @var  Suggested_investigations_patientRepository */
    private $suggestedInvestigationsPatientRepository;

    public function __construct(Suggested_investigations_patientRepository $suggestedInvestigationsPatientRepo)
    {
        $this->suggestedInvestigationsPatientRepository = $suggestedInvestigationsPatientRepo;
    }

    /**
     * Display a listing of the Suggested_investigations_patient.
     *
     * @param Suggested_investigations_patientDataTable $suggestedInvestigationsPatientDataTable
     * @return Response
     */
    public function index(Suggested_investigations_patientDataTable $suggestedInvestigationsPatientDataTable)
    {
        return $suggestedInvestigationsPatientDataTable->render('suggested_investigations_patients.index');
    }

    /**
     * Show the form for creating a new Suggested_investigations_patient.
     *
     * @return Response
     */
    public function create()
    {
        return view('suggested_investigations_patients.create');
    }

    /**
     * Store a newly created Suggested_investigations_patient in storage.
     *
     * @param CreateSuggested_investigations_patientRequest $request
     *
     * @return Response
     */
   public function store(Request $request)
    {
        $input = $request->all();
        $input['created_by'] = Auth::user()->id;
        $input['updated_by'] = Auth::user()->id;
        $suggestedInvestigationsPatient = $this->suggestedInvestigationsPatientRepository->create($input);

        if(Auth::user()->role_id == 1){
        Flash::success('Suggested Investigations Patient saved successfully.');

        return redirect(route('suggestedInvestigationsPatients.index'));
        }else{
            Flash::success('Suggested Investigations saved successfully.');
            return back();
        }
    }

    /**
     * Display the specified Suggested_investigations_patient.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $suggestedInvestigationsPatient = $this->suggestedInvestigationsPatientRepository->find($id);

        if (empty($suggestedInvestigationsPatient)) {
            Flash::error('Suggested Investigations Patient not found');

            return redirect(route('suggestedInvestigationsPatients.index'));
        }

        return view('suggested_investigations_patients.show')->with('suggestedInvestigationsPatient', $suggestedInvestigationsPatient);
    }

    /**
     * Show the form for editing the specified Suggested_investigations_patient.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $suggestedInvestigationsPatient = $this->suggestedInvestigationsPatientRepository->find($id);

        if (empty($suggestedInvestigationsPatient)) {
            Flash::error('Suggested Investigations Patient not found');

            return redirect(route('suggestedInvestigationsPatients.index'));
        }

        return view('suggested_investigations_patients.edit')->with('suggestedInvestigationsPatient', $suggestedInvestigationsPatient);
    }

    /**
     * Update the specified Suggested_investigations_patient in storage.
     *
     * @param  int              $id
     * @param UpdateSuggested_investigations_patientRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSuggested_investigations_patientRequest $request)
    {
        $suggestedInvestigationsPatient = $this->suggestedInvestigationsPatientRepository->find($id);

        if (empty($suggestedInvestigationsPatient)) {
            Flash::error('Suggested Investigations Patient not found');

            return redirect(route('suggestedInvestigationsPatients.index'));
        }

        $suggestedInvestigationsPatient = $this->suggestedInvestigationsPatientRepository->update($request->all(), $id);

        Flash::success('Suggested Investigations Patient updated successfully.');

        return redirect(route('suggestedInvestigationsPatients.index'));
    }

    /**
     * Remove the specified Suggested_investigations_patient from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $suggestedInvestigationsPatient = $this->suggestedInvestigationsPatientRepository->find($id);

        if (empty($suggestedInvestigationsPatient)) {
            Flash::error('Suggested Investigations Patient not found');

            return redirect(route('suggestedInvestigationsPatients.index'));
        }

        $this->suggestedInvestigationsPatientRepository->delete($id);

        Flash::success('Suggested Investigations Patient deleted successfully.');

        return redirect(route('suggestedInvestigationsPatients.index'));
    }
}
