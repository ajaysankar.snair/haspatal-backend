<?php



namespace App\Http\Controllers\API;



use App\Http\Requests\API\CreateReferralSpecialitiesAPIRequest;

use App\Http\Requests\API\UpdateReferralSpecialitiesAPIRequest;

use App\Models\ReferralSpecialities;

use App\Repositories\ReferralSpecialitiesRepository;

use Illuminate\Http\Request;

use App\Http\Controllers\AppBaseController;

use Response;
use DB;
use Auth;


/**

 * Class ReferralSpecialitiesController

 * @package App\Http\Controllers\API

 */



class ReferralSpecialitiesAPIController extends AppBaseController

{

    /** @var  ReferralSpecialitiesRepository */

    private $referralSpecialitiesRepository;



    public function __construct(ReferralSpecialitiesRepository $referralSpecialitiesRepo)

    {

        $this->referralSpecialitiesRepository = $referralSpecialitiesRepo;

    }



    /**

     * Display a listing of the ReferralSpecialities.

     * GET|HEAD /referralSpecialities

     *

     * @param Request $request

     * @return \Illuminate\Http\JsonResponse|Response

     */

    public function index(Request $request)

    {

        $referralSpecialities = $this->referralSpecialitiesRepository->all(

            $request->except(['skip', 'limit']),

            $request->get('skip'),

            $request->get('limit')

        );



        return $this->sendResponse($referralSpecialities->toArray(), 'Referral Specialities retrieved successfully');

    }



    /**

     * Store a newly created ReferralSpecialities in storage.

     * POST /referralSpecialities

     *

     * @param CreateReferralSpecialitiesAPIRequest $request

     *

     * @return \Illuminate\Http\JsonResponse|Response

     */

    public function store(CreateReferralSpecialitiesAPIRequest $request)
    {

        $check =  DB::table('referral_specialities')->where('dr_id',$request->dr_id)->first();

        if ($check) {

            $input = $request->all();

            $update =  DB::table('referral_specialities')->where('dr_id',$request->dr_id)->update(['specialities_name' => $request->specialities_name]);

            $referralSpecialities =  DB::table('referral_specialities')->where('dr_id',$request->dr_id)->first();

            
            return $this->sendResponse($referralSpecialities, 'Referral Specialities saved successfully');
        }else{

            $input = $request->all();

            $input['created_by']= Auth::user()->id;
            $input['updated_by']= Auth::user()->id;
            
            $referralSpecialities = $this->referralSpecialitiesRepository->create($input);
            
            return $this->sendResponse($referralSpecialities->toArray(), 'Referral Specialities saved successfully');
        }






    }



    /**

     * Display the specified ReferralSpecialities.

     * GET|HEAD /referralSpecialities/{id}

     *

     * @param int $id

     *

     * @return \Illuminate\Http\JsonResponse|Response

     */

    public function show($id)

    {

        /** @var ReferralSpecialities $referralSpecialities */

        $referralSpecialities = $this->referralSpecialitiesRepository->find($id);



        if (empty($referralSpecialities)) {

            return $this->sendError('Referral Specialities not found');

        }



        return $this->sendResponse($referralSpecialities->toArray(), 'Referral Specialities retrieved successfully');

    }



    /**

     * Update the specified ReferralSpecialities in storage.

     * PUT/PATCH /referralSpecialities/{id}

     *

     * @param int $id

     * @param UpdateReferralSpecialitiesAPIRequest $request

     *

     * @return \Illuminate\Http\JsonResponse|Response

     */

    public function update($id, UpdateReferralSpecialitiesAPIRequest $request)

    {

        $input = $request->all();



        /** @var ReferralSpecialities $referralSpecialities */

        $referralSpecialities = $this->referralSpecialitiesRepository->find($id);



        if (empty($referralSpecialities)) {

            return $this->sendError('Referral Specialities not found');

        }



        $referralSpecialities = $this->referralSpecialitiesRepository->update($input, $id);



        return $this->sendResponse($referralSpecialities->toArray(), 'ReferralSpecialities updated successfully');

    }



    /**

     * Remove the specified ReferralSpecialities from storage.

     * DELETE /referralSpecialities/{id}

     *

     * @param int $id

     *

     * @throws \Exception

     *

     * @return \Illuminate\Http\JsonResponse|Response

     */

    public function destroy($id)

    {

        /** @var ReferralSpecialities $referralSpecialities */

        $referralSpecialities = $this->referralSpecialitiesRepository->find($id);



        if (empty($referralSpecialities)) {

            return $this->sendError('Referral Specialities not found');

        }



        $referralSpecialities->delete();



        return $this->sendSuccess('Referral Specialities deleted successfully');

    }

}

