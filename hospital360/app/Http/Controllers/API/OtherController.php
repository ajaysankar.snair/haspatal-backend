<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Response;
use App\Models\Follow_users;
use App\User;
use DB;

class OtherController extends AppBaseController
{
    public function follow_users(Request $request)
    {

    	$input = file_get_contents("php://input");
	    $event_json = json_decode($input,true);

	    if(isset($event_json['fb_id']) && isset($event_json['followed_fb_id']) && isset($event_json['status']))
		{
			$fb_id = htmlspecialchars(strip_tags($event_json['fb_id'] , ENT_QUOTES));
			$followed_fb_id = htmlspecialchars(strip_tags($event_json['followed_fb_id'] , ENT_QUOTES));
			$status = htmlspecialchars(strip_tags($event_json['status'] , ENT_QUOTES));

			if($status=="0")
			{
				$result = Follow_users::where("f_user_id",$fb_id)->where("follow_user_id",$followed_fb_id)->delete();

				$output=array("code" => "200", "msg" => "unfollow successful");

				return response()->json(["status" => true,"data" => $output]);
			}else{
				$data_input['f_user_id'] = $fb_id;
				$data_input['follow_user_id'] = $followed_fb_id;


				$result = Follow_users::create($data_input);
				//echo "<pre>";print_r($result);exit;

				if($result)
    			{
    				$output=array( "code" => "200", "msg" => "follow successful");
					return response()->json(["status" => true,"data" => $output]);
    				
    			}else{
    				$output=array( "code" => "200", "msg" => "problem in signup");
					return response()->json(["status" => true,"data" => $output]);
    			}
			}


		}else{
			$output=array( "code" => "201", "msg" => "Json Parem are missing");
			return response()->json(["status" => true,"data" => $output]);

		}

    }

    public function get_followings(Request $request)
    {
    	$input = file_get_contents("php://input");
	    $event_json = json_decode($input,true);

	    if(isset($event_json['fb_id']))
		{
			$fb_id=htmlspecialchars(strip_tags($event_json['fb_id'] , ENT_QUOTES));

			$data = Follow_users::where('f_user_id',$fb_id)->orderBy('f_id', 'desc')->get();

			$array_out = array();
			foreach ($data as $value) {
				$user_details = User::where('id',$value->follow_user_id)->first();
				$loginUser = User::where('id',$value->f_user_id)->first();

				$follow_count = DB::table('follow_users')->where('f_user_id',$fb_id)->where('follow_user_id',$value->follow_user_id)->count();



				if($follow_count == "0")
                {
                	$follow="0";
                	$follow_button_status="Follow";
                }
                elseif($follow_count != "0")
                {
                	$follow="1";
                	$follow_button_status="Unfollow";
                }

                $array_out[] = array(
		        			    "fb_id" => $user_details->id,
							    "username" => $user_details->username,
							    "first_name" => $user_details->first_name,
							    "last_name" => $user_details->last_name,
							    "gender" => $user_details->gender,
							    "bio" => $user_details->bio,
							    "profile_pic" => $user_details->profile_pic,
							    "created" => $user_details->created,
							    "follow_Status" =>array
		                		(
		                			"follow" => $follow,
		                			"follow_status_button" => $follow_button_status
		                		)
		        		);
			}
			if (!empty($array_out)) {
				
				return response()->json(["status" => true,"data" => $array_out]);
			}else{
				return response()->json(["status" => false,"data" => $array_out]);

			}

			

		}else{

			$output=array( "code" => "201", "msg" => "Json Parem are missing");
			return response()->json(["status" => true,"data" => $output]);

		}
    }

    public function get_followers(Request $request)
    {
    	
	    $input = file_get_contents("php://input");
	    $event_json = json_decode($input,true);


	    if(isset($event_json['fb_id']))
		{
			$fb_id=htmlspecialchars(strip_tags($event_json['fb_id'] , ENT_QUOTES));

			$data = Follow_users::where('follow_user_id',$fb_id)->orderBy('f_id', 'desc')->get();

			$array_out = array();
			foreach ($data as $value) {

				$user_details = User::where('id',$value->f_user_id)->first();

				$follow_count = DB::table('follow_users')->where('follow_user_id',$fb_id)->where('f_user_id',$value->follow_user_id)->count();



				if($follow_count == "0")
                {
                	$follow="0";
                	$follow_button_status="Follow";
                }
                elseif($follow_count != "0")
                {
                	$follow="1";
                	$follow_button_status="Unfollow";
                }

                $array_out[] = array(
		        			    "fb_id" => $user_details->id,
							    "username" => $user_details->username,
							    "first_name" => $user_details->first_name,
							    "last_name" => $user_details->last_name,
							    "gender" => $user_details->gender,
							    "bio" => $user_details->bio,
							    "profile_pic" => $user_details->profile_pic,
							    "created" => $user_details->created,
							    "follow_Status" =>array
		                		(
		                			"follow" => $follow,
		                			"follow_status_button" => $follow_button_status
		                		)
		        		);
			}
			if (!empty($array_out)) {
				
				return response()->json(["status" => true,"data" => $array_out]);
			}else{
				return response()->json(["status" => false,"data" => $array_out]);

			}

			
		}else{

			$output=array( "code" => "201", "msg" => "Json Parem are missing");
			return response()->json(["status" => true,"data" => $output]);

		}
    }
}
