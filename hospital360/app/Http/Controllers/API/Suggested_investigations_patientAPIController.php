<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateSuggested_investigations_patientAPIRequest;
use App\Http\Requests\API\UpdateSuggested_investigations_patientAPIRequest;
use App\Models\Suggested_investigations_patient;
use App\Repositories\Suggested_investigations_patientRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;
use DB;
use Auth;

/**
 * Class Suggested_investigations_patientController
 * @package App\Http\Controllers\API
 */

class Suggested_investigations_patientAPIController extends AppBaseController
{
    /** @var  Suggested_investigations_patientRepository */
    private $suggestedInvestigationsPatientRepository;

    public function __construct(Suggested_investigations_patientRepository $suggestedInvestigationsPatientRepo)
    {
        $this->suggestedInvestigationsPatientRepository = $suggestedInvestigationsPatientRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/suggestedInvestigationsPatients",
     *      summary="Get a listing of the Suggested_investigations_patients.",
     *      tags={"Suggested_investigations_patient"},
     *      description="Get all Suggested_investigations_patients",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Suggested_investigations_patient")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $suggestedInvestigationsPatients = $this->suggestedInvestigationsPatientRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($suggestedInvestigationsPatients->toArray(), 'Suggested Investigations Patients retrieved successfully');
    }

    /**
     * @param CreateSuggested_investigations_patientAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/suggestedInvestigationsPatients",
     *      summary="Store a newly created Suggested_investigations_patient in storage",
     *      tags={"Suggested_investigations_patient"},
     *      description="Store Suggested_investigations_patient",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Suggested_investigations_patient that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Suggested_investigations_patient")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Suggested_investigations_patient"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(Request $request)
    {

     $book =  DB::table('suggested_investigations_patient')->where('s_book_id', $request->s_book_id)->orderBy('s_book_id', 'desc')->first();
        if(!empty($book)){
            $input = $request->all();
            $id = $book->id;
           $suggestedInvestigationsPatient = $this->suggestedInvestigationsPatientRepository->update($input, $id);

        }else
        {

        $input = $request->all();
        $input['created_by'] = Auth::user()->id;
        $input['updated_by'] = Auth::user()->id;
        $suggestedInvestigationsPatient = $this->suggestedInvestigationsPatientRepository->create($input);
    }
        return $this->sendResponse($suggestedInvestigationsPatient->toArray(), 'Suggested Investigations Patient saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/suggestedInvestigationsPatients/{id}",
     *      summary="Display the specified Suggested_investigations_patient",
     *      tags={"Suggested_investigations_patient"},
     *      description="Get Suggested_investigations_patient",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Suggested_investigations_patient",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Suggested_investigations_patient"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Suggested_investigations_patient $suggestedInvestigationsPatient */
        $suggestedInvestigationsPatient = $this->suggestedInvestigationsPatientRepository->find($id);

        if (empty($suggestedInvestigationsPatient)) {
            return $this->sendError('Suggested Investigations Patient not found');
        }

        return $this->sendResponse($suggestedInvestigationsPatient->toArray(), 'Suggested Investigations Patient retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateSuggested_investigations_patientAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/suggestedInvestigationsPatients/{id}",
     *      summary="Update the specified Suggested_investigations_patient in storage",
     *      tags={"Suggested_investigations_patient"},
     *      description="Update Suggested_investigations_patient",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Suggested_investigations_patient",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Suggested_investigations_patient that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Suggested_investigations_patient")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Suggested_investigations_patient"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateSuggested_investigations_patientAPIRequest $request)
    {
        $input = $request->all();

        /** @var Suggested_investigations_patient $suggestedInvestigationsPatient */
        $suggestedInvestigationsPatient = $this->suggestedInvestigationsPatientRepository->find($id);

        if (empty($suggestedInvestigationsPatient)) {
            return $this->sendError('Suggested Investigations Patient not found');
        }

        $suggestedInvestigationsPatient = $this->suggestedInvestigationsPatientRepository->update($input, $id);

        return $this->sendResponse($suggestedInvestigationsPatient->toArray(), 'Suggested_investigations_patient updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/suggestedInvestigationsPatients/{id}",
     *      summary="Remove the specified Suggested_investigations_patient from storage",
     *      tags={"Suggested_investigations_patient"},
     *      description="Delete Suggested_investigations_patient",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Suggested_investigations_patient",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Suggested_investigations_patient $suggestedInvestigationsPatient */
        $suggestedInvestigationsPatient = $this->suggestedInvestigationsPatientRepository->find($id);

        if (empty($suggestedInvestigationsPatient)) {
            return $this->sendError('Suggested Investigations Patient not found');
        }

        $suggestedInvestigationsPatient->delete();

        return $this->sendSuccess('Suggested Investigations Patient deleted successfully');
    }
}
