<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateFollow_usersAPIRequest;
use App\Http\Requests\API\UpdateFollow_usersAPIRequest;
use App\Models\Follow_users;
use App\Repositories\Follow_usersRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class Follow_usersController
 * @package App\Http\Controllers\API
 */

class Follow_usersAPIController extends AppBaseController
{
    /** @var  Follow_usersRepository */
    private $followUsersRepository;

    public function __construct(Follow_usersRepository $followUsersRepo)
    {
        $this->followUsersRepository = $followUsersRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/followUsers",
     *      summary="Get a listing of the Follow_users.",
     *      tags={"Follow_users"},
     *      description="Get all Follow_users",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Follow_users")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $followUsers = $this->followUsersRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($followUsers->toArray(), 'Follow Users retrieved successfully');
    }

    /**
     * @param CreateFollow_usersAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/followUsers",
     *      summary="Store a newly created Follow_users in storage",
     *      tags={"Follow_users"},
     *      description="Store Follow_users",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Follow_users that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Follow_users")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Follow_users"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateFollow_usersAPIRequest $request)
    {
        $input = $request->all();

        $followUsers = $this->followUsersRepository->create($input);

        return $this->sendResponse($followUsers->toArray(), 'Follow Users saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/followUsers/{id}",
     *      summary="Display the specified Follow_users",
     *      tags={"Follow_users"},
     *      description="Get Follow_users",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Follow_users",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Follow_users"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Follow_users $followUsers */
        $followUsers = $this->followUsersRepository->find($id);

        if (empty($followUsers)) {
            return $this->sendError('Follow Users not found');
        }

        return $this->sendResponse($followUsers->toArray(), 'Follow Users retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateFollow_usersAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/followUsers/{id}",
     *      summary="Update the specified Follow_users in storage",
     *      tags={"Follow_users"},
     *      description="Update Follow_users",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Follow_users",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Follow_users that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Follow_users")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Follow_users"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateFollow_usersAPIRequest $request)
    {
        $input = $request->all();

        /** @var Follow_users $followUsers */
        $followUsers = $this->followUsersRepository->find($id);

        if (empty($followUsers)) {
            return $this->sendError('Follow Users not found');
        }

        $followUsers = $this->followUsersRepository->update($input, $id);

        return $this->sendResponse($followUsers->toArray(), 'Follow_users updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/followUsers/{id}",
     *      summary="Remove the specified Follow_users from storage",
     *      tags={"Follow_users"},
     *      description="Delete Follow_users",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Follow_users",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Follow_users $followUsers */
        $followUsers = $this->followUsersRepository->find($id);

        if (empty($followUsers)) {
            return $this->sendError('Follow Users not found');
        }

        $followUsers->delete();

        return $this->sendSuccess('Follow Users deleted successfully');
    }
}
