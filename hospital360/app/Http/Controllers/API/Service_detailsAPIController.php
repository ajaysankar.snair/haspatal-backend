<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateService_detailsAPIRequest;
use App\Http\Requests\API\UpdateService_detailsAPIRequest;
use App\Models\Service_details;
use App\Repositories\Service_detailsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class Service_detailsController
 * @package App\Http\Controllers\API
 */

class Service_detailsAPIController extends AppBaseController
{
    /** @var  Service_detailsRepository */
    private $serviceDetailsRepository;

    public function __construct(Service_detailsRepository $serviceDetailsRepo)
    {
        $this->serviceDetailsRepository = $serviceDetailsRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/serviceDetails",
     *      summary="Get a listing of the Service_details.",
     *      tags={"Service_details"},
     *      description="Get all Service_details",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Service_details")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $serviceDetails = $this->serviceDetailsRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($serviceDetails->toArray(), 'Service Details retrieved successfully');
    }

    /**
     * @param CreateService_detailsAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/serviceDetails",
     *      summary="Store a newly created Service_details in storage",
     *      tags={"Service_details"},
     *      description="Store Service_details",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Service_details that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Service_details")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Service_details"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateService_detailsAPIRequest $request)
    {
        $input = $request->all();

        $serviceDetails = $this->serviceDetailsRepository->create($input);

        return $this->sendResponse($serviceDetails->toArray(), 'Service Details saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/serviceDetails/{id}",
     *      summary="Display the specified Service_details",
     *      tags={"Service_details"},
     *      description="Get Service_details",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Service_details",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Service_details"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Service_details $serviceDetails */
        $serviceDetails = $this->serviceDetailsRepository->find($id);

        if (empty($serviceDetails)) {
            return $this->sendError('Service Details not found');
        }

        return $this->sendResponse($serviceDetails->toArray(), 'Service Details retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateService_detailsAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/serviceDetails/{id}",
     *      summary="Update the specified Service_details in storage",
     *      tags={"Service_details"},
     *      description="Update Service_details",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Service_details",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Service_details that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Service_details")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Service_details"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateService_detailsAPIRequest $request)
    {
        $input = $request->all();

        /** @var Service_details $serviceDetails */
        $serviceDetails = $this->serviceDetailsRepository->find($id);

        if (empty($serviceDetails)) {
            return $this->sendError('Service Details not found');
        }

        $serviceDetails = $this->serviceDetailsRepository->update($input, $id);

        return $this->sendResponse($serviceDetails->toArray(), 'Service_details updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/serviceDetails/{id}",
     *      summary="Remove the specified Service_details from storage",
     *      tags={"Service_details"},
     *      description="Delete Service_details",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Service_details",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Service_details $serviceDetails */
        $serviceDetails = $this->serviceDetailsRepository->find($id);

        if (empty($serviceDetails)) {
            return $this->sendError('Service Details not found');
        }

        $serviceDetails->delete();

        return $this->sendSuccess('Service Details deleted successfully');
    }
}
