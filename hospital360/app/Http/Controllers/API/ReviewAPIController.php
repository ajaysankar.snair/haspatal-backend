<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateReviewAPIRequest;
use App\Http\Requests\API\UpdateReviewAPIRequest;
use App\Models\Review;
use App\Repositories\ReviewRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;
use Auth;
use DB;

/**
 * Class ReviewController
 * @package App\Http\Controllers\API
 */

class ReviewAPIController extends AppBaseController
{
    /** @var  ReviewRepository */
    private $reviewRepository;

    public function __construct(ReviewRepository $reviewRepo)
    {
        $this->reviewRepository = $reviewRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/reviews",
     *      summary="Get a listing of the Reviews.",
     *      tags={"Review"},
     *      description="Get all Reviews",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Review")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $reviews = $this->reviewRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($reviews->toArray(), 'Reviews retrieved successfully');
    }

    /**
     * @param CreateReviewAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/reviews",
     *      summary="Store a newly created Review in storage",
     *      tags={"Review"},
     *      description="Store Review",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Review that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Review")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Review"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(Request $request)
    {
         $book =  DB::table('review')->where('book_id', $request->book_id)->orderBy('book_id', 'desc')->first();
         if(!empty($book)){
            $input = $request->all();
            // $input['admin_status'] = 0;
            $id = $book->id;
            $review = $this->reviewRepository->update($input, $id);
            $review->admin_status=0;
            DB::table('review')->where('id', $review->id)->update(['admin_status' => 0]);

        }else{
            $input = $request->all();
            $input['created_by'] = Auth::user()->id;
            $input['updated_by'] = Auth::user()->id;
            $review = $this->reviewRepository->create($input);
            if($request->doctor_f_name){
                DB::table('review')->where('id', $review->id)->update(['patient_f_name' => $request->patient_f_name,'doctor_f_name' => $request->doctor_f_name]);
            }
           
        }
        return $this->sendResponse($review->toArray(), 'Review saved successfully');
    }

    public function hasptal_review(Request $request)
    {
        /* $book =  DB::table('review_hospital')->where('book_id', $request->book_id)->orderBy('book_id', 'desc')->first();
         if(!empty($book)){
            $input = $request->all();
            $id = $book->id;
            $review = $this->reviewRepository->update($input, $id);

        }else{
        }*/
            $input = $request->all();
            $input['created_by'] = Auth::user()->id;
            $input['updated_by'] = Auth::user()->id;
            
            $data = DB::table('review_hospital')->insertGetId(['patient_id' => $input['patient_id'], 'hospital_id' => $input['hospital_id'],'rating' => $input['rating'], 'review_que' => $input['review_que'], 'created_by' => $input['created_by'],'updated_by' => $input['updated_by']]);
            
            $book =  DB::table('review_hospital')->where('id', $data)->orderBy('book_id', 'desc')->first();
        return $this->sendResponse($book, 'Review saved successfully');
    }

    public function review_list(Request $request)
    {
        $book =  DB::table('review_hospital')->where('patient_id', $request->patient_id)->where('hospital_id', $request->hospital_id)->orderBy('book_id', 'desc')->get();
        return $this->sendResponse($book, 'Review fetch successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/reviews/{id}",
     *      summary="Display the specified Review",
     *      tags={"Review"},
     *      description="Get Review",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Review",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Review"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Review $review */
        $review = $this->reviewRepository->find($id);

        if (empty($review)) {
            return $this->sendError('Review not found');
        }

        return $this->sendResponse($review->toArray(), 'Review retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateReviewAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/reviews/{id}",
     *      summary="Update the specified Review in storage",
     *      tags={"Review"},
     *      description="Update Review",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Review",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Review that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Review")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Review"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateReviewAPIRequest $request)
    {
        $input = $request->all();

        /** @var Review $review */
        $review = $this->reviewRepository->find($id);

        if (empty($review)) {
            return $this->sendError('Review not found');
        }

        $review = $this->reviewRepository->update($input, $id);

        return $this->sendResponse($review->toArray(), 'Review updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/reviews/{id}",
     *      summary="Remove the specified Review from storage",
     *      tags={"Review"},
     *      description="Delete Review",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Review",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Review $review */
        $review = $this->reviewRepository->find($id);

        if (empty($review)) {
            return $this->sendError('Review not found');
        }

        $review->delete();

        return $this->sendSuccess('Review deleted successfully');
    }
    public function review_doctor_update_list(Request $request)
    {
        $input = $request->all();
       
        $avgStar = Review::where('doctor_id',$input['doctor_id'])->where('admin_status',1)->avg('rating');
        if($avgStar){
            $avgStar= $avgStar;
        }else{
            $avgStar= 0;
        }
        $totalStar = Review::where('doctor_id',$input['doctor_id'])->where('admin_status',1)->sum('rating');
        if($totalStar){
            $totalStar= $totalStar;
        }else{
            $totalStar= 0;
        }
        $countStar =  Review::where('doctor_id',$input['doctor_id'])->where('admin_status',1)->count('rating');;  
        if($countStar){
            $countStar= $countStar;
        }else{
            $countStar= 0;
        }
        $scoreboard=$totalStar/$countStar*5;
        return response()->json(["success" => true,"review_star" => (int)$avgStar,"totalStar" => (int)$totalStar,"countStar" => (int)$countStar,"scoreboard" => (int)$scoreboard]); 
     


    }
    public function review_list_patient(Request $request)
    {
        
        $myFavourites = Review::join('doctor_details', 'review.doctor_id', '=', 'doctor_details.id')
                        ->join('patient_details', 'review.patient_id', '=', 'patient_details.id')
                        ->select('review.*','patient_details.first_name as patient')
                        ->where('review.doctor_id',$request->doctor_id)
                        ->where('review.admin_status',1)
                        // ->where('my_favourite.deleted_at',null)  
                        ->get(); 
        return $this->sendResponse($myFavourites->toArray(), 'My Favourites retrieved successfully');
    }
}
