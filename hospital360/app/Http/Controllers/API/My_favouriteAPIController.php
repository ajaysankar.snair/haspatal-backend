<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateMy_favouriteAPIRequest;
use App\Http\Requests\API\UpdateMy_favouriteAPIRequest;
use App\Models\My_favourite;
use App\Repositories\My_favouriteRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;
use DB;

/**
 * Class My_favouriteController
 * @package App\Http\Controllers\API
 */

class My_favouriteAPIController extends AppBaseController
{
    /** @var  My_favouriteRepository */
    private $myFavouriteRepository;

    public function __construct(My_favouriteRepository $myFavouriteRepo)
    {
        $this->myFavouriteRepository = $myFavouriteRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/myFavourites",
     *      summary="Get a listing of the My_favourites.",
     *      tags={"My_favourite"},
     *      description="Get all My_favourites",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/My_favourite")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        /*$myFavourites = $this->myFavouriteRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );*/
        $myFavourites = My_favourite::join('patient_details', 'my_favourite.patient_id', '=', 'patient_details.id')
                        ->join('doctor_details', 'my_favourite.doctor_id', '=', 'doctor_details.id')
                        ->select('my_favourite.*','patient_details.first_name as patient','doctor_details.first_name','doctor_details.last_name')
                        //->where('my_favourite.patient_id',$request->patient_id)  
                        ->get(); 
        return $this->sendResponse($myFavourites->toArray(), 'My Favourites retrieved successfully');
    }
    public function my_favourites_patient(Request $request)
    {
        
        $myFavourites = My_favourite::join('patient_details', 'my_favourite.patient_id', '=', 'patient_details.id')
                        ->join('doctor_details', 'my_favourite.doctor_id', '=', 'doctor_details.id')
                        ->select('my_favourite.*','patient_details.first_name as patient','doctor_details.*','doctor_details.first_name','doctor_details.last_name')
                        ->where('my_favourite.patient_id',$request->patient_id)
                        ->where('my_favourite.status',$request->status)
                        // ->where('my_favourite.deleted_at',null)  
                        ->get(); 
        return $this->sendResponse($myFavourites->toArray(), 'My Favourites retrieved successfully');
    }
    public function my_favourites_patient_id(Request $request)
    {
        $admin_price = DB::table('booking_price_admin')->where('id',7)->select('booking_price_admin.book_price')->first();
        
         $myFavourite =  DB::table('my_favourite')
                        ->join('patient_details', 'my_favourite.patient_id', '=', 'patient_details.id')
                        ->join('doctor_details', 'my_favourite.doctor_id', '=', 'doctor_details.id')
                        ->where('my_favourite.deleted_at',null)
                        ->where('my_favourite.patient_id',$request->patient_id)
                        ->select('my_favourite.id as my_id','my_favourite.*','doctor_details.*','patient_details.first_name as patient')
                        ->get();

         $path = array("licence_copy"=>env('APP_URL').'public/media/licence_copy',
                            "profile_pic"=>env('APP_URL').'public/media/profile_pic',
                            "clinic_logo"=>env('APP_URL').'public/media/clinic_logo'
                        );
      if ($myFavourite) {
            return response()->json(["status" => true,"data" => $myFavourite, "message" => "My Favourites retrieved successfully","path" => $path,'admin_price'=>$admin_price]);
        }else{
            return response()->json(["status" => false,"data" => ''     , "message" => "My Favourites Not retrieved successfully"]);
        }
        /*return $this->sendResponse($myFavourite->toArray(), 'My Favourites retrieved successfully');*/
    }
    /**
     * @param Createmy_favouriteAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/myFavourites",
     *      summary="Store a newly created My_favourite in storage",
     *      tags={"My_favourite"},
     *      description="Store My_favourite",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="My_favourite that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/My_favourite")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/My_favourite"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateMy_favouriteAPIRequest $request)
    {
        $input = $request->all();
        /*echo "<pre>";
        print_r($input['doctor_id']); exit;*/
        $myFavourite1 = DB::table('my_favourite')->where('doctor_id',$input['doctor_id'])->where('patient_id',$input['patient_id'])->first();
        /*echo "<pre>";
        print_r($myFavourite1->patient_id); exit;*/
        if(isset($myFavourite1->doctor_id) != $input['doctor_id'] && isset($myFavourite1->patient_id) != $input['patient_id'])
        {
            $myFavourite = $this->myFavouriteRepository->create($input);
            return $this->sendResponse($myFavourite->toArray(), 'My Favourite saved successfully');
        }else{
            //return $this->sendResponse('', 'My Favourite alredy add successfully');
            return response()->json(["success" => false,"data" => ''     , "message" => "My Favourite alredy add successfully"]);
        }


    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/myFavourites/{id}",
     *      summary="Display the specified My_favourite",
     *      tags={"My_favourite"},
     *      description="Get My_favourite",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of My_favourite",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/My_favourite"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
       // echo $id; exit;
        /** @var My_favourite $myFavourite */
        $myFavourite = $this->myFavouriteRepository->find($id);
       

        if (empty($myFavourite)) {
            return $this->sendError('My Favourite not found');
        }

        return $this->sendResponse($myFavourite->toArray(), 'My Favourite retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateMy_favouriteAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/myFavourites/{id}",
     *      summary="Update the specified My_favourite in storage",
     *      tags={"My_favourite"},
     *      description="Update My_favourite",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of My_favourite",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="My_favourite that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/My_favourite")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/My_favourite"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateMy_favouriteAPIRequest $request)
    {
        $input = $request->all();

        /** @var My_favourite $myFavourite */
        $myFavourite = $this->myFavouriteRepository->find($id);

        if (empty($myFavourite)) {
            return $this->sendError('My Favourite not found');
        }

        $myFavourite = $this->myFavouriteRepository->update($input, $id);

        return $this->sendResponse($myFavourite->toArray(), 'My_favourite updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/myFavourites/{id}",
     *      summary="Remove the specified My_favourite from storage",
     *      tags={"My_favourite"},
     *      description="Delete My_favourite",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of My_favourite",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var My_favourite $myFavourite */
        $myFavourite = $this->myFavouriteRepository->find($id);

        if (empty($myFavourite)) {
            return $this->sendError('My Favourite not found');
        }

        $myFavourite->delete();

        return $this->sendSuccess('My Favourite deleted successfully');
    }
    public function my_favourites_create_doctor(CreateMy_favouriteAPIRequest $request)
    {
        $input = $request->all();
      
        /*echo "<pre>";
        print_r($input['doctor_id']); exit;*/
        $myFavourite1 = DB::table('my_favourite')->where('doctor_id',$input['doctor_id'])->where('patient_id',$input['patient_id'])->first();
        // return  $myFavourite1;
        /*echo "<pre>";
        print_r($myFavourite1->patient_id); exit;*/
        if(isset($myFavourite1->doctor_id) != $input['doctor_id'] && isset($myFavourite1->patient_id) != $input['patient_id'])
        {
            $input['status']=2;
            $myFavourite = $this->myFavouriteRepository->create($input);
            return $this->sendResponse($myFavourite->toArray(), 'My Favourite saved successfully');
        }else{
            if($input['status']==0){
                $myFavourite1->status=0;
                DB::table('my_favourite')->where('id', $myFavourite1->id)->update(['status' => 0]);
                return response()->json(["success" => true,"data" => $myFavourite1    , "message" => "Removed from favourites"]);
            }else if($input['status']==2){
                
                    $myFavourite1->status=2;
                    DB::table('my_favourite')->where('id', $myFavourite1->id)->update(['status' => 2]);
                    return response()->json(["success" => true,"data" => $myFavourite1    , "message" => "Add  as My Favourite"]);
                
            }else{
                return response()->json(["success" => false,"data" => $myFavourite1    , "message" => "My Favourite already add successfully"]);
            }
            //return $this->sendResponse('', 'My Favourite alredy add successfully');
           
        }


    }
  
    public function my_favourites_update_list(Request $request)
    {
        $input = $request->all();
       
        $myFavourite1 = DB::table('my_favourite')->where('doctor_id',$input['doctor_id'])->where('patient_id',$input['patient_id'])->first();
        /*echo "<pre>";
        print_r($myFavourite1->patient_id); exit;*/
        if(isset($myFavourite1->doctor_id) == $input['doctor_id'] && isset($myFavourite1->patient_id) == $input['patient_id'])
        {
            
            return response()->json(["success" => false,"status" => $myFavourite1->status    , "message" => "Found as Favourite"]);
        }else{
            //return $this->sendResponse('', 'My Favourite alredy add successfully');
            return response()->json(["success" => false,"status" => 0     , "message" => "You can add as  favourite"]);
        }


    }
    public function my_favourites_create_pharamacy(Request $request)
    {
        $input = $request->all();
     
        /*echo "<pre>";
        print_r($input['doctor_id']); exit;*/
        $myFavourite1 = DB::table('my_favourite')->where('pharmacy_id',$input['pharmacy_id'])->where('patient_id',$input['patient_id'])->first();
        /*echo "<pre>";
        print_r($myFavourite1->patient_id); exit;*/
        if(isset($myFavourite1->pharmacy_id) != $input['pharmacy_id'] && isset($myFavourite1->patient_id) != $input['patient_id'])
        {   $input['status']=1;
            $myFavourite = $this->myFavouriteRepository->create($input);
            $myFavourite->pharmacy_id= $input['pharmacy_id'];
            $myFavourite->save();
            return $this->sendResponse($myFavourite->toArray(), 'My Favourite saved successfully');
        }else{

             if($input['status']==0){
                $myFavourite1->status=0;
                DB::table('my_favourite')->where('id', $myFavourite1->id)->update(['status' => 0]);
                return response()->json(["success" => false,"data" => $myFavourite1    , "message" => "Removed from favourites"]);
            }else if($input['status']==1){
                
                    $myFavourite1->status=1;
                    DB::table('my_favourite')->where('id', $myFavourite1->id)->update(['status' => 1]);
                    return response()->json(["success" => false,"data" => $myFavourite1    , "message" => "Add  as My Favourite"]);
                
            }else{
                return response()->json(["success" => false,"data" => $myFavourite1    , "message" => "My Favourite already add successfully"]);
            }
        }


    }
    public function my_favourites_pharamcy(Request $request)
    {
        
        $myFavourites = My_favourite::join('patient_details', 'my_favourite.patient_id', '=', 'patient_details.id')
                        ->join('business_register', 'my_favourite.pharmacy_id', '=', 'business_register.id')
                        ->select('my_favourite.*','patient_details.first_name as patient','business_register.*')
                        ->where('my_favourite.patient_id',$request->patient_id)
                        ->where('my_favourite.status',$request->status)
                        // ->where('my_favourite.deleted_at',null)  
                        ->get(); 
        return $this->sendResponse($myFavourites->toArray(), 'My Favourites retrieved successfully');
    }
    public function my_favourites_create_lab(Request $request)
    { $input = $request->all();
     
        /*echo "<pre>";
        print_r($input['doctor_id']); exit;*/
        $myFavourite1 = DB::table('my_favourite')->where('pharmacy_id',$input['pharmacy_id'])->where('patient_id',$input['patient_id'])->first();
        /*echo "<pre>";
        print_r($myFavourite1->patient_id); exit;*/
        if(isset($myFavourite1->pharmacy_id) != $input['pharmacy_id'] && isset($myFavourite1->patient_id) != $input['patient_id'])
        {   $input['status']=3;
            $myFavourite = $this->myFavouriteRepository->create($input);
            $myFavourite->pharmacy_id= $input['pharmacy_id'];
            $myFavourite->save();
            return $this->sendResponse($myFavourite->toArray(), 'My Favourite saved successfully');
        }else{

             if($input['status']==0){
                $myFavourite1->status=0;
                DB::table('my_favourite')->where('id', $myFavourite1->id)->update(['status' => 0]);
                return response()->json(["success" => false,"data" => $myFavourite1    , "message" => "Removed from favourites"]);
            }else if($input['status']==3){
                
                    $myFavourite1->status=3;
                    DB::table('my_favourite')->where('id', $myFavourite1->id)->update(['status' => 3]);
                    return response()->json(["success" => false,"data" => $myFavourite1    , "message" => "Add  as My Favourite"]);
                
            }else{
                return response()->json(["success" => false,"data" => $myFavourite1    , "message" => "My Favourite already add successfully"]);
            }
        }


    }
    public function my_favourites_create_imagine(Request $request)
    { $input = $request->all();
     
        /*echo "<pre>";
        print_r($input['doctor_id']); exit;*/
        $myFavourite1 = DB::table('my_favourite')->where('pharmacy_id',$input['pharmacy_id'])->where('patient_id',$input['patient_id'])->first();
        /*echo "<pre>";
        print_r($myFavourite1->patient_id); exit;*/
        if(isset($myFavourite1->pharmacy_id) != $input['pharmacy_id'] && isset($myFavourite1->patient_id) != $input['patient_id'])
        {   $input['status']=4;
            $myFavourite = $this->myFavouriteRepository->create($input);
            $myFavourite->pharmacy_id= $input['pharmacy_id'];
            $myFavourite->save();
            return $this->sendResponse($myFavourite->toArray(), 'My Favourite saved successfully');
        }else{

             if($input['status']==0){
                $myFavourite1->status=0;
                DB::table('my_favourite')->where('id', $myFavourite1->id)->update(['status' => 0]);
                return response()->json(["success" => false,"data" => $myFavourite1    , "message" => "Removed from favourites"]);
            }else if($input['status']==4){
                
                    $myFavourite1->status=4;
                    DB::table('my_favourite')->where('id', $myFavourite1->id)->update(['status' => 4]);
                    return response()->json(["success" => false,"data" => $myFavourite1    , "message" => "Add  as My Favourite"]);
                
            }else{
                return response()->json(["success" => false,"data" => $myFavourite1    , "message" => "My Favourite already add successfully"]);
            }
        }


    }
    public function my_favourites_create_theraphy(Request $request)
    {
         $input = $request->all();
     
        /*echo "<pre>";
        print_r($input['doctor_id']); exit;*/
        $myFavourite1 = DB::table('my_favourite')->where('pharmacy_id',$input['pharmacy_id'])->where('patient_id',$input['patient_id'])->first();
        /*echo "<pre>";
        print_r($myFavourite1->patient_id); exit;*/
        if(isset($myFavourite1->pharmacy_id) != $input['pharmacy_id'] && isset($myFavourite1->patient_id) != $input['patient_id'])
        {   $input['status']=5;
            $myFavourite = $this->myFavouriteRepository->create($input);
            $myFavourite->pharmacy_id= $input['pharmacy_id'];
            $myFavourite->save();
            return $this->sendResponse($myFavourite->toArray(), 'My Favourite saved successfully');
        }else{

             if($input['status']==0){
                $myFavourite1->status=0;
                DB::table('my_favourite')->where('id', $myFavourite1->id)->update(['status' => 0]);
                return response()->json(["success" => false,"data" => $myFavourite1    , "message" => "Removed from favourites"]);
            }else if($input['status']==5){
                
                    $myFavourite1->status=5;
                    DB::table('my_favourite')->where('id', $myFavourite1->id)->update(['status' => 5]);
                    return response()->json(["success" => false,"data" => $myFavourite1    , "message" => "Add  as My Favourite"]);
                
            }else{
                return response()->json(["success" => false,"data" => $myFavourite1    , "message" => "My Favourite already add successfully"]);
            }
        }


    }
    public function my_favourites_create_home(Request $request)
    {
         $input = $request->all();
     
        /*echo "<pre>";
        print_r($input['doctor_id']); exit;*/
        $myFavourite1 = DB::table('my_favourite')->where('pharmacy_id',$input['pharmacy_id'])->where('patient_id',$input['patient_id'])->first();
        /*echo "<pre>";
        print_r($myFavourite1->patient_id); exit;*/
        if(isset($myFavourite1->pharmacy_id) != $input['pharmacy_id'] && isset($myFavourite1->patient_id) != $input['patient_id'])
        {   $input['status']=6;
            $myFavourite = $this->myFavouriteRepository->create($input);
            $myFavourite->pharmacy_id= $input['pharmacy_id'];
            $myFavourite->save();
            return $this->sendResponse($myFavourite->toArray(), 'My Favourite saved successfully');
        }else{

             if($input['status']==0){
                $myFavourite1->status=0;
                DB::table('my_favourite')->where('id', $myFavourite1->id)->update(['status' => 0]);
                return response()->json(["success" => false,"data" => $myFavourite1    , "message" => "Removed from favourites"]);
            }else if($input['status']==6){
                
                    $myFavourite1->status=6;
                    DB::table('my_favourite')->where('id', $myFavourite1->id)->update(['status' => 6]);
                    return response()->json(["success" => false,"data" => $myFavourite1    , "message" => "Add  as My Favourite"]);
                
            }else{
                return response()->json(["success" => false,"data" => $myFavourite1    , "message" => "My Favourite already add successfully"]);
            }
        }


    }
    public function my_favourites_create_couns(Request $request)
    {
         $input = $request->all();
     
        /*echo "<pre>";
        print_r($input['doctor_id']); exit;*/
        $myFavourite1 = DB::table('my_favourite')->where('pharmacy_id',$input['pharmacy_id'])->where('patient_id',$input['patient_id'])->first();
        /*echo "<pre>";
        print_r($myFavourite1->patient_id); exit;*/
        if(isset($myFavourite1->pharmacy_id) != $input['pharmacy_id'] && isset($myFavourite1->patient_id) != $input['patient_id'])
        {   $input['status']=7;
            $myFavourite = $this->myFavouriteRepository->create($input);
            $myFavourite->pharmacy_id= $input['pharmacy_id'];
            $myFavourite->save();
            return $this->sendResponse($myFavourite->toArray(), 'My Favourite saved successfully');
        }else{

             if($input['status']==0){
                $myFavourite1->status=0;
                DB::table('my_favourite')->where('id', $myFavourite1->id)->update(['status' => 0]);
                return response()->json(["success" => false,"data" => $myFavourite1    , "message" => "Removed from favourites"]);
            }else if($input['status']==7){
                
                    $myFavourite1->status=7;
                    DB::table('my_favourite')->where('id', $myFavourite1->id)->update(['status' => 7]);
                    return response()->json(["success" => false,"data" => $myFavourite1    , "message" => "Add  as My Favourite"]);
                
            }else{
                return response()->json(["success" => false,"data" => $myFavourite1    , "message" => "My Favourite already add successfully"]);
            }
        }


    }
}
