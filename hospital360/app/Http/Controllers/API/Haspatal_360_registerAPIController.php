<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateHaspatal_360_registerAPIRequest;
use App\Http\Requests\API\UpdateHaspatal_360_registerAPIRequest;
use App\Models\Haspatal_360_register;
use App\Repositories\Haspatal_360_registerRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;
use DB;
use Auth;
use App\User;
use Validator;
/**
 * Class Haspatal_360_registerController
 * @package App\Http\Controllers\API
 */

class Haspatal_360_registerAPIController extends AppBaseController
{
    /** @var  Haspatal_360_registerRepository */
    private $haspatal360RegisterRepository;

    public function __construct(Haspatal_360_registerRepository $haspatal360RegisterRepo)
    {
        $this->haspatal360RegisterRepository = $haspatal360RegisterRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/haspatal360Registers",
     *      summary="Get a listing of the Haspatal_360_registers.",
     *      tags={"Haspatal_360_register"},
     *      description="Get all Haspatal_360_registers",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Haspatal_360_register")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $haspatal360Registers = $this->haspatal360RegisterRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($haspatal360Registers->toArray(), 'Haspatal 360 Registers retrieved successfully');
    }

    /**
     * @param CreateHaspatal_360_registerAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/haspatal360Registers",
     *      summary="Store a newly created Haspatal_360_register in storage",
     *      tags={"Haspatal_360_register"},
     *      description="Store Haspatal_360_register",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Haspatal_360_register that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Haspatal_360_register")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Haspatal_360_register"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */

    public function haspatal_360_otp(Request $request)
    {
        $validator = Validator::make($request->all(), [
              'mobile_no' => 'required|unique:haspatal_360_register,mobile_no',
        ]);
        if ($validator->fails()) {
          return response()->json(['error'=>$validator->errors()]);
        }
        
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://sms.bulksmsind.in/sendSMS?username=haspatal&message=".urlencode('Your OTP ').$request['otp']."&sendername=HSPTAL&smstype=TRANS&numbers=".$request['mobile_no']."&apikey=206f20fc-d4d5-4a33-ad06-10f453b8ab4a",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => true,
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
            //echo "cURL Error #:" . $err;
            return response()->json(["status" => true,"message" => "OTP Not Send","data"=>$err]);
        } else {
            //echo $response;exit();
            return response()->json(["status" => true,"message" => "Send OTP in your mobile no"]);
        }
    }

    public function store(Request $request)
    {
        $data = $request->input();
        $users = User::where('email', '=', $data['email'])->orWhere('mobile', '=',  $data['mo_prefix'].'-'.$data['mobile_no'])->first();
         if($users){
            return response()->json(["status" => 1,"massage"=>"User  Already Registered",]);  
         }
       

        $input = $request->all();
        $h360_data =  DB::table('haspatal_360_register')->orderBy('id', 'desc')->first();

        if($h360_data->v_uni_id != ''){
          $bookdata = explode('-', $h360_data->v_uni_id); 
          $dd = $bookdata['2'] + 1;   
          $a = str_pad($dd, 6, '0', STR_PAD_LEFT);
          $input['v_uni_id'] = $bookdata['0'].'-'.$bookdata['1'].'-'.$a;
        }else{
            $seq = '1';
            $a = str_pad($seq, 6, '0', STR_PAD_LEFT); 
            $input['v_uni_id'] = '91-V-'.$a;
        }
        $haspatal360Register = $this->haspatal360RegisterRepository->create($input);
        $user = User::create(['first_name' => $request['full_name'],
                                'email' => $request['email'],
                                'mobile' => $request['mo_prefix'].'-'.$request['mobile_no'],
                                // 'mobile2' => $request['mo_prefix'].'-'.$request['mobile2'],
                                // 'mobile' => $request['mobile_no'],
                                'role_id' => $request['role_id'],
                                'user_type' => 4,
                                'userDetailsId'=>$haspatal360Register->id,
                                //'uid'=> $result->data->uid,
                                'password' => bcrypt($request['password']),
                              ]);
        $user->attachRole($request['role_id']);
        $user['mobile2']=$request['mo_prefix'].'-'.$request['mobile2'];


        if ($haspatal360Register) {
            return response()->json(["status" => true,"massage"=>"Haspatal 360 Register saved successfully","data" => $haspatal360Register,"user" => $user]);
        }else{
            return response()->json(["status" => false,"data" => $haspatal360Register]);
        }

        //return $this->sendResponse($haspatal360Register->toArray(), 'Haspatal 360 Register saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/haspatal360Registers/{id}",
     *      summary="Display the specified Haspatal_360_register",
     *      tags={"Haspatal_360_register"},
     *      description="Get Haspatal_360_register",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Haspatal_360_register",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Haspatal_360_register"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Haspatal_360_register $haspatal360Register */
        $haspatal360Register = $this->haspatal360RegisterRepository->find($id);

        if (empty($haspatal360Register)) {
            return $this->sendError('Haspatal 360 Register not found');
        }

        return $this->sendResponse($haspatal360Register->toArray(), 'Haspatal 360 Register retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateHaspatal_360_registerAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/haspatal360Registers/{id}",
     *      summary="Update the specified Haspatal_360_register in storage",
     *      tags={"Haspatal_360_register"},
     *      description="Update Haspatal_360_register",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Haspatal_360_register",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Haspatal_360_register that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Haspatal_360_register")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Haspatal_360_register"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateHaspatal_360_registerAPIRequest $request)
    {
        $input = $request->all();

        /** @var Haspatal_360_register $haspatal360Register */
        $haspatal360Register = $this->haspatal360RegisterRepository->find($id);

        if (empty($haspatal360Register)) {
            return $this->sendError('Haspatal 360 Register not found');
        }

        $haspatal360Register = $this->haspatal360RegisterRepository->update($input, $id);

        return $this->sendResponse($haspatal360Register->toArray(), 'Haspatal_360_register updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/haspatal360Registers/{id}",
     *      summary="Remove the specified Haspatal_360_register from storage",
     *      tags={"Haspatal_360_register"},
     *      description="Delete Haspatal_360_register",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Haspatal_360_register",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Haspatal_360_register $haspatal360Register */
        $haspatal360Register = $this->haspatal360RegisterRepository->find($id);

        if (empty($haspatal360Register)) {
            return $this->sendError('Haspatal 360 Register not found');
        }

        $haspatal360Register->delete();

        return $this->sendSuccess('Haspatal 360 Register deleted successfully');
    }
}
