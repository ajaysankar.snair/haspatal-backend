<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateSpecialitiesAPIRequest;
use App\Http\Requests\API\UpdateSpecialitiesAPIRequest;
use App\Models\Specialities;
use App\Repositories\SpecialitiesRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class SpecialitiesController
 * @package App\Http\Controllers\API
 */

class SpecialitiesAPIController extends AppBaseController
{
    /** @var  SpecialitiesRepository */
    private $specialitiesRepository;

    public function __construct(SpecialitiesRepository $specialitiesRepo)
    {
        $this->specialitiesRepository = $specialitiesRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/specialities",
     *      summary="Get a listing of the Specialities.",
     *      tags={"Specialities"},
     *      description="Get all Specialities",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Specialities")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $specialities = $this->specialitiesRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );
        $specialities = Specialities::orderBy('specility','asc')->get();
        return $this->sendResponse($specialities, 'Specialities retrieved successfully');
    }

    /**
     * @param CreateSpecialitiesAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/specialities",
     *      summary="Store a newly created Specialities in storage",
     *      tags={"Specialities"},
     *      description="Store Specialities",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Specialities that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Specialities")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Specialities"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateSpecialitiesAPIRequest $request)
    {
        $input = $request->all();

        $specialities = $this->specialitiesRepository->create($input);

        return $this->sendResponse($specialities->toArray(), 'Specialities saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/specialities/{id}",
     *      summary="Display the specified Specialities",
     *      tags={"Specialities"},
     *      description="Get Specialities",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Specialities",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Specialities"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Specialities $specialities */
        $specialities = $this->specialitiesRepository->find($id);

        if (empty($specialities)) {
            return $this->sendError('Specialities not found');
        }

        return $this->sendResponse($specialities->toArray(), 'Specialities retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateSpecialitiesAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/specialities/{id}",
     *      summary="Update the specified Specialities in storage",
     *      tags={"Specialities"},
     *      description="Update Specialities",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Specialities",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Specialities that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Specialities")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Specialities"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateSpecialitiesAPIRequest $request)
    {
        $input = $request->all();

        /** @var Specialities $specialities */
        $specialities = $this->specialitiesRepository->find($id);

        if (empty($specialities)) {
            return $this->sendError('Specialities not found');
        }

        $specialities = $this->specialitiesRepository->update($input, $id);

        return $this->sendResponse($specialities->toArray(), 'Specialities updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/specialities/{id}",
     *      summary="Remove the specified Specialities from storage",
     *      tags={"Specialities"},
     *      description="Delete Specialities",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Specialities",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Specialities $specialities */
        $specialities = $this->specialitiesRepository->find($id);

        if (empty($specialities)) {
            return $this->sendError('Specialities not found');
        }

        $specialities->delete();

        return $this->sendSuccess('Specialities deleted successfully');
    }
}
