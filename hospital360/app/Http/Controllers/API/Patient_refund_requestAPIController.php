<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePatient_refund_requestAPIRequest;
use App\Http\Requests\API\UpdatePatient_refund_requestAPIRequest;
use App\Models\Patient_refund_request;
use App\Repositories\Patient_refund_requestRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;
use Auth;
/**
 * Class Patient_refund_requestController
 * @package App\Http\Controllers\API
 */

class Patient_refund_requestAPIController extends AppBaseController
{
    /** @var  Patient_refund_requestRepository */
    private $patientRefundRequestRepository;

    public function __construct(Patient_refund_requestRepository $patientRefundRequestRepo)
    {
        $this->patientRefundRequestRepository = $patientRefundRequestRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/patientRefundRequests",
     *      summary="Get a listing of the Patient_refund_requests.",
     *      tags={"Patient_refund_request"},
     *      description="Get all Patient_refund_requests",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Patient_refund_request")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $patientRefundRequests = $this->patientRefundRequestRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($patientRefundRequests->toArray(), 'Patient Refund Requests retrieved successfully');
    }

    /**
     * @param CreatePatient_refund_requestAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/patientRefundRequests",
     *      summary="Store a newly created Patient_refund_request in storage",
     *      tags={"Patient_refund_request"},
     *      description="Store Patient_refund_request",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Patient_refund_request that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Patient_refund_request")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Patient_refund_request"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(Request $request)
    {

        $input = $request->all();
         if($request->hasfile('cheque_img'))
        {
            $image = $request->file('cheque_img');
            $extension = $image->getClientOriginalExtension(); // getting image extension
            $cheque_img =time().'__'.$image->getClientOriginalName();
            $path = public_path('/media/cheque_img/');
            $image->move($path, $cheque_img);
            $input['cheque_img']= $cheque_img;

        }
         $input['created_by'] = Auth::user()->id;
        $input['updated_by'] = Auth::user()->id;

        $patientRefundRequest = $this->patientRefundRequestRepository->create($input);

        return $this->sendResponse($patientRefundRequest->toArray(), 'Patient Refund Request saved successfully');
    }
    public function patient_refund_details_data(Request $request)
    {
        
         $patient_add_refund = Patient_refund_request::where('patient_id',$request->patient_id)
                        ->join('patient_details', 'patient_refund_request.patient_id', '=', 'patient_details.id')
                        ->select('patient_refund_request.*','patient_details.first_name as patient')
                        ->first();

        
      if ($patient_add_refund) {
            return response()->json(["status" => true,"data" => $patient_add_refund, "message" => "Patient Add Funds retrieved successfully"]);
        }else{
            return response()->json(["status" => false,"data" => '', "message" => "Patient Add Funds Not retrieved successfully"]);
        }
        /*return $this->sendResponse($myFavourite->toArray(), 'My Favourites retrieved successfully');*/
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/patientRefundRequests/{id}",
     *      summary="Display the specified Patient_refund_request",
     *      tags={"Patient_refund_request"},
     *      description="Get Patient_refund_request",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Patient_refund_request",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Patient_refund_request"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Patient_refund_request $patientRefundRequest */
        $patientRefundRequest = $this->patientRefundRequestRepository->find($id);

        if (empty($patientRefundRequest)) {
            return $this->sendError('Patient Refund Request not found');
        }

        return $this->sendResponse($patientRefundRequest->toArray(), 'Patient Refund Request retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdatePatient_refund_requestAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/patientRefundRequests/{id}",
     *      summary="Update the specified Patient_refund_request in storage",
     *      tags={"Patient_refund_request"},
     *      description="Update Patient_refund_request",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Patient_refund_request",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Patient_refund_request that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Patient_refund_request")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Patient_refund_request"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdatePatient_refund_requestAPIRequest $request)
    {
        $input = $request->all();

        /** @var Patient_refund_request $patientRefundRequest */
        $patientRefundRequest = $this->patientRefundRequestRepository->find($id);

        if (empty($patientRefundRequest)) {
            return $this->sendError('Patient Refund Request not found');
        }

        $patientRefundRequest = $this->patientRefundRequestRepository->update($input, $id);

        return $this->sendResponse($patientRefundRequest->toArray(), 'Patient_refund_request updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/patientRefundRequests/{id}",
     *      summary="Remove the specified Patient_refund_request from storage",
     *      tags={"Patient_refund_request"},
     *      description="Delete Patient_refund_request",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Patient_refund_request",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Patient_refund_request $patientRefundRequest */
        $patientRefundRequest = $this->patientRefundRequestRepository->find($id);

        if (empty($patientRefundRequest)) {
            return $this->sendError('Patient Refund Request not found');
        }

        $patientRefundRequest->delete();

        return $this->sendSuccess('Patient Refund Request deleted successfully');
    }
}
