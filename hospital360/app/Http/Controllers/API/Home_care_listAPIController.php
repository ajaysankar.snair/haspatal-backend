<?php



namespace App\Http\Controllers\API;



use App\Http\Requests\API\CreateHome_care_listAPIRequest;

use App\Http\Requests\API\UpdateHome_care_listAPIRequest;

use App\Models\Home_care_list;

use App\Repositories\Home_care_listRepository;

use Illuminate\Http\Request;

use App\Http\Controllers\AppBaseController;

use Response;
use Auth;


/**

 * Class Home_care_listController

 * @package App\Http\Controllers\API

 */



class Home_care_listAPIController extends AppBaseController

{

    /** @var  Home_care_listRepository */

    private $homeCareListRepository;



    public function __construct(Home_care_listRepository $homeCareListRepo)

    {

        $this->homeCareListRepository = $homeCareListRepo;

    }



    /**

     * Display a listing of the Home_care_list.

     * GET|HEAD /homeCareLists

     *

     * @param Request $request

     * @return \Illuminate\Http\JsonResponse|Response

     */

    public function index(Request $request)

    {

        $homeCareLists = $this->homeCareListRepository->all(

            $request->except(['skip', 'limit']),

            $request->get('skip'),

            $request->get('limit')

        );



        return $this->sendResponse($homeCareLists->toArray(), 'Home Care Lists retrieved successfully');

    }



    /**

     * Store a newly created Home_care_list in storage.

     * POST /homeCareLists

     *

     * @param CreateHome_care_listAPIRequest $request

     *

     * @return \Illuminate\Http\JsonResponse|Response

     */

    public function store(CreateHome_care_listAPIRequest $request)

    {

        $input = $request->all();
        $input['created_by']= Auth::user()->id;
        $input['updated_by']= Auth::user()->id;


        $homeCareList = $this->homeCareListRepository->create($input);
        $homeCareList->dr_id =     $request->dr_id;
       
        $homeCareList->save();


        return $this->sendResponse($homeCareList->toArray(), 'Home Care List saved successfully');

    }



    /**

     * Display the specified Home_care_list.

     * GET|HEAD /homeCareLists/{id}

     *

     * @param int $id

     *

     * @return \Illuminate\Http\JsonResponse|Response

     */

    public function show($id)

    {

        /** @var Home_care_list $homeCareList */

        // $homeCareList = $this->homeCareListRepository->find($id);

        $homeCareList = Home_care_list::select('*')->where('dr_id', $id)->get();

        if (empty($homeCareList)) {

            return $this->sendError('Home Care List not found');

        }



        return $this->sendResponse($homeCareList->toArray(), 'Home Care List retrieved successfully');

    }



    /**

     * Update the specified Home_care_list in storage.

     * PUT/PATCH /homeCareLists/{id}

     *

     * @param int $id

     * @param UpdateHome_care_listAPIRequest $request

     *

     * @return \Illuminate\Http\JsonResponse|Response

     */

    public function update($id, UpdateHome_care_listAPIRequest $request)

    {

        $input = $request->all();



        /** @var Home_care_list $homeCareList */

        $homeCareList = $this->homeCareListRepository->find($id);



        if (empty($homeCareList)) {

            return $this->sendError('Home Care List not found');

        }



        $homeCareList = $this->homeCareListRepository->update($input, $id);



        return $this->sendResponse($homeCareList->toArray(), 'Home_care_list updated successfully');

    }



    /**

     * Remove the specified Home_care_list from storage.

     * DELETE /homeCareLists/{id}

     *

     * @param int $id

     *

     * @throws \Exception

     *

     * @return \Illuminate\Http\JsonResponse|Response

     */

    public function destroy($id)

    {

        /** @var Home_care_list $homeCareList */

        $homeCareList = $this->homeCareListRepository->find($id);



        if (empty($homeCareList)) {

            return $this->sendError('Home Care List not found');

        }



        $homeCareList->delete();



        return $this->sendSuccess('Home Care List deleted successfully');

    }

}

