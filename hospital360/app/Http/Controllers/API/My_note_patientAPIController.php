<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateMy_note_patientAPIRequest;
use App\Http\Requests\API\UpdateMy_note_patientAPIRequest;
use App\Models\My_note_patient;
use App\Repositories\My_note_patientRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;
use Auth;
use DB;
use PDF;

/**
 * Class My_note_patientController
 * @package App\Http\Controllers\API
 */

class My_note_patientAPIController extends AppBaseController
{
    /** @var  My_note_patientRepository */
    private $myNotePatientRepository;

    public function __construct(My_note_patientRepository $myNotePatientRepo)
    {
        $this->myNotePatientRepository = $myNotePatientRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/myNotePatients",
     *      summary="Get a listing of the My_note_patients.",
     *      tags={"My_note_patient"},
     *      description="Get all My_note_patients",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/My_note_patient")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $myNotePatients = $this->myNotePatientRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($myNotePatients->toArray(), 'My Note Patients retrieved successfully');
    }
     public function book_id_wise(Request $request)
    {
        $myNotePatients = My_note_patient::where('m_book_id',$request->m_book_id)
                         ->join('patient_details', 'my_note_patient.my_patient_id', '=', 'patient_details.id')
                         ->join('doctor_details', 'my_note_patient.my_doctor_id', '=', 'doctor_details.id')
                         ->select('my_note_patient.*','patient_details.first_name as patient','doctor_details.first_name','doctor_details.last_name')
                        ->get();
        
        return $this->sendResponse($myNotePatients->toArray(), 'My Note Patients retrieved successfully');
    }

    /**
     * @param CreateMy_note_patientAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/myNotePatients",
     *      summary="Store a newly created My_note_patient in storage",
     *      tags={"My_note_patient"},
     *      description="Store My_note_patient",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="My_note_patient that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/My_note_patient")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/My_note_patient"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(request $request)
    {
        $book =  DB::table('my_note_patient')->where('m_book_id', $request->m_book_id)->orderBy('m_book_id', 'desc')->first();
        if(!empty($book)){
        $input = $request->all();
        $patient = DB::table('patient_details')->where('id',$input['my_patient_id'])->select('first_name')->first();
        $doctor = DB::table('doctor_details')->where('id',$input['my_doctor_id'])->select('first_name')->first();
      
        $data = [
          'title' => 'Summary',
          'heading' => env('APP_URL').'/public/image/haspatallogo.png',
          'my_patient_id' => $patient->first_name,       
          'my_doctor_id' => $doctor->first_name,       
          'm_book_id' => $input['m_book_id'],       
          'my_note' => $input['my_note'],       
            ];
        
        $pdf = PDF::loadView('summary_pdf_view', $data); 
        //return view('pdf_view', compact('data'));
              
        $path = public_path('summary/');
        $fileName1 =   time() . '__'. 'summary.' . 'pdf' ;
        $pdf->save($path . '/' . $fileName1); 

        $input['s_pdf'] = 'public/summary/'.$fileName1;
        $id = $book->id;
        $myNotePatient = $this->myNotePatientRepository->update($input, $id);

        }else
        {
        $input = $request->all();
         $patient = DB::table('patient_details')->where('id',$input['my_patient_id'])->select('first_name')->first();
        $doctor = DB::table('doctor_details')->where('id',$input['my_doctor_id'])->select('first_name')->first();
      
        $data = [
          'title' => 'Summary',
          'heading' => env('APP_URL').'/public/image/haspatallogo.png',
          'my_patient_id' => $patient->first_name,       
          'my_doctor_id' => $doctor->first_name,       
          'm_book_id' => $input['m_book_id'],       
          'my_note' => $input['my_note'],       
            ];
        
        $pdf = PDF::loadView('summary_pdf_view', $data); 
        //return view('pdf_view', compact('data'));
        
        $path = public_path('summary/');
        $fileName1 =   time() . '__'. 'summary.' . 'pdf' ;
        $pdf->save($path . '/' . $fileName1); 

        $input['s_pdf'] = 'public/summary/'.$fileName1;
        $input['created_by'] = Auth::user()->id;
        $input['updated_by'] = Auth::user()->id;

        $myNotePatient = $this->myNotePatientRepository->create($input);
    }

        return $this->sendResponse($myNotePatient->toArray(), 'My Note Patient saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/myNotePatients/{id}",
     *      summary="Display the specified My_note_patient",
     *      tags={"My_note_patient"},
     *      description="Get My_note_patient",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of My_note_patient",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/My_note_patient"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var My_note_patient $myNotePatient */
        $myNotePatient = $this->myNotePatientRepository->find($id);

        if (empty($myNotePatient)) {
            return $this->sendError('My Note Patient not found');
        }

        return $this->sendResponse($myNotePatient->toArray(), 'My Note Patient retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateMy_note_patientAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/myNotePatients/{id}",
     *      summary="Update the specified My_note_patient in storage",
     *      tags={"My_note_patient"},
     *      description="Update My_note_patient",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of My_note_patient",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="My_note_patient that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/My_note_patient")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/My_note_patient"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateMy_note_patientAPIRequest $request)
    {
        $input = $request->all();

        /** @var My_note_patient $myNotePatient */
        $myNotePatient = $this->myNotePatientRepository->find($id);

        if (empty($myNotePatient)) {
            return $this->sendError('My Note Patient not found');
        }

        $myNotePatient = $this->myNotePatientRepository->update($input, $id);

        return $this->sendResponse($myNotePatient->toArray(), 'My_note_patient updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/myNotePatients/{id}",
     *      summary="Remove the specified My_note_patient from storage",
     *      tags={"My_note_patient"},
     *      description="Delete My_note_patient",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of My_note_patient",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var My_note_patient $myNotePatient */
        $myNotePatient = $this->myNotePatientRepository->find($id);

        if (empty($myNotePatient)) {
            return $this->sendError('My Note Patient not found');
        }

        $myNotePatient->delete();

        return $this->sendSuccess('My Note Patient deleted successfully');
    }
}
