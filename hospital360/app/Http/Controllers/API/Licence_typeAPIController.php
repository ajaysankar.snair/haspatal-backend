<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateLicence_typeAPIRequest;
use App\Http\Requests\API\UpdateLicence_typeAPIRequest;
use App\Models\Licence_type;
use App\Repositories\Licence_typeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class Licence_typeController
 * @package App\Http\Controllers\API
 */

class Licence_typeAPIController extends AppBaseController
{
    /** @var  Licence_typeRepository */
    private $licenceTypeRepository;

    public function __construct(Licence_typeRepository $licenceTypeRepo)
    {
        $this->licenceTypeRepository = $licenceTypeRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/licenceTypes",
     *      summary="Get a listing of the Licence_types.",
     *      tags={"Licence_type"},
     *      description="Get all Licence_types",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Licence_type")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $licenceTypes = $this->licenceTypeRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($licenceTypes->toArray(), 'Licence Types retrieved successfully');
    }

    /**
     * @param CreateLicence_typeAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/licenceTypes",
     *      summary="Store a newly created Licence_type in storage",
     *      tags={"Licence_type"},
     *      description="Store Licence_type",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Licence_type that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Licence_type")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Licence_type"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateLicence_typeAPIRequest $request)
    {
        $input = $request->all();

        $licenceType = $this->licenceTypeRepository->create($input);

        return $this->sendResponse($licenceType->toArray(), 'Licence Type saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/licenceTypes/{id}",
     *      summary="Display the specified Licence_type",
     *      tags={"Licence_type"},
     *      description="Get Licence_type",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Licence_type",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Licence_type"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Licence_type $licenceType */
        $licenceType = $this->licenceTypeRepository->find($id);

        if (empty($licenceType)) {
            return $this->sendError('Licence Type not found');
        }

        return $this->sendResponse($licenceType->toArray(), 'Licence Type retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateLicence_typeAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/licenceTypes/{id}",
     *      summary="Update the specified Licence_type in storage",
     *      tags={"Licence_type"},
     *      description="Update Licence_type",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Licence_type",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Licence_type that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Licence_type")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Licence_type"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateLicence_typeAPIRequest $request)
    {
        $input = $request->all();

        /** @var Licence_type $licenceType */
        $licenceType = $this->licenceTypeRepository->find($id);

        if (empty($licenceType)) {
            return $this->sendError('Licence Type not found');
        }

        $licenceType = $this->licenceTypeRepository->update($input, $id);

        return $this->sendResponse($licenceType->toArray(), 'Licence_type updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/licenceTypes/{id}",
     *      summary="Remove the specified Licence_type from storage",
     *      tags={"Licence_type"},
     *      description="Delete Licence_type",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Licence_type",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Licence_type $licenceType */
        $licenceType = $this->licenceTypeRepository->find($id);

        if (empty($licenceType)) {
            return $this->sendError('Licence Type not found');
        }

        $licenceType->delete();

        return $this->sendSuccess('Licence Type deleted successfully');
    }
}
