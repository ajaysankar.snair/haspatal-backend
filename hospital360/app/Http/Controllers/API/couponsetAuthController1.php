<?php
namespace App\Http\Controllers\API;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\User;
use Validator;
use App\coupon;
use DB;
use App\Models\student;
use Illuminate\Support\Facades\Password;

class AuthController1 extends Controller
{

  /**
   * Login API
   *
   * @return \Illuminate\Http\Response
   */
  /*public function login(Request $request){

      $countall = DB::table('users')->where('email',$request->email)->count();
 $userdata = DB::table('users')->where('email',$request->email)->first();

            $bdata = DB::table('business_register')->where('user_id',$userdata->id)->first();
      //$bdata->admin_status;

       if($countall>0)
{
      if(Auth::attempt(['email' => $request->email,'password'=> $request->password]))
      {
    if(Auth::attempt(['email' => $request->email, 'password' => $request->password]) and $bdata->admin_status==1)
    {
      $result = DB::table('users')->where('email',$request->email)->update(['device' => $request->device]);
      $user = Auth::user();
      

      //echo "<pre>";print_r($user);exit();
      $userData = DB::table('users')->where('id',$user->id)->first();
      $userInformation = DB::table('patient_details')
                          ->leftJoin('country','country.id','patient_details.country')
                          ->leftJoin('state','state.id','patient_details.state')
                          ->leftJoin('city','city.id','patient_details.city')
                          ->where('patient_details.id',$user->userDetailsId)
                          ->select('patient_details.*','country.country as country_name','state.state  as state_name','city.city as city_name')
                          ->first();
      $success['token'] =  $user->createToken('LaraPassport')->accessToken;
      $success['user_data'] =  $userData;
      $success['user_information'] =  $userInformation;



      return response()->json([
        //'status' => '4',
        'status'=>'100',
        'data' => $success
      ]);
    } else {
      return response()->json([
        'status' => '3',
        'data' => 'User Pending'
      ]);
    }
  }
  else
    {
      return response()->json([
        'status' => '2',
        'data' => 'Invalid ID or password'
      ]);
    }

  }

  else
  {
              return response()->json([
        'status' => '1',
        'data' => 'User not registered'
      ]);
  }

  }*/


          /**
          *
          *@author Santosh Rauniyar
          *   here I've create api to check user auth,pending,all details saved or not (for more contact me)
          **/

 public function login(Request $request){

      $countall = DB::table('users')->where('email',$request->email)->count();
      $userdata = DB::table('users')->where('email',$request->email)->first();

            if(!empty($userdata))
{
            $bdata = DB::table('business_register')->where('user_id',$userdata->id)->first();
      //$bdata->admin_status;

       if($countall>0)
{
      if(Auth::attempt(['email' => $request->email,'password'=> $request->password]))
      {

          if(!empty($bdata->b_name))
          {

                if (!empty($bdata->state_id))
                 {
                  
                    if (!empty($bdata->patient_select_you)) {
                      
                          if ($bdata->admin_status==1) {
                          

                              /**
                              *@param 
                              * success login 
                              *
                              **/

        $result = DB::table('users')->where('email',$request->email)->update(['device' => $request->device]);
      $user = Auth::user();
      
                $isused=coupon::get()->where('user_id',$userdata->id)->where('promo_code','100promo')->where('status',1)->first();
                if (!empty($isused)) {
                  
                  $success['coupon']='true';
                }
                else
                {
                  $success['coupon']='false';
                }

      //echo "<pre>";print_r($user);exit();
      $userData = DB::table('users')->where('id',$user->id)->first();
      $userInformation = DB::table('patient_details')
                          ->leftJoin('country','country.id','patient_details.country')
                          ->leftJoin('state','state.id','patient_details.state')
                          ->leftJoin('city','city.id','patient_details.city')
                          ->where('patient_details.id',$user->userDetailsId)
                          ->select('patient_details.*','country.country as country_name','state.state  as state_name','city.city as city_name')
                          ->first();
      $success['token'] =  $user->createToken('LaraPassport')->accessToken;
      $success['user_data'] =  $userData;
      $success['user_information'] =  $userInformation;



      return response()->json([
        'status' => '4',
        //'status'=>'100',
        'data' =>$success,
        'message'=>'successfully logged in'
      ]);

                          }
                          else
                          {
                              /**
                              *@param 
                              * pending verification 
                              *
                              **/

                                    return response()->json([
        'status' => '3',
        'data' => 'pending verification',

      ]);

                          }

                    }
                    else
                    {
                              /**
                              *@param 
                              * 3rd api not used 
                              *
                              **/


    return response()->json([
        'status' => '13',
        'data' => 'message not used 3rd API',
        'user_id'=>$userdata->id
      ]);

                    }

                }
                else
                {
                  //2nd api not used
                        return response()->json([
        'status' => '12',
        'data' => 'message not used 2nd API',
        'user_id'=>$userdata->id
      ]);

                }

          }
          else
          {
            //status false message not used 1st API

             return response()->json([
        'status' => '11',
        'data' => 'message not used 1st API',
        'user_id'=>$userdata->id
      ]);
          }
  }
  else
    {
      return response()->json([
        'status' => '2',
        'data' => 'Invalid ID or password'
      ]);
    }

  }

  else
  {
              return response()->json([
        'status' => '1',
        'data' => 'User not registered'
      ]);
  }
}
else
{
           return response()->json([
        'status' => '403',
        'data' => 'User not found'
      ]);
}

  }

//closed login api


  
  public function login_dr(Request $request)
  {

    if(Auth::attempt(['email' => $request->email, 'password' => $request->password,'status'=>0])){
      $result = DB::table('users')->where('email',$request->email)->update(['device' => $request->device]);
      $user = Auth::user();
      //echo "<pre>";print_r($user);exit();
      $userData = DB::table('users')->where('id',$user->id)->first();
      $success['token'] =  $user->createToken('LaraPassport')->accessToken;
      $success['user_data'] =  $userData;



      return response()->json([
        'status' => 'success',
        'data' => $success
      ]);
    } else {
      return response()->json([
        'status' => 'error',
        'data' => 'Unauthorized Access'
      ]);
    }
  }

  public function login_h360(Request $request){

    if(Auth::attempt(['email' => $request->email, 'password' => $request->password,'status'=>0])){
      $result = DB::table('users')->where('email',$request->email)->update(['device' => $request->device]);
      $user = Auth::user();
      //echo "<pre>";print_r($user);exit();
      $userData = DB::table('users')->where('id',$user->id)->first();
      $business_details = DB::table('business_register')->where('user_id',$user->id)->first();
      $h360_details = DB::table('haspatal_360_register')->where('id',$user->userDetailsId)->first();
      $success['token'] =  $user->createToken('LaraPassport')->accessToken;
      $success['user_data'] =  $userData;
      $success['business_details'] =  $business_details;
      $success['h360_details'] =  $h360_details;



      return response()->json([
        'status' => 'success',
        'data' => $success
      ]);
    } else {
      return response()->json([
        'status' => 'error',
        'data' => 'Unauthorized Access'
      ]);
    }
  }

  /**
   * Register API
   *
   * @return \Illuminate\Http\Response
   */
  public function register(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'username' => 'required|max:10|unique:users,username',
      'first_name' => 'required',
      'last_name' => 'required',
      'email' => 'required|email|unique:users,email',
      'password' => 'required',
      'gender' => 'required',
      'c_password' => 'required|same:password',
    ]);
    if ($validator->fails()) {
      return response()->json(['error'=>$validator->errors()]);
    }
    $postArray = $request->all();
    $postArray['password'] = bcrypt($postArray['password']);
    $postArray['fb_id'] = rand();
    $user = User::create($postArray);
    $success['token'] =  $user->createToken('LaraPassport')->accessToken;
    $success['user'] =  $user;
    return response()->json([
      'status' => 'success',
      'data' => $success,
    ]);
  }

  /**
   * details api
   *
   * @return \Illuminate\Http\Response
   */
  public function getDetails()
  {
    $user = Auth::user();
    return response()->json(['success' => $user]);
  }

  public function editUser(Request $request, $id)
  {
        $student = student::findOrFail($id);

        $student->first_name = $request->first_name;
        $student->middel_name = $request->middel_name;
        $student->last_name = $request->last_name;
        $student->date_of_birth = $request->date_of_birth;
        $student->mobile = $request->mobile;
        $student->address = $request->address;

        $student->update();

        $student->save();

        if($student != '')
        {

          DB::table('users')->where('student_id', $id)->where('is_student',1)->update(['name' => $request->first_name]);

          return response()->json([
            'status' => 'success',
            'message' => "Student updated successfully",
            'data' => $student,
            ]);
        }
        return response()->json([
            'status' => 'false',
            'message' => "Student not updated successfully",
            'data' => $student,
            ]);
    }

  public function updatePassword(Request $request)
  {
    if (!password_verify($request->old_password, Auth::user()->password)) {
      return response()->json(['status'=> false, 'message'=>'Invalid current Password']);
    }

    unset($request->old_password);
  /*  if ($request->new_password == '' || $request->con_password == '') {
      echo "string"; exit;
      unset($request->new_password);
      unset($request->con_password);

    } else*/
     if ($request->new_password != '' && $request->confirm_password != '') {
      //echo "string1111"; exit;
      $data['password'] = bcrypt($request->new_password);
      $user = User::findOrFail(Auth::user()->id);
      $user->update($data);
      $user->save();
    }


    $response_data = array();
    $response_data['status'] = true;
    $response_data['message'] = 'User details was successfully updated.';
    $response_data['data']['user'] = Auth::user();
    return response()->json($response_data);

  }
        //token get 
 public function get_token($member_id)
 {
 // echo "string"; exit;

    //$main_member_list = User::where('id', $member_id)->get();
    $main_member_list =  DB::table('users')->where('id', $member_id)->get();
                       
                    
  
      if ($main_member_list) {
         return response()->json(["status" => true,"data" => $main_member_list]);
     }else{
         return response()->json(["status" => false,"data" => $main_member_list]);
     }

 }

  public function forgot_password_otp(Request $request)
  {

    $input = $request->all();
    $rules = array(
        'email' => "required|email",
    );
    $validator = Validator::make($input, $rules);
    if ($validator->fails()) {
        $arr = array("status" => 400, "message" => $validator->errors()->first(), "data" => array());
    } else {

        $user = User::where('email',$request->email)->first();

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://sms.bulksmsind.in/sendSMS?username=haspatal&message=".urlencode('Your OTP ').$request->otp."&sendername=HSPTAL&smstype=TRANS&numbers=".$user->mobile."&apikey=206f20fc-d4d5-4a33-ad06-10f453b8ab4a",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => true,
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
            //echo "cURL Error #:" . $err;
            $arr = array("status" => false, "message" => "OTP Not Send", "data" => $err);
        } else {
            //echo $response;exit();
            $arr = array("status" => true, "message" => "Send OTP in your mobile no", "data" => []);

            //return response()->json(["status" => true,"message" => "Send OTP in your mobile no"]);
        }
    }
    return \Response::json($arr);
  }

  public function forgot_password(Request $request)
  {

    $input = $request->all();
    $rules = array(
        'email' => "required|email",
    );
    $validator = Validator::make($input, $rules);
    if ($validator->fails()) {
        $arr = array("status" => 400, "message" => $validator->errors()->first(), "data" => array());
    } else {
        try {
            $response = Password::sendResetLink($request->only('email'), function (Message $message) {
                $message->subject($this->getEmailSubject());
            });
            switch ($response) {
                case Password::RESET_LINK_SENT:
                    return \Response::json(array("status" => true, "message" => trans($response), "data" => array()));
                case Password::INVALID_USER:
                    return \Response::json(array("status" => false, "message" => trans($response), "data" => array()));
            }
        } catch (\Swift_TransportException $ex) {
            $arr = array("status" => false, "message" => $ex->getMessage(), "data" => []);
        } catch (Exception $ex) {
            $arr = array("status" => false, "message" => $ex->getMessage(), "data" => []);
        }
    }
    return \Response::json($arr);
  }


}