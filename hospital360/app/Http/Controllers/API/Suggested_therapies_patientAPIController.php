<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateSuggested_therapies_patientAPIRequest;
use App\Http\Requests\API\UpdateSuggested_therapies_patientAPIRequest;
use App\Models\Suggested_therapies_patient;
use App\Repositories\Suggested_therapies_patientRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;
use DB;
use Auth;

/**
 * Class Suggested_therapies_patientController
 * @package App\Http\Controllers\API
 */

class Suggested_therapies_patientAPIController extends AppBaseController
{
    /** @var  Suggested_therapies_patientRepository */
    private $suggestedTherapiesPatientRepository;

    public function __construct(Suggested_therapies_patientRepository $suggestedTherapiesPatientRepo)
    {
        $this->suggestedTherapiesPatientRepository = $suggestedTherapiesPatientRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/suggestedTherapiesPatients",
     *      summary="Get a listing of the Suggested_therapies_patients.",
     *      tags={"Suggested_therapies_patient"},
     *      description="Get all Suggested_therapies_patients",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Suggested_therapies_patient")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $suggestedTherapiesPatients = $this->suggestedTherapiesPatientRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($suggestedTherapiesPatients->toArray(), 'Suggested Therapies Patients retrieved successfully');
    }

    /**
     * @param CreateSuggested_therapies_patientAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/suggestedTherapiesPatients",
     *      summary="Store a newly created Suggested_therapies_patient in storage",
     *      tags={"Suggested_therapies_patient"},
     *      description="Store Suggested_therapies_patient",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Suggested_therapies_patient that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Suggested_therapies_patient")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Suggested_therapies_patient"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateSuggested_therapies_patientAPIRequest $request)
    {
         $book =  DB::table('suggested_therapies_patient')->where('st_book_id', $request->st_book_id)->orderBy('st_book_id', 'desc')->first();
        if(!empty($book)){
            $input = $request->all();
            $id = $book->id;
           $suggestedTherapiesPatient = $this->suggestedTherapiesPatientRepository->update($input, $id);

        }else
        {
            $input = $request->all();
            $input['created_by'] = Auth::user()->id;
            $input['updated_by'] = Auth::user()->id;
             $suggestedTherapiesPatient = $this->suggestedTherapiesPatientRepository->create($input);
        }
        return $this->sendResponse($suggestedTherapiesPatient->toArray(), 'Suggested Therapies Patient saved successfully');
     }


    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/suggestedTherapiesPatients/{id}",
     *      summary="Display the specified Suggested_therapies_patient",
     *      tags={"Suggested_therapies_patient"},
     *      description="Get Suggested_therapies_patient",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Suggested_therapies_patient",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Suggested_therapies_patient"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Suggested_therapies_patient $suggestedTherapiesPatient */
        $suggestedTherapiesPatient = $this->suggestedTherapiesPatientRepository->find($id);

        if (empty($suggestedTherapiesPatient)) {
            return $this->sendError('Suggested Therapies Patient not found');
        }

        return $this->sendResponse($suggestedTherapiesPatient->toArray(), 'Suggested Therapies Patient retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateSuggested_therapies_patientAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/suggestedTherapiesPatients/{id}",
     *      summary="Update the specified Suggested_therapies_patient in storage",
     *      tags={"Suggested_therapies_patient"},
     *      description="Update Suggested_therapies_patient",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Suggested_therapies_patient",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Suggested_therapies_patient that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Suggested_therapies_patient")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Suggested_therapies_patient"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateSuggested_therapies_patientAPIRequest $request)
    {
        $input = $request->all();

        /** @var Suggested_therapies_patient $suggestedTherapiesPatient */
        $suggestedTherapiesPatient = $this->suggestedTherapiesPatientRepository->find($id);

        if (empty($suggestedTherapiesPatient)) {
            return $this->sendError('Suggested Therapies Patient not found');
        }

        $suggestedTherapiesPatient = $this->suggestedTherapiesPatientRepository->update($input, $id);

        return $this->sendResponse($suggestedTherapiesPatient->toArray(), 'Suggested_therapies_patient updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/suggestedTherapiesPatients/{id}",
     *      summary="Remove the specified Suggested_therapies_patient from storage",
     *      tags={"Suggested_therapies_patient"},
     *      description="Delete Suggested_therapies_patient",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Suggested_therapies_patient",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Suggested_therapies_patient $suggestedTherapiesPatient */
        $suggestedTherapiesPatient = $this->suggestedTherapiesPatientRepository->find($id);

        if (empty($suggestedTherapiesPatient)) {
            return $this->sendError('Suggested Therapies Patient not found');
        }

        $suggestedTherapiesPatient->delete();

        return $this->sendSuccess('Suggested Therapies Patient deleted successfully');
    }
}
