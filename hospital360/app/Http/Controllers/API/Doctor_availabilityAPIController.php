<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateDoctor_availabilityAPIRequest;
use App\Http\Requests\API\UpdateDoctor_availabilityAPIRequest;
use App\Models\Doctor_availability;
use App\Models\BookingRequest;
use App\Repositories\Doctor_availabilityRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;
use DB;
use Auth;
use Carbon\Carbon;
/**
 * Class Doctor_availabilityController
 * @package App\Http\Controllers\API
 */

class Doctor_availabilityAPIController extends AppBaseController
{
    /** @var  Doctor_availabilityRepository */
    private $doctorAvailabilityRepository;

    public function __construct(Doctor_availabilityRepository $doctorAvailabilityRepo)
    {
        $this->doctorAvailabilityRepository = $doctorAvailabilityRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/doctorAvailabilities",
     *      summary="Get a listing of the Doctor_availabilities.",
     *      tags={"Doctor_availability"},
     *      description="Get all Doctor_availabilities",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Doctor_availability")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $doctorAvailabilities = $this->doctorAvailabilityRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($doctorAvailabilities->toArray(), 'Doctor Availabilities retrieved successfully');
    }
     public function doctor_availability_list(Request $request)
    {
        $result = Doctor_availability::where('doctor_id', $request->doctor_id)->get();
        
        if ($result) {
            return response()->json(["status" => true,"massage"=>"Doctors Availabilities retrieved successfully","data" => $result]);
        }else{
            return response()->json(["status" => false,"data" => $result]);
        }

    }
     public function doctor_availability_delete(Request $request)
    {
        $result = DB::table('doctor_availability')->where('id',$request->id)->delete();
          
        if ($result) {
            return response()->json(["status" => true,"massage"=>"Doctors Availabilities Deleted successfully","data" => $result]);
        }else{
            return response()->json(["status" => false,"data" => $result]);
        }

    }
    public function doctor_availability_dete(Request $request)
    {
        $startDate =  Carbon::parse($request->date)->startOfDay(); 
        /*echo $startDate;
        exit;*/
        $result = BookingRequest::whereDate('booking_request.date_time',$startDate)->where('doctor_name',$request->doctor_id)->first(); 
        

        if ($result) {
            return response()->json(["status" => true,"massage"=>"There are appointments booked for selected date. Please reschedule the bookings for these dates first","data" => $result]);
        }else{
            return response()->json(["status" => false,"data" => $result]);
        }

    }
    
    
    
    /**
     * @param CreateDoctor_availabilityAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/doctorAvailabilities",
     *      summary="Store a newly created Doctor_availability in storage",
     *      tags={"Doctor_availability"},
     *      description="Store Doctor_availability",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Doctor_availability that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Doctor_availability")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Doctor_availability"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateDoctor_availabilityAPIRequest $request)
    {

        if (is_array($request->date)) {
                
            $input = $request->all();
            if ($request->has('nonavabile_time_slot')) {
                $input['nonavabile_time_slot'] = implode(',',$request->nonavabile_time_slot);
            }
                
            foreach ($request->date as $value) {
                $input['date'] = $value;
                $input['created_by'] = Auth::user()->id;
                $input['updated_by'] = Auth::user()->id;

                $check = Doctor_availability::where('date', $value)->first();
                if ($check) {
                    $doctorAvailability = $this->doctorAvailabilityRepository->update($input, $check->id);
                }else{
                    $doctorAvailability = $this->doctorAvailabilityRepository->create($input);
                }
                
            }
            
            return $this->sendResponse($doctorAvailability->toArray(), 'Doctor Availability saved successfully');
        }

        $input = $request->all();
        if ($request->has('nonavabile_time_slot')) {
            $input['nonavabile_time_slot'] = implode(',',$request->nonavabile_time_slot);
        }
        $input['created_by'] = Auth::user()->id;
        $input['updated_by'] = Auth::user()->id;
        $doctorAvailability = $this->doctorAvailabilityRepository->create($input);
        
        return $this->sendResponse($doctorAvailability->toArray(), 'Doctor Availability saved successfully');

    }

    public function getMondaysInRange($day,$dateFromString, $dateToString)
    {
        $dateFrom = new \DateTime($dateFromString);
        $dateTo = new \DateTime($dateToString);
        $dates = [];

        if ($dateFrom > $dateTo) {
            return $dates;
        }

        if (1 != $dateFrom->format('N')) {
            $dateFrom->modify('next '.$day);
        }

        while ($dateFrom <= $dateTo) {
            $dates[] = $dateFrom->format('Y-m-d');
            $dateFrom->modify('+1 week');
        }

        return $dates;
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/doctorAvailabilities/{id}",
     *      summary="Display the specified Doctor_availability",
     *      tags={"Doctor_availability"},
     *      description="Get Doctor_availability",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Doctor_availability",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Doctor_availability"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Doctor_availability $doctorAvailability */
        $doctorAvailability = $this->doctorAvailabilityRepository->find($id);

        if (empty($doctorAvailability)) {
            return $this->sendError('Doctor Availability not found');
        }

        return $this->sendResponse($doctorAvailability->toArray(), 'Doctor Availability retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateDoctor_availabilityAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/doctorAvailabilities/{id}",
     *      summary="Update the specified Doctor_availability in storage",
     *      tags={"Doctor_availability"},
     *      description="Update Doctor_availability",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Doctor_availability",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Doctor_availability that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Doctor_availability")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Doctor_availability"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateDoctor_availabilityAPIRequest $request)
    {
        $input = $request->all();

        /** @var Doctor_availability $doctorAvailability */
        $doctorAvailability = $this->doctorAvailabilityRepository->find($id);

        if (empty($doctorAvailability)) {
            return $this->sendError('Doctor Availability not found');
        }

        $doctorAvailability = $this->doctorAvailabilityRepository->update($input, $id);

        return $this->sendResponse($doctorAvailability->toArray(), 'Doctor_availability updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/doctorAvailabilities/{id}",
     *      summary="Remove the specified Doctor_availability from storage",
     *      tags={"Doctor_availability"},
     *      description="Delete Doctor_availability",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Doctor_availability",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Doctor_availability $doctorAvailability */
        $doctorAvailability = $this->doctorAvailabilityRepository->find($id);

        if (empty($doctorAvailability)) {
            return $this->sendError('Doctor Availability not found');
        }

        $doctorAvailability->delete();

        return $this->sendSuccess('Doctor Availability deleted successfully');
    }
}
