<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateBusiness_typeAPIRequest;
use App\Http\Requests\API\UpdateBusiness_typeAPIRequest;
use App\Models\Business_type;
use App\Repositories\Business_typeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class Business_typeController
 * @package App\Http\Controllers\API
 */

class Business_typeAPIController extends AppBaseController
{
    /** @var  Business_typeRepository */
    private $businessTypeRepository;

    public function __construct(Business_typeRepository $businessTypeRepo)
    {
        $this->businessTypeRepository = $businessTypeRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/businessTypes",
     *      summary="Get a listing of the Business_types.",
     *      tags={"Business_type"},
     *      description="Get all Business_types",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Business_type")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $businessTypes = $this->businessTypeRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($businessTypes->toArray(), 'Business Types retrieved successfully');
    }

    /**
     * @param CreateBusiness_typeAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/businessTypes",
     *      summary="Store a newly created Business_type in storage",
     *      tags={"Business_type"},
     *      description="Store Business_type",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Business_type that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Business_type")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Business_type"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateBusiness_typeAPIRequest $request)
    {
        $input = $request->all();

        $businessType = $this->businessTypeRepository->create($input);

        return $this->sendResponse($businessType->toArray(), 'Business Type saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/businessTypes/{id}",
     *      summary="Display the specified Business_type",
     *      tags={"Business_type"},
     *      description="Get Business_type",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Business_type",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Business_type"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Business_type $businessType */
        $businessType = $this->businessTypeRepository->find($id);

        if (empty($businessType)) {
            return $this->sendError('Business Type not found');
        }

        return $this->sendResponse($businessType->toArray(), 'Business Type retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateBusiness_typeAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/businessTypes/{id}",
     *      summary="Update the specified Business_type in storage",
     *      tags={"Business_type"},
     *      description="Update Business_type",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Business_type",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Business_type that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Business_type")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Business_type"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateBusiness_typeAPIRequest $request)
    {
        $input = $request->all();

        /** @var Business_type $businessType */
        $businessType = $this->businessTypeRepository->find($id);

        if (empty($businessType)) {
            return $this->sendError('Business Type not found');
        }

        $businessType = $this->businessTypeRepository->update($input, $id);

        return $this->sendResponse($businessType->toArray(), 'Business_type updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/businessTypes/{id}",
     *      summary="Remove the specified Business_type from storage",
     *      tags={"Business_type"},
     *      description="Delete Business_type",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Business_type",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Business_type $businessType */
        $businessType = $this->businessTypeRepository->find($id);

        if (empty($businessType)) {
            return $this->sendError('Business Type not found');
        }

        $businessType->delete();

        return $this->sendSuccess('Business Type deleted successfully');
    }
}
