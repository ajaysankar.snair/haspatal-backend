<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateAvailable_lab_patientAPIRequest;
use App\Http\Requests\API\UpdateAvailable_lab_patientAPIRequest;
use App\Models\Available_lab_patient;
use App\Repositories\Available_lab_patientRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;
use Auth;
use DB;

/**
 * Class Available_lab_patientController
 * @package App\Http\Controllers\API
 */

class Available_lab_patientAPIController extends AppBaseController
{
    /** @var  Available_lab_patientRepository */
    private $availableLabPatientRepository;

    public function __construct(Available_lab_patientRepository $availableLabPatientRepo)
    {
        $this->availableLabPatientRepository = $availableLabPatientRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/availableLabPatients",
     *      summary="Get a listing of the Available_lab_patients.",
     *      tags={"Available_lab_patient"},
     *      description="Get all Available_lab_patients",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Available_lab_patient")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $availableLabPatients = $this->availableLabPatientRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($availableLabPatients->toArray(), 'Available Lab Patients retrieved successfully');
    }

    /**
     * @param CreateAvailable_lab_patientAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/availableLabPatients",
     *      summary="Store a newly created Available_lab_patient in storage",
     *      tags={"Available_lab_patient"},
     *      description="Store Available_lab_patient",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Available_lab_patient that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Available_lab_patient")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Available_lab_patient"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateAvailable_lab_patientAPIRequest $request)
    {
         $book =  DB::table('Available_lab_patient')->where('a_book_id', $request->a_book_id)->orderBy('a_book_id', 'desc')->first();
        if(!empty($book)){
            $input = $request->all();
            $id = $book->id;
            $availableLabPatient = $this->availableLabPatientRepository->update($input, $id);

        }else
        {
            $input = $request->all();
            $input['created_by'] = Auth::user()->id;
            $input['updated_by'] = Auth::user()->id;

            $availableLabPatient = $this->availableLabPatientRepository->create($input);
        }
        return $this->sendResponse($availableLabPatient->toArray(), 'Available Lab Patient saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/availableLabPatients/{id}",
     *      summary="Display the specified Available_lab_patient",
     *      tags={"Available_lab_patient"},
     *      description="Get Available_lab_patient",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Available_lab_patient",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Available_lab_patient"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Available_lab_patient $availableLabPatient */
        $availableLabPatient = $this->availableLabPatientRepository->find($id);

        if (empty($availableLabPatient)) {
            return $this->sendError('Available Lab Patient not found');
        }

        return $this->sendResponse($availableLabPatient->toArray(), 'Available Lab Patient retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateAvailable_lab_patientAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/availableLabPatients/{id}",
     *      summary="Update the specified Available_lab_patient in storage",
     *      tags={"Available_lab_patient"},
     *      description="Update Available_lab_patient",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Available_lab_patient",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Available_lab_patient that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Available_lab_patient")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Available_lab_patient"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateAvailable_lab_patientAPIRequest $request)
    {
        $input = $request->all();

        /** @var Available_lab_patient $availableLabPatient */
        $availableLabPatient = $this->availableLabPatientRepository->find($id);

        if (empty($availableLabPatient)) {
            return $this->sendError('Available Lab Patient not found');
        }

        $availableLabPatient = $this->availableLabPatientRepository->update($input, $id);

        return $this->sendResponse($availableLabPatient->toArray(), 'Available_lab_patient updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/availableLabPatients/{id}",
     *      summary="Remove the specified Available_lab_patient from storage",
     *      tags={"Available_lab_patient"},
     *      description="Delete Available_lab_patient",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Available_lab_patient",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Available_lab_patient $availableLabPatient */
        $availableLabPatient = $this->availableLabPatientRepository->find($id);

        if (empty($availableLabPatient)) {
            return $this->sendError('Available Lab Patient not found');
        }

        $availableLabPatient->delete();

        return $this->sendSuccess('Available Lab Patient deleted successfully');
    }
}
