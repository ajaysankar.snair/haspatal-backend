<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateBookingRequestAPIRequest;
use App\Http\Requests\API\UpdateBookingRequestAPIRequest;
use App\Models\BookingRequest;
use App\Repositories\BookingRequestRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;
use Carbon\Carbon;
use App\Models\Patient_add_fund;
use App\Models\Wallet_360;
use DB;
use Auth;

/**
 * Class BookingRequestController
 * @package App\Http\Controllers\API
 */

class BookingRequestAPIController extends AppBaseController
{
    /** @var  BookingRequestRepository */
    private $bookingRequestRepository;

    public function __construct(BookingRequestRepository $bookingRequestRepo)
    {
        $this->bookingRequestRepository = $bookingRequestRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/bookingRequests",
     *      summary="Get a listing of the BookingRequests.",
     *      tags={"BookingRequest"},
     *      description="Get all BookingRequests",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/BookingRequest")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $bookingRequests = $this->bookingRequestRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($bookingRequests->toArray(), 'Booking Requests retrieved successfully');
    }
     public function bookingRequest_id($id)
    {
        //cho "string"; exit;    
        /** @var BookingRequest $bookingRequest */
       /* $bookingRequest[] = $this->bookingRequestRepository->find($id);*/
         $bookingRequest = BookingRequest::where('booking_request.patient_name',$id) 
                       // ->where('booking_request.status','!=',5) 
                        ->join('patient_details', 'booking_request.patient_name', '=', 'patient_details.id')
                        ->join('doctor_details', 'booking_request.doctor_name', '=', 'doctor_details.id')
                        ->orderBy('booking_request.id','DESC')
                        ->select('booking_request.*','patient_details.first_name as patient','doctor_details.first_name','doctor_details.last_name','doctor_details.profile_pic','doctor_details.clinic_logo')
                        ->get(); 
      
              $path = array("profile_pic"=>env('APP_URL').'/public/media/profile_pic',
                            "clinic_logo"=>env('APP_URL').'/public/media/clinic_logo'
                        );
  
        if ($bookingRequest) {
            return response()->json(["status" => true,"massage"=>"Booking Request retrieved successfully","data" => $bookingRequest,"path" => $path]);
        }else{
             return response()->json(["status" => false,"data" => $bookingRequest]);
        }
    }
    public function doctorconultant($id)
    {
        //cho "string"; exit;    
        /** @var BookingRequest $bookingRequest */
       /* $bookingRequest[] = $this->bookingRequestRepository->find($id);*/
         $bookingRequest = BookingRequest::where('booking_request.patient_name',$id) 
                       // ->where('booking_request.status','!=',5) 
                        ->join('patient_details', 'booking_request.patient_name', '=', 'patient_details.id')
                        ->join('doctor_details', 'booking_request.doctor_name', '=', 'doctor_details.id')
                        ->orderBy('booking_request.id','DESC')
                        ->select('booking_request.*','patient_details.first_name as patient','doctor_details.first_name','doctor_details.last_name','doctor_details.profile_pic','doctor_details.clinic_logo') ->where('booking_request.call_status','=',9)
                        // ->orWhere('booking_request.call_status', '=', 1)
                        ->get(); 
      
              $path = array("profile_pic"=>env('APP_URL').'/public/media/profile_pic',
                            "clinic_logo"=>env('APP_URL').'/public/media/clinic_logo'
                        );
  
        if ($bookingRequest) {
            foreach ($bookingRequest as $data) {
                $book_id = $data->book_id;
                $review = DB::table('review')->where('book_id',$book_id)->first();
                if($review){
                    $data['review_star']= $review->rating;
                    $data['review_que']= $review->review_que;
                }else{
                    $data['review_star']= 0;
                    // $data['review_que']= "write your review here";
                }
             

            }
            return response()->json(["status" => true,"massage"=>"Booking Request retrieved successfully","data" => $bookingRequest,"path" => $path]);
        }else{
             return response()->json(["status" => false,"data" => $bookingRequest]);
        }
    }
    public function payment_statement(Request $request)
    {
         $startDate =  Carbon::parse($request->s_date)->startOfDay(); 
         $endDate =  Carbon::parse($request->e_date)->startOfDay(); 

          $payment_statement = BookingRequest::where('booking_request.patient_name',$request->patient_id) 
                        ->whereBetween('booking_request.created_at', [$startDate, $endDate]) 
                        ->leftJoin('patient_details', 'booking_request.patient_name', '=', 'patient_details.id')
                        ->leftJoin('doctor_details', 'booking_request.doctor_name', '=', 'doctor_details.id')
                        ->join('patient_add_fund', 'booking_request.patient_name', '=', 'patient_add_fund.patient_id')
                        ->orderBy('booking_request.id','DESC')
                        ->select('booking_request.*','patient_details.first_name as patient','doctor_details.first_name','doctor_details.last_name','patient_add_fund.requested_amount','patient_add_fund.payment_id as wallet_payment_id')
                        ->get(); 
          if ($payment_statement) {
            return response()->json(["status" => true,"massage"=>"Payment Statement retrieved successfully","data" => $payment_statement]);
        }else{
             return response()->json(["status" => false,"data" => $payment_statement]);
        }
    }
    public function booking_slot(Request $request)
    {
        /** @var BookingRequest $bookingRequest */
       /* $bookingRequest[] = $this->bookingRequestRepository->find($id);*/
         $s_date = Carbon::parse($request->date_time)->startOfDay();
         $e_date = Carbon::parse($request->date_time)->endOfDay();
      
         $booking_slot = BookingRequest::where('booking_request.doctor_name',$request->doctor_id) 
                         //->where('booking_request.date_time',$s_date) 
                        ->whereBetween('date_time', [$s_date,$e_date])
                        ->join('patient_details', 'booking_request.patient_name', '=', 'patient_details.id')
                        ->join('doctor_details', 'booking_request.doctor_name', '=', 'doctor_details.id')
                        ->select('booking_request.date_time','patient_details.first_name as patient','doctor_details.first_name','doctor_details.last_name')
                        ->get(); 
       if ($booking_slot) {
            return response()->json(["status" => true,"massage"=>"Booking Request retrieved successfully","data" => $booking_slot]);
        }else{
             return response()->json(["status" => false,"data" => $booking_slot]);
        }
    }

    public function booking_list_after_date(Request $request)
    {
        $s_date = Carbon::parse($request->date_time)->format('Y-m-d H:i');
        
        $booking_slot = BookingRequest::where('patient_name',$request->patient_name) 
                        ->where('date_time','>=',$s_date) 
                        ->join('patient_details', 'booking_request.patient_name', '=', 'patient_details.id')
                        ->join('doctor_details', 'booking_request.doctor_name', '=', 'doctor_details.id')
                        ->select('booking_request.*','patient_details.first_name as patient','doctor_details.first_name','doctor_details.last_name')
                        ->get(); 

       if ($booking_slot) {
            return response()->json(["status" => true,"massage"=>"Booking Request retrieved successfully","data" => $booking_slot]);
        }else{
             return response()->json(["status" => false,"data" => $booking_slot]);
        }
    }

    public function booking_waiting(Request $request)
    {
        $result =  DB::table('booking_request')->where('book_id',$request->book_id)->update(['status' => 2,'waiting_time' => $request->waiting_time]);
        
         if ($result) {
            $book_patient =  DB::table('booking_request')->where('book_id',$request->book_id)->first();
            $patient = DB::table('patient_details')->where('id',$book_patient->patient_name)->select('first_name')->first();
            $user_dr = \App\User::where('userDetailsId',$book_patient->doctor_name)->where('user_type',1)->first();

            $user_patient = \App\User::where('userDetailsId',$book_patient->patient_name)->where('user_type',2)->first();

            $this->send_notification($user_dr->mobile_token,$patient->first_name." enter waiting room","Waiting room");

            $this->send_notification($user_patient->mobile_token," Waiting room is open for you","Waiting room");

            $this->send_mail($user_patient,"Waiting room is open for you");


             return response()->json(["status" => true,"data" => $result, "message" => "Booking waiting...."]);
        }else{
            return response()->json(["status" => false,"data" => $result, "message" => "Booking Not waiting...."]);
         }
    }

    
     public function booking_cancel(Request $request)
    {
       
        // $result =  DB::table('booking_request')->where('book_id',)->update(['status' => 5,'patient_status'=>1]);
        $result = BookingRequest::where('book_id', $request->book_id)->update(['status' => 5,'patient_status'=>1]);

        $booking_data = BookingRequest::where('book_id',$request->book_id)->where('booking_request.status','=',5)->get(); 
      
        $total_fund =  DB::table('patient_add_fund')->where('patient_id', $booking_data[0]->patient_name)->first();

        if (!empty($total_fund)) {
        
            $mi_fund = $total_fund->wa_amount + $booking_data[0]->f_amount;

            $input['w_amount'] =  $mi_fund;

            $fund =  DB::table('patient_add_fund')->where('patient_id',$booking_data[0]->patient_name)->update(['wa_amount' => $mi_fund]);
        }
        
        if ($result) {

            //  return response()->json(["status" => true,"data" => $result, "message" => "Your booking has been cancelled..",'booking_data' => $booking_data]);
            $emailResponsethird = $this->send_mail3($request->book_id);
            $emailResponsethirds = $this->send_mail4($request->book_id);
            // return $emailResponsethirds;
             return response()->json(["status" => true,"data" => $result, "message" => "Your booking has been cancelled..",'booking_data' => $booking_data," 'emailResponsethird '=>$emailResponsethird"]);
        }else{
            return response()->json(["status" => false,"data" => $result, "message" => "Booking Not cancel ...."]);
         }
    }
    ///////////////////////////////////////

    public function send_mail3($book_id)
        
    {
            $bookingRequest = BookingRequest::where('booking_request.book_id',$book_id) 
            // ->where('booking_request.status','!=',5) 
            ->join('patient_details', 'booking_request.patient_name', '=', 'patient_details.id')
            ->join('doctor_details', 'booking_request.doctor_name', '=', 'doctor_details.id')
            ->select('booking_request.*','patient_details.first_name as patient','patient_details.email as patient_email','doctor_details.*')->first();
        if($bookingRequest->consult_mode==1){
            $consult_mode="Video Consultation";
        }else{
            $consult_mode="In-Clinic Consultation";
        }
        
            $subject =
            "Booking Cancellation -".$bookingRequest->patient."-".$bookingRequest->book_id;
            // dd( $user);
            $mailmessage= '<!DOCTYPE html>
            <html lang="en">
                <head>
                    <title>Dr email template</title>
                    <meta charset="utf-8">
                    <!-- CSS only -->
                    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
                    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
                    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900,900i&display=swap" rel="stylesheet">
                    <script src="https://kit.fontawesome.com/fa5e54101c.js" crossorigin="anonymous"></script>
                    <!-- JavaScript Bundle with Popper -->
            
                    <!-- Theme Style -->
                    <!-- <link rel="stylesheet" href="/assets/css/bootstrap.min.css"> -->
                    <style>
                        #page-wrap{
                            max-width: 800px; 
                            margin: 0 auto;
                            font-family: "Roboto";
                            /* padding: 25px 0; */
                        }
                        p,h5,h6{
                            color: #000;
                            font-family: "Roboto";
                            margin: 0;
                        }
                        p span{
                            display: inline-block;
                        }
                    </style>
                </head>
                <body>
                    <div id="page-wrap">
                        <div class="top">
                        
                            <h6 style="margin-bottom: 25px; font-weight:400; font-size: 16px;">Dear '.$bookingRequest->first_name.', </h6>
                            <p style="margin-bottom: 25px;">The booking with the following details has been cancelled by the patient</p>
                    
                            <p><span style="min-width: 180px;">Booking Id</span>:<span style="margin-left: 25px;">'.$bookingRequest->book_id.'</span></p>
                            <p><span style="min-width: 180px;">Appointment Type</span>:<span style="margin-left: 25px;">'.$consult_mode.'</span></p>
                            <p><span style="min-width: 180px;">Appointment Date</span>:<span style="margin-left: 25px;">'.$bookingRequest->consult_date.'</span></p>
                            <p><span style="min-width: 180px;">Appointment Time</span>:<span style="margin-left: 25px;">'.$bookingRequest->consult_time.'</span></p>
                            <p style="margin-bottom: 25px; margin-top: 25px;">Kindly note that the cancellation has been updated in our system.</p>
                            <p style="margin-bottom: 25px;">With Regards,</p>
                            <p style="margin-bottom: 25px;">Team Haspatal Care</p>
                        </div>
                    </div>
                </body>
            </html>
            
            
    '                 ;
            $data['personalizations'] = array("0" => array('to' => array('0' => array('email' => $bookingRequest->email))));

            $data['from'] = array('email' => 'system@haspatal.com');

            $data['subject'] = $subject;
            $data['content'] = array("0" =>array('type' =>' text/html','value' => $mailmessage ));

            //echo json_encode($data);exit();

            $curl = curl_init();

            curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.sendgrid.com/v3/mail/send",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($data),
            CURLOPT_HTTPHEADER => array(
                "authorization: Bearer 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",
                "cache-control: no-cache",
                "content-type: application/json",
                "postman-token: b8c0781f-af48-90cc-1c5b-76f80e0b4be0"
            ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            if ($err) {
                echo "cURL Error #:" . $err;
            } else {
                return "Send Alert in your emailId";
            }
        }
        
        


    ///////////////////////////////////

 ///////////////////////////////////////

 public function send_mail4($book_id)
        
 {
         $bookingRequest = BookingRequest::where('booking_request.book_id',$book_id) 
         // ->where('booking_request.status','!=',5) 
         ->join('patient_details', 'booking_request.patient_name', '=', 'patient_details.id')
         ->join('doctor_details', 'booking_request.doctor_name', '=', 'doctor_details.id')
         ->select('booking_request.*','patient_details.first_name as patient','patient_details.email as patient_email','doctor_details.*')->first();
     if($bookingRequest->consult_mode==1){
         $consult_mode="Video Consultation";
     }else{
         $consult_mode="In-Clinic Consultation";
     }

     $payment =  Patient_add_fund::where('patient_id',$bookingRequest->patient_name)->first();
    //  return $payment;
         $subject =
         "Booking Cancellation -".$bookingRequest->first_name."-".$bookingRequest->book_id;
         // dd( $user);
         $mailmessage= '<!DOCTYPE html>
         <html lang="en">
             <head>
                 <title>Dr email template</title>
                 <meta charset="utf-8">
                 <!-- CSS only -->
                 <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
                 <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
                 <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900,900i&display=swap" rel="stylesheet">
                 <script src="https://kit.fontawesome.com/fa5e54101c.js" crossorigin="anonymous"></script>
                 <!-- JavaScript Bundle with Popper -->
         
                 <!-- Theme Style -->
                 <!-- <link rel="stylesheet" href="/assets/css/bootstrap.min.css"> -->
                 <style>
                     #page-wrap{
                         max-width: 800px; 
                         margin: 0 auto;
                         font-family: "Roboto";
                         /* padding: 25px 0; */
                     }
                     p,h5,h6{
                         color: #000;
                         font-family: "Roboto";
                         margin: 0;
                     }
                     p span{
                         display: inline-block;
                     }
                 </style>
             </head>
             <body>
                 <div id="page-wrap">
                     <div class="top">
                     
                         <h6 style="margin-bottom: 25px; font-weight:400; font-size: 16px;">Dear '.$bookingRequest->patient.', </h6>
                         <p style="margin-bottom: 25px;">The booking with the following details has been canceled by you</p>
                 
                         <p><span style="min-width: 180px;">Booking Id</span>:<span style="margin-left: 25px;">'.$bookingRequest->book_id.'</span></p>
                         <p><span style="min-width: 180px;">Appointment Type</span>:<span style="margin-left: 25px;">'.$consult_mode.'</span></p>
                         <p><span style="min-width: 180px;">Appointment Date</span>:<span style="margin-left: 25px;">'.$bookingRequest->consult_date.'</span></p>
                         <p><span style="min-width: 180px;">Appointment Time</span>:<span style="margin-left: 25px;">'.$bookingRequest->consult_time.'</span></p>
                         <p style="margin-bottom: 25px; margin-top: 25px;">Kindly note that the cancellation has been updated in our system and your Wallet has been credited with Rs. '.$bookingRequest->f_amount.', paid by you for booking the Consultation. The Transaction Id is '.$payment->payment_id.'</p>
                         <p style="margin-bottom: 25px;">The updated balance in your wallet is '.$payment->wa_amount.'</p>
                         <p style="margin-bottom: 25px;">Please note:</p>
                         <p style="margin-bottom: 0; padding-left: 20px; display: flex;">
                    <span style="margin-right: 15px;">4.</span>
                    In case of any difficulty, please contact Haspatal Support Team at:<br>+91-124-405 56 56 mentioning your Booking Id

                </p>
                         <p style="margin-bottom: 25px; margin-top: 25px;">With Regards,</p>
                         <p style="margin-bottom: 25px;">Team Haspatal Care</p>
                     </div>
                 </div>
             </body>
         </html>
         
         
 '                 ;
         $data['personalizations'] = array("0" => array('to' => array('0' => array('email' =>  $bookingRequest->patient_email))));

         $data['from'] = array('email' => 'system@haspatal.com');

         $data['subject'] = $subject;
         $data['content'] = array("0" =>array('type' =>' text/html','value' => $mailmessage ));

         //echo json_encode($data);exit();

         $curl = curl_init();

         curl_setopt_array($curl, array(
         CURLOPT_URL => "https://api.sendgrid.com/v3/mail/send",
         CURLOPT_RETURNTRANSFER => true,
         CURLOPT_ENCODING => "",
         CURLOPT_MAXREDIRS => 10,
         CURLOPT_TIMEOUT => 30,
         CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
         CURLOPT_CUSTOMREQUEST => "POST",
         CURLOPT_POSTFIELDS => json_encode($data),
         CURLOPT_HTTPHEADER => array(
             "authorization: Bearer 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",
             "cache-control: no-cache",
             "content-type: application/json",
             "postman-token: b8c0781f-af48-90cc-1c5b-76f80e0b4be0"
         ),
         ));

         $response = curl_exec($curl);
         $err = curl_error($curl);

         curl_close($curl);

         if ($err) {
             echo "cURL Error #:" . $err;
         } else {
             return "Send Alert in your emailId";
         }
     }
     
     


 ///////////////////////////////////


     public function booking_slot_update(Request $request)
    {
         $result =  DB::table('booking_request')->where('book_id',$request->book_id)->update(['date_time' => $request->date_time,'slot_status' => $request->slot_status,'status' => 4]);

        if ($result) {
             return response()->json(["status" => true,"data" => $result, "message" => "Booking slot Updated ...."]);
        }else{
            return response()->json(["status" => false,"data" => $result, "message" => "Booking Not slot Updated ...."]);
         }
    }
    public function bookingschedule(Request $request)
    {

        $bookingRequest = BookingRequest::where('booking_request.book_id',$request->book_id) 
        // ->where('booking_request.status','!=',5) 
        ->join('patient_details', 'booking_request.patient_name', '=', 'patient_details.id')
        ->join('doctor_details', 'booking_request.doctor_name', '=', 'doctor_details.id')
        ->select('booking_request.*','patient_details.first_name as patient','patient_details.email as patient_email','doctor_details.*')->first();
        // return $bookingRequest;
        //  $result =  DB::table('booking_request')->where('book_id',$request->book_id)->update(['date_time' => $request->date_time,'slot_status' => $request->slot_status,'status' => 4]);
        $time = strtotime($request->consult_date);


        $newformat = date('Y-m-d',$time);
        $date_time=$newformat.' '.$request->consult_time;
        // return 
        $result = BookingRequest::where('book_id', $request->book_id)->update(['date_time' => $request->date_time,'consult_date'=>$request->consult_date,'appointment_time'=>$newformat,'consult_time'=>$request->consult_time,'filter_date_time'=>$date_time]);

        if ($result) {
            $Rescheduled =  BookingRequest::where('book_id', $request->book_id)->first();
            $emailResponsethird = $this->send_mail5( $bookingRequest,$Rescheduled);
            $emailResponsethirds = $this->send_mail6( $bookingRequest,$Rescheduled);
             return response()->json(["status" => true,"data" => $result, "message" => "Booking slot Updated ...."]);
        }else{
            return response()->json(["status" => false,"data" => $result, "message" => "Booking Not slot Updated ...."]);
         }
    }

    public function send_mail5($bookingRequest,$Rescheduled)
    {

    if($bookingRequest->consult_mode==1){
        $consult_mode="Video Consultation";
    }else{
        $consult_mode="In-Clinic Consultation";
    }

    $payment =  Patient_add_fund::where('patient_id',$bookingRequest->patient_name)->first();
   //  return $payment;
        $subject =
        " Appointment Rescheduled -".$bookingRequest->patient."-".$bookingRequest->book_id;
        // dd( $user);
        $mailmessage= '<!DOCTYPE html>
        <html lang="en">
            <head>
                <title>Dr email template</title>
                <meta charset="utf-8">
                <!-- CSS only -->
                <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
                <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
                <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900,900i&display=swap" rel="stylesheet">
                <script src="https://kit.fontawesome.com/fa5e54101c.js" crossorigin="anonymous"></script>
                <!-- JavaScript Bundle with Popper -->
        
                <!-- Theme Style -->
                <!-- <link rel="stylesheet" href="/assets/css/bootstrap.min.css"> -->
                <style>
                    #page-wrap{
                        max-width: 800px; 
                        margin: 0 auto;
                        font-family: "Roboto";
                        /* padding: 25px 0; */
                    }
                    p,h5,h6{
                        color: #000;
                        font-family: "Roboto";
                        margin: 0;
                    }
                    p span{
                        display: inline-block;
                    }
                </style>
            </head>
            <body>
                <div id="page-wrap">
                    <div class="top">
                    
                        <h6 style="margin-bottom: 25px; font-weight:400; font-size: 16px;">Dear '.$bookingRequest->first_name.', </h6>
                        <p style="margin-bottom: 25px;">The booking with the following details has been rescheduled by the patient:</p>
                        <p style="margin-bottom: 25px;"><u><b>Original Booking</b></u></p>
               
                        <p><span style="min-width: 180px;">Booking Id</span>:<span style="margin-left: 25px;">'.$bookingRequest->book_id.'</span></p>
                        <p><span style="min-width: 180px;">Appointment Type</span>:<span style="margin-left: 25px;">'.$consult_mode.'</span></p>
                        <p><span style="min-width: 180px;">Appointment Date</span>:<span style="margin-left: 25px;">'.$bookingRequest->consult_date.'</span></p>
                        <p><span style="min-width: 180px;">Appointment Time</span>:<span style="margin-left: 25px;">'.$bookingRequest->consult_time.'</span></p>
                        <p><span style="min-width: 180px;">Patient Name	</span>:<span style="margin-left: 25px;">'.$bookingRequest->patient.'</span></p>
                    </br>
                    <p style="margin-bottom: 25px;"><u><b>Rescheduled Booking</b></u></p>
                        <p><span style="min-width: 180px;">Booking Id</span>:<span style="margin-left: 25px;">'.$Rescheduled->book_id.'</span></p>
                        <p><span style="min-width: 180px;">Appointment Type</span>:<span style="margin-left: 25px;">'.$consult_mode.'</span></p>
                        <p><span style="min-width: 180px;">Appointment Date</span>:<span style="margin-left: 25px;">'.$Rescheduled->consult_date.'</span></p>
                        <p><span style="min-width: 180px;">Appointment Time</span>:<span style="margin-left: 25px;">'.$Rescheduled->consult_time.'</span></p>
                        <p><span style="min-width: 180px;">Patient Name	</span>:<span style="margin-left: 25px;">'.$bookingRequest->patient.'</span></p>
                         <p><span style="min-width: 180px;">Waiting Room Opens	</span>:<span style="margin-left: 25px;">['.$Rescheduled->consult_time.' - 15 min]</span></p>
                <p style="margin-bottom: 25px;"><span style="min-width: 180px;">Waiting Room Closes</span>:<span style="margin-left: 25px;">['.$Rescheduled->consult_time.' + 60 min]</span></p>
                        <p style="margin-bottom: 25px;">Please note:</p>
                        <p style="margin-bottom: 0; padding-left: 20px; display: flex;">
                        <span style="margin-right: 15px;">1.</span>
                        You can cancel / reschedule the appointment upto '.$Rescheduled->consult_time.' on,'.$Rescheduled->consult_date.'
                    </p>
                    <p style="margin-bottom: 0; padding-left: 20px; display: flex;">
                        <span style="margin-right: 15px;">2.</span>
                         In case of any difficulty, please contact Haspatal Support Team at: 
                            +91-124-405 56 56
                    </p>
                        <p style="margin-bottom: 25px; margin-top: 25px;">With Regards,</p>
                        <p style="margin-bottom: 25px;">Team Haspatal MD</p>
                    </div>
                </div>
            </body>
        </html>
        
        
'                 ;
        $data['personalizations'] = array("0" => array('to' => array('0' => array('email' =>  $bookingRequest->email))));

        $data['from'] = array('email' => 'system@haspatal.com');

        $data['subject'] = $subject;
        $data['content'] = array("0" =>array('type' =>' text/html','value' => $mailmessage ));

        //echo json_encode($data);exit();

        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => "https://api.sendgrid.com/v3/mail/send",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => json_encode($data),
        CURLOPT_HTTPHEADER => array(
            "authorization: Bearer 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",
            "cache-control: no-cache",
            "content-type: application/json",
            "postman-token: b8c0781f-af48-90cc-1c5b-76f80e0b4be0"
        ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            return "Send Alert in your emailId";
        }
    }
    public function send_mail6($bookingRequest,$Rescheduled)
    {

    if($bookingRequest->consult_mode==1){
        $consult_mode="Video Consultation";
    }else{
        $consult_mode="In-Clinic Consultation";
    }

    $payment =  Patient_add_fund::where('patient_id',$bookingRequest->patient_name)->first();
   //  return $payment;
        $subject =
        " Appointment Rescheduled -".$bookingRequest->first_name."-".$bookingRequest->book_id;
        // dd( $user);
        $mailmessage= '<!DOCTYPE html>
        <html lang="en">
            <head>
                <title>Dr email template</title>
                <meta charset="utf-8">
                <!-- CSS only -->
                <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
                <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
                <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900,900i&display=swap" rel="stylesheet">
                <script src="https://kit.fontawesome.com/fa5e54101c.js" crossorigin="anonymous"></script>
                <!-- JavaScript Bundle with Popper -->
        
                <!-- Theme Style -->
                <!-- <link rel="stylesheet" href="/assets/css/bootstrap.min.css"> -->
                <style>
                    #page-wrap{
                        max-width: 800px; 
                        margin: 0 auto;
                        font-family: "Roboto";
                        /* padding: 25px 0; */
                    }
                    p,h5,h6{
                        color: #000;
                        font-family: "Roboto";
                        margin: 0;
                    }
                    p span{
                        display: inline-block;
                    }
                </style>
            </head>
            <body>
                <div id="page-wrap">
                    <div class="top">
                    
                        <h6 style="margin-bottom: 25px; font-weight:400; font-size: 16px;">Dear '.$bookingRequest->patient.', </h6>
                        <p style="margin-bottom: 25px;">The booking with the following details has been rescheduled by the you:</p>
                        <p style="margin-bottom: 25px;"><u><b>Original Booking</b></u></p>
                        <p><span style="min-width: 180px;">Doctor Name	</span>:<span style="margin-left: 25px;">'.$bookingRequest->first_name.'</span></p>
                        <p><span style="min-width: 180px;">Booking Id</span>:<span style="margin-left: 25px;">'.$bookingRequest->book_id.'</span></p>
                        <p><span style="min-width: 180px;">Appointment Type</span>:<span style="margin-left: 25px;">'.$consult_mode.'</span></p>
                        <p><span style="min-width: 180px;">Appointment Date</span>:<span style="margin-left: 25px;">'.$bookingRequest->consult_date.'</span></p>
                        <p><span style="min-width: 180px;">Appointment Time</span>:<span style="margin-left: 25px;">'.$bookingRequest->consult_time.'</span></p>
                       
                    </br>
                    <p style="margin-bottom: 25px;"><u><b>Rescheduled Booking</b></u></p>
                    <p><span style="min-width: 180px;">Doctor Name	</span>:<span style="margin-left: 25px;">'.$bookingRequest->first_name.'</span></p>
                    <p><span style="min-width: 180px;">Booking Id</span>:<span style="margin-left: 25px;">'.$Rescheduled->book_id.'</span></p>
                        <p><span style="min-width: 180px;">Appointment Type</span>:<span style="margin-left: 25px;">'.$consult_mode.'</span></p>
                        <p><span style="min-width: 180px;">Appointment Date</span>:<span style="margin-left: 25px;">'.$Rescheduled->consult_date.'</span></p>
                        <p><span style="min-width: 180px;">Appointment Time</span>:<span style="margin-left: 25px;">'.$Rescheduled->consult_time.'</span></p>
                    
                         <p><span style="min-width: 180px;">Waiting Room Opens	</span>:<span style="margin-left: 25px;">['.$Rescheduled->consult_time.' - 15 min]</span></p>
                <p style="margin-bottom: 25px;"><span style="min-width: 180px;">Waiting Room Closes</span>:<span style="margin-left: 25px;">['.$Rescheduled->consult_time.' + 60 min]</span></p>
                        <p style="margin-bottom: 25px;">Please note:</p>
                        <p style="margin-bottom: 0; padding-left: 20px; display: flex;">
                        <span style="margin-right: 15px;">1.</span>
                        Kindly join the waiting room 15 min before the scheduled appointment time.
                    </p>
                        <p style="margin-bottom: 0; padding-left: 20px; display: flex;">
                        <span style="margin-right: 15px;">2.</span>
                        You can cancel / reschedule the appointment upto '.$Rescheduled->consult_time.' on,'.$Rescheduled->consult_date.'
                    </p>
                    <p style="margin-bottom: 0; padding-left: 20px; display: flex;">
                    <span style="margin-right: 15px;">3.</span>
                    Appointment cannot be cancelled / rescheduled after the above mentioned time or after joining the waiting room.
                </p>
                    
                    <p style="margin-bottom: 0; padding-left: 20px; display: flex;">
                        <span style="margin-right: 15px;">4.</span>
                         In case of any difficulty, please contact Haspatal Support Team at: 
                            +91-124-405 56 56
                    </p>
                        <p style="margin-bottom: 25px; margin-top: 25px;">With Regards,</p>
                        <p style="margin-bottom: 25px;">Team Haspatal Care</p>
                    </div>
                </div>
            </body>
        </html>
        
        
'                 ;
        $data['personalizations'] = array("0" => array('to' => array('0' => array('email' =>  $bookingRequest->patient_email))));

        $data['from'] = array('email' => 'system@haspatal.com');

        $data['subject'] = $subject;
        $data['content'] = array("0" =>array('type' =>' text/html','value' => $mailmessage ));

        //echo json_encode($data);exit();

        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => "https://api.sendgrid.com/v3/mail/send",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => json_encode($data),
        CURLOPT_HTTPHEADER => array(
            "authorization: Bearer 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",
            "cache-control: no-cache",
            "content-type: application/json",
            "postman-token: b8c0781f-af48-90cc-1c5b-76f80e0b4be0"
        ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            return "Send Alert in your emailId";
        }
    }




     public function booking_pending(Request $request)
    {

        $bookingRequest_data = BookingRequest::where('booking_request.patient_name',$request->patient_name)
                    ->where('booking_request.status', 1) 
                    ->join('doctor_details', 'booking_request.doctor_name', '=', 'doctor_details.id')
                    ->select('booking_request.*','doctor_details.first_name','doctor_details.last_name','doctor_details.profile_pic','doctor_details.clinic_logo')
                    ->orderBy('id', 'desc')
                    ->get(); 
        
         $path = array("profile_pic"=>env('APP_URL').'/public/media/profile_pic',
                            "clinic_logo"=>env('APP_URL').'/public/media/clinic_logo'
                        );        
         if ($bookingRequest_data) {
            return response()->json(["status" => true,"data" => $bookingRequest_data, "message" => "Booking Request saved successfully","path" => $path]);
        }else{
            return response()->json(["status" => false,"data" => '' , "message" => "Booking Request not saved successfully"]);
        }
    }

    public function haspatal_schedule(Request $request)
    {
         $startDate =  Carbon::parse($request->s_date)->startOfDay(); 
         $endDate =  Carbon::now()->endOfDay();
         $doctor_id = Auth::user()->userDetailsId;
         $patients = BookingRequest::join('patient_details','booking_request.patient_name','patient_details.id')
                              ->select('booking_request.*','patient_details.first_name','patient_details.last_name','patient_details.id as patient_id')
                              ->whereBetween('booking_request.date_time', [$startDate,$endDate])
                              ->where('booking_request.doctor_name', $doctor_id)
                              ->where('booking_request.status','!=' , 5)
                              ->orderBy('booking_request.id', 'desc')
                              ->get();
                              
        if($request->s_date == '')
        {
             $patients = BookingRequest::join('patient_details','booking_request.patient_name','patient_details.id')
                              ->select('booking_request.*','patient_details.first_name','patient_details.last_name','patient_details.id as patient_id')
                              ->where('booking_request.doctor_name', $doctor_id)
                              ->where('booking_request.status','!=' , 5)
                              ->get();
        }

         $path = array("profile_pic"=>env('APP_URL').'/public/media/profile_pic',
                        "clinic_logo"=>env('APP_URL').'/public/media/clinic_logo'
                        );
         if ($patients) {
            return response()->json(["status" => true,"data" => $patients, "message" => "Booking Request saved successfully","path" => $path]);
        }else{
            return response()->json(["status" => false,"data" => '' , "message" => "Booking Request not saved successfully"]);
        }

    }
    /**
     * @param CreateBookingRequestAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/bookingRequests",
     *      summary="Store a newly created BookingRequest in storage",
     *      tags={"BookingRequest"},
     *      description="Store BookingRequest",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="BookingRequest that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/BookingRequest")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/BookingRequest"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(Request $request)
    {

        // return $request->consult_date;
        $time = strtotime($request->consult_date);

        $newformat = date('Y-m-d',$time);
        $date_time=$newformat.' '.$request->consult_time;

        // return $date_time;
        //echo "string"; exit;
        $input = $request->all();
        $book =  DB::table('booking_request')->orderBy('id', 'desc')->first();
         if(isset($book->book_id) != ''){
            $bookdata = explode('-', $book->book_id); 
            $dd = $bookdata['2'] + 1;   
            $input['book_id'] = $bookdata['0'].'-'.$bookdata['1'].'-'.'000000'.$dd;
            }else{
            $input['book_id'] = '91-B-0000001';
            }
        
        if($request->w_status == 1)
        {
            $total_fund =  DB::table('patient_add_fund')->where('patient_id', $request->patient_name)->first();
            if (empty($total_fund)) {
                return response()->json(["status" => false,"message" => "Insuficient Fund"]);
            }
            if ( $total_fund->wa_amount<=0) {
                return response()->json(["status" => false,"message" => "Insuficient Fund"]);
            }
            $mi_fund = $total_fund->wa_amount - $request->f_amount;
            $mi_fund=$mi_fund - $request->dr_price;
            $input['w_amount'] =  $mi_fund;
            $fund =  DB::table('patient_add_fund')->where('patient_id',$request->patient_name)->update(['wa_amount' => $mi_fund]);
            $haspatalcharge =  DB::table('Haspatalwalletfund')->insert(
                ['patient' => $request->patient_name, 'doctor' => $request->doctor_name, 'hasptalcharge' => $request->dr_price,]
            );
        }

            
        $bookingRequest_data = BookingRequest::where('booking_request.doctor_name',$request->doctor_name)  
                       // ->join('patient_details', 'booking_request.patient_name', '=', 'patient_details.id')
                        ->join('doctor_details', 'booking_request.doctor_name', '=', 'doctor_details.id')
                        ->select('booking_request.*','doctor_details.first_name','doctor_details.last_name','doctor_details.profile_pic')
                        ->orderBy('id', 'desc')
                        ->first(); 
        $path = env('APP_URL').'/public/media/profile_pic';


        $bookingRequest = $this->bookingRequestRepository->create($input);
        $bookingRequest->consult_date=$request->consult_date;
        $bookingRequest->consult_time=$request->consult_time;
        $bookingRequest->appointment_time=$newformat;
        $bookingRequest->filter_date_time=$date_time;
        $bookingRequest->save(); 
        if ($bookingRequest) {
            $patient =  DB::table('patient_details')->where('id',$request->patient_name)->first();

            $user_patient = \App\User::where('userDetailsId',$request->patient_name)->where('user_type',2)->first();
            $user_dr = \App\User::where('userDetailsId',$request->doctor_name)->where('user_type',1)->first();
            $user_patient_sms = \App\User::where('userDetailsId',$request->patient_name)->first();
            $user_dr_sms = \App\User::where('userDetailsId',$request->doctor_name)->first();
            
            //echo "<pre>";print_r($patient);exit();
            // $curl = curl_init();
            // curl_setopt_array($curl, array(
            //     CURLOPT_URL => "http://sms.bulksmsind.in/sendSMS?username=haspatal&message=".urlencode('Your Appointment Booking Successfully')."&sendername=HSPTAL&smstype=TRANS&numbers=".$patient->mobile."&apikey=206f20fc-d4d5-4a33-ad06-10f453b8ab4a",
            //     CURLOPT_RETURNTRANSFER => true,
            //     CURLOPT_ENCODING => true,
            //     CURLOPT_MAXREDIRS => 10,
            //     CURLOPT_TIMEOUT => 30,
            //     CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            //     CURLOPT_CUSTOMREQUEST => "GET",
            // ));
            // $response = curl_exec($curl);
            // $err = curl_error($curl);
            // curl_close($curl);
           $doctor_mobile=$user_dr_sms->mobile;
           // return  $message;
            $message="Dear Dr. ".$user_dr_sms->first_name.",
            Your Slot at ".$request->consult_time." and  ".$request->consult_date.", has been booked by ".$user_patient_sms->first_name." through Haspatal APP. 
            Regards
            Haspatal Team";
            // return  $message;
             $curl = curl_init();
             curl_setopt_array($curl, array(
                 CURLOPT_URL => "http://sms.bulksmsind.in/sendSMS?username=haspatal&message=".urlencode(  $message)."&sendername=MASUPP&smstype=TRANS&numbers=".$doctor_mobile."&apikey=206f20fc-d4d5-4a33-ad06-10f453b8ab4a",
                 CURLOPT_RETURNTRANSFER => true,
                 CURLOPT_ENCODING => true,
                 CURLOPT_MAXREDIRS => 10,
                 CURLOPT_TIMEOUT => 30,
                 CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                 CURLOPT_CUSTOMREQUEST => "GET",
             ));
             $response = curl_exec($curl);
            //  return  $response;
             $err = curl_error($curl);
             curl_close($curl);
            if ($err) {
                
                return response()->json(["status" => true,"message" => "","data"=>$err]);
            } else {


                $bookingRequest_data = BookingRequest::where('booking_request.doctor_name',$request->doctor_name)  
                        ->join('doctor_details', 'booking_request.doctor_name', '=', 'doctor_details.id')
                        ->select('booking_request.*','doctor_details.first_name','doctor_details.last_name','doctor_details.profile_pic')
                        ->orderBy('id', 'desc')
                        ->first(); 
                
                $this->send_notification($user_patient->mobile_token,"Appointment booking successfully","Booking",$bookingRequest_data);
                $this->send_notification($user_dr->mobile_token,$patient->first_name." Appointment booked","Booking",$bookingRequest_data);
                
                /*$this->send_mail($user_patient,"Appointment with Dr ".$user_dr->first_name."  booked successfully for ".$bookingRequest->date_time);*/

                $this->send_mail1($user_patient,$user_dr,$bookingRequest);
                $this->send_mail2($user_dr,$user_patient,$bookingRequest);

                return response()->json(["status" => true,"data" => $bookingRequest_data, "message" => "Booking Request saved successfully","path" => $path]);
                
                
            }
        }else{
            return response()->json(["status" => false,"data" => '' , "message" => "Booking Request not saved successfully"]);
        }

        //return $this->sendResponse($bookingRequest_data->toArray(), 'Booking Request saved successfully');
    }
    public function storeEclinic(Request $request)
    {

        // return $request->consult_date;
        $time = strtotime($request->consult_date);

        $newformat = date('Y-m-d',$time);
        // return $newformat;
        //echo "string"; exit;
        $input = $request->all();
        $book =  DB::table('booking_request')->orderBy('id', 'desc')->first();
         if(isset($book->book_id) != ''){
            $bookdata = explode('-', $book->book_id); 
            $dd = $bookdata['2'] + 1;   
            $input['book_id'] = $bookdata['0'].'-'.$bookdata['1'].'-'.'000000'.$dd;
            }else{
            $input['book_id'] = '91-B-0000001';
            }
        
        if($request->w_status == 1)
        {
            $total_fund =  DB::table('wallet_360')->where('user_id', $request->pharamcy_id)->first();
            if (empty($total_fund)) {
                return response()->json(["status" => false,"message" => "Insuficient Fund"]);
            }
            if ( $total_fund->wa_amount<=0) {
                return response()->json(["status" => false,"message" => "Insuficient Fund"]);
            }
            $mi_fund = $total_fund->wa_amount - $request->f_amount;
            $mi_fund=$mi_fund - $request->dr_price;
            $input['w_amount'] =  $mi_fund;
            $fund =  DB::table('wallet_360')->where('user_id',$request->pharamcy_id)->update(['wa_amount' => $mi_fund]);
            $haspatalcharge =  DB::table('Haspatalwalletfund')->insert(
                ['patient' => $request->patient_name, 'doctor' => $request->doctor_name, 'hasptalcharge' => $request->dr_price,]
            );
        }

            
        $bookingRequest_data = BookingRequest::where('booking_request.doctor_name',$request->doctor_name)  
                       // ->join('patient_details', 'booking_request.patient_name', '=', 'patient_details.id')
                        ->join('doctor_details', 'booking_request.doctor_name', '=', 'doctor_details.id')
                        ->select('booking_request.*','doctor_details.first_name','doctor_details.last_name','doctor_details.profile_pic')
                        ->orderBy('id', 'desc')
                        ->first(); 
        $path = env('APP_URL').'/public/media/profile_pic';


        $bookingRequest = $this->bookingRequestRepository->create($input);
        $bookingRequest->consult_date=$request->consult_date;
        $bookingRequest->consult_time=$request->consult_time;
        $bookingRequest->consult_time=$request->consult_time;
        $bookingRequest->pharmacy_id=$request->pharamcy_id;
        $bookingRequest->save(); 
        if ($bookingRequest) {
            $patient =  DB::table('patient_details')->where('id',$request->patient_name)->first();

            $user_patient = \App\User::where('userDetailsId',$request->patient_name)->where('user_type',2)->first();
            $user_dr = \App\User::where('userDetailsId',$request->doctor_name)->where('user_type',1)->first();
            
            //echo "<pre>";print_r($patient);exit();
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => "http://sms.bulksmsind.in/sendSMS?username=haspatal&message=".urlencode('Your Appointment Booking Successfully')."&sendername=HSPTAL&smstype=TRANS&numbers=".$patient->mobile."&apikey=206f20fc-d4d5-4a33-ad06-10f453b8ab4a",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => true,
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
            ));
            $response = curl_exec($curl);
            $err = curl_error($curl);
            curl_close($curl);
            if ($err) {
                
                return response()->json(["status" => true,"message" => "","data"=>$err]);
            } else {


                $bookingRequest_data = BookingRequest::where('booking_request.doctor_name',$request->doctor_name)  
                        ->join('doctor_details', 'booking_request.doctor_name', '=', 'doctor_details.id')
                        ->select('booking_request.*','doctor_details.first_name','doctor_details.last_name','doctor_details.profile_pic')
                        ->orderBy('id', 'desc')
                        ->first(); 
                
                $this->send_notification($user_patient->mobile_token,"Appointment booking successfully","Booking",$bookingRequest_data);
                $this->send_notification($user_dr->mobile_token,$patient->first_name." Appointment booked","Booking",$bookingRequest_data);
                
                /*$this->send_mail($user_patient,"Appointment with Dr ".$user_dr->first_name."  booked successfully for ".$bookingRequest->date_time);*/

                $this->send_mail1($user_patient,$user_dr,$bookingRequest);
                $this->send_mail2($user_dr,$user_patient,$bookingRequest);

                return response()->json(["status" => true,"data" => $bookingRequest_data, "message" => "Booking Request saved successfully","path" => $path]);
                
                
            }
        }else{
            return response()->json(["status" => false,"data" => '' , "message" => "Booking Request not saved successfully"]);
        }

        //return $this->sendResponse($bookingRequest_data->toArray(), 'Booking Request saved successfully');
    }
    public function send_mail1($user,$dr,$bookdata)
    {
        // $old_date_timestamp = strtotime($bookdata->date_time);
        // $new_date = date('d-m-Y', $old_date_timestamp);   
        // $new_time = date('h:i A', $old_date_timestamp);   

        // $endTime = strtotime("-15 minutes", strtotime($bookdata->date_time));
        // $new_time_add = date('h:i A, d-F-Y', $endTime);
        

        // $msg = "<DOCTYPE html>
        //             <html lang='en-US'>         
        //                 <head>
        //                     <meta charset='utf-8'>
        //                 </head>
        //                 <body>
        //                     <center>
        //                     </center>
        //                         <img src='".env('APP_URL')."./public/image/has.png'>
        //                         <br><br>
        //                         <p>Dear ".$user->first_name."</p>
        //                         <p>Thanks for using Haspatal.com to book the appointment with the following details:</p>
        //                         <h4>Consult Boooking Id</h4>
        //                         <p>".$bookdata->book_id."</p>
        //                         <h4>Doctor Name</h4>
        //                         <p>".$dr->first_name."</p>
        //                         <h4>Appointment Date</h4>
        //                         <p>".$new_date."</p>
        //                         <h4>Appointment Time</h4>
        //                         <p>".$new_time."</p>
        //                         <h4>Kindly Note</h3>
        //                         <p>1. You will receive a reminder email on the date of appointment.</p>
        //                         <p>2. You will also receive an alert email 15 minutes before the booked slot requesting you to enter the waiting room.</p>
        //                         <p>3. You can watch our video to understand the whole process by clicking on button below.</p>
        //                         <p>4. You can also watch our video on how to prepare for best results of video consults.</p>
        //                         <p>5. In case , you need to cancel or reschedule the appointment, you can do it up to <b>".$new_time_add."</b> to receive the full refund.</p>
        //                         <p>6. Please note that cancellation, refund or reschedule requests can not be made after once the waiting room has been activated.</p>
        //                         <p>7. We wish you best outcome of your consult and assure you best of cooperation.</p>
        //                         <br><br>
                                
        //                         <p>Always yours</p>
        //                         <p>Team Hasptal</p>
        //                 </body>
        //             </html>";

        
        // $data['personalizations'] = array("0" => array('to' => array('0' => array('email' => $user->email))));

        // $data['from'] = array('email' => 'system@haspatal.com');

        // $data['subject'] = "Booking Successful ! ".$dr->first_name.", ".$bookdata->date_time;
        // $data['content'] = array("0" =>array('type' =>' text/html','value' => $msg ));

        // //echo json_encode($data);exit();

        // $curl = curl_init();

        // curl_setopt_array($curl, array(
        //   CURLOPT_URL => "https://api.sendgrid.com/v3/mail/send",
        //   CURLOPT_RETURNTRANSFER => true,
        //   CURLOPT_ENCODING => "",
        //   CURLOPT_MAXREDIRS => 10,
        //   CURLOPT_TIMEOUT => 30,
        //   CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        //   CURLOPT_CUSTOMREQUEST => "POST",
        //   CURLOPT_POSTFIELDS => json_encode($data),
        //   CURLOPT_HTTPHEADER => array(
        //     "authorization: Bearer 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",
        //     "cache-control: no-cache",
        //     "content-type: application/json",
        //     "postman-token: b8c0781f-af48-90cc-1c5b-76f80e0b4be0"
        //   ),
        // ));

        // $response = curl_exec($curl);
        // $err = curl_error($curl);

        // curl_close($curl);

        // if ($err) {
        //   echo "cURL Error #:" . $err;
        // } else {
        //   return $response;
        // }






        $bookingRequest = BookingRequest::where('booking_request.book_id',$bookdata->book_id) 
        // ->where('booking_request.status','!=',5) 
        ->join('patient_details', 'booking_request.patient_name', '=', 'patient_details.id')
        ->join('doctor_details', 'booking_request.doctor_name', '=', 'doctor_details.id')
        ->select('booking_request.*','patient_details.first_name as patient','patient_details.email as patient_email','doctor_details.*')->first();
    if($bookingRequest->consult_mode==1){
        $consult_mode="Video Consultation";
    }else{
        $consult_mode="In-Clinic Consultation";
    }

    $payment =  Patient_add_fund::where('patient_id',$bookingRequest->patient_name)->first();
   //  return $payment;
        $subject =
        " Booking Confirmation -".$bookingRequest->patient."-".$bookingRequest->book_id;
        // dd( $user);
        $mailmessage= '<!DOCTYPE html>
        <html lang="en">
            <head>
                <title>Dr email template</title>
                <meta charset="utf-8">
                <!-- CSS only -->
                <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
                <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
                <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900,900i&display=swap" rel="stylesheet">
                <script src="https://kit.fontawesome.com/fa5e54101c.js" crossorigin="anonymous"></script>
                <!-- JavaScript Bundle with Popper -->
        
                <!-- Theme Style -->
                <!-- <link rel="stylesheet" href="/assets/css/bootstrap.min.css"> -->
                <style>
                    #page-wrap{
                        max-width: 800px; 
                        margin: 0 auto;
                        font-family: "Roboto";
                        /* padding: 25px 0; */
                    }
                    p,h5,h6{
                        color: #000;
                        font-family: "Roboto";
                        margin: 0;
                    }
                    p span{
                        display: inline-block;
                    }
                </style>
            </head>
            <body>
                <div id="page-wrap">
                    <div class="top">
                    
                        <h6 style="margin-bottom: 25px; font-weight:400; font-size: 16px;">Dear '.$bookingRequest->first_name.', </h6>
                        <p style="margin-bottom: 25px;">We are pleased to inform you about a confirmed booking with the following details:</p>
                
                        <p><span style="min-width: 180px;">Booking Id</span>:<span style="margin-left: 25px;">'.$bookingRequest->book_id.'</span></p>
                        <p><span style="min-width: 180px;">Appointment Type</span>:<span style="margin-left: 25px;">'.$consult_mode.'</span></p>
                        <p><span style="min-width: 180px;">Appointment Date</span>:<span style="margin-left: 25px;">'.$bookingRequest->consult_date.'</span></p>
                        <p><span style="min-width: 180px;">Appointment Time</span>:<span style="margin-left: 25px;">'.$bookingRequest->consult_time.'</span></p>
                        <p><span style="min-width: 180px;">Patient Name	</span>:<span style="margin-left: 25px;">'.$bookingRequest->patient.'</span></p>
                         <p><span style="min-width: 180px;">Waiting Room Opens	</span>:<span style="margin-left: 25px;">['.$bookingRequest->consult_time.' - 15 min]</span></p>
                <p style="margin-bottom: 25px;"><span style="min-width: 180px;">Waiting Room Closes</span>:<span style="margin-left: 25px;">['.$bookingRequest->consult_time.' + 60 min]</span></p>
                        <p style="margin-bottom: 25px;">Please note:</p>
                        <p style="margin-bottom: 0; padding-left: 20px; display: flex;">
                        <span style="margin-right: 15px;">1.</span>
                        You can cancel / reschedule the appointment upto '.$bookingRequest->consult_time.' on,'.$bookingRequest->consult_date.'
                    </p>
                    <p style="margin-bottom: 0; padding-left: 20px; display: flex;">
                        <span style="margin-right: 15px;">2.</span>
                         In case of any difficulty, please contact Haspatal Support Team at: 
                            +91-124-405 56 56
                    </p>
                        <p style="margin-bottom: 25px; margin-top: 25px;">With Regards,</p>
                        <p style="margin-bottom: 25px;">Team Haspatal MD</p>
                    </div>
                </div>
            </body>
        </html>
        
        
'                 ;
        $data['personalizations'] = array("0" => array('to' => array('0' => array('email' =>  $bookingRequest->email))));

        $data['from'] = array('email' => 'system@haspatal.com');

        $data['subject'] = $subject;
        $data['content'] = array("0" =>array('type' =>' text/html','value' => $mailmessage ));

        //echo json_encode($data);exit();

        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => "https://api.sendgrid.com/v3/mail/send",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => json_encode($data),
        CURLOPT_HTTPHEADER => array(
            "authorization: Bearer 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",
            "cache-control: no-cache",
            "content-type: application/json",
            "postman-token: b8c0781f-af48-90cc-1c5b-76f80e0b4be0"
        ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            return "Send Alert in your emailId";
        }
    }

    public function send_mail2($user,$patient,$bookdata)
    {
        // $old_date_timestamp = strtotime($bookdata->date_time);
        // $new_date = date('d-M-Y', $old_date_timestamp);   
        // $new_time = date('h:i A', $old_date_timestamp);   

        // $endTime = strtotime("-15 minutes", strtotime($bookdata->date_time));
        // $waitingRoomendTime = strtotime("+45 minutes", strtotime($bookdata->date_time));

        // $waiting_start_time = date('h:i A',$endTime);
        // $waiting_end_time = date('h:i A',$waitingRoomendTime);

        // $new_time_add = date('h:i A, d-F-Y', strtotime("-30 minutes", strtotime($bookdata->date_time)));
        
        // if ($bookdata->consult_mode == "Online") {
        //     $a_type = "Video Consultation";
        // }else{
        //     $a_type = "Offline";
        // }
        // if ($bookdata->w_status == 1) {
        //     $p_type = "Paid";
        // }else{
        //     $p_type = "Paid";
        // }

        // $msg = "<DOCTYPE html>
        //             <html lang='en-US'>         
        //                 <head>
        //                     <meta charset='utf-8'>
        //                 </head>
        //                 <body>
        //                     <center>
        //                     </center>
        //                         <p><b>Dear ".$user->first_name."</b></p>
        //                         <p>We are pleased to inform you about a confirmed booking with following details :</p>

        //                         <p>Appointment ID :".$bookdata->book_id."</p>
        //                         <p>Appointment Type :".$a_type."</P>
        //                         <p>Appointment Date :".$new_date."</p>
        //                         <p>Appointment Time :".$new_time."</p>
        //                         <p>Waiting Room Opens :".$waiting_start_time."</p>
        //                         <p>Waiting Room Closes :".$waiting_end_time."</p>
        //                         <p>Patient Name :".$patient->first_name."</p>
        //                         <p>Payment Status :".$p_type."</p>
                                

        //                         <p>Please note:</p>
        //                         <p>1. Kindly Join the waiting room 5 minutes before the scheduled time.</p>
        //                         <p>2. You can cancel/ Reschedule the appointment upto ".$new_time_add."</p>
        //                         <p>3. In case of any difficulty, please contact doctors support team at
        //                         [9599661700]</p>

        //                         <p>Best wishes
        //                         <br>Team Haspatal</p>
        //                 </body>
        //             </html>";

        
        
        // $data['personalizations'] = array("0" => array('to' => array('0' => array('email' => $user->email))));

        // $data['from'] = array('email' => 'system@haspatal.com');

        // $data['subject'] = "Booking Successful !";
        // $data['content'] = array("0" =>array('type' =>' text/html','value' => $msg ));

        // //echo json_encode($data);exit();

        // $curl = curl_init();

        // curl_setopt_array($curl, array(
        //   CURLOPT_URL => "https://api.sendgrid.com/v3/mail/send",
        //   CURLOPT_RETURNTRANSFER => true,
        //   CURLOPT_ENCODING => "",
        //   CURLOPT_MAXREDIRS => 10,
        //   CURLOPT_TIMEOUT => 30,
        //   CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        //   CURLOPT_CUSTOMREQUEST => "POST",
        //   CURLOPT_POSTFIELDS => json_encode($data),
        //   CURLOPT_HTTPHEADER => array(
        //     "authorization: Bearer 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",
        //     "cache-control: no-cache",
        //     "content-type: application/json",
        //     "postman-token: b8c0781f-af48-90cc-1c5b-76f80e0b4be0"
        //   ),
        // ));

        // $response = curl_exec($curl);
        // $err = curl_error($curl);

        // curl_close($curl);

        // if ($err) {
        //   echo "cURL Error #:" . $err;
        // } else {
        //   return $response;
        // }


        $bookingRequest = BookingRequest::where('booking_request.book_id',$bookdata->book_id) 
        // ->where('booking_request.status','!=',5) 
        ->join('patient_details', 'booking_request.patient_name', '=', 'patient_details.id')
        ->join('doctor_details', 'booking_request.doctor_name', '=', 'doctor_details.id')
        ->select('booking_request.*','patient_details.first_name as patient','patient_details.email as patient_email','doctor_details.*')->first();
    if($bookingRequest->consult_mode==1){
        $consult_mode="Video Consultation";
    }else{
        $consult_mode="In-Clinic Consultation";
    }

    $payment =  Patient_add_fund::where('patient_id',$bookingRequest->patient_name)->first();
   //  return $payment;
        $subject =
        " Booking Confirmation -".$bookingRequest->first_name."-".$bookingRequest->book_id;
        // dd( $user);
        $mailmessage= '<!DOCTYPE html>
        <html lang="en">
            <head>
                <title>Dr email template</title>
                <meta charset="utf-8">
                <!-- CSS only -->
                <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
                <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
                <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900,900i&display=swap" rel="stylesheet">
                <script src="https://kit.fontawesome.com/fa5e54101c.js" crossorigin="anonymous"></script>
                <!-- JavaScript Bundle with Popper -->
        
                <!-- Theme Style -->
                <!-- <link rel="stylesheet" href="/assets/css/bootstrap.min.css"> -->
                <style>
                    #page-wrap{
                        max-width: 800px; 
                        margin: 0 auto;
                        font-family: "Roboto";
                        /* padding: 25px 0; */
                    }
                    p,h5,h6{
                        color: #000;
                        font-family: "Roboto";
                        margin: 0;
                    }
                    p span{
                        display: inline-block;
                    }
                </style>
            </head>
            <body>
                <div id="page-wrap">
                    <div class="top">
                    
                        <h6 style="margin-bottom: 25px; font-weight:400; font-size: 16px;">Dear '.$bookingRequest->patient.', </h6>
                        <p style="margin-bottom: 25px;">We are pleased to inform you about a confirmed booking with the following details:</p>
                        <p><span style="min-width: 180px;">Doctor Name	</span>:<span style="margin-left: 25px;">'.$bookingRequest->first_name.'</span></p>
                        <p><span style="min-width: 180px;">Booking Id</span>:<span style="margin-left: 25px;">'.$bookingRequest->book_id.'</span></p>
                        <p><span style="min-width: 180px;">Appointment Type</span>:<span style="margin-left: 25px;">'.$consult_mode.'</span></p>
                        <p><span style="min-width: 180px;">Appointment Date</span>:<span style="margin-left: 25px;">'.$bookingRequest->consult_date.'</span></p>
                        <p><span style="min-width: 180px;">Appointment Time</span>:<span style="margin-left: 25px;">'.$bookingRequest->consult_time.'</span></p>
                     
                         <p><span style="min-width: 180px;">Waiting Room Opens	</span>:<span style="margin-left: 25px;">['.$bookingRequest->consult_time.' - 15 min]</span></p>
                <p style="margin-bottom: 25px;"><span style="min-width: 180px;">Waiting Room Closes</span>:<span style="margin-left: 25px;">['.$bookingRequest->consult_time.' + 60 min]</span></p>
                        <p style="margin-bottom: 25px;">Please note:</p>
                        <p style="margin-bottom: 0; padding-left: 20px; display: flex;">
                        <span style="margin-right: 15px;">1.</span>
                        Kindly join the waiting room 15 min before the scheduled appointment time.
                    </p>
                        <p style="margin-bottom: 0; padding-left: 20px; display: flex;">
                        <span style="margin-right: 15px;">2.</span>
                        You can cancel / reschedule the appointment upto '.$bookingRequest->consult_time.' on,'.$bookingRequest->consult_date.'
                    </p>
                    <p style="margin-bottom: 0; padding-left: 20px; display: flex;">
                    <span style="margin-right: 15px;">3.</span>
                    Appointment cannot be cancelled / rescheduled after the above mentioned time or after joining the waiting room.
                </p>
                    <p style="margin-bottom: 0; padding-left: 20px; display: flex;">
                        <span style="margin-right: 15px;">4.</span>
                         In case of any difficulty, please contact Haspatal Support Team at: 
                            +91-124-405 56 56
                    </p>
                    <p style="margin-bottom: 25px; margin-top: 25px;">Wishing you good health,</p>
                        <p style="margin-bottom: 25px; margin-top: 25px;">With Regards,</p>
                        <p style="margin-bottom: 25px;">Team Haspatal Care</p>
                    </div>
                </div>
            </body>
        </html>
        
        
'                 ;
        $data['personalizations'] = array("0" => array('to' => array('0' => array('email' =>  $bookingRequest->patient_email))));

        $data['from'] = array('email' => 'system@haspatal.com');

        $data['subject'] = $subject;
        $data['content'] = array("0" =>array('type' =>' text/html','value' => $mailmessage ));

        //echo json_encode($data);exit();

        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => "https://api.sendgrid.com/v3/mail/send",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => json_encode($data),
        CURLOPT_HTTPHEADER => array(
            "authorization: Bearer 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",
            "cache-control: no-cache",
            "content-type: application/json",
            "postman-token: b8c0781f-af48-90cc-1c5b-76f80e0b4be0"
        ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            return "Send Alert in your emailId";
        }
        
    }    

    public function send_mail($user,$mes)
    {
        $data['personalizations'] = array("0" => array('to' => array('0' => array('email' => $user->email))));

        $data['from'] = array('email' => 'system@haspatal.com');

        $data['subject'] = "Appointment Booking";
        $data['content'] = array("0" =>array('type' =>' text/html','value' => "<DOCTYPE html><html lang='en-US'>     <head><meta charset='utf-8'></head><body><h2>  ".$mes."</body></html>" ));

        //echo json_encode($data);exit();

        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://api.sendgrid.com/v3/mail/send",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => json_encode($data),
          CURLOPT_HTTPHEADER => array(
            "authorization: Bearer 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",
            "cache-control: no-cache",
            "content-type: application/json",
            "postman-token: b8c0781f-af48-90cc-1c5b-76f80e0b4be0"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          echo "cURL Error #:" . $err;
        } else {
          return $response;
        }
    }

    public function send_notification($token,$message,$title,$data = array())
    {
        $content = array(
           "en" => $message
        );

        $heading = array(
           "en" => $title
        );
       
        $fields = array(
           'app_id' => "33fe1b86-089a-4234-af9d-d7a923926620",
           'include_player_ids' => array($token),
           'data' => array("data" => $data),
           'contents' => $content,
           'headings' => $heading
        );
           
        $fields = json_encode($fields);
        
       
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $response = curl_exec($ch);
        curl_close($ch);
        //echo "<pre>";print_r($response);exit();
        return $response;
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/bookingRequests/{id}",
     *      summary="Display the specified BookingRequest",
     *      tags={"BookingRequest"},
     *      description="Get BookingRequest",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of BookingRequest",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/BookingRequest"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
     {
        /** @var BookingRequest $bookingRequest */
       /* $bookingRequest[] = $this->bookingRequestRepository->find($id);*/
         $bookingRequest = BookingRequest::where('booking_request.id',$id)  
                        ->join('patient_details', 'booking_request.patient_name', '=', 'patient_details.id')
                        ->join('doctor_details', 'booking_request.doctor_name', '=', 'doctor_details.id')
                        ->select('booking_request.*','patient_details.first_name as patient','doctor_details.first_name','doctor_details.last_name','doctor_details.clinic_logo')
                        ->get(); 
         $path = array("profile_pic"=>env('APP_URL').'/public/media/profile_pic',
                            "clinic_logo"=>env('APP_URL').'/public/media/clinic_logo'
                        );   

        if ($bookingRequest) {
            return response()->json(["status" => true,"massage"=>"Booking Request retrieved successfully","data" => $bookingRequest, "path"=>$ $path ]);
        }else{
             return response()->json(["status" => false,"data" => $bookingRequest]);
        }
    }
    /*{

        /** @var BookingRequest $bookingRequest */
       // $bookingRequest = $this->bookingRequestRepository->find($id);

       /* if (empty($bookingRequest)) {
            return $this->sendError('Booking Request not found');
        }

        return $this->sendResponse($bookingRequest->toArray(), 'Booking Request retrieved successfully');
    }
*/
    /**
     * @param int $id
     * @param UpdateBookingRequestAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/bookingRequests/{id}",
     *      summary="Update the specified BookingRequest in storage",
     *      tags={"BookingRequest"},
     *      description="Update BookingRequest",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of BookingRequest",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="BookingRequest that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/BookingRequest")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/BookingRequest"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateBookingRequestAPIRequest $request)
    {
        $input = $request->all();

        /** @var BookingRequest $bookingRequest */
        $bookingRequest = $this->bookingRequestRepository->find($id);

        if (empty($bookingRequest)) {
            return $this->sendError('Booking Request not found');
        }

        $bookingRequest = $this->bookingRequestRepository->update($input, $id);

        return $this->sendResponse($bookingRequest->toArray(), 'BookingRequest updated successfully');
    }

    public function update_call_status($booking_id)
    {
        $result =  DB::table('booking_request')->where('book_id',$booking_id)->update(['call_status' => 1]);

        if ($result) {
            return response()->json(["status" => true]);
        }else{
            return response()->json(["status" => false]);
        }
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/bookingRequests/{id}",
     *      summary="Remove the specified BookingRequest from storage",
     *      tags={"BookingRequest"},
     *      description="Delete BookingRequest",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of BookingRequest",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var BookingRequest $bookingRequest */
        $bookingRequest = $this->bookingRequestRepository->find($id);

        if (empty($bookingRequest)) {
            return $this->sendError('Booking Request not found');
        }

        $bookingRequest->delete();

        return $this->sendSuccess('Booking Request deleted successfully');
    }
}
