<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateHaspatalRegsiterAPIRequest;
use App\Http\Requests\API\UpdateHaspatalRegsiterAPIRequest;
use App\Models\HaspatalRegsiter;
use App\Repositories\HaspatalRegsiterRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;
use App\Models\Doctors;
use DB;
/**
 * Class HaspatalRegsiterController
 * @package App\Http\Controllers\API
 */

class HaspatalRegsiterAPIController extends AppBaseController
{
    /** @var  HaspatalRegsiterRepository */
    private $haspatalRegsiterRepository;

    public function __construct(HaspatalRegsiterRepository $haspatalRegsiterRepo)
    {
        $this->haspatalRegsiterRepository = $haspatalRegsiterRepo;
    }

    /**
     * Display a listing of the HaspatalRegsiter.
     * GET|HEAD /haspatalRegsiters
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $haspatalRegsiters = $this->haspatalRegsiterRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($haspatalRegsiters->toArray(), 'Haspatal Regsiters retrieved successfully');
    }

    public function haspatalSearch(Request $request)
    {

        // $haspatal = HaspatalRegsiter::where('status',1)->where('state_id',$request->state)->where('city_id',$request->city)->where('hospital_name','LIKE', '%' . $request->hospital_name . '%');
        $haspatal=DB::table('haspatal_regsiter')->select('*')->where('status',1)->where('state_id',$request->state)->where('city_id',$request->city)->where('hospital_name','LIKE', '%' . $request->hospital_name . '%');
        // if ($request->state != 0) {
        //     $haspatal->where('state_id',$request->state);
        // }

        // if ($request->city != 0) {
        //     $haspatal->where('city_id',$request->city);
        // }

        // if ($request->hospital_name != 0) {
        //    $haspatal->where('hospital_name','LIKE', '%' . $request->hospital_name . '%');
        // }

        $haspatalRegsiter = $haspatal->get();
        
        return $this->sendResponse($haspatalRegsiter, 'Haspatal retrieved successfully');
    }

    public function hospital_doctor_list($hospitalId)
    {
        $doctors = Doctors::where('created_by',$hospitalId)->get();

        $path = array("licence_copy"=>env('APP_URL').'media/licence_copy',
                            "profile_pic"=>env('APP_URL').'media/profile_pic',
                            "clinic_logo"=>env('APP_URL').'media/clinic_logo'
                        );

        return response()->json(["status" => true,"massage"=>"Haspatal retrieved successfully","data" => $doctors,"path" => $path]);
        return $this->sendResponse($doctors, 'Haspatal retrieved successfully');
    }

    /**
     * Store a newly created HaspatalRegsiter in storage.
     * POST /haspatalRegsiters
     *
     * @param CreateHaspatalRegsiterAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateHaspatalRegsiterAPIRequest $request)
    {
        $input = $request->all();

        $haspatalRegsiter = $this->haspatalRegsiterRepository->create($input);

        return $this->sendResponse($haspatalRegsiter->toArray(), 'Haspatal Regsiter saved successfully');
    }

    /**
     * Display the specified HaspatalRegsiter.
     * GET|HEAD /haspatalRegsiters/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var HaspatalRegsiter $haspatalRegsiter */
        $haspatalRegsiter = $this->haspatalRegsiterRepository->find($id);

        if (empty($haspatalRegsiter)) {
            return $this->sendError('Haspatal Regsiter not found');
        }

        return $this->sendResponse($haspatalRegsiter->toArray(), 'Haspatal Regsiter retrieved successfully');
    }

    /**
     * Update the specified HaspatalRegsiter in storage.
     * PUT/PATCH /haspatalRegsiters/{id}
     *
     * @param int $id
     * @param UpdateHaspatalRegsiterAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateHaspatalRegsiterAPIRequest $request)
    {
        $input = $request->all();

        /** @var HaspatalRegsiter $haspatalRegsiter */
        $haspatalRegsiter = $this->haspatalRegsiterRepository->find($id);

        if (empty($haspatalRegsiter)) {
            return $this->sendError('Haspatal Regsiter not found');
        }

        $haspatalRegsiter = $this->haspatalRegsiterRepository->update($input, $id);

        return $this->sendResponse($haspatalRegsiter->toArray(), 'HaspatalRegsiter updated successfully');
    }

    /**
     * Remove the specified HaspatalRegsiter from storage.
     * DELETE /haspatalRegsiters/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var HaspatalRegsiter $haspatalRegsiter */
        $haspatalRegsiter = $this->haspatalRegsiterRepository->find($id);

        if (empty($haspatalRegsiter)) {
            return $this->sendError('Haspatal Regsiter not found');
        }

        $haspatalRegsiter->delete();

        return $this->sendSuccess('Haspatal Regsiter deleted successfully');
    }
}
