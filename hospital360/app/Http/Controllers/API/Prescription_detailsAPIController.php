<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePrescription_detailsAPIRequest;
use App\Http\Requests\API\UpdatePrescription_detailsAPIRequest;
use App\Models\Prescription_details;
use App\Repositories\Prescription_detailsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;
use Auth;
use DB;
use PDF;

/**
 * Class Prescription_detailsController
 * @package App\Http\Controllers\API
 */

class Prescription_detailsAPIController extends AppBaseController
{
    /** @var  Prescription_detailsRepository */
    private $prescriptionDetailsRepository;

    public function __construct(Prescription_detailsRepository $prescriptionDetailsRepo)
    {
        $this->prescriptionDetailsRepository = $prescriptionDetailsRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/prescriptionDetails",
     *      summary="Get a listing of the Prescription_details.",
     *      tags={"Prescription_details"},
     *      description="Get all Prescription_details",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Prescription_details")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $prescriptionDetails = $this->prescriptionDetailsRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($prescriptionDetails->toArray(), 'Prescription Details retrieved successfully');
    }
     public function prescription_details_id_data(Request $request)
    {
        //echo "string"; exit;
        $result = Prescription_details::where('prescription_details.patient_id', $request->patient_id)
                                        ->join('doctor_details', 'prescription_details.doctor_id', '=', 'doctor_details.id')
                                        ->join('booking_request', 'prescription_details.patient_id', '=', 'booking_request.patient_name')
                                        ->where('booking_request.book_id', $request->book_id)
                                        ->where('prescription_details.booking_id', $request->book_id)
                                        ->select('prescription_details.*','doctor_details.first_name','doctor_details.last_name','booking_request.date_time')
                                        ->get();
        
       if ($result) {
            return response()->json(["status" => true,"massage"=>"Doctors Availabilities retrieved successfully","data" => $result,"path"=>env('APP_URL').'public/media/prescription_image']);
        }else{
             return response()->json(["status" => false,"data" => $result]);
        }

    }
    

    /**
     * @param CreatePrescription_detailsAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/prescriptionDetails",
     *      summary="Store a newly created Prescription_details in storage",
     *      tags={"Prescription_details"},
     *      description="Store Prescription_details",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Prescription_details that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Prescription_details")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Prescription_details"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(Request $request)
    {

        $book =  DB::table('prescription_details')->where('booking_id', $request->booking_id)->orderBy('booking_id', 'desc')->first();

        if(!empty($book)){
            $input = $request->all();

            $data['title'] = "prescription";
            $data['book'] =  DB::table('prescription_details')->where('booking_id', $request->booking_id)->orderBy('booking_id', 'desc')->first();
            $data['patient'] = DB::table('patient_details')->where('id',$book->patient_id)->first();
            $data['doctor'] = DB::table('doctor_details')->where('id',$book->doctor_id)->first();
                
            $data['booking_details'] = DB::table('booking_request')->where('book_id',$request->booking_id)->first();

            $data['m_details'] =  DB::table('medicine_list')->where('id', $book->medicine_id)->first();
            
            //return view('pdf_view', compact('data'));
            if ($request->step == 8) {

                /*PDF::setOptions(['dpi' => 150,'debugLayout'=>false,'debugCss'=> true, 'defaultFont' => 'sans-serif']);*/

                

                $pdf = PDF::loadView('pdf_view', $data); 
                
                $path = public_path('pdf/');
                $fileName1 =   time() . '__'. 'Prescription.' . 'pdf' ;
                $pdf->save($path . '/' . $fileName1); 

                $input['pdf'] = 'public/pdf/'.$fileName1;
            }

            $id = $book->id;
            $prescriptionDetails = $this->prescriptionDetailsRepository->update($input, $id);
            $prescriptionDetails->couns= json_encode($request->couns);
            $prescriptionDetails->imaging= json_encode($request->imaging);
            $prescriptionDetails->medicine= json_encode($request->medicine);
            $prescriptionDetails->theraphy= json_encode($request->theraphy);
            $prescriptionDetails->home= json_encode($request->home);
            $prescriptionDetails->labtest= json_encode($request->labtest);
            $prescriptionDetails->save();

        }else
        {
            $input = $request->all();
             $patient = DB::table('patient_details')->where('id',$input['patient_id'])->select('first_name')->first();
            $doctor = DB::table('doctor_details')->where('id',$input['doctor_id'])->select('first_name')->first();
            
            
            //return view('pdf_view', compact('data'));
            /*$pdf = PDF::loadView('pdf_view', $data); 
            
            $path = public_path('pdf/');
            $fileName1 =   time() . '__'. 'Prescription.' . 'pdf' ;
            $pdf->save($path . '/' . $fileName1); 

            $input['pdf'] = 'public/pdf/'.$fileName1;*/
            $input['created_by'] = Auth::user()->id;
            $input['updated_by'] = Auth::user()->id;
            $prescriptionDetails = $this->prescriptionDetailsRepository->create($input);
            $prescriptionDetails->couns= json_encode($request->couns);
            $prescriptionDetails->imaging= json_encode($request->imaging);
            $prescriptionDetails->medicine= json_encode($request->medicine);
            $prescriptionDetails->theraphy= json_encode($request->theraphy);
            $prescriptionDetails->home= json_encode($request->home);
            $prescriptionDetails->labtest= json_encode($request->labtest);
            $prescriptionDetails->save();
        }

        if ($request->step == 8) {
            $user = \App\User::where('userDetailsId',$input['patient_id'])->where('user_type',2)->first();
            $user_dr = \App\User::where('userDetailsId',$input['doctor_id'])->where('user_type',1)->first();
            
            
            if ($user->mobile_token != null) {
                
                $this->send_notification($user->mobile_token,"Please download the Prescription","prescription",env('APP_URL').$input['pdf']);
            }
            //$this->send_notification($user_dr->mobile_token,$patient->first_name." Prescription create","prescription");
            
            $this->send_mail($user,"Please download the Prescription");
        }

        return $this->sendResponse($prescriptionDetails->toArray(), 'Prescription Details saved successfully');
    }

    public function send_mail($user,$mes)
    {
        $data['personalizations'] = array("0" => array('to' => array('0' => array('email' => $user->email))));

        $data['from'] = array('email' => 'system@haspatal.com');

        $data['subject'] = "Appointment Booking";
        $data['content'] = array("0" =>array('type' =>' text/html','value' => "<DOCTYPE html><html lang='en-US'>     <head><meta charset='utf-8'></head><body><h2>  ".$mes."</body></html>" ));

        //echo json_encode($data);exit();

        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://api.sendgrid.com/v3/mail/send",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => json_encode($data),
          CURLOPT_HTTPHEADER => array(
            "authorization: Bearer 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",
            "cache-control: no-cache",
            "content-type: application/json",
            "postman-token: b8c0781f-af48-90cc-1c5b-76f80e0b4be0"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          echo "cURL Error #:" . $err;
        } else {
          return $response;
        }
    }

    public function send_notification($token,$message,$title,$data = array())
    {
        $content = array(
           "en" => $message
        );

        $heading = array(
           "en" => $title
        );
       
        $fields = array(
           'app_id' => "33fe1b86-089a-4234-af9d-d7a923926620",
           'include_player_ids' => array($token),
           'data' => array("data" => $data),
           'contents' => $content,
           'headings' => $heading
        );
           
        $fields = json_encode($fields);
        
       
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $response = curl_exec($ch);
        curl_close($ch);
        //echo "<pre>";print_r($response);exit();
        return $response;
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/prescriptionDetails/{id}",
     *      summary="Display the specified Prescription_details",
     *      tags={"Prescription_details"},
     *      description="Get Prescription_details",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Prescription_details",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Prescription_details"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Prescription_details $prescriptionDetails */
        $prescriptionDetails = $this->prescriptionDetailsRepository->find($id);

        if (empty($prescriptionDetails)) {
            return $this->sendError('Prescription Details not found');
        }

        return $this->sendResponse($prescriptionDetails->toArray(), 'Prescription Details retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdatePrescription_detailsAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/prescriptionDetails/{id}",
     *      summary="Update the specified Prescription_details in storage",
     *      tags={"Prescription_details"},
     *      description="Update Prescription_details",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Prescription_details",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Prescription_details that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Prescription_details")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Prescription_details"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdatePrescription_detailsAPIRequest $request)
    {
        $input = $request->all();

        /** @var Prescription_details $prescriptionDetails */
        $prescriptionDetails = $this->prescriptionDetailsRepository->find($id);

        if (empty($prescriptionDetails)) {
            return $this->sendError('Prescription Details not found');
        }

        $prescriptionDetails = $this->prescriptionDetailsRepository->update($input, $id);

        return $this->sendResponse($prescriptionDetails->toArray(), 'Prescription_details updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/prescriptionDetails/{id}",
     *      summary="Remove the specified Prescription_details from storage",
     *      tags={"Prescription_details"},
     *      description="Delete Prescription_details",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Prescription_details",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Prescription_details $prescriptionDetails */
        $prescriptionDetails = $this->prescriptionDetailsRepository->find($id);

        if (empty($prescriptionDetails)) {
            return $this->sendError('Prescription Details not found');
        }

        $prescriptionDetails->delete();

        return $this->sendSuccess('Prescription Details deleted successfully');
    }



    /*$url = "https://fcm.googleapis.com/fcm/send";

        $serverKey = 'AAAA4yAJz-A:APA91bGQM-avVeV8gU7S6MemcBlvgHiwX8ClmDZdw_UBlSJQy2GYv3T_s5LPxy9QRX2J_YOHxvsUagDWqlZyGDkpe0iUtX01rivD4eaMVFlyA02GebeLLUHjWwC8fvHnaVaGkRFluaro';
        $title = "Prescription";
        $body = "Prescription Send";
        $notification = array('title' =>$title , 'text' => $body, 'sound' => 'default', 'badge' => '1');
        $arrayToSend = array('to' => $user->mobile_token, 'notification' => $notification,'priority'=>'high');
        $json = json_encode($arrayToSend);

        $headers = array();
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Authorization: key='. $serverKey;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST,"POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        curl_setopt($ch, CURLOPT_HTTPHEADER,$headers);
        //Send the request
        $res = curl_exec($ch);
        //Close request
        echo "<pre>";print_r($res);exit();
        curl_close($ch);
*/
public function Prescription_detailsList(Request $request)

{
    $Prescription_details = Prescription_details::where('prescription_details.patient_id', $request->input('patient_id'))
    ->join('doctor_details', 'prescription_details.doctor_id', '=', 'doctor_details.id')
    ->join('booking_request', 'prescription_details.booking_id', '=', 'booking_request.book_id')
    ->select('prescription_details.*','booking_request.appointment_time','booking_request.consult_time','doctor_details.first_name','doctor_details.last_name','prescription_details.id as prescription_details_id','booking_request.*')->orderBy('appointment_time', 'desc')
    ->orderBy('consult_time', 'asc')
    ->get();
    
    // $Prescription_details = Prescription_details::select('*')->where('patient_id',  $request->input('patient_id'))->get();




    if (empty($Prescription_details)) {

        return $this->sendError('Prescription_details not found');

    }



    return $this->sendResponse($Prescription_details->toArray(), 'Prescription_details retrieved successfully');

}
public function Prescription_SingleList(Request $request)

{

     
     

     $Prescription_details = Prescription_details::findOrFail($request->input('id'));
     if (empty($Prescription_details)) {
         return $this->sendError('Prescription Details not found');
     }

     $lab=json_decode($Prescription_details->labtest);
     $theraphy=json_decode($Prescription_details->theraphy);
     $home=json_decode($Prescription_details->home);
     $couns=json_decode($Prescription_details->couns);
     $imaging=json_decode($Prescription_details->imaging);
     $medicine=json_decode($Prescription_details->medicine);
     return response()->json(["status" => true,"data" => $Prescription_details, "lab" => $lab,"theraphy" => $theraphy,"home" => $home,"couns" => $couns,"imaging" => $imaging,"medicine" => $medicine]);

}
}
