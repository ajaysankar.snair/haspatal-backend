<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateBooking_price_adminAPIRequest;
use App\Http\Requests\API\UpdateBooking_price_adminAPIRequest;
use App\Models\Booking_price_admin;
use App\Repositories\Booking_price_adminRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class Booking_price_adminController
 * @package App\Http\Controllers\API
 */

class Booking_price_adminAPIController extends AppBaseController
{
    /** @var  Booking_price_adminRepository */
    private $bookingPriceAdminRepository;

    public function __construct(Booking_price_adminRepository $bookingPriceAdminRepo)
    {
        $this->bookingPriceAdminRepository = $bookingPriceAdminRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/bookingPriceAdmins",
     *      summary="Get a listing of the Booking_price_admins.",
     *      tags={"Booking_price_admin"},
     *      description="Get all Booking_price_admins",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Booking_price_admin")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $bookingPriceAdmins = $this->bookingPriceAdminRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($bookingPriceAdmins->toArray(), 'Booking Price Admins retrieved successfully');
    }

    /**
     * @param CreateBooking_price_adminAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/bookingPriceAdmins",
     *      summary="Store a newly created Booking_price_admin in storage",
     *      tags={"Booking_price_admin"},
     *      description="Store Booking_price_admin",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Booking_price_admin that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Booking_price_admin")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Booking_price_admin"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateBooking_price_adminAPIRequest $request)
    {
        $input = $request->all();

        $bookingPriceAdmin = $this->bookingPriceAdminRepository->create($input);

        return $this->sendResponse($bookingPriceAdmin->toArray(), 'Booking Price Admin saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/bookingPriceAdmins/{id}",
     *      summary="Display the specified Booking_price_admin",
     *      tags={"Booking_price_admin"},
     *      description="Get Booking_price_admin",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Booking_price_admin",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Booking_price_admin"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Booking_price_admin $bookingPriceAdmin */
        $bookingPriceAdmin = $this->bookingPriceAdminRepository->find($id);

        if (empty($bookingPriceAdmin)) {
            return $this->sendError('Booking Price Admin not found');
        }

        return $this->sendResponse($bookingPriceAdmin->toArray(), 'Booking Price Admin retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateBooking_price_adminAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/bookingPriceAdmins/{id}",
     *      summary="Update the specified Booking_price_admin in storage",
     *      tags={"Booking_price_admin"},
     *      description="Update Booking_price_admin",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Booking_price_admin",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Booking_price_admin that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Booking_price_admin")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Booking_price_admin"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateBooking_price_adminAPIRequest $request)
    {
        $input = $request->all();

        /** @var Booking_price_admin $bookingPriceAdmin */
        $bookingPriceAdmin = $this->bookingPriceAdminRepository->find($id);

        if (empty($bookingPriceAdmin)) {
            return $this->sendError('Booking Price Admin not found');
        }

        $bookingPriceAdmin = $this->bookingPriceAdminRepository->update($input, $id);

        return $this->sendResponse($bookingPriceAdmin->toArray(), 'Booking_price_admin updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/bookingPriceAdmins/{id}",
     *      summary="Remove the specified Booking_price_admin from storage",
     *      tags={"Booking_price_admin"},
     *      description="Delete Booking_price_admin",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Booking_price_admin",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Booking_price_admin $bookingPriceAdmin */
        $bookingPriceAdmin = $this->bookingPriceAdminRepository->find($id);

        if (empty($bookingPriceAdmin)) {
            return $this->sendError('Booking Price Admin not found');
        }

        $bookingPriceAdmin->delete();

        return $this->sendSuccess('Booking Price Admin deleted successfully');
    }
}
