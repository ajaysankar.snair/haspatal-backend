<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateBuy_plan_drAPIRequest;
use App\Http\Requests\API\UpdateBuy_plan_drAPIRequest;
use App\Models\Buy_plan_dr;
use App\Repositories\Buy_plan_drRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class Buy_plan_drController
 * @package App\Http\Controllers\API
 */

class Buy_plan_drAPIController extends AppBaseController
{
    /** @var  Buy_plan_drRepository */
    private $buyPlanDrRepository;

    public function __construct(Buy_plan_drRepository $buyPlanDrRepo)
    {
        $this->buyPlanDrRepository = $buyPlanDrRepo;
    }

    /**
     * Display a listing of the Buy_plan_dr.
     * GET|HEAD /buyPlanDrs
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|Response
     */
    public function index(Request $request)
    {
        $buyPlanDrs = $this->buyPlanDrRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($buyPlanDrs->toArray(), 'Buy Plan Drs retrieved successfully');
    }

    /**
     * Store a newly created Buy_plan_dr in storage.
     * POST /buyPlanDrs
     *
     * @param CreateBuy_plan_drAPIRequest $request
     *
     * @return \Illuminate\Http\JsonResponse|Response
     */
    public function store(CreateBuy_plan_drAPIRequest $request)
    {
        $input = $request->all();

        $buyPlanDr = $this->buyPlanDrRepository->create($input);

        return $this->sendResponse($buyPlanDr->toArray(), 'Buy Plan Dr saved successfully');
    }

    /**
     * Display the specified Buy_plan_dr.
     * GET|HEAD /buyPlanDrs/{id}
     *
     * @param int $id
     *
     * @return \Illuminate\Http\JsonResponse|Response
     */
    public function show($id)
    {
        /** @var Buy_plan_dr $buyPlanDr */
        $buyPlanDr = $this->buyPlanDrRepository->find($id);

        if (empty($buyPlanDr)) {
            return $this->sendError('Buy Plan Dr not found');
        }

        return $this->sendResponse($buyPlanDr->toArray(), 'Buy Plan Dr retrieved successfully');
    }

    /**
     * Update the specified Buy_plan_dr in storage.
     * PUT/PATCH /buyPlanDrs/{id}
     *
     * @param int $id
     * @param UpdateBuy_plan_drAPIRequest $request
     *
     * @return \Illuminate\Http\JsonResponse|Response
     */
    public function update($id, UpdateBuy_plan_drAPIRequest $request)
    {
        $input = $request->all();

        /** @var Buy_plan_dr $buyPlanDr */
        $buyPlanDr = $this->buyPlanDrRepository->find($id);

        if (empty($buyPlanDr)) {
            return $this->sendError('Buy Plan Dr not found');
        }

        $buyPlanDr = $this->buyPlanDrRepository->update($input, $id);

        return $this->sendResponse($buyPlanDr->toArray(), 'Buy_plan_dr updated successfully');
    }

    /**
     * Remove the specified Buy_plan_dr from storage.
     * DELETE /buyPlanDrs/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return \Illuminate\Http\JsonResponse|Response
     */
    public function destroy($id)
    {
        /** @var Buy_plan_dr $buyPlanDr */
        $buyPlanDr = $this->buyPlanDrRepository->find($id);

        if (empty($buyPlanDr)) {
            return $this->sendError('Buy Plan Dr not found');
        }

        $buyPlanDr->delete();

        return $this->sendSuccess('Buy Plan Dr deleted successfully');
    }
}
