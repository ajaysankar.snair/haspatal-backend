<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateSet_service_priceAPIRequest;
use App\Http\Requests\API\UpdateSet_service_priceAPIRequest;
use App\Models\Set_service_price;
use App\Repositories\Set_service_priceRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class Set_service_priceController
 * @package App\Http\Controllers\API
 */

class Set_service_priceAPIController extends AppBaseController
{
    /** @var  Set_service_priceRepository */
    private $setServicePriceRepository;

    public function __construct(Set_service_priceRepository $setServicePriceRepo)
    {
        $this->setServicePriceRepository = $setServicePriceRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/setServicePrices",
     *      summary="Get a listing of the Set_service_prices.",
     *      tags={"Set_service_price"},
     *      description="Get all Set_service_prices",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Set_service_price")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $setServicePrices = $this->setServicePriceRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($setServicePrices->toArray(), 'Set Service Prices retrieved successfully');
    }

    /**
     * @param CreateSet_service_priceAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/setServicePrices",
     *      summary="Store a newly created Set_service_price in storage",
     *      tags={"Set_service_price"},
     *      description="Store Set_service_price",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Set_service_price that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Set_service_price")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Set_service_price"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateSet_service_priceAPIRequest $request)
    {
        $input = $request->all();

        $setServicePrice = $this->setServicePriceRepository->create($input);

        return $this->sendResponse($setServicePrice->toArray(), 'Set Service Price saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/setServicePrices/{id}",
     *      summary="Display the specified Set_service_price",
     *      tags={"Set_service_price"},
     *      description="Get Set_service_price",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Set_service_price",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Set_service_price"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Set_service_price $setServicePrice */
        $setServicePrice = $this->setServicePriceRepository->find($id);

        if (empty($setServicePrice)) {
            return $this->sendError('Set Service Price not found');
        }

        return $this->sendResponse($setServicePrice->toArray(), 'Set Service Price retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateSet_service_priceAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/setServicePrices/{id}",
     *      summary="Update the specified Set_service_price in storage",
     *      tags={"Set_service_price"},
     *      description="Update Set_service_price",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Set_service_price",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Set_service_price that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Set_service_price")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Set_service_price"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateSet_service_priceAPIRequest $request)
    {
        $input = $request->all();

        /** @var Set_service_price $setServicePrice */
        $setServicePrice = $this->setServicePriceRepository->find($id);

        if (empty($setServicePrice)) {
            return $this->sendError('Set Service Price not found');
        }

        $setServicePrice = $this->setServicePriceRepository->update($input, $id);

        return $this->sendResponse($setServicePrice->toArray(), 'Set_service_price updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/setServicePrices/{id}",
     *      summary="Remove the specified Set_service_price from storage",
     *      tags={"Set_service_price"},
     *      description="Delete Set_service_price",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Set_service_price",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Set_service_price $setServicePrice */
        $setServicePrice = $this->setServicePriceRepository->find($id);

        if (empty($setServicePrice)) {
            return $this->sendError('Set Service Price not found');
        }

        $setServicePrice->delete();

        return $this->sendSuccess('Set Service Price deleted successfully');
    }
}
