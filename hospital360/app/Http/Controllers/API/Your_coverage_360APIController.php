<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateYour_coverage_360APIRequest;
use App\Http\Requests\API\UpdateYour_coverage_360APIRequest;
use App\Models\Your_coverage_360;
use App\Repositories\Your_coverage_360Repository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;
use Auth;
use DB;


/**
 * Class Your_coverage_360Controller
 * @package App\Http\Controllers\API
 */

class Your_coverage_360APIController extends AppBaseController
{
    /** @var  Your_coverage_360Repository */
    private $yourCoverage360Repository;

    public function __construct(Your_coverage_360Repository $yourCoverage360Repo)
    {
        $this->yourCoverage360Repository = $yourCoverage360Repo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/yourCoverage360s",
     *      summary="Get a listing of the Your_coverage_360s.",
     *      tags={"Your_coverage_360"},
     *      description="Get all Your_coverage_360s",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Your_coverage_360")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $yourCoverage360s = $this->yourCoverage360Repository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($yourCoverage360s->toArray(), 'Your Coverage 360S retrieved successfully');
    }

    /**
     * @param CreateYour_coverage_360APIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/yourCoverage360s",
     *      summary="Store a newly created Your_coverage_360 in storage",
     *      tags={"Your_coverage_360"},
     *      description="Store Your_coverage_360",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Your_coverage_360 that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Your_coverage_360")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Your_coverage_360"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(Request $request)
    {
        $book =  DB::table('Your_coverage_360')->where('user_id', $request->created_by)->orderBy('created_by', 'desc')->first();
        /*if(!empty($book)){
        $input = $request->all();
        $id = $book->id;
         $yourCoverage360 = $this->yourCoverage360Repository->update($input, $id);
        }else
        {
        }*/

        $input = $request->all();
        $input['user_id'] = $request->created_by;
        $input['created_by'] = Auth::user()->id;
        $input['updated_by'] = Auth::user()->id;

        foreach ($request->c_pincode as $value) {
            $input['c_pincode'] = $value;
            $yourCoverage360 = $this->yourCoverage360Repository->create($input);
        }


        $affected = DB::table('haspatal_360_register')->where('id', Auth::user()->userDetailsId)->update(['your_coverage_status' => 1]);

        return $this->sendResponse($yourCoverage360->toArray(), 'Your Coverage 360 saved successfully');
    }

    public function coverage_view(Request $request)
      {

         $coverage = Your_coverage_360::where('user_id',$request->user_id)->get();
       
        
          if ($coverage) {
            return response()->json(["status" => true,"massage"=>"coverage retrieved successfully","data" => $coverage]);
        }else{
            return response()->json(["status" => false,"data" => $coverage]);
        }

      }


    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/yourCoverage360s/{id}",
     *      summary="Display the specified Your_coverage_360",
     *      tags={"Your_coverage_360"},
     *      description="Get Your_coverage_360",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Your_coverage_360",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Your_coverage_360"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Your_coverage_360 $yourCoverage360 */
        $yourCoverage360 = $this->yourCoverage360Repository->find($id);

        if (empty($yourCoverage360)) {
            return $this->sendError('Your Coverage 360 not found');
        }

        return $this->sendResponse($yourCoverage360->toArray(), 'Your Coverage 360 retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateYour_coverage_360APIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/yourCoverage360s/{id}",
     *      summary="Update the specified Your_coverage_360 in storage",
     *      tags={"Your_coverage_360"},
     *      description="Update Your_coverage_360",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Your_coverage_360",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Your_coverage_360 that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Your_coverage_360")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Your_coverage_360"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateYour_coverage_360APIRequest $request)
    {
        $input = $request->all();

        /** @var Your_coverage_360 $yourCoverage360 */
        $yourCoverage360 = $this->yourCoverage360Repository->find($id);

        if (empty($yourCoverage360)) {
            return $this->sendError('Your Coverage 360 not found');
        }

        $yourCoverage360 = $this->yourCoverage360Repository->update($input, $id);

        return $this->sendResponse($yourCoverage360->toArray(), 'Your_coverage_360 updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/yourCoverage360s/{id}",
     *      summary="Remove the specified Your_coverage_360 from storage",
     *      tags={"Your_coverage_360"},
     *      description="Delete Your_coverage_360",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Your_coverage_360",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Your_coverage_360 $yourCoverage360 */
        $yourCoverage360 = $this->yourCoverage360Repository->find($id);

        if (empty($yourCoverage360)) {
            return $this->sendError('Your Coverage 360 not found');
        }

        $yourCoverage360->delete();

        return $this->sendSuccess('Your Coverage 360 deleted successfully');
    }
}
