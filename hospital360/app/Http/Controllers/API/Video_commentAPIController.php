<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateVideo_commentAPIRequest;
use App\Http\Requests\API\UpdateVideo_commentAPIRequest;
use App\Models\Video_comment;
use App\Repositories\Video_commentRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class Video_commentController
 * @package App\Http\Controllers\API
 */

class Video_commentAPIController extends AppBaseController
{
    /** @var  Video_commentRepository */
    private $videoCommentRepository;

    public function __construct(Video_commentRepository $videoCommentRepo)
    {
        $this->videoCommentRepository = $videoCommentRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/videoComments",
     *      summary="Get a listing of the Video_comments.",
     *      tags={"Video_comment"},
     *      description="Get all Video_comments",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Video_comment")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $videoComments = $this->videoCommentRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($videoComments->toArray(), 'Video Comments retrieved successfully');
    }

    /**
     * @param CreateVideo_commentAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/videoComments",
     *      summary="Store a newly created Video_comment in storage",
     *      tags={"Video_comment"},
     *      description="Store Video_comment",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Video_comment that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Video_comment")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Video_comment"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateVideo_commentAPIRequest $request)
    {
        $input = $request->all();

        $videoComment = $this->videoCommentRepository->create($input);

        return $this->sendResponse($videoComment->toArray(), 'Video Comment saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/videoComments/{id}",
     *      summary="Display the specified Video_comment",
     *      tags={"Video_comment"},
     *      description="Get Video_comment",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Video_comment",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Video_comment"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Video_comment $videoComment */
        $videoComment = $this->videoCommentRepository->find($id);

        if (empty($videoComment)) {
            return $this->sendError('Video Comment not found');
        }

        return $this->sendResponse($videoComment->toArray(), 'Video Comment retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateVideo_commentAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/videoComments/{id}",
     *      summary="Update the specified Video_comment in storage",
     *      tags={"Video_comment"},
     *      description="Update Video_comment",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Video_comment",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Video_comment that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Video_comment")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Video_comment"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateVideo_commentAPIRequest $request)
    {
        $input = $request->all();

        /** @var Video_comment $videoComment */
        $videoComment = $this->videoCommentRepository->find($id);

        if (empty($videoComment)) {
            return $this->sendError('Video Comment not found');
        }

        $videoComment = $this->videoCommentRepository->update($input, $id);

        return $this->sendResponse($videoComment->toArray(), 'Video_comment updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/videoComments/{id}",
     *      summary="Remove the specified Video_comment from storage",
     *      tags={"Video_comment"},
     *      description="Delete Video_comment",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Video_comment",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Video_comment $videoComment */
        $videoComment = $this->videoCommentRepository->find($id);

        if (empty($videoComment)) {
            return $this->sendError('Video Comment not found');
        }

        $videoComment->delete();

        return $this->sendSuccess('Video Comment deleted successfully');
    }
}
