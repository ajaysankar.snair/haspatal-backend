<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePlan_masterAPIRequest;
use App\Http\Requests\API\UpdatePlan_masterAPIRequest;
use App\Models\Plan_master;
use App\Repositories\Plan_masterRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class Plan_masterController
 * @package App\Http\Controllers\API
 */

class Plan_masterAPIController extends AppBaseController
{
    /** @var  Plan_masterRepository */
    private $planMasterRepository;

    public function __construct(Plan_masterRepository $planMasterRepo)
    {
        $this->planMasterRepository = $planMasterRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/planMasters",
     *      summary="Get a listing of the Plan_masters.",
     *      tags={"Plan_master"},
     *      description="Get all Plan_masters",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Plan_master")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $planMasters = $this->planMasterRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($planMasters->toArray(), 'Plan Masters retrieved successfully');
    }

    /**
     * @param CreatePlan_masterAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/planMasters",
     *      summary="Store a newly created Plan_master in storage",
     *      tags={"Plan_master"},
     *      description="Store Plan_master",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Plan_master that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Plan_master")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Plan_master"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreatePlan_masterAPIRequest $request)
    {
        $input = $request->all();

        $planMaster = $this->planMasterRepository->create($input);

        return $this->sendResponse($planMaster->toArray(), 'Plan Master saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/planMasters/{id}",
     *      summary="Display the specified Plan_master",
     *      tags={"Plan_master"},
     *      description="Get Plan_master",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Plan_master",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Plan_master"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Plan_master $planMaster */
        $planMaster = $this->planMasterRepository->find($id);

        if (empty($planMaster)) {
            return $this->sendError('Plan Master not found');
        }

        return $this->sendResponse($planMaster->toArray(), 'Plan Master retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdatePlan_masterAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/planMasters/{id}",
     *      summary="Update the specified Plan_master in storage",
     *      tags={"Plan_master"},
     *      description="Update Plan_master",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Plan_master",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Plan_master that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Plan_master")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Plan_master"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdatePlan_masterAPIRequest $request)
    {
        $input = $request->all();

        /** @var Plan_master $planMaster */
        $planMaster = $this->planMasterRepository->find($id);

        if (empty($planMaster)) {
            return $this->sendError('Plan Master not found');
        }

        $planMaster = $this->planMasterRepository->update($input, $id);

        return $this->sendResponse($planMaster->toArray(), 'Plan_master updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/planMasters/{id}",
     *      summary="Remove the specified Plan_master from storage",
     *      tags={"Plan_master"},
     *      description="Delete Plan_master",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Plan_master",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Plan_master $planMaster */
        $planMaster = $this->planMasterRepository->find($id);

        if (empty($planMaster)) {
            return $this->sendError('Plan Master not found');
        }

        $planMaster->delete();

        return $this->sendSuccess('Plan Master deleted successfully');
    }
}
