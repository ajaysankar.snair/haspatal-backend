<?php



namespace App\Http\Controllers\API;



use App\Http\Requests\API\CreateTherapies_listAPIRequest;

use App\Http\Requests\API\UpdateTherapies_listAPIRequest;

use App\Models\Therapies_list;

use App\Repositories\Therapies_listRepository;

use Illuminate\Http\Request;

use App\Http\Controllers\AppBaseController;

use Response;

use Auth;

/**

 * Class Therapies_listController

 * @package App\Http\Controllers\API

 */



class Therapies_listAPIController extends AppBaseController

{

    /** @var  Therapies_listRepository */

    private $therapiesListRepository;



    public function __construct(Therapies_listRepository $therapiesListRepo)

    {

        $this->therapiesListRepository = $therapiesListRepo;

    }



    /**

     * Display a listing of the Therapies_list.

     * GET|HEAD /therapiesLists

     *

     * @param Request $request

     * @return \Illuminate\Http\JsonResponse|Response

     */

    public function index(Request $request)

    {

        $therapiesLists = $this->therapiesListRepository->all(

            $request->except(['skip', 'limit']),

            $request->get('skip'),

            $request->get('limit')

        );



        return $this->sendResponse($therapiesLists->toArray(), 'Therapies Lists retrieved successfully');

    }



    /**

     * Store a newly created Therapies_list in storage.

     * POST /therapiesLists

     *

     * @param CreateTherapies_listAPIRequest $request

     *

     * @return \Illuminate\Http\JsonResponse|Response

     */

    public function store(CreateTherapies_listAPIRequest $request)

    {

        $input = $request->all();
        $input['created_by']= Auth::user()->id;
        $input['updated_by']= Auth::user()->id;


        $therapiesList = $this->therapiesListRepository->create($input);

       
        $therapiesList->dr_id =     $request->dr_id;
       
        $therapiesList->save();

        return $this->sendResponse($therapiesList->toArray(), 'Therapies List saved successfully');

    }



    /**

     * Display the specified Therapies_list.

     * GET|HEAD /therapiesLists/{id}

     *

     * @param int $id

     *

     * @return \Illuminate\Http\JsonResponse|Response

     */

    public function show($id)

    {

        /** @var Therapies_list $therapiesList */

        // $therapiesList = $this->therapiesListRepository->find($id);

        $therapiesList = Therapies_list::select('*')->where('dr_id', $id)->get();

        if (empty($therapiesList)) {

            return $this->sendError('Therapies List not found');

        }



        return $this->sendResponse($therapiesList->toArray(), 'Therapies List retrieved successfully');

    }



    /**

     * Update the specified Therapies_list in storage.

     * PUT/PATCH /therapiesLists/{id}

     *

     * @param int $id

     * @param UpdateTherapies_listAPIRequest $request

     *

     * @return \Illuminate\Http\JsonResponse|Response

     */

    public function update($id, UpdateTherapies_listAPIRequest $request)

    {

        $input = $request->all();



        /** @var Therapies_list $therapiesList */

        $therapiesList = $this->therapiesListRepository->find($id);



        if (empty($therapiesList)) {

            return $this->sendError('Therapies List not found');

        }



        $therapiesList = $this->therapiesListRepository->update($input, $id);



        return $this->sendResponse($therapiesList->toArray(), 'Therapies_list updated successfully');

    }



    /**

     * Remove the specified Therapies_list from storage.

     * DELETE /therapiesLists/{id}

     *

     * @param int $id

     *

     * @throws \Exception

     *

     * @return \Illuminate\Http\JsonResponse|Response

     */

    public function destroy($id)

    {

        /** @var Therapies_list $therapiesList */

        $therapiesList = $this->therapiesListRepository->find($id);



        if (empty($therapiesList)) {

            return $this->sendError('Therapies List not found');

        }



        $therapiesList->delete();



        return $this->sendSuccess('Therapies List deleted successfully');

    }

}

