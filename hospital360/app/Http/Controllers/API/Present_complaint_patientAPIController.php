<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePresent_complaint_patientAPIRequest;
use App\Http\Requests\API\UpdatePresent_complaint_patientAPIRequest;
use App\Models\Present_complaint_patient;
use App\Repositories\Present_complaint_patientRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;
use Auth;
use DB;

/**
 * Class Present_complaint_patientController
 * @package App\Http\Controllers\API
 */

class Present_complaint_patientAPIController extends AppBaseController
{
    /** @var  Present_complaint_patientRepository */
    private $presentComplaintPatientRepository;

    public function __construct(Present_complaint_patientRepository $presentComplaintPatientRepo)
    {
        $this->presentComplaintPatientRepository = $presentComplaintPatientRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/presentComplaintPatients",
     *      summary="Get a listing of the Present_complaint_patients.",
     *      tags={"Present_complaint_patient"},
     *      description="Get all Present_complaint_patients",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Present_complaint_patient")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $presentComplaintPatients = $this->presentComplaintPatientRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($presentComplaintPatients->toArray(), 'Present Complaint Patients retrieved successfully');
    }
    
     public function present_complaint_list(Request $request)
    {
        
         $myFavourite =  DB::table('present_complaint_patient')
                        ->join('patient_details', 'present_complaint_patient.pc_patient_id', '=', 'patient_details.id')
                        ->join('doctor_details', 'present_complaint_patient.pc_doctor_id', '=', 'doctor_details.id')
                        ->where('present_complaint_patient.book_id',$request->book_id)
                        ->select('present_complaint_patient.id as my_id','present_complaint_patient.*','doctor_details.first_name','doctor_details.last_name','patient_details.first_name as patient')
                        ->get();

         $path = array("complaint_pic"=>env('APP_URL').'/public/media/complaint_pic',
                 "complaint_pdf"=>env('APP_URL').'/public/media/complaint_pdf');
      if ($myFavourite) {
            return response()->json(["status" => true,"data" => $myFavourite, "message" => "Present Complaint Patients retrieved successfully","path" => $path]);
        }else{
            return response()->json(["status" => false,"data" => '', "message" => "Present Complaint Patients retrieved successfully"]);
        }
        /*return $this->sendResponse($myFavourite->toArray(), 'My Favourites retrieved successfully');*/
    }

    /**
     * @param CreatePresent_complaint_patientAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/presentComplaintPatients",
     *      summary="Store a newly created Present_complaint_patient in storage",
     *      tags={"Present_complaint_patient"},
     *      description="Store Present_complaint_patient",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Present_complaint_patient that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Present_complaint_patient")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Present_complaint_patient"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(Request $request)
    {
        //echo "string"; exit;
        $input = $request->all();
         /*if($request->hasfile('complaint_pic'))
        {
            $image = $request->file('complaint_pic');
            $extension = $image->getClientOriginalExtension(); // getting image extension
            $filename =time().'__'.$image->getClientOriginalName();
            $path = public_path('/media/complaint_pic/');
            $image->move($path, $filename);
            $input['complaint_pic'] = $filename;
        }*/
        $complaint_pic=array();
        if($files=$request->file('complaint_pic')){
           foreach($files as $file){
               $name=$file->getClientOriginalName();
               $path = public_path('/media/complaint_pic/');
               $file->move($path,$name);
               $complaint_pic[]=$name;
           }
            $input['complaint_pic'] = implode("$",$complaint_pic);

       }
         if($request->hasfile('complaint_pdf'))
        {
            $image = $request->file('complaint_pdf');
            $extension = $image->getClientOriginalExtension(); // getting image extension
            $filename =time().'__'.$image->getClientOriginalName();
            $path = public_path('/media/complaint_pdf/');
            $image->move($path, $filename);
            $input['complaint_pdf'] = $filename;
        }
         
        $input['created_by'] = Auth::user()->id;
        $input['updated_by'] = Auth::user()->id;
        $presentComplaintPatient = $this->presentComplaintPatientRepository->create($input);

        return $this->sendResponse($presentComplaintPatient->toArray(), 'Present Complaint Patient saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/presentComplaintPatients/{id}",
     *      summary="Display the specified Present_complaint_patient",
     *      tags={"Present_complaint_patient"},
     *      description="Get Present_complaint_patient",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Present_complaint_patient",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Present_complaint_patient"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Present_complaint_patient $presentComplaintPatient */
        $presentComplaintPatient = $this->presentComplaintPatientRepository->find($id);

        if (empty($presentComplaintPatient)) {
            return $this->sendError('Present Complaint Patient not found');
        }

        return $this->sendResponse($presentComplaintPatient->toArray(), 'Present Complaint Patient retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdatePresent_complaint_patientAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/presentComplaintPatients/{id}",
     *      summary="Update the specified Present_complaint_patient in storage",
     *      tags={"Present_complaint_patient"},
     *      description="Update Present_complaint_patient",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Present_complaint_patient",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Present_complaint_patient that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Present_complaint_patient")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Present_complaint_patient"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, Request $request)
    {
        
        $presentComplaintPatient = $this->presentComplaintPatientRepository->find($id);
        $input = $request->all();
        /* if($request->hasfile('complaint_pic'))
        {
            $image = $request->file('complaint_pic');
            $extension = $image->getClientOriginalExtension(); // getting image extension
            $filename =time().'__'.$image->getClientOriginalName();
            $path = public_path('/media/complaint_pic/');
            $image->move($path, $filename);
            $input['complaint_pic'] = $filename;
        }*/
         $complaint_pic=array();
        if($files=$request->file('complaint_pic')){
           foreach($files as $file){
               $name=$file->getClientOriginalName();
               $path = public_path('/media/complaint_pic/');
               $file->move($path,$name);
               $complaint_pic[]=$name;
           }
            $input['complaint_pic'] = implode("$",$complaint_pic);

       }
         if($request->hasfile('complaint_pdf'))
        {
            $image = $request->file('complaint_pdf');
            $extension = $image->getClientOriginalExtension(); // getting image extension
            $filename =time().'__'.$image->getClientOriginalName();
            $path = public_path('/media/complaint_pdf/');
            $image->move($path, $filename);
            $input['complaint_pdf'] = $filename;
        }

        /** @var Present_complaint_patient $presentComplaintPatient */
        $presentComplaintPatient = $this->presentComplaintPatientRepository->find($id);

        if (empty($presentComplaintPatient)) {
            return $this->sendError('Present Complaint Patient not found');
        }

        $presentComplaintPatient = $this->presentComplaintPatientRepository->update($input, $id);

        return $this->sendResponse($presentComplaintPatient->toArray(), 'Present_complaint_patient updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/presentComplaintPatients/{id}",
     *      summary="Remove the specified Present_complaint_patient from storage",
     *      tags={"Present_complaint_patient"},
     *      description="Delete Present_complaint_patient",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Present_complaint_patient",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Present_complaint_patient $presentComplaintPatient */
        $presentComplaintPatient = $this->presentComplaintPatientRepository->find($id);

        if (empty($presentComplaintPatient)) {
            return $this->sendError('Present Complaint Patient not found');
        }

        $presentComplaintPatient->delete();

        return $this->sendSuccess('Present Complaint Patient deleted successfully');
    }
}
