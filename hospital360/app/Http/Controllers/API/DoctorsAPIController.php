<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateDoctorsAPIRequest;
use App\Http\Requests\API\UpdateDoctorsAPIRequest;
use App\Models\Doctors;
use App\Models\BookingRequest;
use App\Models\Buy_plan_dr;
use App\Models\Doctor_withdraw;
use App\Models\Review;
use App\Models\My_note_patient;
use App\Models\Patients;
use App\Models\Prescription_details;
use App\Repositories\DoctorsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;
use Validator;
use DB;
use Auth;
use App\User;
use Carbon\Carbon;
use App\Mail\WelcomeMail;
use Illuminate\Support\Facades\Mail;

use Swift_Mailer;
use Swift_Message;
use Swift_Attachment;
use Swift_SmtpTransport;
use Swift_SendmailTransport;

/**
 * Class DoctorsController
 * @package App\Http\Controllers\API
 */

class DoctorsAPIController extends AppBaseController
{
    /** @var  DoctorsRepository */
    private $doctorsRepository;

    public function __construct(DoctorsRepository $doctorsRepo)
    {
        $this->doctorsRepository = $doctorsRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/doctors",
     *      summary="Get a listing of the Doctors.",
     *      tags={"Doctors"},
     *      description="Get all Doctors",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Doctors")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {

        $doctors = $this->doctorsRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($doctors->toArray(), 'Doctors retrieved successfully');
    }

    public function doctors_list(Request $request)
    {
            $doctors = Doctors::where('status',0);
                        
            if ($request->language != 0) {
                $doctors->where('language', 'LIKE', '%'.$request->language.'%');
            }

            if ($request->speciality != 0) {
                $doctors->where('speciality', 'LIKE', '%'.$request->speciality.'%');
            }

            if ($request->state != 0) {
                $doctors->where('state',$request->state);
            }

            if ($request->city != 0) {
                $doctors->where('city',$request->city);
            }

            $results = $doctors->get();
            //echo "<pre>";print_r($results);exit;

            $admin_price = DB::table('booking_price_admin')->where('id',7)->select('booking_price_admin.book_price')->first();
            
            
            $data = array();
            foreach ($results as $value) {

                $speciality_name = array();
                $language_name = array();
                /*echo "<pre>";
                print_r($doctors->toArray()); exit;*/
                $d = explode(',',$value->speciality);
                $d1 = explode(',',$value->language);

                $speciality_name = DB::table('specilities')->whereIn('id',$d)->get();
                $language_name = DB::table('language')->whereIn('id',$d1)->get();

                $data[] = array("id"=> $value->id,"d_uni_id"=> $value->d_uni_id,"first_name"=> $value->first_name,"last_name"=> $value->last_name,"email"=> $value->email,"bio"=> $value->bio,"pincode"=> $value->pincode,"state"=> $value->state,"city"=> $value->city,"address"=>$value->address,"main_mobile"=> $value->main_mobile,"second_mobile"=> $value->second_mobile,"clinic_team_leader"=> $value->clinic_team_leader,"clinic_admin_mobile_no"=> $value->clinic_admin_mobile_no,"clinic_email"=> $value->clinic_email,"experience"=> $value->experience,"language"=> $value->language,"speciality"=> $value->speciality,"price"=> $value->price,"licence_type"=> $value->licence_type,"licence_no"=> $value->licence_no,"valide_upto"=> $value->valide_upto,"issued_by"=> $value->issued_by,"licence_copy"=> $value->licence_copy,"profile_pic"=> $value->profile_pic,"clinic_logo"=> $value->clinic_logo,"bank_name"=> $value->bank_name,"account_name"=> $value->account_name,"ifsc_code"=> $value->ifsc_code,"account_no"=> $value->account_no,"cheque_img"=> $value->cheque_img,"watsapp_number"=> $value->watsapp_number,"youtube_link"=> $value->youtube_link,"teligram_number"=> $value->teligram_number,"status"=> $value->status,"step"=>  $value->step,"created_by"=> $value->created_by,"updated_by"=> $value->updated_by,"created_at"=> $value->created_at,"updated_at"=> $value->updated_at,"year_of_graduation"=> $value->year_of_graduation,"appointment_type"=> $value->appointment_type,"clinic_wallpapper"=> $value->clinic_wallpapper,"clinic_name"=> $value->clinic_name,"clinic_buisness_name"=> $value->clinic_buisness_name,"pan_number"=> $value->pan_number,"speciality_name"=>$speciality_name,"language_name"=>$language_name);
            }                        
            $path = array("licence_copy"=>env('APP_URL').'media/licence_copy',
            "profile_pic"=>env('APP_URL').'media/profile_pic',
            "clinic_logo"=>env('APP_URL').'media/clinic_logo'
        );
            // $path = array("licence_copy"=>env('APP_URL').'public/media/licence_copy',
            //                 "profile_pic"=>env('APP_URL').'public/media/profile_pic',
            //                 "clinic_logo"=>env('APP_URL').'public/media/clinic_logo'
            //             );

        //return $this->sendCustome($result->toArray(), 'Doctors retrieved successfully',$path);
        if ($results) {
            return response()->json(["status" => true,"massage"=>"Doctors retrieved successfully","data" => $data,"path" => $path,'admin_price'=>$admin_price]);
        }else{
            return response()->json(["status" => false,"data" => $data]);
        }

    }
    public function doctors_search(Request $request)
    {
                      
        $results = Doctors::where('d_uni_id', $request->d_uni_id)->where('status',0)->get();

        $admin_price = DB::table('booking_price_admin')->where('id',7)->select('booking_price_admin.book_price')->first();
      
        $data = array();
        foreach ($results as $value) {

            $speciality_name = array();
            $language_name = array();
            /*echo "<pre>";
            print_r($doctors->toArray()); exit;*/
            $d = explode(',',$value->speciality);
            $d1 = explode(',',$value->language);

            $speciality_name = DB::table('specilities')->whereIn('id',$d)->get();
            $language_name = DB::table('language')->whereIn('id',$d1)->get();

            $data[] = array("id"=> $value->id,"d_uni_id"=> $value->d_uni_id,"first_name"=> $value->first_name,"last_name"=> $value->last_name,"email"=> $value->email,"bio"=> $value->bio,"pincode"=> $value->pincode,"state"=> $value->state,"city"=> $value->city,"address"=>$value->address,"main_mobile"=> $value->main_mobile,"second_mobile"=> $value->second_mobile,"clinic_team_leader"=> $value->clinic_team_leader,"clinic_admin_mobile_no"=> $value->clinic_admin_mobile_no,"clinic_email"=> $value->clinic_email,"experience"=> $value->experience,"language"=> $value->language,"speciality"=> $value->speciality,"price"=> $value->price,"licence_type"=> $value->licence_type,"licence_no"=> $value->licence_no,"valide_upto"=> $value->valide_upto,"issued_by"=> $value->issued_by,"licence_copy"=> $value->licence_copy,"profile_pic"=> $value->profile_pic,"clinic_logo"=> $value->clinic_logo,"bank_name"=> $value->bank_name,"account_name"=> $value->account_name,"ifsc_code"=> $value->ifsc_code,"account_no"=> $value->account_no,"cheque_img"=> $value->cheque_img,"watsapp_number"=> $value->watsapp_number,"youtube_link"=> $value->youtube_link,"teligram_number"=> $value->teligram_number,"status"=> $value->status,"step"=>  $value->step,"created_by"=> $value->created_by,"updated_by"=> $value->updated_by,"created_at"=> $value->created_at,"updated_at"=> $value->updated_at,"year_of_graduation"=> $value->year_of_graduation,"appointment_type"=> $value->appointment_type,"clinic_wallpapper"=> $value->clinic_wallpapper,"clinic_name"=> $value->clinic_name,"clinic_buisness_name"=> $value->clinic_buisness_name,"pan_number"=> $value->pan_number,"speciality_name"=>$speciality_name,"language_name"=>$language_name);
        }  

        $path = array("licence_copy"=>env('APP_URL').'media/licence_copy',
                            "profile_pic"=>env('APP_URL').'media/profile_pic',
                            "clinic_logo"=>env('APP_URL').'media/clinic_logo'
                        );
  
     if ($results) {
            return response()->json(["status" => true,"massage"=>"Doctors retrieved successfully","data" => $data,"path" => $path,'admin_price'=>$admin_price]);
        }else{
            return response()->json(["status" => false,"data" => $results]);
        }

    }
    public function doctors_search_firstname(Request $request)
    {
                      
        $results = Doctors::where('first_name','like', '%' .$request->first_name . '%')->where('status',0)->get();

        $admin_price = DB::table('booking_price_admin')->where('id',7)->select('booking_price_admin.book_price')->first();
      
        $data = array();
        foreach ($results as $value) {

            $speciality_name = array();
            $language_name = array();
            /*echo "<pre>";
            print_r($doctors->toArray()); exit;*/
            $d = explode(',',$value->speciality);
            $d1 = explode(',',$value->language);

            $speciality_name = DB::table('specilities')->whereIn('id',$d)->get();
            $language_name = DB::table('language')->whereIn('id',$d1)->get();

            $data[] = array("id"=> $value->id,"d_uni_id"=> $value->d_uni_id,"first_name"=> $value->first_name,"last_name"=> $value->last_name,"email"=> $value->email,"bio"=> $value->bio,"pincode"=> $value->pincode,"state"=> $value->state,"city"=> $value->city,"address"=>$value->address,"main_mobile"=> $value->main_mobile,"second_mobile"=> $value->second_mobile,"clinic_team_leader"=> $value->clinic_team_leader,"clinic_admin_mobile_no"=> $value->clinic_admin_mobile_no,"clinic_email"=> $value->clinic_email,"experience"=> $value->experience,"language"=> $value->language,"speciality"=> $value->speciality,"price"=> $value->price,"licence_type"=> $value->licence_type,"licence_no"=> $value->licence_no,"valide_upto"=> $value->valide_upto,"issued_by"=> $value->issued_by,"licence_copy"=> $value->licence_copy,"profile_pic"=> $value->profile_pic,"clinic_logo"=> $value->clinic_logo,"bank_name"=> $value->bank_name,"account_name"=> $value->account_name,"ifsc_code"=> $value->ifsc_code,"account_no"=> $value->account_no,"cheque_img"=> $value->cheque_img,"watsapp_number"=> $value->watsapp_number,"youtube_link"=> $value->youtube_link,"teligram_number"=> $value->teligram_number,"status"=> $value->status,"step"=>  $value->step,"created_by"=> $value->created_by,"updated_by"=> $value->updated_by,"created_at"=> $value->created_at,"updated_at"=> $value->updated_at,"year_of_graduation"=> $value->year_of_graduation,"appointment_type"=> $value->appointment_type,"clinic_wallpapper"=> $value->clinic_wallpapper,"clinic_name"=> $value->clinic_name,"clinic_buisness_name"=> $value->clinic_buisness_name,"pan_number"=> $value->pan_number,"speciality_name"=>$speciality_name,"language_name"=>$language_name);
        }  

        $path = array("licence_copy"=>env('APP_URL').'media/licence_copy',
                            "profile_pic"=>env('APP_URL').'media/profile_pic',
                            "clinic_logo"=>env('APP_URL').'media/clinic_logo'
                        );
  
     if ($results) {
            return response()->json(["status" => true,"massage"=>"Doctors retrieved successfully","data" => $data,"path" => $path,'admin_price'=>$admin_price]);
        }else{
            return response()->json(["status" => false,"data" => $results]);
        }

    }
    public function doctors_search_last_name(Request $request)
    {
                      
        $results = Doctors::where('last_name','like', '%' . $request->last_name. '%')->where('status',0)->get();

        $admin_price = DB::table('booking_price_admin')->where('id',7)->select('booking_price_admin.book_price')->first();
      
        $data = array();
        foreach ($results as $value) {

            $speciality_name = array();
            $language_name = array();
            /*echo "<pre>";
            print_r($doctors->toArray()); exit;*/
            $d = explode(',',$value->speciality);
            $d1 = explode(',',$value->language);

            $speciality_name = DB::table('specilities')->whereIn('id',$d)->get();
            $language_name = DB::table('language')->whereIn('id',$d1)->get();

            $data[] = array("id"=> $value->id,"d_uni_id"=> $value->d_uni_id,"first_name"=> $value->first_name,"last_name"=> $value->last_name,"email"=> $value->email,"bio"=> $value->bio,"pincode"=> $value->pincode,"state"=> $value->state,"city"=> $value->city,"address"=>$value->address,"main_mobile"=> $value->main_mobile,"second_mobile"=> $value->second_mobile,"clinic_team_leader"=> $value->clinic_team_leader,"clinic_admin_mobile_no"=> $value->clinic_admin_mobile_no,"clinic_email"=> $value->clinic_email,"experience"=> $value->experience,"language"=> $value->language,"speciality"=> $value->speciality,"price"=> $value->price,"licence_type"=> $value->licence_type,"licence_no"=> $value->licence_no,"valide_upto"=> $value->valide_upto,"issued_by"=> $value->issued_by,"licence_copy"=> $value->licence_copy,"profile_pic"=> $value->profile_pic,"clinic_logo"=> $value->clinic_logo,"bank_name"=> $value->bank_name,"account_name"=> $value->account_name,"ifsc_code"=> $value->ifsc_code,"account_no"=> $value->account_no,"cheque_img"=> $value->cheque_img,"watsapp_number"=> $value->watsapp_number,"youtube_link"=> $value->youtube_link,"teligram_number"=> $value->teligram_number,"status"=> $value->status,"step"=>  $value->step,"created_by"=> $value->created_by,"updated_by"=> $value->updated_by,"created_at"=> $value->created_at,"updated_at"=> $value->updated_at,"year_of_graduation"=> $value->year_of_graduation,"appointment_type"=> $value->appointment_type,"clinic_wallpapper"=> $value->clinic_wallpapper,"clinic_name"=> $value->clinic_name,"clinic_buisness_name"=> $value->clinic_buisness_name,"pan_number"=> $value->pan_number,"speciality_name"=>$speciality_name,"language_name"=>$language_name);
        }  

        $path = array("licence_copy"=>env('APP_URL').'media/licence_copy',
                            "profile_pic"=>env('APP_URL').'media/profile_pic',
                            "clinic_logo"=>env('APP_URL').'media/clinic_logo'
                        );
  
     if ($results) {
            return response()->json(["status" => true,"massage"=>"Doctors retrieved successfully","data" => $data,"path" => $path,'admin_price'=>$admin_price]);
        }else{
            return response()->json(["status" => false,"data" => $results]);
        }

    }
    public function booking_year_search(Request $request)
    {
                      
      $bookingYear_data = BookingRequest::whereYear('booking_request.date_time',$request->year_data)->where('booking_request.patient_name',$request->patient_id)->get(); 
     if ($bookingYear_data) {
            return response()->json(["status" => true,"massage"=>"Booking retrieved successfully","data" => $bookingYear_data]);
        }else{
            return response()->json(["status" => false,"data" => $bookingYear_data]);
        }

    }
     public function booking_year_month_search(Request $request)
    {
                      
      $bookingYear_data = BookingRequest::whereYear('booking_request.date_time',$request->year_data)->whereMonth('booking_request.date_time',$request->month_data)->where('booking_request.patient_name',$request->patient_id)->get(); 
     if ($bookingYear_data) {
            return response()->json(["status" => true,"massage"=>"Booking retrieved successfully","data" => $bookingYear_data]);
        }else{
            return response()->json(["status" => false,"data" => $bookingYear_data]);
        }

    }
    public function statement_view()
   {
     /*if(!empty($request->s_date)){
       $startDate =  Carbon::parse($request->s_date)->startOfDay(); 
       $endDate =  Carbon::now()->endOfDay();*/
       $doctor_id = Auth::user()->userDetailsId;
       $dr_amount = Doctor_withdraw::where('doctor_id',$doctor_id)
                    ->orderBy('id', 'desc')
                   // ->whereBetween('Doctor_withdraw.created_at', [$startDate, $endDate]) 
                    ->get();
      // }
       if ($dr_amount) {
            return response()->json(["status" => true,"massage"=>"Satement retrieved successfully","data" => $dr_amount]);
        }else{
            return response()->json(["status" => false,"data" => $dr_amount]);
        }
    
   } 
   
   public function myconsults_list(Request $request)
   {
       $doctor_id = Auth::user()->userDetailsId;
        $startDate =  Carbon::parse($request->s_date)->startOfDay(); 
        $endDate =  Carbon::parse($request->e_date)->endOfDay();
        $patients = BookingRequest::join('patient_details','booking_request.patient_name','patient_details.id')
                              ->select('booking_request.*','patient_details.first_name','patient_details.last_name','patient_details.id as patient_id')
                            //   ->where('booking_request.status',3)
                              ->whereBetween('booking_request.created_at', [$startDate, $endDate]) 
                              ->where('booking_request.doctor_name', $doctor_id)
                              ->where('booking_request.call_status','=',9)
                            //   ->orWhere('booking_request.call_status', '=', 1)
                             
                             
                              ->orderBy('appointment_time', 'DESC')
                              ->get();
       if ($patients) {
            return response()->json(["status" => true,"massage"=>"my consults list retrieved successfully","data" => $patients]);
        }else{
            return response()->json(["status" => false,"data" => $patients]);
        }
    
   } 
    public function myvisit_summaries_list(Request $request)
    {
        $doctor_id = Auth::user()->userDetailsId;
        $startDate =  Carbon::parse($request->s_date)->startOfDay(); 
        $endDate =  Carbon::parse($request->e_date)->endOfDay();
        $patients = BookingRequest::join('patient_details','booking_request.patient_name','patient_details.id')
                              ->join('users','booking_request.patient_name','users.userDetailsId')
                             ->select('booking_request.*','patient_details.first_name','patient_details.last_name','patient_details.id as patient_id','patient_details.age','patient_details.gender','patient_details.height','patient_details.weight','patient_details.lmp','users.uid as uid')
                            //   ->where('booking_request.status','=',2)
                            //   ->where('users.user_type','=',2)
                              ->whereBetween('booking_request.created_at', [$startDate, $endDate]) 
                            //   ->whereDate('booking_request.updated_at', Carbon::today())
                              ->where('booking_request.doctor_name', $doctor_id)
                              ->orderBy('appointment_time', 'DESC')
                              ->get();
       if ($patients) {
            return response()->json(["status" => true,"massage"=>"my consults list retrieved successfully","data" => $patients]);
        }else{
            return response()->json(["status" => false,"data" => $patients]);
        }
    
    //    $doctor_id = Auth::user()->userDetailsId;
    //     $startDate =  Carbon::parse($request->s_date)->startOfDay(); 
    //     $endDate =  Carbon::now()->endOfDay();
    //    $myNotePatients = My_note_patient::where('my_doctor_id',$doctor_id)
    //                      ->join('patient_details', 'my_note_patient.my_patient_id', '=', 'patient_details.id')
    //                      ->join('doctor_details', 'my_note_patient.my_doctor_id', '=', 'doctor_details.id')
    //                      ->whereBetween('my_note_patient.created_at', [$startDate, $endDate])
    //                      ->select('my_note_patient.*','patient_details.first_name as patient','doctor_details.first_name','doctor_details.last_name')
    //                     ->get();
     
    //  if ($myNotePatients) {
    //         return response()->json(["status" => true,"massage"=>"myvisit summaries list retrieved successfully","data" => $myNotePatients]);
    //     }else{
    //         return response()->json(["status" => false,"data" => $myNotePatients]);
    //     }
    }

     public function my_prescriptions_list(Request $request)
    {
        $doctor_id = Auth::user()->userDetailsId;
        $startDate =  Carbon::parse($request->s_date)->startOfDay(); 
        $endDate =  Carbon::parse($request->e_date)->endOfDay();
        // $prescription_details = Prescription_details::where('prescription_details.doctor_id',$request->userDetailsId)
        //                                 ->join('patient_details', 'prescription_details.patient_id', '=', 'patient_details.id')
        //                                 ->join('booking_request', 'prescription_details.doctor_id', '=', 'booking_request.doctor_name')
        //                                 ->whereBetween('prescription_details.created_at', [$startDate, $endDate])
        //                                 ->select('prescription_details.*','patient_details.first_name','patient_details.last_name','booking_request.*')
        //                                 ->get();
        $Prescription_details = Prescription_details::where('prescription_details.doctor_id',$doctor_id)
        ->join('doctor_details', 'prescription_details.doctor_id', '=', 'doctor_details.id')
        ->join('booking_request', 'prescription_details.booking_id', '=', 'booking_request.book_id')
        ->select('prescription_details.*','booking_request.appointment_time','booking_request.consult_time','doctor_details.first_name','doctor_details.last_name','prescription_details.id as prescription_details_id','booking_request.*')
        ->whereBetween('prescription_details.created_at', [$startDate, $endDate])
        ->orderBy('appointment_time', 'desc')
        ->orderBy('consult_time', 'asc')
        ->get();
        if (empty($Prescription_details)) {

            return $this->sendError('Prescription_details not found');
    
        }
    
    
    
        return $this->sendResponse($Prescription_details->toArray(), 'Prescription_details retrieved successfully');
    //   if ($prescription_details) {
    //         return response()->json(["status" => true,"massage"=>"prescription details list retrieved successfully","data" => $prescription_details]);
    //     }else{
    //         return response()->json(["status" => false,"data" => $prescription_details]);
    //     }
    }
     public function patients_profile(Request $request)
    {
        $doctor_id = Auth::user()->userDetailsId;
        $patient_id =  $request->patient_id; 
        //$book_id =  $request->book_id;
        $detail = Patients::where('id',$patient_id)->first();

      if ($detail) {
            return response()->json(["status" => true,"massage"=>"patients profile retrieved successfully","data" => $detail]);
        }else{
            return response()->json(["status" => false,"data" => $detail]);
        }
    }
    public function appoiment_done(Request $request)
    {
        $booklist =  DB::table('booking_request')->where('book_id',$request->book_id)->update(['status' => 3,'updated_at' => Carbon::now()]);
        if ($booklist) {

            $book_patient =  DB::table('booking_request')->where('book_id',$request->book_id)->first();

            $user_patient = \App\User::where('userDetailsId',$book_patient->patient_name)->where('user_type',2)->first();

            $this->send_notification($user_patient->mobile_token," Consult closed by doctor","Consult closed");

            $this->send_mail1($user_patient,"Consult closed by doctor");

            return response()->json(["status" => true,"massage"=>"appoiment done retrieved successfully","data" => $booklist]);
        }else{
            return response()->json(["status" => false,"data" => $booklist]);
        }
    }
    public function send_mail1($user,$mes)
    {
        $data['personalizations'] = array("0" => array('to' => array('0' => array('email' => $user->email))));

        $data['from'] = array('email' => 'system@haspatal.com');

        $data['subject'] = "Appointment Booking";
        $data['content'] = array("0" =>array('type' =>' text/html','value' => "<DOCTYPE html><html lang='en-US'>     <head><meta charset='utf-8'></head><body><h2>  ".$mes."</body></html>" ));

        //echo json_encode($data);exit();

        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://api.sendgrid.com/v3/mail/send",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => json_encode($data),
          CURLOPT_HTTPHEADER => array(
            "authorization: Bearer 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",
            "cache-control: no-cache",
            "content-type: application/json",
            "postman-token: b8c0781f-af48-90cc-1c5b-76f80e0b4be0"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          echo "cURL Error #:" . $err;
        } else {
          return $response;
        }
    }
    public function dr_cancle_refund_booking(Request $request)
    {
         $doctor_id = Auth::user()->userDetailsId;
         $booklist =  DB::table('booking_request')->where('book_id',$request->book_id)->update(['status' => 5,'patient_status'=>2,'autorefund_status'=>1,'updated_at' => Carbon::now()]);


 
        if ($booklist) {

            $book_patient =  DB::table('booking_request')->where('book_id',$request->book_id)->first();
            $user_patient = \App\User::where('userDetailsId',$book_patient->patient_name)->where('user_type',2)->first();

            $total_fund =  DB::table('patient_add_fund')->where('patient_id', $book_patient->patient_name)->first();

            if (!empty($total_fund)) {
            
                $mi_fund = $total_fund->wa_amount + $book_patient->f_amount;

                $input['w_amount'] =  $mi_fund;

                $fund =  DB::table('patient_add_fund')->where('patient_id',$book_patient->patient_name)->update(['wa_amount' => $mi_fund]);
            }else{

                //$buy_plan = DB::table('patient_add_fund')->insert([ 'plan_id' => $input['plan_id'],'payable_amount'=>$request->payable_amount,'promo_code'=> $request->promo_code,'discount'=>$request->discount,'gst_amount'=>$request->gst_amount,'final_amount'=>$request->final_amount,'gst_number'=>$request->gst_number,'payment_id'=>$request->payment_id,'payment_status'=>$request->payment_status,'user_id'=>$request->user_id]);

            }
            
            $patients = BookingRequest::join('patient_details','booking_request.patient_name','patient_details.id')
                                  ->select('booking_request.*','patient_details.first_name','patient_details.last_name','patient_details.id as patient_id')
                                  ->where('booking_request.status',5)
                                  ->where('booking_request.book_id',$request->book_id)
                                  //->whereDate('booking_request.updated_at', Carbon::today())
                                  ->where('booking_request.doctor_name', $doctor_id)
                                  ->get();
            
            $this->send_notification($user_patient->mobile_token,"Cancel refund","Cancel refund",$patients);
        
            return response()->json(["status" => true,"massage"=>"cancle refund retrieved successfully","data" => $patients]);
        }else{
            return response()->json(["status" => false,"data" => []]);
        }

      }

    public function send_notification($token,$message,$title,$data = array())
    {
        $content = array(
           "en" => $message
        );

        $heading = array(
           "en" => $title
        );
       
        $fields = array(
           'app_id' => "33fe1b86-089a-4234-af9d-d7a923926620",
           'include_player_ids' => array($token),
           'data' => array("data" => $data),
           'contents' => $content,
           'headings' => $heading
        );
           
        $fields = json_encode($fields);
        
       
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $response = curl_exec($ch);
        curl_close($ch);
        //echo "<pre>";print_r($response);exit();
        return $response;
    }
    public function score_card()
    {
         // $doctor_id = Auth::user()->userDetailsId;
         // $score = BookingRequest::where('doctor_name',$doctor_id)->count('doctor_name');
         // $score_data1 = $score / 100;
         // $score_data = number_format($score_data1, 2);

        $doctor_id = Auth::user()->userDetailsId;

        $totalreviewsum = DB::table('review')
                  ->where('review.doctor_id', $doctor_id)->sum('review.rating');

        $total_review = DB::table('review')
                  ->where('review.doctor_id', $doctor_id)->count();        

        $final_total =   $totalreviewsum / $total_review * 5;

        $score_data =  number_format($final_total, 2);
        
        
          if ($score_data) {
            return response()->json(["status" => true,"massage"=>"score data retrieved successfully","data" => $score_data]);
        }else{
            return response()->json(["status" => false,"data" => $score_data]);
        }

      }
    public function complaint_list(Request $request)
    {
         $doctor_id = Auth::user()->userDetailsId;
         $myFavourite =  DB::table('present_complaint_patient')
                        ->join('patient_details', 'present_complaint_patient.pc_patient_id', '=', 'patient_details.id')
                        ->join('doctor_details', 'present_complaint_patient.pc_doctor_id', '=', 'doctor_details.id')
                        ->where('present_complaint_patient.book_id',$request->book_id)
                        ->where('present_complaint_patient.pc_doctor_id',$doctor_id)
                        ->select('present_complaint_patient.id as my_id','present_complaint_patient.*','doctor_details.first_name','doctor_details.last_name','patient_details.first_name as patient')
                        ->get();

         $path = array("complaint_pic"=>env('APP_URL').'media/complaint_pic',
                 "complaint_pdf"=>env('APP_URL').'media/complaint_pdf');
      if ($myFavourite) {
            return response()->json(["status" => true,"data" => $myFavourite, "message" => "Present Complaint Patients retrieved successfully","path" => $path]);
        }else{
            return response()->json(["status" => false,"data" => '', "message" => "Present Complaint Patients retrieved successfully"]);
        }
   }
   public function medications_list(Request $request)
    {
         $doctor_id = Auth::user()->userDetailsId;
         $myFavourite =  DB::table('present_medications_patient')
                        ->where('present_medications_patient.m_book_id',$request->m_book_id)
                        ->where('present_medications_patient.patient_id',$request->patient_id)
                        ->where('present_medications_patient.doctor_id',$doctor_id)
                        ->get();

        if ($myFavourite) {
            return response()->json(["status" => true,"data" => $myFavourite, "message" => "Medications list retrieved successfully"]);
        }else{
            return response()->json(["status" => false,"data" => '', "message" => "Medications list retrieved successfully"]);
        }
   }
   public function my_note_list(Request $request)
    {
         $doctor_id = Auth::user()->userDetailsId;
         $myFavourite =  DB::table('my_note_patient')
                        ->where('my_note_patient.m_book_id',$request->m_book_id)
                        ->where('my_note_patient.my_patient_id',$request->my_patient_id)
                        ->where('my_note_patient.my_doctor_id',$doctor_id)
                        ->get();


        if ($myFavourite) {
            return response()->json(["status" => true,"data" => $myFavourite, "message" => "My Note list retrieved successfully"]);
        }else{
            return response()->json(["status" => false,"data" => '', "message" => "Medications list retrieved successfully"]);
        }
   }
    public function allergies_list(Request $request)
    {
         $doctor_id = Auth::user()->userDetailsId;
         $myFavourite =  DB::table('suggested_investigations_patient')
                        ->where('suggested_investigations_patient.s_book_id',$request->s_book_id)
                        ->where('suggested_investigations_patient.s_patient_id',$request->s_patient_id)
                        ->where('suggested_investigations_patient.s_doctor_id',$doctor_id)
                        ->get();


        if ($myFavourite) {
            return response()->json(["status" => true,"data" => $myFavourite, "message" => "allergies list retrieved successfully"]);
        }else{
            return response()->json(["status" => false,"data" => '', "message" => "allergies list retrieved successfully"]);
        }
   }
    public function hpi_list(Request $request)
    {
         $doctor_id = Auth::user()->userDetailsId;
         $myFavourite =  DB::table('perceived_patient')
                        ->where('perceived_patient.p_book_id',$request->p_book_id)
                        ->where('perceived_patient.p_patient_id',$request->p_patient_id)
                        ->where('perceived_patient.p_doctor_id',$doctor_id)
                        ->get();


        if ($myFavourite) {
            return response()->json(["status" => true,"data" => $myFavourite, "message" => "Hpi list retrieved successfully"]);
        }else{
            return response()->json(["status" => false,"data" => '', "message" => "Hpi list retrieved successfully"]);
        }
   }
      public function consult_list(Request $request)
    {
         $doctor_id = Auth::user()->userDetailsId;
         $myFavourite =  DB::table('consultant')
                        ->where('consultant.c_book_id',$request->c_book_id)
                        ->where('consultant.c_patient_id',$request->c_patient_id)
                        ->where('consultant.c_doctor_id',$doctor_id)
                        ->get();


        if ($myFavourite) {
            return response()->json(["status" => true,"data" => $myFavourite, "message" => "consult list retrieved successfully"]);
        }else{
            return response()->json(["status" => false,"data" => '', "message" => "consult list retrieved successfully"]);
        }
   }
   
   public function drlab_list(Request $request)
    {
         $doctor_id = Auth::user()->userDetailsId;
         $myFavourite =  DB::table('suggested_specialists_patient')
                        ->where('suggested_specialists_patient.ss_book_id',$request->ss_book_id)
                        ->where('suggested_specialists_patient.ss_patient_id',$request->ss_patient_id)
                        ->where('suggested_specialists_patient.ss_doctor_id',$doctor_id)
                        ->get();


        if ($myFavourite) {
            return response()->json(["status" => true,"data" => $myFavourite, "message" => "lebs list retrieved successfully"]);
        }else{
            return response()->json(["status" => false,"data" => '', "message" => "lebs list retrieved successfully"]);
        }
   }
    public function other_list(Request $request)
    {
         $doctor_id = Auth::user()->userDetailsId;
         $myFavourite =  DB::table('Examination_notes_patient')
                        ->where('Examination_notes_patient.e_book_id',$request->e_book_id)
                        ->where('Examination_notes_patient.e_patient_id',$request->e_patient_id)
                        ->where('Examination_notes_patient.each(array)_doctor_id',$doctor_id)
                        ->get();


        if ($myFavourite) {
            return response()->json(["status" => true,"data" => $myFavourite, "message" => "Other list retrieved successfully"]);
        }else{
            return response()->json(["status" => false,"data" => '', "message" => "Other list retrieved successfully"]);
        }
   }
    public function lab_list(Request $request)
    {
         $doctor_id = Auth::user()->userDetailsId;
         $myFavourite =  DB::table('Available_lab_patient')
                        ->where('Available_lab_patient.a_book_id',$request->a_book_id)
                        ->where('Available_lab_patient.a_patient_id',$request->a_patient_id)
                        ->where('Available_lab_patient.a_doctor_id',$doctor_id)
                        ->get();

        if ($myFavourite) {
            return response()->json(["status" => true,"data" => $myFavourite, "message" => "lab list retrieved successfully"]);
        }else{
            return response()->json(["status" => false,"data" => '', "message" => "Lab list retrieved successfully"]);
        }
   }
    public function refer_list(Request $request)
    {
         $doctor_id = Auth::user()->userDetailsId;
         $myFavourite =  DB::table('suggested_therapies_patient')
                        ->where('suggested_therapies_patient.st_book_id',$request->st_book_id)
                        ->where('suggested_therapies_patient.st_patient_id',$request->st_patient_id)
                        ->where('suggested_therapies_patient.st_doctor_id',$doctor_id)
                        ->get();

        if ($myFavourite) {
            return response()->json(["status" => true,"data" => $myFavourite, "message" => "Refer list retrieved successfully"]);
        }else{
            return response()->json(["status" => false,"data" => '', "message" => "Refer list retrieved successfully"]);
        }
   }
       public function treatment_list(Request $request)
    {
         $doctor_id = Auth::user()->userDetailsId;
         $myFavourite =  DB::table('key_points_patient')
                        ->where('key_points_patient.k_book_id',$request->k_book_id)
                        ->where('key_points_patient.patient_id',$request->patient_id)
                        ->where('key_points_patient.doctor_id',$doctor_id)
                        ->get();

        if ($myFavourite) {
            return response()->json(["status" => true,"data" => $myFavourite, "message" => "Treatment list retrieved successfully"]);
        }else{
            return response()->json(["status" => false,"data" => '', "message" => "Treatment list retrieved successfully"]);
        }
   }
    public function cancel_booking()
    {
         $doctor_id = Auth::user()->userDetailsId;
         $myFavourite =  DB::table('booking_request')
                        ->join('doctor_details', 'booking_request.doctor_name', '=', 'doctor_details.id')
                        ->where('booking_request.doctor_name',$doctor_id)
                        ->where('booking_request.status','!=',3)
                        ->where('booking_request.status','!=',5)
                        ->select('booking_request.id','booking_request.date_time','booking_request.book_id','booking_request.doctor_name','doctor_details.first_name','doctor_details.last_name')
                        ->get();

        if ($myFavourite) {
            return response()->json(["status" => true,"data" => $myFavourite, "message" => "cancel booking list retrieved successfully"]);
        }else{
            return response()->json(["status" => false,"data" => '', "message" => "cancel booking list retrieved successfully"]);
        }
   }
   
   public function prescriptionlist(Request $request)
    {
         $doctor_id = Auth::user()->userDetailsId;
         $myFavourite =  DB::table('prescription_details')
                        ->where('prescription_details.booking_id',$request->booking_id)
                        ->where('prescription_details.patient_id',$request->patient_id)
                        ->where('prescription_details.doctor_id',$doctor_id)
                        ->get();

        if ($myFavourite) {
            return response()->json(["status" => true,"data" => $myFavourite, "message" => "prescription list retrieved successfully"]);
        }else{
            return response()->json(["status" => false,"data" => '', "message" => "prescription list retrieved successfully"]);
        }
   }
   
  

   public function imagings_list(Request $request)
    {
         $doctor_id = Auth::user()->userDetailsId;
         $myFavourite =  DB::table('suggested_imagings_patient')
                        ->where('suggested_imagings_patient.si_book_id',$request->si_book_id)
                        ->where('suggested_imagings_patient.si_patient_id',$request->si_patient_id)
                        ->where('suggested_imagings_patient.si_doctor_id',$doctor_id)
                        ->get();

        if ($myFavourite) {
            return response()->json(["status" => true,"data" => $myFavourite, "message" => "lab list retrieved successfully"]);
        }else{
            return response()->json(["status" => false,"data" => '', "message" => "Lab list retrieved successfully"]);
        }
   }
   public function booking_id_dropdown(Request $request)
    {
                      
      $booking =BookingRequest::join('doctor_details','booking_request.doctor_name','doctor_details.id')
                                ->where('booking_request.status',3)
                                ->where('booking_request.patient_name', $request->patient_id)
                                ->select('booking_request.*','doctor_details.first_name','doctor_details.last_name','doctor_details.profile_pic','doctor_details.clinic_logo')
                                ->get(); 
      $path = array("profile_pic"=>env('APP_URL').'media/profile_pic',
                            "clinic_logo"=>env('APP_URL').'media/clinic_logo'
                        );
     if ($booking) {
            return response()->json(["status" => true,"massage"=>"Booking retrieved successfully","data" => $booking,"path" => $path]);
        }else{
            return response()->json(["status" => false,"data" => $booking]);
        }

    }
    public function review_list(Request $request)
    {
                      
      $review_list =Review::join('doctor_details','review.doctor_id','doctor_details.id')
                          ->where('review.patient_id',$request->patient_id)
                          ->select('review.*','doctor_details.first_name','doctor_details.last_name')
                          ->get(); 
     if ($review_list) {
            return response()->json(["status" => true,"massage"=>"Patient Review retrieved successfully","data" => $review_list]);
        }else{
            return response()->json(["status" => false,"data" => $review_list]);
        }

    }
     public function review_list_doctor(Request $request)
    {
       $startDate =  Carbon::parse($request->s_date)->startOfDay(); 
       $endDate =  Carbon::now()->endOfDay(); 
       if($request->s_date == ''){
         $doctor_review_list =Review::join('patient_details', 'review.patient_id', '=', 'patient_details.id')
                            ->where('review.doctor_id',$request->doctor_id)
                            ->select('review.*','patient_details.first_name as patientname')
                            ->get();
       }else{        
       $doctor_review_list =Review::join('patient_details', 'review.patient_id', '=', 'patient_details.id')
                            ->where('review.doctor_id',$request->doctor_id)
                            ->whereBetween('review.created_at', [$startDate, $endDate])
                            ->select('review.*','patient_details.first_name as patientname')
                            ->get(); 
       }                
     if ($doctor_review_list) {
            return response()->json(["status" => true,"massage"=>"Doctot Review retrieved successfully","data" => $doctor_review_list]);
        }else{
            return response()->json(["status" => false,"data" => $doctor_review_list]);
        }

    }
    public function review_answer(Request $request)
    {
                      
      $review_answer = DB::table('review')->where('book_id',$request->book_id)->update(['review_ans' => $request->review_ans]); 
     if ($review_answer) {
        $review_list =Review::where('review.patient_id',$request->patient_id)->get();
            return response()->json(["status" => true,"massage"=>"Review Answer retrieved successfully","data" => $review_list]);
        }else{
            return response()->json(["status" => false,"data" => $review_list]);
        }

    }
    public function time_slots_list(Request $request)
    {
                      
      $time_list = DB::table('time_slot')->where('doctor_id',$request->doctor_id)->get(); 
     if ($time_list) {
        //$review_list =Review::where('review.patient_id',$request->patient_id)->get();
            return response()->json(["status" => true,"massage"=>"Time slots retrieved successfully","data" => $time_list]);
        }else{
            return response()->json(["status" => false,"data" => $time_list]);
        }

    }
    public function haspatal_schedule_home()
    {
         $all_count = DB::table('booking_request')->where('status','!=' , 5)->where('doctor_name', Auth::user()->userDetailsId)->count();

          $today = new \DateTime(); 
          $year  = (int) $today->format('Y');
          $week  = (int) $today->format('W'); // Week of the year
          $day   = (int) $today->format('w'); // Day of the week (0 = sunday)
          $month   = (int) $today->format('m');
          $today_day   = (int) $today->format('d');

          

        $last_month = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereMonth('appointment_time', $month - 1)->count();
        $lastmonth_Consulatnt = BookingRequest::select('*')->where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereMonth('appointment_time', $month - 1)->get();
/////////////////////////////////////
        $last_year = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereYear('appointment_time', $year - 1)->count();
        $lastyearly_Consulatnt = BookingRequest::select('*')->where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereYear('appointment_time', $year - 1)->get();
/////////////////////////////////////
        $this_year = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereYear('appointment_time', $year)->count();
        $yearly_Consulatnt = BookingRequest::join('patient_details','booking_request.patient_name','patient_details.id')
        ->join('users','booking_request.patient_name','users.userDetailsId')
       ->select('booking_request.*','patient_details.first_name','patient_details.last_name','patient_details.id as patient_id','patient_details.age','patient_details.gender','patient_details.height','patient_details.weight','patient_details.lmp','users.uid as uid')
       ->groupBy('booking_request.id')
       ->where('booking_request.doctor_name', Auth::user()->userDetailsId)->where('booking_request.status','!=' , 5)->whereYear('appointment_time', $year)->get();
/////////////////////////////
        $this_month = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereMonth('appointment_time', $month)->count();
        $moth_Consulatnt = BookingRequest::join('patient_details','booking_request.patient_name','patient_details.id')
        ->join('users','booking_request.patient_name','users.userDetailsId')
       ->select('booking_request.*','patient_details.first_name','patient_details.last_name','patient_details.id as patient_id','patient_details.age','patient_details.gender','patient_details.height','patient_details.weight','patient_details.lmp','users.uid as uid')
       ->groupBy('booking_request.id')
       ->where('booking_request.doctor_name', Auth::user()->userDetailsId)->where('booking_request.status','!=' , 5)->whereMonth('appointment_time', $month)->get();
///////////////////////////////
        $this_today = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereDay('appointment_time', $today_day)->whereMonth('appointment_time', $month)->count();
        $todayConsulatnt =  BookingRequest::join('patient_details','booking_request.patient_name','patient_details.id')
        ->join('users','booking_request.patient_name','users.userDetailsId')
       ->select('booking_request.*','patient_details.first_name','patient_details.last_name','patient_details.id as patient_id','patient_details.age','patient_details.gender','patient_details.height','patient_details.weight','patient_details.lmp','users.uid as uid')
       ->groupBy('booking_request.id')
       ->where('booking_request.doctor_name', Auth::user()->userDetailsId)->where('booking_request.status','!=' , 5)->whereDay('booking_request.appointment_time', $today_day)->whereMonth('appointment_time', $month)->get();
   //////////////////////////////////////     
        $this_weeks = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereBetween('appointment_time', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->count();
        $weeksConsulatnt =BookingRequest::join('patient_details','booking_request.patient_name','patient_details.id')
        ->join('users','booking_request.patient_name','users.userDetailsId')
       ->select('booking_request.*','patient_details.first_name','patient_details.last_name','patient_details.id as patient_id','patient_details.age','patient_details.gender','patient_details.height','patient_details.weight','patient_details.lmp','users.uid as uid')
       ->groupBy('booking_request.id')
       ->where('booking_request.doctor_name', Auth::user()->userDetailsId)->where('booking_request.status','!=' , 5)->whereBetween('booking_request.appointment_time', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->get();
        //////////////////////////////////////
        $t_amount = BookingRequest::where('doctor_name',Auth::user()->userDetailsId)->where('call_status', '=',9)->sum('f_amount');
       
        if ($all_count || $last_month || $last_year || $this_year || $this_month || $this_today) {
             return response()->json(["status" => true,"massage"=>"Count Data retrieved successfully","all_count" => $all_count,"last_month" => $last_month,"last_year" => $last_year,"this_year" => $this_year,"this_month"=>$this_month,"today"=>$this_today,'this_week'=>$this_weeks,"total_earnings"=>$t_amount,"yearly_Consulatnt"=>$yearly_Consulatnt,"weeksConsulatnt"=>$weeksConsulatnt,"todayConsulatnt"=>$todayConsulatnt,"moth_Consulatnt"=>$moth_Consulatnt]);
        }else{
            return response()->json(["status" => false,"data" => '']);
        }

    }

    public function homepage_today_booking()
    {
        $mytime = Carbon::now();
        $today = new \DateTime(); 
        $year  = (int) $today->format('Y');
        $week  = (int) $today->format('W'); // Week of the year
        $day   = (int) $today->format('w'); // Day of the week (0 = sunday)
        $month   = (int) $today->format('m');

        $doctor_id = Auth::user()->userDetailsId;
        $today_count = DB::table('booking_request')->where('status','!=' , 5)->where('doctor_name', Auth::user()->userDetailsId)->whereDate('date_time', Carbon::today())->count();

        $time_sloat = DB::table('time_slot')->where('doctor_id',Auth::user()->userDetailsId)->first();

        $mor_start = new \DateTime($time_sloat->mrg_from_time);
        $mor_end = new \DateTime($time_sloat->mrg_to_time);
        $mor_timediff = $mor_start->diff($mor_end);

        $aft_start = new \DateTime($time_sloat->aft_from_time);
        $aft_end = new \DateTime($time_sloat->aft_to_time);
        $aft_timediff = $aft_start->diff($aft_end);

        $eve_start = new \DateTime($time_sloat->eve_from_time);
        $eve_end = new \DateTime($time_sloat->eve_to_time);
        $eve_timediff = $eve_start->diff($eve_end);


        $mor_total_sloat =   ($mor_timediff->h * 60) /  $time_sloat->booking_time_slot;
        $aft_total_sloat =  ($aft_timediff->h * 60) /  $time_sloat->booking_time_slot;
        $eve_total_sloat =  ($eve_timediff->h * 60) /  $time_sloat->booking_time_slot;

        $total_booking_sloat = $mor_total_sloat + $aft_total_sloat + $eve_total_sloat;
            


        /* $this_weeks = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->whereBetween('date_time', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->count();

          $last_weeks = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->whereDate('date_time', date('Y-m-d H:i:s',strtotime('-7 days')) )->count();*/
        $this_weeks = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereBetween('date_time', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->count();
    
        $start = Carbon::now()->subWeek()->startOfWeek();
        $end = Carbon::now()->subWeek()->endOfWeek();
        $last_weeks = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereBetween('date_time', [$start, $end])->count();

           //echo "<pre>"; print_r($last_weeks); exit();

        $yester_day = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereDate('date_time', date('Y-m-d', strtotime("-1 days")) )->count();

        $mor = DB::table('booking_request')->where('slot_status','1')->where('status','!=' , 5)->where('doctor_name', Auth::user()->userDetailsId)->whereDate('date_time', Carbon::today())->count();

        $aft = DB::table('booking_request')->where('slot_status','2')->where('status','!=' , 5)->where('doctor_name', Auth::user()->userDetailsId)->whereDate('date_time', Carbon::today())->count();

        $eve = DB::table('booking_request')->where('slot_status','3')->where('status','!=' , 5)->where('doctor_name', Auth::user()->userDetailsId)->whereDate('date_time', Carbon::today())->count();

        $t_amount = BookingRequest::where('doctor_name',Auth::user()->userDetailsId)->whereDate('date_time', Carbon::today())->sum('f_amount');

         if ($today_count || $this_weeks || $last_weeks || $yester_day || $mor || $aft || $eve) {
             return response()->json(["status" => true,"massage"=>"Today Count Data retrieved successfully","today_count" => $today_count,"this_weeks" => $this_weeks,"last_weeks" => $last_weeks,"yester_day" => $yester_day,"mor" =>  $mor,"aft" => $aft,"eve"=>$eve,"mor_total" =>  $mor_total_sloat,"aft_total" => $aft_total_sloat,"eve_total"=>$eve_total_sloat,"total_booking_slot"=>$total_booking_sloat,"total_earnings"=>$t_amount]);
        }else{
            return response()->json(["status" => false,"data" => '']);
        }
    }
    public function home_this_week()
    {
        $mytime = Carbon::now();

        $doctor_id = Auth::user()->userDetailsId;
        $allcount = DB::table('booking_request')->where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereDate('booking_request.updated_at', Carbon::today())->count();
        $y_date = Carbon::parse(date('Y-m-d H:i:s',strtotime('-1 days')))->startOfDay();


        $yester_day = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereDate('date_time', $y_date)->count();

       
        // $this_weeks = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->whereDate('date_time', '>=', date('Y-m-d H:i:s',strtotime('7 days')) )->count();
        $start = Carbon::now()->subWeek()->startOfWeek();
        $end = Carbon::now()->subWeek()->endOfWeek();
        $last_weeks = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereBetween('date_time', [$start, $end])->count();
        
        $this_month = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereDate('date_time', '>=', [Carbon::now()->startOfMonth(), Carbon::now()->endOfMonth()])->count();
        $this_year = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereDate('date_time', '<=', date('Y-m-d H:i:s',strtotime('365 days')) )->count();


        $time_sloat = DB::table('time_slot')->where('doctor_id',Auth::user()->userDetailsId)->first();

        $mor_start = new \DateTime($time_sloat->mrg_from_time);
        $mor_end = new \DateTime($time_sloat->mrg_to_time);
        $mor_timediff = $mor_start->diff($mor_end);

        $aft_start = new \DateTime($time_sloat->aft_from_time);
        $aft_end = new \DateTime($time_sloat->aft_to_time);
        $aft_timediff = $aft_start->diff($aft_end);

        $eve_start = new \DateTime($time_sloat->eve_from_time);
        $eve_end = new \DateTime($time_sloat->eve_to_time);
        $eve_timediff = $eve_start->diff($eve_end);

        $mor_total_sloat =   ($mor_timediff->h * 60) /  $time_sloat->booking_time_slot;
        $aft_total_sloat =  ($aft_timediff->h * 60) /  $time_sloat->booking_time_slot;
        $eve_total_sloat =  ($eve_timediff->h * 60) /  $time_sloat->booking_time_slot;

        $total_booking_sloat = $mor_total_sloat + $aft_total_sloat + $eve_total_sloat;
        $total_week_booking_sloat = $total_booking_sloat*7;
    
        $this_weeks = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereBetween('date_time', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->count();

        $mon_date = Carbon::now()->startOfWeek();
        $mon = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereDate('date_time',$mon_date)->count();

        $tue_date =  Carbon::parse(date('Y-m-d',strtotime('-2 days')))->startOfDay(); 
        $tue = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->  whereDate('date_time',$tue_date)->count();

        $wed_date =  Carbon::parse(date('Y-m-d',strtotime('-1 days')))->startOfDay(); 
        $wed = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereDate('date_time',$wed_date)->count();

        $thr_date =  Carbon::parse(date('Y-m-d',strtotime('0 days')))->startOfDay(); 
        $thr = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereDate('date_time',$thr_date)->count();

        $fri_date =  Carbon::parse(date('Y-m-d',strtotime('+1 days')))->startOfDay(); 
        $fri = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereDate('date_time',$fri_date)->count();

        $sat_date =  Carbon::parse(date('Y-m-d',strtotime('+2 days')))->startOfDay(); 
        $sat = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereDate('date_time', $sat_date)->count();

        $sun_date =  Carbon::parse(date('Y-m-d',strtotime('+3 days')))->startOfDay(); 
        $sun = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereDate('date_time', $sun_date)->count();

        $t_amount = BookingRequest::where('doctor_name',Auth::user()->userDetailsId)->whereBetween('date_time', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->sum('f_amount');

        if ( $yester_day || $this_weeks || $last_weeks || $this_month || $this_year ||  $mon_date || $mon ||  $tue_date || $tue || $wed_date || $wed || $thr_date || $thr || $fri_date || $fri || $sat_date || $sat || $sun_date || $sun ) {
             return response()->json(["status" => true,"massage"=>"Week Count Data retrieved successfully","yester_day"=>$yester_day,"this_weeks" => $this_weeks,"last_weeks" => $last_weeks,"this_month" => $this_month,"this_year" => $this_year,"mon_date" => $mon_date,"mon" => $mon,"tue_date" => $tue_date,"tue" => $tue,"wed_date" => $wed_date,"wed" => $wed,"thr_date" => $thr_date,"thr" => $thr,"fri_date" =>$fri_date, "fri"=> $fri,"sat_date" =>$sat_date, "sat"=> $sat,"sun_date" =>$sun_date, "sun"=> $sun,"mor_total" =>  $mor_total_sloat,"aft_total" => $aft_total_sloat,"eve_total"=>$eve_total_sloat,"total_booking_slot"=>$total_booking_sloat,"week_booking_slot"=>$total_week_booking_sloat,"total_earnings"=>$t_amount]);
        }else{
            return response()->json(["status" => false,"data" => '']);
        }
    }


     public function home_month()
      {
        $mytime = Carbon::now();
           
           $start = Carbon::now()->subWeek()->startOfWeek();
           $end = Carbon::now()->subWeek()->endOfWeek();
           $last_weeks = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereBetween('date_time', [$start, $end])->count();
            
           $m_start = Carbon::now()->subMonth()->startOfMonth();
           $m_end = Carbon::now()->subMonth()->endOfMonth();

           $last_month = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereBetween('date_time', [$m_start, $m_end])->count();
           $this_year = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereDate('date_time', '<=', date('Y-m-d H:i:s',strtotime(' 365 days')) )->count();

        /*$week_sdate = date('Y-m-01');   
        $week_date1 = Carbon::parse(date('Y-m-d H:i:s',strtotime('7 days')))->startOfDay(); 
        $week1 = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->whereBetween('date_time', [$week_sdate, $week_date1])->whereDate('date_time','>',$week_date1)->count();*/
        
        $time_sloat = DB::table('time_slot')->where('doctor_id',Auth::user()->userDetailsId)->first();

        $mor_start = new \DateTime($time_sloat->mrg_from_time);
        $mor_end = new \DateTime($time_sloat->mrg_to_time);
        $mor_timediff = $mor_start->diff($mor_end);

        $aft_start = new \DateTime($time_sloat->aft_from_time);
        $aft_end = new \DateTime($time_sloat->aft_to_time);
        $aft_timediff = $aft_start->diff($aft_end);

        $eve_start = new \DateTime($time_sloat->eve_from_time);
        $eve_end = new \DateTime($time_sloat->eve_to_time);
        $eve_timediff = $eve_start->diff($eve_end);

        $mor_total_sloat =  ($mor_timediff->h * 60) /  $time_sloat->booking_time_slot;
        $aft_total_sloat =  ($aft_timediff->h * 60) /  $time_sloat->booking_time_slot;
        $eve_total_sloat =  ($eve_timediff->h * 60) /  $time_sloat->booking_time_slot;

        $dt = strtotime($mytime);
        $totalDays = date('t',$dt);

        $total_booking_sloat = $mor_total_sloat + $aft_total_sloat + $eve_total_sloat;
        $total_week_booking_sloat = $total_booking_sloat*7;
        $total_month_booking_sloat = $total_booking_sloat * $totalDays;


        $this_month = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereDate('date_time', '>=', [Carbon::now()->startOfMonth(), Carbon::now()->endOfMonth()])->count();

        $week_sdate = date('Y-m-01');   
        $week_date =  date('Y-m-07');
        $week1 = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereBetween('date_time', [$week_sdate, $week_date])->count();

        $week_sdate2 = date('Y-m-08');   
        $week_date2 =  date('Y-m-14'); 
       // echo $week_sdate2.'<br>';
        
        $week2 = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereBetween('date_time', [$week_sdate2, $week_date2])->count();

        $week_sdate3 = date('Y-m-15');   
        $week_date3 =  date('Y-m-21'); 
        $week3 = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereBetween('date_time', [$week_sdate3, $week_date3])->count();

        $week_sdate4 = date('Y-m-22');   
        $week_date4 =  date('Y-m-31');
        $week4 = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereBetween('date_time', [$week_sdate4, $week_date4])->count();


        $t_amount = BookingRequest::where('doctor_name',Auth::user()->userDetailsId)->whereBetween('date_time', [Carbon::now()->startOfMonth(), Carbon::now()->endOfMonth()])->sum('f_amount');

       if ($this_month || $last_month || $last_weeks || $this_year) {
             return response()->json(["status" => true,"massage"=>"Month Count Data retrieved successfully","this_month" => $this_month,"last_month" => $last_month,"last_weeks" => $last_weeks,"this_year" => $this_year,"week1" => $week1,"week2" => $week2,"week3" => $week3,"week4" => $week4,"mor_total" =>  $mor_total_sloat,"aft_total" => $aft_total_sloat,"eve_total"=>$eve_total_sloat,"total_booking_slot"=>$total_booking_sloat,"week_booking_slot"=>$total_week_booking_sloat,"total_month_booking_slot"=>$total_month_booking_sloat,"total_earnings"=>$t_amount]);
        }else{
            return response()->json(["status" => false,"data" => '']);
        }
      }
      
      public function home_year(Request $request)
      {
         $c_year = $request->c_year;
          $y_start = date($c_year.'-01-01 H:i:s');
          $y_end = date($c_year.'-12-31 H:i:s');
          //echo $y_end; exit;
           $m_end = Carbon::now()->subMonth()->endOfMonth();


           $all_count = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereBetween('date_time', [date($c_year.'-01-01 H:i:s'),date($c_year.'-12-31 H:i:s')])->count();
           $l_start = Carbon::now()->subYear()->startOfYear();
           $l_end = Carbon::now()->subYear()->endOfYear();
           $last_year = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereBetween('date_time', [$l_start, $l_end])->count();

           $m_start = Carbon::now()->subMonth()->startOfMonth();
           $m_end = Carbon::now()->subMonth()->endOfMonth();


           $last_month = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereBetween('date_time', [$m_start, $m_end])->count();



            $time_sloat = DB::table('time_slot')->where('doctor_id',Auth::user()->userDetailsId)->first();

            $mor_start = new \DateTime($time_sloat->mrg_from_time);
            $mor_end = new \DateTime($time_sloat->mrg_to_time);
            $mor_timediff = $mor_start->diff($mor_end);

            $aft_start = new \DateTime($time_sloat->aft_from_time);
            $aft_end = new \DateTime($time_sloat->aft_to_time);
            $aft_timediff = $aft_start->diff($aft_end);

            $eve_start = new \DateTime($time_sloat->eve_from_time);
            $eve_end = new \DateTime($time_sloat->eve_to_time);
            $eve_timediff = $eve_start->diff($eve_end);

            $mor_total_sloat =   ($mor_timediff->h * 60) /  $time_sloat->booking_time_slot;
            $aft_total_sloat =  ($aft_timediff->h * 60) /  $time_sloat->booking_time_slot;
            $eve_total_sloat =  ($eve_timediff->h * 60) /  $time_sloat->booking_time_slot;


            $startTimeStamp = strtotime($c_year.'-01-01');
            $endTimeStamp = strtotime($c_year.'-12-31');
            $timeDiff = abs($endTimeStamp - $startTimeStamp);
            $numberDays = $timeDiff/86400;  // 86400 seconds in one day
            // and you might want to convert to integer
            $numberDays = intval($numberDays);

            $total_booking_sloat = $mor_total_sloat + $aft_total_sloat + $eve_total_sloat;
            $total_week_booking_sloat = $total_booking_sloat * 7;
            $total_year_booking_sloat = $total_booking_sloat * $numberDays;


            $total_jan_booking_sloat = $total_booking_sloat * date('t',strtotime($c_year.'-01-01'));
            $total_fab_booking_sloat = $total_booking_sloat * date('t',strtotime($c_year.'-02-01'));
            $total_mar_booking_sloat = $total_booking_sloat * date('t',strtotime($c_year.'-03-01'));
            $total_apr_booking_sloat = $total_booking_sloat * date('t',strtotime($c_year.'-04-01'));
            $total_may_booking_sloat = $total_booking_sloat * date('t',strtotime($c_year.'-05-01'));
            $total_jun_booking_sloat = $total_booking_sloat * date('t',strtotime($c_year.'-06-01'));
            $total_jul_booking_sloat = $total_booking_sloat * date('t',strtotime($c_year.'-07-01'));
            $total_aug_booking_sloat = $total_booking_sloat * date('t',strtotime($c_year.'-08-01'));
            $total_sep_booking_sloat = $total_booking_sloat * date('t',strtotime($c_year.'-09-01'));
            $total_oct_booking_sloat = $total_booking_sloat * date('t',strtotime($c_year.'-10-01'));
            $total_nov_booking_sloat = $total_booking_sloat * date('t',strtotime($c_year.'-11-01'));
            $total_dec_booking_sloat = $total_booking_sloat * date('t',strtotime($c_year.'-12-01'));

            $this_year = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereBetween('date_time', [date($c_year.'-01-01 H:i:s'),date($c_year.'-12-31 H:i:s')])->count();
           
           $jan = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereBetween('date_time', [date($c_year.'-01-01'), date($c_year.'-01-31')])->count();
           $feb = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereBetween('date_time', [date($c_year.'-02-01'), date($c_year.'-02-29')])->count();
           $mar = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereBetween('date_time', [date($c_year.'-03-01'), date($c_year.'-03-31')])->count();
           $apr = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereBetween('date_time', [date($c_year.'-04-01'), date($c_year.'-04-31')])->count();
           $may = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereBetween('date_time', [date($c_year.'-05-01'), date($c_year.'-05-31')])->count();
           $jun = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereBetween('date_time', [date($c_year.'-06-01'), date($c_year.'-06-31')])->count();
           $jul = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereBetween('date_time', [date($c_year.'-07-01'), date($c_year.'-07-31')])->count();
           $aug = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereBetween('date_time', [date($c_year.'-08-01'), date($c_year.'-08-31')])->count();
           $sep = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereBetween('date_time', [date($c_year.'-09-01'), date($c_year.'-09-31')])->count();
           $oct = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereBetween('date_time', [date($c_year.'-10-01'), date($c_year.'-10-31')])->count();
           $nov = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereBetween('date_time', [date($c_year.'-11-01'), date($c_year.'-11-31')])->count();
           $dec = BookingRequest::where('doctor_name', Auth::user()->userDetailsId)->where('status','!=' , 5)->whereBetween('date_time', [date($c_year.'-12-01'), date($c_year.'-12-31')])->count();
          
           $t_amount = BookingRequest::where('doctor_name',Auth::user()->userDetailsId)->whereBetween('date_time', [date($c_year.'-01-01 H:i:s'),date($c_year.'-12-31 H:i:s')])->sum('f_amount');
           
        if ($all_count || $last_month || $last_year || $this_year) {
             return response()->json(["status" => true,"massage"=>"Year Count Data retrieved successfully","all_count" => $all_count,"last_month" => $last_month,"last_year" => $last_year,"this_year" => $this_year,"jan" => $jan,"feb" => $feb,"mar" => $mar,"apr" => $apr,"may" => $may,"jun" => $jun,"jul" => $jul,"aug" => $aug,"sep" => $sep,"oct" => $oct,"nov" => $nov,"dec" => $dec,"mor_total" =>  $mor_total_sloat,"aft_total" => $aft_total_sloat,"eve_total"=>$eve_total_sloat,"total_booking_slot"=>$total_booking_sloat,"week_booking_slot"=>$total_week_booking_sloat,"total_year_booking_slot"=>$total_year_booking_sloat,"jan_total" => $total_jan_booking_sloat,"feb_total" => $total_fab_booking_sloat,"mar_total" => $total_mar_booking_sloat,"apr_total" => $total_apr_booking_sloat,"may_total" => $total_may_booking_sloat,"jun_total" => $total_jun_booking_sloat,"jul_total" => $total_jul_booking_sloat,"aug_total" => $total_aug_booking_sloat,"sep_total" => $total_sep_booking_sloat,"oct_total" => $total_oct_booking_sloat,"nov_total" => $total_nov_booking_sloat,"dec_total" => $total_dec_booking_sloat,"total_earnings"=>$t_amount]);
        }else{
            return response()->json(["status" => false,"data" => '']);
        }
      }

    /**
     * @param CreateDoctorsAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/doctors",
     *      summary="Store a newly created Doctors in storage",
     *      tags={"Doctors"},
     *      description="Store Doctors",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Doctors that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Doctors")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Doctors"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */

    public function send_otp(Request $request)
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://sms.bulksmsind.in/sendSMS?username=haspatal&message=".urlencode('Your OTP ').$request['otp']."&sendername=HSPTAL&smstype=TRANS&numbers=".$request['mobile']."&apikey=206f20fc-d4d5-4a33-ad06-10f453b8ab4a",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => true,
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
            //echo "cURL Error #:" . $err;
            return response()->json(["status" => true,"message" => "OTP Not Send","data"=>$err]);
        } else {
            //echo $response;exit();
            return response()->json(["status" => true,"message" => "Send OTP in your mobile no"]);
        }
    }

    public function doctor_create_otp(Request $request)
    {
        $check = Doctors::where('main_mobile',$request['main_mobile'])->orWhere('email',$request['email'])->first();

        $validator = Validator::make($request->all(), [
              'email' => 'required|email|unique:doctor_details,email',
              'main_mobile' => 'required|unique:doctor_details,main_mobile',
        ]);
        if ($validator->fails()) {
          return response()->json(['error'=>$validator->errors(),"data"=>$check]);
        }


        if (!empty($check)) {
            return response()->json(["status" => true,"message" => "OTP Not Send","data"=>$check ]);
        }
        
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://sms.bulksmsind.in/sendSMS?username=haspatal&message=".$request['otp'].urlencode(' is the secret OTP for Secure Doctor Registration at Haspatal. Valid for 30 mins.')."&sendername=HSPTAL&smstype=TRANS&numbers=".$request['main_mobile']."&apikey=206f20fc-d4d5-4a33-ad06-10f453b8ab4a",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => true,
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
            //echo "cURL Error #:" . $err;
            return response()->json(["status" => false,"message" => "OTP Not Send","data"=>$err]);
        } else {
            //echo $response;exit();
            return response()->json(["status" => true,"message" => "Send OTP in your mobile no"]);
        }


    }


    public function store(Request $request)
    {
         $input = $request->all();
         $validator = Validator::make($input, [
        //   'email' => 'required|email|unique:doctor_details,email',
        //   'main_mobile' => 'required|unique:doctor_details,main_mobile',
         'email' => 'required|email|unique:users,email',
         'main_mobile' => 'required|unique:users,mobile',
          
        ]);


    /* $validator = Validator::make($request->all(), [
      'username' => 'required|min:4|max:16|unique:users,username',
      'phone_no' => 'required|unique:users,phone_no',
      //'first_name' => 'required',
      //'last_name' => 'required',
      'email' => 'email|unique:users,email',
      'password' => 'required',
      //'gender' => 'required',
      'c_password' => 'required|same:password',
    ]);*/

    if ($validator->fails()) {
      return response()->json(['error'=>'Email or Phone Already used']);
    }

        // if ($request->has('language')) {
        //     # code...
        //     $tags = $request->input('language');

        //     $tags1 = implode(',', $tags);
            
        //     $input['language'] = $tags1;
        // }

        // if($request->hasfile('licence_copy'))
        // {
        //     $image = $request->file('licence_copy');
        //     $extension = $image->getClientOriginalExtension(); // getting image extension
        //     $filename =time().'__'.$image->getClientOriginalName();
        //     $path = public_path('/media/licence_copy/');
        //     $image->move($path, $filename);
        //     $input['licence_copy'] = $filename;
        // }
        // if($request->hasfile('profile_pic'))
        // {
        //     $image1 = $request->file('profile_pic');
        //     $extension1 = $image1->getClientOriginalExtension(); // getting image extension
        //     $filename1 =time().'__'.$image1->getClientOriginalName();
        //     $path1 = public_path('/media/profile_pic/');
        //     $image1->move($path1, $filename1);
        //     $input['profile_pic'] = $filename1;
        // }

        $d_data =  DB::table('doctor_details')->orderBy('id', 'desc')->first();
        if($d_data->d_uni_id != ''){
          $bookdata = explode('-', $d_data->d_uni_id); 
          $dd = $bookdata['2'] + 1;   
          $input['d_uni_id'] = $bookdata['0'].'-'.$bookdata['1'].'-'.'000000'.$dd;
        }else{
          $input['d_uni_id'] = '91-D-0000001';
        }
        
       /* $doctors = $this->doctorsRepository->create($input);
        $curl = curl_init();

            curl_setopt_array($curl, array(
              CURLOPT_URL => "https://api-us.cometchat.io/v2.0/users",
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 30,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "POST",
              CURLOPT_POSTFIELDS => array(
                "name"=>$request['first_name'],
                "uid"=> 'd'.rand(1,99).$request['first_name']
              ),
              CURLOPT_HTTPHEADER => array(
                "apikey: f0ead65846606835fc6b114e7e1ac2d9e496ab41",
                "appid: 1583130789928e2",
              ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            if ($err) {
              echo "cURL Error #:" . $err;
            } else {
              $result = json_decode($response);
              /*echo "<pre>";
              print_r($result->data->uid);
              exit;*/
            //} 


            $doctors = $this->doctorsRepository->create($input);

            $user = User::create(['first_name' => $request['first_name'],
                                  'last_name' => $request['last_name'],
                                  'email' => $request['email'],
                                  'mobile' => $request['main_mobile'],
                                  'role_id' => 3,
                                  'user_type' => 1,
                                  'userDetailsId'=>$doctors->id,
                                  "uid"=> 'd'.rand(1,99).str_replace(' ', '', $request['first_name']),
                                  'doctor_login_status'=> 0,
                                  'password' => bcrypt($request->password),
                                  ]);
            $user->attachRole(3);

            //Mail::to($request['email'])->send(new WelcomeMail($user));
            $this->send_mail($user);
            $data = User::where('id', $user->id)
        ->update(['reg_status' => '11']);
        if ($doctors) {
            return response()->json(["status" => true,"massage"=>"Doctors saved successfully","data" => $doctors,"user" => $user]);
        }else{
            return response()->json(["status" => false,"data" => $doctors]);
        }

        //return $this->sendResponse($doctors->toArray(), 'Doctors saved successfully');
    }

    public function doctorregister(Request $request)
    {
        $users = User::where('id', $request->user_id)->orderBy('id', 'desc')
        ->first();
        $userdetailedid= $users->userDetailsId;
        $doctor =  DB::table('doctor_details')->where('id', $userdetailedid)->orderBy('id', 'desc')->first();
        // return $doctor ;
        // $users = User::where('userDetailsId', $request->userDetailsId)->orderBy('id', 'desc')
        // ->first();
        
        if( $doctor){
            $input = $request->all();
            $id = $doctor->id;
            // if ($request->has('language')) {
            //     # code...
            //     $tags = $request->input('language');
    
            //     $tags1 = implode(',', $tags);
                
            //     $input['language'] = $tags1;
            // }
    
            if($request->hasfile('licence_copy'))
            {
                $image = $request->file('licence_copy');
                $extension = $image->getClientOriginalExtension(); // getting image extension
                $filename =time().'__'.$image->getClientOriginalName();
                $path = public_path('/media/licence_copy/');
                $image->move($path, $filename);
                $input['licence_copy'] = $filename;
            }
            if($request->hasfile('profile_pic'))
            {
                $image1 = $request->file('profile_pic');
                $extension1 = $image1->getClientOriginalExtension(); // getting image extension
                $filename1 =time().'__'.$image1->getClientOriginalName();
                $path1 = public_path('/media/profile_pic/');
                $image1->move($path1, $filename1);
                $input['profile_pic'] = $filename1;
            }
            if ($request->dspecialities) {
            $input['speciality']=implode(',', $request->dspecialities);
             }
             if ($request->language) {
            $input['language']=implode(',', $request->language);
             }
             
            
            $doctors = $this->doctorsRepository->update($input, $id);
            // return 
            if ($request->primarymobile) {
                $doctors-> primarymobile=$request->primarymobile;
             }
             if ($request->alternatemobile) {
                $doctors-> alternatemobile=$request->alternatemobile;
             }
             if ($request->district) {
                $doctors-> district=$request->district;
             }
             if ($request->set_price) {
                $doctors-> price=$request->set_price;
             }
            
            
            $doctors->save();  
            if( $users){
                $users->reg_status=$request->reg_status;;
                $users->save();  
                if($users->reg_status==9){
                    $beforeapproval = $this->send_mailbeforeapproval($doctors);
                    // return  $doctors;
                }
                
            }
            if ($doctors) {
                return response()->json(["status" => true,"massage"=>"Doctors saved successfully","data" => $doctors]);
            }else{
                return response()->json(["status" => false,"data" => $doctors]);
            }
        }else{
            return response()->json(["status" => true,"massage"=>"Permission Not allowed",]);
        }
        

    }

    public function send_mailbeforeapproval($doctors)
    
    {
       
               $subject =
               "Your Haspatal Doctor Registration is under Approval Process";
        // $mailmessage= $content;
        $mailmessage= '<!DOCTYPE html>
        <html lang="en">
            <head>
                <title>Dr email template</title>
                <meta charset="utf-8">
                <!-- CSS only -->
                <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
                <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
                <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900,900i&display=swap" rel="stylesheet">
                <script src="https://kit.fontawesome.com/fa5e54101c.js" crossorigin="anonymous"></script>
                <!-- JavaScript Bundle with Popper -->
        
                <!-- Theme Style -->
                <!-- <link rel="stylesheet" href="/assets/css/bootstrap.min.css"> -->
                <style>
                    #page-wrap{
                        max-width: 800px; 
                        margin: 0 auto;
                        font-family: "Roboto";
                        /* padding: 25px 0; */
                    }
                    p,h5,h6{
                        color: #000;
                        font-family: "Roboto";
                        margin: 0;
                    }
                    p span{
                        display: inline-block;
                    }
                </style>
            </head>
            <body>
                <div id="page-wrap">
                    <div class="top">
                    
                    <h6 style="margin-bottom: 25px; font-weight:400; font-size: 16px;">Dear Dr. '.$doctors->first_name.', </h6>
                        <p style="margin-bottom: 25px;">We thank you for registering on Haspatal MD App. Your application is under approval process.  </p>
                        <p style="margin-bottom: 25px; margin-top: 25px;">Our approval team may contact you for additional information if needed.

                        <p style="margin-bottom: 25px; margin-top: 25px;"">For any query, please contact Haspatal Support Team at: +91-124-405 56 56 or email us at support@haspatal.com</p>
                        <p style="margin-bottom: 25px;  margin-top: 25px;"">Team Haspatal MD</p>
                    </div>
                </div>
            </body>
        </html>
        
        
'                 ;
        $data['personalizations'] = array("0" => array('to' => array('0' => array('email' => $doctors->email))));

        $data['from'] = array('email' => 'system@haspatal.com');

        $data['subject'] = $subject;
        $data['content'] = array("0" =>array('type' =>' text/html','value' => $mailmessage ));

        //echo json_encode($data);exit();

        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://api.sendgrid.com/v3/mail/send",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => json_encode($data),
          CURLOPT_HTTPHEADER => array(
            "authorization: Bearer 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",
            "cache-control: no-cache",
            "content-type: application/json",
            "postman-token: b8c0781f-af48-90cc-1c5b-76f80e0b4be0"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            return "Send OTP in your emailId";
        }
    }

    public function send_mail($user)
    {
        $year = date('Y');
        $data['personalizations'] = array("0" => array('to' => array('0' => array('email' => $user->email)),"dynamic_template_data"=> array("name"=>$user->first_name,"year"=> $year)));

        $data['from'] = array('email' => 'system@haspatal.com');

        //$data['subject'] = "Registration successful";
        $data['template_id'] = "d-c19ea64f41ea4c48ae28b3625202ed9e";
        //$data['content'] = array("0" =>array('type' =>' text/html','value' => "<DOCTYPE html><html lang='en-US'><head><meta charset='utf-8'></head><body><h2>Hi ".$user->first_name.", we’re glad you’re here! Following are your account details: <br> </h3> <h3>Email: </h3><p>".$user->email."</p> </body> </html>" ));

        //echo json_encode($data);exit();

        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://api.sendgrid.com/v3/mail/send",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => json_encode($data),
          CURLOPT_HTTPHEADER => array(
            "authorization: Bearer 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",
            "cache-control: no-cache",
            "content-type: application/json",
            "postman-token: b8c0781f-af48-90cc-1c5b-76f80e0b4be0"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          echo "cURL Error #:" . $err;
        } else {
          return $response;
        }
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/doctors/{id}",
     *      summary="Display the specified Doctors",
     *      tags={"Doctors"},
     *      description="Get Doctors",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Doctors",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Doctors"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */

     public function doctor_profile_data($id)
    {
        /** @var Doctors $doctors */
        // $doctors = $this->doctorsRepository->find($id);
         $doctors = Doctors::join('specilities', 'doctor_details.speciality', '=', 'specilities.id')
            ->join('language', 'doctor_details.language', '=', 'language.id')
            ->select('doctor_details.*', 'specilities.specility as specility_name', 'language.language as language_name')
            ->where('doctor_details.id',$id)
            ->get();
            $admin_price = DB::table('booking_price_admin')->where('id',7)->select('booking_price_admin.book_price')->first();
         $path = array("licence_copy"=>env('APP_URL').'media/licence_copy',
                            "profile_pic"=>env('APP_URL').'media/profile_pic',
                            "clinic_logo"=>env('APP_URL').'media/clinic_logo'
                        );

        if ($doctors) {
            return response()->json(["status" => true,"massage"=>"Doctors retrieved successfully","data" => $doctors,"path" => $path,"admin_price"=> $admin_price]);
        }else{
            return response()->json(["status" => false,"data" => $doctors]);
        }
  }

  public function licence_tyes()
    {
        /** @var Doctors $doctors */
        $licenceTyes= DB::table('licence_type')->get();
        
        if ($licenceTyes) {
            return response()->json(["status" => true,"massage"=>"licence Tyes retrieved successfully","data" => $licenceTyes]);
        }else{
            return response()->json(["status" => false,"data" => $doctors]);
        }
  }
    public function show($id)
    {
        /** @var Doctors $doctors */
        $doctors = $this->doctorsRepository->find($id);
        $doctors['speciality_name'] = array();
        $doctors['language_name'] = array();
        /*echo "<pre>";
        print_r($doctors->toArray()); exit;*/
        $d = explode(',',$doctors->speciality);
        $d1 = explode(',',$doctors->language);

        $doctors['speciality_name'] = DB::table('specilities')->whereIn('id',$d)->get();
        $doctors['language_name'] = DB::table('language')->whereIn('id',$d1)->get();
        
        $admin_price = DB::table('booking_price_admin')->where('id',7)->select('booking_price_admin.book_price')->first();

        $path = array("licence_copy"=>env('APP_URL').'media/licence_copy',
                            "profile_pic"=>env('APP_URL').'media/profile_pic',
                            "clinic_logo"=>env('APP_URL').'media/clinic_logo'
                        );

        //return $this->sendCustome($result->toArray(), 'Doctors retrieved successfully',$path);
     if ($doctors) {
            return response()->json(["status" => true,"massage"=>"Doctors retrieved successfully","data" => $doctors,"path" => $path,"admin_price"=> $admin_price]);
        }else{
            return response()->json(["status" => false,"data" => $doctors]);
        }

  }
  public function current_balance(Request $request)
    {
       // $doctor = Auth::user()->userDetailsId; 
        $doctor=$request->userDetailsId;
        $t_amount = BookingRequest::where('doctor_name',$doctor)->where('call_status', '=',9)->sum('f_amount');
        $total_fund =  DB::table('patient_add_fund')->where('doctor_id', $doctor)->first();
        $dr_amount = Doctor_withdraw::where('doctor_id',$doctor)->sum('requested_amount');
         if(!empty($total_fund))
        {
        $t_fund=$total_fund->dr_amount;
        }
        else
        {
            $t_fund=0;
        }
        $amount = $t_amount + $t_fund - $dr_amount;       
      
       if ($amount  || $dr_amount || $t_amount) {
            return response()->json(["status" => true,"massage"=>"amount retrieved successfully","amount" => $amount,'withdraw'=>$dr_amount,'earnings'=>$t_amount]);
        }else{
            return response()->json(["status" => false,"amount" => $amount]);
        }
    }

    /**
     * @param int $id
     * @param UpdateDoctorsAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/doctors/{id}",
     *      summary="Update the specified Doctors in storage",
     *      tags={"Doctors"},
     *      description="Update Doctors",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Doctors",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Doctors that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Doctors")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Doctors"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, Request $request)
    {
        $input = $request->all();
        /** @var Doctors $doctors */
        $doctors = $this->doctorsRepository->find($id);


        if ($request->has('language')) {
            # code...
            $tags = $request->input('language');

            $tags1 = implode(',', $tags);
            
            $input['language'] = $tags1;
        
        }
        if ($request->has('speciality')) {
            # code...
            $tags2 = $request->input('speciality');

            $tags12 = implode(',', $tags2);
            
            $input['speciality'] = $tags12;
        
        }

        if($request->hasfile('licence_copy'))
        {
            $image = $request->file('licence_copy');
            $extension = $image->getClientOriginalExtension(); // getting image extension
            $media_photos =time().'__'.$image->getClientOriginalName();
            $path = public_path('/media/licence_copy/');
            $image->move($path, $media_photos);
            $input['licence_copy']= $media_photos;

        }
        else if($request->licence_copy){
            $input['licence_copy']=$request->licence_copy;
        }else{
            $input['licence_copy']=$doctors->licence_copy;
        }
        if($request->hasfile('profile_pic'))
        {
            $image = $request->file('profile_pic');
            $extension = $image->getClientOriginalExtension(); // getting image extension
            $media_photos =time().'__'.$image->getClientOriginalName();
            $path = public_path('/media/profile_pic/');
            $image->move($path, $media_photos);
            $input['profile_pic']= $media_photos;

        }
        else if($request->profile_pic){
            $input['profile_pic']=$request->profile_pic;
        }
        else{
            $input['profile_pic']=$doctors->profile_pic;
        } 
        if($request->hasfile('clinic_logo'))
        {
            $image = $request->file('clinic_logo');
            $extension = $image->getClientOriginalExtension(); // getting image extension
            $media_photos =time().'__'.$image->getClientOriginalName();
            $path = public_path('/media/clinic_logo/');
            $image->move($path, $media_photos);
            $input['clinic_logo']= $media_photos;

        }else{
            $input['clinic_logo']=$doctors->clinic_logo;
        }
        if($request->hasfile('cheque_img'))
        {
            $image = $request->file('cheque_img');
            $extension = $image->getClientOriginalExtension(); // getting image extension
            $media_photos =time().'__'.$image->getClientOriginalName();
            $path = public_path('/media/cheque_img/');
            $image->move($path, $media_photos);
            $input['cheque_img']= $media_photos;

        }else{
            $input['cheque_img']=$doctors->cheque_img;
        }
        if($request->hasfile('clinic_wallpapper'))
        {
            $image = $request->file('clinic_wallpapper');
            $extension = $image->getClientOriginalExtension(); // getting image extension
            $media_photos =time().'__'.$image->getClientOriginalName();
            $path = public_path('/media/clinic_wallpapper/');
            $image->move($path, $media_photos);
            $input['clinic_wallpapper']= $media_photos;

        }else{
            $input['clinic_wallpapper']=$doctors->clinic_wallpapper;
        }

        

        if (empty($doctors)) {
            return $this->sendError('Doctors not found');
        }

       if ($request->has('price')) {
            
            $input['price']= $request->price;
            
            $user = DB::table('users')->where('user_type',1)->where('userDetailsId', Auth::user()->userDetailsId)->update(['doctor_login_status_mobile'=> 1]);

            /*$dr_old_price = DB::table('doctor_details')->where('id',Auth::user()->userDetailsId)->orderBy('id', 'desc')->first(); 
            
            if ($dr_old_price->price == $request->price) {
            }else{

                $admin_price = DB::table('booking_price_admin')->orderBy('id', 'desc')->first();
                $input['price']= $request->price + $admin_price->book_price; 
            }*/


        }
        

        if ($request->has('plan_id')) {
            $input['plan_id']= $request->plan_id;

            if (!empty($request->gst_number)) {
                
                $input['gst_number'] = $request->gst_number;
            }else{
                $input['gst_number'] =  '';
            }
            

            $buy_plan = Buy_plan_dr::insert([ 'plan_id' => $input['plan_id'],'payable_amount'=>$request->payable_amount,'promo_code'=> $request->promo_code,'discount'=>$request->discount,'gst_amount'=>$request->gst_amount,'final_amount'=>$request->final_amount,'gst_number'=>$input['gst_number'],'payment_id'=>$request->payment_id,'payment_status'=>$request->payment_status,'user_id'=>$request->user_id]);
            
            $user = DB::table('users')->where('user_type',1)->where('userDetailsId', $id)->update(['doctor_plan_status'=> 1,'plan_id'=> $request->plan_id]);
        }
        
        $doctors = $this->doctorsRepository->update($input, $id);

        return $this->sendResponse($doctors->toArray(), 'Your doctor profile updated successfully !');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/doctors/{id}",
     *      summary="Remove the specified Doctors from storage",
     *      tags={"Doctors"},
     *      description="Delete Doctors",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Doctors",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Doctors $doctors */
        $doctors = $this->doctorsRepository->find($id);

        if (empty($doctors)) {
            return $this->sendError('Doctors not found');
        }

        $doctors->delete();

        return $this->sendSuccess('Doctors deleted successfully');
    }
    public function doctors_list_speciality(Request $request)
    {
        $doctors = Doctors::where('status',0);
                    
        // if ($request->language != 0) {
        //     $doctors->where('language', 'LIKE', '%'.$request->language.'%');
        // }

        if ($request->speciality != 0) {
            $doctors->where('speciality', 'LIKE', '%'.$request->speciality.'%');
        }

        if ($request->hospital != 0) {
            $doctors->where('created_by',$request->hospital);
        }

        // if ($request->city != 0) {
        //     $doctors->where('city',$request->city);
        // }

        $results = $doctors->get();
        //echo "<pre>";print_r($results);exit;

        $admin_price = DB::table('booking_price_admin')->where('id',7)->select('booking_price_admin.book_price')->first();
        
        
        $data = array();
        foreach ($results as $value) {

            $speciality_name = array();
            $language_name = array();
            /*echo "<pre>";
            print_r($doctors->toArray()); exit;*/
            $d = explode(',',$value->speciality);
            $d1 = explode(',',$value->language);

            $speciality_name = DB::table('specilities')->whereIn('id',$d)->get();
            $language_name = DB::table('language')->whereIn('id',$d1)->get();

            $data[] = array("id"=> $value->id,"d_uni_id"=> $value->d_uni_id,"first_name"=> $value->first_name,"last_name"=> $value->last_name,"email"=> $value->email,"bio"=> $value->bio,"pincode"=> $value->pincode,"state"=> $value->state,"city"=> $value->city,"address"=>$value->address,"main_mobile"=> $value->main_mobile,"second_mobile"=> $value->second_mobile,"clinic_team_leader"=> $value->clinic_team_leader,"clinic_admin_mobile_no"=> $value->clinic_admin_mobile_no,"clinic_email"=> $value->clinic_email,"experience"=> $value->experience,"language"=> $value->language,"speciality"=> $value->speciality,"price"=> $value->price,"licence_type"=> $value->licence_type,"licence_no"=> $value->licence_no,"valide_upto"=> $value->valide_upto,"issued_by"=> $value->issued_by,"licence_copy"=> $value->licence_copy,"profile_pic"=> $value->profile_pic,"clinic_logo"=> $value->clinic_logo,"bank_name"=> $value->bank_name,"account_name"=> $value->account_name,"ifsc_code"=> $value->ifsc_code,"account_no"=> $value->account_no,"cheque_img"=> $value->cheque_img,"watsapp_number"=> $value->watsapp_number,"youtube_link"=> $value->youtube_link,"teligram_number"=> $value->teligram_number,"status"=> $value->status,"step"=>  $value->step,"created_by"=> $value->created_by,"updated_by"=> $value->updated_by,"created_at"=> $value->created_at,"updated_at"=> $value->updated_at,"year_of_graduation"=> $value->year_of_graduation,"appointment_type"=> $value->appointment_type,"clinic_wallpapper"=> $value->clinic_wallpapper,"clinic_name"=> $value->clinic_name,"clinic_buisness_name"=> $value->clinic_buisness_name,"pan_number"=> $value->pan_number,"speciality_name"=>$speciality_name,"language_name"=>$language_name);
        }                        
        $path = array("licence_copy"=>env('APP_URL').'media/licence_copy',
        "profile_pic"=>env('APP_URL').'media/profile_pic',
        "clinic_logo"=>env('APP_URL').'media/clinic_logo'
    );
        // $path = array("licence_copy"=>env('APP_URL').'public/media/licence_copy',
        //                 "profile_pic"=>env('APP_URL').'public/media/profile_pic',
        //                 "clinic_logo"=>env('APP_URL').'public/media/clinic_logo'
        //             );

    //return $this->sendCustome($result->toArray(), 'Doctors retrieved successfully',$path);
    if ($results) {
        return response()->json(["status" => true,"massage"=>"Doctors retrieved successfully","data" => $data,"path" => $path,'admin_price'=>$admin_price]);
    }else{
        return response()->json(["status" => false,"data" => $data]);
    }

}
public function check_liciendate(Request $request)
{
    $Doctors = Doctors::findOrFail($request->input('doctor_id'));
    $status=$request->input('status');
    $Doctorslist = Doctors::where('id', $request->input('doctor_id'))->where('email_send_date', $request->input('email_send_date'))
    ->first();
    if($Doctorslist){
        return response()->json(["status" => false,"data" => "Alert : Your Doctor License Expired, Kindly update your updated License "]);  
    }else{

        if($status==1)  {
            $subject =
            "
            Your Doctor License Will Expire Soon ";
              $msg ="Your Doctor License Will Expire Soon’, Kindly update your updated License";
           }  
           if($status==2)  {
            $subject =
            "
            Your Doctor License License Expired ";
            $msg ="Your Doctor License Expired, Kindly update your updated License ";
    
        }   
      
        // dd( $user);
        $mailmessage= '<!DOCTYPE html>
        <html lang="en">
            <head>
                <title>Dr email template</title>
                <meta charset="utf-8">
                <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
                <!-- CSS only -->
                <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
                <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900,900i&display=swap" rel="stylesheet">
                <script src="https://kit.fontawesome.com/fa5e54101c.js" crossorigin="anonymous"></script>
                <!-- JavaScript Bundle with Popper -->
                <style>
                    #page-wrap{
                        max-width: 800px;
                        margin: 0 auto;
                        font-family: "Roboto";
                       
                    }
                    p,h5,h6{
                        color: #000;
                    }
                </style>
                <!-- Theme Style -->
                <link rel="stylesheet" href="css/print.css">
                <!-- <link rel="stylesheet" href="/assets/css/bootstrap.min.css"> -->
            </head>
            <body>
                <div id="page-wrap">
                    <div class="top">
                    
                        <h6 style="margin-bottom: 25px; font-weight:400;  font-size: 16px;">Hi , '.$Doctors->first_name.'  !.</h6>
                        <p>'.$msg.'
    
                        </p>
                        <p style="margin-bottom: 25px;">With Regards,</p>
            
                        <h5 style="font-size: 16px; margin-bottom: 0;">TEAM HASPATAL MD</h5>
                        <p style="margin-bottom: 40px;"><b>PHONE:</b>+91-124-405 56 56</p>
            
                        <h5 style="font-size: 16px;">IMPORTANT:-</h5>
                        <p><b>We have uploaded support videos for our Doctors at following 
                        <a href="https://www.youtube.com/playlist?list=PLb9kI_cIEaQMq43XlL7Gd0ch2Ve_KQfyJ">link</a>
                        
                            You may visit here 24*7 and find useful information</b>
                        </p>
                    </div>
                </div>
            </body>
        </html>
        
    '                 ;
        $data['personalizations'] = array("0" => array('to' => array('0' => array('email' => $Doctors->email))));
    
        $data['from'] = array('email' => 'system@haspatal.com');
    
        $data['subject'] = $subject;
        $data['content'] = array("0" =>array('type' =>' text/html','value' => $mailmessage ));
    
        //echo json_encode($data);exit();
    
        $curl = curl_init();
    
        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://api.sendgrid.com/v3/mail/send",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => json_encode($data),
          CURLOPT_HTTPHEADER => array(
            "authorization: Bearer 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",
            "cache-control: no-cache",
            "content-type: application/json",
            "postman-token: b8c0781f-af48-90cc-1c5b-76f80e0b4be0"
          ),
        ));
    
        $response = curl_exec($curl);
        $err = curl_error($curl);
    
        curl_close($curl);
    
        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            return response()->json(["status" => false,"data" => "Your Doctor License Expired, Kindly update your updated License "]); 
        } 
        


    }
    
    

}
    
}
