<?php



namespace App\Http\Controllers\API;



use App\Http\Requests\API\CreateImaging_listAPIRequest;

use App\Http\Requests\API\UpdateImaging_listAPIRequest;

use App\Models\Imaging_list;

use App\Repositories\Imaging_listRepository;

use Illuminate\Http\Request;

use App\Http\Controllers\AppBaseController;

use Response;
use Auth;


/**

 * Class Imaging_listController

 * @package App\Http\Controllers\API

 */



class Imaging_listAPIController extends AppBaseController

{

    /** @var  Imaging_listRepository */

    private $imagingListRepository;



    public function __construct(Imaging_listRepository $imagingListRepo)

    {

        $this->imagingListRepository = $imagingListRepo;

    }



    /**

     * Display a listing of the Imaging_list.

     * GET|HEAD /imagingLists

     *

     * @param Request $request

     * @return \Illuminate\Http\JsonResponse|Response

     */

    public function index(Request $request)

    {

        $imagingLists = $this->imagingListRepository->all(

            $request->except(['skip', 'limit']),

            $request->get('skip'),

            $request->get('limit')

        );



        return $this->sendResponse($imagingLists->toArray(), 'Imaging Lists retrieved successfully');

    }



    /**

     * Store a newly created Imaging_list in storage.

     * POST /imagingLists

     *

     * @param CreateImaging_listAPIRequest $request

     *

     * @return \Illuminate\Http\JsonResponse|Response

     */

    public function store(CreateImaging_listAPIRequest $request)

    {

        $input = $request->all();
         $input['created_by']= Auth::user()->id;
        $input['updated_by']= Auth::user()->id;


        $imagingList = $this->imagingListRepository->create($input);
        $imagingList->dr_id =     $request->dr_id;
       
        $imagingList->save();
 


        return $this->sendResponse($imagingList->toArray(), 'Imaging List saved successfully');

    }



    /**

     * Display the specified Imaging_list.

     * GET|HEAD /imagingLists/{id}

     *

     * @param int $id

     *

     * @return \Illuminate\Http\JsonResponse|Response

     */

    public function show($id)

    {

        /** @var Imaging_list $imagingList */

        // $imagingList = $this->imagingListRepository->find($id);
        $imagingList = Imaging_list::select('*')->where('dr_id', $id)->get();


        if (empty($imagingList)) {

            return $this->sendError('Imaging List not found');

        }



        return $this->sendResponse($imagingList->toArray(), 'Imaging List retrieved successfully');

    }



    /**

     * Update the specified Imaging_list in storage.

     * PUT/PATCH /imagingLists/{id}

     *

     * @param int $id

     * @param UpdateImaging_listAPIRequest $request

     *

     * @return \Illuminate\Http\JsonResponse|Response

     */

    public function update($id, UpdateImaging_listAPIRequest $request)

    {

        $input = $request->all();



        /** @var Imaging_list $imagingList */

        $imagingList = $this->imagingListRepository->find($id);



        if (empty($imagingList)) {

            return $this->sendError('Imaging List not found');

        }



        $imagingList = $this->imagingListRepository->update($input, $id);



        return $this->sendResponse($imagingList->toArray(), 'Imaging_list updated successfully');

    }



    /**

     * Remove the specified Imaging_list from storage.

     * DELETE /imagingLists/{id}

     *

     * @param int $id

     *

     * @throws \Exception

     *

     * @return \Illuminate\Http\JsonResponse|Response

     */

    public function destroy($id)

    {

        /** @var Imaging_list $imagingList */

        $imagingList = $this->imagingListRepository->find($id);



        if (empty($imagingList)) {

            return $this->sendError('Imaging List not found');

        }



        $imagingList->delete();



        return $this->sendSuccess('Imaging List deleted successfully');

    }

}

