<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateCoupanAPIRequest;
use App\Http\Requests\API\UpdateCoupanAPIRequest;
use App\Models\Coupan;
use App\coupon;
use App\Repositories\CoupanRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;
use DB;
use Carbon\Carbon;

/**
 * Class CoupanController
 * @package App\Http\Controllers\API
 */

class CoupanAPIController extends AppBaseController
{
    /** @var  CoupanRepository */
    private $coupanRepository;

    public function __construct(CoupanRepository $coupanRepo)
    {
        $this->coupanRepository = $coupanRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/coupans",
     *      summary="Get a listing of the Coupans.",
     *      tags={"Coupan"},
     *      description="Get all Coupans",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Coupan")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $coupans = $this->coupanRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($coupans->toArray(), 'Coupans retrieved successfully');
    }
    public function coupans_check(Request $request)
    {
          $result = coupon::where('promo_code', $request->promo_code)->where('user_id',$request->patient_id)->where('category_d',$request->role_id)->where('status',1)->get()->first();
          
          $result1 = coupon::where('promo_code', $request->promo_code)->where('user_id',$request->patient_id)->where('category_d',$request->role_id)->where('status',1)->get();
          $allcount = DB::table('booking_request')->where('promo_code',$request->promo_code)->where('patient_name',$request->patient_id)->count();
                
                if(!empty($result))
               {
          $startDate =  Carbon::parse($result->created_at)->addMonths(12); 
          $endDate =  Carbon::now()->endOfDay(); 
          $code_used = $result->coupon_limit;
               
               //var_dump($allcount);
            if($code_used>$allcount and $startDate>$endDate)
        {
            return response()->json(["status" => true,"massage"=>"Promocode is valid..","data" => $result]);
        }else{
            return response()->json(["status" => false,"data" => '',"massage" => "Promocode is not valid.."]);
        }
               }
               else
               {
                  return response()->json(["status" => false,"data" => '',"massage" => "Promocode is not valid.."]); 
               }
       

    }

    public function coupans_check_dr(Request $request)
    {
          $result = Coupan::where('promo_code', $request->promo_code)->first();
          $result1 = Coupan::where('promo_code', $request->promo_code)->get();
          $allcount = DB::table('buy_plan_dr')->where('promo_code',$request->promo_code)->where('user_id',$request->dr_id)->count();
          $startDate =  Carbon::parse($result->edate)->startOfDay(); 
          $endDate =  Carbon::now()->endOfDay(); 
          $code_used = $result->used_promo_code_time;
               
        if($code_used > $allcount && $startDate > $endDate)
        {
            return response()->json(["status" => true,"massage"=>"Promocode is valid..","data" => $result1]);
        }else{
            return response()->json(["status" => false,"data" => '',"massage" => "Promocode is not valid.."]);
        }
       

    }

    /**
     * @param CreateCoupanAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/coupans",
     *      summary="Store a newly created Coupan in storage",
     *      tags={"Coupan"},
     *      description="Store Coupan",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Coupan that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Coupan")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Coupan"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateCoupanAPIRequest $request)
    {
        $input = $request->all();

        $coupan = $this->coupanRepository->create($input);

        return $this->sendResponse($coupan->toArray(), 'Coupan saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/coupans/{id}",
     *      summary="Display the specified Coupan",
     *      tags={"Coupan"},
     *      description="Get Coupan",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Coupan",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Coupan"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Coupan $coupan */
        $coupan = $this->coupanRepository->find($id);

        if (empty($coupan)) {
            return $this->sendError('Coupan not found');
        }

        return $this->sendResponse($coupan->toArray(), 'Coupan retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateCoupanAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/coupans/{id}",
     *      summary="Update the specified Coupan in storage",
     *      tags={"Coupan"},
     *      description="Update Coupan",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Coupan",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Coupan that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Coupan")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Coupan"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateCoupanAPIRequest $request)
    {
        $input = $request->all();

        /** @var Coupan $coupan */
        $coupan = $this->coupanRepository->find($id);

        if (empty($coupan)) {
            return $this->sendError('Coupan not found');
        }

        $coupan = $this->coupanRepository->update($input, $id);

        return $this->sendResponse($coupan->toArray(), 'Coupan updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/coupans/{id}",
     *      summary="Remove the specified Coupan from storage",
     *      tags={"Coupan"},
     *      description="Delete Coupan",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Coupan",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Coupan $coupan */
        $coupan = $this->coupanRepository->find($id);

        if (empty($coupan)) {
            return $this->sendError('Coupan not found');
        }

        $coupan->delete();

        return $this->sendSuccess('Coupan deleted successfully');
    }
}
