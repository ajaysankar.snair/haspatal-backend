<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePatientsAPIRequest;
use App\Http\Requests\API\UpdatePatientsAPIRequest;
use App\Models\Patients;
use App\Models\BookingRequest;
use App\User;
use App\Repositories\PatientsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;
use DB;
use Validator;
use Auth;
use Carbon\Carbon;
/**
 * Class PatientsController
 * @package App\Http\Controllers\API
 */

class PatientsAPIController extends AppBaseController
{
    /** @var  PatientsRepository */
    private $patientsRepository;

    public function __construct(PatientsRepository $patientsRepo)
    {
        $this->patientsRepository = $patientsRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/patients",
     *      summary="Get a listing of the Patients.",
     *      tags={"Patients"},
     *      description="Get all Patients",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Patients")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $patients = $this->patientsRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($patients->toArray(), 'Patients retrieved successfully');
    }

    /**
     * @param CreatePatientsAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/patients",
     *      summary="Store a newly created Patients in storage",
     *      tags={"Patients"},
     *      description="Store Patients",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Patients that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Patients")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Patients"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */

    public function patients_create_otp(Request $request)
    {
        $validator = Validator::make($request->all(), [
              'first_name' => 'required',
              //'last_name' => 'required',
              'email' => 'required|email|unique:patient_details,email',
              'mobile' => 'required|unique:patient_details,mobile',
             ]);
        if ($validator->fails()) {
          return response()->json(['error'=>'email or phone number already used']);
        }
        
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://sms.bulksmsind.in/sendSMS?username=haspatal&message=".urlencode('Your OTP ').$request['otp']."&sendername=HSPTAL&smstype=TRANS&numbers=".$request['mobile']."&apikey=206f20fc-d4d5-4a33-ad06-10f453b8ab4a",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => true,
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
            //echo "cURL Error #:" . $err;
            return response()->json(["status" => true,"message" => "OTP Not Send","data"=>$err]);
        } else {
            //echo $response;exit();
            return response()->json(["status" => true,"message" => "Send OTP in your mobile no"]);
        }


    }

    public function store(Request $request)
    {
        $input = $request->all();
        $data = $request->input();
        $mo_prfix="91";
        $email = User::where('email', '=', $data['email'])->first();
        if($email){
           return response()->json(["status" => 1,"massage" => "Email Already Exit"]);  
        }
        $mobile = User::where('mobile', '=',   $data['mobile'])->first();

        if($mobile){
           return response()->json(["status" => 1,"massage" => "Mobile Number Already Exit"]);  
        }
        $emailmobile = User::where('email', '=', $data['email'])->where('mobile', '=',  $data['mobile'])->first();
         if($emailmobile){
            return response()->json(["status" => 1,"massage" => "User Already Registered"]);  
         }
        // $validator = Validator::make($request->all(), [
        //       'first_name' => 'required',
        //       //'last_name' => 'required',
        //       'email' => 'required|email|unique:patient_details,email',
        //       'mobile' => 'required|unique:patient_details,mobile',
        //      ]);
        // if ($validator->fails()) {
        // //   return response()->json(['error'=>'email or phone already used']);
        // return response()->json(["status" => 1,"massage" => "Email Or Phone Number Error"]); 
        // }
        
        /*$curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://sms.bulksmsind.in/sendSMS?username=haspatal&message=".urlencode('Your Registration Successfully')."&sendername=HSPTAL&smstype=TRANS&numbers=".$request['mobile']."&apikey=206f20fc-d4d5-4a33-ad06-10f453b8ab4a",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => true,
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
        echo "cURL Error #:" . $err;
        } else {
        echo $response;exit();
        }*/


        /*$curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://api-us.cometchat.io/v2.0/users",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => array(
            "name"=>$request['first_name'],
            "uid"=> 'p'.rand(1,99).$request['first_name']
          ),
          CURLOPT_HTTPHEADER => array(
            "apikey: f0ead65846606835fc6b114e7e1ac2d9e496ab41",
            "appid: 1583130789928e2",
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          echo "cURL Error #:" . $err;
        } else {
          $result = json_decode($response);
        }*/

        $p_data =  DB::table('patient_details')->orderBy('id', 'desc')->first();
        if($p_data->p_uni_id != ''){
          $bookdata = explode('-', $p_data->p_uni_id); 
          $dd = $bookdata['2'] + 1;   
          $input['p_uni_id'] = $bookdata['0'].'-'.$bookdata['1'].'-'.'000000'.$dd;
        }else{
          $input['p_uni_id'] = '91-P-0000001';
        }



        $patients = $this->patientsRepository->create($input);
        
        $user = User::create(['first_name' => $request['first_name'],
                                  'email' => $request['email'],
                                  'mobile' => $request['mobile'],
                                  'role_id' => 2,
                                  'user_type' => 2,
                                  'userDetailsId'=>$patients->id,
                                  'uid'=> 'p'.rand(1,99).str_replace(' ', '', $request['first_name']),
                                  'password' => bcrypt($request['password']),
                                  ]);
          /*echo "<pre>";
          print_r($user);
          exit;*/
       $user->attachRole(2);
       $patients['token'] =  $user->createToken('LaraPassport')->accessToken;
       $patients['user'] =$user->id;
       $data = User::where('id', $user->id)
        ->update(['reg_status' => '11']);

        $this->send_mail($user);
        return $this->sendResponse($patients->toArray(), 'Patients saved successfully');
    }

    public function send_mail($user)
    {
        $year = date('Y');
        $data['personalizations'] = array("0" => array('to' => array('0' => array('email' => $user->email)),"dynamic_template_data"=> array("name"=>$user->first_name,"year"=> $year)));

        $data['from'] = array('email' => 'system@haspatal.com');

        //$data['subject'] = "Registration successful";
        $data['template_id'] = "d-c19ea64f41ea4c48ae28b3625202ed9e";
        //$data['content'] = array("0" =>array('type' =>' text/html','value' => "<DOCTYPE html><html lang='en-US'><head><meta charset='utf-8'></head><body><h2>Hi ".$user->first_name.", we’re glad you’re here! Following are your account details: <br> </h3> <h3>Email: </h3><p>".$user->email."</p> </body> </html>" ));

        //echo json_encode($data);exit();

        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://api.sendgrid.com/v3/mail/send",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => json_encode($data),
          CURLOPT_HTTPHEADER => array(
            "authorization: Bearer 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",
            "cache-control: no-cache",
            "content-type: application/json",
            "postman-token: b8c0781f-af48-90cc-1c5b-76f80e0b4be0"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          echo "cURL Error #:" . $err;
        } else {
          return $response;
        }
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/patients/{id}",
     *      summary="Display the specified Patients",
     *      tags={"Patients"},
     *      description="Get Patients",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Patients",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Patients"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Patients $patients */
        $patients = $this->patientsRepository->find($id);

        if (empty($patients)) {
            return $this->sendError('Patients not found');
        }
        // return $this->sendCustome($patients->toArray(), 'Patients retrieved successfully',env('APP_URL').'public/media/profile_pic');
        // return $this->sendResponse($patients->toArray(), 'Patients retrieved successfully');
        if ($patients) {
            return response()->json(["status" => true,"massage"=>"Patients Request retrieved successfully","data" => $patients ,"path" =>env('APP_URL').'/public/media/profile_pic']);
        }else{
             return response()->json(["status" => false,"data" => $patients]);
        }
    }

    /**
     * @param int $id
     * @param UpdatePatientsAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/patients/{id}",
     *      summary="Update the specified Patients in storage",
     *      tags={"Patients"},
     *      description="Update Patients",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Patients",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Patients that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Patients")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Patients"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, Request $request)
    {
       // echo "string"; exit;
        $input = $request->all();
  
        if($request->hasfile('profile_pic'))
        {
            $image = $request->file('profile_pic');
            $extension = $image->getClientOriginalExtension(); // getting image extension
            $profile_pic =time().'__'.$image->getClientOriginalName();
            $path = public_path('/media/profile_pic/');
            $image->move($path, $profile_pic);
            $input['profile_pic']= $profile_pic;

        }

        

        /** @var Patients $patients */
        $patients = $this->patientsRepository->find($id);

        if (empty($patients)) {
            return $this->sendError('Patients not found');
        }


        if ($request->has('address')) {
            $user = DB::table('users')->where('user_type',2)->where('userDetailsId', $id)->update(['patient_login_status_addressinfo'=> 1]);
           
        }
        if ($request->has('contact_phone1')) {
            $user = DB::table('users')->where('user_type',2)->where('userDetailsId', $id)->update(['patient_login_status_contactinfo'=> 1]);
        }

        $patients = $this->patientsRepository->update($input, $id);
        if ($request->has('district')) {
        $patients->district=$request->district;
        $patients->save();
        }
        if ($request->has('allergy')) {
            $patients->allergy=$request->allergy;
            $patients->save();
            }
            if ($request->has('zipcode')) {
                $patients->zipcode=$request->zipcode;
                $patients->save();
                }
        if ($request->has('genticdisoder')) {
            $patients->genticdisoder=$request->genticdisoder;
            $patients->save();
            }
            if ($request->has('Dob')) {
                $patients->Dob=$request->Dob;
                $patients->save();
                }
            $status = User::where('userDetailsId', $id)
            ->update(['reg_status' =>  $request->status]);
        return $this->sendResponse($patients->toArray(), 'Patients updated successfully');
    }
    public function patient_queue()
    {
        $allcount = DB::table('booking_request')->where('doctor_name', Auth::user()->userDetailsId)->where('status',1)->whereDate('booking_request.updated_at', Carbon::today())->count();
        $check_count = DB::table('booking_request')->where('doctor_name', Auth::user()->userDetailsId)->where('status',3)->whereDate('booking_request.updated_at', Carbon::today())->count();
        $waiting_count = DB::table('booking_request')->where('doctor_name', Auth::user()->userDetailsId)->where('status',2)->whereDate('booking_request.updated_at', Carbon::today())->count();
        if ($allcount || $check_count || $waiting_count) {
            return response()->json(["status" => true,"massage"=>"Booking Request retrieved successfully","total_appointment" => $allcount,"check" => $check_count,'waiting' => $waiting_count]);
        }else{
             return response()->json(["status" => false,"total_appointment" => $allcount,"check" => $check_count,'waiting' => $waiting_count]);
        }
    }

    public function waiting_room_status_list()
    {
        $doctor_id = Auth::user()->userDetailsId;
        // return $doctor_id;
        $patients = BookingRequest::join('patient_details','booking_request.patient_name','patient_details.id')
                              ->join('users','booking_request.patient_name','users.userDetailsId')
                             ->select('booking_request.*','patient_details.first_name','patient_details.last_name','patient_details.id as patient_id','patient_details.age','patient_details.gender','patient_details.height','patient_details.weight','patient_details.Dob','patient_details.genticdisoder','patient_details.allergy','patient_details.lmp','users.uid as uid')
                              ->where('booking_request.status','=',2)
                              ->where('users.user_type','=',2)
                            //   ->whereDate('booking_request.updated_at', Carbon::today())
                              ->where('booking_request.consult_date', date('d-m-Y'))
                              ->where('booking_request.doctor_name', $doctor_id)
                            //   ->where('booking_request.consult_time', '>',  date("H:i"))
                              ->where('booking_request.call_status',  '!=',  9)
                              ->orderBy('consult_time', 'asc')
                              ->get();
        if ($patients) {
            return response()->json(["status" => true,"data" => $patients, "message" => "Booking Waitting Room Request view successfully"]);
        }else{
            return response()->json(["status" => false,"data" => '' , "message" => "Booking Waitting Room Request not view successfully"]);
        }
    }
    // public function patient_waiting_room_status($id)
    // {
    // //   return date('d-m-Y');
    //     // return $id;
    //     $patients = BookingRequest::join('patient_details','booking_request.patient_name','patient_details.id')
    //                           ->join('users','booking_request.patient_name','users.userDetailsId')
    //                          ->select('booking_request.*','patient_details.first_name','patient_details.last_name','patient_details.id as patient_id','patient_details.age','patient_details.gender','patient_details.height','patient_details.weight','patient_details.lmp','users.uid as uid')
    //                         //   ->where('booking_request.status','=',6)
    //                         //   ->where('users.user_type','=',2)
    //                           ->where('booking_request.consult_date', date('d-m-Y'))
    //                           ->where('booking_request.patient_name', $id)
    //                           ->where('booking_request.consult_time', '>',  date("H:i"))
    //                           ->orderBy('consult_time', 'asc')
    //                           ->get();
    //                         //   return $patients;
    //     if ($patients) {
    //         return response()->json(["status" => true,"data" => $patients, "message" => "Booking Waitting Room Request view successfully"]);
    //     }else{
    //         return response()->json(["status" => false,"data" => '' , "message" => "Booking Waitting Room Request not view successfully"]);
    //     }
    // }
    public function patient_waiting_room_status($id)
    {
        //cho "string"; exit;    
        /** @var BookingRequest $bookingRequest */
       /* $bookingRequest[] = $this->bookingRequestRepository->find($id);*/
         $bookingRequest = BookingRequest::where('booking_request.patient_name',$id) 
                       ->where('booking_request.status','!=',5) 
                        ->join('patient_details', 'booking_request.patient_name', '=', 'patient_details.id')
                        ->join('doctor_details', 'booking_request.doctor_name', '=', 'doctor_details.id')
                        // ->orderBy('booking_request.id','DESC')
                        ->select('booking_request.*','patient_details.first_name as patient','doctor_details.first_name','doctor_details.last_name','doctor_details.profile_pic','doctor_details.clinic_logo')
                        ->where('booking_request.consult_date', date('d-m-Y'))
                              ->where('booking_request.patient_name', $id)
                              ->where('booking_request.consult_time', '>',  date("H:i"))
                              ->where('booking_request.call_status',  '!=',  9)
                              ->orderBy('consult_time', 'asc')
                        ->get(); 
                        foreach ($bookingRequest as $patientrequest) {
                          
                           
                   
                            $to_time = strtotime($patientrequest->consult_time);
                            $from_time = strtotime( date("H:i"));
                            $minutes = round(abs($to_time - $from_time) / 60,2);
                            $patientrequest['status_time']=  $minutes;
                        }
              $path = array("profile_pic"=>env('APP_URL').'/public/media/profile_pic',
                            "clinic_logo"=>env('APP_URL').'/public/media/clinic_logo'
                        );
  
        if ($bookingRequest) {
            return response()->json(["status" => true,"massage"=>"Booking Request retrieved successfully","data" => $bookingRequest,"path" => $path]);
        }else{
             return response()->json(["status" => false,"data" => $bookingRequest]);
        }
    }
    // public function patient_futureappointment($id)
    // {
    // //   return date('d-m-Y');
    //     // return $id;
    //     $patients = BookingRequest::join('patient_details','booking_request.patient_name','patient_details.id')
    //                           ->join('users','booking_request.patient_name','users.userDetailsId')
    //                          ->select('booking_request.*','patient_details.first_name','patient_details.last_name','patient_details.id as patient_id','patient_details.age','patient_details.gender','patient_details.height','patient_details.weight','patient_details.lmp','users.uid as uid')
    //                         //   ->where('booking_request.status','=',6)
    //                         //   ->where('users.user_type','=',2)
    //                           ->where('booking_request.appointment_time',  '>=',  date('Y-m-d'))
    //                           ->where('booking_request.patient_name', $id)
    //                         //   ->where('booking_request.consult_time', '>',  date("H:i"))
    //                           ->orderBy('appointment_time', 'asc')
    //                           ->orderBy('consult_time', 'asc')
    //                           ->get();
    //                         //   return $patients;
    //     if ($patients) {
    //         return response()->json(["status" => true,"data" => $patients, "message" => "Booking Waitting Room Request view successfully"]);
    //     }else{
    //         return response()->json(["status" => false,"data" => '' , "message" => "Booking Waitting Room Request not view successfully"]);
    //     }
    // }
    public function patient_futureappointment($id)
    {
        //cho "string"; exit;    
        /** @var BookingRequest $bookingRequest */
       /* $bookingRequest[] = $this->bookingRequestRepository->find($id);*/
         $bookingRequest = BookingRequest::where('booking_request.patient_name',$id) 
                       ->where('booking_request.status','!=',5) 
                        ->join('patient_details', 'booking_request.patient_name', '=', 'patient_details.id')
                        ->join('doctor_details', 'booking_request.doctor_name', '=', 'doctor_details.id')
                        // ->orderBy('booking_request.id','DESC')
                        ->select('booking_request.*','patient_details.first_name as patient','doctor_details.first_name','doctor_details.last_name','doctor_details.profile_pic','doctor_details.clinic_logo')
                        ->where('booking_request.appointment_time',  '>=',  date('Y-m-d'))
                        ->where('booking_request.patient_name', $id)
                        ->where('booking_request.call_status',  '!=',  9)
                        // ->where('booking_request.consult_time', '>',  date("H:i"))
                        ->orderBy('appointment_time', 'asc')
                        ->orderBy('consult_time', 'asc')
                        ->get();
      
              $path = array("profile_pic"=>env('APP_URL').'/public/media/profile_pic',
                            "clinic_logo"=>env('APP_URL').'/public/media/clinic_logo'
                        );
                        foreach ($bookingRequest as $patientrequest) {
                          
                           
                   
                            $to_time = strtotime($patientrequest->consult_time);
                            $from_time = strtotime( date("H:i"));
                            $minutes = round(abs($to_time - $from_time) / 60,2);
                            $patientrequest['status_time']=  $minutes;
                        }
                        // $bookingRequest['current_date']= date('Y-m-d');
                        // $bookingRequest['current_time']= date("H:i");
        if ($bookingRequest) {
            return response()->json(["status" => true,"massage"=>"Booking Request retrieved successfully","data" => $bookingRequest,"path" => $path,"current_date" =>date('Y-m-d'),"current_time" =>  date("H:i")]);
        }else{
             return response()->json(["status" => false,"data" => $bookingRequest]);
        }
    }
    public function patient_futureappointmenttime($id)
    {
        $current_date=strtotime(date("Y-m-d H:i:s"))+900;//15*60=900 seconds
        $current_date=date("Y-m-d H:i:s",$current_date);
        // return $date;
        //cho "string"; exit;    
        /** @var BookingRequest $bookingRequest */
       /* $bookingRequest[] = $this->bookingRequestRepository->find($id);*/
         $bookingRequest = BookingRequest::where('booking_request.patient_name',$id) 
                       ->where('booking_request.status','!=',5) 
                        ->join('patient_details', 'booking_request.patient_name', '=', 'patient_details.id')
                        ->join('doctor_details', 'booking_request.doctor_name', '=', 'doctor_details.id')
                        // ->orderBy('booking_request.id','DESC')
                        ->select('booking_request.*','patient_details.first_name as patient','doctor_details.first_name','doctor_details.last_name','doctor_details.profile_pic','doctor_details.clinic_logo')
                        // ->where('booking_request.appointment_time',  '>=',  date('Y-m-d'))
                        ->where('booking_request.patient_name', $id)
                        ->where('booking_request.call_status',  '!=',  9)
                        ->where('booking_request.filter_date_time', '>=',  $current_date)
                        ->orderBy('appointment_time', 'asc')
                        ->orderBy('consult_time', 'asc')
                        ->get();
      
              $path = array("profile_pic"=>env('APP_URL').'/public/media/profile_pic',
                            "clinic_logo"=>env('APP_URL').'/public/media/clinic_logo'
                        );
                        foreach ($bookingRequest as $patientrequest) {
                          
                           
                   
                            $to_time = strtotime($patientrequest->consult_time);
                            $from_time = strtotime( date("H:i"));
                            $minutes = round(abs($to_time - $from_time) / 60,2);
                            $patientrequest['status_time']=  $minutes;
                        }
                        // $bookingRequest['current_date']= date('Y-m-d');
                        // $bookingRequest['current_time']= date("H:i");
        if ($bookingRequest) {
            return response()->json(["status" => true,"massage"=>"Booking Request retrieved successfully","data" => $bookingRequest,"path" => $path,"current_date" =>date('Y-m-d'),"current_time" =>  date("H:i")]);
        }else{
             return response()->json(["status" => false,"data" => $bookingRequest]);
        }
    }
    public function updatebookingwait($id)
    {
        $result = BookingRequest::where('book_id', $id)->update(['status' =>2]);
        return response()->json(["status" => true,"data" => $result, "message" => " Waitting Room Request view successfully"]);
    }
    public function stopringtune($id)
    {
        $result = BookingRequest::where('book_id', $id)->update(['call_status' =>11]);
        return response()->json(["status" => true,"data" => $result, "message" => " Waitting Room Request view successfully"]);
    }
    public function booked_room_status_list()
    {
        $doctor_id = Auth::user()->userDetailsId;
        // return $doctor_id;
        $patients = BookingRequest::join('patient_details','booking_request.patient_name','patient_details.id')
                              ->join('users','booking_request.patient_name','users.userDetailsId')
                             ->select('booking_request.*','patient_details.first_name','patient_details.last_name','patient_details.id as patient_id','patient_details.age','patient_details.gender','patient_details.height','patient_details.weight','patient_details.lmp','users.uid as uid')  ->groupBy('booking_request.id')
                            //   ->where('booking_request.status','=',2)
                              ->where('users.user_type','=',2)
                            //   ->whereDate('booking_request.updated_at', Carbon::today())
                              ->where('booking_request.doctor_name', $doctor_id)
                              ->orderBy('appointment_time', 'DESC')
                              ->get();
        if ($patients) {
            return response()->json(["status" => true,"data" => $patients, "message" => "Booking Waitting Room Request view successfully"]);
        }else{
            return response()->json(["status" => false,"data" => '' , "message" => "Booking Waitting Room Request not view successfully"]);
        }
    }
    public function singlemeetinglist(Request $request)
    {
    // return $request;
      $booking_id=$request->input('booking_id');
     
      $doctor_id = Auth::user()->userDetailsId;
      // return $doctor_id;
      $patients = BookingRequest::join('patient_details','booking_request.patient_name','patient_details.id')
                            ->join('users','booking_request.patient_name','users.userDetailsId')
                           ->select('booking_request.*','patient_details.first_name','patient_details.last_name','patient_details.id as patient_id','patient_details.age','patient_details.gender','patient_details.height','patient_details.weight','patient_details.lmp','users.uid as uid')
                            ->where('booking_request.id', $booking_id)
                            ->where('users.user_type','=',2)
                          //   ->whereDate('booking_request.updated_at', Carbon::today())
                            ->where('booking_request.doctor_name', $doctor_id)
                            ->get();
      if ($patients) {
          return response()->json(["status" => true,"data" => $patients, "message" => "Booking Waitting Room Request view successfully"]);
      }else{
          return response()->json(["status" => false,"data" => '' , "message" => "Booking Waitting Room Request not view successfully"]);
      }
    }

    public function checked_room_status_list()
    {
        $doctor_id = Auth::user()->userDetailsId;
        // return $doctor_id;
        $patients = BookingRequest::join('patient_details','booking_request.patient_name','patient_details.id')
                              ->join('users','booking_request.patient_name','users.userDetailsId')
                             ->select('booking_request.*','patient_details.first_name','patient_details.last_name','patient_details.id as patient_id','patient_details.age','patient_details.gender','patient_details.height','patient_details.weight','patient_details.lmp','users.uid as uid')   ->groupBy('booking_request.id')
                             ->where('booking_request.call_status','=',9)
                            //  ->orWhere('booking_request.call_status', '=', 1)
                              ->where('users.user_type','=',2)
                            //   ->whereDate('booking_request.updated_at', Carbon::today())
                              ->where('booking_request.doctor_name', $doctor_id)
                              ->orderBy('appointment_time', 'DESC')
                           
                              ->get();

                            //   ->where(function($q) {
                            //     $q->where('booking_request.call_status','=',9)
                            //       ->orWhere('booking_request.call_status', '=', 1);
                            // })




        if ($patients) {
            return response()->json(["status" => true,"data" => $patients, "message" => "Booking Waitting Room Request view successfully"]);
        }else{
            return response()->json(["status" => false,"data" => '' , "message" => "Booking Waitting Room Request not view successfully"]);
        }
    }
     public function reschedule_appoinment()
    {

        $doctor_id = Auth::user()->userDetailsId;
       // $patients = Patients::join('booking_request','', 'booking_request.patient_name')
        $patients = BookingRequest::join('patient_details','booking_request.patient_name','patient_details.id')
                              ->select('booking_request.*','patient_details.first_name','patient_details.last_name','patient_details.id as patient_id')
                              ->where('booking_request.status',4)
                              ->whereDate('booking_request.updated_at', Carbon::today())
                              ->where('booking_request.doctor_name', $doctor_id)
                              ->get();
 
    if ($patients) {
            return response()->json(["status" => true,"data" => $patients, "message" => "Booking Request Reschedule Appoinment view successfully"]);
        }else{
            return response()->json(["status" => false,"data" => '' , "message" => "Booking Request Reschedule Appoinment view not successfully"]);
        }
    }
    public function cancel_refund()
    {

        $doctor_id = Auth::user()->userDetailsId;
       // $patients = Patients::join('booking_request','', 'booking_request.patient_name')
        $patients = BookingRequest::join('patient_details','booking_request.patient_name','patient_details.id')
                              ->select('booking_request.*','patient_details.first_name','patient_details.last_name','patient_details.id as patient_id')
                              ->where('booking_request.status',5)
                              ->whereDate('booking_request.updated_at', Carbon::today())
                              ->where('booking_request.doctor_name', $doctor_id)
                              ->get();
 
     if ($patients) {
            return response()->json(["status" => true,"data" => $patients, "message" => "Booking Request cancel Appoinment view successfully"]);
        }else{
            return response()->json(["status" => false,"data" => '' , "message" => "Booking Request cancel Appoinment view not successfully"]);
        }
    }
    

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/patients/{id}",
     *      summary="Remove the specified Patients from storage",
     *      tags={"Patients"},
     *      description="Delete Patients",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Patients",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Patients $patients */
        $patients = $this->patientsRepository->find($id);

        if (empty($patients)) {
            return $this->sendError('Patients not found');
        }

        $patients->delete();

        return $this->sendSuccess('Patients deleted successfully');
    }
    public function first_otification(Request $request)
    {


        // API access key from Google API's Console
        define( 'API_ACCESS_KEY', 'AAAA4yAJz-A:APA91bGQM-avVeV8gU7S6MemcBlvgHiwX8ClmDZdw_UBlSJQy2GYv3T_s5LPxy9QRX2J_YOHxvsUagDWqlZyGDkpe0iUtX01rivD4eaMVFlyA02GebeLLUHjWwC8fvHnaVaGkRFluaro');
                    
        
        //$data = Auth::user()->mobile_token;
        $data = $request->mobile_token;

        $registrationIds = array($data);
        $Patients = "demo data";
    
        
        // prep the bundle
        $msg = array
        (
            'message'   => "Check Notification",
            'title'     => "Reminder",
            'tickerText'=> 'Ticker text here...Ticker text here...Ticker text here',
            'vibrate'   => 1,
            'sound'     => 1,
            'largeIcon' => 'large_icon',
            'smallIcon' => 'small_icon',
            'Patients'=> $Patients
        );
        $fields = array
        (
            'registration_ids'  => $registrationIds,
            'data'              => $msg
        );
         
         
        $headers = array
        (
            'Authorization: key=' . API_ACCESS_KEY,
            'Content-Type: application/json'
        );

        $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
        $result = curl_exec($ch );
        $err = curl_error($curl);
            if ($err) {
              echo "cURL Error #:" . $err;
            } else {
              $result = json_decode($msg);
              /*echo "<pre>";
              print_r($result->data->uid);
              exit;*/
            }
        curl_close( $ch );

            
    }

    
  
}
