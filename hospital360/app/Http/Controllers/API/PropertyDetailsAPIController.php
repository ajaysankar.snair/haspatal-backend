<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePropertyDetailsAPIRequest;
use App\Http\Requests\API\UpdatePropertyDetailsAPIRequest;
use App\Models\PropertyDetails;
use App\Repositories\PropertyDetailsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class PropertyDetailsController
 * @package App\Http\Controllers\API
 */

class PropertyDetailsAPIController extends AppBaseController
{
    /** @var  PropertyDetailsRepository */
    private $propertyDetailsRepository;

    public function __construct(PropertyDetailsRepository $propertyDetailsRepo)
    {
        $this->propertyDetailsRepository = $propertyDetailsRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/propertyDetails",
     *      summary="Get a listing of the PropertyDetails.",
     *      tags={"PropertyDetails"},
     *      description="Get all PropertyDetails",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/PropertyDetails")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $propertyDetails = $this->propertyDetailsRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($propertyDetails->toArray(), 'Property Details retrieved successfully');
    }

    /**
     * @param CreatePropertyDetailsAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/propertyDetails",
     *      summary="Store a newly created PropertyDetails in storage",
     *      tags={"PropertyDetails"},
     *      description="Store PropertyDetails",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="PropertyDetails that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/PropertyDetails")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/PropertyDetails"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreatePropertyDetailsAPIRequest $request)
    {
        $input = $request->all();

        $propertyDetails = $this->propertyDetailsRepository->create($input);

        return $this->sendResponse($propertyDetails->toArray(), 'Property Details saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/propertyDetails/{id}",
     *      summary="Display the specified PropertyDetails",
     *      tags={"PropertyDetails"},
     *      description="Get PropertyDetails",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of PropertyDetails",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/PropertyDetails"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var PropertyDetails $propertyDetails */
        $propertyDetails = $this->propertyDetailsRepository->find($id);

        if (empty($propertyDetails)) {
            return $this->sendError('Property Details not found');
        }

        return $this->sendResponse($propertyDetails->toArray(), 'Property Details retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdatePropertyDetailsAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/propertyDetails/{id}",
     *      summary="Update the specified PropertyDetails in storage",
     *      tags={"PropertyDetails"},
     *      description="Update PropertyDetails",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of PropertyDetails",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="PropertyDetails that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/PropertyDetails")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/PropertyDetails"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdatePropertyDetailsAPIRequest $request)
    {
        $input = $request->all();

        /** @var PropertyDetails $propertyDetails */
        $propertyDetails = $this->propertyDetailsRepository->find($id);

        if (empty($propertyDetails)) {
            return $this->sendError('Property Details not found');
        }

        $propertyDetails = $this->propertyDetailsRepository->update($input, $id);

        return $this->sendResponse($propertyDetails->toArray(), 'PropertyDetails updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/propertyDetails/{id}",
     *      summary="Remove the specified PropertyDetails from storage",
     *      tags={"PropertyDetails"},
     *      description="Delete PropertyDetails",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of PropertyDetails",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var PropertyDetails $propertyDetails */
        $propertyDetails = $this->propertyDetailsRepository->find($id);

        if (empty($propertyDetails)) {
            return $this->sendError('Property Details not found');
        }

        $propertyDetails->delete();

        return $this->sendSuccess('Property Details deleted successfully');
    }
}
