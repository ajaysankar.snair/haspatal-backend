<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateWallet_360_transactionAPIRequest;
use App\Http\Requests\API\UpdateWallet_360_transactionAPIRequest;
use App\Models\Wallet_360_transaction;
use App\Repositories\Wallet_360_transactionRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;
use Auth;
use DB;

/**
 * Class Wallet_360_transactionController
 * @package App\Http\Controllers\API
 */

class Wallet_360_transactionAPIController extends AppBaseController
{
    /** @var  Wallet_360_transactionRepository */
    private $wallet360TransactionRepository;

    public function __construct(Wallet_360_transactionRepository $wallet360TransactionRepo)
    {
        $this->wallet360TransactionRepository = $wallet360TransactionRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/wallet360Transactions",
     *      summary="Get a listing of the Wallet_360_transactions.",
     *      tags={"Wallet_360_transaction"},
     *      description="Get all Wallet_360_transactions",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Wallet_360_transaction")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $wallet360Transactions = $this->wallet360TransactionRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($wallet360Transactions->toArray(), 'Wallet 360 Transactions retrieved successfully');
    }

    /**
     * @param CreateWallet_360_transactionAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/wallet360Transactions",
     *      summary="Store a newly created Wallet_360_transaction in storage",
     *      tags={"Wallet_360_transaction"},
     *      description="Store Wallet_360_transaction",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Wallet_360_transaction that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Wallet_360_transaction")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Wallet_360_transaction"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateWallet_360_transactionAPIRequest $request)
    {
        $book =  DB::table('wallet_360')->where('user_id', $request->user_id)->orderBy('user_id', 'desc')->first();
        $input = $request->all();
        $input['cut_amount'] = $book->wa_amount -  $request->cut_amount;
        $input['created_by'] = Auth::user()->id;
        $input['updated_by'] = Auth::user()->id;

        $wallet360Transaction = $this->wallet360TransactionRepository->create($input);

        return $this->sendResponse($wallet360Transaction->toArray(), 'Wallet 360 Transaction saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/wallet360Transactions/{id}",
     *      summary="Display the specified Wallet_360_transaction",
     *      tags={"Wallet_360_transaction"},
     *      description="Get Wallet_360_transaction",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Wallet_360_transaction",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Wallet_360_transaction"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Wallet_360_transaction $wallet360Transaction */
        $wallet360Transaction = $this->wallet360TransactionRepository->find($id);

        if (empty($wallet360Transaction)) {
            return $this->sendError('Wallet 360 Transaction not found');
        }

        return $this->sendResponse($wallet360Transaction->toArray(), 'Wallet 360 Transaction retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateWallet_360_transactionAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/wallet360Transactions/{id}",
     *      summary="Update the specified Wallet_360_transaction in storage",
     *      tags={"Wallet_360_transaction"},
     *      description="Update Wallet_360_transaction",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Wallet_360_transaction",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Wallet_360_transaction that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Wallet_360_transaction")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Wallet_360_transaction"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateWallet_360_transactionAPIRequest $request)
    {
        $input = $request->all();

        /** @var Wallet_360_transaction $wallet360Transaction */
        $wallet360Transaction = $this->wallet360TransactionRepository->find($id);

        if (empty($wallet360Transaction)) {
            return $this->sendError('Wallet 360 Transaction not found');
        }

        $wallet360Transaction = $this->wallet360TransactionRepository->update($input, $id);

        return $this->sendResponse($wallet360Transaction->toArray(), 'Wallet_360_transaction updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/wallet360Transactions/{id}",
     *      summary="Remove the specified Wallet_360_transaction from storage",
     *      tags={"Wallet_360_transaction"},
     *      description="Delete Wallet_360_transaction",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Wallet_360_transaction",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Wallet_360_transaction $wallet360Transaction */
        $wallet360Transaction = $this->wallet360TransactionRepository->find($id);

        if (empty($wallet360Transaction)) {
            return $this->sendError('Wallet 360 Transaction not found');
        }

        $wallet360Transaction->delete();

        return $this->sendSuccess('Wallet 360 Transaction deleted successfully');
    }
}
