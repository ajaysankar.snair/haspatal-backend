<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateVideo_like_dislikeAPIRequest;
use App\Http\Requests\API\UpdateVideo_like_dislikeAPIRequest;
use App\Models\Video_like_dislike;
use App\Repositories\Video_like_dislikeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class Video_like_dislikeController
 * @package App\Http\Controllers\API
 */

class Video_like_dislikeAPIController extends AppBaseController
{
    /** @var  Video_like_dislikeRepository */
    private $videoLikeDislikeRepository;

    public function __construct(Video_like_dislikeRepository $videoLikeDislikeRepo)
    {
        $this->videoLikeDislikeRepository = $videoLikeDislikeRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/videoLikeDislikes",
     *      summary="Get a listing of the Video_like_dislikes.",
     *      tags={"Video_like_dislike"},
     *      description="Get all Video_like_dislikes",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Video_like_dislike")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $videoLikeDislikes = $this->videoLikeDislikeRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($videoLikeDislikes->toArray(), 'Video Like Dislikes retrieved successfully');
    }

    /**
     * @param CreateVideo_like_dislikeAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/videoLikeDislikes",
     *      summary="Store a newly created Video_like_dislike in storage",
     *      tags={"Video_like_dislike"},
     *      description="Store Video_like_dislike",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Video_like_dislike that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Video_like_dislike")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Video_like_dislike"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateVideo_like_dislikeAPIRequest $request)
    {
        $input = $request->all();

        $videoLikeDislike = $this->videoLikeDislikeRepository->create($input);

        return $this->sendResponse($videoLikeDislike->toArray(), 'Video Like Dislike saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/videoLikeDislikes/{id}",
     *      summary="Display the specified Video_like_dislike",
     *      tags={"Video_like_dislike"},
     *      description="Get Video_like_dislike",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Video_like_dislike",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Video_like_dislike"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Video_like_dislike $videoLikeDislike */
        $videoLikeDislike = $this->videoLikeDislikeRepository->find($id);

        if (empty($videoLikeDislike)) {
            return $this->sendError('Video Like Dislike not found');
        }

        return $this->sendResponse($videoLikeDislike->toArray(), 'Video Like Dislike retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateVideo_like_dislikeAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/videoLikeDislikes/{id}",
     *      summary="Update the specified Video_like_dislike in storage",
     *      tags={"Video_like_dislike"},
     *      description="Update Video_like_dislike",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Video_like_dislike",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Video_like_dislike that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Video_like_dislike")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Video_like_dislike"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateVideo_like_dislikeAPIRequest $request)
    {
        $input = $request->all();

        /** @var Video_like_dislike $videoLikeDislike */
        $videoLikeDislike = $this->videoLikeDislikeRepository->find($id);

        if (empty($videoLikeDislike)) {
            return $this->sendError('Video Like Dislike not found');
        }

        $videoLikeDislike = $this->videoLikeDislikeRepository->update($input, $id);

        return $this->sendResponse($videoLikeDislike->toArray(), 'Video_like_dislike updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/videoLikeDislikes/{id}",
     *      summary="Remove the specified Video_like_dislike from storage",
     *      tags={"Video_like_dislike"},
     *      description="Delete Video_like_dislike",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Video_like_dislike",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Video_like_dislike $videoLikeDislike */
        $videoLikeDislike = $this->videoLikeDislikeRepository->find($id);

        if (empty($videoLikeDislike)) {
            return $this->sendError('Video Like Dislike not found');
        }

        $videoLikeDislike->delete();

        return $this->sendSuccess('Video Like Dislike deleted successfully');
    }
}
