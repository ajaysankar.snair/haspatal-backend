<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateH_planAPIRequest;
use App\Http\Requests\API\UpdateH_planAPIRequest;
use App\Models\H_plan;
use App\Repositories\H_planRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class H_planController
 * @package App\Http\Controllers\API
 */

class H_planAPIController extends AppBaseController
{
    /** @var  H_planRepository */
    private $hPlanRepository;

    public function __construct(H_planRepository $hPlanRepo)
    {
        $this->hPlanRepository = $hPlanRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/hPlans",
     *      summary="Get a listing of the H_plans.",
     *      tags={"H_plan"},
     *      description="Get all H_plans",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/H_plan")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $hPlans = $this->hPlanRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($hPlans->toArray(), 'H Plans retrieved successfully');
    }

    /**
     * @param CreateH_planAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/hPlans",
     *      summary="Store a newly created H_plan in storage",
     *      tags={"H_plan"},
     *      description="Store H_plan",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="H_plan that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/H_plan")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/H_plan"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateH_planAPIRequest $request)
    {
        $input = $request->all();

        $hPlan = $this->hPlanRepository->create($input);

        return $this->sendResponse($hPlan->toArray(), 'H Plan saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/hPlans/{id}",
     *      summary="Display the specified H_plan",
     *      tags={"H_plan"},
     *      description="Get H_plan",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of H_plan",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/H_plan"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var H_plan $hPlan */
        $hPlan = $this->hPlanRepository->find($id);

        if (empty($hPlan)) {
            return $this->sendError('H Plan not found');
        }

        return $this->sendResponse($hPlan->toArray(), 'H Plan retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateH_planAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/hPlans/{id}",
     *      summary="Update the specified H_plan in storage",
     *      tags={"H_plan"},
     *      description="Update H_plan",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of H_plan",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="H_plan that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/H_plan")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/H_plan"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateH_planAPIRequest $request)
    {
        $input = $request->all();

        /** @var H_plan $hPlan */
        $hPlan = $this->hPlanRepository->find($id);

        if (empty($hPlan)) {
            return $this->sendError('H Plan not found');
        }

        $hPlan = $this->hPlanRepository->update($input, $id);

        return $this->sendResponse($hPlan->toArray(), 'H_plan updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/hPlans/{id}",
     *      summary="Remove the specified H_plan from storage",
     *      tags={"H_plan"},
     *      description="Delete H_plan",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of H_plan",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var H_plan $hPlan */
        $hPlan = $this->hPlanRepository->find($id);

        if (empty($hPlan)) {
            return $this->sendError('H Plan not found');
        }

        $hPlan->delete();

        return $this->sendSuccess('H Plan deleted successfully');
    }
}
