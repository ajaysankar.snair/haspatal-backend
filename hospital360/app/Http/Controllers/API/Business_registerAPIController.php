<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateBusiness_registerAPIRequest;
use App\Http\Requests\API\UpdateBusiness_registerAPIRequest;
use App\Models\Business_register;
use App\Repositories\Business_registerRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;
use Auth;
use DB;
use App\Models\Coupan;

/**
 * Class Business_registerController
 * @package App\Http\Controllers\API
 */

class Business_registerAPIController extends AppBaseController
{
    /** @var  Business_registerRepository */
    private $businessRegisterRepository;

    public function __construct(Business_registerRepository $businessRegisterRepo)
    {
        $this->businessRegisterRepository = $businessRegisterRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/businessRegisters",
     *      summary="Get a listing of the Business_registers.",
     *      tags={"Business_register"},
     *      description="Get all Business_registers",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Business_register")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $businessRegisters = $this->businessRegisterRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($businessRegisters->toArray(), 'Business Registers retrieved successfully');
    }

    /**
     * @param CreateBusiness_registerAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/businessRegisters",
     *      summary="Store a newly created Business_register in storage",
     *      tags={"Business_register"},
     *      description="Store Business_register",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Business_register that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Business_register")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Business_register"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(Request $request)
    {
        
        $book =  DB::table('business_register')->where('user_id', $request->user_id)->orderBy('user_id', 'desc')->first();
       /* echo "<pre>";
        print_r($book); exit;*/

        if(!empty($book)){
            $input = $request->all();
            $id = $book->id;
            if($request->hasfile('b_lic'))
            {
                $image1 = $request->file('b_lic');
                $extension1 = $image1->getClientOriginalExtension(); // getting image extension
                $filename1 =time().'__'.$image1->getClientOriginalName();
                $path1 = public_path('/media/b_lic/');
                $image1->move($path1, $filename1);
                $input['b_lic'] =$filename1;
            }
            if($request->hasfile('shop_pic'))
            {
                $image1 = $request->file('shop_pic');
                $extension1 = $image1->getClientOriginalExtension(); // getting image extension
                $filename1 =time().'__'.$image1->getClientOriginalName();
                $path1 = public_path('/media/shop_pic/');
                $image1->move($path1, $filename1);
                $input['shop_pic'] =$filename1;
            }
            if($request->hasfile('b_card_pic'))
            {
                $image1 = $request->file('b_card_pic');
                $extension1 = $image1->getClientOriginalExtension(); // getting image extension
                $filename1 =time().'__'.$image1->getClientOriginalName();
                $path1 = public_path('/media/b_card_pic/');
                $image1->move($path1, $filename1);
                $input['b_card_pic'] =$filename1;
            }
      
            $businessRegister = $this->businessRegisterRepository->update($input, $id);

        }

        if(empty($book))
        {
            $input = $request->all();
        
            $input['created_by'] = $request->user_id;
            $input['updated_by'] = $request->user_id;
            if($request->hasfile('b_lic'))
            {
                $image1 = $request->file('b_lic');
                $extension1 = $image1->getClientOriginalExtension(); // getting image extension
                $filename1 =time().'__'.$image1->getClientOriginalName();
                $path1 = public_path('/media/b_lic/');
                $image1->move($path1, $filename1);
                $input['b_lic'] = $filename1;
            }
            if($request->hasfile('shop_pic'))
            {
                $image1 = $request->file('shop_pic');
                $extension1 = $image1->getClientOriginalExtension(); // getting image extension
                $filename1 =time().'__'.$image1->getClientOriginalName();
                $path1 = public_path('/media/shop_pic/');
                $image1->move($path1, $filename1);
                $input['shop_pic'] = $filename1;
            }
            if($request->hasfile('b_card_pic'))
            {
                $image1 = $request->file('b_card_pic');
                $extension1 = $image1->getClientOriginalExtension(); // getting image extension
                $filename1 =time().'__'.$image1->getClientOriginalName();
                $path1 = public_path('/media/b_card_pic/');
                $image1->move($path1, $filename1);
                $input['b_card_pic']=$filename1;
            }
            $businessRegister = $this->businessRegisterRepository->create($input);
        }
        return $this->sendResponse($businessRegister->toArray(), 'Business Register saved successfully');
    }


    public function business_profile(Request $request)
    {
        $data = Business_register::where('user_id',$request->user_id)
                                ->join('country', 'business_register.country_id', '=', 'country.id')
                                ->join('state', 'business_register.state_id', '=', 'state.id')
                                ->join('city', 'business_register.city_id', '=', 'city.id')
                                ->select('business_register.*','country.country','state.state','city.city')
                                ->first();

        $affected = DB::table('haspatal_360_register')->where('id', Auth::user()->userDetailsId)->update(['buisness_profile' => 1]);

        $path = array("b_lic"=>env('APP_URL').'public/media/b_lic',
                "b_card_pic"=>env('APP_URL').'public/media/b_card_pic',
                 "shop_pic"=>env('APP_URL').'public/media/shop_pic');

        return response()->json(["status" => true,"data" => $data, "message" => "Business retrieved successfully","path" => $path]);
    }


      public function business_view(Request $request)
      {

         $coverage = Business_register::where('user_id',$request->user_id)->get();
       
        
          if ($coverage) {
            return response()->json(["status" => true,"massage"=>"Business retrieved successfully","data" => $coverage]);
        }else{
            return response()->json(["status" => false,"data" => $coverage]);
        }

      }
       public function business_view_360(Request $request)
      {

        if ($request->pincode != '') {
                $business_view_360 = Business_register::leftJoin('create_offer_360', 'business_register.user_id', '=', 'create_offer_360.user_id')
                                                ->where('business_register.b_id',$request->role_id)
                                                ->where('business_register.pincode',$request->pincode)
                                                ->where('business_register.admin_status',1)
                                                ->select('business_register.*','create_offer_360.user_id as create_offer_360_user_id','create_offer_360.offer_id as offer_id','create_offer_360.id as create_offer_360_id')
                                                ->get();
                
        }else{
            $business_view_360 = Business_register::leftJoin('create_offer_360', 'business_register.user_id', '=', 'create_offer_360.user_id')
                                                ->where('business_register.b_id',$request->role_id)
                                                ->where('business_register.admin_status',1)
                                                ->first();
        }
                            
                            $path = array("b_lic"=>'public/media/b_lic',
                "b_card_pic"=>'public/media/b_card_pic',
                 "shop_pic"=>'public/media/shop_pic');
        
          if ($business_view_360) {
            return response()->json(["status" => true,"massage"=>"Business retrieved successfully","path"=>$path,"data" => $business_view_360]);
        }else{
            return response()->json(["status" => false,"data" => $business_view_360]);
        }

      }

                                public function view_document(Request $request)
      {
                if ($request->user_id) {
                
             $data=Business_register::select('b_lic','shop_pic','b_card_pic')->where('user_id',$request->user_id)->get()->first();
              $path = array("b_lic"=>env('APP_URL').'public/media/b_lic/'.$data->b_lic,
                "b_card_pic"=>env('APP_URL').'public/media/b_card_pic/'.$data->b_card_pic,
                 "shop_pic"=>env('APP_URL').'public/media/shop_pic/'.$data->shop_pic);
              if ($data) {
            return response()->json(["status" => true,"massage"=>"Business retrieved successfully","data" => $data,"path"=>$path]);
        }else{
            return response()->json(["status" => false,"data" => $data]);
        }

                }
            else
            {
                return response()->json(["status"=>false,"message"=>"Invalid Id","keyword"=>'user_id']);
            }
      }
    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/businessRegisters/{id}",
     *      summary="Display the specified Business_register",
     *      tags={"Business_register"},
     *      description="Get Business_register",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Business_register",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Business_register"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Business_register $businessRegister */
        $businessRegister = $this->businessRegisterRepository->find($id);

        if (empty($businessRegister)) {
            return $this->sendError('Business Register not found');
        }

        return $this->sendResponse($businessRegister->toArray(), 'Business Register retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateBusiness_registerAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/businessRegisters/{id}",
     *      summary="Update the specified Business_register in storage",
     *      tags={"Business_register"},
     *      description="Update Business_register",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Business_register",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Business_register that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Business_register")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Business_register"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateBusiness_registerAPIRequest $request)
    {
        $input = $request->all();

        /** @var Business_register $businessRegister */
        $businessRegister = $this->businessRegisterRepository->find($id);

        if (empty($businessRegister)) {
            return $this->sendError('Business Register not found');
        }

        $businessRegister = $this->businessRegisterRepository->update($input, $id);

        return $this->sendResponse($businessRegister->toArray(), 'Business_register updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/businessRegisters/{id}",
     *      summary="Remove the specified Business_register from storage",
     *      tags={"Business_register"},
     *      description="Delete Business_register",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Business_register",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Business_register $businessRegister */
        $businessRegister = $this->businessRegisterRepository->find($id);

        if (empty($businessRegister)) {
            return $this->sendError('Business Register not found');
        }

        $businessRegister->delete();

        return $this->sendSuccess('Business Register deleted successfully');
    }

    public function coupon_list(Request $request)
    {
        $data = Coupan::where('type',$request->b_id)->first();

        return $this->sendResponse($data->toArray(), 'Coupan retrieved successfully');
    }

    public function coupans_check_h360(Request $request)
    {
        $result = Coupan::where('promo_code', $request->promo_code)->first();

        if (empty($result)) {
            return response()->json(["status" => false,"data" => '',"massage" => "This Coupon code is not applicable"]);
        }else{
            
            $check = \App\Models\H360coupanRedeem::where('b_id',$request->b_id)->where('user_id',$request->user_id)->where('coupan_code',$request->promo_code)->exists();

            if ($check) {
                return response()->json(["status" => false,"data" => '',"massage" => "You have used this coupon code!"]);
            }else{
                $data = \App\Models\H360coupanRedeem::create([
                                                    "b_id"=>$request->b_id,
                                                    "user_id"=>$request->user_id,
                                                    "coupan_code"=>$request->promo_code,
                                                    "price"=>$request->price,
                                                    "created_by"=>Auth::user()->id,
                                                    "updated_by"=>Auth::user()->id,
                                                    ]);

                $affected = DB::table('haspatal_360_register')->where('id', Auth::user()->userDetailsId)->update(['coupon_status' => 1]);

                return response()->json(["status" => true,"massage"=>"Coupon is Applyied successfully","data" => $data]);
            }

        }

        $result1 = Coupan::where('promo_code', $request->promo_code)->get();

        $allcount = DB::table('H360_coupan_redeem')->where('coupan_code',$request->promo_code)->where('user_id',$request->user_id)->count();

        $startDate =  \Carbon\Carbon::parse($result->edate)->startOfDay(); 
        $endDate =  \Carbon\Carbon::now()->endOfDay(); 
        $code_used = $result->used_promo_code_time;

        if($code_used >= $allcount)
        {
            return response()->json(["status" => true,"massage"=>"Coupon is valid","data" => $result1]);
        }else{
            return response()->json(["status" => false,"data" => '',"massage" => "You have used this coupon code!"]);
        }
    }


     public function is_business_exist(Request $request)
                {
                    $data=DB::table('users')
                            ->join('haspatal_360_register','haspatal_360_register.email','=','users.email')
                            ->select('users.role_id as b_type','users.id','users.email','users.first_name as full_name')
                            ->where('haspatal_360_register.mobile_no',$request->mobile)
                            ->where('users.user_type',4)
                            ->first();
                            if($data)
                            {
                                return response()->json(['status'=>true,'message'=>'Registration Found','data'=>$data]);
                            }
                            else
                            {
                             return response()->json(['status'=>false,'message'=>'Registration not Found']);

                            }
                }

                    function save_business(Request $request)
                    {
                         $input = $request->all();
                         
                         $book =  DB::table('business_register')->where('user_id', $request->user_id)->orderBy('user_id', 'desc')->first();
        
           if(empty($book))
           {
            $input['created_by'] = '';
            $input['updated_by'] = '';
            if($request->hasfile('b_lic'))
            {
                $image1 = $request->file('b_lic');
                $extension1 = $image1->getClientOriginalExtension(); // getting image extension
                $filename1 =time().'__'.$image1->getClientOriginalName();
                $path1 = public_path('/media/b_lic/');
                $image1->move($path1, $filename1);
                $input['b_lic'] = $filename1;
            }
            if($request->hasfile('shop_pic'))
            {
                $image1 = $request->file('shop_pic');
                $extension1 = $image1->getClientOriginalExtension(); // getting image extension
                $filename1 =time().'__'.$image1->getClientOriginalName();
                $path1 = public_path('/media/shop_pic/');
                $image1->move($path1, $filename1);
                $input['shop_pic'] = $filename1;
            }
            if($request->hasfile('b_card_pic'))
            {
                $image1 = $request->file('b_card_pic');
                $extension1 = $image1->getClientOriginalExtension(); // getting image extension
                $filename1 =time().'__'.$image1->getClientOriginalName();
                $path1 = public_path('/media/b_card_pic/');
                $image1->move($path1, $filename1);
                $input['b_card_pic']=$filename1;
            }
            $businessRegister = $this->businessRegisterRepository->create($input);
            $action='Created';
           }
           
           if(!empty($book))
           {
            $input = $request->all();
            $id = $book->id;
            if($request->hasfile('b_lic'))
            {
                $image1 = $request->file('b_lic');
                $extension1 = $image1->getClientOriginalExtension(); // getting image extension
                $filename1 =time().'__'.$image1->getClientOriginalName();
                $path1 = public_path('/media/b_lic/');
                $image1->move($path1, $filename1);
                $input['b_lic'] =$filename1;
            }
            if($request->hasfile('shop_pic'))
            {
                $image1 = $request->file('shop_pic');
                $extension1 = $image1->getClientOriginalExtension(); // getting image extension
                $filename1 =time().'__'.$image1->getClientOriginalName();
                $path1 = public_path('/media/shop_pic/');
                $image1->move($path1, $filename1);
                $input['shop_pic'] =$filename1;
            }
            if($request->hasfile('b_card_pic'))
            {
                $image1 = $request->file('b_card_pic');
                $extension1 = $image1->getClientOriginalExtension(); // getting image extension
                $filename1 =time().'__'.$image1->getClientOriginalName();
                $path1 = public_path('/media/b_card_pic/');
                $image1->move($path1, $filename1);
                $input['b_card_pic'] =$filename1;
            }
      
            $businessRegister = $this->businessRegisterRepository->update($input, $id);
            $action='Updated';

        }
            
            if($businessRegister){
                return response()->json(['status'=>true,'data'=>$input,'action'=>$action,'user_id'=>$request->user_id]);
            }
            else
            {
                return response()->json(['status'=>false]);
            }
                    }

}
