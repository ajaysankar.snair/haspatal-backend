<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateTaxSetupAPIRequest;
use App\Http\Requests\API\UpdateTaxSetupAPIRequest;
use App\Models\TaxSetup;
use App\Repositories\TaxSetupRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class TaxSetupController
 * @package App\Http\Controllers\API
 */

class TaxSetupAPIController extends AppBaseController
{
    /** @var  TaxSetupRepository */
    private $taxSetupRepository;

    public function __construct(TaxSetupRepository $taxSetupRepo)
    {
        $this->taxSetupRepository = $taxSetupRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/taxSetups",
     *      summary="Get a listing of the TaxSetups.",
     *      tags={"TaxSetup"},
     *      description="Get all TaxSetups",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/TaxSetup")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $taxSetups = $this->taxSetupRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($taxSetups->toArray(), 'Tax Setups retrieved successfully');
    }

    /**
     * @param CreateTaxSetupAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/taxSetups",
     *      summary="Store a newly created TaxSetup in storage",
     *      tags={"TaxSetup"},
     *      description="Store TaxSetup",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="TaxSetup that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/TaxSetup")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/TaxSetup"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateTaxSetupAPIRequest $request)
    {
        $input = $request->all();

        $taxSetup = $this->taxSetupRepository->create($input);

        return $this->sendResponse($taxSetup->toArray(), 'Tax Setup saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/taxSetups/{id}",
     *      summary="Display the specified TaxSetup",
     *      tags={"TaxSetup"},
     *      description="Get TaxSetup",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of TaxSetup",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/TaxSetup"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var TaxSetup $taxSetup */
        $taxSetup = $this->taxSetupRepository->find($id);

        if (empty($taxSetup)) {
            return $this->sendError('Tax Setup not found');
        }

        return $this->sendResponse($taxSetup->toArray(), 'Tax Setup retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateTaxSetupAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/taxSetups/{id}",
     *      summary="Update the specified TaxSetup in storage",
     *      tags={"TaxSetup"},
     *      description="Update TaxSetup",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of TaxSetup",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="TaxSetup that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/TaxSetup")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/TaxSetup"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateTaxSetupAPIRequest $request)
    {
        $input = $request->all();

        /** @var TaxSetup $taxSetup */
        $taxSetup = $this->taxSetupRepository->find($id);

        if (empty($taxSetup)) {
            return $this->sendError('Tax Setup not found');
        }

        $taxSetup = $this->taxSetupRepository->update($input, $id);

        return $this->sendResponse($taxSetup->toArray(), 'TaxSetup updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/taxSetups/{id}",
     *      summary="Remove the specified TaxSetup from storage",
     *      tags={"TaxSetup"},
     *      description="Delete TaxSetup",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of TaxSetup",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var TaxSetup $taxSetup */
        $taxSetup = $this->taxSetupRepository->find($id);

        if (empty($taxSetup)) {
            return $this->sendError('Tax Setup not found');
        }

        $taxSetup->delete();

        return $this->sendSuccess('Tax Setup deleted successfully');
    }
}
