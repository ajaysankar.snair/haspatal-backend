<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateHospital_registerAPIRequest;
use App\Http\Requests\API\UpdateHospital_registerAPIRequest;
use App\Models\Hospital_register;
use App\Repositories\Hospital_registerRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class Hospital_registerController
 * @package App\Http\Controllers\API
 */

class Hospital_registerAPIController extends AppBaseController
{
    /** @var  Hospital_registerRepository */
    private $hospitalRegisterRepository;

    public function __construct(Hospital_registerRepository $hospitalRegisterRepo)
    {
        $this->hospitalRegisterRepository = $hospitalRegisterRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/hospitalRegisters",
     *      summary="Get a listing of the Hospital_registers.",
     *      tags={"Hospital_register"},
     *      description="Get all Hospital_registers",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Hospital_register")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $hospitalRegisters = $this->hospitalRegisterRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($hospitalRegisters->toArray(), 'Hospital Registers retrieved successfully');
    }

    /**
     * @param CreateHospital_registerAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/hospitalRegisters",
     *      summary="Store a newly created Hospital_register in storage",
     *      tags={"Hospital_register"},
     *      description="Store Hospital_register",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Hospital_register that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Hospital_register")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Hospital_register"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateHospital_registerAPIRequest $request)
    {
        $input = $request->all();

        $hospitalRegister = $this->hospitalRegisterRepository->create($input);

        return $this->sendResponse($hospitalRegister->toArray(), 'Hospital Register saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/hospitalRegisters/{id}",
     *      summary="Display the specified Hospital_register",
     *      tags={"Hospital_register"},
     *      description="Get Hospital_register",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Hospital_register",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Hospital_register"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Hospital_register $hospitalRegister */
        $hospitalRegister = $this->hospitalRegisterRepository->find($id);

        if (empty($hospitalRegister)) {
            return $this->sendError('Hospital Register not found');
        }

        return $this->sendResponse($hospitalRegister->toArray(), 'Hospital Register retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateHospital_registerAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/hospitalRegisters/{id}",
     *      summary="Update the specified Hospital_register in storage",
     *      tags={"Hospital_register"},
     *      description="Update Hospital_register",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Hospital_register",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Hospital_register that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Hospital_register")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Hospital_register"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateHospital_registerAPIRequest $request)
    {
        $input = $request->all();

        /** @var Hospital_register $hospitalRegister */
        $hospitalRegister = $this->hospitalRegisterRepository->find($id);

        if (empty($hospitalRegister)) {
            return $this->sendError('Hospital Register not found');
        }

        $hospitalRegister = $this->hospitalRegisterRepository->update($input, $id);

        return $this->sendResponse($hospitalRegister->toArray(), 'Hospital_register updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/hospitalRegisters/{id}",
     *      summary="Remove the specified Hospital_register from storage",
     *      tags={"Hospital_register"},
     *      description="Delete Hospital_register",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Hospital_register",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Hospital_register $hospitalRegister */
        $hospitalRegister = $this->hospitalRegisterRepository->find($id);

        if (empty($hospitalRegister)) {
            return $this->sendError('Hospital Register not found');
        }

        $hospitalRegister->delete();

        return $this->sendSuccess('Hospital Register deleted successfully');
    }
}
