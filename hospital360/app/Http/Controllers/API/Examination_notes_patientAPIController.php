<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateExamination_notes_patientAPIRequest;
use App\Http\Requests\API\UpdateExamination_notes_patientAPIRequest;
use App\Models\Examination_notes_patient;
use App\Repositories\Examination_notes_patientRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;
use DB;
use Auth;

/**
 * Class Examination_notes_patientController
 * @package App\Http\Controllers\API
 */

class Examination_notes_patientAPIController extends AppBaseController
{
    /** @var  Examination_notes_patientRepository */
    private $examinationNotesPatientRepository;

    public function __construct(Examination_notes_patientRepository $examinationNotesPatientRepo)
    {
        $this->examinationNotesPatientRepository = $examinationNotesPatientRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/examinationNotesPatients",
     *      summary="Get a listing of the Examination_notes_patients.",
     *      tags={"Examination_notes_patient"},
     *      description="Get all Examination_notes_patients",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Examination_notes_patient")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $examinationNotesPatients = $this->examinationNotesPatientRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($examinationNotesPatients->toArray(), 'Examination Notes Patients retrieved successfully');
    }

    /**
     * @param CreateExamination_notes_patientAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/examinationNotesPatients",
     *      summary="Store a newly created Examination_notes_patient in storage",
     *      tags={"Examination_notes_patient"},
     *      description="Store Examination_notes_patient",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Examination_notes_patient that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Examination_notes_patient")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Examination_notes_patient"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateExamination_notes_patientAPIRequest $request)
    {
        $book =  DB::table('Examination_notes_patient')->where('e_book_id', $request->e_book_id)->orderBy('e_book_id', 'desc')->first();
        if(!empty($book)){
            $input = $request->all();
            $id = $book->id;
           $examinationNotesPatient = $this->examinationNotesPatientRepository->update($input, $id);

        }else
        {

        $input = $request->all();
        $input['created_by'] = Auth::user()->id;
        $input['updated_by'] = Auth::user()->id;

        $examinationNotesPatient = $this->examinationNotesPatientRepository->create($input);
    }
        return $this->sendResponse($examinationNotesPatient->toArray(), 'Examination Notes Patient saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/examinationNotesPatients/{id}",
     *      summary="Display the specified Examination_notes_patient",
     *      tags={"Examination_notes_patient"},
     *      description="Get Examination_notes_patient",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Examination_notes_patient",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Examination_notes_patient"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Examination_notes_patient $examinationNotesPatient */
        $examinationNotesPatient = $this->examinationNotesPatientRepository->find($id);

        if (empty($examinationNotesPatient)) {
            return $this->sendError('Examination Notes Patient not found');
        }

        return $this->sendResponse($examinationNotesPatient->toArray(), 'Examination Notes Patient retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateExamination_notes_patientAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/examinationNotesPatients/{id}",
     *      summary="Update the specified Examination_notes_patient in storage",
     *      tags={"Examination_notes_patient"},
     *      description="Update Examination_notes_patient",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Examination_notes_patient",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Examination_notes_patient that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Examination_notes_patient")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Examination_notes_patient"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateExamination_notes_patientAPIRequest $request)
    {
        $input = $request->all();

        /** @var Examination_notes_patient $examinationNotesPatient */
        $examinationNotesPatient = $this->examinationNotesPatientRepository->find($id);

        if (empty($examinationNotesPatient)) {
            return $this->sendError('Examination Notes Patient not found');
        }

        $examinationNotesPatient = $this->examinationNotesPatientRepository->update($input, $id);

        return $this->sendResponse($examinationNotesPatient->toArray(), 'Examination_notes_patient updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/examinationNotesPatients/{id}",
     *      summary="Remove the specified Examination_notes_patient from storage",
     *      tags={"Examination_notes_patient"},
     *      description="Delete Examination_notes_patient",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Examination_notes_patient",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Examination_notes_patient $examinationNotesPatient */
        $examinationNotesPatient = $this->examinationNotesPatientRepository->find($id);

        if (empty($examinationNotesPatient)) {
            return $this->sendError('Examination Notes Patient not found');
        }

        $examinationNotesPatient->delete();

        return $this->sendSuccess('Examination Notes Patient deleted successfully');
    }
}
