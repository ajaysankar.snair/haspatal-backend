<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePresent_medications_patientAPIRequest;
use App\Http\Requests\API\UpdatePresent_medications_patientAPIRequest;
use App\Models\Present_medications_patient;
use App\Repositories\Present_medications_patientRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;
use Auth;
use DB;

/**
 * Class Present_medications_patientController
 * @package App\Http\Controllers\API
 */

class Present_medications_patientAPIController extends AppBaseController
{
    /** @var  Present_medications_patientRepository */
    private $presentMedicationsPatientRepository;

    public function __construct(Present_medications_patientRepository $presentMedicationsPatientRepo)
    {
        $this->presentMedicationsPatientRepository = $presentMedicationsPatientRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/presentMedicationsPatients",
     *      summary="Get a listing of the Present_medications_patients.",
     *      tags={"Present_medications_patient"},
     *      description="Get all Present_medications_patients",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Present_medications_patient")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $presentMedicationsPatients = $this->presentMedicationsPatientRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($presentMedicationsPatients->toArray(), 'Present Medications Patients retrieved successfully');
    }

    /**
     * @param CreatePresent_medications_patientAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/presentMedicationsPatients",
     *      summary="Store a newly created Present_medications_patient in storage",
     *      tags={"Present_medications_patient"},
     *      description="Store Present_medications_patient",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Present_medications_patient that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Present_medications_patient")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Present_medications_patient"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreatePresent_medications_patientAPIRequest $request)
    {
        $book =  DB::table('present_medications_patient')->where('m_book_id', $request->m_book_id)->orderBy('m_book_id', 'desc')->first();
        if(!empty($book)){
        $input = $request->all();
        $id = $book->id;
        $presentMedicationsPatient = $this->presentMedicationsPatientRepository->update($input, $id);

        }else
        {
        $input = $request->all();
        $input['created_by'] = Auth::user()->id;
        $input['updated_by'] = Auth::user()->id;

        $presentMedicationsPatient = $this->presentMedicationsPatientRepository->create($input);
        }


        return $this->sendResponse($presentMedicationsPatient->toArray(), 'Present Medications Patient saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/presentMedicationsPatients/{id}",
     *      summary="Display the specified Present_medications_patient",
     *      tags={"Present_medications_patient"},
     *      description="Get Present_medications_patient",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Present_medications_patient",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Present_medications_patient"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Present_medications_patient $presentMedicationsPatient */
        $presentMedicationsPatient = $this->presentMedicationsPatientRepository->find($id);

        if (empty($presentMedicationsPatient)) {
            return $this->sendError('Present Medications Patient not found');
        }

        return $this->sendResponse($presentMedicationsPatient->toArray(), 'Present Medications Patient retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdatePresent_medications_patientAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/presentMedicationsPatients/{id}",
     *      summary="Update the specified Present_medications_patient in storage",
     *      tags={"Present_medications_patient"},
     *      description="Update Present_medications_patient",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Present_medications_patient",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Present_medications_patient that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Present_medications_patient")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Present_medications_patient"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdatePresent_medications_patientAPIRequest $request)
    {
        $input = $request->all();

        /** @var Present_medications_patient $presentMedicationsPatient */
        $presentMedicationsPatient = $this->presentMedicationsPatientRepository->find($id);

        if (empty($presentMedicationsPatient)) {
            return $this->sendError('Present Medications Patient not found');
        }

        $presentMedicationsPatient = $this->presentMedicationsPatientRepository->update($input, $id);

        return $this->sendResponse($presentMedicationsPatient->toArray(), 'Present_medications_patient updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/presentMedicationsPatients/{id}",
     *      summary="Remove the specified Present_medications_patient from storage",
     *      tags={"Present_medications_patient"},
     *      description="Delete Present_medications_patient",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Present_medications_patient",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Present_medications_patient $presentMedicationsPatient */
        $presentMedicationsPatient = $this->presentMedicationsPatientRepository->find($id);

        if (empty($presentMedicationsPatient)) {
            return $this->sendError('Present Medications Patient not found');
        }

        $presentMedicationsPatient->delete();

        return $this->sendSuccess('Present Medications Patient deleted successfully');
    }
}
