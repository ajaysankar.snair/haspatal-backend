<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePerceived_patientAPIRequest;
use App\Http\Requests\API\UpdatePerceived_patientAPIRequest;
use App\Models\Perceived_patient;
use App\Repositories\Perceived_patientRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;
use DB;
use Auth;

/**
 * Class Perceived_patientController
 * @package App\Http\Controllers\API
 */

class Perceived_patientAPIController extends AppBaseController
{
    /** @var  Perceived_patientRepository */
    private $perceivedPatientRepository;

    public function __construct(Perceived_patientRepository $perceivedPatientRepo)
    {
        $this->perceivedPatientRepository = $perceivedPatientRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/perceivedPatients",
     *      summary="Get a listing of the Perceived_patients.",
     *      tags={"Perceived_patient"},
     *      description="Get all Perceived_patients",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Perceived_patient")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $perceivedPatients = $this->perceivedPatientRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($perceivedPatients->toArray(), 'Perceived Patients retrieved successfully');
    }

    /**
     * @param CreatePerceived_patientAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/perceivedPatients",
     *      summary="Store a newly created Perceived_patient in storage",
     *      tags={"Perceived_patient"},
     *      description="Store Perceived_patient",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Perceived_patient that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Perceived_patient")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Perceived_patient"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreatePerceived_patientAPIRequest $request)
    {
        $book =  DB::table('perceived_patient')->where('p_book_id', $request->p_book_id)->orderBy('p_book_id', 'desc')->first();
        if(!empty($book)){
            $input = $request->all();
            $id = $book->id;
           $perceivedPatient = $this->perceivedPatientRepository->update($input, $id);

        }else
        {

        $input = $request->all();
        $input['created_by'] = Auth::user()->id;
        $input['updated_by'] = Auth::user()->id;
        $perceivedPatient = $this->perceivedPatientRepository->create($input);
       }
        return $this->sendResponse($perceivedPatient->toArray(), 'Perceived Patient saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/perceivedPatients/{id}",
     *      summary="Display the specified Perceived_patient",
     *      tags={"Perceived_patient"},
     *      description="Get Perceived_patient",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Perceived_patient",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Perceived_patient"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Perceived_patient $perceivedPatient */
        $perceivedPatient = $this->perceivedPatientRepository->find($id);

        if (empty($perceivedPatient)) {
            return $this->sendError('Perceived Patient not found');
        }

        return $this->sendResponse($perceivedPatient->toArray(), 'Perceived Patient retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdatePerceived_patientAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/perceivedPatients/{id}",
     *      summary="Update the specified Perceived_patient in storage",
     *      tags={"Perceived_patient"},
     *      description="Update Perceived_patient",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Perceived_patient",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Perceived_patient that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Perceived_patient")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Perceived_patient"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdatePerceived_patientAPIRequest $request)
    {
        $input = $request->all();

        /** @var Perceived_patient $perceivedPatient */
        $perceivedPatient = $this->perceivedPatientRepository->find($id);

        if (empty($perceivedPatient)) {
            return $this->sendError('Perceived Patient not found');
        }

        $perceivedPatient = $this->perceivedPatientRepository->update($input, $id);

        return $this->sendResponse($perceivedPatient->toArray(), 'Perceived_patient updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/perceivedPatients/{id}",
     *      summary="Remove the specified Perceived_patient from storage",
     *      tags={"Perceived_patient"},
     *      description="Delete Perceived_patient",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Perceived_patient",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Perceived_patient $perceivedPatient */
        $perceivedPatient = $this->perceivedPatientRepository->find($id);

        if (empty($perceivedPatient)) {
            return $this->sendError('Perceived Patient not found');
        }

        $perceivedPatient->delete();

        return $this->sendSuccess('Perceived Patient deleted successfully');
    }
}
