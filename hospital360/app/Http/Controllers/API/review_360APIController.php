<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createreview_360APIRequest;
use App\Http\Requests\API\Updatereview_360APIRequest;
use App\Models\review_360;
use App\Models\Business_register;
use App\Repositories\review_360Repository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;
use Auth;
use DB;

/**
 * Class review_360Controller
 * @package App\Http\Controllers\API
 */

class review_360APIController extends AppBaseController
{
    /** @var  review_360Repository */
    private $review360Repository;

    public function __construct(review_360Repository $review360Repo)
    {
        $this->review360Repository = $review360Repo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/review360s",
     *      summary="Get a listing of the review_360s.",
     *      tags={"review_360"},
     *      description="Get all review_360s",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/review_360")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        //echo "string"; exit;
        /*$review360s = $this->review360Repository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );*/
        //echo Auth::user()->userDetailsId; exit;
        $review360s = review_360::join('patient_details', 'review_360s.patient_id', '=', 'patient_details.id')
                              ->join('business_register', 'review_360s.f_id', '=', 'business_register.user_id')
                               ->where('review_360s.patient_id',Auth::user()->userDetailsId)
                              ->select('review_360s.*','patient_details.first_name','patient_details.last_name','patient_details.id as patient_id','business_register.b_name')
                              ->get();

        return $this->sendResponse($review360s->toArray(), 'Review 360S retrieved successfully');
    }

      public function pharmacy_city_list(Request $request)
      {

         $order_view = review_360::join('business_register', 'review_360s.f_id', '=', 'business_register.user_id')
                              ->join('create_offer_360', 'review_360s.f_id', '=', 'create_offer_360.user_id')
                              ->where('business_register.city_id',$request->city_id)
                             // ->where('review_360s.patient_id',$request->patient_id)
                              ->select('review_360s.*','business_register.b_name','create_offer_360.offer_id')
                              ->get();


         /*Business_register::join('users', 'business_register.user_id', '=', 'users.id')
                                    ->join('create_offer_360', 'business_register.user_id', '=', 'create_offer_360.user_id')
                                    ->where('city_id',$request->city_id)
                                    ->where('user_id',$request->user_id)
                                     ->select('business_register.*','create_offer_360.offer_id','users.first_name as 360_first_name','users.last_name as 360_last_name')
                                    ->get();*/
       
        
          if ($order_view) {
            return response()->json(["status" => true,"massage"=>"Pharmacy city list retrieved successfully","data" => $order_view]);
        }else{
            return response()->json(["status" => false,"data" => $order_view]);
        }

      }
    

    /**
     * @param Createreview_360APIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/review360s",
     *      summary="Store a newly created review_360 in storage",
     *      tags={"review_360"},
     *      description="Store review_360",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="review_360 that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/review_360")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/review_360"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(Createreview_360APIRequest $request)
    {
         $book =  DB::table('review_360s')->where('book_id', $request->book_id)->orderBy('book_id', 'desc')->first();
        if(!empty($book)){
            $input = $request->all();
            $id = $book->id;
           $review360 = $this->review360Repository->update($input, $id);


        }else
        {
        $input = $request->all();
         $input['created_by'] = Auth::user()->id;
        $input['updated_by'] = Auth::user()->id;

        $review360 = $this->review360Repository->create($input);
        }
        return $this->sendResponse($review360->toArray(), 'Review 360 saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/review360s/{id}",
     *      summary="Display the specified review_360",
     *      tags={"review_360"},
     *      description="Get review_360",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of review_360",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/review_360"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var review_360 $review360 */
        $review360 = $this->review360Repository->find($id);

        if (empty($review360)) {
            return $this->sendError('Review 360 not found');
        }

        return $this->sendResponse($review360->toArray(), 'Review 360 retrieved successfully');
    }

    /**
     * @param int $id
     * @param Updatereview_360APIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/review360s/{id}",
     *      summary="Update the specified review_360 in storage",
     *      tags={"review_360"},
     *      description="Update review_360",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of review_360",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="review_360 that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/review_360")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/review_360"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, Updatereview_360APIRequest $request)
    {
        $input = $request->all();

        /** @var review_360 $review360 */
        $review360 = $this->review360Repository->find($id);

        if (empty($review360)) {
            return $this->sendError('Review 360 not found');
        }

        $review360 = $this->review360Repository->update($input, $id);

        return $this->sendResponse($review360->toArray(), 'review_360 updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/review360s/{id}",
     *      summary="Remove the specified review_360 from storage",
     *      tags={"review_360"},
     *      description="Delete review_360",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of review_360",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var review_360 $review360 */
        $review360 = $this->review360Repository->find($id);

        if (empty($review360)) {
            return $this->sendError('Review 360 not found');
        }

        $review360->delete();

        return $this->sendSuccess('Review 360 deleted successfully');
    }
}
