<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateOrder_360APIRequest;
use App\Http\Requests\API\UpdateOrder_360APIRequest;
use App\Models\Order_360;
use App\Repositories\Order_360Repository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;
use Auth;
use App\Models\Prescription_details;
use App\Models\Wallet_360;
use DB;
use Carbon\Carbon;
use App\User;
use Mail;

/**
 * Class Order_360Controller
 * @package App\Http\Controllers\API
 */

class Order_360APIController extends AppBaseController
{
    /** @var  Order_360Repository */
    private $order360Repository;

    public function __construct(Order_360Repository $order360Repo)
    {
        $this->order360Repository = $order360Repo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/order360s",
     *      summary="Get a listing of the Order_360s.",
     *      tags={"Order_360"},
     *      description="Get all Order_360s",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Order_360")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $order360s = $this->order360Repository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($order360s->toArray(), 'Order 360S retrieved successfully');
    }

    /**
     * @param CreateOrder_360APIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/order360s",
     *      summary="Store a newly created Order_360 in storage",
     *      tags={"Order_360"},
     *      description="Store Order_360",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Order_360 that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Order_360")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Order_360"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */

    public function share_prescription(Request $request)
    {
        $input = $request->all();

        if($request->hasfile('prescription_image'))
        {
            $image = $request->file('prescription_image');
            $extension = $image->getClientOriginalExtension(); // getting image extension
            $filename =time().'__'.$image->getClientOriginalName();
            $path = public_path('/media/prescription_image/');
            $image->move($path, $filename);
            $input['prescription_image'] = $filename;
        }else
        {
            $image=$input['prescription_image'];  
            $image = str_replace(' ', '+', $image);
            $imageName = str_random(10) . '.png';
            $data = base64_decode($image);
            $path = public_path('/media/prescription_image/' . $imageName);
            file_put_contents($path, $data);
            $input['prescription_image'] = $imageName;
        
        }

        $input['haspatal_prescription'] = 2;

        $input['created_by'] = Auth::user()->id;
        $input['updated_by'] = Auth::user()->id;
        $users = User::findOrFail($request->user_id);
        if($users->role_id){
            $input['role_id'] =  $users->role_id;
        }
       
        $order360 = $this->order360Repository->create($input);
        $order_id =
        "ID". $order360->patient_id.
         "00".
         $order360->id;
     
      
        $updateOrder = Order_360::where('id', $order360->id)
        ->update(['Order_id' =>  $order_id]);
        // return $updateOrder;
        $updated_at=$order360->updated_at;
        
       $date= date('d/m/Y H:i', strtotime($updated_at));
    //    $time= Carbon::parse($updated_at)->format('H:i:s');
        // return $date;
        $pharmacy_id= $order360->user_id;
        $user = User::findOrFail($pharmacy_id);
        $content= "Order Number ".$order_id." has been placed for you at ".$date."
Kindly complete the order at the earliest and confirm using Haspatal 360 App";
// return $content;
        // $emailResponse = $this->sendotomail($pharmacy_id,$content);
        // $emailResponsethird = $this->send_mail1( $pharmacy_id,$content);
        $emailResponsethird = $this->send_mail1( $pharmacy_id,$order360->id);
        $emailResponsethirds = $this->send_mail2( $pharmacy_id,$order360->id);
        $mobileResponse = $this->sendotomobile($pharmacy_id,$content);
        $notification = $this->sendFCM($pharmacy_id,$content);
        // return $emailResponsethird;
        return response()->json([
            //'status' => '4',
            'emailresponse'=>$emailResponsethird,
            'order360' =>$order360,
            'mobileResponse' =>$mobileResponse,
            'notification' =>$notification,
            'message' =>'Order placed successfully !'
          ]); 
        // return $this->sendResponse($order360->toArray(), $emailResponsethird, 'Order placed successfully !');
    }
    public function sharehaspatalpres(Request $request)
    {
        $input = $request->all();

      
        $input['haspatal_prescription'] = 3;

        $input['created_by'] = Auth::user()->id;
        $input['updated_by'] = Auth::user()->id;
        $users = User::findOrFail($request->user_id);
        if($users->role_id){
            $input['role_id'] =  $users->role_id;
        }
       
       
        $order360 = $this->order360Repository->create($input);
        $order_id =
        "ID". $order360->patient_id.
         "00".
         $order360->id;
     
      
        $updateOrder = Order_360::where('id', $order360->id)
        ->update(['Order_id' =>  $order_id]);
        // return $updateOrder;
        $updated_at=$order360->updated_at;
        
       $date= date('d/m/Y H:i', strtotime($updated_at));
    //    $time= Carbon::parse($updated_at)->format('H:i:s');
        // return $date;
        $pharmacy_id= $order360->user_id;
        $user = User::findOrFail($pharmacy_id);
        $content= "Order Number ".$order_id." has been placed for you at ".$date."
Kindly complete the order at the earliest and confirm using Haspatal 360 App";
// return $content;
        // $emailResponse = $this->sendotomail($pharmacy_id,$content);
        // $emailResponsethird = $this->send_mail1( $pharmacy_id,$content);
        $emailResponsethird = $this->send_mail1( $pharmacy_id,$order360->id);
        $emailResponsethirds = $this->send_mail2( $pharmacy_id,$order360->id);
        // return  $emailResponsethirds;
        $mobileResponse = $this->sendotomobile($pharmacy_id,$content);
        $notification = $this->sendFCM($pharmacy_id,$content);
        // return $emailResponsethird;
        return response()->json([
            //'status' => '4',
            'emailresponse'=>$emailResponsethird,
            'emailresponse1'=>$emailResponsethirds,
            'order360' =>$order360,
            'mobileResponse' =>$mobileResponse,
            'notification' =>$notification,
            'message' =>'Order placed successfully !'
          ]); 
        // return $this->sendResponse($order360->toArray(), $emailResponsethird, 'Order placed successfully !');
    }
    public function sendotomobile($id,$content)
    {
        // $otp="55";
         $user = User::findOrFail($id);
         $str2 = substr($user->mobile, 3);
        //  return  $str2;
         $mobile= $str2;
        
         $curl = curl_init();
         curl_setopt_array($curl, array(
             CURLOPT_URL => "http://sms.bulksmsind.in/sendSMS?username=haspatal&message=".urlencode(  $content)."&sendername=MASUPP&smstype=TRANS&numbers=". $mobile."&apikey=206f20fc-d4d5-4a33-ad06-10f453b8ab4a",
             CURLOPT_RETURNTRANSFER => true,
             CURLOPT_ENCODING => true,
             CURLOPT_MAXREDIRS => 10,
             CURLOPT_TIMEOUT => 30,
             CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
             CURLOPT_CUSTOMREQUEST => "GET",
         ));
         $response = curl_exec($curl);
        //  return  $response;
         $err = curl_error($curl);
         curl_close($curl);
         if ($err) {
             //echo "cURL Error #:" . $err;
             return response()->json(["status" => true,"message" => "Order Message Not Send","data"=>$err]);
         } else {
             //echo $response;exit();
             return response()->json(["status" => true,"message" => "Order Message in your mobile no"]);
         }
    }
    public function send_mail1($id,$orderid)
    
    {
        $user = User::findOrFail($id);
        $Order_360 = Order_360::join('patient_details', 'Order_360.patient_id', '=', 'patient_details.id')
              ->where('Order_360.id',$orderid)
               ->select('Order_360.*','patient_details.first_name as patient_name','patient_details.mobile as mobile','patient_details.address as patient_address','patient_details.email as patient_email')->first();
            //    return  $Order_360;
               $subject =
              "Order ID  ". $Order_360->Order_id. "  dated ". date('d/m/Y H:i', strtotime($Order_360->updated_at))." has been placed" ;
        // $mailmessage= $content;
        $mailmessage= '<!DOCTYPE html>
        <html lang="en">
            <head>
                <title>Dr email template</title>
                <meta charset="utf-8">
                <!-- CSS only -->
                <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
                <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
                <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900,900i&display=swap" rel="stylesheet">
                <script src="https://kit.fontawesome.com/fa5e54101c.js" crossorigin="anonymous"></script>
                <!-- JavaScript Bundle with Popper -->
        
                <!-- Theme Style -->
                <!-- <link rel="stylesheet" href="/assets/css/bootstrap.min.css"> -->
                <style>
                    #page-wrap{
                        max-width: 800px; 
                        margin: 0 auto;
                        font-family: "Roboto";
                        /* padding: 25px 0; */
                    }
                    p,h5,h6{
                        color: #000;
                        font-family: "Roboto";
                        margin: 0;
                    }
                    p span{
                        display: inline-block;
                    }
                </style>
            </head>
            <body>
                <div id="page-wrap">
                    <div class="top">
                    
                
                        <p style="margin-bottom: 25px;">You have received an order with the following details::</p>
                
                        <p><span style="min-width: 180px;">Order ID</span>:<span style="margin-left: 25px;">'.$Order_360->Order_id.'</span></p>
                        <p><span style="min-width: 180px;">Order Date	</span>:<span style="margin-left: 25px;">'.date('d/m/Y H:i', strtotime($Order_360->updated_at)).'</span></p>
                        <p><span style="min-width: 180px;">Patient Name	</span>:<span style="margin-left: 25px;">'.$Order_360->patient_name.'</span></p>
            
              
                        <p style="margin-bottom: 25px; margin-top: 25px;">Kindly open Haspatal 360 App and confirm the acceptance within 25 minutes.</p>
                        <p style="margin-bottom: 25px;">For any query, please contact Haspatal Support Team at: +91-124-405 56 56 or email us at network@haspatal.com</p>
                        <p style="margin-bottom: 25px;">Team Haspatal 360</p>
                    </div>
                </div>
            </body>
        </html>
        
        
'                 ;
        $data['personalizations'] = array("0" => array('to' => array('0' => array('email' => $user->email))));

        $data['from'] = array('email' => 'system@haspatal.com');

        $data['subject'] = $subject;
        $data['content'] = array("0" =>array('type' =>' text/html','value' => $mailmessage ));

        //echo json_encode($data);exit();

        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://api.sendgrid.com/v3/mail/send",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => json_encode($data),
          CURLOPT_HTTPHEADER => array(
            "authorization: Bearer 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",
            "cache-control: no-cache",
            "content-type: application/json",
            "postman-token: b8c0781f-af48-90cc-1c5b-76f80e0b4be0"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            return "Send OTP in your emailId";
        }
    }
    public function send_mail2($id,$orderid)
    
    {
        // $user = User::findOrFail($id);
        $Order_360 = Order_360::join('patient_details', 'Order_360.patient_id', '=', 'patient_details.id')
        ->join('business_register', 'Order_360.user_id', '=', 'business_register.user_id')
              ->where('Order_360.id',$orderid)
               ->select('Order_360.*','patient_details.first_name as patient_name','patient_details.mobile as mobile','patient_details.address as patient_address','patient_details.email as patient_email','business_register.b_name as b_name')->first();
            //    return  $Order_360;
               $subject =
              "Your Order ID  ". $Order_360->Order_id.  "    dated ". date('d/m/Y H:i', strtotime($Order_360->updated_at))." has been placed" ;
        // $mailmessage= $content;
        $mailmessage= '<!DOCTYPE html>
        <html lang="en">
            <head>
                <title>Dr email template</title>
                <meta charset="utf-8">
                <!-- CSS only -->
                <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
                <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
                <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900,900i&display=swap" rel="stylesheet">
                <script src="https://kit.fontawesome.com/fa5e54101c.js" crossorigin="anonymous"></script>
                <!-- JavaScript Bundle with Popper -->
        
                <!-- Theme Style -->
                <!-- <link rel="stylesheet" href="/assets/css/bootstrap.min.css"> -->
                <style>
                    #page-wrap{
                        max-width: 800px; 
                        margin: 0 auto;
                        font-family: "Roboto";
                        /* padding: 25px 0; */
                    }
                    p,h5,h6{
                        color: #000;
                        font-family: "Roboto";
                        margin: 0;
                    }
                    p span{
                        display: inline-block;
                    }
                </style>
            </head>
            <body>
                <div id="page-wrap">
                    <div class="top">
                    
                
                        <p style="margin-bottom: 25px;">Your order with the following details has been placed:</p>
                
                        <p><span style="min-width: 180px;">Order ID</span>:<span style="margin-left: 25px;">'.$Order_360->Order_id.'</span></p>
                        <p><span style="min-width: 180px;">Order Date	</span>:<span style="margin-left: 25px;">'.date('d/m/Y H:i', strtotime($Order_360->updated_at)).'</span></p>
                        <p><span style="min-width: 120px;">Service</span>:<span style="margin-left: 25px;">'.$Order_360->b_name.'</span></p>
            
              
                        <p style="margin-bottom: 25px; margin-top: 25px;">Kindly open Haspatal 360 App and confirm the acceptance within 25 minutes.</p>
                        <p style="margin-bottom: 25px;">For any query, please contact Haspatal Support Team at: +91-124-405 56 56 or email us at network@haspatal.com</p>
                        <p style="margin-bottom: 25px;">Team Haspatal Care</p>
                    </div>
                </div>
            </body>
        </html>
        
        
'                 ;
        $data['personalizations'] = array("0" => array('to' => array('0' => array('email' => $Order_360->patient_email))));

        $data['from'] = array('email' => 'system@haspatal.com');

        $data['subject'] = $subject;
        $data['content'] = array("0" =>array('type' =>' text/html','value' => $mailmessage ));

        //echo json_encode($data);exit();

        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://api.sendgrid.com/v3/mail/send",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => json_encode($data),
          CURLOPT_HTTPHEADER => array(
            "authorization: Bearer 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",
            "cache-control: no-cache",
            "content-type: application/json",
            "postman-token: b8c0781f-af48-90cc-1c5b-76f80e0b4be0"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            return "Send OTP in your emailId";
        }
    }
    // public function sendotomail($id,$content)
    // {
    //     $user = User::findOrFail($id);
    // $sendmail=  Mail::send('mail', $content, function ($m) use ($user) {
    //         $m->from('system@haspatal.com', 'Haspatal');

    //         $m->to($user->email, $user->email)->subject('OTP From Haspatal!');
    //     });
    //     return "Otp Sended Through Mail";
    // }
    public function store(Request $request)
    {
        
         $book =  DB::table('Order_360')->where('book_id', $request->book_id)->orderBy('book_id', 'desc')->first();
        if(!empty($book)){
           return $this->sendMessage('Order already placed');

        }else
        {
            $input = $request->all();
            $input['created_by'] = Auth::user()->id;
            $input['updated_by'] = Auth::user()->id;
            $order360 = $this->order360Repository->create($input);

            return $this->sendResponse($order360->toArray(), 'Order placed successfully !');
        }
    }

    public function order_360_list(Request $request)
      {
     
       $order_360_list = Order_360::join('prescription_details', 'Order_360.book_id', '=', 'prescription_details.booking_id')
                                   ->join('doctor_details', 'Order_360.doctor_id', '=', 'doctor_details.id')
                                   ->join('patient_details', 'Order_360.patient_id', '=', 'patient_details.id')
                                   ->join('business_register', 'Order_360.user_id', '=', 'business_register.user_id')
                                   ->join('users', 'Order_360.user_id', '=', 'users.id')
                                   ->join('roles', 'Order_360.role_id', '=', 'roles.id')
                                   ->where('Order_360.patient_id',$request->patient_id)
                                   ->where('Order_360.order_status','=','1')
                                   ->select('Order_360.*','prescription_details.*','doctor_details.first_name','doctor_details.last_name','patient_details.first_name as patient_name','users.first_name as 360_first_name','users.last_name as 360_last_name','roles.name','business_register.b_name')
                               ->get();
    
                                           
     if ($order_360_list) {
           return response()->json(["status" => true,"massage"=>"Order list retrieved successfully","data" => $order_360_list]);
       }else{
           return response()->json(["status" => false,"data" => $order_360_list]);
       }
   }

    public function order_360_list_by_prients(Request $request)
    {
        $order_360_list = Order_360::where('Order_360.patient_id',$request->patient_id)
                                // ->where('Order_360.order_status','0')
                                ->where('Order_360.role_id',$request->role_id)
                                ->leftJoin('doctor_details', 'Order_360.doctor_id', '=', 'doctor_details.id')
                                ->join('patient_details', 'Order_360.patient_id', '=', 'patient_details.id')
                                ->join('business_register', 'Order_360.user_id', '=', 'business_register.user_id')
                                ->join('roles', 'Order_360.role_id', '=', 'roles.id')
                                //->leftJoin('prescription_details', 'Order_360.book_id', '=', 'prescription_details.booking_id')
                                ->select('Order_360.*','Order_360.id as main_id','Order_360.user_id as user_id','Order_360.patient_id as patientid','Order_360.doctor_id as doctorid','Order_360.book_id as bookid','Order_360.created_at as order_date','Order_360.role_id as roleid','Order_360.order_status as orderstatus','doctor_details.first_name','doctor_details.last_name','patient_details.first_name as patient_name','roles.name','business_register.b_name')
                                ->orderBy('Order_360.updated_at','DESC')->get();

                                $path =  'https://app.kizakuhaspatal.co.in/media/prescription_image/';
                                if ($order_360_list) {
                                foreach ($order_360_list as $prescription) {
                                                   
                                    
                                    $prescriptions_details =  Prescription_details::where('booking_id', $prescription->book_id)
                                    ->first(); 
                                    if($prescriptions_details){
                                        $prescrptiondate= $prescriptions_details->created_at;
                                        $prescription['prescrptiondate']=$prescrptiondate->format('d-m-Y');
                                        $prescription['prescriptions_details']= $prescriptions_details;
                                        if($prescriptions_details->labtest){
                                        $prescription['lab']=json_decode($prescriptions_details->labtest);
                                        }
                                        if($prescriptions_details->theraphy){
                                        $prescription['theraphy']=json_decode($prescriptions_details->theraphy);
                                        }
                                        if($prescriptions_details->home){
                                        $prescription['home']=json_decode($prescriptions_details->home);
                                        }
                                        if($prescriptions_details->couns){
                                        $prescription['couns']=json_decode($prescriptions_details->couns);
                                        }
                                        if($prescriptions_details->imaging){
                                        $prescription['imaging']=json_decode($prescriptions_details->imaging);
                                        }
                                        if($prescriptions_details->medicine){
                                        $prescription['medicine']=json_decode($prescriptions_details->medicine);
                                        }
                                    }
                                  
                                   
                                }    
                            }   



    
                                           
        if ($order_360_list) {
            return response()->json(["status" => true,"massage"=>"Order list retrieved successfully","data" => $order_360_list]);
        }else{
            return response()->json(["status" => false,"data" => $order_360_list]);
        }
   }

    public function cancel_order_update(Request $request)
    {

        $complate_order_update =  DB::table('Order_360')->where('id',$request->main_id)->update(['order_status' => 4]);
        if ($complate_order_update) {
            $cancel_order_update1 = $this->send_mailcanceeled1( $request->main_id);
            $cancel_order_update2 = $this->send_mailcanceeled2( $request->main_id);
            return response()->json(["status" => true,"massage"=>"Order Cancelled Successfully","data" => $complate_order_update]);
        }else{
            return response()->json(["status" => false,"data" => $complate_order_update]);
        }

    }
    public function send_mailcanceeled1($orderid)
    
    {
        // $user = User::findOrFail($id);
        $Order_360 = Order_360::join('patient_details', 'Order_360.patient_id', '=', 'patient_details.id')
        ->join('business_register', 'Order_360.user_id', '=', 'business_register.user_id')
              ->where('Order_360.id',$orderid)
               ->select('Order_360.*','patient_details.first_name as patient_name','patient_details.mobile as mobile','patient_details.address as patient_address','patient_details.email as patient_email','business_register.b_name as b_name')->first();
                $user = User::findOrFail($Order_360->user_id);
            //    return  $Order_360;
               $subject =
              "Order ID  ". $Order_360->Order_id. "  dated ". date('d/m/Y H:i', strtotime($Order_360->updated_at))." has been cancelled" ;
        // $mailmessage= $content;
        $mailmessage= '<!DOCTYPE html>
        <html lang="en">
            <head>
                <title>Dr email template</title>
                <meta charset="utf-8">
                <!-- CSS only -->
                <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
                <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
                <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900,900i&display=swap" rel="stylesheet">
                <script src="https://kit.fontawesome.com/fa5e54101c.js" crossorigin="anonymous"></script>
                <!-- JavaScript Bundle with Popper -->
        
                <!-- Theme Style -->
                <!-- <link rel="stylesheet" href="/assets/css/bootstrap.min.css"> -->
                <style>
                    #page-wrap{
                        max-width: 800px; 
                        margin: 0 auto;
                        font-family: "Roboto";
                        /* padding: 25px 0; */
                    }
                    p,h5,h6{
                        color: #000;
                        font-family: "Roboto";
                        margin: 0;
                    }
                    p span{
                        display: inline-block;
                    }
                </style>
            </head>
            <body>
                <div id="page-wrap">
                    <div class="top">
                    
                
                        <p style="margin-bottom: 25px;">Your order with the following details has been cancelled by the Patient.</p>
                
                        <p><span style="min-width: 180px;">Order ID</span>:<span style="margin-left: 25px;">'.$Order_360->Order_id.'</span></p>
                        <p><span style="min-width: 180px;">Order Date	</span>:<span style="margin-left: 25px;">'.date('d/m/Y H:i', strtotime($Order_360->updated_at)).'</span></p>
                        <p><span style="min-width: 180px;">Patient Name	</span>:<span style="margin-left: 25px;">'.$Order_360->patient_name.'</span></p>
            
              
                        <p style="margin-bottom: 25px; margin-top: 25px;">Kindly be advised not to proceed to deliver this order.</p>
                        <p style="margin-bottom: 25px; margin-top: 25px;"">For any query, please contact Haspatal Support Team at: +91-124-405 56 56 or email us at network@haspatal.com</p>
                        <p style="margin-bottom: 25px;  margin-top: 25px;"">Team Haspatal 360</p>
                    </div>
                </div>
            </body>
        </html>
        
        
'                 ;
        $data['personalizations'] = array("0" => array('to' => array('0' => array('email' => $user->email))));

        $data['from'] = array('email' => 'system@haspatal.com');

        $data['subject'] = $subject;
        $data['content'] = array("0" =>array('type' =>' text/html','value' => $mailmessage ));

        //echo json_encode($data);exit();

        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://api.sendgrid.com/v3/mail/send",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => json_encode($data),
          CURLOPT_HTTPHEADER => array(
            "authorization: Bearer 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",
            "cache-control: no-cache",
            "content-type: application/json",
            "postman-token: b8c0781f-af48-90cc-1c5b-76f80e0b4be0"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            return "Send OTP in your emailId";
        }
    }

    public function send_mailcanceeled2($orderid)
    
    {
        // $user = User::findOrFail($id);
        $Order_360 = Order_360::join('patient_details', 'Order_360.patient_id', '=', 'patient_details.id')
        ->join('business_register', 'Order_360.user_id', '=', 'business_register.user_id')
              ->where('Order_360.id',$orderid)
               ->select('Order_360.*','patient_details.first_name as patient_name','patient_details.mobile as mobile','patient_details.address as patient_address','patient_details.email as patient_email','business_register.b_name as b_name')->first();
            //    return  $Order_360;
               $subject =
              "Your Order ID  ". $Order_360->Order_id.  "    dated ". date('d/m/Y H:i', strtotime($Order_360->updated_at))." has been cancelled" ;
        // $mailmessage= $content;
        $mailmessage= '<!DOCTYPE html>
        <html lang="en">
            <head>
                <title>Dr email template</title>
                <meta charset="utf-8">
                <!-- CSS only -->
                <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
                <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
                <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900,900i&display=swap" rel="stylesheet">
                <script src="https://kit.fontawesome.com/fa5e54101c.js" crossorigin="anonymous"></script>
                <!-- JavaScript Bundle with Popper -->
        
                <!-- Theme Style -->
                <!-- <link rel="stylesheet" href="/assets/css/bootstrap.min.css"> -->
                <style>
                    #page-wrap{
                        max-width: 800px; 
                        margin: 0 auto;
                        font-family: "Roboto";
                        /* padding: 25px 0; */
                    }
                    p,h5,h6{
                        color: #000;
                        font-family: "Roboto";
                        margin: 0;
                    }
                    p span{
                        display: inline-block;
                    }
                </style>
            </head>
            <body>
                <div id="page-wrap">
                    <div class="top">
                    
                
                        <p style="margin-bottom: 25px;">Your order with the following details has been cancelled successfully.</p>
                
                        <p><span style="min-width: 180px;">Order ID</span>:<span style="margin-left: 25px;">'.$Order_360->Order_id.'</span></p>
                        <p><span style="min-width: 180px;">Order Date	</span>:<span style="margin-left: 25px;">'.date('d/m/Y H:i', strtotime($Order_360->updated_at)).'</span></p>
                        <p><span style="min-width: 120px;">Service</span>:<span style="margin-left: 25px;">'.$Order_360->b_name.'</span></p>
            
              
               
                        <p style="margin-bottom: 25px;">For any query, please contact Haspatal Support Team at: +91-124-405 56 56 or email us at care@haspatal.com</p>
                        <p style="margin-bottom: 25px;">Team Haspatal Care</p>
                    </div>
                </div>
            </body>
        </html>
        
        
'                 ;
        $data['personalizations'] = array("0" => array('to' => array('0' => array('email' => $Order_360->patient_email))));

        $data['from'] = array('email' => 'system@haspatal.com');

        $data['subject'] = $subject;
        $data['content'] = array("0" =>array('type' =>' text/html','value' => $mailmessage ));

        //echo json_encode($data);exit();

        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://api.sendgrid.com/v3/mail/send",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => json_encode($data),
          CURLOPT_HTTPHEADER => array(
            "authorization: Bearer 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",
            "cache-control: no-cache",
            "content-type: application/json",
            "postman-token: b8c0781f-af48-90cc-1c5b-76f80e0b4be0"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            return "Send OTP in your emailId";
        }
    }
    public function order_view(Request $request)
      {

         $order_view = Order_360::leftJoin('doctor_details', 'Order_360.doctor_id', '=', 'doctor_details.id')
                                ->join('patient_details', 'Order_360.patient_id', '=', 'patient_details.id')
                                ->where('Order_360.user_id',$request->user_id)
                                ->where('Order_360.order_status','=','0')

                                       ->select('Order_360.*','doctor_details.first_name','doctor_details.last_name','doctor_details.address as doctor_address','patient_details.first_name as patient_name','patient_details.mobile as mobile','patient_details.address as patient_address','doctor_details.main_mobile as doctor_mobile','doctor_details.d_uni_id as d_uni_id')
                                ->orderBy('updated_at','DESC')->get();
       
       
                                // $path =  'http://app.haspatal.com/public/media/prescription_image/';

                                $path =  'https://app.kizakuhaspatal.co.in/media/prescription_image/';
                                if ($order_view) {
                                    foreach ($order_view as $prescription) {
                                                       
                                        
                                        $prescriptions_details =  Prescription_details::where('booking_id', $prescription->book_id)
                                        ->first(); 
                                        if($prescriptions_details){
                                            $prescrptiondate= $prescriptions_details->created_at;
                                            $prescription['prescrptiondate']=$prescrptiondate->format('d-m-Y');
                                            $prescription['prescriptions_details']= $prescriptions_details;
                                            if($prescriptions_details->labtest){
                                            $prescription['lab']=json_decode($prescriptions_details->labtest);
                                            }
                                            if($prescriptions_details->theraphy){
                                            $prescription['theraphy']=json_decode($prescriptions_details->theraphy);
                                            }
                                            if($prescriptions_details->home){
                                            $prescription['home']=json_decode($prescriptions_details->home);
                                            }
                                            if($prescriptions_details->couns){
                                            $prescription['couns']=json_decode($prescriptions_details->couns);
                                            }
                                            if($prescriptions_details->imaging){
                                            $prescription['imaging']=json_decode($prescriptions_details->imaging);
                                            }
                                            if($prescriptions_details->medicine){
                                            $prescription['medicine']=json_decode($prescriptions_details->medicine);
                                            }
                                        }
                                      
                                       
                                    }    
                                } 
          if ($order_view) {
            return response()->json(["status" => true,"massage"=>"Order retrieved successfully","data" => $order_view,"path" => $path]);
        }else{
            return response()->json(["status" => false,"data" => $order_view]);
        }

      }
      public function allorder(Request $request)
      {

         $order_view = Order_360::leftJoin('doctor_details', 'Order_360.doctor_id', '=', 'doctor_details.id')
                                ->join('patient_details', 'Order_360.patient_id', '=', 'patient_details.id')
                                ->where('Order_360.user_id',$request->user_id)
                               
                                ->select('Order_360.*','doctor_details.first_name','doctor_details.last_name','doctor_details.address as doctor_address','patient_details.first_name as patient_name','patient_details.mobile as mobile','patient_details.address as patient_address','doctor_details.main_mobile as doctor_mobile','doctor_details.d_uni_id as d_uni_id')
                                ->orderBy('updated_at','DESC')->get();
       
                                // $path =  'http://app.haspatal.com/public/media/prescription_image/';
                                $path =  'https://app.kizakuhaspatal.co.in/media/prescription_image/';
                                if ($order_view) {
                                foreach ($order_view as $prescription) {
                                                   
                                    
                                    $prescriptions_details =  Prescription_details::where('booking_id', $prescription->book_id)
                                    ->first(); 
                                    if($prescriptions_details){
                                        $prescrptiondate= $prescriptions_details->created_at;
                                        $prescription['prescrptiondate']=$prescrptiondate->format('d-m-Y');
                                        $prescription['prescriptions_details']= $prescriptions_details;
                                        if($prescriptions_details->labtest){
                                        $prescription['lab']=json_decode($prescriptions_details->labtest);
                                        }
                                        if($prescriptions_details->theraphy){
                                        $prescription['theraphy']=json_decode($prescriptions_details->theraphy);
                                        }
                                        if($prescriptions_details->home){
                                        $prescription['home']=json_decode($prescriptions_details->home);
                                        }
                                        if($prescriptions_details->couns){
                                        $prescription['couns']=json_decode($prescriptions_details->couns);
                                        }
                                        if($prescriptions_details->imaging){
                                        $prescription['imaging']=json_decode($prescriptions_details->imaging);
                                        }
                                        if($prescriptions_details->medicine){
                                        $prescription['medicine']=json_decode($prescriptions_details->medicine);
                                        }
                                    }
                                  
                                   
                                }    
                            }   
          if ($order_view) {

            return response()->json(["status" => true,"massage"=>"Order retrieved successfully","data" => $order_view,"path" => $path]);
        }else{
            return response()->json(["status" => false,"data" => $order_view]);
        }

      }
     public function complate_order_update(Request $request)
      {

        $complate_order_update =  DB::table('Order_360')->where('book_id',$request->book_id)->update(['order_status' => 1]);
        if ($complate_order_update) {
            return response()->json(["status" => true,"massage"=>"Order Done successfully","data" => $complate_order_update]);
        }else{
            return response()->json(["status" => false,"data" => $complate_order_update]);
        }

      }
      public function pharmacy_orderaccept(Request $request)
      {

        $book = Wallet_360::where('user_id', $request->user_id)->orderBy('user_id', 'desc')->first();
        if($book->wa_amount<=0){
            return response()->json(["status" => false,"massage"=>"Add Fund","data" => $book->wa_amount]);  
        }
        $users_role = User::where('id', $request->user_id)->orderBy('id', 'desc')->first();
        if($users_role->role_id=="11"){
            $order_status=1;
        }else{
            $order_status=9;
        }
        $complate_order_update =  DB::table('Order_360')->where('patient_id',$request->patient_id)->where('created_at',$request->created_at)->update(['order_status' => $order_status]);
        if($complate_order_update==1){
        $book = Wallet_360::where('user_id', $request->user_id)->orderBy('user_id', 'desc')->first();
        // return $book ;
        
        if(!empty($book)){
            if($book->wa_amount<=0){
                return response()->json(["status" => false,"massage"=>"Add Fund","data" => $complate_order_update]);  
            }
            $users = User::where('id', $request->user_id)->orderBy('id', 'desc')->first();
            if($users->role_id){

           
            $role_id=$users->role_id;
            if($role_id=="11"){
                $charge='10';
            } 
            else if($role_id=="12"){ 
                $charge='50';

          }
             else if($role_id=="13"){ 
                $charge='150';

             }
             else if($role_id=="16"){ 
                $charge='250';

             }
             else if($role_id=="14"){ 
                $charge='250';

             }
             else if($role_id=="17"){ 
                $charge='250';

             } else{
                $charge='10';
            }
          }else{
              $charge='10'; 
          }
            $wallet=$book->wa_amount;
            $book->wa_amount =   (float)$wallet -(float) $charge;
       
            $book->save();
                

        }
    }else{
        return response()->json(["status" => false,"massage"=>"Record Not Found","data" => $complate_order_update]);  
    }
        if ($complate_order_update) {
            return response()->json(["status" => true,"massage"=>"Order Done successfully","data" => $complate_order_update]);
        }else{
            return response()->json(["status" => false,"data" => $complate_order_update]);
        }

      }
      public function cancel_order_pharamcyupdate(Request $request)
    {

        $complate_order_update =  DB::table('Order_360')->where('patient_id',$request->patient_id)->where('created_at',$request->created_at)->update(['order_status' => 2]);
        if ($complate_order_update) {
            return response()->json(["status" => true,"massage"=>"Order Cancel successfully","data" => $complate_order_update]);
        }else{
            return response()->json(["status" => false,"data" => $complate_order_update]);
        }

    }
      public function today_order_list(Request $request)
      {
        $startDate =  Carbon::parse($request->s_date)->startOfDay(); 
        $endDate =  Carbon::now()->endOfDay();
        $user_id = Auth::user()->id;
        $today_order_list = Order_360::leftJoin('doctor_details', 'Order_360.doctor_id', '=', 'doctor_details.id')
                                   ->join('patient_details', 'Order_360.patient_id', '=', 'patient_details.id')
                                   ->whereBetween('Order_360.created_at', [$startDate, $endDate])
                                     ->where('Order_360.user_id',$user_id)
                                     ->where('Order_360.order_status','=','0')
                                     ->select('Order_360.*','doctor_details.first_name','doctor_details.last_name','doctor_details.address as doctor_address','patient_details.first_name as patient_name','patient_details.mobile as mobile','patient_details.address as patient_address','doctor_details.main_mobile as doctor_mobile','doctor_details.d_uni_id as d_uni_id')
                                     ->orderBy('updated_at','DESC')->get();
                                    //  $path =  'http://app.haspatal.com/public/media/prescription_image/';
                                    $path =  'https://app.kizakuhaspatal.co.in/media/prescription_image/';
                                            if ($today_order_list) {
                                foreach ($today_order_list as $prescription) {
                                                   
                                    
                                    $prescriptions_details =  Prescription_details::where('booking_id', $prescription->book_id)
                                    ->first(); 
                                    if($prescriptions_details){
                                        $prescrptiondate= $prescriptions_details->created_at;
                                        $prescription['prescrptiondate']=$prescrptiondate->format('d-m-Y');
                                        $prescription['prescriptions_details']= $prescriptions_details;
                                        if($prescriptions_details->labtest){
                                        $prescription['lab']=json_decode($prescriptions_details->labtest);
                                        }
                                        if($prescriptions_details->theraphy){
                                        $prescription['theraphy']=json_decode($prescriptions_details->theraphy);
                                        }
                                        if($prescriptions_details->home){
                                        $prescription['home']=json_decode($prescriptions_details->home);
                                        }
                                        if($prescriptions_details->couns){
                                        $prescription['couns']=json_decode($prescriptions_details->couns);
                                        }
                                        if($prescriptions_details->imaging){
                                        $prescription['imaging']=json_decode($prescriptions_details->imaging);
                                        }
                                        if($prescriptions_details->medicine){
                                        $prescription['medicine']=json_decode($prescriptions_details->medicine);
                                        }
                                    }
                                  
                                   
                                }    
                            }  
                                            
      if ($today_order_list) {
            return response()->json(["status" => true,"massage"=>"Today Order list retrieved successfully","data" => $today_order_list,"path" => $path]);
        }else{
            return response()->json(["status" => false,"data" => '']);
        }
    }
     public function complate_order_list(Request $request)
      {
       
    //     $complate_order_list = Order_360::leftJoin('prescription_details', 'Order_360.book_id', '=', 'prescription_details.booking_id')
    //                                 ->leftJoin('doctor_details', 'Order_360.doctor_id', '=', 'doctor_details.id')
    //                                 ->join('patient_details', 'Order_360.patient_id', '=', 'patient_details.id')
    //                                 ->where('Order_360.user_id',$request->user_id)
    //                                 ->where('Order_360.order_status','=','1')
    //                                 ->select('Order_360.*','prescription_details.*','doctor_details.first_name','doctor_details.last_name','patient_details.first_name as patient_name','patient_details.mobile as mobile')
    //                             ->get();
    //                             $path =  env('APP_URL') . 'public/media/prescription_image/';
                                            
    //   if ($complate_order_list) {
    //         return response()->json(["status" => true,"massage"=>"Complete Order list retrieved successfully","data" => $complate_order_list,"path" => $path]);
    //     }else{
    //         return response()->json(["status" => false,"data" => $complate_order_list]);
    //     }
    $order_view = Order_360::leftJoin('doctor_details', 'Order_360.doctor_id', '=', 'doctor_details.id')
    ->join('patient_details', 'Order_360.patient_id', '=', 'patient_details.id')
    ->where('Order_360.user_id',$request->user_id)
    ->where('Order_360.order_status','=','9')

    ->select('Order_360.*','doctor_details.first_name','doctor_details.last_name','doctor_details.address as doctor_address','patient_details.first_name as patient_name','patient_details.mobile as mobile','patient_details.address as patient_address','doctor_details.main_mobile as doctor_mobile','doctor_details.d_uni_id as d_uni_id')
                                     ->orderBy('updated_at','DESC')->get();

    // $path =  'http://app.haspatal.com/public/media/prescription_image/';
    $path =  'https://app.kizakuhaspatal.co.in/media/prescription_image/';
    if ($order_view) {
        foreach ($order_view as $prescription) {
                           
            
            $prescriptions_details =  Prescription_details::where('booking_id', $prescription->book_id)
            ->first(); 
            if($prescriptions_details){
                $prescrptiondate= $prescriptions_details->created_at;
                $prescription['prescrptiondate']=$prescrptiondate->format('d-m-Y');
                $prescription['prescriptions_details']= $prescriptions_details;
                if($prescriptions_details->labtest){
                $prescription['lab']=json_decode($prescriptions_details->labtest);
                }
                if($prescriptions_details->theraphy){
                $prescription['theraphy']=json_decode($prescriptions_details->theraphy);
                }
                if($prescriptions_details->home){
                $prescription['home']=json_decode($prescriptions_details->home);
                }
                if($prescriptions_details->couns){
                $prescription['couns']=json_decode($prescriptions_details->couns);
                }
                if($prescriptions_details->imaging){
                $prescription['imaging']=json_decode($prescriptions_details->imaging);
                }
                if($prescriptions_details->medicine){
                $prescription['medicine']=json_decode($prescriptions_details->medicine);
                }
            }
          
           
        }    
    }  
    
if ($order_view) {
return response()->json(["status" => true,"massage"=>"Order retrieved successfully","data" => $order_view,"path" => $path]);
}else{
return response()->json(["status" => false,"data" => $order_view]);
}
    }






    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/order360s/{id}",
     *      summary="Display the specified Order_360",
     *      tags={"Order_360"},
     *      description="Get Order_360",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Order_360",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Order_360"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Order_360 $order360 */
        $order360 = $this->order360Repository->find($id);

        if (empty($order360)) {
            return $this->sendError('Order 360 not found');
        }

        return $this->sendResponse($order360->toArray(), 'Order 360 retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateOrder_360APIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/order360s/{id}",
     *      summary="Update the specified Order_360 in storage",
     *      tags={"Order_360"},
     *      description="Update Order_360",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Order_360",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Order_360 that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Order_360")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Order_360"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateOrder_360APIRequest $request)
    {
        $input = $request->all();

        /** @var Order_360 $order360 */
        $order360 = $this->order360Repository->find($id);

        if (empty($order360)) {
            return $this->sendError('Order 360 not found');
        }

        $order360 = $this->order360Repository->update($input, $id);

        return $this->sendResponse($order360->toArray(), 'Order_360 updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/order360s/{id}",
     *      summary="Remove the specified Order_360 from storage",
     *      tags={"Order_360"},
     *      description="Delete Order_360",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Order_360",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Order_360 $order360 */
        $order360 = $this->order360Repository->find($id);

        if (empty($order360)) {
            return $this->sendError('Order 360 not found');
        }

        $order360->delete();

        return $this->sendSuccess('Order 360 deleted successfully');
    }
    function sendFCM($user_id,$message)
    {
        // $user_id="249";
        $user = User::findOrFail($user_id);
        $token=$user->mobile_token;
        $path_to_firebase_cm = 'https://fcm.googleapis.com/fcm/send';
        $fields = array(
            'to' => $token,
    
            'notification' => array('title' => 'Order Placed', 'body' => $message, 'sound' => 'default', 'click_action' => 'FCM_PLUGIN_ACTIVITY', 'icon' => 'fcm_push_icon'),
    
            'data' => array('title' => 'title', 'Order Placed' => $message, 'sound' => 'default', 'icon' => 'fcm_push_icon'),
        );
        $headers = array(
            'Authorization:key=' . 'AAAASwzcPiA:APA91bHC6kmNC8f05jmzD8n60NZ9HxqcdjzBY7IoHUulGliwaghHY6oFsK3qw7Q6c4G9GS97WTNR4xmoW4EqT3bI04gsdh6MtHOxa0ZDO3pryOCZeqaDkUQghmPDlGE_ebEfgcSH3DlE  ',
            'Content-Type:application/json'
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $path_to_firebase_cm); 
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); 
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

    public function medicine_cost_update(Request $request)
    {
        $Order_360 = Order_360::where('Order_id', $request->Order_id)->orderBy('id', 'desc')
        ->first();
        if( $Order_360){
            $Order_360->medicine_cost=$request->medicine_cost;
            $Order_360->discount_cost=$request->discount_cost;
            $Order_360->total_cost=$request->total_cost;
            $Order_360->delivery_slot=$request->delivery_slot;
            $Order_360->save(); 
            return response()->json(["status" => true,"data" => $Order_360, "message" => "Updated successfully"]);
        }
        else{
            return response()->json(["status" => false,"message" => "Not updated"]);
        }
    }
    public function package_details__update(Request $request)
    {
        $Order_360 = Order_360::where('Order_id', $request->Order_id)->orderBy('id', 'desc')
        ->first();
        if( $Order_360){
            $Order_360->bill_no=$request->bill_no;
            $Order_360->amount=$request->amount;
            $Order_360->order_pharmcy_no=$request->order_pharmcy_no;
            $Order_360->bill_copy=$request->bill_copy;
             /////packed status
            $Order_360->order_status=5;
            $Order_360->save(); 
            return response()->json(["status" => true,"data" => $Order_360, "message" => "Updated successfully"]);
        }
        else{
            return response()->json(["status" => false,"message" => "Not updated"]);
        }
    }
    public function Confirmpacckageupdate(Request $request)
    {
        $Order_360 = Order_360::where('Order_id', $request->Order_id)->orderBy('id', 'desc')
        ->first();
        if( $Order_360){
            $Order_360->patient_address=$request->patient_address;
            $Order_360->patient_phone=$request->patient_phone;
            $Order_360->dispatch_through=$request->dispatch_through;
            $Order_360->dispatch_phone=$request->dispatch_phone;
            $Order_360->Dispatch_time=$request->Dispatch_time;
            /////dispatched status
            $Order_360->order_status=6;
            $Order_360->save(); 
            
 
    
    
    
   
            return response()->json(["status" => true,"data" => $Order_360, "message" => "Updated successfully"]);
        }
        else{
            return response()->json(["status" => false,"message" => "Not updated"]);
        }
    }
    public function dispatchedorder(Request $request)
    {

       $order_view = Order_360::leftJoin('doctor_details', 'Order_360.doctor_id', '=', 'doctor_details.id')
                              ->join('patient_details', 'Order_360.patient_id', '=', 'patient_details.id')
                              ->where('Order_360.user_id',$request->user_id)
                              ->where('Order_360.order_status','=','6')

                                     ->select('Order_360.*','doctor_details.first_name','doctor_details.last_name','doctor_details.address as doctor_address','patient_details.first_name as patient_name','patient_details.mobile as mobile','patient_details.address as patient_address','doctor_details.main_mobile as doctor_mobile','doctor_details.d_uni_id as d_uni_id')
                              ->orderBy('updated_at','DESC')->get();
     
     
                              // $path =  'http://app.haspatal.com/public/media/prescription_image/';

                              $path =  'https://app.kizakuhaspatal.co.in/media/prescription_image/';
                              if ($order_view) {
                                  foreach ($order_view as $prescription) {
                                                     
                                      
                                      $prescriptions_details =  Prescription_details::where('booking_id', $prescription->book_id)
                                      ->first(); 
                                      if($prescriptions_details){
                                          $prescrptiondate= $prescriptions_details->created_at;
                                          $prescription['prescrptiondate']=$prescrptiondate->format('d-m-Y');
                                          $prescription['prescriptions_details']= $prescriptions_details;
                                          if($prescriptions_details->labtest){
                                          $prescription['lab']=json_decode($prescriptions_details->labtest);
                                          }
                                          if($prescriptions_details->theraphy){
                                          $prescription['theraphy']=json_decode($prescriptions_details->theraphy);
                                          }
                                          if($prescriptions_details->home){
                                          $prescription['home']=json_decode($prescriptions_details->home);
                                          }
                                          if($prescriptions_details->couns){
                                          $prescription['couns']=json_decode($prescriptions_details->couns);
                                          }
                                          if($prescriptions_details->imaging){
                                          $prescription['imaging']=json_decode($prescriptions_details->imaging);
                                          }
                                          if($prescriptions_details->medicine){
                                          $prescription['medicine']=json_decode($prescriptions_details->medicine);
                                          }
                                      }
                                    
                                     
                                  }    
                              } 
        if ($order_view) {
          return response()->json(["status" => true,"massage"=>"Order retrieved successfully","data" => $order_view,"path" => $path]);
      }else{
          return response()->json(["status" => false,"data" => $order_view]);
      }

    }
    public function orderstatusupdate(Request $request)
    {
        $Order_360 = Order_360::where('Order_id', $request->Order_id)->orderBy('id', 'desc')
        ->first();
        if( $Order_360){
            $Order_360->order_status=$request->order_status;
            $Order_360->save(); 
            
 
    
    
    
   
            return response()->json(["status" => true,"data" => $Order_360, "message" => "Updated successfully"]);
        }
        else{
            return response()->json(["status" => false,"message" => "Not updated"]);
        }
    }
    public function dispatchorderstatusupdate(Request $request)
    {
        $Order_360 = Order_360::where('Order_id', $request->Order_id)->orderBy('id', 'desc')
        ->first();
        if( $Order_360){
            $Order_360->dispatch_status=$request->dispatch_status;
            $Order_360->save(); 
            
 
    
    
    
   
            return response()->json(["status" => true,"data" => $Order_360, "message" => "Updated successfully"]);
        }
        else{
            return response()->json(["status" => false,"message" => "Not updated"]);
        }
    }
}
