<?php



namespace App\Http\Controllers\API;



use App\Http\Requests\API\CreateLabTestAPIRequest;

use App\Http\Requests\API\UpdateLabTestAPIRequest;

use App\Models\LabTest;

use App\Repositories\LabTestRepository;

use Illuminate\Http\Request;

use App\Http\Controllers\AppBaseController;

use Response;
use Auth;


/**

 * Class LabTestController

 * @package App\Http\Controllers\API

 */



class LabTestAPIController extends AppBaseController

{

    /** @var  LabTestRepository */

    private $labTestRepository;



    public function __construct(LabTestRepository $labTestRepo)

    {

        $this->labTestRepository = $labTestRepo;

    }



    /**

     * Display a listing of the LabTest.

     * GET|HEAD /labTests

     *

     * @param Request $request

     * @return \Illuminate\Http\JsonResponse|Response

     */

    public function index(Request $request)

    {

        $labTests = $this->labTestRepository->all(

            $request->except(['skip', 'limit']),

            $request->get('skip'),

            $request->get('limit')

        );



        return $this->sendResponse($labTests->toArray(), 'Lab Tests retrieved successfully');

    }



    /**

     * Store a newly created LabTest in storage.

     * POST /labTests

     *

     * @param CreateLabTestAPIRequest $request

     *

     * @return \Illuminate\Http\JsonResponse|Response

     */

    public function store(CreateLabTestAPIRequest $request)

    {

        $input = $request->all();

        $input['created_by']= Auth::user()->id;
        $input['updated_by']= Auth::user()->id;



        $labTest = $this->labTestRepository->create($input);
        $labTest->dr_id =     $request->dr_id;
       
       $labTest->save();


        return $this->sendResponse($labTest->toArray(), 'Lab Test saved successfully');

    }



    /**

     * Display the specified LabTest.

     * GET|HEAD /labTests/{id}

     *

     * @param int $id

     *

     * @return \Illuminate\Http\JsonResponse|Response

     */

    public function show($id)

    {

        /** @var LabTest $labTest */

        // $labTest = $this->labTestRepository->find($id);
        $labTest = LabTest::select('*')->where('dr_id', $id)->get();



        if (empty($labTest)) {

            return $this->sendError('Lab Test not found');

        }



        return $this->sendResponse($labTest->toArray(), 'Lab Test retrieved successfully');

    }



    /**

     * Update the specified LabTest in storage.

     * PUT/PATCH /labTests/{id}

     *

     * @param int $id

     * @param UpdateLabTestAPIRequest $request

     *

     * @return \Illuminate\Http\JsonResponse|Response

     */

    public function update($id, UpdateLabTestAPIRequest $request)

    {

        $input = $request->all();



        /** @var LabTest $labTest */

        $labTest = $this->labTestRepository->find($id);



        if (empty($labTest)) {

            return $this->sendError('Lab Test not found');

        }



        $labTest = $this->labTestRepository->update($input, $id);



        return $this->sendResponse($labTest->toArray(), 'LabTest updated successfully');

    }



    /**

     * Remove the specified LabTest from storage.

     * DELETE /labTests/{id}

     *

     * @param int $id

     *

     * @throws \Exception

     *

     * @return \Illuminate\Http\JsonResponse|Response

     */

    public function destroy($id)

    {

        /** @var LabTest $labTest */

        $labTest = $this->labTestRepository->find($id);



        if (empty($labTest)) {

            return $this->sendError('Lab Test not found');

        }



        $labTest->delete();



        return $this->sendSuccess('Lab Test deleted successfully');

    }

}

