<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateCreate_offer_360APIRequest;
use App\Http\Requests\API\UpdateCreate_offer_360APIRequest;
use App\Models\Create_offer_360;
use App\Repositories\Create_offer_360Repository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;
use Auth;
use DB;

/**
 * Class Create_offer_360Controller
 * @package App\Http\Controllers\API
 */

class Create_offer_360APIController extends AppBaseController
{
    /** @var  Create_offer_360Repository */
    private $createOffer360Repository;

    public function __construct(Create_offer_360Repository $createOffer360Repo)
    {
        $this->createOffer360Repository = $createOffer360Repo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/createOffer360s",
     *      summary="Get a listing of the Create_offer_360s.",
     *      tags={"Create_offer_360"},
     *      description="Get all Create_offer_360s",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Create_offer_360")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $createOffer360s = $this->createOffer360Repository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($createOffer360s->toArray(), 'Create Offer 360S retrieved successfully');
    }

      public function Offer_view(Request $request)
      {

         $Offer = Create_offer_360::where('created_by',$request->user_id)->get();
       
        
          if ($Offer) {
            return response()->json(["status" => true,"massage"=>"Offer retrieved successfully","data" => $Offer]);
        }else{
            return response()->json(["status" => false,"data" => $Offer]);
        }

      }

    /**
     * @param CreateCreate_offer_360APIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/createOffer360s",
     *      summary="Store a newly created Create_offer_360 in storage",
     *      tags={"Create_offer_360"},
     *      description="Store Create_offer_360",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Create_offer_360 that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Create_offer_360")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Create_offer_360"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateCreate_offer_360APIRequest $request)
    {
        $book =  DB::table('create_offer_360')->where('user_id', $request->user_id)->orderBy('user_id', 'desc')->first();
        if(!empty($book)){
            $input = $request->all();
            $id = $book->id;
                  
           $createOffer360 = $this->createOffer360Repository->update($input, $id);
            

        }else
        {
        $input = $request->all();
        $input['created_by'] = Auth::user()->id;
        $input['updated_by'] = Auth::user()->id;

        $createOffer360 = $this->createOffer360Repository->create($input);
        }
        return $this->sendResponse($createOffer360->toArray(), 'Create Offer 360 saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/createOffer360s/{id}",
     *      summary="Display the specified Create_offer_360",
     *      tags={"Create_offer_360"},
     *      description="Get Create_offer_360",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Create_offer_360",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Create_offer_360"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Create_offer_360 $createOffer360 */
        $createOffer360 = $this->createOffer360Repository->find($id);

        if (empty($createOffer360)) {
            return $this->sendError('Create Offer 360 not found');
        }

        return $this->sendResponse($createOffer360->toArray(), 'Create Offer 360 retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateCreate_offer_360APIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/createOffer360s/{id}",
     *      summary="Update the specified Create_offer_360 in storage",
     *      tags={"Create_offer_360"},
     *      description="Update Create_offer_360",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Create_offer_360",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Create_offer_360 that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Create_offer_360")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Create_offer_360"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateCreate_offer_360APIRequest $request)
    {
        $input = $request->all();

        /** @var Create_offer_360 $createOffer360 */
        $createOffer360 = $this->createOffer360Repository->find($id);

        if (empty($createOffer360)) {
            return $this->sendError('Create Offer 360 not found');
        }

        $createOffer360 = $this->createOffer360Repository->update($input, $id);

        return $this->sendResponse($createOffer360->toArray(), 'Create_offer_360 updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/createOffer360s/{id}",
     *      summary="Remove the specified Create_offer_360 from storage",
     *      tags={"Create_offer_360"},
     *      description="Delete Create_offer_360",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Create_offer_360",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Create_offer_360 $createOffer360 */
        $createOffer360 = $this->createOffer360Repository->find($id);

        if (empty($createOffer360)) {
            return $this->sendError('Create Offer 360 not found');
        }

        $createOffer360->delete();

        return $this->sendSuccess('Create Offer 360 deleted successfully');
    }
}
