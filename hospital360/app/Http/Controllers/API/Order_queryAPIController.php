<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateOrder_queryAPIRequest;
use App\Http\Requests\API\UpdateOrder_queryAPIRequest;
use App\Models\Order_query;
use App\Repositories\Order_queryRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;
use Auth;
use DB;

/**
 * Class Order_queryController
 * @package App\Http\Controllers\API
 */

class Order_queryAPIController extends AppBaseController
{
    /** @var  Order_queryRepository */
    private $orderQueryRepository;

    public function __construct(Order_queryRepository $orderQueryRepo)
    {
        $this->orderQueryRepository = $orderQueryRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/orderQueries",
     *      summary="Get a listing of the Order_queries.",
     *      tags={"Order_query"},
     *      description="Get all Order_queries",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Order_query")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $orderQueries = $this->orderQueryRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($orderQueries->toArray(), 'Order Queries retrieved successfully');
    }

    /**
     * @param CreateOrder_queryAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/orderQueries",
     *      summary="Store a newly created Order_query in storage",
     *      tags={"Order_query"},
     *      description="Store Order_query",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Order_query that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Order_query")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Order_query"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(Request $request)
    {
       
        $input = $request->all();
        $input['created_by'] = Auth::user()->id;
        $input['updated_by'] = Auth::user()->id;
        $orderQuery = $this->orderQueryRepository->create($input);

        return $this->sendResponse($orderQuery->toArray(), 'Order Query saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/orderQueries/{id}",
     *      summary="Display the specified Order_query",
     *      tags={"Order_query"},
     *      description="Get Order_query",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Order_query",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Order_query"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Order_query $orderQuery */
        $orderQuery = $this->orderQueryRepository->find($id);

        if (empty($orderQuery)) {
            return $this->sendError('Order Query not found');
        }

        return $this->sendResponse($orderQuery->toArray(), 'Order Query retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateOrder_queryAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/orderQueries/{id}",
     *      summary="Update the specified Order_query in storage",
     *      tags={"Order_query"},
     *      description="Update Order_query",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Order_query",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Order_query that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Order_query")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Order_query"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateOrder_queryAPIRequest $request)
    {
        $input = $request->all();

        /** @var Order_query $orderQuery */
        $orderQuery = $this->orderQueryRepository->find($id);

        if (empty($orderQuery)) {
            return $this->sendError('Order Query not found');
        }

        $orderQuery = $this->orderQueryRepository->update($input, $id);

        return $this->sendResponse($orderQuery->toArray(), 'Order_query updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/orderQueries/{id}",
     *      summary="Remove the specified Order_query from storage",
     *      tags={"Order_query"},
     *      description="Delete Order_query",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Order_query",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Order_query $orderQuery */
        $orderQuery = $this->orderQueryRepository->find($id);

        if (empty($orderQuery)) {
            return $this->sendError('Order Query not found');
        }

        $orderQuery->delete();

        return $this->sendSuccess('Order Query deleted successfully');
    }
}
