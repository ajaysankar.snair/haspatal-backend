<?php

namespace App\Http\Controllers\API;
use App\Models\BookingRequest;
use App\Http\Requests\API\CreateTimeSlotAPIRequest;
use App\Http\Requests\API\UpdateTimeSlotAPIRequest;
use App\Models\TimeSlot;
use App\Repositories\TimeSlotRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;
use DB;
use Auth;
/**
 * Class TimeSlotController
 * @package App\Http\Controllers\API
 */

class TimeSlotAPIController extends AppBaseController
{
    /** @var  TimeSlotRepository */
    private $timeSlotRepository;

    public function __construct(TimeSlotRepository $timeSlotRepo)
    {
        $this->timeSlotRepository = $timeSlotRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/timeSlots",
     *      summary="Get a listing of the TimeSlots.",
     *      tags={"TimeSlot"},
     *      description="Get all TimeSlots",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/TimeSlot")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $timeSlots = $this->timeSlotRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($timeSlots->toArray(), 'Time Slots retrieved successfully');
    }
   public function time_slots_doctor(Request $request)
    {
        // $result = TimeSlot::where('doctor_id', $request->doctor_id)->get();
        
        $date    = date('d-m-Y');
        if( $date ==$request->input('date')){
            $today = date("H:i");
        }else{
            $today = "00:00";
        }
       
        // return $date;
        // return  $today ;
        // $result = TimeSlot::where('doctor_id', $request->doctor_id)->get();
        $result = TimeSlot::where('doctor_id', $request->doctor_id)->orderBy('id', 'desc')
        ->first();
        // $booked_time = BookingRequest::select('consult_time')->where('consult_date', '=', $request->input('date'))->get();
        $booked_time = array();
        // $booked_time=  BookingRequest::where('consult_date', $request->input('date'))->pluck('consult_time')->toArray();
        $booked_time=  BookingRequest::where('consult_date', $request->input('date'))  ->where('status','!=',5) ->pluck('consult_time')->toArray();
        $mrg_from_time= $result->mrg_from_time;
        $mrg_to_time=$result->mrg_to_time;
       

      
     
        $duration=$result->booking_time_slot;
        $filterQuery = $today;
        if($mrg_to_time && $mrg_from_time)
        {
            $morn_time_slots = array();
            $mrg_start_time    = strtotime($mrg_from_time); //change to strtotime
            $mrg_end_time      = strtotime($mrg_to_time); //change to strtotime
            
            $mrg_add_mins  = $duration * 60;
            
            while ($mrg_start_time <= $mrg_end_time) // loop between time
            {
            $morn_time_slots[] = date("H:i", $mrg_start_time);
            $mrg_start_time += $mrg_add_mins; // to check endtime
            }
            $fullmoringSlot=$morn_time_slots;
            // return  $morn_time_slots;

            // filtering array
            // foreach ($booked_time as $booked_times)
            // {
            
            //     $morn_time_slots = array_values(array_filter($morn_time_slots, fn ($m) => $m != $booked_times));
            
                
            // }
            $morn_time_slots=array_diff($morn_time_slots,$booked_time);
            // return  $result;
            
            $morning_slot = [];
            foreach ($morn_time_slots as $key => $value) {
                $colonreplacedByDotOfValue = str_replace(":",".",$value);
                $colonreplacedByDotfilterQuery  = str_replace(":",".",$filterQuery);

                if ($colonreplacedByDotOfValue > $colonreplacedByDotfilterQuery) {
                    $morning_slot[] = $value;
                }
            }
        } 
        else
        {
        $morn_time_slots = array("Not available");
        $morning_slot = array("Not available");
        $fullmoringSlot = array("Not available");
        }
    // return $morn_time_slots;

        
        // afternoon 
        $aft_from_time= $result->aft_from_time;
        $aft_to_time=$result->aft_to_time;
        if($aft_from_time && $aft_to_time)
        {
        // return $duration;
        $aft_time_slots = array();
        $aft_start_time    = strtotime($aft_from_time); //change to strtotime
        $aft_end_time      = strtotime($aft_to_time); //change to strtotime
         
        $aft_add_mins  = $duration * 60;
         
        while ($aft_start_time <= $aft_end_time) // loop between time
        {
           $aft_time_slots[] = date("H:i", $aft_start_time);
           $aft_start_time += $aft_add_mins; // to check endtime
        }


        // foreach ($booked_time as $booked_times)
        // {
        
        //     $aft_time_slots = array_values(array_filter($aft_time_slots, fn ($m) => $m != $booked_times));
        
        // }
        $fullafterSlot=$aft_time_slots;
        $aft_time_slots=array_diff($aft_time_slots,$booked_time);
        $afterslot = [];
        foreach ($aft_time_slots as $key => $value) {
            $colonreplacedByDotOfValue = str_replace(":",".",$value);
            $colonreplacedByDotfilterQuery  = str_replace(":",".",$filterQuery);

            if ($colonreplacedByDotOfValue > $colonreplacedByDotfilterQuery) {
                $afterslot[] = $value;
            }
        }
    }
        else
        {
        $aft_time_slots = array("Not available");
        $afterslot = array("Not available");
        $fullafterSlot = array("Not available");
        }
        // evening
        $eve_from_time= $result->eve_from_time;
        $eve_to_time=$result->eve_to_time;
        // return $eve_to_time;
        if($eve_from_time && $eve_to_time)
        {
           
       
        $eve_time_slots = array();
        $eve_start_time    = strtotime($eve_from_time); //change to strtotime
        $eve_end_time      = strtotime($eve_to_time); //change to strtotime
         
        $eve_add_mins  = $duration * 60;
        // return $eve_end_time;
        while ($eve_start_time <= $eve_end_time) // loop between time
        {
           $eve_time_slots[] = date("H:i", $eve_start_time);
           $eve_start_time += $eve_add_mins; // to check endtime
        }
        
        // foreach ($booked_time as $booked_times)
        // {
        
        //     $eve_time_slots = array_values(array_filter($eve_time_slots, fn ($m) => $m != $booked_times));
        
        // }
        $fullevengSlot=$eve_time_slots;
          
        $eve_time_slots=array_diff($eve_time_slots,$booked_time);
        $eveningSlot = [];
        foreach ($eve_time_slots as $key => $value) {
            $colonreplacedByDotOfValue = str_replace(":",".",$value);
            $colonreplacedByDotfilterQuery  = str_replace(":",".",$filterQuery);

            if ($colonreplacedByDotOfValue > $colonreplacedByDotfilterQuery) {
                $eveningSlot[] = $value;
            }
        }
    }
    else
    {
    $eve_time_slots = array("Not available");
    $eveningSlot = array("Not available");
    $fullevengSlot = array("Not available");
    }
        // return $eve_time_slots;
     
        if ($result) {
            return response()->json(["status" => true,"massage"=>"Time Slots retrieved successfully","morn_time_slots"=>$morning_slot,"aft_time_slots"=>$afterslot,"eve_time_slots"=>$eveningSlot,"data" => $result,"booked_time" => $booked_time,"fullevengSlot" => $fullevengSlot,"fullmoringSlot" => $fullmoringSlot,"fullafterSlot" => $fullafterSlot]);
        }else{
            return response()->json(["status" => false,"data" => $result]);
        }
    }
    /**
     * @param CreateTimeSlotAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/timeSlots",
     *      summary="Store a newly created TimeSlot in storage",
     *      tags={"TimeSlot"},
     *      description="Store TimeSlot",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="TimeSlot that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/TimeSlot")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/TimeSlot"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(Request $request)
    {
        
          $book =  DB::table('time_slot')->where('doctor_id', $request->doctor_id)->orderBy('doctor_id', 'desc')->first();
        if(!empty($book)){
            $input = $request->all();
            $id = $book->id;
            $timeSlot = $this->timeSlotRepository->update($input, $id);
        }else
        {
            $user = DB::table('users')->where('user_type',1)->where('userDetailsId', Auth::user()->userDetailsId)->update(['doctor_login_status'=> 1]);
            $input = $request->all();
            $input['created_by'] = Auth::user()->id;
            $input['updated_by'] = Auth::user()->id;
            $timeSlot = $this->timeSlotRepository->create($input);
        }
        return $this->sendResponse($timeSlot->toArray(), 'Time Slot saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/timeSlots/{id}",
     *      summary="Display the specified TimeSlot",
     *      tags={"TimeSlot"},
     *      description="Get TimeSlot",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of TimeSlot",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/TimeSlot"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var TimeSlot $timeSlot */
        $timeSlot = $this->timeSlotRepository->find($id);

        if (empty($timeSlot)) {
            return $this->sendError('Time Slot not found');
        }

        return $this->sendResponse($timeSlot->toArray(), 'Time Slot retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateTimeSlotAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/timeSlots/{id}",
     *      summary="Update the specified TimeSlot in storage",
     *      tags={"TimeSlot"},
     *      description="Update TimeSlot",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of TimeSlot",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="TimeSlot that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/TimeSlot")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/TimeSlot"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateTimeSlotAPIRequest $request)
    {
        $input = $request->all();

        /** @var TimeSlot $timeSlot */
        $timeSlot = $this->timeSlotRepository->find($id);

        if (empty($timeSlot)) {
            return $this->sendError('Time Slot not found');
        }

        $timeSlot = $this->timeSlotRepository->update($input, $id);

        return $this->sendResponse($timeSlot->toArray(), 'TimeSlot updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/timeSlots/{id}",
     *      summary="Remove the specified TimeSlot from storage",
     *      tags={"TimeSlot"},
     *      description="Delete TimeSlot",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of TimeSlot",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var TimeSlot $timeSlot */
        $timeSlot = $this->timeSlotRepository->find($id);

        if (empty($timeSlot)) {
            return $this->sendError('Time Slot not found');
        }

        $timeSlot->delete();

        return $this->sendSuccess('Time Slot deleted successfully');
    }
}
