<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateSuggested_imagings_patientAPIRequest;
use App\Http\Requests\API\UpdateSuggested_imagings_patientAPIRequest;
use App\Models\Suggested_imagings_patient;
use App\Repositories\Suggested_imagings_patientRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;
use DB;
use Auth;

/**
 * Class Suggested_imagings_patientController
 * @package App\Http\Controllers\API
 */

class Suggested_imagings_patientAPIController extends AppBaseController
{
    /** @var  Suggested_imagings_patientRepository */
    private $suggestedImagingsPatientRepository;

    public function __construct(Suggested_imagings_patientRepository $suggestedImagingsPatientRepo)
    {
        $this->suggestedImagingsPatientRepository = $suggestedImagingsPatientRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/suggestedImagingsPatients",
     *      summary="Get a listing of the Suggested_imagings_patients.",
     *      tags={"Suggested_imagings_patient"},
     *      description="Get all Suggested_imagings_patients",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Suggested_imagings_patient")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $suggestedImagingsPatients = $this->suggestedImagingsPatientRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($suggestedImagingsPatients->toArray(), 'Suggested Imagings Patients retrieved successfully');
    }

    /**
     * @param CreateSuggested_imagings_patientAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/suggestedImagingsPatients",
     *      summary="Store a newly created Suggested_imagings_patient in storage",
     *      tags={"Suggested_imagings_patient"},
     *      description="Store Suggested_imagings_patient",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Suggested_imagings_patient that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Suggested_imagings_patient")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Suggested_imagings_patient"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateSuggested_imagings_patientAPIRequest $request)
    {
        $book =  DB::table('suggested_imagings_patient')->where('si_book_id', $request->si_book_id)->orderBy('si_book_id', 'desc')->first();
        if(!empty($book)){
        $input = $request->all();
        $id = $book->id;
        $suggestedImagingsPatient = $this->suggestedImagingsPatientRepository->update($input, $id);

        }else
        {
            $input = $request->all();
            $input['created_by'] = Auth::user()->id;
            $input['updated_by'] = Auth::user()->id;
            $suggestedImagingsPatient = $this->suggestedImagingsPatientRepository->create($input);
        }
        return $this->sendResponse($suggestedImagingsPatient->toArray(), 'Suggested Imagings Patient saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/suggestedImagingsPatients/{id}",
     *      summary="Display the specified Suggested_imagings_patient",
     *      tags={"Suggested_imagings_patient"},
     *      description="Get Suggested_imagings_patient",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Suggested_imagings_patient",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Suggested_imagings_patient"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Suggested_imagings_patient $suggestedImagingsPatient */
        $suggestedImagingsPatient = $this->suggestedImagingsPatientRepository->find($id);

        if (empty($suggestedImagingsPatient)) {
            return $this->sendError('Suggested Imagings Patient not found');
        }

        return $this->sendResponse($suggestedImagingsPatient->toArray(), 'Suggested Imagings Patient retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateSuggested_imagings_patientAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/suggestedImagingsPatients/{id}",
     *      summary="Update the specified Suggested_imagings_patient in storage",
     *      tags={"Suggested_imagings_patient"},
     *      description="Update Suggested_imagings_patient",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Suggested_imagings_patient",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Suggested_imagings_patient that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Suggested_imagings_patient")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Suggested_imagings_patient"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateSuggested_imagings_patientAPIRequest $request)
    {
        $input = $request->all();

        /** @var Suggested_imagings_patient $suggestedImagingsPatient */
        $suggestedImagingsPatient = $this->suggestedImagingsPatientRepository->find($id);

        if (empty($suggestedImagingsPatient)) {
            return $this->sendError('Suggested Imagings Patient not found');
        }

        $suggestedImagingsPatient = $this->suggestedImagingsPatientRepository->update($input, $id);

        return $this->sendResponse($suggestedImagingsPatient->toArray(), 'Suggested_imagings_patient updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/suggestedImagingsPatients/{id}",
     *      summary="Remove the specified Suggested_imagings_patient from storage",
     *      tags={"Suggested_imagings_patient"},
     *      description="Delete Suggested_imagings_patient",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Suggested_imagings_patient",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Suggested_imagings_patient $suggestedImagingsPatient */
        $suggestedImagingsPatient = $this->suggestedImagingsPatientRepository->find($id);

        if (empty($suggestedImagingsPatient)) {
            return $this->sendError('Suggested Imagings Patient not found');
        }

        $suggestedImagingsPatient->delete();

        return $this->sendSuccess('Suggested Imagings Patient deleted successfully');
    }
}
