<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatehospitalWithdrawAPIRequest;
use App\Http\Requests\API\UpdatehospitalWithdrawAPIRequest;
use App\Models\hospitalWithdraw;
use App\Repositories\hospitalWithdrawRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class hospitalWithdrawController
 * @package App\Http\Controllers\API
 */

class hospitalWithdrawAPIController extends AppBaseController
{
    /** @var  hospitalWithdrawRepository */
    private $hospitalWithdrawRepository;

    public function __construct(hospitalWithdrawRepository $hospitalWithdrawRepo)
    {
        $this->hospitalWithdrawRepository = $hospitalWithdrawRepo;
    }

    /**
     * Display a listing of the hospitalWithdraw.
     * GET|HEAD /hospitalWithdraws
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|Response
     */
    public function index(Request $request)
    {
        $hospitalWithdraws = $this->hospitalWithdrawRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($hospitalWithdraws->toArray(), 'Hospital Withdraws retrieved successfully');
    }

    /**
     * Store a newly created hospitalWithdraw in storage.
     * POST /hospitalWithdraws
     *
     * @param CreatehospitalWithdrawAPIRequest $request
     *
     * @return \Illuminate\Http\JsonResponse|Response
     */
    public function store(CreatehospitalWithdrawAPIRequest $request)
    {
        $input = $request->all();

        $hospitalWithdraw = $this->hospitalWithdrawRepository->create($input);

        return $this->sendResponse($hospitalWithdraw->toArray(), 'Hospital Withdraw saved successfully');
    }

    /**
     * Display the specified hospitalWithdraw.
     * GET|HEAD /hospitalWithdraws/{id}
     *
     * @param int $id
     *
     * @return \Illuminate\Http\JsonResponse|Response
     */
    public function show($id)
    {
        /** @var hospitalWithdraw $hospitalWithdraw */
        $hospitalWithdraw = $this->hospitalWithdrawRepository->find($id);

        if (empty($hospitalWithdraw)) {
            return $this->sendError('Hospital Withdraw not found');
        }

        return $this->sendResponse($hospitalWithdraw->toArray(), 'Hospital Withdraw retrieved successfully');
    }

    /**
     * Update the specified hospitalWithdraw in storage.
     * PUT/PATCH /hospitalWithdraws/{id}
     *
     * @param int $id
     * @param UpdatehospitalWithdrawAPIRequest $request
     *
     * @return \Illuminate\Http\JsonResponse|Response
     */
    public function update($id, UpdatehospitalWithdrawAPIRequest $request)
    {
        $input = $request->all();

        /** @var hospitalWithdraw $hospitalWithdraw */
        $hospitalWithdraw = $this->hospitalWithdrawRepository->find($id);

        if (empty($hospitalWithdraw)) {
            return $this->sendError('Hospital Withdraw not found');
        }

        $hospitalWithdraw = $this->hospitalWithdrawRepository->update($input, $id);

        return $this->sendResponse($hospitalWithdraw->toArray(), 'hospitalWithdraw updated successfully');
    }

    /**
     * Remove the specified hospitalWithdraw from storage.
     * DELETE /hospitalWithdraws/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return \Illuminate\Http\JsonResponse|Response
     */
    public function destroy($id)
    {
        /** @var hospitalWithdraw $hospitalWithdraw */
        $hospitalWithdraw = $this->hospitalWithdrawRepository->find($id);

        if (empty($hospitalWithdraw)) {
            return $this->sendError('Hospital Withdraw not found');
        }

        $hospitalWithdraw->delete();

        return $this->sendSuccess('Hospital Withdraw deleted successfully');
    }
}
