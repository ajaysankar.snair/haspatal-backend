<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateWallet_360APIRequest;
use App\Http\Requests\API\UpdateWallet_360APIRequest;
use App\Models\Wallet_360;
use App\Repositories\Wallet_360Repository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;
use Auth;
use DB;

/**
 * Class Wallet_360Controller
 * @package App\Http\Controllers\API
 */

class Wallet_360APIController extends AppBaseController
{
    /** @var  Wallet_360Repository */
    private $wallet360Repository;

    public function __construct(Wallet_360Repository $wallet360Repo)
    {
        $this->wallet360Repository = $wallet360Repo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/wallet360s",
     *      summary="Get a listing of the Wallet_360s.",
     *      tags={"Wallet_360"},
     *      description="Get all Wallet_360s",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Wallet_360")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $wallet360s = $this->wallet360Repository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($wallet360s->toArray(), 'Wallet 360S retrieved successfully');
    }

    /**
     * @param CreateWallet_360APIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/wallet360s",
     *      summary="Store a newly created Wallet_360 in storage",
     *      tags={"Wallet_360"},
     *      description="Store Wallet_360",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Wallet_360 that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Wallet_360")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Wallet_360"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateWallet_360APIRequest $request)
    {
         $book =  DB::table('wallet_360')->where('user_id', $request->user_id)->orderBy('user_id', 'desc')->first();
        if(!empty($book)){
            $input = $request->all();
            $id = $book->id;
            $input['wa_amount'] = $request->wa_amount + $book->wa_amount;
            
            $wallet360 = $this->wallet360Repository->update($input, $id);
            

        }else
        {
            $input = $request->all();
            $input['created_by'] = Auth::user()->id;
            $input['updated_by'] = Auth::user()->id;
            $wallet360 = $this->wallet360Repository->create($input);
        }

        

        return $this->sendResponse($wallet360->toArray(), 'Wallet 360 saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/wallet360s/{id}",
     *      summary="Display the specified Wallet_360",
     *      tags={"Wallet_360"},
     *      description="Get Wallet_360",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Wallet_360",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Wallet_360"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Wallet_360 $wallet360 */
        $wallet360 = $this->wallet360Repository->find($id);

        if (empty($wallet360)) {
            return $this->sendError('Wallet 360 not found');
        }

        return $this->sendResponse($wallet360->toArray(), 'Wallet 360 retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateWallet_360APIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/wallet360s/{id}",
     *      summary="Update the specified Wallet_360 in storage",
     *      tags={"Wallet_360"},
     *      description="Update Wallet_360",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Wallet_360",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Wallet_360 that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Wallet_360")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Wallet_360"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateWallet_360APIRequest $request)
    {
        $input = $request->all();

        /** @var Wallet_360 $wallet360 */
        $wallet360 = $this->wallet360Repository->find($id);

        if (empty($wallet360)) {
            return $this->sendError('Wallet 360 not found');
        }

        $wallet360 = $this->wallet360Repository->update($input, $id);

        return $this->sendResponse($wallet360->toArray(), 'Wallet_360 updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/wallet360s/{id}",
     *      summary="Remove the specified Wallet_360 from storage",
     *      tags={"Wallet_360"},
     *      description="Delete Wallet_360",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Wallet_360",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Wallet_360 $wallet360 */
        $wallet360 = $this->wallet360Repository->find($id);

        if (empty($wallet360)) {
            return $this->sendError('Wallet 360 not found');
        }

        $wallet360->delete();

        return $this->sendSuccess('Wallet 360 deleted successfully');
    }
}
