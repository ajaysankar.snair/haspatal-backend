<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateConsultantAPIRequest;
use App\Http\Requests\API\UpdateConsultantAPIRequest;
use App\Models\Consultant;
use App\Repositories\ConsultantRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;
use DB;
use Auth;

/**
 * Class ConsultantController
 * @package App\Http\Controllers\API
 */

class ConsultantAPIController extends AppBaseController
{
    /** @var  ConsultantRepository */
    private $consultantRepository;

    public function __construct(ConsultantRepository $consultantRepo)
    {
        $this->consultantRepository = $consultantRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/consultants",
     *      summary="Get a listing of the Consultants.",
     *      tags={"Consultant"},
     *      description="Get all Consultants",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Consultant")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $consultants = $this->consultantRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($consultants->toArray(), 'Consultants retrieved successfully');
    }

    /**
     * @param CreateConsultantAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/consultants",
     *      summary="Store a newly created Consultant in storage",
     *      tags={"Consultant"},
     *      description="Store Consultant",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Consultant that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Consultant")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Consultant"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateConsultantAPIRequest $request)
    {
        $book =  DB::table('consultant')->where('c_book_id', $request->c_book_id)->orderBy('c_book_id', 'desc')->first();
        if(!empty($book)){
            $input = $request->all();
            $id = $book->id;
           $consultant = $this->consultantRepository->update($input, $id);
        }else
        {

        $input = $request->all();
        $input['created_by'] = Auth::user()->id;
        $input['updated_by'] = Auth::user()->id;

        $consultant = $this->consultantRepository->create($input);

    }
        return $this->sendResponse($consultant->toArray(), 'Consultant saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/consultants/{id}",
     *      summary="Display the specified Consultant",
     *      tags={"Consultant"},
     *      description="Get Consultant",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Consultant",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Consultant"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Consultant $consultant */
        $consultant = $this->consultantRepository->find($id);

        if (empty($consultant)) {
            return $this->sendError('Consultant not found');
        }

        return $this->sendResponse($consultant->toArray(), 'Consultant retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateConsultantAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/consultants/{id}",
     *      summary="Update the specified Consultant in storage",
     *      tags={"Consultant"},
     *      description="Update Consultant",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Consultant",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Consultant that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Consultant")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Consultant"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateConsultantAPIRequest $request)
    {
        $input = $request->all();

        /** @var Consultant $consultant */
        $consultant = $this->consultantRepository->find($id);

        if (empty($consultant)) {
            return $this->sendError('Consultant not found');
        }

        $consultant = $this->consultantRepository->update($input, $id);

        return $this->sendResponse($consultant->toArray(), 'Consultant updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/consultants/{id}",
     *      summary="Remove the specified Consultant from storage",
     *      tags={"Consultant"},
     *      description="Delete Consultant",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Consultant",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Consultant $consultant */
        $consultant = $this->consultantRepository->find($id);

        if (empty($consultant)) {
            return $this->sendError('Consultant not found');
        }

        $consultant->delete();

        return $this->sendSuccess('Consultant deleted successfully');
    }
}
