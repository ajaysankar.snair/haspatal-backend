<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateDoctor_withdrawAPIRequest;
use App\Http\Requests\API\UpdateDoctor_withdrawAPIRequest;
use App\Models\Doctor_withdraw;
use App\Repositories\Doctor_withdrawRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;
use Auth;
use DB;

/**
 * Class Doctor_withdrawController
 * @package App\Http\Controllers\API
 */

class Doctor_withdrawAPIController extends AppBaseController
{
    /** @var  Doctor_withdrawRepository */
    private $doctorWithdrawRepository;

    public function __construct(Doctor_withdrawRepository $doctorWithdrawRepo)
    {
        $this->doctorWithdrawRepository = $doctorWithdrawRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/doctorWithdraws",
     *      summary="Get a listing of the Doctor_withdraws.",
     *      tags={"Doctor_withdraw"},
     *      description="Get all Doctor_withdraws",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Doctor_withdraw")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $doctorWithdraws = $this->doctorWithdrawRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($doctorWithdraws->toArray(), 'Doctor Withdraws retrieved successfully');
    }

    /**
     * @param CreateDoctor_withdrawAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/doctorWithdraws",
     *      summary="Store a newly created Doctor_withdraw in storage",
     *      tags={"Doctor_withdraw"},
     *      description="Store Doctor_withdraw",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Doctor_withdraw that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Doctor_withdraw")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Doctor_withdraw"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(request $request)
    {
        
        $input = $request->all();
        $input['created_by'] = Auth::user()->id;
        $input['updated_by'] = Auth::user()->id;
        $input['doctor_id'] = Auth::user()->userDetailsId; 

        $input['available_balance'] =  $request->current_balance - $request->requested_amount;
        //echo $input['available_balance']; exit;

        $doctorWithdraw = $this->doctorWithdrawRepository->create($input);


        return $this->sendResponse($doctorWithdraw->toArray(), 'Doctor Withdraw saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/doctorWithdraws/{id}",
     *      summary="Display the specified Doctor_withdraw",
     *      tags={"Doctor_withdraw"},
     *      description="Get Doctor_withdraw",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Doctor_withdraw",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Doctor_withdraw"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Doctor_withdraw $doctorWithdraw */
        $doctorWithdraw = $this->doctorWithdrawRepository->find($id);

        if (empty($doctorWithdraw)) {
            return $this->sendError('Doctor Withdraw not found');
        }

        return $this->sendResponse($doctorWithdraw->toArray(), 'Doctor Withdraw retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateDoctor_withdrawAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/doctorWithdraws/{id}",
     *      summary="Update the specified Doctor_withdraw in storage",
     *      tags={"Doctor_withdraw"},
     *      description="Update Doctor_withdraw",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Doctor_withdraw",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Doctor_withdraw that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Doctor_withdraw")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Doctor_withdraw"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateDoctor_withdrawAPIRequest $request)
    {
        $input = $request->all();

        /** @var Doctor_withdraw $doctorWithdraw */
        $doctorWithdraw = $this->doctorWithdrawRepository->find($id);

        if (empty($doctorWithdraw)) {
            return $this->sendError('Doctor Withdraw not found');
        }

        $doctorWithdraw = $this->doctorWithdrawRepository->update($input, $id);

        return $this->sendResponse($doctorWithdraw->toArray(), 'Doctor_withdraw updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/doctorWithdraws/{id}",
     *      summary="Remove the specified Doctor_withdraw from storage",
     *      tags={"Doctor_withdraw"},
     *      description="Delete Doctor_withdraw",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Doctor_withdraw",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Doctor_withdraw $doctorWithdraw */
        $doctorWithdraw = $this->doctorWithdrawRepository->find($id);

        if (empty($doctorWithdraw)) {
            return $this->sendError('Doctor Withdraw not found');
        }

        $doctorWithdraw->delete();

        return $this->sendSuccess('Doctor Withdraw deleted successfully');
    }
}
