<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatehospitalAddFundAPIRequest;
use App\Http\Requests\API\UpdatehospitalAddFundAPIRequest;
use App\Models\hospitalAddFund;
use App\Repositories\hospitalAddFundRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class hospitalAddFundController
 * @package App\Http\Controllers\API
 */

class hospitalAddFundAPIController extends AppBaseController
{
    /** @var  hospitalAddFundRepository */
    private $hospitalAddFundRepository;

    public function __construct(hospitalAddFundRepository $hospitalAddFundRepo)
    {
        $this->hospitalAddFundRepository = $hospitalAddFundRepo;
    }

    /**
     * Display a listing of the hospitalAddFund.
     * GET|HEAD /hospitalAddFunds
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|Response
     */
    public function index(Request $request)
    {
        $hospitalAddFunds = $this->hospitalAddFundRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($hospitalAddFunds->toArray(), 'Hospital Add Funds retrieved successfully');
    }

    /**
     * Store a newly created hospitalAddFund in storage.
     * POST /hospitalAddFunds
     *
     * @param CreatehospitalAddFundAPIRequest $request
     *
     * @return \Illuminate\Http\JsonResponse|Response
     */
    public function store(CreatehospitalAddFundAPIRequest $request)
    {
        $input = $request->all();

        $hospitalAddFund = $this->hospitalAddFundRepository->create($input);

        return $this->sendResponse($hospitalAddFund->toArray(), 'Hospital Add Fund saved successfully');
    }

    /**
     * Display the specified hospitalAddFund.
     * GET|HEAD /hospitalAddFunds/{id}
     *
     * @param int $id
     *
     * @return \Illuminate\Http\JsonResponse|Response
     */
    public function show($id)
    {
        /** @var hospitalAddFund $hospitalAddFund */
        $hospitalAddFund = $this->hospitalAddFundRepository->find($id);

        if (empty($hospitalAddFund)) {
            return $this->sendError('Hospital Add Fund not found');
        }

        return $this->sendResponse($hospitalAddFund->toArray(), 'Hospital Add Fund retrieved successfully');
    }

    /**
     * Update the specified hospitalAddFund in storage.
     * PUT/PATCH /hospitalAddFunds/{id}
     *
     * @param int $id
     * @param UpdatehospitalAddFundAPIRequest $request
     *
     * @return \Illuminate\Http\JsonResponse|Response
     */
    public function update($id, UpdatehospitalAddFundAPIRequest $request)
    {
        $input = $request->all();

        /** @var hospitalAddFund $hospitalAddFund */
        $hospitalAddFund = $this->hospitalAddFundRepository->find($id);

        if (empty($hospitalAddFund)) {
            return $this->sendError('Hospital Add Fund not found');
        }

        $hospitalAddFund = $this->hospitalAddFundRepository->update($input, $id);

        return $this->sendResponse($hospitalAddFund->toArray(), 'hospitalAddFund updated successfully');
    }

    /**
     * Remove the specified hospitalAddFund from storage.
     * DELETE /hospitalAddFunds/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return \Illuminate\Http\JsonResponse|Response
     */
    public function destroy($id)
    {
        /** @var hospitalAddFund $hospitalAddFund */
        $hospitalAddFund = $this->hospitalAddFundRepository->find($id);

        if (empty($hospitalAddFund)) {
            return $this->sendError('Hospital Add Fund not found');
        }

        $hospitalAddFund->delete();

        return $this->sendSuccess('Hospital Add Fund deleted successfully');
    }
}
