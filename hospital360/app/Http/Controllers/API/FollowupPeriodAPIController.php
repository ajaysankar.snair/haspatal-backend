<?php



namespace App\Http\Controllers\API;



use App\Http\Requests\API\CreateFollowupPeriodAPIRequest;

use App\Http\Requests\API\UpdateFollowupPeriodAPIRequest;

use App\Models\FollowupPeriod;

use App\Repositories\FollowupPeriodRepository;

use Illuminate\Http\Request;

use App\Http\Controllers\AppBaseController;

use Response;
use DB;
use Auth;


/**

 * Class FollowupPeriodController

 * @package App\Http\Controllers\API

 */



class FollowupPeriodAPIController extends AppBaseController

{

    /** @var  FollowupPeriodRepository */

    private $followupPeriodRepository;



    public function __construct(FollowupPeriodRepository $followupPeriodRepo)

    {

        $this->followupPeriodRepository = $followupPeriodRepo;

    }



    /**

     * Display a listing of the FollowupPeriod.

     * GET|HEAD /followupPeriods

     *

     * @param Request $request

     * @return \Illuminate\Http\JsonResponse|Response

     */

    public function index(Request $request)

    {

        $followupPeriods = $this->followupPeriodRepository->all(

            $request->except(['skip', 'limit']),

            $request->get('skip'),

            $request->get('limit')

        );



        return $this->sendResponse($followupPeriods->toArray(), 'Followup Periods retrieved successfully');

    }



    /**

     * Store a newly created FollowupPeriod in storage.

     * POST /followupPeriods

     *

     * @param CreateFollowupPeriodAPIRequest $request

     *

     * @return \Illuminate\Http\JsonResponse|Response

     */

    public function store(CreateFollowupPeriodAPIRequest $request)

    {

        $input = $request->all();

        $input1['dr_id'] = $request->dr_id;
        $input1['created_by']= Auth::user()->id;
        $input1['updated_by']= Auth::user()->id;

        foreach ($request->followup_list as $value) {
            
            $input1['followup_list'] = $value;
            $followupPeriod = $this->followupPeriodRepository->create($input1);
        }




        return $this->sendResponse($followupPeriod->toArray(), 'Followup Period saved successfully');

    }



    /**

     * Display the specified FollowupPeriod.

     * GET|HEAD /followupPeriods/{id}

     *

     * @param int $id

     *

     * @return \Illuminate\Http\JsonResponse|Response

     */

    public function show($id)

    {

        /** @var FollowupPeriod $followupPeriod */

        $followupPeriod = $this->followupPeriodRepository->find($id);



        if (empty($followupPeriod)) {

            return $this->sendError('Followup Period not found');

        }



        return $this->sendResponse($followupPeriod->toArray(), 'Followup Period retrieved successfully');

    }



    /**

     * Update the specified FollowupPeriod in storage.

     * PUT/PATCH /followupPeriods/{id}

     *

     * @param int $id

     * @param UpdateFollowupPeriodAPIRequest $request

     *

     * @return \Illuminate\Http\JsonResponse|Response

     */

    public function update($id, UpdateFollowupPeriodAPIRequest $request)

    {

        $input = $request->all();



        /** @var FollowupPeriod $followupPeriod */

        $followupPeriod = $this->followupPeriodRepository->find($id);



        if (empty($followupPeriod)) {

            return $this->sendError('Followup Period not found');

        }



        $followupPeriod = $this->followupPeriodRepository->update($input, $id);



        return $this->sendResponse($followupPeriod->toArray(), 'FollowupPeriod updated successfully');

    }



    /**

     * Remove the specified FollowupPeriod from storage.

     * DELETE /followupPeriods/{id}

     *

     * @param int $id

     *

     * @throws \Exception

     *

     * @return \Illuminate\Http\JsonResponse|Response

     */

    public function destroy($id)

    {

        /** @var FollowupPeriod $followupPeriod */

        $followupPeriod = $this->followupPeriodRepository->find($id);



        if (empty($followupPeriod)) {

            return $this->sendError('Followup Period not found');

        }



        $followupPeriod->delete();



        return $this->sendSuccess('Followup Period deleted successfully');

    }

}

