<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateMemberDetailsAPIRequest;
use App\Http\Requests\API\UpdateMemberDetailsAPIRequest;
use App\Models\MemberDetails;
use App\Repositories\MemberDetailsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class MemberDetailsController
 * @package App\Http\Controllers\API
 */

class MemberDetailsAPIController extends AppBaseController
{
    /** @var  MemberDetailsRepository */
    private $memberDetailsRepository;

    public function __construct(MemberDetailsRepository $memberDetailsRepo)
    {
        $this->memberDetailsRepository = $memberDetailsRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/memberDetails",
     *      summary="Get a listing of the MemberDetails.",
     *      tags={"MemberDetails"},
     *      description="Get all MemberDetails",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/MemberDetails")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $memberDetails = $this->memberDetailsRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($memberDetails->toArray(), 'Member Details retrieved successfully');
    }

    /**
     * @param CreateMemberDetailsAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/memberDetails",
     *      summary="Store a newly created MemberDetails in storage",
     *      tags={"MemberDetails"},
     *      description="Store MemberDetails",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="MemberDetails that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/MemberDetails")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/MemberDetails"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateMemberDetailsAPIRequest $request)
    {
        $input = $request->all();

        $memberDetails = $this->memberDetailsRepository->create($input);

        return $this->sendResponse($memberDetails->toArray(), 'Member Details saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/memberDetails/{id}",
     *      summary="Display the specified MemberDetails",
     *      tags={"MemberDetails"},
     *      description="Get MemberDetails",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of MemberDetails",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/MemberDetails"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var MemberDetails $memberDetails */
        $memberDetails = $this->memberDetailsRepository->find($id);

        if (empty($memberDetails)) {
            return $this->sendError('Member Details not found');
        }

        return $this->sendResponse($memberDetails->toArray(), 'Member Details retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateMemberDetailsAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/memberDetails/{id}",
     *      summary="Update the specified MemberDetails in storage",
     *      tags={"MemberDetails"},
     *      description="Update MemberDetails",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of MemberDetails",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="MemberDetails that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/MemberDetails")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/MemberDetails"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateMemberDetailsAPIRequest $request)
    {
        $input = $request->all();

        /** @var MemberDetails $memberDetails */
        $memberDetails = $this->memberDetailsRepository->find($id);

        if (empty($memberDetails)) {
            return $this->sendError('Member Details not found');
        }

        $memberDetails = $this->memberDetailsRepository->update($input, $id);

        return $this->sendResponse($memberDetails->toArray(), 'MemberDetails updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/memberDetails/{id}",
     *      summary="Remove the specified MemberDetails from storage",
     *      tags={"MemberDetails"},
     *      description="Delete MemberDetails",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of MemberDetails",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var MemberDetails $memberDetails */
        $memberDetails = $this->memberDetailsRepository->find($id);

        if (empty($memberDetails)) {
            return $this->sendError('Member Details not found');
        }

        $memberDetails->delete();

        return $this->sendSuccess('Member Details deleted successfully');
    }
}
