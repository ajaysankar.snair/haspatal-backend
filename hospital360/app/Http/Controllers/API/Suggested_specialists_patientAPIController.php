<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateSuggested_specialists_patientAPIRequest;
use App\Http\Requests\API\UpdateSuggested_specialists_patientAPIRequest;
use App\Models\Suggested_specialists_patient;
use App\Repositories\Suggested_specialists_patientRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;
use DB;
use Auth;


/**
 * Class Suggested_specialists_patientController
 * @package App\Http\Controllers\API
 */

class Suggested_specialists_patientAPIController extends AppBaseController
{
    /** @var  Suggested_specialists_patientRepository */
    private $suggestedSpecialistsPatientRepository;

    public function __construct(Suggested_specialists_patientRepository $suggestedSpecialistsPatientRepo)
    {
        $this->suggestedSpecialistsPatientRepository = $suggestedSpecialistsPatientRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/suggestedSpecialistsPatients",
     *      summary="Get a listing of the Suggested_specialists_patients.",
     *      tags={"Suggested_specialists_patient"},
     *      description="Get all Suggested_specialists_patients",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Suggested_specialists_patient")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $suggestedSpecialistsPatients = $this->suggestedSpecialistsPatientRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($suggestedSpecialistsPatients->toArray(), 'Suggested Specialists Patients retrieved successfully');
    }

    /**
     * @param CreateSuggested_specialists_patientAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/suggestedSpecialistsPatients",
     *      summary="Store a newly created Suggested_specialists_patient in storage",
     *      tags={"Suggested_specialists_patient"},
     *      description="Store Suggested_specialists_patient",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Suggested_specialists_patient that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Suggested_specialists_patient")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Suggested_specialists_patient"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateSuggested_specialists_patientAPIRequest $request)
    {
         $book =  DB::table('suggested_specialists_patient')->where('ss_book_id', $request->ss_book_id)->orderBy('ss_book_id', 'desc')->first();
        if(!empty($book)){
            $input = $request->all();
            $id = $book->id;
           $suggestedSpecialistsPatient = $this->suggestedSpecialistsPatientRepository->update($input, $id);

        }else
        {

        $input = $request->all();
        $input['created_by'] = Auth::user()->id;
        $input['updated_by'] = Auth::user()->id;
        $suggestedSpecialistsPatient = $this->suggestedSpecialistsPatientRepository->create($input);
       }

        return $this->sendResponse($suggestedSpecialistsPatient->toArray(), 'Suggested Specialists Patient saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/suggestedSpecialistsPatients/{id}",
     *      summary="Display the specified Suggested_specialists_patient",
     *      tags={"Suggested_specialists_patient"},
     *      description="Get Suggested_specialists_patient",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Suggested_specialists_patient",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Suggested_specialists_patient"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Suggested_specialists_patient $suggestedSpecialistsPatient */
        $suggestedSpecialistsPatient = $this->suggestedSpecialistsPatientRepository->find($id);

        if (empty($suggestedSpecialistsPatient)) {
            return $this->sendError('Suggested Specialists Patient not found');
        }

        return $this->sendResponse($suggestedSpecialistsPatient->toArray(), 'Suggested Specialists Patient retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateSuggested_specialists_patientAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/suggestedSpecialistsPatients/{id}",
     *      summary="Update the specified Suggested_specialists_patient in storage",
     *      tags={"Suggested_specialists_patient"},
     *      description="Update Suggested_specialists_patient",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Suggested_specialists_patient",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Suggested_specialists_patient that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Suggested_specialists_patient")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Suggested_specialists_patient"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateSuggested_specialists_patientAPIRequest $request)
    {
        $input = $request->all();

        /** @var Suggested_specialists_patient $suggestedSpecialistsPatient */
        $suggestedSpecialistsPatient = $this->suggestedSpecialistsPatientRepository->find($id);

        if (empty($suggestedSpecialistsPatient)) {
            return $this->sendError('Suggested Specialists Patient not found');
        }

        $suggestedSpecialistsPatient = $this->suggestedSpecialistsPatientRepository->update($input, $id);

        return $this->sendResponse($suggestedSpecialistsPatient->toArray(), 'Suggested_specialists_patient updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/suggestedSpecialistsPatients/{id}",
     *      summary="Remove the specified Suggested_specialists_patient from storage",
     *      tags={"Suggested_specialists_patient"},
     *      description="Delete Suggested_specialists_patient",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Suggested_specialists_patient",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Suggested_specialists_patient $suggestedSpecialistsPatient */
        $suggestedSpecialistsPatient = $this->suggestedSpecialistsPatientRepository->find($id);

        if (empty($suggestedSpecialistsPatient)) {
            return $this->sendError('Suggested Specialists Patient not found');
        }

        $suggestedSpecialistsPatient->delete();

        return $this->sendSuccess('Suggested Specialists Patient deleted successfully');
    }
}
