<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateH360coupanAPIRequest;
use App\Http\Requests\API\UpdateH360coupanAPIRequest;
use App\Models\H360coupan;
use App\Repositories\H360coupanRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class H360coupanController
 * @package App\Http\Controllers\API
 */

class H360coupanAPIController extends AppBaseController
{
    /** @var  H360coupanRepository */
    private $h360coupanRepository;

    public function __construct(H360coupanRepository $h360coupanRepo)
    {
        $this->h360coupanRepository = $h360coupanRepo;
    }

    /**
     * Display a listing of the H360coupan.
     * GET|HEAD /h360coupans
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|Response
     */
    public function index(Request $request)
    {
        $h360coupans = $this->h360coupanRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($h360coupans->toArray(), 'H360Coupans retrieved successfully');
    }

    /**
     * Store a newly created H360coupan in storage.
     * POST /h360coupans
     *
     * @param CreateH360coupanAPIRequest $request
     *
     * @return \Illuminate\Http\JsonResponse|Response
     */
    public function store(CreateH360coupanAPIRequest $request)
    {
        $input = $request->all();

        $h360coupan = $this->h360coupanRepository->create($input);

        return $this->sendResponse($h360coupan->toArray(), 'H360Coupan saved successfully');
    }

    /**
     * Display the specified H360coupan.
     * GET|HEAD /h360coupans/{id}
     *
     * @param int $id
     *
     * @return \Illuminate\Http\JsonResponse|Response
     */
    public function show($id)
    {
        /** @var H360coupan $h360coupan */
        $h360coupan = $this->h360coupanRepository->find($id);

        if (empty($h360coupan)) {
            return $this->sendError('H360Coupan not found');
        }

        return $this->sendResponse($h360coupan->toArray(), 'H360Coupan retrieved successfully');
    }

    /**
     * Update the specified H360coupan in storage.
     * PUT/PATCH /h360coupans/{id}
     *
     * @param int $id
     * @param UpdateH360coupanAPIRequest $request
     *
     * @return \Illuminate\Http\JsonResponse|Response
     */
    public function update($id, UpdateH360coupanAPIRequest $request)
    {
        $input = $request->all();

        /** @var H360coupan $h360coupan */
        $h360coupan = $this->h360coupanRepository->find($id);

        if (empty($h360coupan)) {
            return $this->sendError('H360Coupan not found');
        }

        $h360coupan = $this->h360coupanRepository->update($input, $id);

        return $this->sendResponse($h360coupan->toArray(), 'H360coupan updated successfully');
    }

    /**
     * Remove the specified H360coupan from storage.
     * DELETE /h360coupans/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return \Illuminate\Http\JsonResponse|Response
     */
    public function destroy($id)
    {
        /** @var H360coupan $h360coupan */
        $h360coupan = $this->h360coupanRepository->find($id);

        if (empty($h360coupan)) {
            return $this->sendError('H360Coupan not found');
        }

        $h360coupan->delete();

        return $this->sendSuccess('H360Coupan deleted successfully');
    }
}
