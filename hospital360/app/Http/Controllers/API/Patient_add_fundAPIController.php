<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePatient_add_fundAPIRequest;
use App\Http\Requests\API\UpdatePatient_add_fundAPIRequest;
use App\Models\Patient_add_fund;
use App\Repositories\Patient_add_fundRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;
use Auth;
use DB;

/**
 * Class Patient_add_fundController
 * @package App\Http\Controllers\API
 */

class Patient_add_fundAPIController extends AppBaseController
{
    /** @var  Patient_add_fundRepository */
    private $patientAddFundRepository;

    public function __construct(Patient_add_fundRepository $patientAddFundRepo)
    {
        $this->patientAddFundRepository = $patientAddFundRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/patientAddFunds",
     *      summary="Get a listing of the Patient_add_funds.",
     *      tags={"Patient_add_fund"},
     *      description="Get all Patient_add_funds",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Patient_add_fund")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $patientAddFunds = $this->patientAddFundRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($patientAddFunds->toArray(), 'Patient Add Funds retrieved successfully');
    }

     public function patient_add_funds_data(Request $request)
    {
        
         $patient_add_fund = Patient_add_fund::where('patient_id',$request->patient_id)
                        ->join('patient_details', 'patient_add_fund.patient_id', '=', 'patient_details.id')
                        ->select('patient_add_fund.*','patient_details.first_name as patient')
                        ->first();

        
      if ($patient_add_fund) {
            return response()->json(["status" => true,"data" => $patient_add_fund, "message" => "Patient Add Funds retrieved successfully","path" => env('APP_URL').'public/media/transaction_proof']);
        }else{
            return response()->json(["status" => false,"data" => ''     , "message" => "Patient Add Funds Not retrieved successfully"]);
        }
        /*return $this->sendResponse($myFavourite->toArray(), 'My Favourites retrieved successfully');*/
    }

    /**
     * @param CreatePatient_add_fundAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/patientAddFunds",
     *      summary="Store a newly created Patient_add_fund in storage",
     *      tags={"Patient_add_fund"},
     *      description="Store Patient_add_fund",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Patient_add_fund that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Patient_add_fund")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Patient_add_fund"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function patient_balance(Request $request)
    {
        
        //  $patient_balance = Patient_add_fund::where('patient_id',$request->patient_id)->select('patient_add_fund.*','patient_add_fund.wa_amount as requested_amount')->get();
        $patient_balance =  Patient_add_fund::where('patient_id', $request->patient_id)->orderBy('patient_id', 'desc')->first();
         if ($patient_balance) {
                return response()->json(["status" => true,"message"=>"Patient Add Fund saved successfully","data" => $patient_balance]);
            }else{
                return response()->json(["status" => false,"data" => '']);
            }
    }
    public function store(Request $request)
    {
        $input = $request->all();
        
        $last_amount = Patient_add_fund::where('patient_id',$request->patient_id)->first();
        if(!empty($last_amount)){
          $amount = $last_amount->wa_amount + $request->requested_amount;
          $patientAddFund = DB::table('patient_add_fund')->where('patient_id',$request->patient_id)->update(['wa_amount' => $amount]);
          if ($patientAddFund) {
                 $last_amount_list = Patient_add_fund::where('patient_id',$request->patient_id)->get();
                return response()->json(["status" => true,"message"=>"Patient Add Fund saved successfully","data" => $last_amount_list]);
            }else{
                return response()->json(["status" => false,"data" => '']);
            }
        }else{
             
        // $input['created_by'] = Auth::user()->id;
        // $input['updated_by'] = Auth::user()->id;
        $input['created_by'] = $request->patient_id;
        $input['updated_by'] = $request->patient_id;
        $input['wa_amount'] = $request->requested_amount;
        $patientAddFund = $this->patientAddFundRepository->create($input);
         if ($patientAddFund) {
                 $last_amount_list = Patient_add_fund::where('patient_id',$request->patient_id)->get();
                return response()->json(["status" => true,"message"=>"Patient Add Fund saved successfully","data" => $last_amount_list]);
            }else{
                return response()->json(["status" => false,"data" => '']);
            }
      }
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/patientAddFunds/{id}",
     *      summary="Display the specified Patient_add_fund",
     *      tags={"Patient_add_fund"},
     *      description="Get Patient_add_fund",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Patient_add_fund",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Patient_add_fund"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Patient_add_fund $patientAddFund */
        $patientAddFund = $this->patientAddFundRepository->find($id);

        if (empty($patientAddFund)) {
            return $this->sendError('Patient Add Fund not found');
        }

        return $this->sendResponse($patientAddFund->toArray(), 'Patient Add Fund retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdatePatient_add_fundAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/patientAddFunds/{id}",
     *      summary="Update the specified Patient_add_fund in storage",
     *      tags={"Patient_add_fund"},
     *      description="Update Patient_add_fund",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Patient_add_fund",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Patient_add_fund that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Patient_add_fund")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Patient_add_fund"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdatePatient_add_fundAPIRequest $request)
    {
        $input = $request->all();

        /** @var Patient_add_fund $patientAddFund */
        $patientAddFund = $this->patientAddFundRepository->find($id);

        if (empty($patientAddFund)) {
            return $this->sendError('Patient Add Fund not found');
        }

        $patientAddFund = $this->patientAddFundRepository->update($input, $id);

        return $this->sendResponse($patientAddFund->toArray(), 'Patient_add_fund updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/patientAddFunds/{id}",
     *      summary="Remove the specified Patient_add_fund from storage",
     *      tags={"Patient_add_fund"},
     *      description="Delete Patient_add_fund",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Patient_add_fund",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Patient_add_fund $patientAddFund */
        $patientAddFund = $this->patientAddFundRepository->find($id);

        if (empty($patientAddFund)) {
            return $this->sendError('Patient Add Fund not found');
        }

        $patientAddFund->delete();

        return $this->sendSuccess('Patient Add Fund deleted successfully');
    }
}
