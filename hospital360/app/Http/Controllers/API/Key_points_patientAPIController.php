<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateKey_points_patientAPIRequest;
use App\Http\Requests\API\UpdateKey_points_patientAPIRequest;
use App\Models\Key_points_patient;
use App\Repositories\Key_points_patientRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;
use DB;
use Auth;


/**
 * Class Key_points_patientController
 * @package App\Http\Controllers\API
 */

class Key_points_patientAPIController extends AppBaseController
{
    /** @var  Key_points_patientRepository */
    private $keyPointsPatientRepository;

    public function __construct(Key_points_patientRepository $keyPointsPatientRepo)
    {
        $this->keyPointsPatientRepository = $keyPointsPatientRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/keyPointsPatients",
     *      summary="Get a listing of the Key_points_patients.",
     *      tags={"Key_points_patient"},
     *      description="Get all Key_points_patients",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Key_points_patient")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $keyPointsPatients = $this->keyPointsPatientRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($keyPointsPatients->toArray(), 'Key Points Patients retrieved successfully');
    }

    /**
     * @param CreateKey_points_patientAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/keyPointsPatients",
     *      summary="Store a newly created Key_points_patient in storage",
     *      tags={"Key_points_patient"},
     *      description="Store Key_points_patient",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Key_points_patient that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Key_points_patient")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Key_points_patient"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateKey_points_patientAPIRequest $request)
    {
         $book =  DB::table('key_points_patient')->where('k_book_id', $request->k_book_id)->orderBy('k_book_id', 'desc')->first();
        if(!empty($book)){
            $input = $request->all();
            $id = $book->id;
            $keyPointsPatient = $this->keyPointsPatientRepository->update($input, $id);

        }else
        {
            $input = $request->all();
            $input['created_by'] = Auth::user()->id;
            $input['updated_by'] = Auth::user()->id;
            $keyPointsPatient = $this->keyPointsPatientRepository->create($input);
        }
        return $this->sendResponse($keyPointsPatient->toArray(), 'Key Points Patient saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/keyPointsPatients/{id}",
     *      summary="Display the specified Key_points_patient",
     *      tags={"Key_points_patient"},
     *      description="Get Key_points_patient",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Key_points_patient",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Key_points_patient"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Key_points_patient $keyPointsPatient */
        $keyPointsPatient = $this->keyPointsPatientRepository->find($id);

        if (empty($keyPointsPatient)) {
            return $this->sendError('Key Points Patient not found');
        }

        return $this->sendResponse($keyPointsPatient->toArray(), 'Key Points Patient retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateKey_points_patientAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/keyPointsPatients/{id}",
     *      summary="Update the specified Key_points_patient in storage",
     *      tags={"Key_points_patient"},
     *      description="Update Key_points_patient",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Key_points_patient",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Key_points_patient that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Key_points_patient")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Key_points_patient"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateKey_points_patientAPIRequest $request)
    {
        $input = $request->all();

        /** @var Key_points_patient $keyPointsPatient */
        $keyPointsPatient = $this->keyPointsPatientRepository->find($id);

        if (empty($keyPointsPatient)) {
            return $this->sendError('Key Points Patient not found');
        }

        $keyPointsPatient = $this->keyPointsPatientRepository->update($input, $id);

        return $this->sendResponse($keyPointsPatient->toArray(), 'Key_points_patient updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/keyPointsPatients/{id}",
     *      summary="Remove the specified Key_points_patient from storage",
     *      tags={"Key_points_patient"},
     *      description="Delete Key_points_patient",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Key_points_patient",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Key_points_patient $keyPointsPatient */
        $keyPointsPatient = $this->keyPointsPatientRepository->find($id);

        if (empty($keyPointsPatient)) {
            return $this->sendError('Key Points Patient not found');
        }

        $keyPointsPatient->delete();

        return $this->sendSuccess('Key Points Patient deleted successfully');
    }
}
