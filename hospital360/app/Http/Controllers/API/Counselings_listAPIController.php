<?php



namespace App\Http\Controllers\API;



use App\Http\Requests\API\CreateCounselings_listAPIRequest;

use App\Http\Requests\API\UpdateCounselings_listAPIRequest;

use App\Models\Counselings_list;

use App\Repositories\Counselings_listRepository;

use Illuminate\Http\Request;

use App\Http\Controllers\AppBaseController;

use Response;
use Auth;


/**

 * Class Counselings_listController

 * @package App\Http\Controllers\API

 */



class Counselings_listAPIController extends AppBaseController

{

    /** @var  Counselings_listRepository */

    private $counselingsListRepository;



    public function __construct(Counselings_listRepository $counselingsListRepo)

    {

        $this->counselingsListRepository = $counselingsListRepo;

    }



    /**

     * Display a listing of the Counselings_list.

     * GET|HEAD /counselingsLists

     *

     * @param Request $request

     * @return \Illuminate\Http\JsonResponse|Response

     */

    public function index(Request $request)

    {

        $counselingsLists = $this->counselingsListRepository->all(

            $request->except(['skip', 'limit']),

            $request->get('skip'),

            $request->get('limit')

        );



        return $this->sendResponse($counselingsLists->toArray(), 'Counselings Lists retrieved successfully');

    }



    /**

     * Store a newly created Counselings_list in storage.

     * POST /counselingsLists

     *

     * @param CreateCounselings_listAPIRequest $request

     *

     * @return \Illuminate\Http\JsonResponse|Response

     */

    public function store(CreateCounselings_listAPIRequest $request)

    {

        $input = $request->all();

        $input['created_by']= Auth::user()->id;
        $input['updated_by']= Auth::user()->id;

        $counselingsList = $this->counselingsListRepository->create($input);
        $counselingsList->dr_id =     $request->dr_id;
       
        $counselingsList->save();


        return $this->sendResponse($counselingsList->toArray(), 'Counselings List saved successfully');

    }



    /**

     * Display the specified Counselings_list.

     * GET|HEAD /counselingsLists/{id}

     *

     * @param int $id

     *

     * @return \Illuminate\Http\JsonResponse|Response

     */

    public function show($id)

    {

        /** @var Counselings_list $counselingsList */

        // $counselingsList = $this->counselingsListRepository->find($id);

        $counselingsList = Counselings_list::select('*')->where('dr_id', $id)->get();

        if (empty($counselingsList)) {

            return $this->sendError('Counselings List not found');

        }



        return $this->sendResponse($counselingsList->toArray(), 'Counselings List retrieved successfully');

    }



    /**

     * Update the specified Counselings_list in storage.

     * PUT/PATCH /counselingsLists/{id}

     *

     * @param int $id

     * @param UpdateCounselings_listAPIRequest $request

     *

     * @return \Illuminate\Http\JsonResponse|Response

     */

    public function update($id, UpdateCounselings_listAPIRequest $request)

    {

        $input = $request->all();



        /** @var Counselings_list $counselingsList */

        $counselingsList = $this->counselingsListRepository->find($id);



        if (empty($counselingsList)) {

            return $this->sendError('Counselings List not found');

        }



        $counselingsList = $this->counselingsListRepository->update($input, $id);



        return $this->sendResponse($counselingsList->toArray(), 'Counselings_list updated successfully');

    }



    /**

     * Remove the specified Counselings_list from storage.

     * DELETE /counselingsLists/{id}

     *

     * @param int $id

     *

     * @throws \Exception

     *

     * @return \Illuminate\Http\JsonResponse|Response

     */

    public function destroy($id)

    {

        /** @var Counselings_list $counselingsList */

        $counselingsList = $this->counselingsListRepository->find($id);



        if (empty($counselingsList)) {

            return $this->sendError('Counselings List not found');

        }



        $counselingsList->delete();



        return $this->sendSuccess('Counselings List deleted successfully');

    }

}

