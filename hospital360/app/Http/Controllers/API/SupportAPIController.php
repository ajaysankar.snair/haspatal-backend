<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateSupportAPIRequest;
use App\Http\Requests\API\UpdateSupportAPIRequest;
use App\Models\Support;
use App\Repositories\SupportRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class SupportController
 * @package App\Http\Controllers\API
 */

class SupportAPIController extends AppBaseController
{
    /** @var  SupportRepository */
    private $supportRepository;

    public function __construct(SupportRepository $supportRepo)
    {
        $this->supportRepository = $supportRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/supports",
     *      summary="Get a listing of the Supports.",
     *      tags={"Support"},
     *      description="Get all Supports",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Support")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $supports = $this->supportRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($supports->toArray(), 'Supports retrieved successfully');
    }

    /**
     * @param CreateSupportAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/supports",
     *      summary="Store a newly created Support in storage",
     *      tags={"Support"},
     *      description="Store Support",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Support that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Support")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Support"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateSupportAPIRequest $request)
    {
        $input = $request->all();

        $support = $this->supportRepository->create($input);

        return $this->sendResponse($support->toArray(), 'Support saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/supports/{id}",
     *      summary="Display the specified Support",
     *      tags={"Support"},
     *      description="Get Support",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Support",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Support"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Support $support */
        $support = $this->supportRepository->find($id);

        if (empty($support)) {
            return $this->sendError('Support not found');
        }

        return $this->sendResponse($support->toArray(), 'Support retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateSupportAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/supports/{id}",
     *      summary="Update the specified Support in storage",
     *      tags={"Support"},
     *      description="Update Support",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Support",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Support that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Support")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Support"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateSupportAPIRequest $request)
    {
        $input = $request->all();

        /** @var Support $support */
        $support = $this->supportRepository->find($id);

        if (empty($support)) {
            return $this->sendError('Support not found');
        }

        $support = $this->supportRepository->update($input, $id);

        return $this->sendResponse($support->toArray(), 'Support updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/supports/{id}",
     *      summary="Remove the specified Support from storage",
     *      tags={"Support"},
     *      description="Delete Support",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Support",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Support $support */
        $support = $this->supportRepository->find($id);

        if (empty($support)) {
            return $this->sendError('Support not found');
        }

        $support->delete();

        return $this->sendSuccess('Support deleted successfully');
    }
}
