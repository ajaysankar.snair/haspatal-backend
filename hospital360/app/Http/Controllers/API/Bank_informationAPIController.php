<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateBank_informationAPIRequest;
use App\Http\Requests\API\UpdateBank_informationAPIRequest;
use App\Models\Bank_information;
use App\Repositories\Bank_informationRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class Bank_informationController
 * @package App\Http\Controllers\API
 */

class Bank_informationAPIController extends AppBaseController
{
    /** @var  Bank_informationRepository */
    private $bankInformationRepository;

    public function __construct(Bank_informationRepository $bankInformationRepo)
    {
        $this->bankInformationRepository = $bankInformationRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/bankInformations",
     *      summary="Get a listing of the Bank_informations.",
     *      tags={"Bank_information"},
     *      description="Get all Bank_informations",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Bank_information")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $bankInformations = $this->bankInformationRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($bankInformations->toArray(), 'Bank Informations retrieved successfully');
    }

    /**
     * @param CreateBank_informationAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/bankInformations",
     *      summary="Store a newly created Bank_information in storage",
     *      tags={"Bank_information"},
     *      description="Store Bank_information",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Bank_information that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Bank_information")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Bank_information"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateBank_informationAPIRequest $request)
    {
        $input = $request->all();

        $bankInformation = $this->bankInformationRepository->create($input);

        return $this->sendResponse($bankInformation->toArray(), 'Bank Information saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/bankInformations/{id}",
     *      summary="Display the specified Bank_information",
     *      tags={"Bank_information"},
     *      description="Get Bank_information",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Bank_information",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Bank_information"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Bank_information $bankInformation */
        $bankInformation = $this->bankInformationRepository->find($id);

        if (empty($bankInformation)) {
            return $this->sendError('Bank Information not found');
        }

        return $this->sendResponse($bankInformation->toArray(), 'Bank Information retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateBank_informationAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/bankInformations/{id}",
     *      summary="Update the specified Bank_information in storage",
     *      tags={"Bank_information"},
     *      description="Update Bank_information",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Bank_information",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Bank_information that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Bank_information")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Bank_information"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateBank_informationAPIRequest $request)
    {
        $input = $request->all();

        /** @var Bank_information $bankInformation */
        $bankInformation = $this->bankInformationRepository->find($id);

        if (empty($bankInformation)) {
            return $this->sendError('Bank Information not found');
        }

        $bankInformation = $this->bankInformationRepository->update($input, $id);

        return $this->sendResponse($bankInformation->toArray(), 'Bank_information updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/bankInformations/{id}",
     *      summary="Remove the specified Bank_information from storage",
     *      tags={"Bank_information"},
     *      description="Delete Bank_information",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Bank_information",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Bank_information $bankInformation */
        $bankInformation = $this->bankInformationRepository->find($id);

        if (empty($bankInformation)) {
            return $this->sendError('Bank Information not found');
        }

        $bankInformation->delete();

        return $this->sendSuccess('Bank Information deleted successfully');
    }
}
