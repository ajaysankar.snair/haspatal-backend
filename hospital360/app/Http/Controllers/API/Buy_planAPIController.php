<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateBuy_planAPIRequest;
use App\Http\Requests\API\UpdateBuy_planAPIRequest;
use App\Models\Buy_plan;
use App\Models\Wallet_360;
use App\Repositories\Buy_planRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;
use DB;
use Auth;

/**
 * Class Buy_planController
 * @package App\Http\Controllers\API
 */

class Buy_planAPIController extends AppBaseController
{
    /** @var  Buy_planRepository */
    private $buyPlanRepository;

    public function __construct(Buy_planRepository $buyPlanRepo)
    {
        $this->buyPlanRepository = $buyPlanRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/buyPlans",
     *      summary="Get a listing of the Buy_plans.",
     *      tags={"Buy_plan"},
     *      description="Get all Buy_plans",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Buy_plan")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $buyPlans = $this->buyPlanRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($buyPlans->toArray(), 'Buy Plans retrieved successfully');
    }

    /**
     * @param CreateBuy_planAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/buyPlans",
     *      summary="Store a newly created Buy_plan in storage",
     *      tags={"Buy_plan"},
     *      description="Store Buy_plan",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Buy_plan that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Buy_plan")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Buy_plan"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateBuy_planAPIRequest $request)
    {
        $input = $request->all();
        $input['created_by'] = Auth::user()->id;
        $input['updated_by'] = Auth::user()->id;
    
          if($request->w_status == 1)
          {
            $total_fund =  DB::table('wallet_360')->where('user_id', $request->user_id)->first();
             $mi_fund = $total_fund->wa_amount - $request->price;
            //$input['w_amount'] =  $mi_fund;
            $fund =  DB::table('wallet_360')->where('user_id',$request->user_id)->update(['wa_amount' => $mi_fund]);
          }
        $buyPlan = $this->buyPlanRepository->create($input);

        return $this->sendResponse($buyPlan->toArray(), 'Buy Plan saved successfully');
    }
      public function wallet_balance_360(Request $request)
    {
        
       // $balance =  DB::table('wallet_360')->where('user_id', $request->user_id)->orderBy('user_id', 'desc')->get();   

                $balance=wallet_360::get()->where('user_id', $request->user_id)->first();
           
       if (!empty($balance)) {
            return response()->json(["status" => true,"massage"=>"Balance retrieved successfully","balance" => $balance]);
        }else{
            return response()->json(["status" => false,"balance" =>0]);
        }
    }
    public function active_plan(Request $request)
    {
        
        $active_plan =  DB::table('buy_plan')
                        ->join('h_plan','buy_plan.plan_id','h_plan.id')
                        ->where('buy_plan.user_id', $request->user_id)
                        //->orderBy('buy_plan.user_id', 'desc')
                        ->select('buy_plan.*','h_plan.plan_name')
                        ->get();   
      
       if ($active_plan) {
            return response()->json(["status" => true,"massage"=>"Active Plan retrieved successfully","active_plan" => $active_plan]);
        }else{
            return response()->json(["status" => false,"active_plan" => $active_plan]);
        }
    }
    public function statement_360(Request $request)
    {
        
        $statement_360 =  DB::table('buy_plan')
                        ->join('wallet_360','buy_plan.user_id','wallet_360.user_id')
                        ->join('h_plan','buy_plan.plan_id','h_plan.id')
                        ->where('buy_plan.user_id', $request->user_id)
                        //->orderBy('buy_plan.user_id', 'desc')
                        ->select('buy_plan.*','wallet_360.wa_amount','wallet_360.payment_id','h_plan.plan_name')
                        ->get();   
      
       if ($statement_360) {
            return response()->json(["status" => true,"massage"=>"Statement retrieved successfully","statement_360" => $statement_360]);
        }else{
            return response()->json(["status" => false,"statement_360" => $statement_360]);
        }
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/buyPlans/{id}",
     *      summary="Display the specified Buy_plan",
     *      tags={"Buy_plan"},
     *      description="Get Buy_plan",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Buy_plan",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Buy_plan"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Buy_plan $buyPlan */
        $buyPlan = $this->buyPlanRepository->find($id);

        if (empty($buyPlan)) {
            return $this->sendError('Buy Plan not found');
        }

        return $this->sendResponse($buyPlan->toArray(), 'Buy Plan retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateBuy_planAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/buyPlans/{id}",
     *      summary="Update the specified Buy_plan in storage",
     *      tags={"Buy_plan"},
     *      description="Update Buy_plan",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Buy_plan",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Buy_plan that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Buy_plan")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Buy_plan"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateBuy_planAPIRequest $request)
    {
        $input = $request->all();

        /** @var Buy_plan $buyPlan */
        $buyPlan = $this->buyPlanRepository->find($id);

        if (empty($buyPlan)) {
            return $this->sendError('Buy Plan not found');
        }

        $buyPlan = $this->buyPlanRepository->update($input, $id);

        return $this->sendResponse($buyPlan->toArray(), 'Buy_plan updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/buyPlans/{id}",
     *      summary="Remove the specified Buy_plan from storage",
     *      tags={"Buy_plan"},
     *      description="Delete Buy_plan",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Buy_plan",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Buy_plan $buyPlan */
        $buyPlan = $this->buyPlanRepository->find($id);

        if (empty($buyPlan)) {
            return $this->sendError('Buy Plan not found');
        }

        $buyPlan->delete();

        return $this->sendSuccess('Buy Plan deleted successfully');
    }
}
