<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePrescription_detailsRequest;
use App\Http\Requests\UpdatePrescription_detailsRequest;
use App\Repositories\Prescription_detailsRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;
use DB;
use Auth;
use App\User;
use PDF;

class Prescription_detailsController extends AppBaseController
{
    /** @var  Prescription_detailsRepository */
    private $prescriptionDetailsRepository;

    public function __construct(Prescription_detailsRepository $prescriptionDetailsRepo)
    {
        $this->prescriptionDetailsRepository = $prescriptionDetailsRepo;
    }

    /**
     * Display a listing of the Prescription_details.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        //$prescriptionDetails = $this->prescriptionDetailsRepository->paginate(10);
                                       
         $prescriptionDetails = DB::table('prescription_details')
         ->join('doctor_details','doctor_details.id','prescription_details.doctor_id')
         ->join('patient_details','patient_details.id','prescription_details.patient_id')
         ->select('prescription_details.*','patient_details.first_name as p_first_name','doctor_details.first_name as d_first_name')
         ->paginate(10);
          
                   return view('prescription_details.index')->with('prescriptionDetails', $prescriptionDetails);
           
    }

    /**
     * Show the form for creating a new Prescription_details.
     *
     * @return Response
     */
    public function create()
    {
        $patientList = DB::table('patient_details')->orderBy('first_name','asc')->pluck('first_name','id');
        $doctorList = DB::table('doctor_details')->orderBy('first_name','asc')->pluck('first_name','id');
        return view('prescription_details.create',compact('patientList','doctorList'));
        
    }

    /**
     * Store a newly created Prescription_details in storage.
     *
     * @param CreatePrescription_detailsRequest $request
     *
     * @return Response
     */
    public function store(CreatePrescription_detailsRequest $request)
    {
        $input = $request->all();
        $input['created_by'] = Auth::user()->id;
        $input['updated_by'] = Auth::user()->id;

        if($request->hasfile('prescription_image'))
        {
            $image = $request->file('prescription_image');
            $extension = $image->getClientOriginalExtension(); // getting image extension
            $filename =time().'__'.$image->getClientOriginalName();
            $path = public_path('/media/prescription_image/');
            $image->move($path, $filename);
            $input['prescription_image'] = $filename;
        }
        $patient = DB::table('patient_details')->where('id',$input['patient_id'])->select('first_name')->first();
        $doctor = DB::table('doctor_details')->where('id',$input['doctor_id'])->select('first_name')->first();
        $data = [
          'title' => 'prescription',
          'heading' => env('APP_URL').'/public/image/haspatallogo.png',
          'patient_id' => $patient->first_name,       
          'doctor_id' => $doctor->first_name,       
          'booking_id' => $input['booking_id'],       
          'prescription' => $input['prescription'],       
            ];
        
        //$pdf = PDF::loadView('pdf_view', $data); 
        return view('pdf_view', compact('data'));
        
        $path = public_path('pdf/');
        $fileName1 =   time() . '__'. 'Prescription.' . 'pdf' ;
        $pdf->save($path . '/' . $fileName1); 

        $input['pdf'] = 'public/pdf/'.$fileName1;
        
        $prescriptionDetails = $this->prescriptionDetailsRepository->create($input);

        Flash::success('Prescription Details saved successfully.');

        return redirect(route('prescriptionDetails.index'));
    }

    public function pdf_view($booking)
    {
        $book =  DB::table('prescription_details')->where('booking_id',$booking)->orderBy('booking_id', 'desc')->first();

        $patient = DB::table('patient_details')->where('id',$book->patient_id)->first();
        $doctor = DB::table('doctor_details')->where('id',$book->doctor_id)->first();
            
        $booking_details = DB::table('booking_request')->where('book_id',$booking)->first();

        $m_details =  DB::table('medicine_list')->where('id', $book->medicine_id)->first();

        //$pdf = \App::make('dompdf.wrapper');
/*$html = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
  <html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <style type="text/css">
      .override_margin{
          padding: 1px !important;
          margin: 0px !important;
      }
    </style>
  </head>
  <body style="padding: 1%">
    <div class="container">
      
      <div class="row" style="background-color: lightgray;height: 250px;padding: 2%;">
        <div class="col-sm-6">
          <h3>'.$doctor->clinic_buisness_name.'</h3>
          <span>'.$doctor->bio.'</span><br>
          <span>Registration number</span><br>
          <span>'.$doctor->address.'</span><br>
          <span>'.$doctor->email. '/'. $doctor->main_mobile.'</span><br>
        </div>
      </div>
      <div style="height: 10px"></div>
      <div class="row override_margin" >
          <div class="col-sm-6">
              <div class="form-group row">
                  <label for="invoice_no" class="col-sm-4 col-form-label">date of consultation
                  </label>
                  <div class="col-sm-8">
                      <input type="text" tabindex="1" class="form-control" value="" readonly=""  />
                  </div>
              </div>
          </div>

          <div class="col-sm-6">
              <div class="form-group row">
                  <label for="invoice_value" class="col-sm-4 col-form-label">AGE
                      
                  </label>
                  <div class="col-sm-8">
                      <input type="text" tabindex="4" class="form-control" value="{{$patient->age}}" readonly="" />
                  </div>
              </div>
          </div>
          
          
      </div>
      <div class="row override_margin" style="padding: 2%;">
          <div class="col-sm-6">
              <div class="form-group row">
                  <label for="invoice_no" class="col-sm-4 col-form-label">Name of patient
                  </label>
                  <div class="col-sm-8">
                      <input type="text" tabindex="1" class="form-control" value="{{$patient->first_name}}" readonly=""  />
                  </div>
              </div>
          </div>

          <div class="col-sm-6">
              <div class="form-group row">
                  <label for="invoice_value" class="col-sm-4 col-form-label">Gender
                      
                  </label>
                  <div class="col-sm-8">
                      <input type="text" tabindex="4" class="form-control" value="{{$patient->gender}}" readonly="" />
                  </div>
              </div>
          </div>
          
          
      </div>
      <div class="row override_margin" style="padding: 2%;">
          <div class="col-sm-6">
              <div class="form-group row">
                  <label for="invoice_no" class="col-sm-4 col-form-label">Address
                  </label>
                  <div class="col-sm-8">
                      <textarea class="form-control">
                        {{$patient->address}}
                      </textarea>
                  </div>
              </div>
          </div>

          <div class="col-sm-6">
              <div class="form-group row">
                  <label for="invoice_value" class="col-sm-4 col-form-label">Height
                      
                  </label>
                  <div class="col-sm-8">
                      <input type="text" tabindex="4" class="form-control" value="{{$patient->height}}" readonly="" />
                  </div>
              </div>
          </div>
          
          
      </div>
      <div class="row override_margin">
          <div class="col-sm-6">
          </div>

          <div class="col-sm-6">
              <div class="form-group row">
                  <label for="invoice_value" class="col-sm-4 col-form-label">LMP
                      
                  </label>
                  <div class="col-sm-8">
                      <input type="text" tabindex="4" class="form-control" value="{{$patient->lmp}}" readonly="" />
                  </div>
              </div>
          </div>
          
      </div>
      
      <hr>

      <div class="row" style="padding: 2%;margin: 0px !important;">
          <div class="col-sm-5">
              <b><label for="invoice_no" class="col-form-label">chief complaints</label></b>
              <div style="min-height: 50px;"></div>
              <b><label>RELEVANT Points From History</label></b>
              <div style="min-height: 50px;"></div>
              <b><label>Examination/Lab Findings</label></b>
              <div style="min-height: 50px;"></div>
              <b><label>Suggested Investigations</label></b>

          </div>

          <div class="col-sm-1" style="border-left: 1px solid #e5e5e5;height: 300px;"></div>
          
          <div class="col-sm-6">
            <b><label class="col-form-label">DIAGNOSIS OR PROVISIONAL DIAGNOSIS</label></b>
            <div style="min-height: 100px;"></div>

            <label>1. {{strtoupper($m_details->medicine_name)}} ({{strtoupper($m_details->generic_name)}})</label><br><label style="margin-top: -2%; margin-left: 3%; ">{{$m_details->drug_from}},  {{$m_details->drug_strength}}, {{$book->faq_administration}} & {{$book->duration}}</label>
            
          </div>

          
          
      </div>

      <hr>
      <div class="row" style="padding: 2%;margin: 0px !important;">
          <div class="col-sm-6">
              <b><label for="invoice_no" class="col-sm-6 col-form-label">Special Instructions</label></b>
          </div>

          <div class="col-sm-6" style="text-align: right;">
            <div style="min-height: 100px;"></div>
            <b><label>RMPs Signature & Stamp</label></b>
          </div>
          
          
      </div>
      <hr>
      <b><label style="font-size: xx-small;">Note: This prescription is generated on a teleconsultation.</label></b>
    </div>
  </body>
</html>';*/
                //$pdf->loadHTML($html)->setPaper('letter', 'landscape');
                $data['title'] = "prescription";

        $data['book'] =  DB::table('prescription_details')->where('booking_id',$booking)->orderBy('booking_id', 'desc')->first();

        $data['patient'] = DB::table('patient_details')->where('id',$book->patient_id)->first();
        $data['doctor'] = DB::table('doctor_details')->where('id',$book->doctor_id)->first();
            
        $data['booking_details'] = DB::table('booking_request')->where('book_id',$booking)->first();

        $data['m_details'] =  DB::table('medicine_list')->where('id', $book->medicine_id)->first();
        
        PDF::setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif']);

        $pdf = PDF::loadView('pdf_view', $data); 
                
        return $pdf->stream();
        $data['title'] = "prescription";

        $book =  DB::table('prescription_details')->where('booking_id',$booking)->orderBy('booking_id', 'desc')->first();

        $patient = DB::table('patient_details')->where('id',$book->patient_id)->first();
        $doctor = DB::table('doctor_details')->where('id',$book->doctor_id)->first();
            
        $booking_details = DB::table('booking_request')->where('book_id',$booking)->first();

        $m_details =  DB::table('medicine_list')->where('id', $book->medicine_id)->first();
        
        //$pdf = PDF::loadView('pdf_view', $data); 
        return view('pdf_view', compact('data','doctor','patient','booking_details','book','m_details'));
    }

    /**
     * Display the specified Prescription_details.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $prescriptionDetails = $this->prescriptionDetailsRepository->find($id);

        if (empty($prescriptionDetails)) {
            Flash::error('Prescription Details not found');

            return redirect(route('prescriptionDetails.index'));
        }

        return view('prescription_details.show')->with('prescriptionDetails', $prescriptionDetails);
    }

    /**
     * Show the form for editing the specified Prescription_details.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $prescriptionDetails = $this->prescriptionDetailsRepository->find($id);

        if (empty($prescriptionDetails)) {
            Flash::error('Prescription Details not found');

            return redirect(route('prescriptionDetails.index'));
        }
        $patientList = DB::table('patient_details')->pluck('first_name','id');
        $doctorList = DB::table('doctor_details')->pluck('first_name','id');
        return view('prescription_details.edit',compact('patientList','doctorList'))->with('prescriptionDetails', $prescriptionDetails);
    }

    /**
     * Update the specified Prescription_details in storage.
     *
     * @param int $id
     * @param UpdatePrescription_detailsRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePrescription_detailsRequest $request)
    {
        $prescriptionDetails = $this->prescriptionDetailsRepository->find($id);
        if($request->hasfile('prescription_image'))
        {
            $image = $request->file('prescription_image');
            $extension = $image->getClientOriginalExtension(); // getting image extension
            $filename =time().'__'.$image->getClientOriginalName();
            $path = public_path('/media/prescription_image/');
            $image->move($path, $filename);
            $input['prescription_image'] = $filename;
        }
        if (empty($prescriptionDetails)) {
            Flash::error('Prescription Details not found');

            return redirect(route('prescriptionDetails.index'));
        }

        $prescriptionDetails = $this->prescriptionDetailsRepository->update($input, $id);

        Flash::success('Prescription Details updated successfully.');

        return redirect(route('prescriptionDetails.index'));
    }

    /**
     * Remove the specified Prescription_details from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $prescriptionDetails = $this->prescriptionDetailsRepository->find($id);

        if (empty($prescriptionDetails)) {
            Flash::error('Prescription Details not found');

            return redirect(route('prescriptionDetails.index'));
        }

        $this->prescriptionDetailsRepository->delete($id);

        Flash::success('Prescription Details deleted successfully.');

        return redirect(route('prescriptionDetails.index'));
    }
}
