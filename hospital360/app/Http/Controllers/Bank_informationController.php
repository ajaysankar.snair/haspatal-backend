<?php

namespace App\Http\Controllers;

use App\DataTables\Bank_informationDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateBank_informationRequest;
use App\Http\Requests\UpdateBank_informationRequest;
use App\Repositories\Bank_informationRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Auth;
use DB;
class Bank_informationController extends AppBaseController
{
    /** @var  Bank_informationRepository */
    private $bankInformationRepository;

    public function __construct(Bank_informationRepository $bankInformationRepo)
    {
        $this->bankInformationRepository = $bankInformationRepo;
    }

    /**
     * Display a listing of the Bank_information.
     *
     * @param Bank_informationDataTable $bankInformationDataTable
     * @return Response
     */
    public function index(Bank_informationDataTable $bankInformationDataTable)
    {
        return $bankInformationDataTable->render('bank_informations.index');
    }

    /**
     * Show the form for creating a new Bank_information.
     *
     * @return Response
     */
    public function create()
    {
        return view('bank_informations.create');
    }

    /**
     * Store a newly created Bank_information in storage.
     *
     * @param CreateBank_informationRequest $request
     *
     * @return Response
     */
    public function store(CreateBank_informationRequest $request)
    {
        $input = $request->all();
        if($request->hasfile('cancle_cheque'))
        {
            $image = $request->file('cancle_cheque');
            $extension = $image->getClientOriginalExtension(); // getting image extension
            $filename =time().'__'.$image->getClientOriginalName();
            $path = public_path('/media/cancle_cheque/');
            $image->move($path, $filename);
            $input['cancle_cheque'] = $filename;
        }
        $input['created_by'] = Auth::user()->id;
        $input['updated_by'] = Auth::user()->id;
        $bankInformation = $this->bankInformationRepository->create($input);
        
        Flash::success('Bank Information saved successfully.');

        return redirect(route('bankInformations.index'));
    }

    /**
     * Display the specified Bank_information.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $bankInformation = $this->bankInformationRepository->find($id);

        if (empty($bankInformation)) {
            Flash::error('Bank Information not found');

            return redirect(route('bankInformations.index'));
        }

        return view('bank_informations.show')->with('bankInformation', $bankInformation);
    }

    /**
     * Show the form for editing the specified Bank_information.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $bankInformation = $this->bankInformationRepository->find($id);

        if (empty($bankInformation)) {
            Flash::error('Bank Information not found');

            return redirect(route('bankInformations.index'));
        }

        return view('bank_informations.edit')->with('bankInformation', $bankInformation);
    }

    /**
     * Update the specified Bank_information in storage.
     *
     * @param  int              $id
     * @param UpdateBank_informationRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateBank_informationRequest $request)
    {
        $bankInformation = $this->bankInformationRepository->find($id);

        if (empty($bankInformation)) {
            Flash::error('Bank Information not found');

            return redirect(route('bankInformations.index'));
        }

        $bankInformation = $this->bankInformationRepository->update($request->all(), $id);

        Flash::success('Bank Information updated successfully.');

        return redirect(route('bankInformations.index'));
    }

    /**
     * Remove the specified Bank_information from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $bankInformation = $this->bankInformationRepository->find($id);

        if (empty($bankInformation)) {
            Flash::error('Bank Information not found');

            return redirect(route('bankInformations.index'));
        }

        $this->bankInformationRepository->delete($id);

        Flash::success('Bank Information deleted successfully.');

        return redirect(route('bankInformations.index'));
    }
}
