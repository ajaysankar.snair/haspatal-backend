<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Http\Request;

Route::get('/', function () {
    return view('auth.login');
});

Route::get('/regsitersHaspatal', function () {
    return view('auth.haspatal.register');
})->name('haspatal.register.create');

Route::get('/regsitersH360', function () {
    return view('auth.haspatal.register360');
})->name('haspatal.register360.create');

Route::get('loginadmin', function () {
    return view('loginadmin');
});
Route::get('registers', function () {
    return view('adminregistartion');
});
Route::get('doctorlist', function () {
    return view('doctorlist');
});
Route::get('singledoctor', function () {
    return view('singledoctor');
});
// Route::get('aprrovereg', function () {
//     return view('approvereg');
// });
Route::get('views', function () {
    return view('views');
});
Route::get('terms', function () {
    return view('terms');
});
Route::get('approvals', function () {
    return view('approvereg');
});
Route::get('rjct', function () {
    return view('rjct');
});
Route::get('indexs', function () {
    return view('indexs');
});
Route::get('orders', function () {
    return view('orders');
});
Route::get('todays', function () {
    return view('todays');
});
Route::get('pending', function () {
    return view('pending');
});
Route::get('complete', function () {
    return view('complete');
});
Route::get('otp', function () {
    return view('otp');
});
Route::get('wallpaper', function () {
    return view('wallpaper');
});
Route::get('/hasptal_otp_send',function(){
    return view('auth.haspatal.otp');
});

Route::post('/hasptal_otp_send','HaspatalRegsiterController@send_otp')->name('haspatal.otp');

Route::post('/hasptal_register','HaspatalRegsiterController@register')->name('haspatal.register');
Route::post('/hasptal_register_360','Haspatal_360_registerController@register')->name('haspatal.register360');

Route::get('send_mail','API\DoctorsAPIController@send_mail');

Route::resource('users', 'UserController')->middleware('auth');
Route::get('get-state-list',function(Request $request){
    $states = \App\Models\State::where("country_id",$request->country_id)->orderBy('state','asc')->pluck("state","id");
        //echo "<pre>";print_r($states);exit;
        return response()->json($states);
});
Route::get('/aprroveregs', 'adminsController@aprroveregs');
Route::get('/rjcts', 'adminsController@rjcts');
Route::get('/orders', 'adminsController@allorders');
Route::get('/todays', 'adminsController@todays');
Route::get('/pending', 'adminsController@pending');
Route::get('/complete', 'adminsController@complete');
Route::get('/allreg', 'adminsController@allbus');
Route::get('/alldoctor', 'adminsController@alldoctor');
Route::post('settings', 'adminsController@settings')->name('settings');
Route::post('doctorsingle', 'adminsController@doctorsingle')->name('doctorsingle');
Route::post('loginregister', 'adminsController@loginregister')->name('loginregister');
Route::post('adminregister', 'adminsController@adminregister')->name('adminregister');
Route::post('approve', 'adminsController@approve')->name('approve');
Route::post('doctorapprove', 'adminsController@doctorapprove')->name('doctorapprove');
Route::post('doctorhold', 'adminsController@doctorhold')->name('doctorhold');
Route::post('reject', 'adminsController@reject')->name('reject');
Route::post('otpVerification', 'adminsController@otpVerification')->name('otpVerification');
Route::post('hold', 'adminsController@hold')->name('hold');
Route::post('deleteregister', 'adminsController@deleteregister')->name('deleteregister');
Route::get('get-city-list',function(Request $request){
     $city = \App\Models\City::where("state_name",$request->state_id)->orderBy('city','asc')->pluck("city","id");
        return response()->json($city);
});
Route::get('user_active/{id}','HomeController@user_active')->name('user_active');
Route::get('user_deactive/{id}','HomeController@user_deactive')->name('user_deactive');

Route::get('dr_active/{id}','DoctorsController@dr_active')->name('dr_active');
Route::get('dr_deactive/{id}','DoctorsController@dr_deactive')->name('dr_deactive');

Route::get('h360_active/{id}','Haspatal_360_registerController@h360_active')->name('h360_active');
Route::get('h360_deactive/{id}','Haspatal_360_registerController@h360_deactive')->name('h360_deactive');

Route::get('plan_active/{id}','Plan_masterController@plan_active')->name('plan_active');
Route::get('plan_deactive/{id}','Plan_masterController@plan_deactive')->name('plan_deactive');

Route::get('get-hospitalservice-list','HomeController@hospitalservicelist');
Route::get('get-hospitaldoctor-list','HomeController@hospitaldoctorlist');

Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->middleware('verified');

Route::resource('users', 'UserController')->middleware('auth');


Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->middleware('verified');


Route::get('generator_builder', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@builder')->name('io_generator_builder');

Route::get('field_template', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@fieldTemplate')->name('io_field_template');

Route::get('relation_field_template', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@relationFieldTemplate')->name('io_relation_field_template');

Route::post('generator_builder/generate', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@generate')->name('io_generator_builder_generate');

Route::post('generator_builder/rollback', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@rollback')->name('io_generator_builder_rollback');

Route::post(
    'generator_builder/generate-from-file',
    '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@generateFromFile'
)->name('io_generator_builder_generate_from_file');

Route::get('generator_builder', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@builder')->name('io_generator_builder');

Route::get('field_template', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@fieldTemplate')->name('io_field_template');

Route::get('relation_field_template', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@relationFieldTemplate')->name('io_relation_field_template');

Route::post('generator_builder/generate', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@generate')->name('io_generator_builder_generate');

Route::post('generator_builder/rollback', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@rollback')->name('io_generator_builder_rollback');

Route::post(
    'generator_builder/generate-from-file',
    '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@generateFromFile'
)->name('io_generator_builder_generate_from_file');

//User Module with roles & permissions
//User
Route::group(['prefix' => 'user', 'middleware' => ['auth']], function () {
    Route::get('/', 'AclController@userIndex');
    Route::get('create', 'AclController@createUser');
    Route::post('/', 'AclController@storeUser');
    Route::get('{id}/edit', 'AclController@editUser');
    Route::post('{id}/update', 'AclController@updateUser');
    Route::post('{id}/delete', 'AclController@deleteUser')->name('user.delete');
});

//Roles
Route::group(['prefix' => 'user/role', 'middleware' => ['auth']], function () {
    Route::get('/', 'AclController@roleIndex');
    Route::get('create', 'AclController@createRole');
    Route::post('/', 'AclController@storeRole');
    Route::get('{id}/edit', 'AclController@editRole');
    Route::post('{id}/update', 'AclController@updateRole');
    Route::post('{id}/delete', 'AclController@deleteRole');
});

//Permissions
Route::group(['prefix' => 'user/permission', 'middleware' => ['auth']], function () {
    Route::get('/', 'AclController@permissionIndex');
    Route::get('create', 'AclController@createPermission');
    Route::post('/', 'AclController@storePermission');
    Route::get('{id}/edit', 'AclController@editPermission');
    Route::post('{id}/update', 'AclController@updatePermission');
    Route::post('{id}/delete', 'AclController@deletePermission');
});


Route::group(['prefix' => 'report', 'middleware' => ['auth']], function () {   
    Route::get('video_report', 'ReportController@video_report');
});

    

Route::get('/dashbord', 'HomeController@new');
Route::get('/profile', 'HomeController@profile');
Route::get('/mybio', 'HomeController@mybio');
Route::get('/address', 'HomeController@address');
Route::get('/contact', 'HomeController@contact');
Route::get('/license', 'HomeController@license');
Route::get('/password', 'HomeController@password');
Route::get('/profilepic', 'HomeController@profilepic');
Route::get('/consults', 'HomeController@consults');
Route::get('/patients_queue', 'HomeController@patients_queue');
Route::get('/tostart', 'HomeController@tostart')->name('tostart');
Route::get('/tocomplete', 'HomeController@tocomplete');
Route::get('/cancel', 'HomeController@cancel')->name('cancel');
Route::get('/reschedule', 'HomeController@reschedule')->name('reschedule');
Route::get('/patients_profile/{id}/{b_id}', 'HomeController@patients_profile')->name('patients_profile');
Route::get('/patients_complaint/{id}/{b_id}', 'HomeController@patients_complaint')->name('patients_complaint');
Route::get('/wallet', 'HomeController@wallet');
Route::get('/current_balance', 'HomeController@current_balance');
Route::get('/add_fund', 'HomeController@add_fund');
Route::get('/refund_request', 'HomeController@refund_request');
Route::get('/set_prices', 'HomeController@set_prices');
Route::get('/banking_info', 'HomeController@banking_info');
Route::get('/account_statement', 'HomeController@account_statement');
Route::get('/records', 'HomeController@records');
Route::get('/myconsults_records', 'HomeController@myconsults_records');
Route::get('/myconsults_records2', 'HomeController@myconsults_records2');
Route::get('/myvisit_summaries', 'HomeController@myvisit_summaries');
Route::get('/myvisit_summaries2', 'HomeController@myvisit_summaries2');
Route::get('/mybookings', 'HomeController@mybookings');
Route::get('/mybookings2', 'HomeController@mybookings2');
Route::get('/my_prescriptions', 'HomeController@my_prescriptions');
Route::get('/my_prescriptions2', 'HomeController@my_prescriptions2');
Route::get('/reviews_menu', 'HomeController@reviews_menu');
Route::get('/my_reviews_summary', 'HomeController@my_reviews_summary');
Route::get('/my_reviews', 'HomeController@reviews');
Route::get('/score_card', 'HomeController@score_card');
Route::get('/my_responce', 'HomeController@responce');
Route::get('/settings_menu', 'HomeController@settings_menu');
Route::get('/tiemslots', 'HomeController@tiemslots');
Route::get('/support', 'HomeController@support');
Route::post('/tiemslots_update/{id}', 'TimeSlotController@update')->name('tiemslots_update.update');
Route::post('/account_statement_view', 'HomeController@account_statement_view')->name('account_statement_view');

Route::post('/myconsults_records2', 'HomeController@myconsults_records2')->name('myconsults_records2');
Route::post('/myvisit_summaries2', 'HomeController@myvisit_summaries2')->name('myvisit_summaries2');
Route::post('/my_prescriptions2', 'HomeController@my_prescriptions2')->name('my_prescriptions2');

Route::get('/setting_doctor_availability', 'HomeController@setting_doctor_availability');


Route::post('/mybio_update/{id}', 'DoctorsController@update')->name('doctorsBio.update');

Route::post('/dr_profilepic_remove', 'HomeController@dr_profilepic_remove')->name('dr_profilepic_remove.data');

Route::post('/dt_doctor_availability', 'Doctor_availabilityController@store')->name('dt_doctor_availability.store');
Route::post('/doctor_add_fund', 'Patient_add_fundController@store')->name('doctor_add_fund.store');
Route::post('/doctor_add_support', 'SupportController@store')->name('doctor_add_support.store');
Route::post('/doctor_withdraw', 'Doctor_withdrawController@store')->name('doctor_withdraw.store');

Route::post('/appoiment_done', 'HomeController@appoiment_done')->name('appoiment_done.update');

Route::post('/register', 'DoctorsController@store')->name('doctorsreg.update');

Route::post('/change_password', 'DoctorsController@change_password')->name('change_password.change_password');

Route::post('/key_points', 'Key_points_patientController@store')->name('key_points.store');
Route::post('/present_medications', 'Present_medications_patientController@store')->name('present_medications.store');
Route::post('/examination_notes', 'Examination_notes_patientController@store')->name('examination_notes.store');
Route::post('/available_lab', 'Available_lab_patientController@store')->name('available_lab.store');
Route::post('/perceived_patient', 'Perceived_patientController@store')->name('perceived_patient.store');
Route::post('/suggested_investigations', 'Suggested_investigations_patientController@store')->name('suggested_investigations.store');
Route::post('/suggested_imagings', 'Suggested_imagings_patientController@store')->name('suggested_imagings.store');
Route::post('/suggested_specialists', 'Suggested_specialists_patientController@store')->name('suggested_specialists.store');
Route::post('/suggested_therapies', 'Suggested_therapies_patientController@store')->name('suggested_therapies.store');
Route::post('/my_note', 'My_note_patientController@store')->name('my_note.store');
Route::post('/prescription_1', 'Prescription_detailsController@store')->name('prescription_1.store');

Route::get('prescription_pdf/{booking}','Prescription_detailsController@pdf_view');

Route::group(['prefix' => '', 'middleware' => ['permission:admin|view_booking_requests','auth']], function () {

Route::resource('bookingRequests', 'BookingRequestController');
});

    Route::resource('languages', 'LanguageAPIController');
    Route::resource('specialities', 'SpecialitiesAPIController');
    Route::resource('my_favourites', 'My_favouriteAPIController');
    Route::resource('countries', 'CountryAPIController');
    Route::resource('states', 'StateAPIController');

    Route::resource('Prescription_details', 'Prescription_detailsAPIController');
    Route::resource('cities', 'CityAPIController');


    Route::resource('patient_add_funds', 'Patient_add_fundAPIController');
    Route::resource('patient_refund_requests', 'Patient_refund_requestAPIController');



Route::resource('patients', 'PatientsAPIController')->middleware('auth');



Route::resource('service_details', 'Service_detailsAPIController')->middleware('auth');

Route::resource('bank_informations', 'Bank_informationAPIController')->middleware('auth');
Route::resource('set_service_prices', 'Set_service_priceAPIController')->middleware('auth');
Route::resource('licenceTypes', 'Licence_typeController')->middleware('auth');




// New Design Start 
// Route::get('/homepage', 'HomeController@design');
Route::post('/homepagedetails', 'HomeController@homepagedetails')->name('homepagedetails');
Route::get('/dr_address', 'HomeController@dr_address');
Route::get('/dr_contact', 'HomeController@dr_contact');
Route::get('/dr_profilepic', 'HomeController@dr_profilepic');
Route::get('/dr_license', 'HomeController@dr_license');
Route::get('/dr_mybio', 'HomeController@dr_mybio');
Route::get('/dr_password', 'HomeController@dr_password');
Route::get('/dr_bankinginfo', 'HomeController@dr_bankinginfo');
Route::get('/dr_current_balance', 'HomeController@dr_current_balance');
Route::get('/dr_refund', 'HomeController@dr_refund');
Route::get('/dr_setprice', 'HomeController@dr_setprice');
Route::get('/dr_patients_queue', 'HomeController@dr_patients_queue');
Route::get('/dr_tostart', 'HomeController@dr_tostart');
Route::get('/dr_tocomplete', 'HomeController@dr_tocomplete');
Route::get('/dr_reschedule_booking', 'HomeController@dr_reschedule_booking');
Route::get('/dr_cancle_refund_booking', 'HomeController@dr_cancle_refund_booking');

Route::get('/dr_patients_profile/{id}/{b_id}', 'HomeController@dr_patients_profile')->name('dr_patients_profile');
Route::get('/dr_score_card', 'HomeController@dr_score_card');
Route::get('/dr_statement', 'HomeController@dr_statement');
Route::post('/account_statement_view', 'HomeController@dr_account_statement_view')->name('dr_account_statement_view');
Route::get('/dr_add_fund', 'HomeController@dr_add_fund');
Route::get('/dr_review_summery', 'HomeController@dr_review_summery');
Route::get('/dr_myreview', 'HomeController@dr_myreview');
Route::get('/dr_myresponce', 'HomeController@dr_myresponce');
Route::get('/dr_my_consults_records', 'HomeController@dr_my_consults_records');
Route::get('/dr_my_consults_records2', 'HomeController@dr_my_consults_records2');
Route::post('/dr_my_consults_records2', 'HomeController@dr_my_consults_records2')->name('dr_my_consults_records2');
Route::get('/dr_my_visit_summaries', 'HomeController@dr_my_visit_summaries');
Route::get('/dr_my_visit_summaries2', 'HomeController@dr_my_visit_summaries2');
Route::post('/dr_my_visit_summaries2', 'HomeController@dr_my_visit_summaries2')->name('dr_my_visit_summaries2');
Route::get('/dr_my_prescriptions', 'HomeController@dr_my_prescriptions');
Route::get('/dr_my_prescriptions2', 'HomeController@dr_my_prescriptions2');
Route::post('/dr_my_prescriptions2', 'HomeController@dr_my_prescriptions2')->name('dr_my_prescriptions2');

Route::get('/dr_setting', 'HomeController@dr_setting');
Route::post('/social/{id}', 'DoctorsController@update')->name('social.update');
Route::post('/social', 'DoctorsController@store')->name('social.store');
Route::get('/homepage', 'HomeController@homepagedemo');
Route::get('/today', 'HomeController@homepagetodaybooking');
Route::get('/shifts', 'HomeController@homepageshifts');
Route::get('/shiftsdetails/{year?}/{startdate?}/{enddate?}', 'HomeController@homepageshiftsdetails');
Route::get('/shiftsdetailsmorning', 'HomeController@homepageshiftsdetailsmorning');
Route::get('/shiftsdetailsafternoon', 'HomeController@homepageshiftsdetailsafternoon');
Route::get('/shiftsdetailsevening', 'HomeController@homepageshiftsdetailsevening');

Route::get('/thisweek', 'HomeController@homepagethisweek');
Route::get('/thisweek_days_list', 'HomeController@homepagethisweek_day_list');
Route::get('/thisweeklist/{date?}', 'HomeController@homepagethisweeklist');
Route::get('/thismonth', 'HomeController@homepagethismonth');
Route::get('/thismonth_week_list', 'HomeController@homepagethismonth_week_list');
Route::get('/thismonth_day_list', 'HomeController@homepagethismonth_day_list');
Route::get('/week_one', 'HomeController@homepagethismonthweek_one_list');
Route::get('/week_two', 'HomeController@homepagethismonthweek_two_list');
Route::get('/week_three', 'HomeController@homepagethismonthweek_three_list');
Route::get('/week_four', 'HomeController@homepagethismonthweek_four_list');
Route::get('/week_five', 'HomeController@homepagethismonthweek_five_list');
Route::get('/thismonthlist', 'HomeController@homepagethismonthlist');
Route::get('/thisyear', 'HomeController@homepagethisyear');
Route::get('/thisyear_month_list/{test?}', 'HomeController@homepagethisyear_month_wise_list');
Route::get('/getyear', 'HomeController@home_year');
Route::get('/thisyear_week_list', 'HomeController@homepagethisyear_week_list');
Route::get('/thisyear_day_wise_list', 'HomeController@homepagethisyear_day_wise_list');
Route::get('/thisyearlist', 'HomeController@homepagethisyearlist');

/*hospital Routes start*/
Route::get('/hasptal_bio', 'HomeController@hasptal_bio');
Route::get('/h_address', 'HomeController@h_address');
Route::get('/h_password', 'HomeController@h_password');
Route::get('/h_contact', 'HomeController@h_contact');
Route::get('/h_profilepic', 'HomeController@h_profilepic');
Route::get('/h_license', 'HomeController@h_license');
Route::get('/h_current_balance', 'HomeController@h_current_balance');
Route::get('/h_add_fund', 'HomeController@h_add_fund');
Route::post('/h_add_fund_store', 'hospitalAddFundController@store')->name('h_add_fund.store');
Route::post('/h_profilepic_remove', 'HomeController@h_profilepic_remove');
Route::post('/hasptal_bio_update/{id}', 'HaspatalRegsiterController@update1')->name('hasptal.bio.update');

Route::get('/h_refund', 'HomeController@h_refund');
Route::post('/h_refund', 'hospitalWithdrawController@store')->name('h_refund.store');
Route::get('/h_statement', 'HomeController@h_statement');
Route::post('/h_account_statement_view', 'HomeController@h_account_statement_view')->name('h_account_statement_view');

Route::get('/h_bankinginfo', 'HomeController@h_bankinginfo');
/*hospital Routes end*/


Route::get('/hospital_profile', 'HomeController@hospital_profile');
Route::post('/hospital_profile/{id}', 'HaspatalRegsiterController@update1')->name('hospital_profileEdit.update');
Route::get('/hospital_consult_price', 'HomeController@hospital_consult_price')->name('hospital_consult_price');;
Route::get('/hospital_register_doctor', 'HomeController@hospital_register_doctor')->name('hospital_register_doctor');
Route::get('/hospital_specialities_create', 'HomeController@hospital_specialities_create')->name('hospital_specialities_create');
Route::get('/hospital_specialities_edit/{id}', 'HomeController@hospital_specialities_edit')->name('hospital_specialities_edit');
Route::get('/hospital_specialities_list', 'HomeController@hospital_specialities_list')->name('hospital_specialities_list');

Route::get('/hospital_doctor_list', 'HomeController@hospital_doctor_list')->name('hospital_doctor_list');

Route::get('/hospital_consult_price_create', 'HomeController@hospital_consult_price_create')->name('hospital_consult_price_create');
Route::get('/hospital_consult_price_edit/{id}', 'HomeController@hospital_consult_price_edit')->name('hospital_consult_price_edit');
Route::post('/my_review_answer', 'HomeController@review_answer')->name('my_review_answer.update');


Route::resource('propertyTypes', 'Property_typeController');

Route::resource('countries', 'CountryController');

Route::resource('states', 'StateController');

Route::resource('cities', 'CityController')->middleware('auth');
Route::get('cityImport', 'CityController@cityImport')->name('cityImport')->middleware('auth');
Route::post('cityImport', 'CityController@cityImportPost')->name('cityImportPost')->middleware('auth');

Route::resource('propertyDetails', 'PropertyDetailsController')->middleware('auth');

Route::resource('taxSetups', 'TaxSetupController')->middleware('auth');

Route::resource('memberDetails', 'MemberDetailsController')->middleware('auth');

Route::resource('languages', 'LanguageController')->middleware('auth');

Route::resource('specialities', 'SpecialitiesController')->middleware('auth');

Route::resource('doctors', 'DoctorsController')->middleware('auth');

Route::resource('patients', 'PatientsController')->middleware('auth');

Route::get('patientsImport', function(){
    return view('patients.patientImport');
});
Route::post('patientsImport', 'PatientsController@patientsImportPost')->name('patientsImportPost')->middleware('auth');

Route::resource('serviceDetails', 'Service_detailsController')->middleware('auth');

Route::resource('setServicePrices', 'Set_service_priceController')->middleware('auth');





Route::resource('bankInformations', 'Bank_informationController')->middleware('auth');

Route::resource('licenceTypes', 'Licence_typeController')->middleware('auth');











Route::resource('bookingRequests', 'BookingRequestController')->middleware('auth');

Route::resource('myFavourites', 'My_favouriteController')->middleware('auth');

Route::resource('doctorAvailabilities', 'Doctor_availabilityController')->middleware('auth');

Route::resource('prescriptionDetails', 'Prescription_detailsController')->middleware('auth');

Route::resource('patientAddFunds', 'Patient_add_fundController')->middleware('auth');

Route::resource('patientRefundRequests', 'Patient_refund_requestController')->middleware('auth');

Route::resource('timeSlots', 'TimeSlotController')->middleware('auth');

Route::resource('coupans', 'CoupanController')->middleware('auth');



Route::resource('keyPointsPatients', 'Key_points_patientController')->middleware('auth');

Route::resource('presentMedicationsPatients', 'Present_medications_patientController')->middleware('auth');

Route::resource('examinationNotesPatients', 'Examination_notes_patientController')->middleware('auth');

Route::resource('availableLabPatients', 'Available_lab_patientController')->middleware('auth');

Route::resource('perceivedPatients', 'Perceived_patientController')->middleware('auth');

Route::resource('suggestedInvestigationsPatients', 'Suggested_investigations_patientController');

Route::resource('suggestedImagingsPatients', 'Suggested_imagings_patientController')->middleware('auth');

Route::resource('suggestedSpecialistsPatients', 'Suggested_specialists_patientController')->middleware('auth');

Route::resource('suggestedTherapiesPatients', 'Suggested_therapies_patientController')->middleware('auth');

Route::resource('presentComplaintPatients', 'Present_complaint_patientController')->middleware('auth');

Route::resource('planMasters', 'Plan_masterController')->middleware('auth');



Route::resource('doctorWithdraws', 'Doctor_withdrawController')->middleware('auth');

Route::resource('supports', 'SupportController')->middleware('auth');

Route::resource('hospitalRegisters', 'Hospital_registerController')->middleware('auth');

Route::resource('reviews', 'ReviewController')->middleware('auth');

Route::resource('businessTypes', 'Business_typeController')->middleware('auth');

Route::resource('hPlans', 'H_planController')->middleware('auth');

Route::resource('businessRegisters', 'Business_registerController')->middleware('auth');

Route::resource('offers', 'OfferController')->middleware('auth');

Route::resource('haspatal360Registers', 'Haspatal_360_registerController')->middleware('auth');

Route::resource('bookingPriceAdmins', 'Booking_price_adminController')->middleware('auth');

Route::resource('yourCoverage360s', 'Your_coverage_360Controller')->middleware('auth');

Route::resource('createOffer360s', 'Create_offer_360Controller')->middleware('auth');

Route::resource('wallet360s', 'Wallet_360Controller')->middleware('auth');

Route::resource('wallet360Transactions', 'Wallet_360_transactionController')->middleware('auth');

Route::resource('buyPlans', 'Buy_planController')->middleware('auth');

Route::resource('order360s', 'Order_360Controller')->middleware('auth');

Route::resource('orderQueries', 'Order_queryController')->middleware('auth');

Route::resource('haspatalRegsiters', 'HaspatalRegsiterController');

Route::resource('setConsultPrices', 'Set_Consult_PricesController')->middleware('auth');

Route::resource('review360s', 'review_360Controller')->middleware('auth');

Route::resource('taxes', 'TaxController');

Route::resource('buyPlanDrs', 'Buy_plan_drController');

Route::resource('medicines', 'MedicineController');

Route::resource('labTests', 'LabTestController');

Route::resource('imagingLists', 'Imaging_listController');

Route::resource('therapiesLists', 'Therapies_listController');

Route::resource('counselingsLists', 'Counselings_listController');

Route::resource('homeCareLists', 'Home_care_listController');

Route::resource('referralSpecialities', 'ReferralSpecialitiesController');









Route::resource('followupPeriods', 'FollowupPeriodController');

Route::resource('hospitalAddFunds', 'hospitalAddFundController');

Route::resource('hospitalWithdraws', 'hospitalWithdrawController');

Route::resource('tasks', 'TaskController');
Route::get('taskAssign', 'TaskController@taskAssign');
Route::get('taskUpdate', 'TaskController@taskUpdate');
Route::get('operator_task', 'TaskController@operator_task')->name('operator_task');


Route::resource('h360coupans', 'H360coupanController');


//test by Santosh

//Route::get('api/validateRegistration/{email}/{mo_prefix}/{mobile_no}','AdditionalApiController@validateRegistration');