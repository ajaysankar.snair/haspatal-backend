<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
                    

        Route::get('pending_business','AdditionalApiController@get_pending_business');
                Route::get('single_pending_business','AdditionalApiController@single_pending_business');
        
        Route::post('totaldoctors','AdditionalApiController@doctor_count_registration')->middleware('auth:api');//approved
        Route::post('totalPendingdoctors','AdditionalApiController@doctor_count_pending')->middleware('auth:api');//pending
                Route::post('totalregDoctors','AdditionalApiController@doctor_count_registration')->middleware('auth:api');//total reg
                Route::post('totalRejectedDoctors','AdditionalApiController@doctor_count_rejected')->middleware('auth:api');//total rejected
                
                Route::post('doctorListToKizakuTotal','AdditionalApiController@doctor_listTotal')->middleware('auth:api');//total 
                Route::post('doctorListToKizakuToday','AdditionalApiController@doctor_listToday')->middleware('auth:api');//Today 
                Route::post('doctorListToKizakuYear','AdditionalApiController@doctor_listYear')->middleware('auth:api');//Year 
                Route::post('doctorListToKizakuWeek','AdditionalApiController@doctor_listWeek')->middleware('auth:api');//Year 
                Route::post('/changePassword','AdditionalApiController@forgot_password_otp');
                Route::post('doctorListToKizakuYesterday','AdditionalApiController@doctor_listYest')->middleware('auth:api');//Yeterday 
                Route::post('doctorListToKizakuFortnight','AdditionalApiController@doctor_listForty')->middleware('auth:api');//Fortnight 
                Route::post('doctorListToKizakuMonth','AdditionalApiController@doctor_listMonth')->middleware('auth:api');//Monthly 
                //patients API on Kizaku

                	 Route::post('totalpatients','AdditionalApiController@patient_count_registration')->middleware('auth:api');//approved
        Route::post('totalPendingpatients','AdditionalApiController@patient_count_pending')->middleware('auth:api');//pending
                Route::post('totalregDoctors','AdditionalApiController@patient_count_registration')->middleware('auth:api');//total reg
                Route::post('totalRejectedDoctors','AdditionalApiController@patient_count_rejected')->middleware('auth:api');//total rejected
                
                Route::post('patientListToKizakuTotal','AdditionalApiController@patient_listTotal')->middleware('auth:api');//total 
                Route::post('patientListToKizakuToday','AdditionalApiController@patient_listToday')->middleware('auth:api');//Today 
                Route::post('patientListToKizakuYear','AdditionalApiController@patient_listYear')->middleware('auth:api');//Year 
                Route::post('patientListToKizakuWeek','AdditionalApiController@patient_listWeek')->middleware('auth:api');//Year 
                
                Route::post('patientListToKizakuYesterday','AdditionalApiController@patient_listYest')->middleware('auth:api');//Yeterday 
                Route::post('patientListToKizakuFortnight','AdditionalApiController@patient_listForty')->middleware('auth:api');//Fortnight 
                Route::post('patientListToKizakuMonth','AdditionalApiController@patient_listMonth')->middleware('auth:api');//Monthly 
                
                           Route::post('compared_dashboard','ServiceCompareController@compare_pdashboard')->middleware('auth:api');
                //close
        //business Haspatal360 According to Centers Counting
        
        Route::post('Haspatal360ServiceCount','ServicesForKizaku@singleService_count')->middleware('auth:api');
        //list for all business registered

                     Route::post('businessListToKizakuTotal','ServicesForKizaku@business_listTotal')->middleware('auth:api');//total 
                Route::post('businessListToKizakuToday','ServicesForKizaku@business_listToday')->middleware('auth:api');//Today 
                Route::post('businessListToKizakuYear','ServicesForKizaku@business_listYear')->middleware('auth:api');//Year 
                Route::post('businessListToKizakuWeek','ServicesForKizaku@business_listWeek')->middleware('auth:api');//Year 
                
                Route::post('businessListToKizakuYesterday','ServicesForKizaku@business_listYest')->middleware('auth:api');//Yeterday 
                Route::post('businessListToKizakuFortnight','ServicesForKizaku@business_listForty')->middleware('auth:api');//Fortnight 
                Route::post('businessListToKizakuMonth','ServicesForKizaku@business_listMonth')->middleware('auth:api');//Monthly 
    Route::post('business_filter','AdditionalApiController@business_filter')->middleware('auth:api');//Monthly

    Route::post('patient_filter','AdditionalApiController@patient_filter')->middleware('auth:api');// patients count 

        Route::post('doctor_filter','AdditionalApiController@doctor_filter')->middleware('auth:api');//doctors count 
        
        
        //KIZAKU Linked API for Discard the Business

Route::post('discard2business','AdditionalApiController@business_h360_support');
Route::get('discard_list','AdditionalApiController@discarded_list');
        
Route::post('kizakudoctorList','AdditionalApiController@doctor_list')->middleware('auth:api');
    Route::post('state_list','AdditionalApiController@states');
    Route::post('district_list','AdditionalApiController@districts');
    Route::post('city_list','AdditionalApiController@cities');
    Route::post('pincode_list','AdditionalApiController@pincodes');

//business list
    Route::post('/get_state_business','AjaxController@get_state_business');
Route::post('/get_district_business','AjaxController@get_district_business');
Route::post('/get_city_business','AjaxController@get_city_business');

Route::get('viewbusiness','AdditionalApiController@viewbusiness');
Route::post('business_profiles', 'AdditionalApiController@business_profiles');
Route::get('get_service_types','AdditionalApiController@serviceType');
Route::post('saveCoupan','AdditionalApiController@saveCoupan');
Route::post('couponcode','AdditionalApiController@couponcode');
Route::post('couponredeem','AdditionalApiController@coupon_redeem');
Route::post('wallet_balance_360', 'API\Buy_planAPIController@wallet_balance_360');
Route::post('get_state_list','AdditionalApiController@getsate');



//business list according to Location on kizaku

 Route::get('/userByState','hospital_doctors@statekizaku');
 Route::get('/userByDistrict','hospital_doctors@districtkizaku');
 Route::get('/userByCity','hospital_doctors@citykizaku');

Route::post('add_country','AdditionalApiController@insert');
Route::get('country_list','AdditionalApiController@country_list');
Route::post('destroy_country','AdditionalApiController@destroy_country');
Route::post('update_country','AdditionalApiController@update_country');

Route::post('add_state','AdditionalApiController@add_state');
Route::post('destroy_state','AdditionalApiController@destroy_state');
Route::post('update_state','AdditionalApiController@update_state');


Route::post('add_district','AdditionalApiController@add_district');
Route::post('destroy_district','AdditionalApiController@destroy_district');
Route::post('update_district','AdditionalApiController@update_district');


Route::post('add_city','AdditionalApiController@add_city');
Route::post('destroy_city','AdditionalApiController@destroy_city');
Route::post('update_city','AdditionalApiController@update_city');


Route::post('add_pincode','AdditionalApiController@add_pincode');
Route::post('destroy_pincode','AdditionalApiController@destroy_pincode');
Route::post('update_pincode','AdditionalApiController@update_pincode');
Route::post('get_pincode','AdditionalApiController@get_pincode');
Route::post('new_register','AdditionalApiController@newreg');

/**
*
* @author Santosh Rauniyar's api start
*
*/
Route::post('new_register2','AdditionalApiController@newreg2');
Route::post('new_register3','AdditionalApiController@newreg3');
Route::post('set_pincode','AdditionalApiController@set_pincode');
Route::post('update_app','AdditionalApiController@update_app');
Route::post('patient_login','API\AuthController1@patient_login');
Route::get('view_users','AdditionalApiController@show');
Route::get('user_by_role','AdditionalApiController@count_users_by_roleID');
Route::get('user_approved','AdditionalApiController@count_users_approved');
Route::get('change_dashboard','AdditionalApiController@hdashboard_change');
Route::post('compare_dashboard','AdditionalApiController@compare_dashboard');
Route::post('view_documents','API\Business_registerAPIController@view_document');
Route::get('approved_user','AdditionalApiController@count_users_approved');
Route::post('doctors_list', 'API\DoctorsAPIController@doctors_list');
Route::post('doctors_list_speciality', 'API\DoctorsAPIController@doctors_list_speciality');
Route::post('doctorregister', 'API\DoctorsAPIController@doctorregister');

Route::post('booking_list_after_date', 'API\BookingRequestAPIController@booking_list_after_date');

Route::post('your_coupons','AdditionalApiController@your_coupons');
//Route::post('coupans_check', 'API\CoupanAPIController@coupans_check');
/**
*
* @author Santosh Rauniyar closed
*
*/
Route::get('business_by_pins','AdditionalApiController@get_business');

Route::post('get_districtlist','AdditionalApiController@get_districtlist');
Route::post('login', 'API\AuthController1@login');
Route::post('login_dr', 'API\AuthController1@login_dr');
Route::post('login_h360', 'API\AuthController1@login_h360');
Route::post('register', 'API\AuthController1@register');
Route::get('get_token/{id}', 'API\AuthController1@get_token');

Route::get('send_notifications', 'API\AuthController1@send_notifications');

Route::get('follow_users', 'API\OtherController@follow_users');
Route::get('get_followings', 'API\OtherController@get_followings');
Route::get('get_followers', 'API\OtherController@get_followers');
Route::post('patients_create', 'API\PatientsAPIController@store');
Route::post('patients_create_otp', 'API\PatientsAPIController@patients_create_otp');
Route::resource('languages', 'API\LanguageAPIController');
Route::resource('plan', 'API\Plan_masterAPIController');
Route::resource('licenceTypes', 'API\Licence_typeAPIController');
Route::resource('specialities', 'API\SpecialitiesAPIController');
Route::resource('states', 'API\StateAPIController');
Route::get('city_list/{state_id}', 'API\CityAPIController@city_list');
Route::post('doctor_create', 'API\DoctorsAPIController@store');
Route::post('doctor_create_otp', 'API\DoctorsAPIController@doctor_create_otp');
Route::post('haspatal_360_create', 'API\Haspatal_360_registerAPIController@store');
Route::post('haspatal_360_otp', 'API\Haspatal_360_registerAPIController@haspatal_360_otp');
Route::post('business_create', 'API\Business_registerAPIController@store');

Route::post('send_otp', 'API\DoctorsAPIController@send_otp');

Route::post('doctor_profile_edit/{id}', 'API\DoctorsAPIController@update');

Route::get('demo', 'API\DoctorsAPIController@demo');
Route::get('send_mail/{user}/{dr}/{boookdata}', 'API\BookingRequestAPIController@send_mail1');
Route::post('selftest', 'API\CityAPIController@selftest');
Route::post('get_questions', 'API\CityAPIController@get_questions');


Route::post('forgot_password_otp', 'API\AuthController1@forgot_password_otp');
Route::post('forgot_password', 'API\AuthController1@forgot_password');

Route::middleware('auth:api')->group(function () {
    Route::resource('patients', 'API\PatientsAPIController');
    Route::post('first_otification', 'API\PatientsAPIController@first_otification');
	Route::post('patients_update/{id}', 'API\PatientsAPIController@update');
	Route::get('all_consultant','API\ConsultantAPIController@consultant_data');

        
/****
 * 
 * All Consultant API for Kizaku
 * 
 * ****/


Route::get('consultant_filter','API\ConsultantToKizakuController@consultant_filter');
Route::get('consultantlisttotal','API\ConsultantToKizakuController@consultant_data');

Route::get('consultantlisttoday','API\ConsultantToKizakuController@consultant_dataToday');
Route::get('consultantlistyesterday','API\ConsultantToKizakuController@consultant_dataYesterday');
Route::get('consultantlistweek','API\ConsultantToKizakuController@consultant_dataWeek');
Route::get('consultantlistyear','API\ConsultantToKizakuController@consultant_dataYear');
Route::get('consultantlistmonth','API\ConsultantToKizakuController@consultant_dataMonth');
Route::get('consultantlistfortnight','API\ConsultantToKizakuController@consultant_dataFortnight');



	//Route::post('doctors_list', 'API\DoctorsAPIController@doctors_list');
	Route::post('doctors_search', 'API\DoctorsAPIController@doctors_search');
	Route::post('doctors_search_firstname', 'API\DoctorsAPIController@doctors_search_firstname');
	Route::post('doctors_search_last_name', 'API\DoctorsAPIController@doctors_search_last_name');
    Route::post('change_password', 'API\AuthController1@updatePassword');
	Route::post('booking_year_search', 'API\DoctorsAPIController@booking_year_search');
	Route::post('booking_year_month_search', 'API\DoctorsAPIController@booking_year_month_search');

	Route::post('booking_create', 'API\BookingRequestAPIController@store');
	Route::post('storeEclinic', 'API\BookingRequestAPIController@storeEclinic');
	Route::post('booking_slot', 'API\BookingRequestAPIController@booking_slot');
	Route::post('payment_statement', 'API\BookingRequestAPIController@payment_statement');

	//Route::post('booking_list_after_date', 'API\BookingRequestAPIController@booking_list_after_date');
	Route::post('my_favourites_create_doctor', 'API\My_favouriteAPIController@my_favourites_create_doctor');
	Route::post('my_favourites_create_pharamacy', 'API\My_favouriteAPIController@my_favourites_create_pharamacy');
	Route::post('my_favourites_create_lab', 'API\My_favouriteAPIController@my_favourites_create_lab');
	Route::post('my_favourites_create_imagine', 'API\My_favouriteAPIController@my_favourites_create_imagine');
	Route::post('my_favourites_create_theraphy', 'API\My_favouriteAPIController@my_favourites_create_theraphy');
	Route::post('my_favourites_create_home', 'API\My_favouriteAPIController@my_favourites_create_home');
	Route::post('my_favourites_create_couns', 'API\My_favouriteAPIController@my_favourites_create_couns');

	Route::post('my_favourites_getlist', 'API\My_favouriteAPIController@my_favourites_getlist');
	Route::post('my_favourites_deletelist', 'API\My_favouriteAPIController@my_favourites_deletelist');
	Route::post('my_favourites_update_list', 'API\My_favouriteAPIController@my_favourites_update_list');
	Route::post('my_favourites_pharamcy', 'API\My_favouriteAPIController@my_favourites_pharamcy');

	Route::post('my_favourites_create', 'API\My_favouriteAPIController@store');
	Route::post('my_favourites_patient', 'API\My_favouriteAPIController@my_favourites_patient');
	Route::post('my_favourites_patient_id', 'API\My_favouriteAPIController@my_favourites_patient_id');
	Route::post('doctor_availability_list', 'API\Doctor_availabilityAPIController@doctor_availability_list');

	Route::post('time_slots_doctor', 'API\TimeSlotAPIController@time_slots_doctor');

	Route::post('prescription_details_id_data', 'API\Prescription_detailsAPIController@prescription_details_id_data');

	Route::post('patient_add_funds_create', 'API\Patient_add_fundAPIController@store');
	Route::post('patient_balance', 'API\Patient_add_fundAPIController@patient_balance');
	Route::post('patient_add_funds_data', 'API\Patient_add_fundAPIController@store');

	Route::post('patient_refund_details_create', 'API\Patient_refund_requestAPIController@store');
	Route::post('patient_refund_details_data', 'API\Patient_refund_requestAPIController@patient_refund_details_data');
	Route::post('present_complaint_create', 'API\Present_complaint_patientAPIController@store');
	Route::post('present_complaint_update/{id}', 'API\Present_complaint_patientAPIController@update');
	Route::post('present_complaint_list', 'API\Present_complaint_patientAPIController@present_complaint_list');
	Route::post('book_id_wise', 'API\My_note_patientAPIController@book_id_wise');
	Route::post('coupans_check', 'API\CoupanAPIController@coupans_check');
	Route::post('coupans_check_dr', 'API\CoupanAPIController@coupans_check_dr');
	Route::post('coupans_list', 'API\CoupanAPIController@coupans_list');
	Route::post('booking_waiting', 'API\BookingRequestAPIController@booking_waiting');
	Route::post('booking_cancel', 'API\BookingRequestAPIController@booking_cancel');
	Route::post('booking_pending', 'API\BookingRequestAPIController@booking_pending');
	Route::post('booking_slot_update', 'API\BookingRequestAPIController@booking_slot_update');
	Route::post('bookingschedule', 'API\BookingRequestAPIController@bookingschedule');
	/*Doctor Api*/
 	Route::post('doctor_profile/{id}', 'API\DoctorsAPIController@update');
 	Route::post('set_price/{id}', 'API\DoctorsAPIController@update');
 	Route::post('bank_info/{id}', 'API\DoctorsAPIController@update');
 	Route::post('haspatal_schedule', 'API\BookingRequestAPIController@haspatal_schedule');
	Route::post('withdraws', 'API\Doctor_withdrawAPIController@store');
 	Route::post('myconsults_list', 'API\DoctorsAPIController@myconsults_list');
 	Route::post('imagings_list', 'API\DoctorsAPIController@imagings_list');
	Route::post('check_liciendate', 'API\DoctorsAPIController@check_liciendate');
 	Route::post('myvisit_summaries_list', 'API\DoctorsAPIController@myvisit_summaries_list');
 	Route::post('my_prescriptions_list', 'API\DoctorsAPIController@my_prescriptions_list');
 	Route::post('patients_profile', 'API\DoctorsAPIController@patients_profile');
 	Route::post('refer_list', 'API\DoctorsAPIController@refer_list');
 	Route::post('imagings_create', 'API\Suggested_imagings_patientAPIController@store');
 	Route::post('refer_create', 'API\Suggested_therapies_patientAPIController@store');
 	Route::post('treatment_create', 'API\Key_points_patientAPIController@store');
 	Route::post('treatment_list', 'API\DoctorsAPIController@treatment_list');
 	Route::post('appoiment_done', 'API\DoctorsAPIController@appoiment_done');
 	Route::post('social_link/{id}', 'API\DoctorsAPIController@update');

 	Route::post('complaint_list', 'API\DoctorsAPIController@complaint_list');
 	Route::post('medications_list', 'API\DoctorsAPIController@medications_list');
 	Route::post('prescriptionlist', 'API\DoctorsAPIController@prescriptionlist');
 	Route::post('my_note_list', 'API\DoctorsAPIController@my_note_list');
 	Route::post('lab_list', 'API\DoctorsAPIController@lab_list');
 	Route::post('medications_create', 'API\Present_medications_patientAPIController@store');
 	Route::post('lab_create', 'API\Available_lab_patientAPIController@store');
 	Route::post('review_create', 'API\ReviewAPIController@store');

	 Route::post('review_list_patient', 'API\ReviewAPIController@review_list_patient');
	 Route::post('review_list_doctor', 'API\ReviewAPIController@review_list_doctor');
	 Route::post('review_doctor_update_list', 'API\ReviewAPIController@review_doctor_update_list');
	 
 	Route::post('review_create_hospotal', 'API\ReviewAPIController@hasptal_review');
 	Route::post('review_list_hospital', 'API\ReviewAPIController@review_list');
 	Route::post('booking_id_dropdown', 'API\DoctorsAPIController@booking_id_dropdown');
 	Route::post('review_list', 'API\DoctorsAPIController@review_list');
 	Route::post('review_list_doctor', 'API\DoctorsAPIController@review_list_doctor');
 	Route::post('review_answer', 'API\DoctorsAPIController@review_answer');
 	Route::post('mynote_create', 'API\My_note_patientAPIController@store');
 	Route::post('prescription_create', 'API\Prescription_detailsAPIController@store');
 	Route::post('prescription_create1', 'API\Prescription_detailsAPIController@store');
 	Route::post('dr_cancle_refund_booking', 'API\DoctorsAPIController@dr_cancle_refund_booking');
 	Route::post('allergies_create', 'API\Suggested_investigations_patientAPIController@store');
 	Route::post('allergies_list', 'API\DoctorsAPIController@allergies_list');
 	Route::post('hpi_create', 'API\Perceived_patientAPIController@store');
 	Route::post('hpi_list', 'API\DoctorsAPIController@hpi_list');
	Route::post('drlab_create', 'API\Suggested_specialists_patientAPIController@store');
 	Route::post('drlab_list', 'API\DoctorsAPIController@drlab_list');
 	Route::post('other_create', 'API\Examination_notes_patientAPIController@store');
 	Route::post('other_list', 'API\DoctorsAPIController@other_list');
 	Route::post('consult_create', 'API\ConsultantAPIController@store');
 	Route::post('consult_list', 'API\DoctorsAPIController@consult_list');
 	Route::post('doctor_availability_create', 'API\Doctor_availabilityAPIController@store');
 	Route::post('doctor_availability_delete', 'API\Doctor_availabilityAPIController@doctor_availability_delete');
 	Route::post('doctor_availability_dete', 'API\Doctor_availabilityAPIController@doctor_availability_dete');

 	Route::post('time_slots_create', 'API\TimeSlotAPIController@store');
 	Route::post('time_slots_list', 'API\DoctorsAPIController@time_slots_list');
 	
 	Route::get('hospital_doctor_list/{hospitalId}', 'API\HaspatalRegsiterAPIController@hospital_doctor_list');
 	
 	Route::post('business_registers_create', 'API\Business_registerAPIController@store');
 	Route::post('business_view', 'API\Business_registerAPIController@business_view');
 	Route::post('business_profile', 'API\Business_registerAPIController@business_profile');
 	
 	Route::post('coupon_list', 'API\Business_registerAPIController@coupon_list');
 	Route::post('coupans_check_h360', 'API\Business_registerAPIController@coupans_check_h360');
 	Route::post('redeem_coupon', 'API\Business_registerAPIController@redeem_coupon');
 	
 	Route::post('your_coverage_create', 'API\Your_coverage_360APIController@store');
 	Route::post('create_offer_360_add', 'API\Create_offer_360APIController@store');
 	Route::post('Offer_view_data', 'API\Create_offer_360APIController@Offer_view');
 	Route::post('coverage_view', 'API\Your_coverage_360APIController@coverage_view');
 	Route::post('wallet_360_create', 'API\Wallet_360APIController@store');
 	Route::post('wallet_360_cut', 'API\Wallet_360_transactionAPIController@store');
 	Route::post('buy_plans_360', 'API\Buy_planAPIController@store');
 	//Route::post('wallet_balance_360', 'API\Buy_planAPIController@wallet_balance_360');
 	Route::post('active_plan', 'API\Buy_planAPIController@active_plan');
 	Route::post('statement_360', 'API\Buy_planAPIController@statement_360');
	Route::post('business_view_360', 'API\Business_registerAPIController@business_view_360');
	 

	Route::post('order_360_create', 'API\Order_360APIController@store');
	Route::post('order_view', 'API\Order_360APIController@order_view');
	Route::post('dispatchedorder', 'API\Order_360APIController@dispatchedorder');


	Route::post('complate_order_update', 'API\Order_360APIController@complate_order_update');
	Route::post('today_order_list', 'API\Order_360APIController@today_order_list');
	Route::post('complate_order_list', 'API\Order_360APIController@complate_order_list');
	Route::post('order_queries_create', 'API\Order_queryAPIController@store');
	Route::post('order_360_list', 'API\Order_360APIController@order_360_list');
	Route::post('review_360_create', 'API\review_360APIController@store');
	Route::post('pharmacy_city_list', 'API\review_360APIController@pharmacy_city_list');
 	
Route::post('count_for_salesOfficer','AdditionalApiController@count_for_salesOfficer');
	Route::post('order_360_list_by_prients', 'API\Order_360APIController@order_360_list_by_prients');
	Route::post('cancel_order_update', 'API\Order_360APIController@cancel_order_update');
	Route::post('share_prescription', 'API\Order_360APIController@share_prescription');
	Route::post('sharehaspatalpres', 'API\Order_360APIController@sharehaspatalpres');
 	
	Route::post('haspatal_list', 'API\HaspatalRegsiterAPIController@haspatalSearch');



 	Route::get('score_card', 'API\DoctorsAPIController@score_card');
 	Route::get('statement_view', 'API\DoctorsAPIController@statement_view');
	Route::get('patient_queue', 'API\PatientsAPIController@patient_queue');
 	Route::get('doctor_profile_data/{id}', 'API\DoctorsAPIController@doctor_profile_data');
 	Route::get('licence_tyes', 'API\DoctorsAPIController@licence_tyes');
 	Route::post('current_balance', 'API\DoctorsAPIController@current_balance');
	 Route::get('waiting_room_status_list', 'API\PatientsAPIController@waiting_room_status_list');
	 Route::get('booked_room_status_list', 'API\PatientsAPIController@booked_room_status_list');
	 Route::get('checked_room_status_list', 'API\PatientsAPIController@checked_room_status_list');
	 Route::post('singlemeetinglist', 'API\PatientsAPIController@singlemeetinglist');
	 Route::get('updatebookingwait/{id}', 'API\PatientsAPIController@updatebookingwait');
	 Route::get('stopringtune/{id}', 'API\PatientsAPIController@stopringtune');
	 Route::get('patient_waiting_room_status/{id}', 'API\PatientsAPIController@patient_waiting_room_status');
	 Route::get('patient_futureappointment/{id}', 'API\PatientsAPIController@patient_futureappointment');
	 Route::get('patient_futureappointmenttime/{id}', 'API\PatientsAPIController@patient_futureappointmenttime');
 	Route::get('reschedule_appoinment', 'API\PatientsAPIController@reschedule_appoinment');
 	Route::get('cancel_refund', 'API\PatientsAPIController@cancel_refund');
 	Route::get('cancel_booking', 'API\DoctorsAPIController@cancel_booking');
 	
 	Route::get('haspatal_schedule_home', 'API\DoctorsAPIController@haspatal_schedule_home');
 	Route::get('homepage_today_booking', 'API\DoctorsAPIController@homepage_today_booking');
 	Route::get('home_this_week', 'API\DoctorsAPIController@home_this_week');
 	Route::get('home_month', 'API\DoctorsAPIController@home_month');
	Route::post('home_year', 'API\DoctorsAPIController@home_year');
 	

	Route::get('state_list/{country_id}', 'API\StateAPIController@state_list');
	
	Route::get('bookingRequest_id/{id}', 'API\BookingRequestAPIController@bookingRequest_id');
	Route::get('doctorconultant/{id}', 'API\BookingRequestAPIController@doctorconultant');
	Route::get('bookingRequest_call_update/{booking_id}', 'API\BookingRequestAPIController@update_call_status');

	Route::resource('bookingRequests', 'API\BookingRequestAPIController');
	
	Route::resource('doctors', 'API\DoctorsAPIController');
	Route::resource('my_favourites', 'API\My_favouriteAPIController');
	Route::resource('countries', 'API\CountryAPIController');
	
	Route::resource('coupans', 'API\CoupanAPIController');
	Route::resource('complaint_patients', 'API\Present_complaint_patientAPIController');
	Route::resource('Prescription_details', 'API\Prescription_detailsAPIController');
	Route::resource('cities', 'API\CityAPIController');

	Route::resource('patient_add_funds', 'API\Patient_add_fundAPIController');
	Route::resource('patient_refund_requests', 'API\Patient_refund_requestAPIController');
	Route::resource('my_note_patients', 'API\My_note_patientAPIController');
	Route::resource('business_registers', 'API\Business_registerAPIController');
	Route::resource('business_types', 'API\Business_typeAPIController');
	Route::resource('h_plans', 'API\H_planAPIController');
    Route::resource('offers', 'API\OfferAPIController');
    Route::resource('review_360s', 'API\review_360APIController');


    Route::get('taxList','API\TaxAPIController@index');
	Route::resource('taxes', 'API\TaxAPIController');

	Route::resource('medicines', 'API\MedicineAPIController');
    Route::post('medicines_update/{id}','API\MedicineAPIController@update');
    Route::post('medicines_delete/{id}','API\MedicineAPIController@destroy');


    Route::resource('lab_tests', 'API\LabTestAPIController');
    Route::post('lab_tests_update/{id}','API\LabTestAPIController@update');
    Route::post('lab_tests_delete/{id}','API\LabTestAPIController@destroy');

	Route::resource('imaging_lists', 'API\Imaging_listAPIController');
	Route::post('imaging_lists_update/{id}','API\Imaging_listAPIController@update');
    Route::post('imaging_lists_delete/{id}','API\Imaging_listAPIController@destroy');

	Route::resource('therapies_lists', 'API\Therapies_listAPIController');
	Route::post('therapies_lists_update/{id}','API\Therapies_listAPIController@update');
    Route::post('therapies_lists_delete/{id}','API\Therapies_listAPIController@destroy');

	Route::resource('counselings_lists', 'API\Counselings_listAPIController');
	Route::post('counselings_lists_update/{id}','API\Counselings_listAPIController@update');
    Route::post('counselings_lists_delete/{id}','API\Counselings_listAPIController@destroy');

	Route::resource('home_care_lists', 'API\Home_care_listAPIController');
	Route::post('home_care_lists_update/{id}','API\Home_care_listAPIController@update');
    Route::post('home_care_lists_delete/{id}','API\Home_care_listAPIController@destroy');

    Route::resource('referral_specialities', 'API\ReferralSpecialitiesAPIController');
    Route::post('referral_specialities_update/{id}','API\ReferralSpecialitiesAPIController@update');
    Route::post('referral_specialities_delete/{id}','API\ReferralSpecialitiesAPIController@destroy');

	Route::resource('followup_periods', 'API\FollowupPeriodAPIController');
	Route::post('followup_periods_update/{id}','API\FollowupPeriodAPIController@update');
    Route::post('followup_periods_delete/{id}','API\FollowupPeriodAPIController@destroy');
});


Route::resource('plan_masters', 'Plan_masterAPIController');

Route::resource('videos', 'VideoAPIController');

Route::resource('video_comments', 'Video_commentAPIController');

Route::resource('video_like_dislikes', 'Video_like_dislikeAPIController');

/*Route::resource('follow_users', 'Follow_usersAPIController');*/

Route::resource('property_types', 'Property_typeAPIController');



Route::resource('property_details', 'PropertyDetailsAPIController');

Route::resource('tax_setups', 'TaxSetupAPIController');

Route::resource('member_details', 'MemberDetailsAPIController');



/*Route::resource('doctors', 'DoctorsAPIController');*/


Route::resource('service_details', 'Service_detailsAPIController');

Route::resource('bank_informations', 'Bank_informationAPIController');
Route::resource('set_service_prices', 'Set_service_priceAPIController');

//Route::resource('Doctor_availability', 'Doctor_availabilityAPIController');


Route::resource('time_slots', 'TimeSlotAPIController');




Route::resource('key_points_patients', 'Key_points_patientAPIController');

Route::resource('present_medications_patients', 'Present_medications_patientAPIController');

Route::resource('examination_notes_patients', 'Examination_notes_patientAPIController');

Route::resource('available_lab_patients', 'Available_lab_patientAPIController');

Route::resource('perceived_patients', 'Perceived_patientAPIController');

Route::resource('suggested_investigations_patients', 'Suggested_investigations_patientAPIController');

Route::resource('suggested_imagings_patients', 'Suggested_imagings_patientAPIController');

Route::resource('suggested_specialists_patients', 'Suggested_specialists_patientAPIController');

Route::resource('suggested_therapies_patients', 'Suggested_therapies_patientAPIController');

Route::resource('present_complaint_patients', 'Present_complaint_patientAPIController');





Route::resource('doctor_withdraws', 'Doctor_withdrawAPIController');

Route::resource('supports', 'SupportAPIController');

Route::resource('hospital_registers', 'Hospital_registerAPIController');









Route::resource('booking_price_admins', 'Booking_price_adminAPIController');

Route::resource('your_coverage_360s', 'Your_coverage_360APIController');

Route::resource('create_offer_360s', 'Create_offer_360APIController');

Route::resource('wallet_360s', 'Wallet_360APIController');

Route::resource('wallet_360_transactions', 'Wallet_360_transactionAPIController');

Route::resource('buy_plans', 'Buy_planAPIController');

Route::resource('order_360s', 'Order_360APIController');

Route::resource('order_queries', 'Order_queryAPIController');

Route::resource('haspatal_regsiters', 'HaspatalRegsiterAPIController');











Route::resource('hospital_add_funds', 'hospitalAddFundAPIController');

Route::resource('hospital_withdraws', 'hospitalWithdrawAPIController');

Route::resource('tasks', 'TaskAPIController');
Route::group([

	
'middleware' => ['api', 'cors']

	
	], function ($router)
	{
	
		Route::get('get_districts','AdditionalApiController@get_districtlist');
		Route::post('haspatal360register', 'RegistrationController@store');
		Route::post('city', 'RegistrationController@city_list');
		Route::get('state','AdditionalApiController@getsate');
		Route::post('iprescription_details_id_data', 'API\Prescription_detailsAPIController@prescription_details_id_data');
		Route::post('haspatal_360_otp', 'RegistrationController@haspatal_360_otp');
		Route::post('getreedemCoupon', 'RegistrationController@getreedemCoupon');
		Route::post('businesregister3601','NewupdatesController@newreg');
		Route::post('saveCoupan360','AdditionalApiController@saveCoupan');
		Route::get('couponcode360','AdditionalApiController@couponcode');
		Route::post('haspatal360Coverage','haspatalupdate360@haspatal360Coverage');
		Route::post('login_h3600', 'RegistrationController@login');
		Route::post('loginAcess3600', 'RegistrationController@loginAcess3600');
		Route::post('singleDoctor','haspatalupdate360@singleDoctor');
		Route::post('haspatalType','haspatalupdate360@haspatalType');
		Route::post('acceptTerms','NewupdatesController@acceptTerms');
		Route::post('icomplate_order_list', 'API\Order_360APIController@complate_order_list');
		Route::post('ibusiness_view', 'API\Business_registerAPIController@business_view');
		Route::post('ichange_password', 'RegistrationController@updatePassword');
		Route::post('iorder_queries_create', 'API\Order_queryAPIController@store');
		Route::post('itoday_order_list', 'API\Order_360APIController@today_order_list');
		Route::post('iorder_view', 'API\Order_360APIController@order_view');
		Route::post('pharmacy_orderaccept', 'API\Order_360APIController@pharmacy_orderaccept');
		Route::post('cancel_order_pharamcyupdate', 'API\Order_360APIController@cancel_order_pharamcyupdate');
		///////////////////////////
		Route::post('medicine_cost_update', 'API\Order_360APIController@medicine_cost_update');
		Route::post('package_details__update', 'API\Order_360APIController@package_details__update');
		Route::post('Confirmpacckageupdate', 'API\Order_360APIController@Confirmpacckageupdate');
		Route::post('orderstatusupdate', 'API\Order_360APIController@orderstatusupdate');
		Route::post('dispatchorderstatusupdate', 'API\Order_360APIController@dispatchorderstatusupdate');
		Route::post('orderstatusupdate', 'API\Order_360APIController@orderstatusupdate');
		////////////////////////////////////////////////////////
		Route::post('district', 'RegistrationController@district');
	    Route::post('iwallet_360_create', 'NewupdatesController@Wstore');
	    Route::post('ibuy_plans_360', 'API\Buy_planAPIController@store');
		Route::post('icomplate_order_update', 'API\Order_360APIController@complate_order_update');
 	    Route::post('iactive_plan', 'API\Buy_planAPIController@active_plan');
		Route::post('istatement_360', 'API\Buy_planAPIController@statement_360');
		Route::post('uploadfile', 'RegistrationController@uploadfile');
		Route::post('uploadfiledoctorprofile', 'haspatalupdate360@uploadfiledoctorprofile');
		Route::post('uploadfiledoctorclinic', 'haspatalupdate360@uploadfiledoctorclinic');
		Route::post('uploadfiledocuments', 'haspatalupdate360@uploadfiledocuments');
		Route::post('patient_documents_details', 'haspatalupdate360@patient_documents_details');
		Route::post('patient_documen_doctor', 'haspatalupdate360@patient_documen_doctor');
		Route::post('patient_login_Elinic', 'Elicinc@patient_login_Elinic');
		Route::post('check_date', 'Elicinc@check_date');
		Route::post('uploadfilewallpaper', 'haspatalupdate360@uploadfilewallpaper');
		Route::post('uploadfilelicense', 'haspatalupdate360@uploadfilelicense');
		Route::post('iwallet_balance_360', 'NewupdatesController@wallet_balance_360');
		Route::post('uploadfileshop_pic', 'haspatalupdate360@uploadfileshop_pic');
		Route::post('uploadfileb_lic', 'haspatalupdate360@uploadfileb_lic');
		Route::post('uploadfileb_card_pic', 'haspatalupdate360@uploadfileb_card_pic');
		Route::post('uploadfilebillCopy', 'haspatalupdate360@uploadfilebillCopy');
		Route::post('profile360','NewupdatesController@profile360');
		Route::post('createOffer','NewupdatesController@createOffer');
		Route::post('updationAvailable','haspatalupdate360@updationAvailable');
		Route::post('updationAvailableDoctor','haspatalupdate360@updationAvailableDoctor');
		Route::post('otpverification','haspatalupdate360@otpverification');
		Route::post('pharmacysearch','haspatalupdate360@pharmacysearch');
		Route::post('singleBusiness','haspatalupdate360@singleBusiness');
		Route::post('pharmacycodesearch','findlocation@pharmacycodesearch');
		Route::post('pharmacylocationSearch','findlocation@pharmacylocationSearch');
		Route::post('patient_add_fund', 'API\Patient_add_fundAPIController@store');
		Route::post('patient_balanceWallet', 'API\Patient_add_fundAPIController@patient_balance');
	    Route::post('sendmail','RegistrationController@HaspatalnewOTP');
		Route::post('hasptalverifiedOTP','RegistrationController@hasptalverifiedOTP');
		Route::post('allorder', 'API\Order_360APIController@allorder');
		Route::post('zoomtoken', 'ZoomController@zoomtoken');
		Route::post('zoomMeeting', 'ZoomController@zoommeeting');
		Route::post('joinMeeting', 'ZoomController@joinMeeting');
		Route::post('updatemeeting', 'ZoomController@updatemeeting');
		Route::post('callended_status', 'ZoomController@callended_status');
		Route::post('enablex', 'ZoomController@enablex');
		Route::post('enablextoken', 'ZoomController@enablextoken');
		Route::post('sendFCM', 'ZoomController@sendFCM');
		Route::post('updateToken', 'NewupdatesController@updateToken');
		Route::post('Prescription_detailsList', 'API\Prescription_detailsAPIController@Prescription_detailsList');
		Route::post('Prescription_SingleList', 'API\Prescription_detailsAPIController@Prescription_SingleList');
		Route::post('patients_updates1/{id}', 'API\PatientsAPIController@update');	
		Route::post('promcodeverify','haspatalupdate360@promcodeverify');
	});


Route::get('validateRegistration/{email}/{mo_prefix}/{mobile_no}','AdditionalApiController@validateRegistration');
	Route::get('view_users','AdditionalApiController@show');
//Care haspatal API FOR KIZAKU CONNECTION

    Route::post('khaspatalusers','Haspatal2Kizaku@all');
Route::post('single_hospital','Haspatal2Kizaku@single_haspatal');
Route::post('user_status','HaspatalRegsiterController@set_user_status');



Route::post('/is_business_exist','API\Business_registerAPIController@is_business_exist');
Route::post('/save_business','API\Business_registerAPIController@save_business');