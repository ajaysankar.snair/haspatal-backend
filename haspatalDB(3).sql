-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jan 21, 2021 at 11:29 AM
-- Server version: 5.6.49-cll-lve
-- PHP Version: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `haspatalDB`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_menu`
--

CREATE TABLE `admin_menu` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `order` int(11) NOT NULL DEFAULT '0',
  `title` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `uri` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permission` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `admin_operation_log`
--

CREATE TABLE `admin_operation_log` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `method` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `input` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `admin_permissions`
--

CREATE TABLE `admin_permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `http_method` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `http_path` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `admin_roles`
--

CREATE TABLE `admin_roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `admin_role_menu`
--

CREATE TABLE `admin_role_menu` (
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `admin_role_permissions`
--

CREATE TABLE `admin_role_permissions` (
  `role_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `admin_role_users`
--

CREATE TABLE `admin_role_users` (
  `role_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `admin_users`
--

CREATE TABLE `admin_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `admin_user_permissions`
--

CREATE TABLE `admin_user_permissions` (
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Available_lab_patient`
--

CREATE TABLE `Available_lab_patient` (
  `id` int(10) UNSIGNED NOT NULL,
  `a_patient_id` int(11) NOT NULL,
  `a_doctor_id` int(11) NOT NULL,
  `a_book_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `available_lab` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `Available_lab_patient`
--

INSERT INTO `Available_lab_patient` (`id`, `a_patient_id`, `a_doctor_id`, `a_book_id`, `available_lab`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 19, 1, 'book-0000047', 'test data', 35, 35, '2020-05-15 08:03:18', '2020-05-15 08:03:18', NULL),
(2, 19, 1, 'book-000006', 'test lab1', 35, 35, '2020-05-15 08:26:44', '2020-05-15 08:26:53', NULL),
(3, 19, 1, 'book-0000011', 'lab', 35, 35, '2020-05-16 12:39:48', '2020-05-16 12:39:48', NULL),
(4, 221, 136, '91-B-000000114', 'blood glucose', 300, 300, '2020-10-20 12:10:19', '2020-10-20 12:10:19', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `bank_information`
--

CREATE TABLE `bank_information` (
  `id` int(10) UNSIGNED NOT NULL,
  `bank_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `account_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ifsc_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `account_no` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cancle_cheque` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '1' COMMENT '0 for inactive 1 for active',
  `created_by` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `updated_by` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bank_information`
--

INSERT INTO `bank_information` (`id`, `bank_name`, `account_name`, `ifsc_code`, `account_no`, `cancle_cheque`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'iciciq', 'dhaval', '15222', '0255655', '1583911045__IMG-20191214-WA0001.jpg', 1, '1', '1', '2020-04-08 13:19:00', '2020-04-08 13:19:00', NULL),
(2, 'SBI', 'SBI Acc', 'SBIN000006', '1259656655666', 'index.png', 1, '35', '35', '2020-04-13 05:38:37', '2020-04-13 05:38:37', NULL),
(3, 'ICICI', 'ICICI Acc', 'ICICI2200', '1256666689', 'index.png', 1, '1', '1', '2020-04-13 05:41:23', '2020-04-13 05:41:23', NULL),
(4, 'MMC', 'abcd1234567890', 'abcd00001', '1234567890', 'I2CNode.png', 1, '41', '41', '2020-04-13 07:18:34', '2020-04-13 07:18:34', NULL),
(5, 'sbi', 'pooja kumari', 'dfer5432', '23432435422242', '4.png', 1, '1', '1', '2020-06-08 09:35:56', '2020-06-08 09:36:21', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `booking_price_admin`
--

CREATE TABLE `booking_price_admin` (
  `id` int(10) UNSIGNED NOT NULL,
  `book_price` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `booking_price_admin`
--

INSERT INTO `booking_price_admin` (`id`, `book_price`, `description`, `created_by`, `updated_by`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '0', 'Admin Price', 1, 1, 0, '2020-06-02 11:59:31', '2020-08-24 16:23:26', '2020-08-24 16:23:26'),
(3, '6000', 'dgfvda', 1, 1, 0, '2020-06-22 07:46:31', '2020-08-24 16:19:48', '2020-08-24 16:19:48'),
(4, '0', 'Admin_Price', 1, 1, 0, '2020-08-24 16:28:44', '2020-08-24 16:40:38', '2020-08-24 16:40:38'),
(5, '40', 'demo', 1, 1, 0, '2020-08-24 17:16:54', '2020-08-24 18:47:51', NULL),
(6, '10', 'test', 1, 1, 0, '2020-08-24 17:23:02', '2020-08-24 17:23:02', NULL),
(7, '1', 'test', 1, 1, 0, '2020-08-24 17:23:25', '2020-09-20 23:09:34', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `booking_request`
--

CREATE TABLE `booking_request` (
  `id` int(10) UNSIGNED NOT NULL,
  `book_id` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `patient_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `doctor_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_time` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `waiting_time` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_status` smallint(6) NOT NULL DEFAULT '0' COMMENT '0 for inpaid, 1 for paid',
  `promo_code` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dr_price` int(11) DEFAULT NULL,
  `discount` int(11) DEFAULT NULL,
  `f_amount` int(11) DEFAULT NULL,
  `status` smallint(6) NOT NULL DEFAULT '1' COMMENT '1 for pending, 2 for waiting ,3 for completed,4 for rescheduled, 5 for cancel',
  `patient_status` tinyint(4) DEFAULT NULL COMMENT '1 for patient cnacel and 2 for doctor cancel',
  `consult_mode` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'offline',
  `w_status` int(11) DEFAULT NULL COMMENT '1 for wallet cut',
  `w_amount` int(255) DEFAULT NULL COMMENT 'fund to cut amount',
  `slot_status` int(11) DEFAULT NULL COMMENT '1 for Morning , 2 for Afternoon, 3 for Evening',
  `call_status` tinyint(4) NOT NULL DEFAULT '0',
  `autorefund_status` tinyint(4) NOT NULL DEFAULT '0',
  `created_by` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `updated_by` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `booking_request`
--

INSERT INTO `booking_request` (`id`, `book_id`, `patient_name`, `doctor_name`, `date_time`, `waiting_time`, `payment_id`, `payment_status`, `promo_code`, `dr_price`, `discount`, `f_amount`, `status`, `patient_status`, `consult_mode`, `w_status`, `w_amount`, `slot_status`, `call_status`, `autorefund_status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(27, 'book-000001', '19', '38', '2020-06-22 22:30', NULL, NULL, 0, NULL, 78, NULL, 78, 6, 3, 'offline', 1, 14422, 3, 0, 1, '19', '19', '2020-06-22 09:09:22', '2020-06-22 09:09:22'),
(28, 'book-000002', '19', '1', '2020-05-23 11:00', '20', NULL, 0, NULL, 50, NULL, 50, 6, 3, 'offline', 1, 14372, 1, 0, 1, '19', '19', '2020-06-23 05:08:31', '2020-06-23 05:08:31'),
(29, 'book-000003', '19', '1', '2020-06-24 12:30', '0', NULL, 0, NULL, 50, NULL, 50, 6, 3, 'offline', 1, 14322, 1, 0, 1, '19', '19', '2020-06-24 06:59:21', '2020-06-24 06:59:21'),
(30, 'book-000004', '19', '1', '2020-06-17 13:00', '29', NULL, 0, NULL, 50, NULL, 50, 6, 3, 'offline', 1, 14272, 2, 0, 1, '19', '19', '2020-06-24 07:00:26', '2020-06-24 07:00:26'),
(31, 'book-000005', '19', '1', '2020-06-24 18:00', NULL, NULL, 0, NULL, 50, NULL, 50, 6, 3, 'offline', 1, 14222, 3, 0, 1, '19', '19', '2020-06-24 07:01:15', '2020-06-24 07:01:15'),
(32, 'book-000006', '19', '1', '2020-06-27 11:00', NULL, 'pay_F6l9mhGNU7Jvja', 0, NULL, 50, NULL, 50, 4, NULL, 'offline', 2, NULL, NULL, 0, 0, '19', '19', '2020-06-25 06:33:36', '2020-06-25 06:33:36'),
(33, 'book-000007', '19', '1', '2020-06-28 12:30', NULL, NULL, 0, NULL, 50, NULL, 50, 6, 3, 'offline', 1, 14172, 1, 0, 1, '19', '19', '2020-06-25 10:37:07', '2020-06-25 10:37:07'),
(34, 'book-000008', '19', '1', '2020-06-25 16:30', NULL, NULL, 0, NULL, 50, NULL, 50, 6, 3, 'offline', 1, 14122, 2, 0, 1, '19', '19', '2020-06-25 10:58:57', '2020-06-25 10:58:57'),
(35, 'book-000009', '19', '39', '2020-07-06 23:30', NULL, NULL, 0, NULL, 2, NULL, 2, 5, NULL, 'offline', 1, 14120, 1, 0, 0, '19', '19', '2020-07-05 18:46:34', '2020-07-05 18:46:34'),
(36, 'book-0000010', '19', '1', '2020-07-07 11:00', NULL, NULL, 0, NULL, 50, NULL, 50, 5, NULL, 'offline', 1, 14070, 1, 0, 0, '19', '19', '2020-07-05 18:48:53', '2020-07-05 18:48:53'),
(37, 'book-0000011', '19', '39', '2020-07-06 09:30', NULL, NULL, 0, NULL, 2, NULL, 2, 6, 3, 'offline', 1, 14068, 1, 0, 1, '19', '19', '2020-07-05 18:51:46', '2020-07-05 18:51:46'),
(38, 'book-0000012', '19', '39', '2020-07-07 03:01', NULL, NULL, 0, NULL, 2, NULL, 2, 5, NULL, 'offline', 1, 14066, 1, 0, 0, '19', '19', '2020-07-05 18:54:36', '2020-07-05 18:54:36'),
(39, 'book-0000013', '19', '39', '2020-07-10 07:01', NULL, NULL, 0, NULL, 2, NULL, 2, 4, NULL, 'offline', 1, 14064, NULL, 0, 0, '19', '19', '2020-07-05 18:54:38', '2020-07-05 18:54:38'),
(40, 'book-0000014', '19', '39', '2020-07-06 01:31', NULL, NULL, 0, NULL, 2, NULL, 2, 6, 3, 'offline', 1, 14062, 1, 0, 1, '19', '19', '2020-07-05 20:18:24', '2020-07-05 20:18:24'),
(41, 'book-0000015', '19', '39', '2020-07-06 09:01', NULL, NULL, 0, NULL, 2, NULL, 2, 6, 3, 'offline', 1, 14060, 1, 0, 1, '19', '19', '2020-07-05 20:24:06', '2020-07-05 20:24:06'),
(42, 'book-0000016', '62', '1', '2020-07-17 18:00', NULL, 'pay_FFWRVGHt3PVeDK', 0, NULL, 50, NULL, 50, 6, 3, 'offline', 2, NULL, 2, 0, 1, '62', '62', '2020-07-17 17:00:57', '2020-07-17 17:00:57'),
(43, 'book-0000017', '19', '1', '2020-07-29 15:30', '1', NULL, 0, NULL, 250, NULL, 250, 3, NULL, 'offline', 1, 13810, 2, 0, 0, '19', '19', '2020-07-29 16:51:09', '2020-07-29 17:01:38'),
(44, 'book-0000018', '56', '1', '2020-07-31 11:30', NULL, 'pay_FKzAlqJSE2wZoW', 0, NULL, 250, NULL, 250, 5, NULL, 'offline', 2, NULL, 1, 0, 0, '56', '56', '2020-07-31 12:22:31', '2020-07-31 12:22:31'),
(45, 'book-0000019', '56', '1', '2020-07-31 15:30', '25', 'pay_FL3SamT9t8nbdO', 0, NULL, 50, NULL, 50, 3, NULL, 'offline', 2, NULL, 2, 0, 0, '56', '56', '2020-07-31 16:33:52', '2020-07-31 16:38:49'),
(46, 'book-0000020', '56', '1', '2020-07-31 17:00', '42', 'pay_FL3dCX7Sc3LFDY', 0, NULL, 50, NULL, 50, 6, 3, 'offline', 2, NULL, 2, 0, 1, '56', '56', '2020-07-31 16:43:55', '2020-07-31 16:43:55'),
(47, 'book-0000021', '56', '1', '2020-08-01 15:35', '0', 'pay_FLPzTQH4gmQSrr', 0, NULL, 50, NULL, 50, 6, 3, 'offline', 2, NULL, 2, 0, 1, '56', '56', '2020-08-01 14:36:41', '2020-08-01 14:36:41'),
(48, 'book-0000022', '56', '1', '2020-08-01 15:15', '22', 'pay_FLPzuq5FseKu0s', 0, NULL, 50, NULL, 50, 3, NULL, 'offline', 2, NULL, 2, 0, 0, '56', '56', '2020-08-01 14:36:41', '2020-08-01 16:57:29'),
(49, 'book-0000023', '56', '1', '2020-08-01 16:00', '25', 'pay_FLSUBKsgiKn4GD', 0, NULL, 50, NULL, 50, 6, 3, 'offline', 2, NULL, 2, 0, 1, '56', '56', '2020-08-01 17:02:44', '2020-08-01 17:02:44'),
(50, 'book-0000024', '19', '1', '2020-08-01 17:00', '28', NULL, 0, NULL, 50, NULL, 50, 6, 3, 'offline', 1, 13760, 2, 0, 1, '19', '19', '2020-08-01 18:01:18', '2020-08-01 18:01:18'),
(51, 'book-0000025', '56', '1', '2020-08-01 18:30', NULL, 'pay_FLUn9crw9KW60I', 0, NULL, 50, NULL, 50, 4, NULL, 'offline', 2, NULL, 3, 0, 0, '56', '56', '2020-08-01 19:18:24', '2020-08-01 19:18:24'),
(52, 'book-0000026', '56', '1', '2020-08-01 18:00', NULL, 'pay_FLUnUi8oM0OR7W', 0, NULL, 50, NULL, 50, 5, NULL, 'offline', 2, NULL, 2, 0, 0, '56', '56', '2020-08-01 19:18:24', '2020-08-01 19:18:24'),
(53, 'book-0000027', '19', '1', '2020-08-04 11:30', '34', NULL, 0, NULL, 100, NULL, 100, 3, NULL, 'offline', 1, 13660, 1, 0, 0, '19', '19', '2020-08-04 12:25:43', '2020-08-04 13:14:54'),
(54, 'book-0000028', '19', '1', '2020-08-04 14:50', '41', NULL, 0, NULL, 100, NULL, 100, 6, 3, 'offline', 1, 13560, 1, 0, 1, '19', '19', '2020-08-04 12:31:57', '2020-08-04 12:31:57'),
(55, 'book-0000029', '19', '1', '2020-08-04 15:30', '41', NULL, 0, NULL, 100, NULL, 100, 6, 3, 'offline', 1, 13460, 1, 0, 1, '19', '19', '2020-08-04 13:17:58', '2020-08-04 13:17:58'),
(56, 'book-0000030', '19', '1', '2020-08-05 12:00', NULL, NULL, 0, NULL, 100, NULL, 100, 6, 3, 'offline', 1, 13360, 1, 0, 1, '19', '19', '2020-08-05 13:05:56', '2020-08-05 13:05:56'),
(57, 'book-0000031', '19', '1', '2020-08-05 12:30', NULL, 'pay_FMzUTd7mkM0nPy', 0, '123456', 100, 50, 50, 6, 3, 'offline', 2, NULL, 1, 0, 1, '19', '19', '2020-08-05 13:58:55', '2020-08-05 13:58:55'),
(58, 'book-0000032', '19', '1', '2020-08-05 12:30', NULL, 'pay_FMzVg6FVqPPzt7', 0, '123456', 100, 50, 50, 6, 3, 'offline', 2, NULL, 1, 0, 1, '19', '19', '2020-08-05 14:00:03', '2020-08-05 14:00:03'),
(59, 'book-0000033', '19', '1', '2020-08-05 12:30', NULL, NULL, 0, NULL, 100, NULL, 100, 6, 3, 'offline', 1, 13260, 1, 0, 1, '19', '19', '2020-08-05 14:01:17', '2020-08-05 14:01:17'),
(60, 'book-0000034', '19', '1', '2020-08-21 11:30', NULL, NULL, 0, NULL, 100, NULL, 100, 6, 3, 'offline', 1, 13160, 1, 0, 1, '19', '19', '2020-08-05 16:14:12', '2020-08-05 16:14:12'),
(61, 'book-0000035', '19', '1', '2020-08-14 22:58:00', NULL, NULL, 0, NULL, NULL, NULL, NULL, 6, 3, 'offline', NULL, NULL, NULL, 0, 1, '19', '19', '2020-08-05 16:21:08', '2020-08-05 16:21:08'),
(62, 'book-0000036', '19', '1', '2020-08-14 22:58:00', NULL, NULL, 0, NULL, NULL, NULL, NULL, 6, 3, 'offline', NULL, NULL, NULL, 0, 1, '19', '19', '2020-08-05 16:21:58', '2020-08-05 16:21:58'),
(63, 'book-0000037', '19', '1', '2020-08-21 11:30', NULL, NULL, 0, NULL, 100, NULL, 100, 6, 3, 'offline', 1, 13060, 1, 0, 1, '19', '19', '2020-08-05 16:34:01', '2020-08-05 16:34:01'),
(64, 'book-0000038', '19', '1', '2020-08-21 11:30', NULL, 'pay_FN2Dm3YOw7K7Ib', 0, NULL, 100, NULL, 100, 6, 3, 'offline', 2, NULL, 1, 0, 1, '19', '19', '2020-08-05 16:39:12', '2020-08-05 16:39:12'),
(65, 'book-0000039', '19', '1', '2020-08-20 12:30', NULL, NULL, 0, NULL, 100, NULL, 100, 6, 3, 'offline', 1, 12960, 1, 0, 1, '19', '19', '2020-08-05 16:45:39', '2020-08-05 16:45:39'),
(66, 'book-0000040', '19', '1', '2020-08-05 16:00', NULL, 'pay_FN2LzFNnJUMAkM', 0, NULL, 100, NULL, 100, 6, 3, 'offline', 2, NULL, 2, 0, 1, '19', '19', '2020-08-05 16:46:56', '2020-08-05 16:46:56'),
(67, 'book-0000041', '19', '1', '2020-08-05 17:30', '29', NULL, 0, '123456', 100, 50, 50, 6, 3, 'offline', 1, 12910, 2, 0, 1, '19', '19', '2020-08-05 17:53:57', '2020-08-05 17:53:57'),
(68, 'book-0000042', '19', '1', '2020-08-06 13:30', '14', NULL, 0, NULL, 50, NULL, 50, 6, 3, 'offline', 1, 12860, 1, 0, 1, '19', '19', '2020-08-06 14:45:23', '2020-08-06 14:45:23'),
(69, 'book-0000043', '19', '1', '2020-08-13 14:30', NULL, 'pay_FQBrGMvCAeVs8q', 0, NULL, 50, NULL, 50, 6, 3, 'offline', 2, NULL, 1, 0, 1, '19', '19', '2020-08-13 16:02:00', '2020-08-13 16:02:00'),
(70, 'book-0000044', '19', '1', '2020-08-13 14:30', NULL, NULL, 0, NULL, 50, NULL, 50, 6, 3, 'offline', 1, 12810, 1, 0, 1, '19', '19', '2020-08-13 18:04:38', '2020-08-13 18:04:38'),
(71, 'book-0000045', '19', '1', '2020-08-13 14:30', NULL, NULL, 0, NULL, 50, NULL, 50, 6, 3, 'offline', 1, 12760, 1, 0, 1, '19', '19', '2020-08-13 18:05:35', '2020-08-13 18:05:35'),
(72, 'book-0000046', '19', '1', '2020-08-13 14:30', NULL, NULL, 0, NULL, 50, NULL, 50, 6, 3, 'offline', 1, 12710, 1, 0, 1, '19', '19', '2020-08-13 18:05:53', '2020-08-13 18:05:53'),
(73, 'book-0000047', '19', '1', '2020-08-13 14:30', NULL, NULL, 0, NULL, 50, NULL, 50, 5, NULL, 'offline', 1, 12660, 1, 0, 0, '19', '19', '2020-08-14 19:55:15', '2020-08-14 20:14:51'),
(74, 'book-0000048', '19', '1', '2020-08-15 12:00', NULL, NULL, 0, NULL, 50, NULL, 50, 6, 3, 'offline', 1, 12610, 1, 0, 1, '19', '19', '2020-08-15 12:34:36', '2020-08-15 12:34:36'),
(75, 'book-0000049', '19', '1', '2020-08-15 11:30', '18', NULL, 0, NULL, 50, NULL, 50, 6, 3, 'offline', 1, 12560, 1, 0, 1, '19', '19', '2020-08-15 12:39:51', '2020-08-15 12:39:51'),
(76, 'book-0000050', '19', '1', '2020-08-15 17:00', NULL, NULL, 0, NULL, 50, NULL, 50, 6, 3, 'offline', 1, 12510, 2, 0, 1, '19', '19', '2020-08-15 18:15:12', '2020-08-15 18:15:12'),
(77, 'book-0000051', '19', '1', '2020-08-13 14:30', NULL, NULL, 0, NULL, 50, NULL, 50, 6, 3, 'offline', 1, 12460, 1, 0, 1, '19', '19', '2020-08-15 19:36:59', '2020-08-15 19:36:59'),
(78, 'book-0000052', '26', '51', '2020-08-21 16:00', NULL, NULL, 0, NULL, 350, NULL, 350, 6, 3, 'offline', 1, 750, 2, 0, 1, '26', '26', '2020-08-20 15:24:49', '2020-08-20 15:24:49'),
(79, 'book-0000053', '197', '51', '2020-08-25 16:00', NULL, 'pay_FUbAmTdq0oITZg', 0, NULL, 1, NULL, 1, 6, 3, 'offline', 2, NULL, 2, 0, 1, '197', '197', '2020-08-24 19:24:09', '2020-08-24 19:24:09'),
(80, 'book-0000054', '201', '93', '2020-09-04 09:50', NULL, 'pay_FYRu0yrPnGdmXV', 0, '147852', 101, 100, 1, 5, NULL, 'offline', 2, NULL, 1, 0, 0, '201', '201', '2020-09-03 12:55:59', '2020-09-03 22:53:34'),
(81, 'book-0000055', '202', '128', '2020-09-10 10:45', '0', 'pay_FbCpCcOSsToRQ9', 0, NULL, 1, NULL, 1, 6, 3, 'offline', 2, NULL, 1, 0, 1, '202', '202', '2020-09-10 12:07:57', '2020-09-10 12:07:57'),
(82, 'book-0000056', '196', '91', '2020-09-15 19:00', NULL, 'pay_FdGtosmRkH4Juq', 0, 'welcome', 61, 10, 51, 6, 3, 'offline', 2, NULL, 3, 0, 1, '196', '196', '2020-09-15 17:24:54', '2020-09-15 17:24:54'),
(83, 'book-0000057', '196', '114', '2020-09-17 18:00', NULL, NULL, 0, 'welcome', 200, 10, 190, 4, NULL, 'offline', 1, 310, 3, 0, 0, '196', '196', '2020-09-17 17:28:17', '2020-09-23 12:09:32'),
(84, 'book-0000058', '196', '142', '2020-09-18 18:10', NULL, NULL, 0, 'welcome', 200, 10, 190, 6, 3, 'Online', 1, 620, 3, 0, 1, '196', '196', '2020-09-18 12:21:35', '2020-09-18 12:21:35'),
(85, 'book-0000059', '196', '142', '2020-09-19 15:00', '2', NULL, 0, NULL, 200, NULL, 200, 6, 3, 'Online', 1, 420, 2, 0, 1, '196', '196', '2020-09-19 13:09:09', '2020-09-19 13:09:09'),
(86, 'book-0000060', '216', '144', '2020-09-23 11:45', NULL, 'pay_FgB1uin1KWoyGC', 0, NULL, 10, NULL, 10, 6, 3, 'Online', 2, NULL, 1, 0, 1, '216', '216', '2020-09-23 01:38:50', '2020-09-23 01:38:50'),
(87, 'book-0000061', '221', '144', '2020-09-23 15:30', '0', 'pay_FgQa55ctyxY39A', 0, NULL, 10, NULL, 10, 5, 2, 'Online', 2, NULL, 2, 0, 1, '221', '221', '2020-09-23 16:51:34', '2020-10-07 18:41:31'),
(88, 'book-0000062', '196', '91', '2020-09-23 19:00', NULL, NULL, 0, NULL, 67, NULL, 67, 6, 3, 'Online', 1, 353, 3, 0, 1, '196', '196', '2020-09-23 18:02:59', '2020-09-23 18:02:59'),
(89, 'book-0000063', '204', '145', '2020-09-24 16:45', NULL, 'pay_FgqVUlEDO7GjbJ', 0, NULL, 12, NULL, 12, 6, 3, 'Online', 2, NULL, 2, 0, 1, '204', '204', '2020-09-24 18:12:14', '2020-09-24 18:12:14'),
(90, 'book-0000064', '201', '144', '2020-09-24 18:00', '14', 'pay_FgqXHP90hucOJC', 0, NULL, 2, NULL, 2, 3, NULL, 'Online', 2, NULL, 3, 0, 0, '201', '201', '2020-09-24 18:13:54', '2020-09-24 20:06:23'),
(91, 'book-0000065', '196', '142', '2020-09-24 18:10', '29', NULL, 0, NULL, 50, NULL, 50, 5, 2, 'Online', 1, 150, NULL, 0, 0, '196', '196', '2020-09-24 18:24:31', '2020-09-25 17:40:44'),
(92, 'book-0000066', '191', '114', '2020-09-25 18:30', NULL, NULL, 1, NULL, 50, NULL, 50, 6, 3, 'online', 2, NULL, 1, 0, 1, '', '', '2020-09-24 19:20:07', '2020-09-24 19:20:07'),
(93, 'book-0000067', '191', '114', '2020-09-25 18:30', NULL, NULL, 1, NULL, 50, NULL, 50, 6, 3, 'online', 2, NULL, 1, 0, 1, '', '', '2020-09-24 19:20:58', '2020-09-24 19:20:58'),
(94, 'book-0000068', '201', '144', '2020-09-25 11:30', '8', 'pay_Fh9SyswOyHz6rQ', 0, NULL, 2, NULL, 2, 6, 3, 'Online', 2, NULL, 1, 0, 1, '201', '201', '2020-09-25 12:45:57', '2020-09-25 12:45:57'),
(95, 'book-0000069', '196', '142', '2020-09-25 13:00', '43', 'pay_FhA9FY5FpYED4B', 0, NULL, 203, NULL, 203, 6, 3, 'Online', 2, NULL, 2, 0, 1, '196', '196', '2020-09-25 13:24:29', '2020-09-25 13:24:29'),
(96, 'book-0000070', '196', '142', '2020-09-25 13:10', '29', 'pay_FhBNV97K3C4hCT', 0, NULL, 203, NULL, 203, 6, 3, 'Online', 2, NULL, 2, 0, 1, '196', '196', '2020-09-25 14:36:41', '2020-09-25 14:36:41'),
(97, 'book-0000071', '196', '142', '2020-09-25 13:20', NULL, 'pay_FhCtXpqY1V9q8q', 0, NULL, 203, NULL, 203, 6, 3, 'Online', 2, NULL, 2, 0, 1, '196', '196', '2020-09-25 16:05:42', '2020-09-25 16:05:42'),
(98, 'book-0000072', '196', '142', '2020-09-25 18:00', NULL, 'pay_FhEuLHFnkKf71k', 0, NULL, 203, NULL, 203, 6, 3, 'Online', 2, NULL, 3, 0, 1, '196', '196', '2020-09-25 18:03:48', '2020-09-25 18:03:48'),
(99, 'book-0000073', '196', '142', '2020-09-26 10:00', '27', NULL, 0, NULL, 203, NULL, 203, 6, 3, 'Online', 1, 47, 1, 0, 1, '196', '196', '2020-09-26 11:01:18', '2020-09-26 11:01:18'),
(100, 'book-0000074', '216', '144', '2020-09-27 15:15', NULL, 'pay_Fi0Zf74pkSevY5', 0, NULL, 10, NULL, 10, 6, 3, 'Online', 2, NULL, 2, 0, 1, '216', '216', '2020-09-27 16:41:36', '2020-09-27 16:41:36'),
(101, 'book-0000075', '208', '144', '2020-09-27 15:30', '8', 'pay_Fi0iw35obW7oG3', 0, NULL, 10, NULL, 10, 6, 3, 'Online', 2, NULL, 2, 0, 1, '208', '208', '2020-09-27 16:50:40', '2020-09-27 16:50:40'),
(105, 'book-0000076', '191', '114', '2020-09-29 15:30', NULL, 'pay_dgfgfdgdfgd', 1, NULL, 50, NULL, 50, 6, 3, 'offline', 2, NULL, 3, 0, 1, '', '', '2020-09-28 19:28:40', '2020-09-28 19:28:40'),
(106, 'book-0000077', '229', '144', '2020-09-30 19:15', NULL, 'pay_FjEkiB7YfLEunU', 0, NULL, 9, NULL, 9, 5, 2, 'Online', 2, NULL, 3, 0, 1, '229', '229', '2020-09-30 19:12:58', '2020-10-07 18:42:17'),
(107, 'book-0000078', '196', '142', '2020-10-01 13:00', NULL, NULL, 0, NULL, 203, NULL, 203, 5, 1, 'Online', 1, 844, 2, 0, 0, '196', '196', '2020-10-01 11:38:52', '2020-10-01 11:38:52'),
(108, 'book-0000079', '196', '142', '2020-10-01 14:00', '12', NULL, 0, NULL, 203, NULL, 203, 6, 3, 'Online', 1, 844, 2, 0, 1, '196', '196', '2020-10-01 15:14:23', '2020-10-01 15:14:23'),
(109, 'book-0000080', '196', '142', '2020-10-01 14:10', '10', NULL, 0, NULL, 203, NULL, 203, 6, 3, 'Online', 1, 641, 2, 0, 1, '196', '196', '2020-10-01 15:27:47', '2020-10-01 15:27:47'),
(110, 'book-0000081', '196', '142', '2020-10-01 18:00', NULL, NULL, 0, NULL, 203, NULL, 203, 6, 3, 'Online', 1, 438, 3, 1, 1, '196', '196', '2020-10-01 16:56:07', '2020-10-01 16:56:07'),
(111, 'book-0000082', '191', '114', '2020-10-05 09:40', NULL, 'pay_FjujySvtuzAPgj', 0, NULL, 51, NULL, 51, 5, 1, 'Online', 2, NULL, 1, 0, 0, '191', '191', '2020-10-02 12:17:09', '2020-10-02 12:17:09'),
(112, 'book-0000083', '191', '114', '2020-10-02 14:10', NULL, 'pay_Fjv6Y6mGT2Y2v3', 0, NULL, 51, NULL, 51, 5, 1, 'Online', 2, NULL, 2, 0, 0, '191', '191', '2020-10-02 12:38:30', '2020-10-02 12:38:30'),
(113, 'book-0000084', '191', '114', '2020-10-02 18:00', NULL, NULL, 0, NULL, 51, NULL, 51, 6, 3, 'Online', 1, 0, NULL, 0, 1, '191', '191', '2020-10-02 18:36:58', '2020-10-02 18:36:58'),
(114, 'book-0000085', '191', '114', '2020-10-03 19:50', NULL, 'pay_FkSKhsfLEEzR4c', 0, NULL, 51, NULL, 51, 6, 3, 'Online', 2, NULL, 3, 0, 1, '191', '191', '2020-10-03 21:08:55', '2020-10-03 21:08:55'),
(115, 'book-0000086', '216', '144', '2020-10-10 11:30', NULL, 'pay_FknOxVZLmsuyKW', 0, NULL, 10, NULL, 10, 4, 3, 'Online', 2, NULL, 1, 0, 1, '216', '216', '2020-10-04 17:45:31', '2020-10-04 17:45:31'),
(116, 'book-0000087', '196', '142', '2020-10-06 13:00', '26', NULL, 0, NULL, 203, NULL, 203, 6, 3, 'Online', 1, 2367, 2, 0, 1, '196', '196', '2020-10-06 12:02:58', '2020-10-06 12:02:58'),
(117, 'book-0000088', '191', '114', '2020-10-06 17:30', '1', 'pay_FlbePCW60xySs6', 0, NULL, 51, NULL, 51, 6, 3, 'Online', 2, NULL, 3, 0, 1, '191', '191', '2020-10-06 18:54:42', '2020-10-06 18:54:42'),
(118, 'book-0000089', '196', '142', '2020-10-07 18:00', '29', NULL, 0, NULL, 203, NULL, 203, 3, NULL, 'Online', 1, 2367, 3, 0, 0, '196', '196', '2020-10-07 16:39:42', '2020-10-07 16:49:21'),
(119, 'book-0000090', '196', '142', '2020-10-07 18:10', '15', NULL, 0, NULL, 203, NULL, 203, 6, 3, 'Online', 1, 2164, 3, 0, 1, '196', '196', '2020-10-07 18:22:48', '2020-10-07 18:22:48'),
(120, 'book-0000091', '196', '91', '2020-10-07 19:00', NULL, NULL, 0, NULL, 67, NULL, 67, 6, 3, 'Online', 1, 2097, 3, 0, 1, '196', '196', '2020-10-07 18:58:39', '2020-10-07 18:58:39'),
(121, 'book-0000092', '196', '142', '2020-10-07 18:20', NULL, NULL, 0, NULL, 203, NULL, 203, 6, 3, 'Online', 1, 1894, 3, 0, 1, '196', '196', '2020-10-07 18:59:20', '2020-10-07 18:59:20'),
(122, '91-B-0000093', '196', '142', '2020-10-08 18:10', NULL, NULL, 0, NULL, 1, NULL, 1, 4, NULL, 'Offline', 1, 2366, NULL, 0, 0, '196', '196', '2020-10-08 17:21:40', '2020-10-08 17:21:40'),
(123, '91-B-00000094', '201', '93', '2020-10-10 09:25', NULL, 'pay_FmoN77J5MXGIvJ', 0, NULL, 3, NULL, 3, 6, 3, 'Online', 2, NULL, 1, 0, 1, '201', '201', '2020-10-09 20:00:20', '2020-10-09 20:00:20'),
(124, '91-B-00000095', '196', '142', '2020-10-10 18:00', NULL, NULL, 0, NULL, 203, NULL, 203, 6, 3, 'Online', 1, 2163, 3, 0, 1, '196', '196', '2020-10-10 15:56:32', '2020-10-10 15:56:32'),
(125, '91-B-00000096', '216', '144', '2020-10-11 12:00', NULL, 'pay_FnUYmR1PWAANcB', 0, NULL, 10, NULL, 10, 6, 3, 'Online', 2, NULL, 1, 0, 1, '216', '216', '2020-10-11 13:16:31', '2020-10-11 13:16:31'),
(126, '91-B-00000097', '196', '142', '2020-10-11 18:50', '1', NULL, 0, NULL, 203, NULL, 203, 6, 3, 'Online', 1, 2163, 3, 0, 1, '196', '196', '2020-10-11 20:18:09', '2020-10-11 20:18:09'),
(127, '91-B-00000098', '196', '142', '2020-10-12 19:30', '17', NULL, 0, NULL, 203, NULL, 203, 6, 3, 'Online', 1, 2163, 3, 0, 1, '196', '196', '2020-10-12 20:42:19', '2020-10-12 20:42:19'),
(128, '91-B-00000099', '196', '142', '2020-10-13 15:00', '13', NULL, 0, NULL, 203, NULL, 203, 6, 3, 'Online', 1, 2163, 2, 0, 1, '196', '196', '2020-10-13 16:16:28', '2020-10-13 16:16:28'),
(129, '91-B-000000100', '201', '157', '2020-10-31 06:00', NULL, 'pay_Fpy937Vj4rS7q1', 0, NULL, 1, NULL, 1, 6, 3, 'Offline', 2, NULL, 1, 0, 1, '201', '201', '2020-10-17 19:30:58', '2020-10-17 19:30:58'),
(130, '91-B-000000101', '196', '142', '2020-10-17 21:20', '6', NULL, 0, NULL, 203, NULL, 203, 6, 3, 'Online', 1, 2163, 3, 0, 1, '196', '196', '2020-10-17 22:43:04', '2020-10-17 22:43:04'),
(131, '91-B-000000102', '196', '142', '2020-10-18 13:00', NULL, NULL, 0, NULL, 203, NULL, 203, 5, 1, 'Online', 1, 2163, 2, 0, 0, '196', '196', '2020-10-18 12:39:44', '2020-10-18 12:39:44'),
(132, '91-B-000000103', '196', '142', '2020-10-18 13:20', NULL, NULL, 0, NULL, 203, NULL, 203, 4, NULL, 'Online', 1, 2163, NULL, 0, 0, '196', '196', '2020-10-18 13:02:22', '2020-10-18 13:02:22'),
(133, '91-B-000000104', '216', '144', '2020-10-19 09:15', NULL, 'pay_FqSrtB0XQureFm', 0, NULL, 10, NULL, 10, 4, NULL, 'Online', 2, NULL, NULL, 0, 0, '216', '216', '2020-10-19 01:34:14', '2020-10-19 01:34:14'),
(135, '91-B-000000105', '191', '114', '2020-09-29 15:30', NULL, 'pay_dgfgfdgdfgd', 1, NULL, 50, NULL, 50, 6, 3, 'offline', 2, NULL, NULL, 0, 1, '', '', '2020-10-19 19:45:50', '2020-10-19 19:45:50'),
(136, '91-B-000000106', '191', '114', '2020-09-29 15:30', NULL, 'pay_dgfgfdgdfgd', 1, NULL, 50, NULL, 50, 6, 3, 'offline', 2, NULL, NULL, 0, 1, '', '', '2020-10-19 19:46:54', '2020-10-19 19:46:54'),
(137, '91-B-000000107', '191', '114', '2020-09-29 15:30', NULL, 'pay_dgfgfdgdfgd', 1, NULL, 50, NULL, 50, 6, 3, 'offline', 2, NULL, NULL, 0, 1, '', '', '2020-10-19 19:51:39', '2020-10-19 19:51:39'),
(138, '91-B-000000108', '191', '114', '2020-09-29 15:30', NULL, 'pay_dgfgfdgdfgd', 1, NULL, 50, NULL, 50, 6, 3, 'offline', 2, NULL, NULL, 0, 1, '', '', '2020-10-19 19:52:13', '2020-10-19 19:52:13'),
(139, '91-B-000000109', '196', '142', '2020-10-19 20:50', NULL, NULL, 0, NULL, 203, NULL, 203, 6, 3, 'Online', 1, 1960, 3, 0, 1, '196', '196', '2020-10-19 22:03:23', '2020-10-19 22:03:23'),
(140, '91-B-000000110', '221', '136', '2021-01-05 15:00', NULL, 'pay_Fr1vaqiKECrWhF', 0, NULL, 2, NULL, 2, 4, 3, 'Online', 2, NULL, 2, 0, 1, '221', '221', '2020-10-20 11:52:05', '2020-10-20 11:52:05'),
(141, '91-B-000000111', '201', '157', '2020-10-20 12:00', NULL, 'pay_Fr22Z6KOwR9hTA', 0, NULL, 1, NULL, 1, 6, 3, 'Offline', 2, NULL, 2, 0, 1, '201', '201', '2020-10-20 11:58:36', '2020-10-20 11:58:36'),
(142, '91-B-000000112', '201', '157', '2020-10-20 12:15', NULL, 'pay_Fr23YYgb3g62Hh', 0, NULL, 5001, NULL, 5001, 6, 3, 'Online', 2, NULL, 2, 0, 1, '201', '201', '2020-10-20 11:59:32', '2020-10-20 11:59:32'),
(143, '91-B-000000113', '201', '157', '2020-10-20 18:00', NULL, 'pay_Fr25uH0jX2efzM', 0, NULL, 5001, NULL, 5001, 6, 3, 'Online', 2, NULL, 3, 0, 1, '201', '201', '2020-10-20 12:01:46', '2020-10-20 12:01:46'),
(144, '91-B-000000114', '221', '136', '2020-10-20 10:35', '0', 'pay_Fr26s4h2lsWXPT', 0, NULL, 2, NULL, 2, 3, NULL, 'Online', 2, NULL, 1, 0, 0, '221', '221', '2020-10-20 12:02:45', '2020-10-20 12:13:06'),
(145, '91-B-000000115', '221', '136', '2020-10-20 10:50', '1', 'pay_Fr2LlR4S2F9mvn', 0, NULL, 2, NULL, 2, 3, NULL, 'Online', 2, NULL, 1, 0, 0, '221', '221', '2020-10-20 12:16:47', '2020-10-20 12:24:01'),
(146, '91-B-000000116', '221', '136', '2020-10-20 11:00', '0', 'pay_Fr2WE9KSqol2Ix', 0, NULL, 2, NULL, 2, 3, NULL, 'Online', 2, NULL, 1, 0, 0, '221', '221', '2020-10-20 12:26:40', '2020-10-20 12:36:43'),
(147, '91-B-000000117', '221', '136', '2020-10-20 11:15', NULL, 'pay_Fr2nepl7CxfXgY', 0, NULL, 2, NULL, 2, 6, 3, 'Online', 2, NULL, 1, 0, 1, '221', '221', '2020-10-20 12:43:10', '2020-10-20 12:43:10'),
(148, '91-B-000000118', '221', '136', '2020-10-20 11:35', '0', 'pay_Fr38fRsd8qhrjT', 0, NULL, 2, NULL, 2, 3, NULL, 'Online', 2, NULL, 1, 0, 0, '221', '221', '2020-10-20 13:03:04', '2020-10-20 13:08:18'),
(149, '91-B-000000119', '221', '136', '2020-10-20 16:25', '1', 'pay_Fr86C4LUaD3CMF', 0, NULL, 2, NULL, 2, 3, NULL, 'Online', 2, NULL, 2, 0, 0, '221', '221', '2020-10-20 17:54:11', '2020-10-20 17:58:02'),
(150, '91-B-000000120', '216', '144', '2020-10-21 10:45', '19', 'pay_FrQUiB5GakplL2', 0, NULL, 10, NULL, 10, 6, 3, 'Online', 2, NULL, 1, 0, 1, '216', '216', '2020-10-21 11:53:53', '2020-10-21 11:53:53'),
(151, '91-B-000000121', '221', '136', '2020-10-21 12:35', '2', 'pay_FrSez9OCASdscG', 0, NULL, 2, NULL, 2, 6, 3, 'Online', 2, NULL, 2, 0, 1, '221', '221', '2020-10-21 14:01:01', '2020-10-21 14:01:01'),
(152, '91-B-000000122', '196', '142', '2020-10-21 20:30', '10', NULL, 0, NULL, 203, NULL, 203, 6, 3, 'Online', 1, 1960, 3, 0, 1, '196', '196', '2020-10-21 21:42:50', '2020-10-21 21:42:50'),
(153, '91-B-000000123', '196', '142', '2020-10-21 20:40', '9', NULL, 0, NULL, 203, NULL, 203, 6, 3, 'Online', 1, 1757, 3, 0, 1, '196', '196', '2020-10-21 21:52:30', '2020-10-21 21:52:30'),
(154, '91-B-000000124', '196', '142', '2020-10-21 21:00', '8', NULL, 0, NULL, 203, NULL, 203, 6, 3, 'Online', 1, 1554, 3, 0, 1, '196', '196', '2020-10-21 22:05:10', '2020-10-21 22:05:10'),
(155, '91-B-000000125', '196', '142', '2020-10-21 21:10', '7', NULL, 0, NULL, 203, NULL, 203, 3, NULL, 'Online', 1, 1351, 3, 0, 0, '196', '196', '2020-10-21 22:25:10', '2020-10-21 22:55:20'),
(156, '91-B-000000126', '196', '142', '2020-10-21 21:20', NULL, NULL, 0, NULL, 203, NULL, 203, 6, 3, 'Online', 1, 1148, 3, 0, 1, '196', '196', '2020-10-21 22:33:47', '2020-10-21 22:33:47'),
(157, '91-B-000000127', '196', '142', '2020-10-21 21:40', '2', NULL, 0, NULL, 203, NULL, 203, 6, 3, 'Online', 1, 945, 3, 0, 1, '196', '196', '2020-10-21 22:54:18', '2020-10-21 22:54:18'),
(158, '91-B-000000128', '196', '142', '2020-10-21 21:50', '15', NULL, 0, NULL, 203, NULL, 203, 2, 3, 'Online', 1, 945, 3, 0, 1, '196', '196', '2020-10-21 23:14:21', '2020-10-21 23:14:21'),
(159, '91-B-000000129', '216', '144', '2020-10-22 09:00', NULL, 'pay_FrdDFeetB8kKfi', 0, NULL, 10, NULL, 10, 6, 3, 'Online', 2, NULL, 1, 0, 1, '216', '216', '2020-10-22 00:20:22', '2020-10-22 00:20:22'),
(160, '91-B-000000130', '201', '157', '2020-10-23 05:00', NULL, 'pay_FrePrT1fNEI8nn', 0, NULL, 5001, NULL, 5001, 6, 3, 'Online', 2, NULL, 1, 0, 1, '201', '201', '2020-10-22 01:31:01', '2020-10-22 01:31:01'),
(161, '91-B-000000131', '201', '157', '2020-10-22 05:30', NULL, 'pay_FreSPUPLxrbnN2', 0, NULL, 5001, NULL, 5001, 6, 3, 'Online', 2, NULL, 1, 0, 1, '201', '201', '2020-10-22 01:33:28', '2020-10-22 01:33:28'),
(162, '91-B-000000132', '221', '136', '2020-10-22 19:10', NULL, 'pay_Frx1UTSE3kk600', 0, NULL, 2, NULL, 2, 6, 3, 'Online', 2, NULL, 3, 0, 1, '221', '221', '2020-10-22 19:43:07', '2020-10-22 19:43:07'),
(163, '91-B-000000133', '196', '142', '2020-10-24 05:00', NULL, NULL, 0, NULL, 203, NULL, 203, 6, 3, 'Online', 1, 1757, 1, 0, 1, '196', '196', '2020-10-22 22:22:57', '2020-10-22 22:22:57'),
(164, '91-B-000000134', '196', '142', '2020-10-22 21:40', '4', NULL, 0, NULL, 203, NULL, 203, 6, 3, 'Online', 1, 1554, 3, 0, 1, '196', '196', '2020-10-22 22:47:24', '2020-10-22 22:47:24'),
(165, '91-B-000000135', '216', '144', '2020-10-24 09:00', NULL, 'pay_Fs2jYbz0qdTpOX', 0, NULL, 10, NULL, 10, 6, 3, 'Online', 2, NULL, 1, 0, 1, '216', '216', '2020-10-23 01:19:04', '2020-10-23 01:19:04'),
(166, '91-B-000000136', '191', '114', '2020-10-23 14:00', NULL, 'pay_FsFMpIZQ7gbnb5', 0, NULL, 51, NULL, 51, 3, 3, 'Online', 2, NULL, 2, 0, 1, '191', '191', '2020-10-23 13:39:49', '2020-10-23 13:39:49'),
(167, '91-B-000000137', '221', '136', '2020-10-23 21:20', NULL, NULL, 0, NULL, 2, NULL, 2, 6, 3, 'Online', 1, 26, 3, 0, 1, '221', '221', '2020-10-23 22:04:05', '2020-10-23 22:04:05'),
(168, '91-B-000000138', '216', '144', '2020-10-26 09:00', NULL, 'pay_FsQCjtty2F4AuR', 0, NULL, 10, NULL, 10, 6, 3, 'Online', 2, NULL, 1, 0, 1, '216', '216', '2020-10-24 00:16:40', '2020-10-24 00:16:40'),
(169, '91-B-000000139', '201', '157', '2020-10-24 10:00', '11', NULL, 0, NULL, 5001, NULL, 5001, 6, 3, 'Online', 1, 15009, 1, 0, 1, '201', '201', '2020-10-24 11:11:36', '2020-10-24 11:11:36'),
(170, '91-B-000000140', '221', '136', '2020-10-24 11:15', '5', NULL, 0, NULL, 2, NULL, 2, 3, NULL, 'Online', 1, 26, 1, 0, 0, '221', '221', '2020-10-24 12:33:12', '2020-10-24 12:45:09'),
(171, '91-B-000000141', '201', '157', '2020-10-25 10:16', NULL, NULL, 0, NULL, 5001, NULL, 5001, 4, NULL, 'Online', 1, 15009, NULL, 0, 0, '201', '201', '2020-10-24 19:41:23', '2020-10-24 19:41:23'),
(172, '91-B-000000142', '201', '144', '2020-10-25 13:30', NULL, NULL, 0, NULL, 1, NULL, 1, 6, 3, 'Offline', 1, 15008, 2, 0, 1, '201', '201', '2020-10-24 20:04:42', '2020-10-24 20:04:42'),
(173, '91-B-000000143', '201', '157', '2020-10-26 10:44', NULL, NULL, 0, NULL, 5001, NULL, 5001, 6, 3, 'Online', 1, 10007, 1, 0, 1, '201', '201', '2020-10-24 20:10:38', '2020-10-24 20:10:38'),
(174, '91-B-000000144', '196', '142', '2020-10-25 08:00', '5', NULL, 0, NULL, 203, NULL, 203, 6, 3, 'Online', 1, 1757, 1, 0, 1, '196', '196', '2020-10-25 09:23:39', '2020-10-25 09:23:39'),
(175, '91-B-000000145', '191', '114', '2020-10-25 14:08', NULL, NULL, 0, NULL, 51, NULL, 51, 6, 3, 'Online', 1, 503, 2, 0, 1, '191', '191', '2020-10-25 13:43:14', '2020-10-25 13:43:14'),
(176, '91-B-000000146', '196', '142', '2020-10-25 13:20', NULL, NULL, 0, NULL, 203, NULL, 203, 6, 3, 'Online', 1, 1757, 2, 0, 1, '196', '196', '2020-10-25 14:39:14', '2020-10-25 14:39:14'),
(177, '91-B-000000147', '196', '142', '2020-10-29 05:00', NULL, NULL, 0, NULL, 203, NULL, 203, 4, NULL, 'Online', 1, 1554, NULL, 0, 0, '196', '196', '2020-10-25 14:39:47', '2020-10-25 14:39:47'),
(178, '91-B-000000148', '196', '142', '2020-10-25 14:00', NULL, NULL, 0, NULL, 203, NULL, 203, 6, 3, 'Online', 1, 1351, 2, 0, 1, '196', '196', '2020-10-25 14:55:19', '2020-10-25 14:55:19'),
(179, '91-B-000000149', '191', '114', '2020-10-26 18:00', NULL, NULL, 0, NULL, 51, NULL, 51, 4, NULL, 'Online', 1, 503, NULL, 0, 0, '191', '191', '2020-10-26 18:52:33', '2020-10-26 18:52:33'),
(180, '91-B-000000150', '191', '114', '2020-10-26 18:05', '13', NULL, 0, NULL, 51, NULL, 51, 3, NULL, 'Online', 1, 452, 3, 0, 0, '191', '191', '2020-10-26 19:01:10', '2020-10-26 19:25:03'),
(181, '91-B-000000151', '216', '144', '2020-10-30 10:30', NULL, NULL, 0, NULL, 10, NULL, 10, 6, 3, 'Online', 1, 170, 1, 0, 1, '216', '216', '2020-10-28 18:47:51', '2020-10-28 18:47:51'),
(182, '91-B-000000152', '254', '144', '2020-10-28 18:15', '10', 'pay_FuK3ybYvhvApMo', 0, NULL, 10, NULL, 10, 6, 3, 'Online', 2, NULL, 3, 0, 1, '254', '254', '2020-10-28 19:33:42', '2020-10-28 19:33:42'),
(183, '91-B-000000153', '191', '114', '2020-10-29 18:47', NULL, NULL, 0, NULL, 51, NULL, 51, 6, 3, 'Online', 1, 401, 3, 0, 1, '191', '191', '2020-10-29 19:10:07', '2020-10-29 19:10:07'),
(184, '91-B-000000154', '254', '144', '2020-10-30 12:00', NULL, NULL, 0, NULL, 10, NULL, 10, 5, 1, 'Online', 1, 0, NULL, 0, 0, '254', '254', '2020-10-30 01:20:06', '2020-10-30 01:20:06'),
(185, '91-B-000000155', '254', '144', '2020-10-30 13:30', NULL, NULL, 0, NULL, 10, NULL, 10, 4, NULL, 'Online', 1, 0, NULL, 0, 0, '254', '254', '2020-10-30 13:31:07', '2020-10-30 13:31:07'),
(186, '91-B-000000156', '221', '136', '2020-10-30 13:40', '2', NULL, 0, NULL, 2, NULL, 2, 6, 3, 'Online', 1, 24, 2, 0, 1, '221', '221', '2020-10-30 15:08:10', '2020-10-30 15:08:10'),
(187, '91-B-000000157', '221', '136', '2020-10-30 13:45', '3', NULL, 0, NULL, 2, NULL, 2, 6, 3, 'Online', 1, 22, 2, 0, 1, '221', '221', '2020-10-30 15:11:55', '2020-10-30 15:11:55'),
(188, '91-B-000000158', '221', '136', '2020-11-06 10:25', NULL, NULL, 0, NULL, 2, NULL, 2, 6, 3, 'Online', 1, 24, 1, 0, 1, '221', '221', '2020-11-05 19:04:19', '2020-11-05 19:04:19'),
(189, '91-B-000000159', '254', '136', '2020-11-07 14:40', NULL, 'pay_Fxn101CiePdEUy', 0, NULL, 2, NULL, 2, 4, 1, 'Online', 2, NULL, NULL, 0, 0, '254', '254', '2020-11-06 19:20:00', '2020-11-06 19:20:00'),
(190, '91-B-000000160', '254', '136', '2020-11-06 13:05', NULL, NULL, 0, NULL, 2, NULL, 2, 4, NULL, 'Online', 1, 0, NULL, 0, 0, '254', '254', '2020-11-06 19:59:45', '2020-11-06 19:59:45'),
(191, '91-B-000000161', '254', '136', '2020-11-07 19:50', NULL, 'pay_FyEmZzx7VTPFYK', 0, NULL, 2, NULL, 2, 5, 1, 'Online', 2, NULL, 3, 0, 0, '254', '254', '2020-11-07 22:30:03', '2020-11-07 22:30:03'),
(192, '91-B-000000162', '254', '160', '2020-11-07 18:30', NULL, NULL, 0, NULL, 1, NULL, 1, 5, 2, 'Online', 1, 7, 2, 0, 1, '254', '254', '2020-11-08 00:47:20', '2020-11-08 00:49:37'),
(193, '91-B-000000163', '254', '160', '2020-11-07 18:00', '8', NULL, 0, NULL, 1, NULL, 1, 3, NULL, 'Online', 1, 7, 2, 0, 0, '254', '254', '2020-11-08 00:51:13', '2020-11-08 00:55:25'),
(194, '91-B-000000164', '221', '136', '2020-11-08 10:05', NULL, NULL, 0, NULL, 2, NULL, 2, 6, 3, 'Online', 1, 24, 1, 0, 1, '221', '221', '2020-11-08 01:10:39', '2020-11-08 01:10:39'),
(195, '91-B-000000165', '221', '136', '2020-11-16 10:25', NULL, NULL, 0, NULL, 2, NULL, 2, 6, 3, 'Online', 1, 24, 1, 0, 1, '221', '221', '2020-11-15 01:25:45', '2020-11-15 01:25:45'),
(196, '91-B-000000166', '221', '136', '2020-11-19 12:00', '6', NULL, 0, NULL, 2, NULL, 2, 6, 3, 'Online', 1, 24, 1, 0, 1, '221', '221', '2020-11-19 18:48:45', '2020-11-19 18:48:45'),
(197, '91-B-000000167', '221', '136', '2020-11-19 11:55', '2', NULL, 0, NULL, 2, NULL, 2, 6, 3, 'Online', 1, 22, 1, 0, 1, '221', '221', '2020-11-19 18:50:03', '2020-11-19 18:50:03'),
(198, '91-B-000000168', '260', '136', '2020-11-22 13:40', '1', 'pay_G48ri9GBxSbKVy', 0, NULL, 2, NULL, 2, 6, 3, 'Online', 2, NULL, 2, 0, 1, '260', '260', '2020-11-22 20:36:25', '2020-11-22 20:36:25'),
(199, '91-B-000000169', '221', '136', '2020-12-21 14:30', NULL, NULL, 0, NULL, 2, NULL, 2, 6, 3, 'Online', 1, 24, 2, 0, 1, '221', '221', '2020-12-21 21:23:35', '2020-12-21 21:23:35'),
(200, '91-B-000000170', '221', '136', '2021-01-05 14:20', NULL, NULL, 0, NULL, 2, NULL, 2, 6, 3, 'Online', 1, 24, 2, 0, 1, '221', '221', '2021-01-05 21:19:07', '2021-01-05 21:19:07'),
(201, '91-B-000000171', '221', '136', '2021-01-05 14:30', '0', NULL, 0, NULL, 2, NULL, 2, 3, NULL, 'Online', 1, 22, 2, 0, 0, '221', '221', '2021-01-05 21:25:32', '2021-01-05 21:34:47'),
(202, '91-B-000000172', '221', '136', '2021-01-06 19:00', '3', NULL, 0, NULL, 2, NULL, 2, 3, NULL, 'Online', 1, 22, 3, 0, 0, '221', '221', '2021-01-07 01:36:53', '2021-01-07 01:58:46'),
(203, '91-B-000000173', '221', '136', '2021-01-06 19:05', '5', NULL, 0, NULL, 2, NULL, 2, 6, 3, 'Online', 1, 20, 3, 0, 1, '221', '221', '2021-01-07 01:59:32', '2021-01-07 01:59:32'),
(204, '91-B-000000174', '221', '136', '2021-01-09 17:50', NULL, NULL, 0, NULL, 2, NULL, 2, 6, 3, 'Online', 1, 20, 2, 0, 1, '221', '221', '2021-01-10 00:42:40', '2021-01-10 00:42:40'),
(205, '91-B-000000175', '221', '136', '2021-01-09 17:55', NULL, NULL, 0, NULL, 2, NULL, 2, 6, 3, 'Online', 1, 18, 2, 0, 1, '221', '221', '2021-01-10 00:55:09', '2021-01-10 00:55:09');

-- --------------------------------------------------------

--
-- Table structure for table `business_register`
--

CREATE TABLE `business_register` (
  `id` int(10) UNSIGNED NOT NULL,
  `v_uni_id` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `b_id` int(11) DEFAULT NULL COMMENT 'role id',
  `b_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `conatact_person` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile1` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile2` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gst_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `working_hr` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `open_time` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `close_time` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `open_24` tinyint(4) DEFAULT '2' COMMENT '1 for yes and 2 for no',
  `weekly_off` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `all_days_open` tinyint(4) NOT NULL DEFAULT '2' COMMENT '1 for yes and 2 for no',
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_id` int(5) NOT NULL DEFAULT '3',
  `state_id` int(11) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `pincode` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `b_lic` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `b_card_pic` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shop_pic` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `patient_select_you` text COLLATE utf8mb4_unicode_ci,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `step` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `district` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `admin_status` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `business_register`
--

INSERT INTO `business_register` (`id`, `v_uni_id`, `user_id`, `b_id`, `b_name`, `conatact_person`, `mobile1`, `mobile2`, `email`, `gst_no`, `working_hr`, `open_time`, `close_time`, `open_24`, `weekly_off`, `all_days_open`, `address`, `country_id`, `state_id`, `city_id`, `pincode`, `b_lic`, `b_card_pic`, `shop_pic`, `image`, `description`, `patient_select_you`, `created_by`, `updated_by`, `status`, `step`, `created_at`, `updated_at`, `deleted_at`, `district`, `admin_status`) VALUES
(40, NULL, 1111, NULL, 'Samntu', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24, 'sunday', 1, 'ffghfg', 3, 36, 1744, '2733306', '1610703505__124835240_1083793208728045_4549733389804897883_o.jpg', '1610698267__Kizaku Logo.png', '1610698267__i1.png', NULL, NULL, '36-1744-2733306-ffghfg-hg-mah', 1111, 1111, NULL, NULL, '2021-01-15 20:41:07', '2021-01-21 22:40:16', NULL, NULL, '1'),
(48, NULL, 119, NULL, 'M a', NULL, NULL, NULL, NULL, NULL, NULL, '00:00', '00:00', 24, NULL, 0, 'hillview medical college calicut', 3, 36, 1744, '673008', NULL, '1610968618__cropped4699865828203527193.jpg', '1610968618__cropped4003238627901190709.jpg', NULL, NULL, '[\"Easy Location\",\"Door Delivery\",\"Only Original Medicines\",\"Good Discounts\",\"Friendly Beheviour\",\"Enter Business Profile\",\"{\\\"Enter Business Profile\\\":\\\"hair akdba sjka\\\"}\"]', 119, 119, NULL, NULL, '2021-01-18 23:46:58', '2021-01-21 23:31:34', NULL, NULL, '1'),
(49, NULL, 120, NULL, 'bio', NULL, NULL, NULL, NULL, NULL, NULL, '00:00', '00:00', 24, NULL, 0, 'medical college', 3, 36, 1744, '673008', '1610969104__cropped1494084494474579706.jpg', '1610969067__cropped494813564960736932.jpg', '1610969067__cropped245377029095086685.jpg', NULL, NULL, '[\"Easy Location\",\"Door Delivery\",\"Only Original Medicines\",\"Good Discounts\",\"Friendly Beheviour\"]', 120, 120, NULL, NULL, '2021-01-18 23:54:27', '2021-01-18 23:55:08', NULL, NULL, NULL),
(52, NULL, 124, NULL, 'Testt', NULL, NULL, NULL, NULL, NULL, NULL, '00:00', '00:00', 24, NULL, 0, NULL, 3, NULL, NULL, NULL, NULL, '1611137911__i1.png', '1611137911__i1.png', NULL, NULL, NULL, 124, 124, NULL, NULL, '2021-01-20 21:57:26', '2021-01-20 22:48:31', NULL, NULL, NULL),
(53, NULL, 128, NULL, 'M. A. Medical Supports', NULL, NULL, NULL, NULL, NULL, NULL, '9:30AM', '7:30PM', 2, 'Sunday', 2, 'Hill View Building, Medical College, Calicut.', 3, 36, 1744, '673008', NULL, 'file:///data/user/0/com.haspatal.haspatal360/cache/cropped8694249838972293301.jpg', 'file:///data/user/0/com.haspatal.haspatal360/cache/cropped705122588153622545.jpg', 'file:///data/user/0/com.haspatal.haspatal360/cache/cropped8099938658997651679.jpg', NULL, NULL, 128, 128, NULL, NULL, '2021-01-21 00:50:46', '2021-01-21 23:38:02', NULL, NULL, '1'),
(54, NULL, 130, NULL, 'M. A. Medical Supports', NULL, NULL, NULL, NULL, NULL, NULL, '9:30AM', '8:0PM', 2, 'Sunday', 2, 'Hill View Building, Medical College, Calicut', 3, 36, 1744, '673008', NULL, 'file:///data/user/0/com.haspatal.haspatal360/cache/cropped9053527160958502049.jpg', 'file:///data/user/0/com.haspatal.haspatal360/cache/cropped2235347436456223493.jpg', 'file:///data/user/0/com.haspatal.haspatal360/cache/cropped611692460124418176.jpg', NULL, '[\"Easy Location\",\"Door Delivery\",\"Only Original Medicines\",\"Good Discounts\",\"Friendly Beheviour\"]', 130, 130, NULL, NULL, '2021-01-21 05:32:42', '2021-01-21 23:38:23', NULL, NULL, '1'),
(63, NULL, 143, NULL, 'new backend', NULL, NULL, NULL, NULL, NULL, NULL, '8:00 am', '8:00 pm', 2, NULL, 2, 'new backend test 1 uploading documents and view', 3, 3, 1599, '695551', '1611210964__2.jpg', '1611210874__about.jpg', '1611210880__1.jpg', NULL, '1', NULL, 143, 143, NULL, NULL, '2021-01-21 19:06:11', '2021-01-21 19:06:11', NULL, NULL, NULL),
(64, NULL, 144, NULL, 'test 2 new backend', NULL, NULL, NULL, NULL, NULL, NULL, '9:00 am', '6:00 pm', 2, NULL, 2, 'new backend test 1 uploading documents and view', 3, 3, 1733, '695550', '1611211447__2.jpg', '1611211380__1.jpg', '1611211387__about.jpg', NULL, '1', NULL, 144, 144, NULL, NULL, '2021-01-21 19:14:18', '2021-01-21 23:31:07', NULL, NULL, '1');

-- --------------------------------------------------------

--
-- Table structure for table `business_type`
--

CREATE TABLE `business_type` (
  `id` int(10) UNSIGNED NOT NULL,
  `b_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `b_desc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `business_type`
--

INSERT INTO `business_type` (`id`, `b_name`, `b_desc`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Demo', 'Demo data', 1, 1, '2020-05-28 10:34:37', '2020-05-28 10:34:37', NULL),
(2, 'Hospital', 'Hospital 1111', 1, 1, '2020-06-08 09:26:19', '2020-06-08 09:26:19', NULL),
(3, 'Aelius', 'Hospitaliyiuffiuhishiuusug', 1, 1, '2020-06-08 10:27:42', '2020-06-08 10:27:42', NULL),
(4, 'ffgfdq2423423@', 'fewffwf@$', 1, 1, '2020-06-09 08:08:48', '2020-06-09 08:08:55', '2020-06-09 08:08:55');

-- --------------------------------------------------------

--
-- Table structure for table `buy_plan`
--

CREATE TABLE `buy_plan` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `plan_id` int(11) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `limit` int(11) DEFAULT NULL,
  `payment_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `p_status` int(11) DEFAULT NULL,
  `w_status` int(11) DEFAULT NULL COMMENT '1 for wallet cut and 2 for rezorpay',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `buy_plan`
--

INSERT INTO `buy_plan` (`id`, `user_id`, `plan_id`, `price`, `limit`, `payment_id`, `order_id`, `p_status`, `w_status`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 97, 2, 200, 5, 'null', 'null', NULL, 1, 97, 97, '2020-06-11 10:35:54', '2020-06-11 10:35:54', NULL),
(2, 97, 1, 0, 2, 'null', 'null', NULL, 2, 97, 97, '2020-06-11 10:40:22', '2020-06-11 10:40:22', NULL),
(3, 109, 2, 200, 5, 'pay_FLStia0QpmZKJf', 'pay_FLStia0QpmZKJf', NULL, 2, 109, 109, '2020-08-01 17:26:53', '2020-08-01 17:26:53', NULL),
(4, 386, 1, 0, 2, 'null', 'null', NULL, 2, 386, 386, '2020-11-07 23:35:35', '2020-11-07 23:35:35', NULL),
(5, 419, 1, 0, 2, 'null', 'null', NULL, 2, 419, 419, '2020-11-24 15:41:51', '2020-11-24 15:41:51', NULL),
(6, 434, 1, 0, 2, 'null', 'null', NULL, 2, 434, 434, '2020-12-30 01:17:59', '2020-12-30 01:17:59', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `buy_plan_dr`
--

CREATE TABLE `buy_plan_dr` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `plan_id` int(11) DEFAULT NULL,
  `payable_amount` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `promo_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `discount` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gst_amount` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `final_amount` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gst_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_status` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `buy_plan_dr`
--

INSERT INTO `buy_plan_dr` (`id`, `user_id`, `plan_id`, `payable_amount`, `promo_code`, `discount`, `gst_amount`, `final_amount`, `gst_number`, `payment_id`, `payment_status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, 114, 3, '500', NULL, NULL, '18', '518', '51515151', 'pay_dfjdfjfdfdf', 1, NULL, NULL, NULL),
(3, 142, 4, '18250', 'welcome', '10', '12', '20428.8', 'bn', 'pay_FiSGQgsTsQuQsU', 1, NULL, NULL, NULL),
(16, 136, 4, '1', NULL, NULL, NULL, '1', '', NULL, NULL, '2020-10-17 17:10:51', '2020-10-17 17:10:51', NULL),
(15, 142, 4, '1', NULL, NULL, NULL, '1', '', NULL, NULL, '2020-10-17 14:53:10', '2020-10-17 14:53:10', NULL),
(14, 157, 4, '365', NULL, '0.0', '12', '408.8', '', 'pay_Fpy53JuE9c77ek', 1, '2020-10-17 12:27:11', '2020-10-17 12:27:11', NULL),
(10, 157, 4, '365', NULL, '0.0', '12', '408.8', '45', 'pay_FpFD2uCyIQdfvs', 1, NULL, NULL, NULL),
(11, 142, 4, '1', NULL, NULL, '1', '1', '1', '1', 1, NULL, NULL, NULL),
(12, 142, 4, '1', NULL, NULL, '1', '1', '1', '1', 1, NULL, NULL, NULL),
(13, 114, 2, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `id` int(10) UNSIGNED NOT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country_name` int(11) NOT NULL,
  `state_name` int(11) NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '1' COMMENT '0 for inactive, 1 for active',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id`, `city`, `country_name`, `state_name`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'rajkot 6', 1, 1, 1, 1, 1, '2020-03-28 03:23:00', '2020-05-28 06:17:27', '2020-05-28 06:17:27'),
(2, 'Rajkot', 3, 1, 1, 1, 1, '2020-04-07 01:34:10', '2020-09-10 12:10:34', '2020-09-10 12:10:34'),
(3, 'Ahmedabad', 3, 1, 1, 1, 1, '2020-04-10 06:54:34', '2020-09-10 12:06:55', '2020-09-10 12:06:55'),
(4, 'Jamnagar', 3, 1, 1, 1, 1, '2020-04-10 09:52:30', '2020-09-10 12:09:07', '2020-09-10 12:09:07'),
(5, 'Surat', 3, 1, 1, 1, 1, '2020-04-13 07:04:29', '2020-08-28 19:28:24', '2020-08-28 19:28:24'),
(6, 'Navsari', 3, 1, 1, 1, 1, '2020-04-13 07:05:07', '2020-09-10 12:10:10', '2020-09-10 12:10:10'),
(7, 'Mohali', 3, 2, 1, 1, 1, '2020-05-28 06:17:45', '2020-05-28 06:17:45', NULL),
(8, 'Sasaram', 3, 4, 1, 1, 1, '2020-06-08 09:24:24', '2020-09-10 11:57:42', '2020-09-10 11:57:42'),
(9, 'Surat', 3, 1, 1, 1, 1, '2020-07-16 17:55:06', '2020-09-10 12:10:46', '2020-09-10 12:10:46'),
(10, 'demo', 3, 0, 1, 1, 1, '2020-08-28 16:18:58', '2020-08-28 16:18:58', NULL),
(11, 'rajkot1', 3, 1, 1, 1, 1, '2020-08-28 16:26:39', '2020-08-28 19:28:16', '2020-08-28 19:28:16'),
(12, 'Nerela', 3, 5, 1, 1, 1, '2020-08-28 19:48:34', '2020-08-28 19:48:34', NULL),
(13, 'Burari', 3, 5, 1, 1, 1, '2020-08-28 19:49:10', '2020-08-28 19:49:10', NULL),
(14, 'Timarpur', 3, 5, 1, 1, 1, '2020-08-28 19:49:30', '2020-08-28 19:49:30', NULL),
(15, 'Badli', 3, 5, 1, 1, 1, '2020-08-28 19:50:44', '2020-08-28 19:50:44', NULL),
(16, 'Rithala', 3, 5, 1, 1, 1, '2020-08-28 19:51:02', '2020-08-28 19:51:02', NULL),
(17, 'Bawana', 3, 1, 1, 1, 1, '2020-08-28 19:51:22', '2020-08-28 19:51:41', '2020-08-28 19:51:41'),
(18, 'Bawana', 3, 5, 1, 1, 1, '2020-08-28 19:51:54', '2020-08-28 19:51:54', NULL),
(19, 'Mundka', 3, 5, 1, 1, 1, '2020-08-28 19:52:17', '2020-08-28 19:52:17', NULL),
(20, 'Adarsh Nagar', 3, 5, 1, 1, 1, '2020-08-28 19:54:31', '2020-08-28 19:54:31', NULL),
(21, 'Kirari', 3, 5, 1, 1, 1, '2020-08-28 19:55:03', '2020-08-28 19:55:03', NULL),
(22, 'Sultanpur Majra', 3, 5, 1, 1, 1, '2020-08-28 19:56:01', '2020-08-28 19:56:01', NULL),
(23, 'Nangloi Jat', 3, 5, 1, 1, 1, '2020-08-28 19:56:17', '2020-08-28 19:56:17', NULL),
(24, 'Mangol Puri', 3, 5, 1, 1, 1, '2020-08-28 19:56:33', '2020-08-28 19:56:33', NULL),
(25, 'Rohini', 3, 5, 1, 1, 1, '2020-08-28 19:56:55', '2020-08-28 19:56:55', NULL),
(26, 'Shalimar Bagh', 3, 5, 1, 1, 1, '2020-08-28 19:57:18', '2020-08-28 19:57:18', NULL),
(27, 'Shakur Basti', 3, 5, 1, 1, 1, '2020-08-28 19:57:34', '2020-08-28 19:57:34', NULL),
(28, 'Tri Nagar', 3, 5, 1, 1, 1, '2020-08-28 19:57:55', '2020-08-28 19:57:55', NULL),
(29, 'Wazirpur', 3, 5, 1, 1, 1, '2020-08-28 19:58:18', '2020-08-28 19:58:18', NULL),
(30, 'Model Town', 3, 5, 1, 1, 1, '2020-08-28 19:59:22', '2020-08-28 19:59:22', NULL),
(31, 'Sadar Bazar', 3, 5, 1, 1, 1, '2020-08-28 20:00:25', '2020-08-28 20:00:25', NULL),
(32, 'Chandni Chowk', 3, 5, 1, 1, 1, '2020-08-28 20:00:46', '2020-08-28 20:00:46', NULL),
(33, 'Matia Mahal', 3, 5, 1, 1, 1, '2020-08-28 20:01:05', '2020-08-28 20:01:05', NULL),
(34, 'Ballimaran', 3, 5, 1, 1, 1, '2020-08-28 20:01:26', '2020-08-28 20:01:26', NULL),
(35, 'Karol Bagh', 3, 5, 1, 1, 1, '2020-08-28 20:01:43', '2020-08-28 20:01:43', NULL),
(36, 'Patel Nagar', 3, 5, 1, 1, 1, '2020-08-28 20:02:00', '2020-08-28 20:02:00', NULL),
(37, 'Moti Nagar', 3, 5, 1, 1, 1, '2020-08-28 20:02:19', '2020-08-28 20:02:19', NULL),
(38, 'Madipur', 3, 5, 1, 1, 1, '2020-08-28 20:02:35', '2020-08-28 20:02:35', NULL),
(39, 'Rajouri Garden', 3, 5, 1, 1, 1, '2020-08-28 20:02:53', '2020-08-28 20:02:53', NULL),
(40, 'Hari Nagar', 3, 5, 1, 1, 1, '2020-08-28 20:03:13', '2020-08-28 20:03:13', NULL),
(41, 'Tilak Nagar', 3, 5, 1, 1, 1, '2020-08-28 20:03:34', '2020-08-28 20:03:34', NULL),
(42, 'Janakpuri', 3, 5, 1, 1, 1, '2020-08-28 20:03:51', '2020-08-28 20:03:51', NULL),
(43, 'Vikaspuri', 3, 5, 1, 1, 1, '2020-08-28 20:04:07', '2020-08-28 20:04:07', NULL),
(44, 'Uttam Nagar', 3, 5, 1, 1, 1, '2020-08-28 20:04:35', '2020-08-28 20:04:35', NULL),
(45, 'Dwarka', 3, 5, 1, 1, 1, '2020-08-28 20:04:51', '2020-08-28 20:04:51', NULL),
(46, 'Matiala', 3, 5, 1, 1, 1, '2020-08-28 20:05:11', '2020-08-28 20:05:11', NULL),
(47, 'Najafgarh', 3, 5, 1, 1, 1, '2020-08-28 20:05:33', '2020-08-28 20:05:33', NULL),
(48, 'Bijwasan', 3, 5, 1, 1, 1, '2020-08-28 20:05:50', '2020-08-28 20:05:50', NULL),
(49, 'Palam', 3, 5, 1, 1, 1, '2020-08-28 20:06:05', '2020-08-28 20:06:05', NULL),
(50, 'Delhi Cantonment', 3, 5, 1, 1, 1, '2020-08-28 20:06:25', '2020-08-28 20:06:25', NULL),
(51, 'Rajinder Nagar', 3, 5, 1, 1, 1, '2020-08-28 20:06:40', '2020-08-28 20:06:40', NULL),
(52, 'New Delhi', 3, 5, 1, 1, 1, '2020-08-28 20:07:00', '2020-08-28 20:07:00', NULL),
(53, 'Jangpura', 3, 5, 1, 1, 1, '2020-08-28 20:07:21', '2020-08-28 20:07:21', NULL),
(54, 'Kasturba Nagar', 3, 5, 1, 1, 1, '2020-08-28 20:07:40', '2020-08-28 20:07:40', NULL),
(55, 'Malviya Nagar', 3, 5, 1, 1, 1, '2020-08-28 20:07:59', '2020-08-28 20:07:59', NULL),
(56, 'R K Puram', 3, 5, 1, 1, 1, '2020-08-28 20:08:14', '2020-08-28 20:08:14', NULL),
(57, 'Mehrauli', 3, 5, 1, 1, 1, '2020-08-28 20:08:31', '2020-08-28 20:08:31', NULL),
(58, 'Chhatarpur', 3, 5, 1, 1, 1, '2020-08-28 20:08:46', '2020-08-28 20:08:46', NULL),
(59, 'Deoli', 3, 5, 1, 1, 1, '2020-08-28 20:09:04', '2020-08-28 20:09:04', NULL),
(60, 'Ambedkar Nagar', 3, 5, 1, 1, 1, '2020-08-28 20:09:26', '2020-08-28 20:09:26', NULL),
(61, 'Sangam Vihar', 3, 5, 1, 1, 1, '2020-08-28 20:09:43', '2020-08-28 20:09:43', NULL),
(62, 'Greater Kailash', 3, 5, 1, 1, 1, '2020-08-28 20:10:08', '2020-08-28 20:10:08', NULL),
(63, 'Kalkaji', 3, 5, 1, 1, 1, '2020-08-28 20:10:25', '2020-08-28 20:10:25', NULL),
(64, 'Tughlkabad', 3, 5, 1, 1, 1, '2020-08-28 20:10:43', '2020-08-28 20:10:43', NULL),
(65, 'Badarpur', 3, 5, 1, 1, 1, '2020-08-28 20:11:01', '2020-08-28 20:11:01', NULL),
(66, 'Okhla', 3, 5, 1, 1, 1, '2020-08-28 20:11:17', '2020-08-28 20:11:17', NULL),
(67, 'Trilokpuri', 3, 5, 1, 1, 1, '2020-08-28 20:11:34', '2020-08-28 20:11:34', NULL),
(68, 'Kondli', 3, 5, 1, 1, 1, '2020-08-28 20:11:47', '2020-08-28 20:11:47', NULL),
(69, 'Patparganj', 3, 5, 1, 1, 1, '2020-08-28 20:12:04', '2020-08-28 20:12:04', NULL),
(70, 'Laxmi Nagar', 3, 5, 1, 1, 1, '2020-08-28 20:12:20', '2020-08-28 20:12:20', NULL),
(71, 'Vishwas Nagar', 3, 5, 1, 1, 1, '2020-08-28 20:12:44', '2020-08-28 20:12:44', NULL),
(72, 'Krishna Nagar', 3, 5, 1, 1, 1, '2020-08-28 20:12:58', '2020-08-28 20:12:58', NULL),
(73, 'Gandhi Nagar', 3, 5, 1, 1, 1, '2020-08-28 20:13:17', '2020-08-28 20:13:17', NULL),
(74, 'Shahdara', 3, 5, 1, 1, 1, '2020-08-28 20:13:32', '2020-08-28 20:13:32', NULL),
(75, 'Seemapuri', 3, 5, 1, 1, 1, '2020-08-28 20:13:46', '2020-08-28 20:13:46', NULL),
(76, 'Rohtas Nagar', 3, 5, 1, 1, 1, '2020-08-28 20:14:01', '2020-08-28 20:14:01', NULL),
(77, 'Seelampur', 3, 5, 1, 1, 1, '2020-08-28 20:14:19', '2020-08-28 20:14:19', NULL),
(78, 'Ghonda', 3, 5, 1, 1, 1, '2020-08-28 20:14:41', '2020-08-28 20:14:41', NULL),
(79, 'Babarpur', 3, 5, 1, 1, 1, '2020-08-28 20:15:00', '2020-08-28 20:15:00', NULL),
(80, 'Gokalpur', 3, 5, 1, 1, 1, '2020-08-28 20:15:20', '2020-08-28 20:15:20', NULL),
(81, 'Mustafabad', 3, 5, 1, 1, 1, '2020-08-28 20:15:37', '2020-08-28 20:15:37', NULL),
(82, 'Karawal Nagar', 3, 5, 1, 1, 1, '2020-08-28 20:15:54', '2020-08-28 20:15:54', NULL),
(97, 'Adilabad', 3, 26, 1, 1, 1, '2020-09-01 10:35:26', '2020-09-01 10:35:26', NULL),
(98, 'Armur', 3, 26, 1, 1, 1, '2020-09-01 10:35:46', '2020-09-01 10:35:46', NULL),
(99, 'Asifabad', 3, 26, 1, 1, 1, '2020-09-01 10:36:09', '2020-09-01 10:36:09', NULL),
(100, 'Bellampalle', 3, 26, 1, 1, 1, '2020-09-01 10:36:44', '2020-09-01 10:36:44', NULL),
(101, 'Bhadrachalam', 3, 26, 1, 1, 1, '2020-09-01 10:37:10', '2020-09-01 10:37:10', NULL),
(102, 'Bhongir', 3, 26, 1, 1, 1, '2020-09-01 10:37:33', '2020-09-01 10:37:33', NULL),
(103, 'Bhupalpally', 3, 26, 1, 1, 1, '2020-09-01 10:37:57', '2020-09-01 10:37:57', NULL),
(104, 'Bodhan', 3, 26, 1, 1, 1, '2020-09-01 10:38:20', '2020-09-01 10:38:20', NULL),
(105, 'Gadwal', 3, 26, 1, 1, 1, '2020-09-01 10:38:47', '2020-09-01 10:38:47', NULL),
(106, 'Hyderabad', 3, 26, 1, 1, 1, '2020-09-01 10:39:23', '2020-09-01 10:39:23', NULL),
(107, 'Jagtial', 3, 26, 1, 1, 1, '2020-09-01 10:39:47', '2020-09-01 10:39:47', NULL),
(108, 'Jangaon', 3, 26, 1, 1, 1, '2020-09-01 10:40:14', '2020-09-01 10:40:14', NULL),
(109, 'Kagaznagar', 3, 26, 1, 1, 1, '2020-09-01 10:40:42', '2020-09-01 10:40:42', NULL),
(110, 'Kamareddy', 3, 26, 1, 1, 1, '2020-09-01 10:41:10', '2020-09-01 10:41:10', NULL),
(111, 'Karimnagar', 3, 26, 1, 1, 1, '2020-09-01 10:41:32', '2020-09-01 10:41:32', NULL),
(112, 'Khammam', 3, 26, 1, 1, 1, '2020-09-01 10:41:56', '2020-09-01 10:41:56', NULL),
(113, 'Khanapuram Haveli', 3, 26, 1, 1, 1, '2020-09-01 10:42:20', '2020-09-01 10:42:20', NULL),
(114, 'Kodad', 3, 26, 1, 1, 1, '2020-09-01 10:42:41', '2020-09-01 10:42:41', NULL),
(115, 'Koratla', 3, 26, 1, 1, 1, '2020-09-01 10:43:11', '2020-09-01 10:43:11', NULL),
(116, 'Kothagudem', 3, 26, 1, 1, 1, '2020-09-01 10:43:33', '2020-09-01 10:43:33', NULL),
(117, 'Mahabubabad', 3, 26, 1, 1, 1, '2020-09-01 10:44:10', '2020-09-01 10:44:10', NULL),
(118, 'Mahabubnagar', 3, 26, 1, 1, 1, '2020-09-01 10:44:38', '2020-09-01 10:44:38', NULL),
(119, 'Mancherial', 3, 26, 1, 1, 1, '2020-09-01 10:44:59', '2020-09-01 10:44:59', NULL),
(120, 'Mandamarri', 3, 26, 1, 1, 1, '2020-09-01 10:45:21', '2020-09-01 10:45:21', NULL),
(121, 'Medak', 3, 26, 1, 1, 1, '2020-09-01 10:45:47', '2020-09-01 10:45:47', NULL),
(122, 'Medchal', 3, 26, 1, 1, 1, '2020-09-01 10:46:14', '2020-09-01 10:46:14', NULL),
(123, 'Metpalle', 3, 26, 1, 1, 1, '2020-09-01 10:46:38', '2020-09-01 10:46:38', NULL),
(124, 'Mulugu', 3, 26, 1, 1, 1, '2020-09-01 10:47:33', '2020-09-01 10:47:33', NULL),
(125, 'Nagar Kurnool', 3, 26, 1, 1, 1, '2020-09-01 10:47:57', '2020-09-01 10:47:57', NULL),
(126, 'Nalgonda', 3, 26, 1, 1, 1, '2020-09-01 10:48:23', '2020-09-01 10:48:23', NULL),
(127, 'Narayanpet', 3, 26, 1, 1, 1, '2020-09-01 10:48:49', '2020-09-01 10:48:49', NULL),
(128, 'Nirmal', 3, 26, 1, 1, 1, '2020-09-01 10:49:10', '2020-09-01 10:49:10', NULL),
(129, 'Nizamabad', 3, 26, 1, 1, 1, '2020-09-01 10:49:33', '2020-09-01 10:49:33', NULL),
(130, 'Palwancha', 3, 26, 1, 1, 1, '2020-09-01 10:49:54', '2020-09-01 10:49:54', NULL),
(131, 'Peddapalli', 3, 26, 1, 1, 1, '2020-09-01 10:50:19', '2020-09-01 10:50:19', NULL),
(132, 'Ramagundam', 3, 26, 1, 1, 1, '2020-09-01 10:50:41', '2020-09-01 10:50:41', NULL),
(133, 'Sangareddy', 3, 26, 1, 1, 1, '2020-09-01 10:51:02', '2020-09-01 10:51:02', NULL),
(134, 'Shamshabad', 3, 26, 1, 1, 1, '2020-09-01 10:51:27', '2020-09-01 10:51:27', NULL),
(135, 'Siddipet', 3, 26, 1, 1, 1, '2020-09-01 10:51:51', '2020-09-01 10:51:51', NULL),
(136, 'Sircilla', 3, 26, 1, 1, 1, '2020-09-01 10:52:19', '2020-09-01 10:52:19', NULL),
(137, 'Suryapet', 3, 26, 1, 1, 1, '2020-09-01 10:52:54', '2020-09-01 10:52:54', NULL),
(138, 'Tandur', 3, 26, 1, 1, 1, '2020-09-01 10:53:16', '2020-09-01 10:53:16', NULL),
(139, 'Vikarabad', 3, 26, 1, 1, 1, '2020-09-01 10:53:40', '2020-09-01 10:53:40', NULL),
(140, 'Wanaparthy', 3, 26, 1, 1, 1, '2020-09-01 10:54:16', '2020-09-01 10:54:16', NULL),
(141, 'Warangal', 3, 26, 1, 1, 1, '2020-09-01 10:54:41', '2020-09-01 10:54:41', NULL),
(142, 'Zahirabad', 3, 26, 1, 1, 1, '2020-09-01 10:55:03', '2020-09-01 10:55:03', NULL),
(143, 'Vadodara', 3, 1, 1, 1, 1, '2020-09-01 11:03:01', '2020-09-10 12:11:11', '2020-09-10 12:11:11'),
(144, 'Bhavnagar', 3, 1, 1, 1, 1, '2020-09-01 11:03:22', '2020-09-10 12:07:45', '2020-09-10 12:07:45'),
(145, 'Junagadh', 3, 1, 1, 1, 1, '2020-09-01 11:03:46', '2020-09-10 12:09:26', '2020-09-10 12:09:26'),
(146, 'Anand', 3, 1, 1, 1, 1, '2020-09-01 11:04:05', '2020-09-10 12:07:11', '2020-09-10 12:07:11'),
(147, 'Morbi', 3, 1, 1, 1, 1, '2020-09-01 11:04:26', '2020-09-10 12:09:37', '2020-09-10 12:09:37'),
(148, 'Gandhinagar', 3, 1, 1, 1, 1, '2020-09-01 11:04:46', '2020-09-10 12:08:17', '2020-09-10 12:08:17'),
(149, 'Gandhidham', 3, 26, 1, 1, 1, '2020-09-01 11:06:19', '2020-09-01 11:06:19', NULL),
(150, 'Nadiad', 3, 1, 1, 1, 1, '2020-09-01 11:06:38', '2020-09-10 12:09:55', '2020-09-10 12:09:55'),
(151, 'Surendranagar', 3, 1, 1, 1, 1, '2020-09-01 11:07:17', '2020-09-10 12:10:59', '2020-09-10 12:10:59'),
(152, 'Bharuch', 3, 1, 1, 1, 1, '2020-09-01 11:07:38', '2020-09-10 12:07:24', '2020-09-10 12:07:24'),
(153, 'Daman, U.T.', 3, 1, 1, 1, 1, '2020-09-01 11:08:10', '2020-09-10 12:07:58', '2020-09-10 12:07:58'),
(154, 'Porbandar', 3, 1, 1, 1, 1, '2020-09-01 11:08:37', '2020-09-10 12:10:21', '2020-09-10 12:10:21'),
(155, 'Gir Somnath', 3, 1, 1, 1, 1, '2020-09-01 11:09:04', '2020-09-10 12:08:43', '2020-09-10 12:08:43'),
(156, 'Bicholim', 3, 10, 1, 1, 1, '2020-09-01 11:13:01', '2020-09-10 12:01:23', '2020-09-10 12:01:23'),
(157, 'Canacona', 3, 10, 1, 1, 1, '2020-09-01 11:13:28', '2020-09-10 12:01:41', '2020-09-10 12:01:41'),
(158, 'Cuncolim', 3, 10, 1, 1, 1, '2020-09-01 11:13:55', '2020-09-10 12:01:52', '2020-09-10 12:01:52'),
(159, 'Curchorem', 3, 10, 1, 1, 1, '2020-09-01 11:14:22', '2020-09-10 12:02:07', '2020-09-10 12:02:07'),
(160, 'Mapusa', 3, 10, 1, 1, 1, '2020-09-01 11:14:50', '2020-09-10 12:02:42', '2020-09-10 12:02:42'),
(161, 'Margao', 3, 10, 1, 1, 1, '2020-09-01 11:15:13', '2020-09-10 12:02:55', '2020-09-10 12:02:55'),
(162, 'Mormugao', 3, 10, 1, 1, 1, '2020-09-01 11:15:49', '2020-09-10 12:03:11', '2020-09-10 12:03:11'),
(163, 'Panaji', 3, 10, 1, 1, 1, '2020-09-01 11:16:13', '2020-09-10 12:03:22', '2020-09-10 12:03:22'),
(164, 'Pernem', 3, 10, 1, 1, 1, '2020-09-01 11:16:39', '2020-09-10 12:01:09', '2020-09-10 12:01:09'),
(165, 'Ponda', 3, 10, 1, 1, 1, '2020-09-01 11:17:03', '2020-09-10 12:03:34', '2020-09-10 12:03:34'),
(166, 'Quepem', 3, 10, 1, 1, 1, '2020-09-01 11:17:26', '2020-09-10 12:03:46', '2020-09-10 12:03:46'),
(167, 'Sanguem', 3, 10, 1, 1, 1, '2020-09-01 11:17:51', '2020-09-10 12:03:57', '2020-09-10 12:03:57'),
(168, 'Sanquelim', 3, 10, 1, 1, 1, '2020-09-01 11:18:13', '2020-09-10 12:04:12', '2020-09-10 12:04:12'),
(169, 'Valpoi', 3, 10, 1, 1, 1, '2020-09-01 11:18:34', '2020-09-10 12:04:24', '2020-09-10 12:04:24'),
(170, 'Kosli', 3, 11, 1, 1, 1, '2020-09-01 14:11:37', '2020-09-10 12:13:16', '2020-09-10 12:13:16'),
(171, 'Faridabad', 3, 11, 1, 1, 1, '2020-09-01 14:12:01', '2020-09-10 12:14:31', '2020-09-10 12:14:31'),
(172, 'Gurugram', 3, 11, 1, 1, 1, '2020-09-01 14:12:22', '2020-09-10 12:14:43', '2020-09-10 12:14:43'),
(173, 'Panipat', 3, 11, 1, 1, 1, '2020-09-01 14:12:43', '2020-09-10 12:16:06', '2020-09-10 12:16:06'),
(174, 'Ambala', 3, 11, 1, 1, 1, '2020-09-01 14:13:06', '2020-09-10 12:13:33', '2020-09-10 12:13:33'),
(175, 'Yamunanagar', 3, 11, 1, 1, 1, '2020-09-01 14:13:33', '2020-09-10 12:17:50', '2020-09-10 12:17:50'),
(176, 'Rohtak', 3, 11, 1, 1, 1, '2020-09-01 14:13:59', '2020-09-10 12:16:45', '2020-09-10 12:16:45'),
(177, 'Hisar', 3, 11, 1, 1, 1, '2020-09-01 14:14:20', '2020-09-10 12:14:55', '2020-09-10 12:14:55'),
(178, 'Karnal', 3, 11, 1, 1, 1, '2020-09-01 14:14:46', '2020-09-10 12:15:33', '2020-09-10 12:15:33'),
(179, 'Sonipat', 3, 11, 1, 1, 1, '2020-09-01 14:15:11', '2020-09-10 12:17:27', '2020-09-10 12:17:27'),
(180, 'Panchkula', 3, 11, 1, 1, 1, '2020-09-01 14:15:46', '2020-09-10 12:15:55', '2020-09-10 12:15:55'),
(181, 'Bhiwani', 3, 11, 1, 1, 1, '2020-09-01 14:16:10', '2020-09-10 12:14:18', '2020-09-10 12:14:18'),
(182, 'Sirsa', 3, 11, 1, 1, 1, '2020-09-01 14:16:38', '2020-09-10 12:17:12', '2020-09-10 12:17:12'),
(183, 'Bahadurgarh', 3, 11, 1, 1, 1, '2020-09-01 14:17:03', '2020-09-10 12:13:59', '2020-09-10 12:13:59'),
(184, 'Jind', 3, 11, 1, 1, 1, '2020-09-01 14:17:42', '2020-09-10 12:15:08', '2020-09-10 12:15:08'),
(185, 'Thanesar', 3, 11, 1, 1, 1, '2020-09-01 14:18:07', '2020-09-10 12:17:38', '2020-09-10 12:17:38'),
(186, 'Kaithal', 3, 11, 1, 1, 1, '2020-09-01 14:18:31', '2020-09-10 12:15:21', '2020-09-10 12:15:21'),
(187, 'Rewari', 3, 11, 1, 1, 1, '2020-09-01 14:18:56', '2020-09-10 12:16:34', '2020-09-10 12:16:34'),
(188, 'Palwal', 3, 11, 1, 1, 1, '2020-09-01 14:19:25', '2020-09-10 12:15:44', '2020-09-10 12:15:44'),
(189, 'Pundri', 3, 11, 1, 1, 1, '2020-09-01 14:19:47', '2020-09-10 12:16:20', '2020-09-10 12:16:20'),
(190, 'Silchar Part-X', 3, 35, 1, 1, 1, '2020-09-01 17:11:46', '2020-09-01 17:16:27', '2020-09-01 17:16:27'),
(191, 'Sonari', 3, 35, 1, 1, 1, '2020-09-01 17:12:04', '2020-09-01 17:16:35', '2020-09-01 17:16:35'),
(192, 'Sualkuchi', 3, 35, 1, 1, 1, '2020-09-01 17:12:25', '2020-09-01 17:16:45', '2020-09-01 17:16:45'),
(193, 'Tangla', 3, 35, 1, 1, 1, '2020-09-01 17:12:46', '2020-09-01 17:16:56', '2020-09-01 17:16:56'),
(194, 'Tezpur', 3, 35, 1, 1, 1, '2020-09-01 17:13:04', '2020-09-01 17:17:04', '2020-09-01 17:17:04'),
(195, 'Tihu', 3, 35, 1, 1, 1, '2020-09-01 17:13:26', '2020-09-01 17:17:11', '2020-09-01 17:17:11'),
(196, 'Tinsukia', 3, 35, 1, 1, 1, '2020-09-01 17:13:47', '2020-09-01 17:17:20', '2020-09-01 17:17:20'),
(197, 'Titabor Town', 3, 35, 1, 1, 1, '2020-09-01 17:14:08', '2020-09-01 17:17:27', '2020-09-01 17:17:27'),
(198, 'Udalguri', 3, 35, 1, 1, 1, '2020-09-01 17:14:26', '2020-09-01 17:17:34', '2020-09-01 17:17:34'),
(199, 'Umrangso', 3, 35, 1, 1, 1, '2020-09-01 17:14:45', '2020-09-01 17:16:15', '2020-09-01 17:16:15'),
(200, 'Uttar Krishnapur', 3, 35, 1, 1, 1, '2020-09-01 17:15:09', '2020-09-01 17:17:42', '2020-09-01 17:17:42'),
(201, 'Bakultala', 3, 35, 1, 1, 1, '2020-09-01 17:23:46', '2020-09-01 17:23:46', NULL),
(202, 'Bambooflat (Bombooflat)', 3, 35, 1, 1, 1, '2020-09-01 17:24:51', '2020-09-01 17:24:51', NULL),
(203, 'Garacharma', 3, 35, 1, 1, 1, '2020-09-01 17:25:20', '2020-09-01 17:25:20', NULL),
(204, 'Port Blair', 3, 35, 1, 1, 1, '2020-09-01 17:25:39', '2020-09-01 17:25:39', NULL),
(205, 'Prothrapur', 3, 35, 1, 1, 1, '2020-09-01 17:26:03', '2020-09-01 17:26:03', NULL),
(206, 'Adoni', 3, 6, 1, 1, 1, '2020-09-01 17:29:15', '2020-09-01 17:29:15', NULL),
(207, 'Anantapuram', 3, 6, 1, 1, 1, '2020-09-01 17:29:36', '2020-09-01 17:29:36', NULL),
(208, 'Bhimavaram', 3, 6, 1, 1, 1, '2020-09-01 17:29:52', '2020-09-01 17:29:52', NULL),
(209, 'Chilakaluripet', 3, 6, 1, 1, 1, '2020-09-01 17:30:13', '2020-09-01 17:30:13', NULL),
(210, 'Chirala', 3, 6, 1, 1, 1, '2020-09-01 17:30:38', '2020-09-01 17:30:38', NULL),
(211, 'Chittoor', 3, 6, 1, 1, 1, '2020-09-01 17:32:16', '2020-09-01 17:32:16', NULL),
(212, 'Dharmavaram', 3, 6, 1, 1, 1, '2020-09-01 17:32:44', '2020-09-01 17:32:44', NULL),
(213, 'Eluru', 3, 6, 1, 1, 1, '2020-09-01 17:33:06', '2020-09-01 17:33:06', NULL),
(214, 'Gudivada', 3, 6, 1, 1, 1, '2020-09-01 17:33:45', '2020-09-01 17:33:45', NULL),
(215, 'Guntakal', 3, 6, 1, 1, 1, '2020-09-01 17:34:08', '2020-09-01 17:34:08', NULL),
(216, 'Guntur', 3, 6, 1, 1, 1, '2020-09-01 17:34:41', '2020-09-01 17:34:41', NULL),
(217, 'Hindupur', 3, 6, 1, 1, 1, '2020-09-01 17:35:22', '2020-09-01 17:35:22', NULL),
(218, 'Kadapa', 3, 6, 1, 1, 1, '2020-09-01 17:35:53', '2020-09-01 17:35:53', NULL),
(219, 'Kakinada', 3, 6, 1, 1, 1, '2020-09-01 17:36:33', '2020-09-01 17:36:33', NULL),
(220, 'Kurnool', 3, 6, 1, 1, 1, '2020-09-01 17:37:12', '2020-09-01 17:37:12', NULL),
(221, 'Machilipatnam', 3, 6, 1, 1, 1, '2020-09-01 17:37:45', '2020-09-01 17:37:45', NULL),
(222, 'Madanapalle', 3, 6, 1, 1, 1, '2020-09-01 17:38:09', '2020-09-01 17:38:09', NULL),
(223, 'Mangalagiri', 3, 6, 1, 1, 1, '2020-09-01 17:38:41', '2020-09-01 17:38:41', NULL),
(224, 'Nandyal', 3, 6, 1, 1, 1, '2020-09-01 17:39:05', '2020-09-01 17:39:05', NULL),
(225, 'Narasaraopet', 3, 6, 1, 1, 1, '2020-09-01 17:39:35', '2020-09-01 17:39:35', NULL),
(226, 'Nellore', 3, 6, 1, 1, 1, '2020-09-01 17:41:43', '2020-09-01 17:41:43', NULL),
(227, 'Ongole', 3, 6, 1, 1, 1, '2020-09-01 17:45:35', '2020-09-01 17:45:35', NULL),
(228, 'Proddatur', 3, 6, 1, 1, 1, '2020-09-01 17:45:58', '2020-09-01 17:45:58', NULL),
(229, 'Rajamahendravaram', 3, 6, 1, 1, 1, '2020-09-01 17:46:37', '2020-09-01 17:46:37', NULL),
(230, 'Rajamahendravaram', 3, 6, 1, 1, 1, '2020-09-01 17:47:33', '2020-09-01 17:47:33', NULL),
(231, 'Srikakulam', 3, 6, 1, 1, 1, '2020-09-01 17:47:51', '2020-09-01 17:47:51', NULL),
(232, 'Tadepalligudem', 3, 6, 1, 1, 1, '2020-09-01 17:48:22', '2020-09-01 17:48:22', NULL),
(233, 'Tadipatri', 3, 6, 1, 1, 1, '2020-09-01 17:53:55', '2020-09-01 17:53:55', NULL),
(234, 'Tenali', 3, 6, 1, 1, 1, '2020-09-01 17:54:20', '2020-09-01 17:54:20', NULL),
(235, 'Tirupati', 3, 6, 1, 1, 1, '2020-09-01 17:54:38', '2020-09-01 17:54:38', NULL),
(236, 'Vijayawada', 3, 6, 1, 1, 1, '2020-09-01 17:54:55', '2020-09-01 17:54:55', NULL),
(237, 'Visakhapatnam', 3, 6, 1, 1, 1, '2020-09-01 17:55:12', '2020-09-01 17:55:12', NULL),
(238, 'Vizianagaram', 3, 6, 1, 1, 1, '2020-09-01 17:55:32', '2020-09-01 17:55:32', NULL),
(239, 'Yemmiganur', 3, 6, 1, 1, 1, '2020-09-01 17:55:51', '2020-09-01 17:55:51', NULL),
(240, 'Along', 3, 36, 1, 1, 1, '2020-09-01 17:58:07', '2020-09-01 17:58:07', NULL),
(241, 'Basar', 3, 36, 1, 1, 1, '2020-09-01 17:58:33', '2020-09-01 17:58:33', NULL),
(242, 'Bomdila', 3, 36, 1, 1, 1, '2020-09-01 17:58:58', '2020-09-01 17:58:58', NULL),
(243, 'Changlang', 3, 36, 1, 1, 1, '2020-09-01 17:59:21', '2020-09-01 17:59:21', NULL),
(244, 'Daporijo', 3, 36, 1, 1, 1, '2020-09-01 17:59:42', '2020-09-01 17:59:42', NULL),
(245, 'Deomali', 3, 36, 1, 1, 1, '2020-09-01 18:00:05', '2020-09-01 18:00:05', NULL),
(246, 'Itanagar', 3, 36, 1, 1, 1, '2020-09-01 18:00:41', '2020-09-01 18:00:41', NULL),
(247, 'Jairampur', 3, 36, 1, 1, 1, '2020-09-01 18:01:12', '2020-09-01 18:01:12', NULL),
(248, 'Khonsa', 3, 36, 1, 1, 1, '2020-09-01 18:01:34', '2020-09-01 18:01:34', NULL),
(249, 'Naharlagun', 3, 36, 1, 1, 1, '2020-09-01 18:02:00', '2020-09-01 18:02:00', NULL),
(250, 'Namsai', 3, 36, 1, 1, 1, '2020-09-01 18:02:52', '2020-09-01 18:02:52', NULL),
(251, 'Pasighat', 3, 36, 1, 1, 1, '2020-09-01 18:03:15', '2020-09-01 18:03:15', NULL),
(252, 'Roing', 3, 36, 1, 1, 1, '2020-09-01 18:03:37', '2020-09-01 18:03:37', NULL),
(253, 'Seppa', 3, 36, 1, 1, 1, '2020-09-01 18:04:36', '2020-09-01 18:04:36', NULL),
(254, 'Tawang', 3, 36, 1, 1, 1, '2020-09-01 18:05:00', '2020-09-01 18:05:00', NULL),
(255, 'Tezu', 3, 36, 1, 1, 1, '2020-09-01 18:05:22', '2020-09-01 18:05:22', NULL),
(256, 'Ziro', 3, 36, 1, 1, 1, '2020-09-01 18:05:50', '2020-09-01 18:05:50', NULL),
(257, 'Abhayapuri', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(258, 'Ambikapur Part-X', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(259, 'Amguri', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(260, 'Anand Nagar', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(261, 'Badarpur', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(262, 'Badarpur Rly Town', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(263, 'Bahbari Gaon', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(264, 'Bamun Sualkuchi', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(265, 'Barbari (AMC Area)', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(266, 'Barpathar', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(267, 'Barpeta', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(268, 'Barpeta Road', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(269, 'Basugaon', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(270, 'Bihpuria', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(271, 'Bijni', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(272, 'Bilasipara', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(273, 'Biswanath Chariali', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(274, 'Bohari', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(275, 'Bokajan', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(276, 'Bokakhat', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(277, 'Bongaigaon', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(278, 'Bongaigaon', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(279, 'Borgolai Grant No.11', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(280, 'Chabua', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(281, 'Chandrapur Bagicha', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(282, 'Chapar', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(283, 'Chekonidhara', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(284, 'Choto Haibor', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(285, 'Dergaon', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(286, 'Dharapur', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(287, 'Dhekiajuli', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(288, 'Dhemaji', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(289, 'Dhing', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(290, 'Dhubri', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(291, 'Dibrugarh', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(292, 'Digboi', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(293, 'Digboi Oil Town', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(294, 'Dimaruguri', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(295, 'Diphu', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(296, 'Doboka', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(297, 'Dokmoka', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(298, 'Donkamokam', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(299, 'Doom Dooma', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(300, 'Duliajan No.1', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(301, 'Duliajan Oil Town', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(302, 'Durga Nagar Part-V', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(303, 'Gauripur', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(304, 'Goalpara', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 12:02:26', '2020-09-10 12:02:26'),
(305, 'Gohpur', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(306, 'Golaghat', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(307, 'Golokganj', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(308, 'Gossaigaon', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(309, 'Guwahati', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(310, 'Haflong', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(311, 'Hailakandi', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(312, 'Hamren', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(313, 'HPCL Township', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(314, 'Hojai', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(315, 'Howli', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(316, 'Howraghat', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(317, 'Jagiroad', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(318, 'Jonai Bazar', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(319, 'Jorhat', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(320, 'Kampur Town', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(321, 'Kanakpur Part-II', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(322, 'Karimganj', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(323, 'Kharijapikon', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(324, 'Kharupatia', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(325, 'Kochpara', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(326, 'Kokrajhar', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(327, 'Kumar Kaibarta Gaon', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(328, 'Lakhipur', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(329, 'Lakhipur', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(330, 'Lala', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(331, 'Lanka', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(332, 'Lido Tikok', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(333, 'Lido Town', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(334, 'Lumding', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(335, 'Lumding Rly Colony', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(336, 'Mahur', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(337, 'Maibong', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(338, 'Majgaon', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(339, 'Makum', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(340, 'Mangaldoi', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(341, 'Mankachar', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(342, 'Margherita', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(343, 'Mariani', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(344, 'Marigaon', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(345, 'Moran Town', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(346, 'Moranhat', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(347, 'Nagaon', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(348, 'Naharkatiya', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(349, 'Nalbari', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(350, 'Namrup', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(351, 'Naubaisa Gaon', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(352, 'Nazira', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(353, 'New Bongaigaon Rly. Colony', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(354, 'North Guwahati', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(355, 'North Lakhimpur', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(356, 'Numaligarh Refinery Township', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(357, 'Palasbari', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(358, 'Pathsala', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(359, 'Rangapara', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(360, 'Rangia', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(361, 'Salakati', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(362, 'Sapatgram', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(363, 'Sarbhog', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(364, 'Sarthebari', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(365, 'Sarupathar', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(366, 'Sarupathar Bengali', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(367, 'Senchoa Gaon', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(368, 'Sibsagar', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(369, 'Silapathar', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(370, 'Silchar', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(371, 'Silchar Part-X', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(372, 'Sonari', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(373, 'Sualkuchi', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(374, 'Tangla', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(375, 'Tezpur', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(376, 'Tihu', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(377, 'Tinsukia', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(378, 'Titabor Town', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(379, 'Udalguri', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(380, 'Umrangso', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(381, 'Uttar Krishnapur', 3, 7, 1, 1, 1, '2020-09-10 11:56:03', '2020-09-10 11:56:03', NULL),
(382, 'Amarpur', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 12:13:48', '2020-09-10 12:13:48'),
(383, 'Araria', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 12:16:56', '2020-09-10 12:16:56'),
(384, 'Areraj', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(385, 'Arrah', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(386, 'Asarganj', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(387, 'Aurangabad', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(388, 'Bagaha', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(389, 'Bahadurganj', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(390, 'Bairgania', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(391, 'Bakhtiarpur', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(392, 'Banka', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(393, 'Banmankhi Bazar', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(394, 'Barahiya', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(395, 'Barauli', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(396, 'Barauni IOC Township', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(397, 'Barbigha', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(398, 'Barh', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(399, 'Begusarai', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(400, 'Behea', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(401, 'Belsand', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(402, 'Bettiah', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(403, 'Bhabua', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(404, 'Bhagalpur (M.Corp)', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(405, 'Bihar', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(406, 'Bikramganj', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(407, 'Birpur', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(408, 'Bodh Gaya', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(409, 'Buxar', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(410, 'Chakia', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(411, 'Chanpatia', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(412, 'Chapra', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(413, 'Colgong', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(414, 'Dalsinghsarai', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(415, 'Darbhanga', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(416, 'Daudnagar', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(417, 'Dehri', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(418, 'Dhaka', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(419, 'Dighwara', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(420, 'Dinapur Cantt.', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(421, 'Dinapur Nizamat', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(422, 'Dumra', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(423, 'Dumraon', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(424, 'Fatwah', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(425, 'Forbesganj', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(426, 'Gaya', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(427, 'Ghoghardiha', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(428, 'Gogri Jamalpur', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(429, 'Gopalganj', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(430, 'Habibpur', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(431, 'Hajipur', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(432, 'Hilsa', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(433, 'Hisua', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(434, 'Islampur', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(435, 'Jagdishpur', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(436, 'Jainagar', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(437, 'Jamalpur', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(438, 'Jamhaur', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(439, 'Jamui', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(440, 'Janakpur Road', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(441, 'Jehanabad', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(442, 'Jhajha', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(443, 'Jhanjharpur', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(444, 'Jogbani', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(445, 'Kanti', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(446, 'Kasba', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(447, 'Kataiya', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(448, 'Katihar', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(449, 'Khagaria', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(450, 'Khagaul', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(451, 'Kharagpur', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(452, 'Khusrupur', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(453, 'Kishanganj', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(454, 'Koath', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(455, 'Koilwar', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(456, 'Lakhisarai', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(457, 'Lalganj', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(458, 'Lauthaha', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(459, 'Madhepura', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(460, 'Madhubani', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(461, 'Maharajganj', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(462, 'Mahnar Bazar', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(463, 'Mairwa', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(464, 'Makhdumpur', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(465, 'Maner', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(466, 'Manihari', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(467, 'Marhaura', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(468, 'Masaurhi', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(469, 'Mirganj', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(470, 'Mohiuddinagar', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(471, 'Mokameh', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(472, 'Motihari', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(473, 'Motipur', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(474, 'Munger', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(475, 'Murliganj', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(476, 'Muzaffarpur', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(477, 'Nabinagar', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(478, 'Narkatiaganj', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(479, 'Naugachhia', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(480, 'Nawada', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(481, 'Nirmali', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(482, 'Nokha', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(483, 'Paharpur', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(484, 'Patna', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(485, 'Phulwari Sharif', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(486, 'Piro', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(487, 'Purnia', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(488, 'Rafiganj', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(489, 'Raghunathpur', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(490, 'Rajgir', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(491, 'Ramnagar', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(492, 'Raxaul Bazar', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(493, 'Revelganj', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(494, 'Rosera', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(495, 'Saharsa', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(496, 'Samastipur', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(497, 'Sasaram', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(498, 'Shahpur', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(499, 'Sheikhpura', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(500, 'Sheohar', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(501, 'Sherghati', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(502, 'Silao', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(503, 'Sitamarhi', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(504, 'Siwan', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(505, 'Sonepur', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(506, 'Sugauli', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(507, 'Sultanganj', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(508, 'Supaul', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(509, 'Tekari', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(510, 'Thakurganj', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(511, 'Warisaliganj', 3, 4, 1, 1, 1, '2020-09-10 11:58:16', '2020-09-10 11:58:16', NULL),
(512, 'Ahiwara', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(513, 'Akaltara', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(514, 'Ambagarh Chowki', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(515, 'Ambikapur', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(516, 'Arang', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(517, 'Bade Bacheli', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(518, 'Bagbahara', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(519, 'Baikunthpur', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(520, 'Balod', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(521, 'Baloda', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(522, 'Baloda Bazar', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(523, 'Banarsi', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(524, 'Basna', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(525, 'Bemetra', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(526, 'Bhanpuri', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(527, 'Bhatapara', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(528, 'Bhatgaon', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(529, 'Bhilai Charoda', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(530, 'Bhilai Nagar', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(531, 'Bilaspur', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(532, 'Bilha', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(533, 'Birgaon', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(534, 'Bodri', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(535, 'Champa', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(536, 'Chharchha', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(537, 'Chhuikhadan', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(538, 'Chirmiri', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(539, 'Dalli-Rajhara', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(540, 'Dantewada', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(541, 'Deori', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(542, 'Dhamdha', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(543, 'Dhamtari', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(544, 'Dharamjaigarh', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(545, 'Dipka', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(546, 'Dongargaon', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(547, 'Dongragarh', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(548, 'Durg', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(549, 'Frezarpur', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(550, 'Gandai', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(551, 'Gaurella', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(552, 'Geedam', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(553, 'Gharghoda', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(554, 'Gobranawapara', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(555, 'Gogaon', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(556, 'Hatkachora', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(557, 'Jagdalpur', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(558, 'Jamul', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(559, 'Jashpur nagar', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(560, 'Jhagrakhand', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(561, 'Kanker', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(562, 'Katghora', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(563, 'Kawardha', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(564, 'Khairagarh', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(565, 'Khamhria', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(566, 'Kharod', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(567, 'Kharsia', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(568, 'Khongapani', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(569, 'Kirandul', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(570, 'Kondagaon', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(571, 'Korba', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(572, 'Kota', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(573, 'Kumhari', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(574, 'Kurud', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(575, 'Lingiyadih', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(576, 'Lormi', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(577, 'Mahasamund', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(578, 'Manendragarh', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(579, 'Mehmand', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(580, 'Mowa', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(581, 'Mungeli', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(582, 'Naila Janjgir', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(583, 'Namna Kalan', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(584, 'Naya Baradwar', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(585, 'Pandariya', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(586, 'Patan', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(587, 'Pathalgaon', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL);
INSERT INTO `city` (`id`, `city`, `country_name`, `state_name`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(588, 'Pendra', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(589, 'Phunderdihari', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(590, 'Pithora', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(591, 'Raigarh', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(592, 'Raipur', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(593, 'Rajgamar', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(594, 'Rajnandgaon', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(595, 'Ramanujganj', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(596, 'Ratanpur', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(597, 'Sakti', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(598, 'Saraipali', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(599, 'Sarangarh', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(600, 'Shivrinarayan', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(601, 'Simga', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(602, 'Sirgiti', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(603, 'Surajpur', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(604, 'Takhatpur', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(605, 'Telgaon', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(606, 'Tilda Newra', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(607, 'Urla', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(608, 'Vishrampur', 3, 9, 1, 1, 1, '2020-09-10 12:00:00', '2020-09-10 12:00:00', NULL),
(609, 'Aldona', 3, 10, 1, 1, 1, '2020-09-10 12:05:20', '2020-09-10 12:05:20', NULL),
(610, 'Aquem', 3, 10, 1, 1, 1, '2020-09-10 12:05:20', '2020-09-10 12:05:20', NULL),
(611, 'Bambolim', 3, 10, 1, 1, 1, '2020-09-10 12:05:20', '2020-09-10 12:05:20', NULL),
(612, 'Bandora', 3, 10, 1, 1, 1, '2020-09-10 12:05:20', '2020-09-10 12:05:20', NULL),
(613, 'Benaulim', 3, 10, 1, 1, 1, '2020-09-10 12:05:20', '2020-09-10 12:05:20', NULL),
(614, 'Bicholim', 3, 10, 1, 1, 1, '2020-09-10 12:05:20', '2020-09-10 12:05:20', NULL),
(615, 'Calangute', 3, 10, 1, 1, 1, '2020-09-10 12:05:20', '2020-09-10 12:05:20', NULL),
(616, 'Calapor', 3, 10, 1, 1, 1, '2020-09-10 12:05:20', '2020-09-10 12:05:20', NULL),
(617, 'Canacona', 3, 10, 1, 1, 1, '2020-09-10 12:05:20', '2020-09-10 12:05:20', NULL),
(618, 'Candolim', 3, 10, 1, 1, 1, '2020-09-10 12:05:20', '2020-09-10 12:05:20', NULL),
(619, 'Carapur', 3, 10, 1, 1, 1, '2020-09-10 12:05:20', '2020-09-10 12:05:20', NULL),
(620, 'Chicalim', 3, 10, 1, 1, 1, '2020-09-10 12:05:20', '2020-09-10 12:05:20', NULL),
(621, 'Chimbel', 3, 10, 1, 1, 1, '2020-09-10 12:05:20', '2020-09-10 12:05:20', NULL),
(622, 'Chinchinim', 3, 10, 1, 1, 1, '2020-09-10 12:05:20', '2020-09-10 12:05:20', NULL),
(623, 'Colvale', 3, 10, 1, 1, 1, '2020-09-10 12:05:20', '2020-09-10 12:05:20', NULL),
(624, 'Cuncolim', 3, 10, 1, 1, 1, '2020-09-10 12:05:20', '2020-09-10 12:05:20', NULL),
(625, 'Curchorem Cacora', 3, 10, 1, 1, 1, '2020-09-10 12:05:20', '2020-09-10 12:05:20', NULL),
(626, 'Curti', 3, 10, 1, 1, 1, '2020-09-10 12:05:20', '2020-09-10 12:05:20', NULL),
(627, 'Davorlim', 3, 10, 1, 1, 1, '2020-09-10 12:05:20', '2020-09-10 12:05:20', NULL),
(628, 'Goa Velha', 3, 10, 1, 1, 1, '2020-09-10 12:05:20', '2020-09-10 12:05:20', NULL),
(629, 'Guirim', 3, 10, 1, 1, 1, '2020-09-10 12:05:20', '2020-09-10 12:05:20', NULL),
(630, 'Mapusa', 3, 10, 1, 1, 1, '2020-09-10 12:05:20', '2020-09-10 12:05:20', NULL),
(631, 'Margao', 3, 10, 1, 1, 1, '2020-09-10 12:05:20', '2020-09-10 12:05:20', NULL),
(632, 'Mormugao', 3, 10, 1, 1, 1, '2020-09-10 12:05:20', '2020-09-10 12:05:20', NULL),
(633, 'Navelim', 3, 10, 1, 1, 1, '2020-09-10 12:05:20', '2020-09-10 12:05:20', NULL),
(634, 'Pale', 3, 10, 1, 1, 1, '2020-09-10 12:05:20', '2020-09-10 12:05:20', NULL),
(635, 'Panaji', 3, 10, 1, 1, 1, '2020-09-10 12:05:20', '2020-09-10 12:05:20', NULL),
(636, 'Parcem', 3, 10, 1, 1, 1, '2020-09-10 12:05:20', '2020-09-10 12:05:20', NULL),
(637, 'Penha-de-Franca', 3, 10, 1, 1, 1, '2020-09-10 12:05:20', '2020-09-10 12:05:20', NULL),
(638, 'Pernem', 3, 10, 1, 1, 1, '2020-09-10 12:05:20', '2020-09-10 12:05:20', NULL),
(639, 'Ponda', 3, 10, 1, 1, 1, '2020-09-10 12:05:20', '2020-09-10 12:05:20', NULL),
(640, 'Quepem', 3, 10, 1, 1, 1, '2020-09-10 12:05:20', '2020-09-10 12:05:20', NULL),
(641, 'Queula', 3, 10, 1, 1, 1, '2020-09-10 12:05:20', '2020-09-10 12:05:20', NULL),
(642, 'Reis Magos', 3, 10, 1, 1, 1, '2020-09-10 12:05:20', '2020-09-10 12:05:20', NULL),
(643, 'Saligao', 3, 10, 1, 1, 1, '2020-09-10 12:05:20', '2020-09-10 12:05:20', NULL),
(644, 'Sancoale', 3, 10, 1, 1, 1, '2020-09-10 12:05:20', '2020-09-10 12:05:20', NULL),
(645, 'Sanguem', 3, 10, 1, 1, 1, '2020-09-10 12:05:20', '2020-09-10 12:05:20', NULL),
(646, 'Sanquelim', 3, 10, 1, 1, 1, '2020-09-10 12:05:20', '2020-09-10 12:05:20', NULL),
(647, 'Sanvordem', 3, 10, 1, 1, 1, '2020-09-10 12:05:20', '2020-09-10 12:05:20', NULL),
(648, 'Sao Jose-de-Areal', 3, 10, 1, 1, 1, '2020-09-10 12:05:20', '2020-09-10 12:05:20', NULL),
(649, 'Siolim', 3, 10, 1, 1, 1, '2020-09-10 12:05:20', '2020-09-10 12:05:20', NULL),
(650, 'Socorro (Serula)', 3, 10, 1, 1, 1, '2020-09-10 12:05:20', '2020-09-10 12:05:20', NULL),
(651, 'Valpoi', 3, 10, 1, 1, 1, '2020-09-10 12:05:20', '2020-09-10 12:05:20', NULL),
(652, 'Varca', 3, 10, 1, 1, 1, '2020-09-10 12:05:20', '2020-09-10 12:05:20', NULL),
(653, 'Aadityana', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(654, 'Aambaliyasan', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(655, 'Aantaliya', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(656, 'Aarambhada', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(657, 'Abrama', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(658, 'Adalaj', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(659, 'Ahmedabad', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(660, 'Ahmedabad Cantonment', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(661, 'Alang', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(662, 'Ambaji', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(663, 'Amreli', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(664, 'Anand', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(665, 'Andada', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(666, 'Anjar', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(667, 'Anklav', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(668, 'Anklesvar', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(669, 'Anklesvar INA', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(670, 'Atul', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(671, 'Bagasara', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(672, 'Bajva', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(673, 'Balasinor', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(674, 'Bantwa', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(675, 'Bardoli', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(676, 'Bavla', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(677, 'Bedi', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(678, 'Bhachau', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(679, 'Bhanvad', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(680, 'Bharuch', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(681, 'Bharuch INA', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(682, 'Bhavnagar', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(683, 'Bhayavadar', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(684, 'Bhuj', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(685, 'Bilimora', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(686, 'Bodeli', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(687, 'Bopal', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(688, 'Boriavi', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(689, 'Borsad', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(690, 'Botad', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(691, 'Chaklasi', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(692, 'Chala', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(693, 'Chalala', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(694, 'Chalthan', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(695, 'Chanasma', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(696, 'Chandkheda', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(697, 'Chandlodiya', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(698, 'Chanod', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(699, 'Chhaprabhatha', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(700, 'Chhatral INA', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(701, 'Chhaya', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(702, 'Chhota Udaipur', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(703, 'Chikhli', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(704, 'Chiloda(Naroda)', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(705, 'Chorvad', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(706, 'Dabhoi', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(707, 'Dahegam', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(708, 'Dakor', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(709, 'Damnagar', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(710, 'Deesa', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(711, 'Devgadbaria', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(712, 'Devsar', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(713, 'Dhandhuka', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(714, 'Dhanera', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(715, 'Dharampur', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(716, 'Dhola', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(717, 'Dholka', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(718, 'Dhoraji', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(719, 'Dhrangadhra', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(720, 'Dhrol', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(721, 'Digvijaygram', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(722, 'Dohad', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(723, 'Dungra', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(724, 'Dwarka', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(725, 'Freelandgunj', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(726, 'Gadhada', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(727, 'Gandevi', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(728, 'Gandhidham', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(729, 'Gandhinagar', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(730, 'Gariadhar', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(731, 'Ghatlodiya', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(732, 'Ghogha', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(733, 'Godhra', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(734, 'Gondal', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(735, 'Gota', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(736, 'GSFC Complex INA', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(737, 'Hajira INA', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(738, 'Halol', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(739, 'Halvad', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(740, 'Harij', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(741, 'Himatnagar', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(742, 'Ichchhapor', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(743, 'Idar', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(744, 'Jafrabad', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(745, 'Jalalpore', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(746, 'Jam Jodhpur', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(747, 'Jambusar', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(748, 'Jamnagar', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(749, 'Jasdan', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(750, 'Jawaharnagar (Gujarat Refinery)', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(751, 'Jetpur Navagadh', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(752, 'Jodhpur', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(753, 'Joshipura', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(754, 'Junagadh', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(755, 'Kadi', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(756, 'Kadodara', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(757, 'Kalavad', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(758, 'Kali', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(759, 'Kalol', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(760, 'Kalol', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(761, 'Kalol INA', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(762, 'Kalol INA', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(763, 'Kandla', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(764, 'Kanodar', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(765, 'Kapadvanj', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(766, 'Karachiya', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(767, 'Karamsad', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(768, 'Karjan', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(769, 'Katpar', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(770, 'Keshod', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(771, 'Kevadiya', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(772, 'Khambhalia', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(773, 'Khambhat', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(774, 'Kharaghoda', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(775, 'Kheda', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(776, 'Khedbrahma', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(777, 'Kheralu', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(778, 'Kodinar', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(779, 'Kosamba', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(780, 'Kutiyana', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(781, 'Lambha', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(782, 'Lathi', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(783, 'Limbdi', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(784, 'Limla', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(785, 'Lunawada', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(786, 'Mahemdavad', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(787, 'Mahesana', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(788, 'Mahudha', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(789, 'Mahuva', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(790, 'Mahuvar', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(791, 'Makarba', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(792, 'Maktampur', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(793, 'Malpur', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(794, 'Manavadar', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(795, 'Mandvi', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(796, 'Mangrol', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(797, 'Mansa', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(798, 'Meghraj', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(799, 'Memnagar', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(800, 'Mithapur', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(801, 'Modasa', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(802, 'Mogravadi', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(803, 'Morvi', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(804, 'Motera', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(805, 'Mundra', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(806, 'Nadiad', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(807, 'Nanakvada', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(808, 'Nandej', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(809, 'Nandesari', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(810, 'Nandesari INA', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(811, 'NavagamGhed', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(812, 'Navsari', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(813, 'Ode', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(814, 'Okha port', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(815, 'Paddhari', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(816, 'Padra', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(817, 'Palanpur', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(818, 'Palej', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(819, 'Palitana', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(820, 'Pardi', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(821, 'Parnera', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(822, 'Parvat', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(823, 'Patan', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(824, 'Petlad', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(825, 'Petro Chemical Complex INA', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(826, 'Porbandar', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(827, 'Prantij', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(828, 'Radhanpur', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(829, 'Rajkot', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(830, 'Rajpipla', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(831, 'Rajula', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(832, 'Ramol', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(833, 'Ranavav', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(834, 'Ranip', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(835, 'Ranoli', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(836, 'Rapar', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(837, 'Sachin', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(838, 'Sachin INA', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(839, 'Salaya', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(840, 'Sanand', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(841, 'Santrampur', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(842, 'Sarigam INA', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(843, 'Sarkhej-Okaf', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(844, 'Savarkundla', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(845, 'Sayan', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(846, 'Sidhpur', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(847, 'Sihor', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(848, 'Sikka', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(849, 'Singarva', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(850, 'Songadh', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(851, 'Surajkaradi', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(852, 'Surat', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(853, 'Surendranagar Dudhrej', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(854, 'Talaja', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(855, 'Talod', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(856, 'Tarsali', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(857, 'Thaltej', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(858, 'Thangadh', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(859, 'Tharad', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(860, 'Ukai', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(861, 'Umbergaon', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(862, 'Umbergaon INA', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(863, 'Umreth', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(864, 'Un', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(865, 'Una', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(866, 'Unjha', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(867, 'Upleta', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(868, 'Utran', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(869, 'Vadia', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(870, 'Vadnagar', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(871, 'Vadodara', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(872, 'Vaghodia INA', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(873, 'Vallabh Vidhyanagar', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(874, 'Valsad', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(875, 'Valsad INA', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(876, 'Vanthali', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(877, 'Vapi', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(878, 'Vapi INA', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(879, 'Vartej', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(880, 'Vasna Borsad INA', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(881, 'Vastral', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(882, 'Vastrapur', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(883, 'Vejalpur', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(884, 'Veraval', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(885, 'Vijalpor', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(886, 'Vijapur', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(887, 'Viramgam', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(888, 'Visavadar', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(889, 'Visnagar', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(890, 'Vitthal Udyognagar INA', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(891, 'Vyara', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(892, 'Wadhwan', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(893, 'Wankaner', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(894, 'Zalod', 3, 1, 1, 1, 1, '2020-09-10 12:12:01', '2020-09-10 12:12:01', NULL),
(895, 'Ambala', 3, 11, 1, 1, 1, '2020-09-10 12:19:22', '2020-09-10 12:19:22', NULL),
(896, 'Ambala Cantt.', 3, 11, 1, 1, 1, '2020-09-10 12:19:22', '2020-09-10 12:19:22', NULL),
(897, 'Ambala Sadar', 3, 11, 1, 1, 1, '2020-09-10 12:19:22', '2020-09-10 12:19:22', NULL),
(898, 'Asan  Khurd', 3, 11, 1, 1, 1, '2020-09-10 12:19:22', '2020-09-10 12:19:22', NULL),
(899, 'Assandh', 3, 11, 1, 1, 1, '2020-09-10 12:19:22', '2020-09-10 12:19:22', NULL),
(900, 'Ateli', 3, 11, 1, 1, 1, '2020-09-10 12:19:22', '2020-09-10 12:19:22', NULL),
(901, 'Babiyal', 3, 11, 1, 1, 1, '2020-09-10 12:19:22', '2020-09-10 12:19:22', NULL),
(902, 'Bahadurgarh', 3, 11, 1, 1, 1, '2020-09-10 12:19:22', '2020-09-10 12:19:22', NULL),
(903, 'Barwala', 3, 11, 1, 1, 1, '2020-09-10 12:19:22', '2020-09-10 12:19:22', NULL),
(904, 'Bawal', 3, 11, 1, 1, 1, '2020-09-10 12:19:22', '2020-09-10 12:19:22', NULL),
(905, 'Bawani Khera', 3, 11, 1, 1, 1, '2020-09-10 12:19:22', '2020-09-10 12:19:22', NULL),
(906, 'Beri', 3, 11, 1, 1, 1, '2020-09-10 12:19:22', '2020-09-10 12:19:22', NULL),
(907, 'Bhiwani', 3, 11, 1, 1, 1, '2020-09-10 12:19:22', '2020-09-10 12:19:22', NULL),
(908, 'Bilaspur', 3, 11, 1, 1, 1, '2020-09-10 12:19:22', '2020-09-10 12:19:22', NULL),
(909, 'Buria', 3, 11, 1, 1, 1, '2020-09-10 12:19:22', '2020-09-10 12:19:22', NULL),
(910, 'Charkhi Dadri', 3, 11, 1, 1, 1, '2020-09-10 12:19:22', '2020-09-10 12:19:22', NULL),
(911, 'Cheeka', 3, 11, 1, 1, 1, '2020-09-10 12:19:22', '2020-09-10 12:19:22', NULL),
(912, 'Chhachhrauli', 3, 11, 1, 1, 1, '2020-09-10 12:19:22', '2020-09-10 12:19:22', NULL),
(913, 'Dharuhera', 3, 11, 1, 1, 1, '2020-09-10 12:19:22', '2020-09-10 12:19:22', NULL),
(914, 'Dundahera', 3, 11, 1, 1, 1, '2020-09-10 12:19:22', '2020-09-10 12:19:22', NULL),
(915, 'Ellenabad', 3, 11, 1, 1, 1, '2020-09-10 12:19:22', '2020-09-10 12:19:22', NULL),
(916, 'Farakhpur', 3, 11, 1, 1, 1, '2020-09-10 12:19:22', '2020-09-10 12:19:22', NULL),
(917, 'Faridabad', 3, 11, 1, 1, 1, '2020-09-10 12:19:22', '2020-09-10 12:19:22', NULL),
(918, 'Farrukhnagar', 3, 11, 1, 1, 1, '2020-09-10 12:19:22', '2020-09-10 12:19:22', NULL),
(919, 'Fatehabad', 3, 11, 1, 1, 1, '2020-09-10 12:19:22', '2020-09-10 12:19:22', NULL),
(920, 'Ferozepur Jhirka', 3, 11, 1, 1, 1, '2020-09-10 12:19:22', '2020-09-10 12:19:22', NULL),
(921, 'Ganaur', 3, 11, 1, 1, 1, '2020-09-10 12:19:22', '2020-09-10 12:19:22', NULL),
(922, 'Gharaunda', 3, 11, 1, 1, 1, '2020-09-10 12:19:22', '2020-09-10 12:19:22', NULL),
(923, 'Gohana', 3, 11, 1, 1, 1, '2020-09-10 12:19:22', '2020-09-10 12:19:22', NULL),
(924, 'Gurgaon', 3, 11, 1, 1, 1, '2020-09-10 12:19:22', '2020-09-10 12:19:22', NULL),
(925, 'Gurgaon Rural', 3, 11, 1, 1, 1, '2020-09-10 12:19:22', '2020-09-10 12:19:22', NULL),
(926, 'Haileymandi', 3, 11, 1, 1, 1, '2020-09-10 12:19:22', '2020-09-10 12:19:22', NULL),
(927, 'Hansi', 3, 11, 1, 1, 1, '2020-09-10 12:19:22', '2020-09-10 12:19:22', NULL),
(928, 'Hassanpur', 3, 11, 1, 1, 1, '2020-09-10 12:19:22', '2020-09-10 12:19:22', NULL),
(929, 'Hathin', 3, 11, 1, 1, 1, '2020-09-10 12:19:22', '2020-09-10 12:19:22', NULL),
(930, 'Hisar', 3, 11, 1, 1, 1, '2020-09-10 12:19:22', '2020-09-10 12:19:22', NULL),
(931, 'Hodal', 3, 11, 1, 1, 1, '2020-09-10 12:19:22', '2020-09-10 12:19:22', NULL),
(932, 'Indri', 3, 11, 1, 1, 1, '2020-09-10 12:19:22', '2020-09-10 12:19:22', NULL),
(933, 'Jagadhri', 3, 11, 1, 1, 1, '2020-09-10 12:19:22', '2020-09-10 12:19:22', NULL),
(934, 'Jakhal Mandi', 3, 11, 1, 1, 1, '2020-09-10 12:19:22', '2020-09-10 12:19:22', NULL),
(935, 'Jhajjar', 3, 11, 1, 1, 1, '2020-09-10 12:19:22', '2020-09-10 12:19:22', NULL),
(936, 'Jind', 3, 11, 1, 1, 1, '2020-09-10 12:19:22', '2020-09-10 12:19:22', NULL),
(937, 'Julana', 3, 11, 1, 1, 1, '2020-09-10 12:19:22', '2020-09-10 12:19:22', NULL),
(938, 'Kaithal', 3, 11, 1, 1, 1, '2020-09-10 12:19:22', '2020-09-10 12:19:22', NULL),
(939, 'Kalan Wali', 3, 11, 1, 1, 1, '2020-09-10 12:19:22', '2020-09-10 12:19:22', NULL),
(940, 'Kalanaur', 3, 11, 1, 1, 1, '2020-09-10 12:19:22', '2020-09-10 12:19:22', NULL),
(941, 'Kalayat', 3, 11, 1, 1, 1, '2020-09-10 12:19:22', '2020-09-10 12:19:22', NULL),
(942, 'Kalka', 3, 11, 1, 1, 1, '2020-09-10 12:19:22', '2020-09-10 12:19:22', NULL),
(943, 'Kanina', 3, 11, 1, 1, 1, '2020-09-10 12:19:22', '2020-09-10 12:19:22', NULL),
(944, 'Kansepur', 3, 11, 1, 1, 1, '2020-09-10 12:19:22', '2020-09-10 12:19:22', NULL),
(945, 'Kardhan', 3, 11, 1, 1, 1, '2020-09-10 12:19:22', '2020-09-10 12:19:22', NULL),
(946, 'Karnal', 3, 11, 1, 1, 1, '2020-09-10 12:19:22', '2020-09-10 12:19:22', NULL),
(947, 'Kharkhoda', 3, 11, 1, 1, 1, '2020-09-10 12:19:22', '2020-09-10 12:19:22', NULL),
(948, 'Ladrawan', 3, 11, 1, 1, 1, '2020-09-10 12:19:22', '2020-09-10 12:19:22', NULL),
(949, 'Ladwa', 3, 11, 1, 1, 1, '2020-09-10 12:19:22', '2020-09-10 12:19:22', NULL),
(950, 'Loharu', 3, 11, 1, 1, 1, '2020-09-10 12:19:22', '2020-09-10 12:19:22', NULL),
(951, 'Maham', 3, 11, 1, 1, 1, '2020-09-10 12:19:22', '2020-09-10 12:19:22', NULL),
(952, 'Mahendragarh', 3, 11, 1, 1, 1, '2020-09-10 12:19:22', '2020-09-10 12:19:22', NULL),
(953, 'Mandi Dabwali', 3, 11, 1, 1, 1, '2020-09-10 12:19:22', '2020-09-10 12:19:22', NULL),
(954, 'Mustafabad', 3, 11, 1, 1, 1, '2020-09-10 12:19:22', '2020-09-10 12:19:22', NULL),
(955, 'Nagai Chaudhry', 3, 11, 1, 1, 1, '2020-09-10 12:19:22', '2020-09-10 12:19:22', NULL),
(956, 'Naraingarh', 3, 11, 1, 1, 1, '2020-09-10 12:19:22', '2020-09-10 12:19:22', NULL),
(957, 'Narnaul', 3, 11, 1, 1, 1, '2020-09-10 12:19:22', '2020-09-10 12:19:22', NULL),
(958, 'Narnaund', 3, 11, 1, 1, 1, '2020-09-10 12:19:22', '2020-09-10 12:19:22', NULL),
(959, 'Narwana', 3, 11, 1, 1, 1, '2020-09-10 12:19:22', '2020-09-10 12:19:22', NULL),
(960, 'Nilokheri', 3, 11, 1, 1, 1, '2020-09-10 12:19:22', '2020-09-10 12:19:22', NULL),
(961, 'Nuh', 3, 11, 1, 1, 1, '2020-09-10 12:19:22', '2020-09-10 12:19:22', NULL),
(962, 'Palwal', 3, 11, 1, 1, 1, '2020-09-10 12:19:22', '2020-09-10 12:19:22', NULL),
(963, 'Panchkula Urban Estate', 3, 11, 1, 1, 1, '2020-09-10 12:19:22', '2020-09-10 12:19:22', NULL),
(964, 'Panipat', 3, 11, 1, 1, 1, '2020-09-10 12:19:22', '2020-09-10 12:19:22', NULL),
(965, 'Panipat Taraf Ansar', 3, 11, 1, 1, 1, '2020-09-10 12:19:22', '2020-09-10 12:19:22', NULL),
(966, 'Panipat Taraf Rajputan', 3, 11, 1, 1, 1, '2020-09-10 12:19:23', '2020-09-10 12:19:23', NULL),
(967, 'Panipt Taraf Makhdum Zadgan', 3, 11, 1, 1, 1, '2020-09-10 12:19:23', '2020-09-10 12:19:23', NULL),
(968, 'Pataudi', 3, 11, 1, 1, 1, '2020-09-10 12:19:23', '2020-09-10 12:19:23', NULL),
(969, 'Pehowa', 3, 11, 1, 1, 1, '2020-09-10 12:19:23', '2020-09-10 12:19:23', NULL),
(970, 'Pinjore', 3, 11, 1, 1, 1, '2020-09-10 12:19:23', '2020-09-10 12:19:23', NULL),
(971, 'Punahana', 3, 11, 1, 1, 1, '2020-09-10 12:19:23', '2020-09-10 12:19:23', NULL),
(972, 'Pundri', 3, 11, 1, 1, 1, '2020-09-10 12:19:23', '2020-09-10 12:19:23', NULL),
(973, 'Radaur', 3, 11, 1, 1, 1, '2020-09-10 12:19:23', '2020-09-10 12:19:23', NULL),
(974, 'Raipur Rani', 3, 11, 1, 1, 1, '2020-09-10 12:19:23', '2020-09-10 12:19:23', NULL),
(975, 'Rania', 3, 11, 1, 1, 1, '2020-09-10 12:19:23', '2020-09-10 12:19:23', NULL),
(976, 'Ratia', 3, 11, 1, 1, 1, '2020-09-10 12:19:23', '2020-09-10 12:19:23', NULL),
(977, 'Rewari', 3, 11, 1, 1, 1, '2020-09-10 12:19:23', '2020-09-10 12:19:23', NULL),
(978, 'Rewari (Rural)', 3, 11, 1, 1, 1, '2020-09-10 12:19:23', '2020-09-10 12:19:23', NULL),
(979, 'Rohtak', 3, 11, 1, 1, 1, '2020-09-10 12:19:23', '2020-09-10 12:19:23', NULL),
(980, 'Sadaura', 3, 11, 1, 1, 1, '2020-09-10 12:19:23', '2020-09-10 12:19:23', NULL),
(981, 'Safidon', 3, 11, 1, 1, 1, '2020-09-10 12:19:23', '2020-09-10 12:19:23', NULL),
(982, 'Samalkha', 3, 11, 1, 1, 1, '2020-09-10 12:19:23', '2020-09-10 12:19:23', NULL),
(983, 'Sankhol', 3, 11, 1, 1, 1, '2020-09-10 12:19:23', '2020-09-10 12:19:23', NULL),
(984, 'Sasauli', 3, 11, 1, 1, 1, '2020-09-10 12:19:23', '2020-09-10 12:19:23', NULL),
(985, 'Shahbad', 3, 11, 1, 1, 1, '2020-09-10 12:19:23', '2020-09-10 12:19:23', NULL),
(986, 'Sirsa', 3, 11, 1, 1, 1, '2020-09-10 12:19:23', '2020-09-10 12:19:23', NULL),
(987, 'Siwani', 3, 11, 1, 1, 1, '2020-09-10 12:19:23', '2020-09-10 12:19:23', NULL),
(988, 'Sohna', 3, 11, 1, 1, 1, '2020-09-10 12:19:23', '2020-09-10 12:19:23', NULL),
(989, 'Sonipat', 3, 11, 1, 1, 1, '2020-09-10 12:19:23', '2020-09-10 12:19:23', NULL),
(990, 'Sukhrali', 3, 11, 1, 1, 1, '2020-09-10 12:19:23', '2020-09-10 12:19:23', NULL),
(991, 'Taoru', 3, 11, 1, 1, 1, '2020-09-10 12:19:23', '2020-09-10 12:19:23', NULL),
(992, 'Taraori', 3, 11, 1, 1, 1, '2020-09-10 12:19:23', '2020-09-10 12:19:23', NULL),
(993, 'Thanesar', 3, 11, 1, 1, 1, '2020-09-10 12:19:23', '2020-09-10 12:19:23', NULL),
(994, 'Tilpat', 3, 11, 1, 1, 1, '2020-09-10 12:19:23', '2020-09-10 12:19:23', NULL),
(995, 'Tohana', 3, 11, 1, 1, 1, '2020-09-10 12:19:23', '2020-09-10 12:19:23', NULL),
(996, 'Tosham', 3, 11, 1, 1, 1, '2020-09-10 12:19:23', '2020-09-10 12:19:23', NULL),
(997, 'Uchana', 3, 11, 1, 1, 1, '2020-09-10 12:19:23', '2020-09-10 12:19:23', NULL),
(998, 'Uklanamandi', 3, 11, 1, 1, 1, '2020-09-10 12:19:23', '2020-09-10 12:19:23', NULL),
(999, 'Uncha Siwana', 3, 11, 1, 1, 1, '2020-09-10 12:19:23', '2020-09-10 12:19:23', NULL),
(1000, 'Yamunanagar', 3, 11, 1, 1, 1, '2020-09-10 12:19:23', '2020-09-10 12:19:23', NULL),
(1107, 'Arki', 3, 33, 1, 1, 1, '2020-09-10 12:36:38', '2020-09-10 12:36:38', NULL),
(1108, 'Baddi', 3, 33, 1, 1, 1, '2020-09-10 12:36:38', '2020-09-10 12:36:38', NULL),
(1109, 'Bakloh', 3, 33, 1, 1, 1, '2020-09-10 12:36:38', '2020-09-10 12:36:38', NULL),
(1110, 'Banjar', 3, 33, 1, 1, 1, '2020-09-10 12:36:38', '2020-09-10 12:36:38', NULL),
(1111, 'Bhota', 3, 33, 1, 1, 1, '2020-09-10 12:36:38', '2020-09-10 12:36:38', NULL),
(1112, 'Bhuntar', 3, 33, 1, 1, 1, '2020-09-10 12:36:38', '2020-09-10 12:36:38', NULL),
(1113, 'Bilaspur', 3, 33, 1, 1, 1, '2020-09-10 12:36:38', '2020-09-10 12:36:38', NULL),
(1114, 'Chamba', 3, 33, 1, 1, 1, '2020-09-10 12:36:38', '2020-09-10 12:36:38', NULL),
(1115, 'Chaupal', 3, 33, 1, 1, 1, '2020-09-10 12:36:38', '2020-09-10 12:36:38', NULL),
(1116, 'Chuari Khas', 3, 33, 1, 1, 1, '2020-09-10 12:36:38', '2020-09-10 12:36:38', NULL),
(1117, 'Dagshai', 3, 33, 1, 1, 1, '2020-09-10 12:36:38', '2020-09-10 12:36:38', NULL),
(1118, 'Dalhousie', 3, 33, 1, 1, 1, '2020-09-10 12:36:38', '2020-09-10 12:36:38', NULL),
(1119, 'Dalhousie', 3, 33, 1, 1, 1, '2020-09-10 12:36:38', '2020-09-10 12:36:38', NULL),
(1120, 'Daulatpur', 3, 33, 1, 1, 1, '2020-09-10 12:36:38', '2020-09-10 12:36:38', NULL),
(1121, 'Dera Gopipur', 3, 33, 1, 1, 1, '2020-09-10 12:36:38', '2020-09-10 12:36:38', NULL),
(1122, 'Dharmsala', 3, 33, 1, 1, 1, '2020-09-10 12:36:38', '2020-09-10 12:36:38', NULL),
(1123, 'Gagret', 3, 33, 1, 1, 1, '2020-09-10 12:36:38', '2020-09-10 12:36:38', NULL),
(1124, 'Ghumarwin', 3, 33, 1, 1, 1, '2020-09-10 12:36:38', '2020-09-10 12:36:38', NULL),
(1125, 'Hamirpur', 3, 33, 1, 1, 1, '2020-09-10 12:36:38', '2020-09-10 12:36:38', NULL),
(1126, 'Jawalamukhi', 3, 33, 1, 1, 1, '2020-09-10 12:36:38', '2020-09-10 12:36:38', NULL),
(1127, 'Jogindarnagar', 3, 33, 1, 1, 1, '2020-09-10 12:36:38', '2020-09-10 12:36:38', NULL),
(1128, 'Jubbal', 3, 33, 1, 1, 1, '2020-09-10 12:36:38', '2020-09-10 12:36:38', NULL),
(1129, 'Jutogh', 3, 33, 1, 1, 1, '2020-09-10 12:36:38', '2020-09-10 12:36:38', NULL),
(1130, 'Kangra', 3, 33, 1, 1, 1, '2020-09-10 12:36:38', '2020-09-10 12:36:38', NULL),
(1131, 'Kasauli', 3, 33, 1, 1, 1, '2020-09-10 12:36:38', '2020-09-10 12:36:38', NULL),
(1132, 'Kotkhai', 3, 33, 1, 1, 1, '2020-09-10 12:36:38', '2020-09-10 12:36:38', NULL),
(1133, 'Kullu', 3, 33, 1, 1, 1, '2020-09-10 12:36:38', '2020-09-10 12:36:38', NULL),
(1134, 'Manali', 3, 33, 1, 1, 1, '2020-09-10 12:36:38', '2020-09-10 12:36:38', NULL),
(1135, 'Mandi', 3, 33, 1, 1, 1, '2020-09-10 12:36:38', '2020-09-10 12:36:38', NULL),
(1136, 'Mant Khas', 3, 33, 1, 1, 1, '2020-09-10 12:36:38', '2020-09-10 12:36:38', NULL),
(1137, 'Mehatpur Basdehra', 3, 33, 1, 1, 1, '2020-09-10 12:36:38', '2020-09-10 12:36:38', NULL),
(1138, 'Nadaun', 3, 33, 1, 1, 1, '2020-09-10 12:36:38', '2020-09-10 12:36:38', NULL),
(1139, 'Nagrota Bagwan', 3, 33, 1, 1, 1, '2020-09-10 12:36:38', '2020-09-10 12:36:38', NULL),
(1140, 'Nahan', 3, 33, 1, 1, 1, '2020-09-10 12:36:38', '2020-09-10 12:36:38', NULL),
(1141, 'Naina Devi', 3, 33, 1, 1, 1, '2020-09-10 12:36:38', '2020-09-10 12:36:38', NULL),
(1142, 'Nalagarh', 3, 33, 1, 1, 1, '2020-09-10 12:36:38', '2020-09-10 12:36:38', NULL),
(1143, 'Narkanda', 3, 33, 1, 1, 1, '2020-09-10 12:36:38', '2020-09-10 12:36:38', NULL),
(1144, 'Nurpur', 3, 33, 1, 1, 1, '2020-09-10 12:36:38', '2020-09-10 12:36:38', NULL),
(1145, 'Palampur', 3, 33, 1, 1, 1, '2020-09-10 12:36:38', '2020-09-10 12:36:38', NULL),
(1146, 'Paonta Sahib', 3, 33, 1, 1, 1, '2020-09-10 12:36:38', '2020-09-10 12:36:38', NULL),
(1147, 'Parwanoo', 3, 33, 1, 1, 1, '2020-09-10 12:36:38', '2020-09-10 12:36:38', NULL),
(1148, 'Rajgarh', 3, 33, 1, 1, 1, '2020-09-10 12:36:38', '2020-09-10 12:36:38', NULL),
(1149, 'Rampur', 3, 33, 1, 1, 1, '2020-09-10 12:36:38', '2020-09-10 12:36:38', NULL),
(1150, 'Rawalsar', 3, 33, 1, 1, 1, '2020-09-10 12:36:38', '2020-09-10 12:36:38', NULL),
(1151, 'Rohru', 3, 33, 1, 1, 1, '2020-09-10 12:36:38', '2020-09-10 12:36:38', NULL),
(1152, 'Sabathu', 3, 33, 1, 1, 1, '2020-09-10 12:36:38', '2020-09-10 12:36:38', NULL),
(1153, 'Santokhgarh', 3, 33, 1, 1, 1, '2020-09-10 12:36:38', '2020-09-10 12:36:38', NULL),
(1154, 'Sarkaghat', 3, 33, 1, 1, 1, '2020-09-10 12:36:38', '2020-09-10 12:36:38', NULL),
(1155, 'Seoni', 3, 33, 1, 1, 1, '2020-09-10 12:36:38', '2020-09-10 12:36:38', NULL),
(1156, 'Shimla', 3, 33, 1, 1, 1, '2020-09-10 12:36:38', '2020-09-10 12:36:38', NULL),
(1157, 'Solan', 3, 33, 1, 1, 1, '2020-09-10 12:36:38', '2020-09-10 12:36:38', NULL),
(1158, 'Sundarnagar', 3, 33, 1, 1, 1, '2020-09-10 12:36:38', '2020-09-10 12:36:38', NULL),
(1159, 'Talai', 3, 33, 1, 1, 1, '2020-09-10 12:36:38', '2020-09-10 12:36:38', NULL),
(1160, 'Theog', 3, 33, 1, 1, 1, '2020-09-10 12:36:38', '2020-09-10 12:36:38', NULL),
(1161, 'Tira Sujanpur', 3, 33, 1, 1, 1, '2020-09-10 12:36:38', '2020-09-10 12:36:38', NULL),
(1162, 'Una', 3, 33, 1, 1, 1, '2020-09-10 12:36:38', '2020-09-10 12:36:38', NULL),
(1163, 'Yol', 3, 33, 1, 1, 1, '2020-09-10 12:36:38', '2020-09-10 12:36:38', NULL),
(1164, 'Adityapur', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1165, 'Amlabad', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1166, 'Angarpathar', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1167, 'Ara', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1168, 'Babua Kalan', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1169, 'Bagbera', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1170, 'Baliari', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1171, 'Balkundra', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1172, 'Bandhgora', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1173, 'Barajamda', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1174, 'Barhi', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1175, 'Barkakana', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1176, 'Barughutu', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1177, 'Barwadih', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1178, 'Basaria', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1179, 'Basukinath', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1180, 'Bermo', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1181, 'Bhagatdih', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1182, 'Bhojudih', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1183, 'Bhowrah', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1184, 'Bhuli', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1185, 'Bokaro', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1186, 'Bokaro Steel City', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1187, 'Bundu', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1188, 'Chaibasa', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1189, 'Chakradharpur', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1190, 'Chakulia', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1191, 'Chandaur', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1192, 'Chandil', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1193, 'Chandrapura', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1194, 'Chas', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1195, 'Chatra', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1196, 'Chhatatanr', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1197, 'Chhota Gobindpur', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1198, 'Chhotaputki', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1199, 'Chiria', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1200, 'Chirkunda', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1201, 'Churi', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1202, 'Daltonganj', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1203, 'Danguwapasi', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1204, 'Dari', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1205, 'Deoghar', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1206, 'Deorikalan', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1207, 'Dhanbad', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1208, 'Dhanwar', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1209, 'Dhaunsar', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1210, 'Dugda', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1211, 'Dumarkunda', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1212, 'Dumka', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1213, 'Egarkunr', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1214, 'Gadhra', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1215, 'Garhwa', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1216, 'Ghatshila', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1217, 'Ghorabandha', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1218, 'Gidi', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1219, 'Giridih', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1220, 'Gobindpur', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1221, 'Godda', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1222, 'Godhar', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1223, 'Gomoh', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1224, 'Gua', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1225, 'Gumia', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1226, 'Gumla', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1227, 'Haludbani', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1228, 'Hazaribag', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1229, 'Hesla', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1230, 'Hussainabad', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1231, 'Isri', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1232, 'Jadugora', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1233, 'Jamadoba', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1234, 'Jamshedpur', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1235, 'Jamtara', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1236, 'Jaridih Bazar', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1237, 'Jasidih', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1238, 'Jena', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1239, 'Jharia', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1240, 'Jharia Khas', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1241, 'Jhinkpani', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1242, 'Jhumri Tilaiya', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1243, 'Jorapokhar', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1244, 'Jugsalai', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1245, 'Kailudih', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1246, 'Kalikapur', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1247, 'Kandra', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1248, 'Kanke', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1249, 'Katras', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1250, 'Kedla', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1251, 'Kenduadih', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1252, 'Kharkhari', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1253, 'Kharsawan', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1254, 'Khelari', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1255, 'Khunti', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1256, 'Kiriburu', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1257, 'Kodarma', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1258, 'Kuju', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1259, 'Kurpania', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1260, 'Kustai', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1261, 'Lakarka', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1262, 'Lapanga', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1263, 'Latehar', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1264, 'Lohardaga', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1265, 'Loyabad', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1266, 'Madhupur', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1267, 'Maithon', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1268, 'Malkera', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1269, 'Mango', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1270, 'Marma', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1271, 'Meghahatuburu Forest village', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1272, 'Mera', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1273, 'Meru', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1274, 'Mihijam', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1275, 'Mugma', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1276, 'Muri', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1277, 'Musabani', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1278, 'Nagri Kalan', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1279, 'Nirsa', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1280, 'Noamundi', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1281, 'Okni NO.II', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL);
INSERT INTO `city` (`id`, `city`, `country_name`, `state_name`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1282, 'Orla', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1283, 'Pakaur', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1284, 'Palawa', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1285, 'Panchet', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1286, 'Paratdih', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1287, 'Pathardih', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1288, 'Patratu', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1289, 'Phusro', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1290, 'Pondar Kanali', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1291, 'Rajmahal', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1292, 'Ramgarh Cantonment', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1293, 'Ranchi', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1294, 'Religara alias Pachhiari', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1295, 'Rohraband', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1296, 'Sahibganj', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1297, 'Sahnidih', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1298, 'Saraidhela', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1299, 'Sarjamda', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1300, 'Saunda', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1301, 'Seraikela', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1302, 'Sewai', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1303, 'Sijhua', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1304, 'Sijua', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1305, 'Simdega', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1306, 'Sindri', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1307, 'Sinduria', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1308, 'Sini', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1309, 'Sirka', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1310, 'Siuliban', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1311, 'Tati', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1312, 'Tenu Dam-cum- Kathhara', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1313, 'Tisra', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1314, 'Topa', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1315, 'Topchanchi', 3, 12, 1, 1, 1, '2020-09-10 12:39:16', '2020-09-10 12:39:16', NULL),
(1316, 'Adityapatna', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1317, 'Adyar', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1318, 'Afzalpur', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1319, 'Aland', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1320, 'Alnavar', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1321, 'Alur', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1322, 'Ambikanagara', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1323, 'Anekal', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1324, 'Ankola', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1325, 'Annigeri', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1326, 'Arkalgud', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1327, 'Arsikere', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1328, 'Athni', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1329, 'Aurad', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1330, 'Badami', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1331, 'Bagalkot', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1332, 'Bagepalli', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1333, 'Bail Hongal', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1334, 'Bajala', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1335, 'Bajpe', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1336, 'Bangalore', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1337, 'Bangarapet', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1338, 'Bankapura', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1339, 'Bannur', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1340, 'Bantwal', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1341, 'Basavakalyan', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1342, 'Basavana Bagevadi', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1343, 'Belgaum', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1344, 'Belgaum Cantonment', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1345, 'Bellary', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1346, 'Beltangadi', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1347, 'Belur', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1348, 'Belvata', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1349, 'Bhadravati', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1350, 'Bhalki', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1351, 'Bhatkal', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1352, 'Bhimarayanagudi', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1353, 'Bhogadi', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1354, 'Bidar', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1355, 'Bijapur', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1356, 'Bilgi', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1357, 'Birur', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1358, 'Bommanahalli', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1359, 'Bommasandra', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1360, 'Byadgi', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1361, 'Byatarayanapura', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1362, 'Challakere', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1363, 'Chamrajnagar', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1364, 'Channagiri', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1365, 'Channapatna', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1366, 'Channarayapattana', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1367, 'Chik Ballapur', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1368, 'Chikmagalur', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1369, 'Chiknayakanhalli', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1370, 'Chikodi', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1371, 'Chincholi', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1372, 'Chintamani', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1373, 'Chitapur', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1374, 'Chitgoppa', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1375, 'Chitradurga', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1376, 'Dandeli', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1377, 'Dargajogihalli', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1378, 'Dasarahalli', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1379, 'Davanagere', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1380, 'Devadurga', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1381, 'Devanhalli', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1382, 'Dod Ballapur', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1383, 'Donimalai Township', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1384, 'Gadag-Betigeri', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1385, 'Gajendragarh', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1386, 'Gangawati', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1387, 'Gauribidanur', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1388, 'Gokak', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1389, 'Gokak Falls', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1390, 'Gonikoppal', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1391, 'Gottikere', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1392, 'Gubbi', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1393, 'Gudibanda', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1394, 'Gulbarga', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1395, 'Guledgudda', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1396, 'Gundlupet', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1397, 'Gurmatkal', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1398, 'Haliyal', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1399, 'Hangal', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1400, 'Harihar', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1401, 'Harpanahalli', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1402, 'Hassan', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1403, 'Hatti', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1404, 'Hatti Gold Mines', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1405, 'Haveri', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1406, 'Hebbagodi', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1407, 'Hebbalu', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1408, 'Heggadadevanakote', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1409, 'Herohalli', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1410, 'Hindalgi', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1411, 'Hirekerur', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1412, 'Hiriyur', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1413, 'Holalkere', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1414, 'Holenarsipur', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1415, 'Homnabad', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1416, 'Honavar', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1417, 'Honnali', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1418, 'Hoovina Hadagalli', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1419, 'Hosanagara', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1420, 'Hosdurga', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1421, 'Hoskote', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1422, 'Hospet', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1423, 'Hubli-Dharwad', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1424, 'Hukeri', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1425, 'Hunasamaranahalli', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1426, 'Hungund', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1427, 'Hunsur', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1428, 'Ilkal', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1429, 'Indi', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1430, 'Jagalur', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1431, 'Jamkhandi', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1432, 'Jevargi', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1433, 'Jog Falls', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1434, 'Kadur', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1435, 'Kalghatgi', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1436, 'Kamalapuram', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1437, 'Kampli', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1438, 'Kanakapura', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1439, 'Kangrali (BK)', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1440, 'Kangrali (KH)', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1441, 'Kannur', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1442, 'Karkal', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1443, 'Karwar', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1444, 'Kengeri', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1445, 'Kerur', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1446, 'Khanapur', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1447, 'Kodigenahalli', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1448, 'Kodiyal', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1449, 'Kolar', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1450, 'Kollegal', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1451, 'Konanakunte', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1452, 'Konnur', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1453, 'Koppa', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1454, 'Koppal', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1455, 'Koratagere', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1456, 'Kotekara', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1457, 'Kothnur', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1458, 'Kotturu', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1459, 'Krishnarajanagar', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1460, 'Krishnarajapura', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1461, 'Krishnarajasagara', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1462, 'Krishnarajpet', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1463, 'Kudchi', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1464, 'Kudligi', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1465, 'Kudremukh', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1466, 'Kumta', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1467, 'Kundapura', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1468, 'Kundgol', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1469, 'Kunigal', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1470, 'Kurgunta', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1471, 'Kushalnagar', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1472, 'Kushtagi', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1473, 'Lakshmeshwar', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1474, 'Lingsugur', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1475, 'Londa', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1476, 'Maddur', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1477, 'Madhugiri', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1478, 'Madikeri', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1479, 'Magadi', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1480, 'Mahadevapura', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1481, 'Mahalingpur', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1482, 'Malavalli', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1483, 'Mallar', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1484, 'Malur', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1485, 'Mandya', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1486, 'Mangalore', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1487, 'Manvi', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1488, 'Molakalmuru', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1489, 'Mudalgi', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1490, 'Mudbidri', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1491, 'Muddebihal', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1492, 'Mudgal', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1493, 'Mudhol', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1494, 'Mudigere', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1495, 'Mudushedde', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1496, 'Mulbagal', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1497, 'Mulgund', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1498, 'Mulki', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1499, 'Mulur', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1500, 'Mundargi', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1501, 'Mundgod', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1502, 'Munirabad Project Area', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1503, 'Munnur', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1504, 'Mysore', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1505, 'Nagamangala', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1506, 'Nanjangud', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1507, 'Narasimharajapura', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1508, 'Naregal', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1509, 'Nargund', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1510, 'Navalgund', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1511, 'Nelmangala', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1512, 'Nipani', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1513, 'Pandavapura', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1514, 'Pattanagere', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1515, 'Pavagada', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1516, 'Piriyapatna', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1517, 'Pudu', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1518, 'Puttur', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1519, 'Rabkavi Banhatti', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1520, 'Raichur', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1521, 'Ramanagaram', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1522, 'Ramdurg', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1523, 'Ranibennur', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1524, 'Raybag', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1525, 'Robertson Pet', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1526, 'Ron', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1527, 'Sadalgi', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1528, 'Sagar', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1529, 'Sakleshpur', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1530, 'Saligram', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1531, 'Sandur', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1532, 'Sankeshwar', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1533, 'Sathyamangala', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1534, 'Saundatti-Yellamma', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1535, 'Savanur', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1536, 'Sedam', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1537, 'Shahabad', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1538, 'Shahabad ACC', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1539, 'Shahpur', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1540, 'Shaktinagar', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1541, 'Shiggaon', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1542, 'Shikarpur', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1543, 'Shimoga', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1544, 'Shirhatti', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1545, 'Shorapur', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1546, 'Shrirangapattana', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1547, 'Siddapur', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1548, 'Sidlaghatta', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1549, 'Sindgi', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1550, 'Sindhnur', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1551, 'Sira', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1552, 'Siralkoppa', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1553, 'Sirsi', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1554, 'Siruguppa', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1555, 'Someshwar', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1556, 'Somvarpet', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1557, 'Sorab', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1558, 'Sringeri', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1559, 'Srinivaspur', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1560, 'Sulya', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1561, 'Talikota', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1562, 'Tarikere', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1563, 'Tekkalakota', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1564, 'Terdal', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1565, 'Thokur-62', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1566, 'Thumbe', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1567, 'Tiptur', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1568, 'Tirthahalli', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1569, 'Tirumakudal-Narsipur', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1570, 'Tumkur', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1571, 'Turuvekere', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1572, 'Udupi', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1573, 'Ullal', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1574, 'Uttarahalli', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1575, 'Venkatapura', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1576, 'Vijayapura', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1577, 'Virajpet', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1578, 'Wadi', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1579, 'Wadi ACC', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1580, 'Yadgir', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1581, 'Yelahanka', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1582, 'Yelandur', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1583, 'Yelbarga', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1584, 'Yellapur', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1585, 'Yenagudde', 3, 13, 1, 1, 1, '2020-09-10 12:41:53', '2020-09-10 12:41:53', NULL),
(1586, 'Adoor', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1587, 'Akathiyoor', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1588, 'Alappuzha', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1589, 'Aluva', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1590, 'Ancharakandy', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1591, 'Angamaly', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1592, 'Arookutty', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1593, 'Aroor', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1594, 'Attingal', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1595, 'Avinissery', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1596, 'Azhikode North', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1597, 'Azhikode South', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1598, 'Bangramanjeshwar', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1599, 'Beypore', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1600, 'Brahmakulam', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1601, 'Chala', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1602, 'Chalakudy', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1603, 'Changanassery', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1604, 'Chavakkad', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1605, 'Chelora', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1606, 'Chendamangalam', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1607, 'Chengamanad', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1608, 'Chengannur', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1609, 'Cheranallur', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1610, 'Cherthala', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1611, 'Cheruthazham', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1612, 'Cheruvannur', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1613, 'Chevvoor', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1614, 'Chirakkal', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1615, 'Chittur-Thathamangalam', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1616, 'Chockli', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1617, 'Choornikkara', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1618, 'Chowwara', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1619, 'Dharmadom', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1620, 'Edathala', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1621, 'Elayavoor', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1622, 'Eloor', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1623, 'Eranholi', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1624, 'Erattupetta', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1625, 'Feroke', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1626, 'Guruvayoor', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1627, 'Hosabettu', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1628, 'Idukki Township', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1629, 'Iringaprom', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1630, 'Irinjalakuda', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1631, 'Iriveri', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1632, 'Kadachira', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1633, 'Kadamakkudy', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1634, 'Kadirur', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1635, 'Kadungalloor', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1636, 'Kalamassery', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1637, 'Kalliasseri', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1638, 'Kalpetta', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1639, 'Kanhangad', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1640, 'Kanhirode', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1641, 'Kanjikkuzhi', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1642, 'Kannadiparamba', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1643, 'Kannapuram', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1644, 'Kannur', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1645, 'Kannur Cantonment', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1646, 'Karuvanthuruthy', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1647, 'Kasaragod', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1648, 'Kayamkulam', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1649, 'Kochi', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1650, 'Kodungallur', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1651, 'Kokkothamangalam', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1652, 'Kolazhy', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1653, 'Kollam', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1654, 'Komalapuram', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1655, 'Koothuparamba', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1656, 'Koratty', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1657, 'Kothamangalam', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1658, 'Kottayam', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1659, 'Kottayam-Malabar', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1660, 'Kottuvally', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1661, 'Kozhikode', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1662, 'Kudlu', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1663, 'Kunnamkulam', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1664, 'Kureekkad', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1665, 'Malappuram', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1666, 'Manjeri', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1667, 'Manjeshwar', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1668, 'Maradu', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1669, 'Marathakkara', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1670, 'Mattannur', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1671, 'Mavelikkara', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1672, 'Mavilayi', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1673, 'Mavoor', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1674, 'Methala', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1675, 'Muhamma', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1676, 'Mulavukad', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1677, 'Munderi', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1678, 'Muvattupuzha', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1679, 'Muzhappilangad', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1680, 'Nadathara', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1681, 'Narath', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1682, 'Nedumangad', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1683, 'Nenmenikkara', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1684, 'New Mahe', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1685, 'Neyyattinkara', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1686, 'Olavanna', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1687, 'Ottappalam', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1688, 'Paduvilayi', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1689, 'Palai', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1690, 'Palakkad', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1691, 'Palayad', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1692, 'Palissery', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1693, 'Pallikkunnu', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1694, 'Paluvai', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1695, 'Panniyannur', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1696, 'Panoor', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1697, 'Pantheeramkavu', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1698, 'Pappinisseri', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1699, 'Paravoor', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1700, 'Paravur', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1701, 'Pathanamthitta', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1702, 'Pathiriyad', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1703, 'Pattiom', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1704, 'Pavaratty', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1705, 'Payyannur', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1706, 'Perakam', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1707, 'Peralasseri', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1708, 'Peringathur', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1709, 'Perinthalmanna', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1710, 'Perumbaikad', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1711, 'Perumbavoor', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1712, 'Pinarayi', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1713, 'Ponnani', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1714, 'Pottore', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1715, 'Punalur', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1716, 'Puranattukara', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1717, 'Puthukkad', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1718, 'Puthunagaram', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1719, 'Puzhathi', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1720, 'Quilandy', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1721, 'Ramanattukara', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1722, 'Shoranur', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1723, 'Taliparamba', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1724, 'Thaikkad', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1725, 'Thalassery', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1726, 'Thiruvalla', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1727, 'Thiruvankulam', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1728, 'Thodupuzha', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1729, 'Thottada', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1730, 'Thrippunithura', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1731, 'Thrissur', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1732, 'Tirur', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1733, 'Trivandrum', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1734, 'Udma', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1735, 'Vadakara', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1736, 'Vaikom', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1737, 'Valapattanam', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1738, 'Vallachira', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1739, 'Varam', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1740, 'Varappuzha', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1741, 'Varkala', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1742, 'Vazhakkala', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1743, 'Venmanad', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1744, 'Villiappally', 3, 3, 1, 1, 1, '2020-09-10 12:45:12', '2020-09-10 12:45:12', NULL),
(1745, 'Amini', 3, 15, 1, 1, 1, '2020-09-10 12:46:36', '2020-09-10 12:46:36', NULL),
(1746, 'Kavaratti', 3, 15, 1, 1, 1, '2020-09-10 12:47:03', '2020-09-10 12:47:03', NULL),
(1747, 'Minicoy', 3, 15, 1, 1, 1, '2020-09-10 12:48:35', '2020-09-10 12:48:35', NULL),
(1748, 'Agar', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1749, 'Ajaigarh', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1750, 'Akoda', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1751, 'Akodia', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1752, 'Alampur', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1753, 'Alirajpur', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1754, 'Alot', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1755, 'Amanganj', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1756, 'Amarkantak', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1757, 'Amarpatan', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1758, 'Amarwara', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1759, 'Ambada', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1760, 'Ambah', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1761, 'Amla', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1762, 'Amlai', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1763, 'Anjad', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1764, 'Antari', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1765, 'Anuppur', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1766, 'Aron', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1767, 'Ashok Nagar', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1768, 'Ashta', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1769, 'Babai', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1770, 'Bada Malhera', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1771, 'Badagaon', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1772, 'Badagoan', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1773, 'Badarwas', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1774, 'Badawada', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1775, 'Badi', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1776, 'Badkuhi', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1777, 'Badnagar', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1778, 'Badnawar', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1779, 'Badod', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1780, 'Badoda', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1781, 'Badra', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1782, 'Bagh', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1783, 'Bagli', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1784, 'Baihar', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1785, 'Baikunthpur', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1786, 'Balaghat', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1787, 'Baldeogarh', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1788, 'Bamhani', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1789, 'Bamor', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1790, 'Bamora', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1791, 'Banda', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1792, 'Bangawan', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1793, 'Bansatar Kheda', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1794, 'Baraily', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1795, 'Barela', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1796, 'Barghat', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1797, 'Barhi', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1798, 'Barigarh', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1799, 'Barwaha', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1800, 'Barwani', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1801, 'Basoda', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1802, 'Begamganj', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1803, 'Beohari', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1804, 'Berasia', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1805, 'Betma', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1806, 'Betul', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1807, 'Betul-Bazar', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1808, 'Bhainsdehi', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1809, 'Bhamodi', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1810, 'Bhander', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1811, 'Bhanpura', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1812, 'Bharveli', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1813, 'Bhaurasa', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1814, 'Bhavra', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1815, 'Bhedaghat', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1816, 'Bhikangaon', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1817, 'Bhilakhedi', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1818, 'Bhind', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1819, 'Bhitarwar', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1820, 'Bhopal', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1821, 'Biaora', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1822, 'Bijawar', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1823, 'Bijeypur', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1824, 'Bijuri', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1825, 'Bilaua', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1826, 'Bilpura', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1827, 'Bina Etawa', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1828, 'Bina Rly Colony', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1829, 'Birsinghpur', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1830, 'Boda', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1831, 'Budhni', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1832, 'Burhanpur', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1833, 'Burhar', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1834, 'Buxwaha', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1835, 'Chachaura-Binaganj', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1836, 'Chakghat', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1837, 'Chandameta-Butaria', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1838, 'Chanderi', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1839, 'Chandia', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1840, 'Chandla', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1841, 'Chaurai Khas', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1842, 'Chhatarpur', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1843, 'Chhindwara', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1844, 'Chhota Chhindwara (Gotegaon)', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1845, 'Chichli', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1846, 'Chitrakoot', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1847, 'Churhat', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1848, 'Daboh', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1849, 'Dabra', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1850, 'Damoh', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1851, 'Damua', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1852, 'Datia', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1853, 'Deodara', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1854, 'Deori', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1855, 'Deori', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1856, 'Depalpur', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1857, 'Devendranagar', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL);
INSERT INTO `city` (`id`, `city`, `country_name`, `state_name`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1858, 'Devhara', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1859, 'Dewas', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1860, 'Dhamnod', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1861, 'Dhana', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1862, 'Dhanpuri', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1863, 'Dhar', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1864, 'Dharampuri', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1865, 'Dighawani', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1866, 'Diken', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1867, 'Dindori', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1868, 'Dola', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1869, 'Dongar Parasia', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1870, 'Dumar Kachhar', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1871, 'G.C.F Jabalpur', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1872, 'Gadarwara', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1873, 'Gairatganj', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1874, 'Garhakota', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1875, 'Garhi-Malhara', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1876, 'Garoth', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1877, 'Ghansor', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1878, 'Ghuwara', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1879, 'Gogapur', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1880, 'Gohad', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1881, 'Gormi', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1882, 'Govindgarh', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1883, 'Guna', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1884, 'Gurh', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1885, 'Gwalior', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1886, 'Hanumana', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1887, 'Harda', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1888, 'Harpalpur', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1889, 'Harrai', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1890, 'Harsud', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1891, 'Hatod', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1892, 'Hatpipalya', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1893, 'Hatta', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1894, 'Hindoria', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1895, 'Hirapur', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1896, 'Hoshangabad', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1897, 'Ichhawar', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1898, 'Iklehra', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1899, 'Indergarh', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1900, 'Indore', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1901, 'Isagarh', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1902, 'Itarsi', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1903, 'Jabalpur', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1904, 'Jabalpur Cantt.', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1905, 'Jaisinghnagar', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1906, 'Jaithari', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1907, 'Jaitwara', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1908, 'Jamai', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1909, 'Jaora', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1910, 'Jata Chhapar', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1911, 'Jatara', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1912, 'Jawad', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1913, 'Jawar', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1914, 'Jeron Khalsa', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1915, 'Jhabua', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1916, 'Jhundpura', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1917, 'Jiran', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1918, 'Jirapur', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1919, 'Jobat', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1920, 'Joura', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1921, 'Kailaras', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1922, 'Kakarhati', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1923, 'Kali Chhapar', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1924, 'Kanad', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1925, 'Kannod', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1926, 'Kantaphod', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1927, 'Kareli', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1928, 'Karera', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1929, 'Kari', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1930, 'Karnawad', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1931, 'Karrapur', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1932, 'Kasrawad', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1933, 'Katangi', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1934, 'Katangi', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1935, 'Kelhauri(chachai)', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1936, 'Khachrod', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1937, 'Khajuraho', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1938, 'Khand(Bansagar)', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1939, 'Khandwa', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1940, 'Khaniyadhana', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1941, 'Khargapur', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1942, 'Khargone', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1943, 'Khategaon', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1944, 'Khetia', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1945, 'Khilchipur', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1946, 'Khirkiya', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1947, 'Khujner', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1948, 'Khurai', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1949, 'Kolaras', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1950, 'Kotar', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1951, 'Kothi', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1952, 'Kotma', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1953, 'Kukshi', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1954, 'Kumbhraj', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1955, 'Kurwai', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1956, 'Kymore', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1957, 'Lahar', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1958, 'Lakhnadon', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1959, 'Lateri', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1960, 'Laundi', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1961, 'Lidhorakhas', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1962, 'Lodhikheda', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1963, 'Loharda', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1964, 'Machalpur', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1965, 'Maharajpur', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1966, 'Maheshwar', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1967, 'Mahidpur', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1968, 'Maihar', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1969, 'Majholi', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1970, 'Makronia', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1971, 'Maksi', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1972, 'Malaj Khand', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1973, 'Malhargarh', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1974, 'Manasa', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1975, 'Manawar', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1976, 'Mandav', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1977, 'Mandideep', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1978, 'Mandla', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1979, 'Mandleshwar', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1980, 'Mandsaur', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1981, 'Manegaon', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1982, 'Mangawan', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1983, 'Manglaya Sadak', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1984, 'Manpur', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1985, 'Mau', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1986, 'Mauganj', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1987, 'Meghnagar', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1988, 'Mehara Gaon', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1989, 'Mehgaon', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1990, 'Mhow Cantt.', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1991, 'Mhowgaon', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1992, 'Mihona', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1993, 'Mohgaon', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1994, 'Morar Cantt.', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1995, 'Morena', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1996, 'Multai', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1997, 'Mundi', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1998, 'Mungaoli', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(1999, 'Murwara (Katni)', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2000, 'Nagda', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2001, 'Nagod', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2002, 'Nagri', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2003, 'Nai Garhi', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2004, 'Nainpur', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2005, 'Nalkheda', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2006, 'Namli', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2007, 'Narayangarh', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2008, 'Narsimhapur', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2009, 'Narsinghgarh', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2010, 'Narwar', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2011, 'Nasrullaganj', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2012, 'Naudhia', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2013, 'Neemuch', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2014, 'Nepanagar', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2015, 'Neuton Chikhli Kalan', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2016, 'Niwari', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2017, 'Nowgaon', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2018, 'Nowrozabad(Khodargama)', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2019, 'O.F.Khamaria', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2020, 'Obedullaganj', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2021, 'Omkareshwar', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2022, 'Orachha', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2023, 'Ordinance Factory Itarsi', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2024, 'Pachmarhi Cantt', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2025, 'Pachore', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2026, 'Pal Chourai', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2027, 'Palda', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2028, 'Palera', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2029, 'Pali', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2030, 'Panagar', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2031, 'Panara', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2032, 'Pandhana', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2033, 'Pandhurna', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2034, 'Panna', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2035, 'Pansemal', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2036, 'Pasan', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2037, 'Patan', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2038, 'Patharia', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2039, 'Pawai', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2040, 'Petlawad', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2041, 'Phuphkalan', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2042, 'Pichhore', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2043, 'Pichhore', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2044, 'Pipariya', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2045, 'Pipariya', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2046, 'Piploda', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2047, 'Piplya Mandi', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2048, 'Pithampur', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2049, 'Polay Kalan', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2050, 'Porsa', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2051, 'Prithvipur', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2052, 'Raghogarh -Vijaypur', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2053, 'Rahatgarh', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2054, 'Raisen', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2055, 'Rajakhedi', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2056, 'Rajgarh', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2057, 'Rajgarh', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2058, 'Rajnagar', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2059, 'Rajpur', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2060, 'Rampur Baghelan', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2061, 'Rampur Naikin', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2062, 'Rampura', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2063, 'Ranapur', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2064, 'Ratangarh', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2065, 'Ratlam', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2066, 'Ratlam Rly. Colony (Ratlam Kasba)', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2067, 'Rau', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2068, 'Rehli', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2069, 'Rehti', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2070, 'Rewa', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2071, 'Runji Gautampura', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2072, 'Sabalgarh', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2073, 'Sagar', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2074, 'Sagar Cantt.', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2075, 'Sailana', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2076, 'Sanawad', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2077, 'Sanchi', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2078, 'Sarangpur', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2079, 'Sardarpur', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2080, 'Sarni', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2081, 'Satai', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2082, 'Satna', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2083, 'Satwas', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2084, 'Sausar', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2085, 'Sawer', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2086, 'Sehore', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2087, 'Semaria', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2088, 'Sendhwa', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2089, 'Seondha', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2090, 'Seoni', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2091, 'Seoni Malwa', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2092, 'Sethia', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2093, 'Shahdol', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2094, 'Shahgarh', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2095, 'Shahpur', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2096, 'Shahpur', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2097, 'Shahpur', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2098, 'Shahpura', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2099, 'Shahpura', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2100, 'Shajapur', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2101, 'Shamgarh', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2102, 'Sheopur', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2103, 'Shivpuri', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2104, 'Shujalpur', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2105, 'Sidhi', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2106, 'Sihora', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2107, 'Singoli', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2108, 'Singrauli', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2109, 'Sinhasa', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2110, 'Sirgora', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2111, 'Sirmaur', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2112, 'Sironj', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2113, 'Sitamau', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2114, 'Sohagpur', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2115, 'Sonkatch', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2116, 'Soyatkalan', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2117, 'Suhagi', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2118, 'Sultanpur', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2119, 'Susner', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2120, 'Suthaliya', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2121, 'Tal', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2122, 'Talen', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2123, 'Tarana', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2124, 'Taricharkalan', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2125, 'Tekanpur', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2126, 'Tendu Kheda', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2127, 'Teonthar', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2128, 'Thandla', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2129, 'Tikamgarh', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2130, 'Timarni', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2131, 'Tirodi', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2132, 'Udaipura', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2133, 'Ujjain', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2134, 'Ukwa', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2135, 'Umaria', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2136, 'Unchehara', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2137, 'Unhel', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2138, 'Vehicle Fac. Jabalpur', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2139, 'Vidisha', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2140, 'Vijayraghavgarh', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2141, 'Wara Seoni', 3, 31, 1, 1, 1, '2020-09-10 12:51:18', '2020-09-10 12:51:18', NULL),
(2142, 'Achalpur', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2143, 'Ahmadnagar', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2144, 'Ahmadnagar', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2145, 'Ahmadpur', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2146, 'Ajra', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2147, 'Akkalkot', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2148, 'Akola', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2149, 'Akot', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2150, 'Alandi', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2151, 'Alibag', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2152, 'Amalner', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2153, 'Ambad', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2154, 'Ambarnath', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2155, 'Ambejogai', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2156, 'Ambivali Tarf Wankhal', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2157, 'Amravati', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2158, 'Anjangaon', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2159, 'Arvi', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2160, 'Ashta', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2161, 'Aurangabad', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2162, 'Aurangabad', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2163, 'Ausa', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2164, 'Babhulgaon', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2165, 'Badlapur', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2166, 'Balapur', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2167, 'Ballarpur', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2168, 'Baramati', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2169, 'Barshi', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2170, 'Basmath', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2171, 'Bhadravati', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2172, 'Bhagur', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2173, 'Bhandara', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2174, 'Bhingar', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2175, 'Bhiwandi', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2176, 'Bhokardan', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2177, 'Bhor', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2178, 'Bhum', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2179, 'Bhusawal', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2180, 'Bid', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2181, 'Biloli', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2182, 'Birwadi', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2183, 'Boisar', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2184, 'Brahmapuri', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2185, 'Budhgaon', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2186, 'Buldana', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2187, 'Chakan', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2188, 'Chalisgaon', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2189, 'Chandrapur', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2190, 'Chandur', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2191, 'Chandur', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2192, 'Chandurbazar', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2193, 'Chicholi', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2194, 'Chikhaldara', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2195, 'Chikhli', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2196, 'Chinchani', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2197, 'Chiplun', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2198, 'Chopda', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2199, 'Dahanu', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2200, 'Dapoli Camp', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2201, 'Darwha', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2202, 'Daryapur Banosa', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2203, 'Dattapur Dhamangaon', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2204, 'Daund', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2205, 'Davlameti', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2206, 'Deglur', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2207, 'Dehu', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2208, 'Dehu Road', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2209, 'Deolali', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2210, 'Deolali Pravara', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2211, 'Deoli', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2212, 'Desaiganj', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2213, 'Deulgaon Raja', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2214, 'Dewhadi', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2215, 'Dharangaon', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2216, 'Dharmabad', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2217, 'Dharur', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2218, 'Dhatau', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2219, 'Dhule', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2220, 'Digdoh', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2221, 'Digras', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2222, 'Dondaicha-Warwade', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2223, 'Dudhani', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2224, 'Durgapur', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2225, 'Dyane', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2226, 'Eklahare', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2227, 'Erandol', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2228, 'Faizpur', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2229, 'Gadchiroli', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2230, 'Gadhinglaj', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2231, 'Gandhinagar', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2232, 'Ganeshpur', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2233, 'Gangakhed', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2234, 'Gangapur', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2235, 'Georai', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2236, 'Ghatanji', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2237, 'Ghoti Budruk', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2238, 'Ghugus', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2239, 'Ghulewadi', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2240, 'Godoli', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2241, 'Gokhivare', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2242, 'Gondiya', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2243, 'Goregaon', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2244, 'Greater Mumbai', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2245, 'Guhagar', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2246, 'Hadgaon', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2247, 'Hinganghat', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2248, 'Hingoli', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2249, 'Hupari', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2250, 'Ichalkaranji', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2251, 'Igatpuri', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2252, 'Indapur', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2253, 'Jalgaon', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2254, 'Jalgaon', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2255, 'Jalgaon (Jamod)', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2256, 'Jalna', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2257, 'Jamkhed', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2258, 'Jawhar', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2259, 'Jaysingpur', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2260, 'Jejuri', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2261, 'Jintur', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2262, 'Junnar', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2263, 'Kabnur', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2264, 'Kagal', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2265, 'Kalamb', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2266, 'Kalambe Turf Thane', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2267, 'Kalameshwar', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2268, 'Kalamnuri', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2269, 'Kalundre', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2270, 'Kalyan-Dombivali', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2271, 'Kamptee', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2272, 'Kamptee', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2273, 'Kandari', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2274, 'Kandhar', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2275, 'Kandri', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2276, 'Kandri', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2277, 'Kanhan (Pipri)', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2278, 'Kankavli', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2279, 'Kannad', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2280, 'Karad', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2281, 'Karanja', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2282, 'Karanje Turf Satara', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2283, 'Karivali', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2284, 'Karjat', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2285, 'Karmala', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2286, 'Kasara Budruk', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2287, 'Katai', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2288, 'Katkar', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2289, 'Katol', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2290, 'Kegaon', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2291, 'Khadkale', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2292, 'Khamgaon', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2293, 'Khapa', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2294, 'Khed', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2295, 'Khed', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2296, 'Kherdi', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2297, 'Khoni', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2298, 'Khopoli', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2299, 'Khuldabad', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2300, 'Kinwat', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2301, 'Kirkee', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2302, 'Kodoli', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2303, 'Kolhapur', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2304, 'Kon', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2305, 'Kondumal', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2306, 'Kopargaon', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2307, 'Kopharad', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2308, 'Korochi', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2309, 'Kudal', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2310, 'Kundalwadi', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2311, 'Kurduvadi', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2312, 'Kurundvad', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2313, 'Kusgaon Budruk', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2314, 'Lanja', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2315, 'Lasalgaon', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2316, 'Latur', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2317, 'Loha', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2318, 'Lonar', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2319, 'Lonavala', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2320, 'Madhavnagar', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2321, 'Mahabaleshwar', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2322, 'Mahad', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2323, 'Mahadula', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2324, 'Maindargi', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2325, 'Malegaon', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2326, 'Malkapur', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2327, 'Malkapur', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2328, 'Malkapur', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2329, 'Malwan', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2330, 'Manadur', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2331, 'Manchar', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2332, 'Mangalvedhe', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2333, 'Mangrulpir', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2334, 'Manjlegaon', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2335, 'Manmad', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2336, 'Manor', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2337, 'Mansar', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2338, 'Manwath', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2339, 'Matheran', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2340, 'Mehkar', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2341, 'Mhasla', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2342, 'Mhaswad', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2343, 'Mira-Bhayandar', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2344, 'Mohpa', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2345, 'Mohpada Alias Wasambe', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2346, 'Morshi', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2347, 'Mowad', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2348, 'Mudkhed', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2349, 'Mukhed', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2350, 'Mul', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2351, 'Murbad', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2352, 'Murgud', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2353, 'Murtijapur', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2354, 'Murud', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2355, 'Murum', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2356, 'Nachane', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2357, 'Nagapur', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2358, 'Nagardeole', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2359, 'Nagothana', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2360, 'Nagpur', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2361, 'Nakoda', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2362, 'Nala Sopara', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2363, 'Naldurg', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2364, 'Nanded-Waghala', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2365, 'Nandgaon', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2366, 'Nandura', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2367, 'Nandurbar', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2368, 'Narkhed', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2369, 'Nashik', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2370, 'Navghar-Manikpur', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2371, 'Navi Mumbai', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2372, 'Navi Mumbai (Panvel, Raigarh)', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2373, 'Nawapur', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2374, 'Neral', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2375, 'Nilanga', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2376, 'Nildoh', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2377, 'Nimbhore Budruk', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2378, 'Osmanabad', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2379, 'Ozar', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2380, 'Pachgaon', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2381, 'Pachora', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2382, 'Padagha', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2383, 'Paithan', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2384, 'Palghar', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2385, 'Pali', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2386, 'Panchgani', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2387, 'Pandharkaoda', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2388, 'Pandharpur', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2389, 'Pandharpur', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2390, 'Panhala', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2391, 'Panvel', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2392, 'Paranda', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2393, 'Parbhani', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2394, 'Parli', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2395, 'Parola', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2396, 'Partur', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2397, 'Pasthal', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2398, 'Patan', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2399, 'Pathardi', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2400, 'Pathri', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2401, 'Patur', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2402, 'Pauni', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2403, 'Pen', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2404, 'Peth Umri', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2405, 'Phaltan', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2406, 'Pimpri Chinchwad', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2407, 'Poladpur', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2408, 'Pulgaon', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2409, 'Pune', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2410, 'Pune', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2411, 'Purna', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2412, 'Purushottamnagar', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2413, 'Pusad', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2414, 'Rahimatpur', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2415, 'Rahta Pimplas', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2416, 'Rahuri', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2417, 'Rajapur', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2418, 'Rajgurunagar (Khed)', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2419, 'Rajur', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2420, 'Rajura', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2421, 'Ramtek', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2422, 'Ratnagiri', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2423, 'Raver', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2424, 'Risod', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2425, 'Roha Ashtami', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2426, 'Sailu', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2427, 'Sandor', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2428, 'Sangamner', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2429, 'Sangli-Miraj & Kupwad', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2430, 'Sangole', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2431, 'Sasti', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2432, 'Sasvad', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2433, 'Satana', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2434, 'Satara', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2435, 'Savda', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2436, 'Savner', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL);
INSERT INTO `city` (`id`, `city`, `country_name`, `state_name`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2437, 'Sawantwadi', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2438, 'Sawari Jawharnagar', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2439, 'Shahade', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2440, 'Shahapur', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2441, 'Shegaon', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2442, 'Shelar', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2443, 'Shendurjana', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2444, 'Shirdi', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2445, 'Shirpur-Warwade', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2446, 'Shirur', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2447, 'Shirwal', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2448, 'Shivajinagar', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2449, 'Shivatkar (Nira)', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2450, 'Shrigonda', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2451, 'Shrirampur', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2452, 'Shrirampur(Rural)', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2453, 'Shrivardhan', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2454, 'Sillewada', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2455, 'Sillod', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2456, 'Sindi', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2457, 'Sindi Turf Hindnagar', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2458, 'Sindkhed Raja', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2459, 'Singnapur', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2460, 'Sinnar', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2461, 'Solapur', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2462, 'Sonegaon (Nipani)', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2463, 'Sonpeth', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2464, 'Soyagaon', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2465, 'Surgana', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2466, 'Talegaon Dabhade', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2467, 'Talode', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2468, 'Taloje Panchnad', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2469, 'Tarapur', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2470, 'Tasgaon', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2471, 'Tathavade', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2472, 'Tekadi', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2473, 'Telhara', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2474, 'Thane', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2475, 'Tirora', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2476, 'Totaladoh', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2477, 'Trimbak', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2478, 'Tuljapur', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2479, 'Tumsar', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2480, 'Uchgaon', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2481, 'Udgir', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2482, 'Ulhasnagar', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2483, 'Umarga', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2484, 'Umarkhed', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2485, 'Umarsara', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2486, 'Umbar Pada Nandade', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2487, 'Umred', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2488, 'Umri Pragane Balapur', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2489, 'Uran', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2490, 'Uran Islampur', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2491, 'Utekhol', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2492, 'Vada', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2493, 'Vadgaon', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2494, 'Vadgaon Kasba', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2495, 'Vaijapur', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2496, 'Vanvadi (Sadashivgad)', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2497, 'Vasai', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2498, 'Vasantnagar', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2499, 'Vashind', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2500, 'Vengurla', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2501, 'Virar', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2502, 'Vita', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2503, 'Wadgaon Road', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2504, 'Wadi', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2505, 'Waghapur', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2506, 'Wai', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2507, 'Wajegaon', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2508, 'Walani', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2509, 'Waliv', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2510, 'Wanadongri', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2511, 'Wani', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2512, 'Wardha', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2513, 'Warora', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2514, 'Warud', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2515, 'Washim', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2516, 'Yavatmal', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2517, 'Yawal', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2518, 'Yerkheda', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2519, 'Yevla', 3, 16, 1, 1, 1, '2020-09-10 12:52:58', '2020-09-10 12:52:58', NULL),
(2520, 'Andro', 3, 17, 1, 1, 1, '2020-09-10 12:54:16', '2020-09-10 12:54:16', NULL),
(2521, 'Bishnupur', 3, 17, 1, 1, 1, '2020-09-10 12:54:16', '2020-09-10 12:54:16', NULL),
(2522, 'Heriok', 3, 17, 1, 1, 1, '2020-09-10 12:54:16', '2020-09-10 12:54:16', NULL),
(2523, 'Imphal', 3, 17, 1, 1, 1, '2020-09-10 12:54:16', '2020-09-10 12:54:16', NULL),
(2524, 'Jiribam', 3, 17, 1, 1, 1, '2020-09-10 12:54:16', '2020-09-10 12:54:16', NULL),
(2525, 'Kakching', 3, 17, 1, 1, 1, '2020-09-10 12:54:16', '2020-09-10 12:54:16', NULL),
(2526, 'Kakching Khunou', 3, 17, 1, 1, 1, '2020-09-10 12:54:16', '2020-09-10 12:54:16', NULL),
(2527, 'Khongman', 3, 17, 1, 1, 1, '2020-09-10 12:54:16', '2020-09-10 12:54:16', NULL),
(2528, 'Kumbi', 3, 17, 1, 1, 1, '2020-09-10 12:54:16', '2020-09-10 12:54:16', NULL),
(2529, 'Kwakta', 3, 17, 1, 1, 1, '2020-09-10 12:54:16', '2020-09-10 12:54:16', NULL),
(2530, 'Lamai', 3, 17, 1, 1, 1, '2020-09-10 12:54:16', '2020-09-10 12:54:16', NULL),
(2531, 'Lamjaotongba', 3, 17, 1, 1, 1, '2020-09-10 12:54:16', '2020-09-10 12:54:16', NULL),
(2532, 'Lamshang', 3, 17, 1, 1, 1, '2020-09-10 12:54:16', '2020-09-10 12:54:16', NULL),
(2533, 'Lilong (Imphal West)', 3, 17, 1, 1, 1, '2020-09-10 12:54:16', '2020-09-10 12:54:16', NULL),
(2534, 'Lilong (Thoubal)', 3, 17, 1, 1, 1, '2020-09-10 12:54:16', '2020-09-10 12:54:16', NULL),
(2535, 'Mayang Imphal', 3, 17, 1, 1, 1, '2020-09-10 12:54:16', '2020-09-10 12:54:16', NULL),
(2536, 'Moirang', 3, 17, 1, 1, 1, '2020-09-10 12:54:16', '2020-09-10 12:54:16', NULL),
(2537, 'Moreh', 3, 17, 1, 1, 1, '2020-09-10 12:54:16', '2020-09-10 12:54:16', NULL),
(2538, 'Nambol', 3, 17, 1, 1, 1, '2020-09-10 12:54:16', '2020-09-10 12:54:16', NULL),
(2539, 'Naoriya Pakhanglakpa', 3, 17, 1, 1, 1, '2020-09-10 12:54:16', '2020-09-10 12:54:16', NULL),
(2540, 'Ningthoukhong', 3, 17, 1, 1, 1, '2020-09-10 12:54:16', '2020-09-10 12:54:16', NULL),
(2541, 'Oinam', 3, 17, 1, 1, 1, '2020-09-10 12:54:16', '2020-09-10 12:54:16', NULL),
(2542, 'Porompat', 3, 17, 1, 1, 1, '2020-09-10 12:54:16', '2020-09-10 12:54:16', NULL),
(2543, 'Samurou', 3, 17, 1, 1, 1, '2020-09-10 12:54:16', '2020-09-10 12:54:16', NULL),
(2544, 'Sekmai Bazar', 3, 17, 1, 1, 1, '2020-09-10 12:54:16', '2020-09-10 12:54:16', NULL),
(2545, 'Sikhong Sekmai', 3, 17, 1, 1, 1, '2020-09-10 12:54:16', '2020-09-10 12:54:16', NULL),
(2546, 'Sugnu', 3, 17, 1, 1, 1, '2020-09-10 12:54:16', '2020-09-10 12:54:16', NULL),
(2547, 'Thongkhong Laxmi Bazar', 3, 17, 1, 1, 1, '2020-09-10 12:54:16', '2020-09-10 12:54:16', NULL),
(2548, 'Thoubal', 3, 17, 1, 1, 1, '2020-09-10 12:54:16', '2020-09-10 12:54:16', NULL),
(2549, 'Torban (Khetri Leikai)', 3, 17, 1, 1, 1, '2020-09-10 12:54:16', '2020-09-10 12:54:16', NULL),
(2550, 'Wangjing', 3, 17, 1, 1, 1, '2020-09-10 12:54:16', '2020-09-10 12:54:16', NULL),
(2551, 'Wangoi', 3, 17, 1, 1, 1, '2020-09-10 12:54:16', '2020-09-10 12:54:16', NULL),
(2552, 'Yairipok', 3, 17, 1, 1, 1, '2020-09-10 12:54:16', '2020-09-10 12:54:16', NULL),
(2553, 'Baghmara', 3, 18, 1, 1, 1, '2020-09-10 12:55:32', '2020-09-10 12:55:32', NULL),
(2554, 'Cherapunjee', 3, 18, 1, 1, 1, '2020-09-10 12:55:32', '2020-09-10 12:55:32', NULL),
(2555, 'Jawai', 3, 18, 1, 1, 1, '2020-09-10 12:55:32', '2020-09-10 12:55:32', NULL),
(2556, 'Madanrting', 3, 18, 1, 1, 1, '2020-09-10 12:55:32', '2020-09-10 12:55:32', NULL),
(2557, 'Mairang', 3, 18, 1, 1, 1, '2020-09-10 12:55:32', '2020-09-10 12:55:32', NULL),
(2558, 'Mawlai', 3, 18, 1, 1, 1, '2020-09-10 12:55:32', '2020-09-10 12:55:32', NULL),
(2559, 'Nongmynsong', 3, 18, 1, 1, 1, '2020-09-10 12:55:32', '2020-09-10 12:55:32', NULL),
(2560, 'Nongpoh', 3, 18, 1, 1, 1, '2020-09-10 12:55:32', '2020-09-10 12:55:32', NULL),
(2561, 'Nongstoin', 3, 18, 1, 1, 1, '2020-09-10 12:55:32', '2020-09-10 12:55:32', NULL),
(2562, 'Nongthymmai', 3, 18, 1, 1, 1, '2020-09-10 12:55:32', '2020-09-10 12:55:32', NULL),
(2563, 'Pynthorumkhrah', 3, 18, 1, 1, 1, '2020-09-10 12:55:32', '2020-09-10 12:55:32', NULL),
(2564, 'Resubelpara', 3, 18, 1, 1, 1, '2020-09-10 12:55:32', '2020-09-10 12:55:32', NULL),
(2565, 'Shillong', 3, 18, 1, 1, 1, '2020-09-10 12:55:32', '2020-09-10 12:55:32', NULL),
(2566, 'Shillong Cantt.', 3, 18, 1, 1, 1, '2020-09-10 12:55:32', '2020-09-10 12:55:32', NULL),
(2567, 'Tura', 3, 18, 1, 1, 1, '2020-09-10 12:55:32', '2020-09-10 12:55:32', NULL),
(2568, 'Williamnagar', 3, 18, 1, 1, 1, '2020-09-10 12:55:32', '2020-09-10 12:55:32', NULL),
(2569, 'Aizawl', 3, 19, 1, 1, 1, '2020-09-10 12:57:10', '2020-09-10 12:57:10', NULL),
(2570, 'Bairabi', 3, 19, 1, 1, 1, '2020-09-10 12:57:10', '2020-09-10 12:57:10', NULL),
(2571, 'Biate', 3, 19, 1, 1, 1, '2020-09-10 12:57:10', '2020-09-10 12:57:10', NULL),
(2572, 'Champhai', 3, 19, 1, 1, 1, '2020-09-10 12:57:10', '2020-09-10 12:57:10', NULL),
(2573, 'Darlawn', 3, 19, 1, 1, 1, '2020-09-10 12:57:10', '2020-09-10 12:57:10', NULL),
(2574, 'Hnahthial', 3, 19, 1, 1, 1, '2020-09-10 12:57:10', '2020-09-10 12:57:10', NULL),
(2575, 'Khawhai', 3, 19, 1, 1, 1, '2020-09-10 12:57:10', '2020-09-10 12:57:10', NULL),
(2576, 'Khawzawl', 3, 19, 1, 1, 1, '2020-09-10 12:57:10', '2020-09-10 12:57:10', NULL),
(2577, 'Kolasib', 3, 19, 1, 1, 1, '2020-09-10 12:57:10', '2020-09-10 12:57:10', NULL),
(2578, 'Lengpui', 3, 19, 1, 1, 1, '2020-09-10 12:57:10', '2020-09-10 12:57:10', NULL),
(2579, 'Lunglei', 3, 19, 1, 1, 1, '2020-09-10 12:57:10', '2020-09-10 12:57:10', NULL),
(2580, 'Mamit', 3, 19, 1, 1, 1, '2020-09-10 12:57:10', '2020-09-10 12:57:10', NULL),
(2581, 'N. Vanlaiphai', 3, 19, 1, 1, 1, '2020-09-10 12:57:10', '2020-09-10 12:57:10', NULL),
(2582, 'N.Kawnpui', 3, 19, 1, 1, 1, '2020-09-10 12:57:10', '2020-09-10 12:57:10', NULL),
(2583, 'Saiha', 3, 19, 1, 1, 1, '2020-09-10 12:57:10', '2020-09-10 12:57:10', NULL),
(2584, 'Sairang', 3, 19, 1, 1, 1, '2020-09-10 12:57:10', '2020-09-10 12:57:10', NULL),
(2585, 'Saitual', 3, 19, 1, 1, 1, '2020-09-10 12:57:10', '2020-09-10 12:57:10', NULL),
(2586, 'Serchhip', 3, 19, 1, 1, 1, '2020-09-10 12:57:10', '2020-09-10 12:57:10', NULL),
(2587, 'Thenzawl', 3, 19, 1, 1, 1, '2020-09-10 12:57:10', '2020-09-10 12:57:10', NULL),
(2588, 'Tlabung', 3, 19, 1, 1, 1, '2020-09-10 12:57:10', '2020-09-10 12:57:10', NULL),
(2589, 'Vairengte', 3, 19, 1, 1, 1, '2020-09-10 12:57:10', '2020-09-10 12:57:10', NULL),
(2590, 'Zawlnuam', 3, 19, 1, 1, 1, '2020-09-10 12:57:10', '2020-09-10 12:57:10', NULL),
(2591, 'Chumukedima', 3, 20, 1, 1, 1, '2020-09-10 12:58:27', '2020-09-10 12:58:27', NULL),
(2592, 'Dimapur', 3, 20, 1, 1, 1, '2020-09-10 12:58:27', '2020-09-10 12:58:27', NULL),
(2593, 'Kohima', 3, 20, 1, 1, 1, '2020-09-10 12:58:27', '2020-09-10 12:58:27', NULL),
(2594, 'Mokokchung', 3, 20, 1, 1, 1, '2020-09-10 12:58:27', '2020-09-10 12:58:27', NULL),
(2595, 'Mon', 3, 20, 1, 1, 1, '2020-09-10 12:58:27', '2020-09-10 12:58:27', NULL),
(2596, 'Phek', 3, 20, 1, 1, 1, '2020-09-10 12:58:27', '2020-09-10 12:58:27', NULL),
(2597, 'Tuensang', 3, 20, 1, 1, 1, '2020-09-10 12:58:27', '2020-09-10 12:58:27', NULL),
(2598, 'Wokha', 3, 20, 1, 1, 1, '2020-09-10 12:58:27', '2020-09-10 12:58:27', NULL),
(2599, 'Zunheboto', 3, 20, 1, 1, 1, '2020-09-10 12:58:27', '2020-09-10 12:58:27', NULL),
(2600, 'Kamakshyanagar', 3, 21, 1, 1, 1, '2020-09-11 13:40:13', '2020-09-11 13:40:13', NULL),
(2601, 'Kantabanji', 3, 21, 1, 1, 1, '2020-09-11 13:40:13', '2020-09-11 13:40:13', NULL),
(2602, 'Kantilo', 3, 21, 1, 1, 1, '2020-09-11 13:40:13', '2020-09-11 13:40:13', NULL),
(2603, 'Karanjia', 3, 21, 1, 1, 1, '2020-09-11 13:40:13', '2020-09-11 13:40:13', NULL),
(2604, 'Kashinagara', 3, 21, 1, 1, 1, '2020-09-11 13:40:13', '2020-09-11 13:40:13', NULL),
(2605, 'Kavisuryanagar', 3, 21, 1, 1, 1, '2020-09-11 13:40:13', '2020-09-11 13:40:13', NULL),
(2606, 'Kendrapara', 3, 21, 1, 1, 1, '2020-09-11 13:40:13', '2020-09-11 13:40:13', NULL),
(2607, 'Kendujhar', 3, 21, 1, 1, 1, '2020-09-11 13:40:13', '2020-09-11 13:40:13', NULL),
(2608, 'Kesinga', 3, 21, 1, 1, 1, '2020-09-11 13:40:13', '2020-09-11 13:40:13', NULL),
(2609, 'Khaliapali', 3, 21, 1, 1, 1, '2020-09-11 13:40:13', '2020-09-11 13:40:13', NULL),
(2610, 'Khalikote', 3, 21, 1, 1, 1, '2020-09-11 13:40:13', '2020-09-11 13:40:13', NULL),
(2611, 'Khandapada', 3, 21, 1, 1, 1, '2020-09-11 13:40:13', '2020-09-11 13:40:13', NULL),
(2612, 'Khariar', 3, 21, 1, 1, 1, '2020-09-11 13:40:13', '2020-09-11 13:40:13', NULL),
(2613, 'Khariar Road', 3, 21, 1, 1, 1, '2020-09-11 13:40:13', '2020-09-11 13:40:13', NULL),
(2614, 'Khatiguda', 3, 21, 1, 1, 1, '2020-09-11 13:40:13', '2020-09-11 13:40:13', NULL),
(2615, 'Khordha', 3, 21, 1, 1, 1, '2020-09-11 13:40:13', '2020-09-11 13:40:13', NULL),
(2616, 'Kochinda', 3, 21, 1, 1, 1, '2020-09-11 13:40:13', '2020-09-11 13:40:13', NULL),
(2617, 'Kodala', 3, 21, 1, 1, 1, '2020-09-11 13:40:13', '2020-09-11 13:40:13', NULL),
(2618, 'Konark', 3, 21, 1, 1, 1, '2020-09-11 13:40:13', '2020-09-11 13:40:13', NULL),
(2619, 'Koraput', 3, 21, 1, 1, 1, '2020-09-11 13:40:13', '2020-09-11 13:40:13', NULL),
(2620, 'Kotpad', 3, 21, 1, 1, 1, '2020-09-11 13:40:13', '2020-09-11 13:40:13', NULL),
(2621, 'Lattikata', 3, 21, 1, 1, 1, '2020-09-11 13:40:13', '2020-09-11 13:40:13', NULL),
(2622, 'Makundapur', 3, 21, 1, 1, 1, '2020-09-11 13:40:13', '2020-09-11 13:40:13', NULL),
(2623, 'Malkangiri', 3, 21, 1, 1, 1, '2020-09-11 13:40:13', '2020-09-11 13:40:13', NULL),
(2624, 'Mukhiguda', 3, 21, 1, 1, 1, '2020-09-11 13:40:13', '2020-09-11 13:40:13', NULL),
(2625, 'Nabarangapur', 3, 21, 1, 1, 1, '2020-09-11 13:40:13', '2020-09-11 13:40:13', NULL),
(2626, 'NALCO', 3, 21, 1, 1, 1, '2020-09-11 13:40:13', '2020-09-11 13:40:13', NULL),
(2627, 'Nayagarh', 3, 21, 1, 1, 1, '2020-09-11 13:40:13', '2020-09-11 13:40:13', NULL),
(2628, 'Nilagiri', 3, 21, 1, 1, 1, '2020-09-11 13:40:13', '2020-09-11 13:40:13', NULL),
(2629, 'Nimapada', 3, 21, 1, 1, 1, '2020-09-11 13:40:13', '2020-09-11 13:40:13', NULL),
(2630, 'Nuapatna', 3, 21, 1, 1, 1, '2020-09-11 13:40:13', '2020-09-11 13:40:13', NULL),
(2631, 'OCL Industrialship', 3, 21, 1, 1, 1, '2020-09-11 13:40:13', '2020-09-11 13:40:13', NULL),
(2632, 'Padmapur', 3, 21, 1, 1, 1, '2020-09-11 13:40:13', '2020-09-11 13:40:13', NULL),
(2633, 'Panposh', 3, 21, 1, 1, 1, '2020-09-11 13:40:13', '2020-09-11 13:40:13', NULL),
(2634, 'Paradip', 3, 21, 1, 1, 1, '2020-09-11 13:40:13', '2020-09-11 13:40:13', NULL),
(2635, 'Parlakhemundi', 3, 21, 1, 1, 1, '2020-09-11 13:40:13', '2020-09-11 13:40:13', NULL),
(2636, 'Patnagarh', 3, 21, 1, 1, 1, '2020-09-11 13:40:13', '2020-09-11 13:40:13', NULL),
(2637, 'Pattamundai', 3, 21, 1, 1, 1, '2020-09-11 13:40:13', '2020-09-11 13:40:13', NULL),
(2638, 'Phulabani', 3, 21, 1, 1, 1, '2020-09-11 13:40:13', '2020-09-11 13:40:13', NULL),
(2639, 'Pipili', 3, 21, 1, 1, 1, '2020-09-11 13:40:13', '2020-09-11 13:40:13', NULL),
(2640, 'Polasara', 3, 21, 1, 1, 1, '2020-09-11 13:40:13', '2020-09-11 13:40:13', NULL),
(2641, 'Pratapsasan', 3, 21, 1, 1, 1, '2020-09-11 13:40:13', '2020-09-11 13:40:13', NULL),
(2642, 'Puri', 3, 21, 1, 1, 1, '2020-09-11 13:40:13', '2020-09-11 13:40:13', NULL),
(2643, 'Purusottampur', 3, 21, 1, 1, 1, '2020-09-11 13:40:13', '2020-09-11 13:40:13', NULL),
(2644, 'Rairangpur', 3, 21, 1, 1, 1, '2020-09-11 13:40:13', '2020-09-11 13:40:13', NULL),
(2645, 'Rajagangapur', 3, 21, 1, 1, 1, '2020-09-11 13:40:13', '2020-09-11 13:40:13', NULL),
(2646, 'Rambha', 3, 21, 1, 1, 1, '2020-09-11 13:40:13', '2020-09-11 13:40:13', NULL),
(2647, 'Raurkela', 3, 21, 1, 1, 1, '2020-09-11 13:40:13', '2020-09-11 13:40:13', NULL),
(2648, 'Raurkela Industrialship', 3, 21, 1, 1, 1, '2020-09-11 13:40:13', '2020-09-11 13:40:13', NULL),
(2649, 'Rayagada', 3, 21, 1, 1, 1, '2020-09-11 13:40:13', '2020-09-11 13:40:13', NULL),
(2650, 'Redhakhol', 3, 21, 1, 1, 1, '2020-09-11 13:40:13', '2020-09-11 13:40:13', NULL),
(2651, 'Remuna', 3, 21, 1, 1, 1, '2020-09-11 13:40:13', '2020-09-11 13:40:13', NULL),
(2652, 'Rengali Dam Projectship', 3, 21, 1, 1, 1, '2020-09-11 13:40:13', '2020-09-11 13:40:13', NULL),
(2653, 'Sambalpur', 3, 21, 1, 1, 1, '2020-09-11 13:40:13', '2020-09-11 13:40:13', NULL),
(2654, 'Sonapur', 3, 21, 1, 1, 1, '2020-09-11 13:40:13', '2020-09-11 13:40:13', NULL),
(2655, 'Soro', 3, 21, 1, 1, 1, '2020-09-11 13:40:13', '2020-09-11 13:40:13', NULL),
(2656, 'Sunabeda', 3, 21, 1, 1, 1, '2020-09-11 13:40:13', '2020-09-11 13:40:13', NULL),
(2657, 'Sundargarh', 3, 21, 1, 1, 1, '2020-09-11 13:40:13', '2020-09-11 13:40:13', NULL),
(2658, 'Surada', 3, 21, 1, 1, 1, '2020-09-11 13:40:13', '2020-09-11 13:40:13', NULL),
(2659, 'Talcher', 3, 21, 1, 1, 1, '2020-09-11 13:40:13', '2020-09-11 13:40:13', NULL),
(2660, 'Talcher Thermal Power Station Township', 3, 21, 1, 1, 1, '2020-09-11 13:40:13', '2020-09-11 13:40:13', NULL),
(2661, 'Tarbha', 3, 21, 1, 1, 1, '2020-09-11 13:40:13', '2020-09-11 13:40:13', NULL),
(2662, 'Tensa', 3, 21, 1, 1, 1, '2020-09-11 13:40:13', '2020-09-11 13:40:13', NULL),
(2663, 'Titlagarh', 3, 21, 1, 1, 1, '2020-09-11 13:40:13', '2020-09-11 13:40:13', NULL),
(2664, 'Udala', 3, 21, 1, 1, 1, '2020-09-11 13:40:13', '2020-09-11 13:40:13', NULL),
(2665, 'Umarkote', 3, 21, 1, 1, 1, '2020-09-11 13:40:13', '2020-09-11 13:40:13', NULL),
(2666, 'Karaikal', 3, 22, 1, 1, 1, '2020-09-11 13:41:59', '2020-09-11 13:41:59', NULL),
(2667, 'Kurumbapet', 3, 22, 1, 1, 1, '2020-09-11 13:41:59', '2020-09-11 13:41:59', NULL),
(2668, 'Mahe', 3, 22, 1, 1, 1, '2020-09-11 13:41:59', '2020-09-11 13:41:59', NULL),
(2669, 'Ozhukarai', 3, 22, 1, 1, 1, '2020-09-11 13:41:59', '2020-09-11 13:41:59', NULL),
(2670, 'Puducherry', 3, 22, 1, 1, 1, '2020-09-11 13:41:59', '2020-09-11 13:41:59', NULL),
(2671, 'Yanam', 3, 22, 1, 1, 1, '2020-09-11 13:41:59', '2020-09-11 13:41:59', NULL),
(2672, 'Abohar', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2673, 'Adampur', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2674, 'Ahmedgarh', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2675, 'Ajnala', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2676, 'Akalgarh', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2677, 'Alawalpur', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2678, 'Amloh', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2679, 'Amritsar', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2680, 'Amritsar Cantt.', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2681, 'Anandpur Sahib', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2682, 'Badhni Kalan', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2683, 'Bagha Purana', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2684, 'Balachaur', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2685, 'Banaur', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2686, 'Banga', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2687, 'Baretta', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2688, 'Bariwala', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2689, 'Barnala', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2690, 'Bassi Pathana', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2691, 'Batala', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2692, 'Bathinda', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2693, 'Begowal', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2694, 'Bhabat', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2695, 'Bhadaur', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2696, 'Bhankharpur', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2697, 'Bharoli Kalan', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2698, 'Bhawanigarh', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2699, 'Bhikhi', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2700, 'Bhikhiwind', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2701, 'Bhisiana', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2702, 'Bhogpur', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2703, 'Bhucho Mandi', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2704, 'Bhulath', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2705, 'Budha Theh', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2706, 'Budhlada', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2707, 'Cheema', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2708, 'Chohal', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2709, 'Daroha', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2710, 'Dasua', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2711, 'Daulatpur', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2712, 'Dera Baba Nanak', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2713, 'Dera Bassi', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2714, 'Dhanaula', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2715, 'Dharamkot', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2716, 'Dhariwal', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2717, 'Dhilwan', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2718, 'Dhuri', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2719, 'Dina Nagar', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2720, 'Dirba', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2721, 'Faridkot', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2722, 'Fatehgarh Churian', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2723, 'Fazilka', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2724, 'Fiozpur', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2725, 'Firozpur Cantt.', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2726, 'Gardhiwala', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2727, 'Garhshanker', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2728, 'Ghagga', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2729, 'Ghanaur', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2730, 'Gidderbaha', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2731, 'Gobindgarh', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2732, 'Goniana', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2733, 'Goraya', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2734, 'Gurdaspur', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2735, 'Guru Har Sahai', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2736, 'Hajipur', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2737, 'Handiaya', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2738, 'Hariana', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2739, 'Hoshiarpur', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2740, 'Hussainpur', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2741, 'Jagraon', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2742, 'Jaitu', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2743, 'Jalalabad', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2744, 'Jalandhar', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2745, 'Jalandhar Cantt.', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2746, 'Jandiala', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2747, 'Jandiala', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2748, 'Jugial', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2749, 'Kalanaur', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2750, 'Kapurthala', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2751, 'Karoran', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2752, 'Kartarpur', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2753, 'Khamanon', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2754, 'Khanauri', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2755, 'Khanna', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2756, 'Kharar', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2757, 'Khem Karan', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2758, 'Kot Fatta', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2759, 'Kot Kapura', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2760, 'Kurali', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2761, 'Lehragaga', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2762, 'Lodhian Khas', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2763, 'Longowal', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2764, 'Ludhiana', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2765, 'Machhiwara', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2766, 'Mahilpur', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2767, 'Majitha', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2768, 'Makhu', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2769, 'Malerkotla', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2770, 'Maloud', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2771, 'Malout', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2772, 'Mansa', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2773, 'Maur', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2774, 'Moga', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2775, 'Moonak', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2776, 'Morinda', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2777, 'Mukerian', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2778, 'Muktsar', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2779, 'Mullanpur Dakha', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2780, 'Mullanpur- Garibdas', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2781, 'Nabha', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2782, 'Nakodar', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2783, 'Nangal', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2784, 'Nawanshahr', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2785, 'Nehon', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2786, 'Noor Mahal', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2787, 'Pathankot', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2788, 'Patiala', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2789, 'Patti', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2790, 'Pattran', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2791, 'Payal', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2792, 'Phagwara', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2793, 'Phillaur', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2794, 'Qadian', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2795, 'Rahon', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2796, 'Raikot', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2797, 'Rajasansi', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2798, 'Rajpura', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2799, 'Raman', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2800, 'Ramdass', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2801, 'Rampura Phul', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2802, 'Rayya', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2803, 'Rupnagar', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2804, 'Rurki Kasba', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2805, 'S.A.S. Nagar (Mohali)', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2806, 'Sahnewal', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2807, 'Samana', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2808, 'Samrala', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2809, 'Sanaur', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2810, 'Sangat', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2811, 'Sangrur', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2812, 'Sansarpur', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2813, 'Sardulgarh', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2814, 'Shahkot', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2815, 'Shamchaurasi', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2816, 'Shekhpura', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2817, 'Sirhind -Fategarh', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2818, 'Sri Hargobindpur', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2819, 'Sujanpur', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2820, 'Sultanpur Lodhi', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2821, 'Sunam', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2822, 'Talwandi Bhai', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2823, 'Talwara', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2824, 'Tappa', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2825, 'Tarn Taran', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2826, 'Urmar Tanda', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2827, 'Zira', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2828, 'Zirakpur', 3, 2, 1, 1, 1, '2020-09-11 13:44:14', '2020-09-11 13:44:14', NULL),
(2829, 'Abu Road', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2830, 'Ajmer', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2831, 'Aklera', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2832, 'Alwar', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2833, 'Amet', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2834, 'Antah', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2835, 'Anupgarh', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2836, 'Asind', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2837, 'Baggar', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2838, 'Bagru', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2839, 'Bakani', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2840, 'Bali', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2841, 'Balotra', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2842, 'Bandikui', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2843, 'Banswara', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2844, 'Baran', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2845, 'Bari', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2846, 'Bari Sadri', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2847, 'Barmer', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2848, 'Basni Belima', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2849, 'Bayana', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2850, 'Beawar', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2851, 'Beejoliya Kalan', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2852, 'Begun', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2853, 'Behat', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2854, 'Behror', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2855, 'Bhadra', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2856, 'Bhalariya', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2857, 'Bharatpur', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2858, 'Bhawani Mandi', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2859, 'Bhilwara', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2860, 'Bhinder', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2861, 'Bhinmal', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2862, 'Bhiwadi', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2863, 'Bhusawar', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2864, 'Bidasar', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2865, 'Bikaner', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2866, 'Bilara', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2867, 'Bissau', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2868, 'Budhpura', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2869, 'Bundi', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2870, 'Chaksu', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2871, 'Chechat', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2872, 'Chhabra', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2873, 'Chhapar', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2874, 'Chhipabarod', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2875, 'Chhoti Sadri', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2876, 'Chirawa', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2877, 'Chittaurgarh', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2878, 'Chomu', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2879, 'Churu', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2880, 'Dariba', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2881, 'Dausa', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2882, 'Deeg', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2883, 'Deoli', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2884, 'Deshnoke', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2885, 'Devgarh', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2886, 'Dhariawad', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2887, 'Dhaulpur', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2888, 'Didwana', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2889, 'Dungargarh', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2890, 'Dungarpur', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2891, 'Fatehnagar', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2892, 'Fatehpur', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2893, 'Gajsinghpur', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2894, 'Galiakot', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2895, 'Ganganagar', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2896, 'Gangapur', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2897, 'Gangapur City', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2898, 'Goredi Chancha', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2899, 'Gothra', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2900, 'Govindgarh', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2901, 'Gulabpura', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2902, 'Hanumangarh', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2903, 'Hindaun', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2904, 'Indragarh', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2905, 'Jahazpur', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2906, 'Jaipur', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2907, 'Jaisalmer', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2908, 'Jaitaran', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2909, 'Jalor', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2910, 'Jhalawar', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2911, 'Jhalrapatan', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2912, 'Jhunjhunun', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2913, 'Jobner', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2914, 'Jodhpur', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2915, 'Kaithoon', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2916, 'Kaman', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2917, 'Kanor', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2918, 'Kapasan', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2919, 'Kaprain', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2920, 'Karanpur', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2921, 'Karauli', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2922, 'Kekri', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2923, 'Keshoraipatan', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2924, 'Kesrisinghpur', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2925, 'Khairthal', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2926, 'Khandela', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2927, 'Kherli', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2928, 'Kherliganj', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2929, 'Kherwara Chhaoni', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2930, 'Khetri', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2931, 'Kiranipura', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2932, 'Kishangarh', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2933, 'Kishangarh', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2934, 'Kishangarh Renwal', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2935, 'Kolvi Mandi Rajendra pura', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2936, 'Kota', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2937, 'Kotputli', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2938, 'Kuchaman City', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2939, 'Kuchera', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2940, 'Kumbhkot', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2941, 'Kumher', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2942, 'Kushalgarh', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2943, 'Lachhmangarh', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2944, 'Ladnu', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2945, 'Lakheri', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2946, 'Lalsot', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2947, 'Losal', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2948, 'Mahu Kalan', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2949, 'Mahwa', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2950, 'Makrana', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2951, 'Malpura', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2952, 'Mandalgarh', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2953, 'Mandawa', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2954, 'Mandawar', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2955, 'Mangrol', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2956, 'Manohar Thana', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2957, 'Marwar Junction', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2958, 'Merta City', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2959, 'Modak', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2960, 'Mount Abu', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2961, 'Mukandgarh', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2962, 'Mundwa', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2963, 'Nadbai', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2964, 'Nagar', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2965, 'Nagaur', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2966, 'Nainwa', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2967, 'Nasirabad', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2968, 'Nathdwara', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2969, 'Nawa', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2970, 'Nawalgarh', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2971, 'Neem-Ka-Thana', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2972, 'Newa Talai', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2973, 'Nimbahera', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2974, 'Niwai', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2975, 'Nohar', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2976, 'Nokha', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2977, 'Padampur', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2978, 'Pali', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2979, 'Parbatsar', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2980, 'Partapur', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2981, 'Phalna', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2982, 'Phalodi', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2983, 'Phulera', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2984, 'Pilani', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2985, 'Pilibanga', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2986, 'Pindwara', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2987, 'Piparcity', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2988, 'Pirawa', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2989, 'Pokaran', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2990, 'Pratapgarh', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2991, 'Pushkar', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2992, 'Raisinghnagar', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2993, 'Rajakhera', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2994, 'Rajaldesar', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2995, 'Rajgarh', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2996, 'Rajgarh', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2997, 'Rajsamand', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2998, 'Ramganj Mandi', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(2999, 'Ramgarh', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(3000, 'Rani', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(3001, 'Ratangarh', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(3002, 'Ratannagar', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(3003, 'Rawatbhata', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(3004, 'Rawatsar', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(3005, 'Reengus', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(3006, 'Rikhabdeo', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(3007, 'Sadri', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(3008, 'Sadulshahar', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(3009, 'Sagwara', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(3010, 'Salumbar', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(3011, 'Sambhar', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(3012, 'Sanchore', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(3013, 'Sangaria', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(3014, 'Sangod', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL);
INSERT INTO `city` (`id`, `city`, `country_name`, `state_name`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(3015, 'Sardarshahar', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(3016, 'Sarwar', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(3017, 'Satalkheri', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(3018, 'Sawai Madhopur', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(3019, 'Shahpura', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(3020, 'Shahpura', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(3021, 'Sheoganj', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(3022, 'Sikar', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(3023, 'Sirohi', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(3024, 'Sogariya', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(3025, 'Sojat', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(3026, 'Sojat Road', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(3027, 'Sri Madhopur', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(3028, 'Sujangarh', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(3029, 'Suket', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(3030, 'Sumerpur', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(3031, 'Surajgarh', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(3032, 'Suratgarh', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(3033, 'Takhatgarh', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(3034, 'Taranagar', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(3035, 'Tijara', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(3036, 'Todabhim', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(3037, 'Todaraisingh', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(3038, 'Todra', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(3039, 'Tonk', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(3040, 'Udaipur', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(3041, 'Udaipurwati', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(3042, 'Udpura', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(3043, 'Uniara', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(3044, 'Vanasthali', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(3045, 'Vidyavihar', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(3046, 'Vijainagar', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(3047, 'Vijainagar', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(3048, 'Viratnagar', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(3049, 'Weir', 3, 23, 1, 1, 1, '2020-09-11 13:49:38', '2020-09-11 13:49:38', NULL),
(3050, 'Gangtok', 3, 24, 1, 1, 1, '2020-09-11 13:51:05', '2020-09-11 13:51:05', NULL),
(3051, 'Gyalshing', 3, 24, 1, 1, 1, '2020-09-11 13:51:05', '2020-09-11 13:51:05', NULL),
(3052, 'Jorethang', 3, 24, 1, 1, 1, '2020-09-11 13:51:05', '2020-09-11 13:51:05', NULL),
(3053, 'Mangan', 3, 24, 1, 1, 1, '2020-09-11 13:51:05', '2020-09-11 13:51:05', NULL),
(3054, 'Namchi', 3, 24, 1, 1, 1, '2020-09-11 13:51:05', '2020-09-11 13:51:05', NULL),
(3055, 'Nayabazar', 3, 24, 1, 1, 1, '2020-09-11 13:51:05', '2020-09-11 13:51:05', NULL),
(3056, 'Rangpo', 3, 24, 1, 1, 1, '2020-09-11 13:51:05', '2020-09-11 13:51:05', NULL),
(3057, 'Singtam', 3, 24, 1, 1, 1, '2020-09-11 13:51:05', '2020-09-11 13:51:05', NULL),
(3058, 'Kattumannarkoil', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3059, 'Kattuputhur', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3060, 'Kaveripakkam', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3061, 'Kaveripattinam', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3062, 'Kayalpattinam', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3063, 'Kayatharu', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3064, 'Keelakarai', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3065, 'Keeramangalam', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3066, 'Keeranur', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3067, 'Keeranur', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3068, 'Keeripatti', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3069, 'Keezhapavur', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3070, 'Kelamangalam', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3071, 'Kembainaickenpalayam', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3072, 'Kethi', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3073, 'Kilampadi', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3074, 'Kilkulam', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3075, 'Kilkunda', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3076, 'Killiyur', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3077, 'Killlai', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3078, 'Kilpennathur', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3079, 'Kilvelur', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3080, 'Kinathukadavu', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3081, 'Kodaikanal', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3082, 'Kodavasal', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3083, 'Kodumudi', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3084, 'Kolachal', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3085, 'Kolappalur', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3086, 'Kolathupalayam', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3087, 'Kolathur', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3088, 'Kollankodu', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3089, 'Kollankoil', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3090, 'Komaralingam', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3091, 'Kombai', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3092, 'Konavattam', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3093, 'Kondalampatti', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3094, 'Konganapuram', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3095, 'Kooraikundu', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3096, 'Koothappar', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3097, 'Koradacheri', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3098, 'Kotagiri', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3099, 'Kothinallur', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3100, 'Kottaiyur', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3101, 'Kottakuppam', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3102, 'Kottaram', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3103, 'Kottivakkam', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3104, 'Kottur', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3105, 'Kouthanallur', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3106, 'Kovilpatti', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3107, 'Krishnagiri', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3108, 'Krishnarayapuram', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3109, 'Krishnasamudram', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3110, 'Kuchanur', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3111, 'Kuhalur', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3112, 'Kulasekarapuram', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3113, 'Kulithalai', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3114, 'Kumarapalayam', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3115, 'Kumarapuram', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3116, 'Kumbakonam', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3117, 'Kundrathur', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3118, 'Kuniyamuthur', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3119, 'Kunnathur', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3120, 'Kurichi', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3121, 'Kurinjipadi', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3122, 'Kurudampalayam', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3123, 'Kurumbalur', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3124, 'Kuthalam', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3125, 'Kuzhithurai', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3126, 'Labbaikudikadu', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3127, 'Lakkampatti', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3128, 'Lalgudi', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3129, 'Lalpet', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3130, 'Llayangudi', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3131, 'Madambakkam', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3132, 'Madathukulam', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3133, 'Madavaram', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3134, 'Madippakkam', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3135, 'Madukkarai', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3136, 'Madukkur', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3137, 'Madurai', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3138, 'Maduranthakam', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3139, 'Maduravoyal', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3140, 'Mallamooppampatti', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3141, 'Mallankinaru', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3142, 'Mallasamudram', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3143, 'Mallur', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3144, 'Mamallapuram', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3145, 'Mamsapuram', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3146, 'Manachanallur', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3147, 'Manali', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3148, 'Manalmedu', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3149, 'Manalurpet', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3150, 'Manamadurai', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3151, 'Manapakkam', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3152, 'Manapparai', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3153, 'Manavalakurichi', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3154, 'Mandaikadu', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3155, 'Mandapam', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3156, 'Mangadu', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3157, 'Mangalam', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3158, 'Mangalampet', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3159, 'Manimutharu', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3160, 'Mannargudi', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3161, 'Mappilaiurani', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3162, 'Maraimalainagar', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3163, 'Marakkanam', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3164, 'Maramangalathupatti', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3165, 'Marandahalli', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3166, 'Markayankottai', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3167, 'Marudur', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3168, 'Marungur', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3169, 'Mathigiri', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3170, 'Mayiladuthurai', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3171, 'Mecheri', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3172, 'Meenambakkam', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3173, 'Melacheval', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3174, 'Melachokkanathapuram', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3175, 'Melagaram', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3176, 'Melamadai', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3177, 'Melamaiyur', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3178, 'Melathiruppanthuruthi', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3179, 'Melattur', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3180, 'Melpattampakkam', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3181, 'Melur', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3182, 'Melvisharam', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3183, 'Mettupalayam', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3184, 'Mettupalayam', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3185, 'Mettur', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3186, 'Minjur', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3187, 'Modakurichi', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3188, 'Mohanur', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3189, 'Moolakaraipatti', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3190, 'Moovarasampettai', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3191, 'Mopperipalayam', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3192, 'Mudukulathur', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3193, 'Mukasipidariyur', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3194, 'Mukkudal', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3195, 'Mulagumudu', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3196, 'Mulanur', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3197, 'Muruganpalayam', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3198, 'Musiri', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3199, 'Muthupet', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3200, 'Muthur', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3201, 'Muttayyapuram', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3202, 'Myladi', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3203, 'Naduvattam', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3204, 'Nagapattinam', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3205, 'Nagavakulam', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3206, 'Nagercoil', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3207, 'Nagojanahalli', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3208, 'Nallampatti', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3209, 'Nallur', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3210, 'Namagiripettai', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3211, 'Namakkal', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3212, 'Nambiyur', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3213, 'Nandambakkam', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3214, 'Nandivaram-Guduvancheri', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3215, 'Nangavalli', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3216, 'Nangavaram', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3217, 'Nanguneri', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3218, 'Nanjikottai', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3219, 'Nannilam', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3220, 'Naranammalpuram', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3221, 'Naranapuram', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3222, 'Narasimhanaickenpalayam', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3223, 'Narasingapuram', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3224, 'Narasingapuram', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3225, 'Naravarikuppam', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3226, 'Nasiyanur', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3227, 'Natham', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3228, 'Nathampannai', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3229, 'Natrampalli', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3230, 'Nattapettai', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3231, 'Nattarasankottai', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3232, 'Navalpattu', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3233, 'Nazerath', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3234, 'Needamangalam', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3235, 'Neelagiri', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3236, 'Neelankarai', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3237, 'Neikkarapatti', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3238, 'Neiyyur', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3239, 'Nellikuppam', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3240, 'Nelliyalam', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3241, 'Nemili', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3242, 'Neripperichal', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3243, 'Nerkunram', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3244, 'Nerkuppai', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3245, 'Nerunjipettai', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3246, 'Neykkarappatti', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3247, 'Neyveli', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3248, 'Nilakkottai', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3249, 'O\' Valley', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3250, 'Odaipatti', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3251, 'Odaiyakulam', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3252, 'Oddanchatram', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3253, 'Odugathur', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3254, 'Oggiyamduraipakkam', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3255, 'Olagadam', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3256, 'Omalur', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3257, 'Orathanadu (Mukthambalpuram)', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3258, 'Othakadai', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3259, 'Othakalmandapam', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3260, 'Ottapparai', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3261, 'P. J. Cholapuram', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3262, 'P.Mettupalayam', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3263, 'P.N.Patti', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3264, 'Pacode', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3265, 'Padaiveedu', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3266, 'Padianallur', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3267, 'Padirikuppam', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3268, 'Padmanabhapuram', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3269, 'Palaganangudy', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3270, 'Palakkodu', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3271, 'Palamedu', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3272, 'Palani', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3273, 'Palani Chettipatti', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3274, 'Palavakkam', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3275, 'Palavansathu', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3276, 'Palayam', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3277, 'Palladam', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3278, 'Pallapalayam', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3279, 'Pallapalayam', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3280, 'Pallapatti', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3281, 'Pallapatti', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3282, 'Pallapatti', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3283, 'Pallathur', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3284, 'Pallavaram', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3285, 'Pallikaranai', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3286, 'Pallikonda', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3287, 'Pallipalayam', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3288, 'Pallipalayam Agraharam', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3289, 'Pallipattu', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3290, 'Pammal', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3291, 'Panagudi', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3292, 'Panaimarathupatti', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3293, 'Panapakkam', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3294, 'Panboli', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3295, 'Pandamangalam', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3296, 'Pannaikadu', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3297, 'Pannaipuram', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3298, 'Panruti', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3299, 'Papanasam', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3300, 'Pappankurichi', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3301, 'Papparapatti', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3302, 'Papparapatti', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3303, 'Pappireddipatti', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3304, 'Paramakudi', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3305, 'Paramathi', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3306, 'Parangipettai', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3307, 'Paravai', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3308, 'Pasur', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3309, 'Pathamadai', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3310, 'Pattinam', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3311, 'Pattiveeranpatti', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3312, 'Pattukkottai', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3313, 'Pazhugal', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3314, 'Peerkankaranai', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3315, 'Pennadam', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3316, 'Pennagaram', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3317, 'Pennathur', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3318, 'Peraiyur', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3319, 'Peralam', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3320, 'Perambalur', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3321, 'Peranamallur', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3322, 'Peravurani', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3323, 'Periya Negamam', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3324, 'Periyakodiveri', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3325, 'Periyakulam', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3326, 'Periyanaickenpalayam', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3327, 'Periyapatti', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3328, 'Periyasemur', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3329, 'Pernampattu', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3330, 'Perumagalur', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3331, 'Perumandi', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3332, 'Perumuchi', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3333, 'Perundurai', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3334, 'Perungalathur', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3335, 'Perungudi', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3336, 'Perungulam', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3337, 'Perur', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3338, 'Pethampalayam', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3339, 'Pethanaickenpalayam', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3340, 'Pillanallur', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3341, 'Polichalur', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3342, 'Pollachi', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3343, 'Polur', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3344, 'Ponmani', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3345, 'Ponnamaravathi', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3346, 'Ponnampatti', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3347, 'Ponneri', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3348, 'Poolambadi', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3349, 'Poolampatti', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3350, 'Pooluvapatti', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3351, 'Poonamallee', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3352, 'Porur', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3353, 'Pothanur', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3354, 'Pothatturpettai', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3355, 'Pudukadai', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3356, 'Pudukkottai', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3357, 'Pudukkottai', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3358, 'Pudupalaiyam Aghraharam', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3359, 'Pudupalayam', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3360, 'Pudupatti', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3361, 'Pudupatti', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3362, 'Pudupattinam', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3363, 'Pudur (S)', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3364, 'Puduvayal', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3365, 'Puliyankudi', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3366, 'Puliyur', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3367, 'Pullampadi', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3368, 'Punjai Thottakurichi', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3369, 'Punjaipugalur', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3370, 'Punjaipuliampatti', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3371, 'Puthalam', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3372, 'Puvalur', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3373, 'Puzhal', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3374, 'Puzhithivakkam (Ullagaram)', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3375, 'R.Pudupatti', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3376, 'R.S.Mangalam', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3377, 'Rajapalayam', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3378, 'Ramanathapuram', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3379, 'Ramapuram', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3380, 'Rameswaram', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3381, 'Ranipettai', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3382, 'Rasipuram', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3383, 'Rayagiri', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3384, 'Reethapuram', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3385, 'Rosalpatti', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3386, 'Rudravathi', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3387, 'S. Kannanur', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3388, 'S.Kodikulam', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3389, 'S.Nallur', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3390, 'Salangapalayam', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3391, 'Salem', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3392, 'Samalapuram', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3393, 'Samathur', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3394, 'Sambavar Vadagarai', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3395, 'Sankaramanallur', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3396, 'Sankarankoil', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3397, 'Sankarapuram', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3398, 'Sankari', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3399, 'Sankarnagar', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3400, 'Saravanampatti', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3401, 'Sarcarsamakulam', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3402, 'Sathankulam', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3403, 'Sathiyavijayanagaram', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3404, 'Sathuvachari', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3405, 'Sathyamangalam', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3406, 'Sattur', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3407, 'Sayalgudi', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3408, 'Sayapuram', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3409, 'Seerapalli', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3410, 'Seevur', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3411, 'Seithur', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3412, 'Sembakkam', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3413, 'Semmipalayam', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3414, 'Senthamangalam', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3415, 'Sentharapatti', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3416, 'Senur', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3417, 'Sethiathoppu', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3418, 'Sevilimedu', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3419, 'Sevugampatti', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3420, 'Shenbakkam', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3421, 'Shenkottai', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3422, 'Sholavandan', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3423, 'Sholinganallur', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3424, 'Sholingur', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3425, 'Sholur', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3426, 'Sikkarayapuram', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3427, 'Singampuneri', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3428, 'Singaperumalkoil', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3429, 'Sirkali', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3430, 'Sirugamani', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3431, 'Sirumugai', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3432, 'Sithayankottai', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3433, 'Sithurajapuram', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3434, 'Sivaganga', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3435, 'Sivagiri', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3436, 'Sivagiri', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3437, 'Sivakasi', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3438, 'Sivanthipuram', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3439, 'Srimushnam', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3440, 'Sriperumbudur', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3441, 'Sriramapuram', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3442, 'Srivaikuntam', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3443, 'Srivilliputhur', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3444, 'St.Thomas Mount-cum-Pallavaram', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3445, 'Suchindram', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3446, 'Suleeswaranpatti', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3447, 'Sulur', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3448, 'Sundarapandiam', 3, 25, 1, 1, 1, '2020-09-11 13:52:50', '2020-09-11 13:52:50', NULL),
(3449, 'Sundarapandiapuram', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3450, 'Surampatti', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3451, 'Surandai', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3452, 'Suriyampalayam', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3453, 'Swamimalai', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3454, 'T.Kallupatti', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3455, 'Tambaram', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3456, 'Tayilupatti', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3457, 'Tenkasi', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3458, 'Thadikombu', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3459, 'Thakkolam', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3460, 'Thalainayar', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3461, 'Thalakudi', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3462, 'Thamaraikulam', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3463, 'Thammampatti', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3464, 'Thanjavur', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3465, 'Thanthoni', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3466, 'Tharamangalam', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3467, 'Tharangambadi', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3468, 'Thathaiyangarpet', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3469, 'Thedavur', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3470, 'Thenambakkam', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3471, 'Thengampudur', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3472, 'Theni Allinagaram', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3473, 'Thenkarai', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3474, 'Thenkarai', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3475, 'Thenthamaraikulam', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3476, 'Thenthiruperai', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3477, 'Thesur', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3478, 'Thevaram', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3479, 'Thevur', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3480, 'Thiagadurgam', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3481, 'Thingalnagar', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3482, 'Thirukarungudi', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3483, 'Thirukattupalli', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3484, 'Thirumalayampalayam', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3485, 'Thirumangalam', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3486, 'Thirumazhisai', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3487, 'Thirunagar', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3488, 'Thirunageswaram', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3489, 'Thiruneermalai', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3490, 'Thirunindravur', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3491, 'Thiruparankundram', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3492, 'Thiruparappu', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3493, 'Thiruporur', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3494, 'Thiruppanandal', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3495, 'Thirupuvanam', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3496, 'Thirupuvanam', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3497, 'Thiruthangal', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3498, 'Thiruthuraipoondi', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3499, 'Thiruvaiyaru', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3500, 'Thiruvalam', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3501, 'Thiruvallur', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3502, 'Thiruvarur', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3503, 'Thiruvattaru', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3504, 'Thiruvenkatam', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3505, 'Thiruvennainallur', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3506, 'Thiruverumbur', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3507, 'Thiruvidaimarudur', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3508, 'Thiruvithankodu', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3509, 'Thisayanvilai', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3510, 'Thittacheri', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3511, 'Thondamuthur', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3512, 'Thondi', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3513, 'Thoothukkudi', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3514, 'Thorapadi', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3515, 'Thorapadi', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3516, 'Thottipalayam', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3517, 'Thottiyam', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3518, 'Thudiyalur', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3519, 'Thuraiyur', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3520, 'Thuthipattu', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3521, 'Thuvakudi', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3522, 'Timiri', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3523, 'Tindivanam', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3524, 'Tiruchendur', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3525, 'Tiruchengode', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3526, 'Tiruchirappalli', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3527, 'Tirukalukundram', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3528, 'Tirukkoyilur', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3529, 'Tirunelveli', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3530, 'Tirupathur', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3531, 'Tirupathur', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3532, 'Tiruppur', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3533, 'Tirusulam', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3534, 'Tiruttani', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3535, 'Tiruvannamalai', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3536, 'Tiruverkadu', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3537, 'Tiruvethipuram', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3538, 'Tiruvottiyur', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3539, 'Tittakudi', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3540, 'TNPL Pugalur', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3541, 'Udangudi', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3542, 'Udayarpalayam', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3543, 'Udhagamandalam', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3544, 'Udumalaipettai', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3545, 'Ullur', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3546, 'Ulundurpettai', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3547, 'Unjalaur', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3548, 'Unnamalaikadai', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3549, 'Uppidamangalam', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3550, 'Uppiliapuram', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3551, 'Urapakkam', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3552, 'Usilampatti', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3553, 'Uthamapalayam', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3554, 'Uthangarai', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3555, 'Uthayendram', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3556, 'Uthiramerur', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3557, 'Uthukkottai', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3558, 'Uthukuli', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3559, 'V. Pudur', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3560, 'Vadakarai Keezhpadugai', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3561, 'Vadakkanandal', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3562, 'Vadakkuvalliyur', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3563, 'Vadalur', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3564, 'Vadamadurai', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3565, 'Vadavalli', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3566, 'Vadipatti', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3567, 'Vadugapatti', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3568, 'Vadugapatti', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3569, 'Vaitheeswarankoil', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3570, 'Valangaiman', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3571, 'Valasaravakkam', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3572, 'Valavanur', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3573, 'Vallam', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3574, 'Valparai', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3575, 'Valvaithankoshtam', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL);
INSERT INTO `city` (`id`, `city`, `country_name`, `state_name`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(3576, 'Vanavasi', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3577, 'Vandalur', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3578, 'Vandavasi', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3579, 'Vandiyur', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3580, 'Vaniputhur', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3581, 'Vaniyambadi', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3582, 'Varadarajanpettai', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3583, 'Vasudevanallur', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3584, 'Vathirairuppu', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3585, 'Vazhapadi', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3586, 'Vedapatti', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3587, 'Vedaranyam', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3588, 'Vedasandur', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3589, 'Veeraganur', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3590, 'Veerakeralam', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3591, 'Veerakkalpudur', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3592, 'Veerapandi', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3593, 'Veerapandi', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3594, 'Veerapandi', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3595, 'Veerappanchatram', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3596, 'Veeravanallur', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3597, 'Velampalayam', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3598, 'Velankanni', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3599, 'Vellakinar', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3600, 'Vellakoil', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3601, 'Vellalur', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3602, 'Vellimalai', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3603, 'Vellore', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3604, 'Vellottamparappu', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3605, 'Velur', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3606, 'Vengampudur', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3607, 'Vengathur', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3608, 'Venkarai', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3609, 'Vennanthur', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3610, 'Veppathur', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3611, 'Verkilambi', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3612, 'Vettaikaranpudur', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3613, 'Vettavalam', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3614, 'Vijayapuri', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3615, 'Vikramasingapuram', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3616, 'Vikravandi', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3617, 'Vilangudi', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3618, 'Vilankurichi', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3619, 'Vilapakkam', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3620, 'Vilathikulam', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3621, 'Vilavur', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3622, 'Villukuri', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3623, 'Viluppuram', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3624, 'Virudhachalam', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3625, 'Virudhunagar', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3626, 'Virupakshipuram', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3627, 'Viswanatham', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3628, 'Walajabad', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3629, 'Walajapet', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3630, 'Wellington', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3631, 'Zamin Uthukuli', 3, 25, 1, 1, 1, '2020-09-11 13:52:51', '2020-09-11 13:52:51', NULL),
(3632, 'Agartala', 3, 27, 1, 1, 1, '2020-09-11 13:54:46', '2020-09-11 13:54:46', NULL),
(3633, 'Amarpur', 3, 27, 1, 1, 1, '2020-09-11 13:54:46', '2020-09-11 13:54:46', NULL),
(3634, 'Ambassa', 3, 27, 1, 1, 1, '2020-09-11 13:54:46', '2020-09-11 13:54:46', NULL),
(3635, 'Badharghat', 3, 27, 1, 1, 1, '2020-09-11 13:54:46', '2020-09-11 13:54:46', NULL),
(3636, 'Belonia', 3, 27, 1, 1, 1, '2020-09-11 13:54:46', '2020-09-11 13:54:46', NULL),
(3637, 'Dharmanagar', 3, 27, 1, 1, 1, '2020-09-11 13:54:46', '2020-09-11 13:54:46', NULL),
(3638, 'Gakulnagar', 3, 27, 1, 1, 1, '2020-09-11 13:54:46', '2020-09-11 13:54:46', NULL),
(3639, 'Gandhigram', 3, 27, 1, 1, 1, '2020-09-11 13:54:46', '2020-09-11 13:54:46', NULL),
(3640, 'Indranagar', 3, 27, 1, 1, 1, '2020-09-11 13:54:46', '2020-09-11 13:54:46', NULL),
(3641, 'Jogendranagar', 3, 27, 1, 1, 1, '2020-09-11 13:54:46', '2020-09-11 13:54:46', NULL),
(3642, 'Kailasahar', 3, 27, 1, 1, 1, '2020-09-11 13:54:46', '2020-09-11 13:54:46', NULL),
(3643, 'Kamalpur', 3, 27, 1, 1, 1, '2020-09-11 13:54:46', '2020-09-11 13:54:46', NULL),
(3644, 'Kanchanpur', 3, 27, 1, 1, 1, '2020-09-11 13:54:46', '2020-09-11 13:54:46', NULL),
(3645, 'Khowai', 3, 27, 1, 1, 1, '2020-09-11 13:54:46', '2020-09-11 13:54:46', NULL),
(3646, 'Kumarghat', 3, 27, 1, 1, 1, '2020-09-11 13:54:46', '2020-09-11 13:54:46', NULL),
(3647, 'Kunjaban', 3, 27, 1, 1, 1, '2020-09-11 13:54:46', '2020-09-11 13:54:46', NULL),
(3648, 'Narsingarh', 3, 27, 1, 1, 1, '2020-09-11 13:54:46', '2020-09-11 13:54:46', NULL),
(3649, 'Pratapgarh', 3, 27, 1, 1, 1, '2020-09-11 13:54:46', '2020-09-11 13:54:46', NULL),
(3650, 'Ranirbazar', 3, 27, 1, 1, 1, '2020-09-11 13:54:46', '2020-09-11 13:54:46', NULL),
(3651, 'Sabroom', 3, 27, 1, 1, 1, '2020-09-11 13:54:46', '2020-09-11 13:54:46', NULL),
(3652, 'Sonamura', 3, 27, 1, 1, 1, '2020-09-11 13:54:46', '2020-09-11 13:54:46', NULL),
(3653, 'Teliamura', 3, 27, 1, 1, 1, '2020-09-11 13:54:46', '2020-09-11 13:54:46', NULL),
(3654, 'Udaipur', 3, 27, 1, 1, 1, '2020-09-11 13:54:46', '2020-09-11 13:54:46', NULL),
(3655, 'Achhalda', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3656, 'Achhnera', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3657, 'Adari', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3658, 'Afzalgarh', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3659, 'Agarwal Mandi', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3660, 'Agra', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3661, 'Agra', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3662, 'Ahraura', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3663, 'Ailum', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3664, 'Air Force Area', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3665, 'Ajhuwa', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3666, 'Akbarpur', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3667, 'Akbarpur', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3668, 'Aliganj', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3669, 'Aligarh', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3670, 'Allahabad', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3671, 'Allahabad', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3672, 'Allahganj', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3673, 'Allapur', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3674, 'Amanpur', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3675, 'Ambehta', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3676, 'Amethi', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3677, 'Amethi', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3678, 'Amila', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3679, 'Amilo', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3680, 'Aminagar Sarai', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3681, 'Aminagar Urf Bhurbaral', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3682, 'Amraudha', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3683, 'Amroha', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3684, 'Anandnagar', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3685, 'Anpara', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3686, 'Antu', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3687, 'Anupshahr', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3688, 'Aonla', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3689, 'Armapur Estate', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3690, 'Ashrafpur Kichhauchha', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3691, 'Atarra', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3692, 'Atasu', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3693, 'Atrauli', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3694, 'Atraulia', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3695, 'Auraiya', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3696, 'Aurangabad', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3697, 'Aurangabad Bangar', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3698, 'Auras', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3699, 'Awagarh', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3700, 'Ayodhya', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3701, 'Azamgarh', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3702, 'Azizpur', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3703, 'Azmatgarh', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3704, 'Babarpur Ajitmal', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3705, 'Baberu', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3706, 'Babina', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3707, 'Babrala', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3708, 'Babugarh', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3709, 'Bachhraon', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3710, 'Bachhrawan', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3711, 'Bad', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3712, 'Baghpat', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3713, 'Bah', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3714, 'Bahadurganj', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3715, 'Baheri', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3716, 'Bahjoi', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3717, 'Bahraich', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3718, 'Bahsuma', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3719, 'Bahuwa', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3720, 'Bajna', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3721, 'Bakewar', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3722, 'Bakiabad', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3723, 'Baldeo', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3724, 'Ballia', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3725, 'Balrampur', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3726, 'Banat', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3727, 'Banda', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3728, 'Bangarmau', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3729, 'Banki', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3730, 'Bansdih', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3731, 'Bansgaon', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3732, 'Bansi', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3733, 'Baragaon', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3734, 'Baragaon', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3735, 'Baraut', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3736, 'Bareilly', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3737, 'Bareilly', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3738, 'Barhalganj', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3739, 'Barhani Bazar', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3740, 'Barkhera', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3741, 'Barsana', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3742, 'Barua Sagar', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3743, 'Barwar', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3744, 'Basti', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3745, 'Begumabad Budhana', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3746, 'Behta Hajipur', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3747, 'Bela Pratapgarh', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3748, 'Belthara Road', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3749, 'Beniganj', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3750, 'Beswan', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3751, 'Bewar', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3752, 'Bhadarsa', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3753, 'Bhadohi', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3754, 'Bhagwant Nagar', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3755, 'Bharatganj', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3756, 'Bhargain', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3757, 'Bharthana', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3758, 'Bharuhana', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3759, 'Bharwari', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3760, 'Bhatni Bazar', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3761, 'Bhatpar Rani', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3762, 'Bhawan Bahadur Nagar', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3763, 'Bhinga', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3764, 'Bhogaon', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3765, 'Bhojpur Dharampur', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3766, 'Bhokarhedi', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3767, 'Bhulepur', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3768, 'Bidhuna', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3769, 'Bighapur', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3770, 'Bijnor', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3771, 'Bijpur', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3772, 'Bikapur', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3773, 'Bilari', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3774, 'Bilariaganj', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3775, 'Bilaspur', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3776, 'Bilaspur', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3777, 'Bilgram', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3778, 'Bilhaur', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3779, 'Bilram', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3780, 'Bilsanda', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3781, 'Bilsi', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3782, 'Bindki', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3783, 'Bisalpur', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3784, 'Bisanda Buzurg', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3785, 'Bisauli', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3786, 'Bisharatganj', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3787, 'Bisokhar', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3788, 'Biswan', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3789, 'Bithoor', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3790, 'Budaun', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3791, 'Budhana', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3792, 'Bugrasi', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3793, 'Bulandshahr', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3794, 'Chail', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3795, 'Chak Imam Ali', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3796, 'Chakeri', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3797, 'Chakia', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3798, 'Chandauli', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3799, 'Chandausi', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3800, 'Chandpur', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3801, 'Charkhari', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3802, 'Charthaval', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3803, 'Chaumuhan', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3804, 'Chhaprauli', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3805, 'Chharra Rafatpur', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3806, 'Chhata', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3807, 'Chhatari', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3808, 'Chhibramau', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3809, 'Chhutmalpur', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3810, 'Chilkana Sultanpur', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3811, 'Chirgaon', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3812, 'Chitbara Gaon', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3813, 'Chitrakoot Dham (Karwi)', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3814, 'Chopan', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3815, 'Choubepur Kalan', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3816, 'Chunar', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3817, 'Churk Ghurma', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3818, 'Colonelganj', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3819, 'Dadri', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3820, 'Dalmau', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3821, 'Dankaur', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3822, 'Dariyabad', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3823, 'Dasna', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3824, 'Dataganj', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3825, 'Daurala', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3826, 'Dayalbagh', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3827, 'Deoband', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3828, 'Deoranian', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3829, 'Deoria', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3830, 'Dewa', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3831, 'Dhampur', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3832, 'Dhanauha', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3833, 'Dhanauli', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3834, 'Dhanaura', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3835, 'Dharoti Khurd', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3836, 'Dhaura Tanda', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3837, 'Dhaurehra', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3838, 'Dibai', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3839, 'Dibiyapur', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3840, 'Dildarnagar Fatehpur Bazar', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3841, 'Doghat', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3842, 'Dohrighat', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3843, 'Dostpur', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3844, 'Dudhi', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3845, 'Dulhipur', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3846, 'Ekdil', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3847, 'Erich', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3848, 'Etah', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3849, 'Etawah', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3850, 'Etmadpur', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3851, 'Faizabad', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3852, 'Faizabad', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3853, 'Faizganj', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3854, 'Farah', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3855, 'Faridnagar', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3856, 'Faridpur', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3857, 'Faridpur', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3858, 'Fariha', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3859, 'Farrukhabad-cum-Fatehgarh', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3860, 'Fatehabad', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3861, 'Fatehganj Pashchimi', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3862, 'Fatehganj Purvi', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3863, 'Fatehgarh', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3864, 'Fatehpur', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3865, 'Fatehpur', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3866, 'Fatehpur Chaurasi', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3867, 'Fatehpur Sikri', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3868, 'Firozabad', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3869, 'Gajraula', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3870, 'Gangaghat', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3871, 'Gangapur', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3872, 'Gangoh', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3873, 'Ganj Dundawara', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3874, 'Ganj Muradabad', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3875, 'Garautha', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3876, 'Garhi Pukhta', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3877, 'Garhmukteshwar', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3878, 'Gaura Barhaj', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3879, 'Gauri Bazar', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3880, 'Gausganj', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3881, 'Gawan', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3882, 'Ghatampur', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3883, 'Ghaziabad', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3884, 'Ghazipur', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3885, 'Ghiraur', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3886, 'Ghorawal', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3887, 'Ghosi', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3888, 'Ghosia Bazar', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3889, 'Ghughuli', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3890, 'Gohand', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3891, 'Gokul', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3892, 'Gola Bazar', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3893, 'Gola Gokarannath', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3894, 'Gonda', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3895, 'Gopamau', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3896, 'Gopiganj', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3897, 'Gorakhpur', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3898, 'Gosainganj', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3899, 'Gosainganj', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3900, 'Govardhan', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3901, 'Gulaothi', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3902, 'Gularia Bhindara', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3903, 'Gulariya', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3904, 'Gunnaur', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3905, 'Gursahaiganj', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3906, 'Gursarai', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3907, 'Gyanpur', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3908, 'Hafizpur', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3909, 'Haidergarh', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3910, 'Haldaur', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3911, 'Hamirpur', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3912, 'Handia', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3913, 'Hapur', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3914, 'Hardoi', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3915, 'Harduaganj', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3916, 'Hargaon', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3917, 'Hariharpur', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3918, 'Harraiya', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3919, 'Hasanpur', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3920, 'Hasayan', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3921, 'Hastinapur', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3922, 'Hata', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3923, 'Hathras', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3924, 'Hyderabad', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3925, 'Ibrahimpur', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3926, 'Iglas', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3927, 'Ikauna', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3928, 'Iltifatganj Bazar', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3929, 'Indian Telephone Industry, Mankapur (Sp. Village)', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3930, 'Islamnagar', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3931, 'Itaunja', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3932, 'Jafarabad', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3933, 'Jagner', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3934, 'Jahanabad', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3935, 'Jahangirabad', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3936, 'Jahangirpur', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3937, 'Jais', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3938, 'Jaithara', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3939, 'Jalalabad', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3940, 'Jalalabad', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3941, 'Jalalabad', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3942, 'Jalali', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3943, 'Jalalpur', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3944, 'Jalaun', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3945, 'Jalesar', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3946, 'Jamshila', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3947, 'Jangipur', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3948, 'Jansath', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3949, 'Jarwal', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3950, 'Jasrana', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3951, 'Jaswantnagar', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3952, 'Jatari', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3953, 'Jaunpur', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3954, 'Jewar', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3955, 'Jhalu', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3956, 'Jhansi', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3957, 'Jhansi', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3958, 'Jhansi Rly. Settlement', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3959, 'Jhinjhak', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3960, 'Jhinjhana', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3961, 'Jhusi', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3962, 'Jhusi Kohna', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3963, 'Jiyanpur', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3964, 'Joya', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3965, 'Jyoti Khuria', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3966, 'Kabrai', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3967, 'Kachhauna Patseni', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3968, 'Kachhla', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3969, 'Kachhwa', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3970, 'Kadaura', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3971, 'Kadipur', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3972, 'Kailashpur', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3973, 'Kaimganj', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3974, 'Kairana', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3975, 'Kakgaina', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3976, 'Kakod', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3977, 'Kakori', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3978, 'Kakrala', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3979, 'Kalinagar', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3980, 'Kalpi', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3981, 'Kamalganj', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3982, 'Kampil', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3983, 'Kandhla', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3984, 'Kandwa', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3985, 'Kannauj', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3986, 'Kanpur', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3987, 'Kanpur', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3988, 'Kanth', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3989, 'Kanth', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3990, 'Kaptanganj', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3991, 'Karari', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3992, 'Karhal', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3993, 'Karnawal', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3994, 'Kasganj', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3995, 'Katariya', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3996, 'Katghar Lalganj', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3997, 'Kathera', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3998, 'Katra', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(3999, 'Katra', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(4000, 'Katra Medniganj', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(4001, 'Kauriaganj', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(4002, 'Kemri', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(4003, 'Kerakat', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(4004, 'Khadda', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(4005, 'Khaga', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(4006, 'Khailar', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(4007, 'Khair', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(4008, 'Khairabad', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(4009, 'Khairabad', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(4010, 'Khalilabad', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(4011, 'Khamaria', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(4012, 'Khanpur', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(4013, 'Kharela', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(4014, 'Khargupur', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(4015, 'Khariya', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(4016, 'Kharkhoda', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(4017, 'Khatauli', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(4018, 'Khatauli Rural', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(4019, 'Khekada', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(4020, 'Kheragarh', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(4021, 'Kheri', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(4022, 'Kheta Sarai', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(4023, 'Khudaganj', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(4024, 'Khurja', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(4025, 'Khutar', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(4026, 'Kiraoli', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(4027, 'Kiratpur', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(4028, 'Kishni', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(4029, 'Kishunpur', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(4030, 'Kithaur', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(4031, 'Koeripur', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(4032, 'Konch', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(4033, 'Kopaganj', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(4034, 'Kora Jahanabad', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(4035, 'Koraon', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(4036, 'Korwa', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(4037, 'Kosi Kalan', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(4038, 'Kota', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(4039, 'Kotra', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(4040, 'Kotwa', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(4041, 'Kul Pahar', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(4042, 'Kunda', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(4043, 'Kundarki', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(4044, 'Kunwargaon', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(4045, 'Kuraoli', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(4046, 'Kurara', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(4047, 'Kursath', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(4048, 'Kursath', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(4049, 'Kurthi Jafarpur', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(4050, 'Kushinagar', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(4051, 'Kusmara', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(4052, 'Laharpur', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(4053, 'Lakhimpur', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(4054, 'Lakhna', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(4055, 'Lal Gopalganj Nindaura', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(4056, 'Lalganj', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(4057, 'Lalitpur', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(4058, 'Lar', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(4059, 'Lawar NP', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(4060, 'Ledwa Mahua', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(4061, 'Lohta', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(4062, 'Loni', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(4063, 'Lucknow', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(4064, 'Lucknow', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(4065, 'Machhlishahr', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(4066, 'Madhoganj', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(4067, 'Madhogarh', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(4068, 'Maghar', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(4069, 'Mahaban', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(4070, 'Mahmudabad', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(4071, 'Mahoba', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(4072, 'Maholi', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(4073, 'Mahona', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(4074, 'Mahrajganj', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(4075, 'Mahrajganj', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(4076, 'Mahrajganj', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(4077, 'Mahroni', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(4078, 'Mailani', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(4079, 'Mainpuri', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(4080, 'Majhara Pipar Ehatmali', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(4081, 'Majhauli Raj', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(4082, 'Malihabad', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(4083, 'Mallawan', 3, 30, 1, 1, 1, '2020-09-11 13:56:49', '2020-09-11 13:56:49', NULL),
(4084, 'Mandawar', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4085, 'Manikpur', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4086, 'Manikpur Sarhat', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4087, 'Maniyar', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4088, 'Manjhanpur', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4089, 'Mankapur', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4090, 'Marehra', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4091, 'Mariahu', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4092, 'Maruadih Rly. Settlement', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4093, 'Maswasi', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4094, 'Mataundh', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4095, 'Mathura', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4096, 'Mathura', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4097, 'Mau Aima', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4098, 'Maudaha', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4099, 'Maunath Bhanjan', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4100, 'Mauranipur', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4101, 'Maurawan', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4102, 'Mawana', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4103, 'Meerut', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4104, 'Meerut', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4105, 'Mehdawal', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4106, 'Mehnagar', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4107, 'Mendu', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4108, 'Milak', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4109, 'Miranpur', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4110, 'Mirganj', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4111, 'Mirzapur-cum-Vindhyachal', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4112, 'Misrikh-cum-Neemsar', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4113, 'Modinagar', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4114, 'Mogra Badshahpur', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4115, 'Mohammadabad', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4116, 'Mohammadabad', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4117, 'Mohammadi', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4118, 'Mohan', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4119, 'Mohanpur', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4120, 'Mohiuddinpur', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4121, 'Moradabad', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4122, 'Moth', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4123, 'Mubarakpur', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4124, 'Mughalsarai', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4125, 'Mughalsarai Rly. Settlement', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4126, 'Muhammadabad', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4127, 'Mukrampur Khema', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4128, 'Mundera Bazar', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4129, 'Mundia', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4130, 'Muradnagar', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4131, 'Mursan', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4132, 'Musafirkhana', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4133, 'Muzaffarnagar', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4134, 'Nadigaon', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4135, 'Nagina', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4136, 'Nagram', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4137, 'Nai Bazar', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4138, 'Nainana Jat', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4139, 'Najibabad', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4140, 'Nakur', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4141, 'Nanauta', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4142, 'Nandgaon', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4143, 'Nanpara', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4144, 'Naraini', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4145, 'Narauli', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4146, 'Naraura', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4147, 'Naugawan Sadat', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4148, 'Nautanwa', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL);
INSERT INTO `city` (`id`, `city`, `country_name`, `state_name`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(4149, 'Nawabganj', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4150, 'Nawabganj', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4151, 'Nawabganj', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4152, 'Nawabganj', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4153, 'Nehtaur', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4154, 'Nichlaul', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4155, 'Nidhauli Kalan', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4156, 'Niwari', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4157, 'Nizamabad', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4158, 'Noida', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4159, 'Noorpur', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4160, 'Northern Rly. Colony', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4161, 'Nyoria Husainpur', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4162, 'Nyotini', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4163, 'Obra', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4164, 'Oel Dhakwa', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4165, 'Orai', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4166, 'Oran', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4167, 'Ordnance Factory Muradnagar', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4168, 'Pachperwa', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4169, 'Padrauna', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4170, 'Pahasu', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4171, 'Paintepur', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4172, 'Pali', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4173, 'Pali', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4174, 'Palia Kalan', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4175, 'Parasi', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4176, 'Parichha', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4177, 'Parikshitgarh', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4178, 'Parsadepur', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4179, 'Patala', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4180, 'Patiyali', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4181, 'Patti', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4182, 'Phalauda', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4183, 'Phaphund', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4184, 'Phulpur', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4185, 'Phulpur', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4186, 'Phulwaria', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4187, 'Pihani', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4188, 'Pilibhit', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4189, 'Pilkhana', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4190, 'Pilkhuwa', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4191, 'Pinahat', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4192, 'Pipalsana Chaudhari', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4193, 'Pipiganj', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4194, 'Pipraich', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4195, 'Pipri', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4196, 'Powayan', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4197, 'Pratapgarh City', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4198, 'Pukhrayan', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4199, 'Puranpur', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4200, 'Purdilnagar', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4201, 'Purquazi', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4202, 'Purwa', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4203, 'Qasimpur Power House Colony', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4204, 'Rabupura', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4205, 'Radhakund', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4206, 'Rae Bareli', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4207, 'Raja Ka Rampur', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4208, 'Rajapur', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4209, 'Ramkola', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4210, 'Ramnagar', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4211, 'Ramnagar', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4212, 'Rampur', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4213, 'Rampur Bhawanipur', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4214, 'Rampur Karkhana', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4215, 'Rampur Maniharan', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4216, 'Rampura', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4217, 'Ranipur', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4218, 'Rashidpur Garhi', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4219, 'Rasra', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4220, 'Rasulabad', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4221, 'Rath', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4222, 'Raya', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4223, 'Renukoot', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4224, 'Reoti', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4225, 'Richha', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4226, 'Risia Bazar', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4227, 'Rithora', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4228, 'Rly. Settlement Roza', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4229, 'Robertsganj', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4230, 'Rudauli', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4231, 'Rudayan', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4232, 'Rudrapur', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4233, 'Rura', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4234, 'Rustamnagar Sahaspur', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4235, 'Sadabad', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4236, 'Sadat', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4237, 'Safipur', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4238, 'Sahanpur', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4239, 'Saharanpur', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4240, 'Sahaspur', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4241, 'Sahaswan', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4242, 'Sahatwar', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4243, 'Sahawar', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4244, 'Sahjanwa', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4245, 'Sahpau NP', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4246, 'Saidpur', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4247, 'Saidpur', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4248, 'Sainthal', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4249, 'Saiyad Raja', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4250, 'Sakhanu', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4251, 'Sakit', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4252, 'Salarpur Khadar', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4253, 'Salempur', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4254, 'Salon', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4255, 'Sambhal', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4256, 'Samdhan', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4257, 'Samthar', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4258, 'Sandi', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4259, 'Sandila', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4260, 'Sarai Aquil', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4261, 'Sarai Mir', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4262, 'Sardhana', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4263, 'Sarila', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4264, 'Sarsawan', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4265, 'Sasni', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4266, 'Satrikh', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4267, 'Saunkh', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4268, 'Saurikh', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4269, 'Seohara', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4270, 'Sewalkhas', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4271, 'Sewarhi', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4272, 'Shahabad', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4273, 'Shahabad', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4274, 'Shahganj', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4275, 'Shahi', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4276, 'Shahjahanpur', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4277, 'Shahjahanpur', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4278, 'Shahpur', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4279, 'Shamli', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4280, 'Shamsabad', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4281, 'Shamsabad', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4282, 'Shankargarh', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4283, 'Shergarh', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4284, 'Sherkot', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4285, 'Shikarpur', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4286, 'Shikohabad', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4287, 'Shishgarh', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4288, 'Shivdaspur', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4289, 'Shivli', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4290, 'Shivrajpur', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4291, 'Shohratgarh', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4292, 'Siana', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4293, 'Siddhaur', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4294, 'Sidhauli', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4295, 'Sidhpura', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4296, 'Sikanderpur', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4297, 'Sikanderpur', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4298, 'Sikandra', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4299, 'Sikandra Rao', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4300, 'Sikandrabad', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4301, 'Singahi Bhiraura', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4302, 'Sirathu', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4303, 'Sirauli', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4304, 'Sirsa', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4305, 'Sirsaganj', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4306, 'Sirsi', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4307, 'Sisauli', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4308, 'Siswa Bazar', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4309, 'Sitapur', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4310, 'Som', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4311, 'Soron', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4312, 'Suar', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4313, 'Sukhmalpur Nizamabad', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4314, 'Sultanpur', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4315, 'Sumerpur', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4316, 'Suriyawan', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4317, 'Swamibagh', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4318, 'Talbehat', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4319, 'Talgram', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4320, 'Tambaur-cum-Ahmadabad', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4321, 'Tanda', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4322, 'Tanda', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4323, 'Tatarpur Lallu', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4324, 'Tetri Bazar', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4325, 'Thakurdwara', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4326, 'Thana Bhawan', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4327, 'Thiriya Nizamat Khan', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4328, 'Tikait Nagar', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4329, 'Tikri', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4330, 'Tilhar', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4331, 'Tindwari', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4332, 'Tirwaganj', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4333, 'Titron', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4334, 'Tondi Fatehpur', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4335, 'Tulsipur', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4336, 'Tundla', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4337, 'Tundla Kham', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4338, 'Tundla Rly. Colony', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4339, 'Ugu', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4340, 'Ujhani', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4341, 'Ujhari', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4342, 'Umri', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4343, 'Umri Kalan', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4344, 'Un', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4345, 'Unchahar', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4346, 'Unnao', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4347, 'Usawan', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4348, 'Usehat', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4349, 'Utraula', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4350, 'Varanasi', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4351, 'Varanasi', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4352, 'Vijaigarh', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4353, 'Vrindavan', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4354, 'Warhapur', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4355, 'Wazirganj', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4356, 'Zaidpur', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4357, 'Zamania', 3, 30, 1, 1, 1, '2020-09-11 13:56:50', '2020-09-11 13:56:50', NULL),
(4358, 'Almora', 3, 28, 1, 1, 1, '2020-09-11 13:59:35', '2020-09-11 13:59:35', NULL),
(4359, 'Badrinathpuri', 3, 28, 1, 1, 1, '2020-09-11 13:59:35', '2020-09-11 13:59:35', NULL),
(4360, 'Bageshwar', 3, 28, 1, 1, 1, '2020-09-11 13:59:35', '2020-09-11 13:59:35', NULL),
(4361, 'Bajpur', 3, 28, 1, 1, 1, '2020-09-11 13:59:35', '2020-09-11 13:59:35', NULL),
(4362, 'Banbasa', 3, 28, 1, 1, 1, '2020-09-11 13:59:35', '2020-09-11 13:59:35', NULL),
(4363, 'Bandia', 3, 28, 1, 1, 1, '2020-09-11 13:59:35', '2020-09-11 13:59:35', NULL),
(4364, 'Barkot', 3, 28, 1, 1, 1, '2020-09-11 13:59:35', '2020-09-11 13:59:35', NULL),
(4365, 'BHEL Ranipur', 3, 28, 1, 1, 1, '2020-09-11 13:59:35', '2020-09-11 13:59:35', NULL),
(4366, 'Bhimtal', 3, 28, 1, 1, 1, '2020-09-11 13:59:35', '2020-09-11 13:59:35', NULL),
(4367, 'Bhowali', 3, 28, 1, 1, 1, '2020-09-11 13:59:35', '2020-09-11 13:59:35', NULL),
(4368, 'Chakrata', 3, 28, 1, 1, 1, '2020-09-11 13:59:35', '2020-09-11 13:59:35', NULL),
(4369, 'Chamba', 3, 28, 1, 1, 1, '2020-09-11 13:59:35', '2020-09-11 13:59:35', NULL),
(4370, 'Chamoli Gopeshwar', 3, 28, 1, 1, 1, '2020-09-11 13:59:35', '2020-09-11 13:59:35', NULL),
(4371, 'Champawat', 3, 28, 1, 1, 1, '2020-09-11 13:59:35', '2020-09-11 13:59:35', NULL),
(4372, 'Clement Town', 3, 28, 1, 1, 1, '2020-09-11 13:59:35', '2020-09-11 13:59:35', NULL),
(4373, 'Dehradun', 3, 28, 1, 1, 1, '2020-09-11 13:59:35', '2020-09-11 13:59:35', NULL),
(4374, 'Dehradun', 3, 28, 1, 1, 1, '2020-09-11 13:59:35', '2020-09-11 13:59:35', NULL),
(4375, 'Dev Prayag', 3, 28, 1, 1, 1, '2020-09-11 13:59:35', '2020-09-11 13:59:35', NULL),
(4376, 'Dhaluwala', 3, 28, 1, 1, 1, '2020-09-11 13:59:35', '2020-09-11 13:59:35', NULL),
(4377, 'Dhandera', 3, 28, 1, 1, 1, '2020-09-11 13:59:35', '2020-09-11 13:59:35', NULL),
(4378, 'Dharchula', 3, 28, 1, 1, 1, '2020-09-11 13:59:35', '2020-09-11 13:59:35', NULL),
(4379, 'Dharchula Dehat', 3, 28, 1, 1, 1, '2020-09-11 13:59:35', '2020-09-11 13:59:35', NULL),
(4380, 'Didihat', 3, 28, 1, 1, 1, '2020-09-11 13:59:35', '2020-09-11 13:59:35', NULL),
(4381, 'Dineshpur', 3, 28, 1, 1, 1, '2020-09-11 13:59:35', '2020-09-11 13:59:35', NULL),
(4382, 'Dogadda', 3, 28, 1, 1, 1, '2020-09-11 13:59:35', '2020-09-11 13:59:35', NULL),
(4383, 'Doiwala', 3, 28, 1, 1, 1, '2020-09-11 13:59:35', '2020-09-11 13:59:35', NULL),
(4384, 'Dwarahat', 3, 28, 1, 1, 1, '2020-09-11 13:59:35', '2020-09-11 13:59:35', NULL),
(4385, 'FRI and College Area', 3, 28, 1, 1, 1, '2020-09-11 13:59:35', '2020-09-11 13:59:35', NULL),
(4386, 'Gadarpur', 3, 28, 1, 1, 1, '2020-09-11 13:59:35', '2020-09-11 13:59:35', NULL),
(4387, 'Gangotri', 3, 28, 1, 1, 1, '2020-09-11 13:59:35', '2020-09-11 13:59:35', NULL),
(4388, 'Gochar', 3, 28, 1, 1, 1, '2020-09-11 13:59:35', '2020-09-11 13:59:35', NULL),
(4389, 'Haldwani-cum-Kathgodam', 3, 28, 1, 1, 1, '2020-09-11 13:59:35', '2020-09-11 13:59:35', NULL),
(4390, 'Hardwar', 3, 28, 1, 1, 1, '2020-09-11 13:59:35', '2020-09-11 13:59:35', NULL),
(4391, 'Herbertpur', 3, 28, 1, 1, 1, '2020-09-11 13:59:35', '2020-09-11 13:59:35', NULL),
(4392, 'Jaspur', 3, 28, 1, 1, 1, '2020-09-11 13:59:35', '2020-09-11 13:59:35', NULL),
(4393, 'Jhabrera', 3, 28, 1, 1, 1, '2020-09-11 13:59:35', '2020-09-11 13:59:35', NULL),
(4394, 'Joshimath', 3, 28, 1, 1, 1, '2020-09-11 13:59:35', '2020-09-11 13:59:35', NULL),
(4395, 'Kachnal Gosain', 3, 28, 1, 1, 1, '2020-09-11 13:59:35', '2020-09-11 13:59:35', NULL),
(4396, 'Kaladungi', 3, 28, 1, 1, 1, '2020-09-11 13:59:35', '2020-09-11 13:59:35', NULL),
(4397, 'Karn Prayag', 3, 28, 1, 1, 1, '2020-09-11 13:59:35', '2020-09-11 13:59:35', NULL),
(4398, 'Kashipur', 3, 28, 1, 1, 1, '2020-09-11 13:59:35', '2020-09-11 13:59:35', NULL),
(4399, 'Kashirampur', 3, 28, 1, 1, 1, '2020-09-11 13:59:35', '2020-09-11 13:59:35', NULL),
(4400, 'Kedarnath', 3, 28, 1, 1, 1, '2020-09-11 13:59:35', '2020-09-11 13:59:35', NULL),
(4401, 'Kela Khera', 3, 28, 1, 1, 1, '2020-09-11 13:59:35', '2020-09-11 13:59:35', NULL),
(4402, 'Khatima', 3, 28, 1, 1, 1, '2020-09-11 13:59:35', '2020-09-11 13:59:35', NULL),
(4403, 'Kichha', 3, 28, 1, 1, 1, '2020-09-11 13:59:35', '2020-09-11 13:59:35', NULL),
(4404, 'Kirtinagar', 3, 28, 1, 1, 1, '2020-09-11 13:59:35', '2020-09-11 13:59:35', NULL),
(4405, 'Kotdwara', 3, 28, 1, 1, 1, '2020-09-11 13:59:35', '2020-09-11 13:59:35', NULL),
(4406, 'Laksar', 3, 28, 1, 1, 1, '2020-09-11 13:59:35', '2020-09-11 13:59:35', NULL),
(4407, 'Lalkuan', 3, 28, 1, 1, 1, '2020-09-11 13:59:35', '2020-09-11 13:59:35', NULL),
(4408, 'Landaura', 3, 28, 1, 1, 1, '2020-09-11 13:59:35', '2020-09-11 13:59:35', NULL),
(4409, 'Landour', 3, 28, 1, 1, 1, '2020-09-11 13:59:35', '2020-09-11 13:59:35', NULL),
(4410, 'Lansdowne', 3, 28, 1, 1, 1, '2020-09-11 13:59:35', '2020-09-11 13:59:35', NULL),
(4411, 'Lohaghat', 3, 28, 1, 1, 1, '2020-09-11 13:59:35', '2020-09-11 13:59:35', NULL),
(4412, 'Mahua Dabra Haripura', 3, 28, 1, 1, 1, '2020-09-11 13:59:35', '2020-09-11 13:59:35', NULL),
(4413, 'Mahua Kheraganj', 3, 28, 1, 1, 1, '2020-09-11 13:59:35', '2020-09-11 13:59:35', NULL),
(4414, 'Manglaur', 3, 28, 1, 1, 1, '2020-09-11 13:59:35', '2020-09-11 13:59:35', NULL),
(4415, 'Mohanpur ', 3, 28, 1, 1, 1, '2020-09-11 13:59:35', '2020-09-11 13:59:35', NULL),
(4416, 'Muni Ki Reti', 3, 28, 1, 1, 1, '2020-09-11 13:59:35', '2020-09-11 13:59:35', NULL),
(4417, 'Mussoori', 3, 28, 1, 1, 1, '2020-09-11 13:59:35', '2020-09-11 13:59:35', NULL),
(4418, 'Nagla', 3, 28, 1, 1, 1, '2020-09-11 13:59:35', '2020-09-11 13:59:35', NULL),
(4419, 'Nainital', 3, 28, 1, 1, 1, '2020-09-11 13:59:35', '2020-09-11 13:59:35', NULL),
(4420, 'Nainital', 3, 28, 1, 1, 1, '2020-09-11 13:59:35', '2020-09-11 13:59:35', NULL),
(4421, 'Nand Prayag', 3, 28, 1, 1, 1, '2020-09-11 13:59:35', '2020-09-11 13:59:35', NULL),
(4422, 'Narendra Nagar', 3, 28, 1, 1, 1, '2020-09-11 13:59:35', '2020-09-11 13:59:35', NULL),
(4423, 'Pauri', 3, 28, 1, 1, 1, '2020-09-11 13:59:35', '2020-09-11 13:59:35', NULL),
(4424, 'Pithoragarh', 3, 28, 1, 1, 1, '2020-09-11 13:59:35', '2020-09-11 13:59:35', NULL),
(4425, 'Pratitnagar', 3, 28, 1, 1, 1, '2020-09-11 13:59:35', '2020-09-11 13:59:35', NULL),
(4426, 'Raipur', 3, 28, 1, 1, 1, '2020-09-11 13:59:35', '2020-09-11 13:59:35', NULL),
(4427, 'Ramnagar', 3, 28, 1, 1, 1, '2020-09-11 13:59:35', '2020-09-11 13:59:35', NULL),
(4428, 'Ranikhet', 3, 28, 1, 1, 1, '2020-09-11 13:59:35', '2020-09-11 13:59:35', NULL),
(4429, 'Rishikesh', 3, 28, 1, 1, 1, '2020-09-11 13:59:35', '2020-09-11 13:59:35', NULL),
(4430, 'Roorkee', 3, 28, 1, 1, 1, '2020-09-11 13:59:35', '2020-09-11 13:59:35', NULL),
(4431, 'Roorkee', 3, 28, 1, 1, 1, '2020-09-11 13:59:35', '2020-09-11 13:59:35', NULL),
(4432, 'Rudra Prayag', 3, 28, 1, 1, 1, '2020-09-11 13:59:35', '2020-09-11 13:59:35', NULL),
(4433, 'Rudrapur', 3, 28, 1, 1, 1, '2020-09-11 13:59:35', '2020-09-11 13:59:35', NULL),
(4434, 'Shaktigarh', 3, 28, 1, 1, 1, '2020-09-11 13:59:35', '2020-09-11 13:59:35', NULL),
(4435, 'Sitarganj', 3, 28, 1, 1, 1, '2020-09-11 13:59:35', '2020-09-11 13:59:35', NULL),
(4436, 'Srinagar', 3, 28, 1, 1, 1, '2020-09-11 13:59:35', '2020-09-11 13:59:35', NULL),
(4437, 'Sultanpur', 3, 28, 1, 1, 1, '2020-09-11 13:59:35', '2020-09-11 13:59:35', NULL),
(4438, 'Tanakpur', 3, 28, 1, 1, 1, '2020-09-11 13:59:35', '2020-09-11 13:59:35', NULL),
(4439, 'Tehri', 3, 28, 1, 1, 1, '2020-09-11 13:59:35', '2020-09-11 13:59:35', NULL),
(4440, 'Uttarkashi', 3, 28, 1, 1, 1, '2020-09-11 13:59:35', '2020-09-11 13:59:35', NULL),
(4441, 'Veerbhadra', 3, 28, 1, 1, 1, '2020-09-11 13:59:35', '2020-09-11 13:59:35', NULL),
(4442, 'Vikasnagar', 3, 28, 1, 1, 1, '2020-09-11 13:59:35', '2020-09-11 13:59:35', NULL),
(4443, 'Adra', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4444, 'Ahmadpur', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4445, 'Aiho', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4446, 'Aistala', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4447, 'Alipurduar', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4448, 'Alipurduar Rly.Jnc.', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4449, 'Alpur', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4450, 'Amkula', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4451, 'Amodghata', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4452, 'Amtala', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4453, 'Andul', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4454, 'Anksa', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4455, 'Ankurhati', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4456, 'Anup Nagar', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4457, 'Arambag', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4458, 'Argari', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4459, 'Arra', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4460, 'Asansol', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4461, 'Ashoknagar Kalyangarh', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4462, 'Aurangabad', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4463, 'Bablari Dewanganj', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4464, 'Badhagachhi', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4465, 'Baduria', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4466, 'Bagnan', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4467, 'Baharampur', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4468, 'Bahirgram', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4469, 'Bahula', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4470, 'Baidyabati', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4471, 'Bairatisal', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4472, 'Balaram Pota', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4473, 'Balarampur', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4474, 'Balarampur', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4475, 'Balichak', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4476, 'Ballavpur', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4477, 'Bally', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4478, 'Bally', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4479, 'Balurghat', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4480, 'Bamunari', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4481, 'Banarhat Tea Garden', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4482, 'Bangaon', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4483, 'Bankra', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4484, 'Bankura', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4485, 'Bansberia', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4486, 'Banshra', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4487, 'Banupur', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4488, 'Bara Bamonia', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4489, 'Barabazar', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4490, 'Baranagar', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4491, 'Barasat', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4492, 'Barddhaman', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4493, 'Barijhati', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4494, 'Barjora', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4495, 'Barrackpur', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4496, 'Barrackpur Cantonment', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4497, 'Baruihuda', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4498, 'Baruipur', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4499, 'Basirhat', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4500, 'Baska', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4501, 'Begampur', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4502, 'Beldanga', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4503, 'Beldubi', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4504, 'Belebathan', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4505, 'Beliatore', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4506, 'Bhadreswar', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4507, 'Bhandardaha', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4508, 'Bhangar Raghunathpur', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4509, 'Bhangri Pratham Khanda', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4510, 'Bhanowara', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4511, 'Bhatpara', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4512, 'Bholar Dabri', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4513, 'Bidhan Nagar', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4514, 'Bikihakola', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4515, 'Bilandapur', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4516, 'Bilpahari', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4517, 'Bipra  Noapara', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4518, 'Birlapur', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4519, 'Birnagar', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4520, 'Bishnupur', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4521, 'Bishnupur', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4522, 'Bolpur', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4523, 'Bowali', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4524, 'Budge Budge', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4525, 'Cart Road', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4526, 'Chachanda', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4527, 'Chak Bankola', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4528, 'Chak Bansberia', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4529, 'Chak Enayetnagar', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4530, 'Chak Kashipur', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4531, 'Chakapara', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4532, 'Chakdaha', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4533, 'Champdani', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4534, 'Chamrail', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4535, 'Chandannagar', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4536, 'Chandpur', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4537, 'Chandrakona', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4538, 'Chapari', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4539, 'Chapui', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4540, 'Char Brahmanagar', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4541, 'Char Maijdia', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4542, 'Charka', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4543, 'Chata Kalikapur', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4544, 'Chechakhata', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4545, 'Chelad', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4546, 'Chhora', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4547, 'Chikrand', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4548, 'Chittaranjan', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4549, 'Contai', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4550, 'Cooper\'s Camp', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4551, 'Dafahat', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4552, 'Dainhat', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4553, 'Dakshin Baguan', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4554, 'Dakshin Jhapardaha', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4555, 'Dakshin Rajyadharpur', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4556, 'Dalkhola', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4557, 'Dalurband', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4558, 'Darappur', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4559, 'Darjiling', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4560, 'Debipur', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4561, 'Deuli', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4562, 'Dhakuria', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4563, 'Dhandadihi', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4564, 'Dhanyakuria', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4565, 'Dharmapur', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4566, 'Dhatrigram', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4567, 'Dhuilya', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4568, 'Dhulian', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4569, 'Dhupguri', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4570, 'Dhusaripara', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4571, 'Diamond Harbour', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4572, 'Dignala', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4573, 'Dinhata', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4574, 'Domjur', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4575, 'Dubrajpur', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4576, 'Dumdum', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4577, 'Durgapur', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4578, 'Durllabhganj', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4579, 'Egra', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4580, 'Eksara', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4581, 'English Bazar', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4582, 'Falakata', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4583, 'Farrakka Barrage Township', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4584, 'Fatellapur', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4585, 'Gabberia', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4586, 'Gairkata', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4587, 'Gangarampur', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4588, 'Garalgachha', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4589, 'Garshyamnagar', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4590, 'Garulia', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4591, 'Gayespur', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4592, 'Ghatal', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4593, 'Ghorsala', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4594, 'Goaljan', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4595, 'Goasafat', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4596, 'Gobardanga', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4597, 'Gopalpur', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4598, 'Gopinathpur', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4599, 'Gora Bazar', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4600, 'Guma', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4601, 'Guriahati', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4602, 'Guskara', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4603, 'Habra', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4604, 'Haldia', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4605, 'Haldibari', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4606, 'Halisahar', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4607, 'Haora', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4608, 'Harharia Chak', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4609, 'Haripur', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4610, 'Harishpur', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4611, 'Hatgachha', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4612, 'Hatsimla', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4613, 'Hijuli', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4614, 'Hindusthan Cables Town', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4615, 'Hugli-Chinsurah', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4616, 'Ichhapur Defence  Estate', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4617, 'Islampur', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4618, 'Jafarpur', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4619, 'Jagadanandapur', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4620, 'Jagadishpur', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4621, 'Jagtaj', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4622, 'Jala Kendua', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4623, 'Jalpaiguri', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4624, 'Jamuria', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4625, 'Jangipur', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4626, 'Jaygaon', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4627, 'Jaynagar Mazilpur', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4628, 'Jemari', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4629, 'Jemari  (J.K. Nagar Township)', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4630, 'Jetia', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4631, 'Jhalda', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4632, 'Jhargram', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4633, 'Jhorhat', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4634, 'Jiaganj Azimganj', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4635, 'Jot Kamal', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4636, 'Kachu Pukur', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4637, 'Kajora', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4638, 'Kakdihi', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4639, 'Kalara', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4640, 'Kaliaganj', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4641, 'Kalimpong', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4642, 'Kalna', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4643, 'Kalyani', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4644, 'Kamarhati', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4645, 'Kanaipur', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4646, 'Kanchrapara', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4647, 'Kandi', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4648, 'Kankuria', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4649, 'Kantlia', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4650, 'Kanyanagar', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4651, 'Karimpur', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4652, 'Kasba', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4653, 'Kasim Bazar', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4654, 'Katwa', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4655, 'Kaugachhi', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4656, 'Kenda', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4657, 'Kendra Khottamdi', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4658, 'Kendua', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4659, 'Kesabpur', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4660, 'Khagrabari', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4661, 'Khalia', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4662, 'Khalor', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4663, 'Khandra', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4664, 'Khantora', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4665, 'Kharagpur', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4666, 'Kharagpur Rly. Settlement ', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4667, 'Kharar', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4668, 'Khardaha', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4669, 'Kharimala Khagrabari', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4670, 'Kharsarai', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4671, 'Khodarampur', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4672, 'Koch Bihar', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4673, 'Kodalia', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4674, 'Kolaghat', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4675, 'Kolkata', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4676, 'Konardihi', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4677, 'Konnagar', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4678, 'Krishnanagar', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4679, 'Krishnapur', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4680, 'Kshidirpur', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4681, 'Kshirpai', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4682, 'Kulihanda', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4683, 'Kulti', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4684, 'Kunustara', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4685, 'Kurseong', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4686, 'Madanpur', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4687, 'Madhusudanpur', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4688, 'Madhyamgram', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4689, 'Maheshtala', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4690, 'Mahiari', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4691, 'Mahira', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4692, 'Mainaguri', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4693, 'Makardaha', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4694, 'Mal', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4695, 'Mandarbani', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4696, 'Manikpur', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4697, 'Mansinhapur', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4698, 'Maslandapur', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4699, 'Mathabhanga', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4700, 'Medinipur', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4701, 'Mekliganj', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4702, 'Memari', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4703, 'Mirik', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4704, 'Monoharpur', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4705, 'Mrigala', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4706, 'Muragachha', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4707, 'Murgathaul', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4708, 'Murshidabad', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4709, 'Nabadwip', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4710, 'Nabagram', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4711, 'Nabagram Colony', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4712, 'Nabgram', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4713, 'Nachhratpur Katabari', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4714, 'Naihati', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4715, 'Nasra', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4716, 'Natibpur', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4717, 'Naupala', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4718, 'Nebadhai Duttapukur', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4719, 'New Barrackpur', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4720, 'Nibra', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL);
INSERT INTO `city` (`id`, `city`, `country_name`, `state_name`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(4721, 'Nokpul', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4722, 'North Barrackpur', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4723, 'North Dumdum', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4724, 'Old Maldah', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4725, 'Ondal', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4726, 'Pairagachha', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4727, 'Palashban', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4728, 'Panchla', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4729, 'Panchpara', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4730, 'Pandua', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4731, 'Pangachhiya (B)', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4732, 'Paniara', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4733, 'Panihati', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4734, 'Panuhat', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4735, 'Par Beliya', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4736, 'Parashkol', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4737, 'Parasia', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4738, 'Parbbatipur', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4739, 'Paschim  Jitpur', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4740, 'Paschim Punropara', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4741, 'Pattabong Tea Garden', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4742, 'Patuli', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4743, 'Patulia', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4744, 'Phulia', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4745, 'Podara', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4746, 'Prayagpur', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4747, 'Pujali', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4748, 'Purba Tajpur', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4749, 'Puruliya', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4750, 'Raghudebbati', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4751, 'Raghunathchak', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4752, 'Raghunathpur', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4753, 'Raghunathpur (PS-Dankuni)', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4754, 'Raghunathpur (PS-Magra)', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4755, 'Raigachhi', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4756, 'Raiganj', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4757, 'Rajarhat Gopalpur', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4758, 'Rajpur Sonarpur', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4759, 'Ramchandrapur', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4760, 'Ramjibanpur', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4761, 'Ramnagar', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4762, 'Rampurhat', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4763, 'Ranaghat', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4764, 'Raniganj', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4765, 'Ratibati', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4766, 'Rishra', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4767, 'Rishra', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4768, 'Ruiya', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4769, 'S.T. Power Project Town', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4770, 'Sadpur', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4771, 'Sahajadpur', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4772, 'Sahapur', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4773, 'Sainthia', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4774, 'Salap', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4775, 'Sankarpur', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4776, 'Sankrail', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4777, 'Santipur', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4778, 'Santoshpur', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4779, 'Sarenga', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4780, 'Sarpi', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4781, 'Satigachha', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4782, 'Serampore', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4783, 'Serpur', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4784, 'Shankhanagar', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4785, 'Siduli', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4786, 'Siliguri', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4787, 'Simla', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4788, 'Singur', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4789, 'Sirsha', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4790, 'Sobhaganj', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4791, 'Sonamukhi', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4792, 'Sonatikiri', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4793, 'South Dumdum', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4794, 'Srikantabati', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4795, 'Srirampur', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4796, 'Sukdal', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4797, 'Suri', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4798, 'Taherpur', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4799, 'Taki', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4800, 'Talbandha', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4801, 'Tamluk', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4802, 'Tarakeswar', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4803, 'Tentulkuli', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4804, 'Titagarh', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4805, 'Tufanganj', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4806, 'Ukhra', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4807, 'Uluberia', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4808, 'Uttar Bagdogra', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4809, 'Uttar Durgapur', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4810, 'Uttar Goara', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4811, 'Uttar Kalas', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4812, 'Uttar Kamakhyaguri', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4813, 'Uttar Latabari', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4814, 'Uttar Mahammadpur', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4815, 'Uttar Pirpur', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4816, 'Uttar Raypur', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL),
(4817, 'Uttarpara Kotrung', 3, 29, 1, 1, 1, '2020-09-11 14:03:50', '2020-09-11 14:03:50', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `consultant`
--

CREATE TABLE `consultant` (
  `id` int(10) UNSIGNED NOT NULL,
  `c_patient_id` int(11) DEFAULT NULL,
  `c_doctor_id` int(11) DEFAULT NULL,
  `c_book_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `consultant_note` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `consultant`
--

INSERT INTO `consultant` (`id`, `c_patient_id`, `c_doctor_id`, `c_book_id`, `consultant_note`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 19, 1, 'book-00001', 'test data gg tt', 35, 35, '2020-05-16 11:58:26', '2020-05-16 11:58:38', NULL),
(2, 19, 1, 'book-0000011', 'test consultant1', 35, 35, '2020-05-16 12:39:32', '2020-05-16 12:39:38', NULL),
(3, 208, 144, 'book-0000075', 'gvvhvvnv', 309, 309, '2020-09-27 16:55:40', '2020-09-27 16:55:40', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `counselings_list`
--

CREATE TABLE `counselings_list` (
  `id` int(10) UNSIGNED NOT NULL,
  `dr_id` int(11) NOT NULL,
  `counseling_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `counselings_list`
--

INSERT INTO `counselings_list` (`id`, `dr_id`, `counseling_name`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 114, 'demo', 249, 249, '2020-10-03 17:51:51', '2020-10-03 17:53:33', '2020-10-03 17:53:33'),
(2, 0, 'test counseling', 307, 307, '2020-10-03 19:12:32', '2020-10-03 19:12:32', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `id` int(10) UNSIGNED NOT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '1' COMMENT '0 for inactive 1 for active',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`id`, `country`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(3, 'India', 1, 1, 1, '2020-04-07 01:30:41', '2020-04-10 09:42:28', NULL),
(4, 'USA', 1, 1, 1, '2020-06-22 07:42:42', '2020-06-22 07:42:42', NULL),
(5, 'Siriya', 1, 1, 1, '2020-06-22 07:42:57', '2020-06-22 07:42:57', NULL),
(6, 'Koriya', 1, 1, 1, '2020-06-22 07:43:12', '2020-06-22 07:43:12', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `coupan`
--

CREATE TABLE `coupan` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` int(11) DEFAULT NULL,
  `promo_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `used_promo_code_time` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `sdate` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `edate` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `coupan`
--

INSERT INTO `coupan` (`id`, `type`, `promo_code`, `used_promo_code_time`, `amount_type`, `price`, `sdate`, `edate`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, NULL, '123456', '10', '1', 50, '2020-04-17 02:02:02', '2020-08-26 02:02:02', '2020-04-17 11:20:10', '2020-04-30 05:10:12', NULL),
(2, NULL, '147852', '10', '1', 100, '2020-05-01 00:00:00', '2020-09-11 00:00:00', '2020-05-01 12:50:03', '2020-09-12 13:02:12', NULL),
(3, NULL, 'Welcome', '3', '2', 10, '2020-09-12 00:00:00', '2020-09-30 00:00:00', '2020-09-12 13:03:41', '2020-09-12 13:03:41', NULL),
(4, 11, 'pharmacies100', '1', '1', 100, '2020-11-13', '2020-11-20', '2020-11-13 19:16:26', '2020-11-13 19:16:26', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `couponrates`
--

CREATE TABLE `couponrates` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rate` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `category_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `promo_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `couponrates`
--

INSERT INTO `couponrates` (`id`, `name`, `rate`, `created_at`, `updated_at`, `category_id`, `promo_code`) VALUES
(1, 'Pharmacy', '250', '2021-01-02 16:12:02', '2021-01-02 16:12:02', '11', '100promo'),
(2, 'Labs', '1250', '2021-01-02 16:12:39', '2021-01-02 16:12:39', '12', '100promo'),
(3, 'Imaging Centre', '3750', '2021-01-02 16:13:18', '2021-01-02 16:13:18', '13', '100promo'),
(4, 'Theraphy centre', '5000', '2021-01-02 16:13:52', '2021-01-02 16:13:52', '16', '100promo'),
(5, 'Home Care', '5000', '2021-01-02 16:14:22', '2021-01-02 16:14:22', '14', '100promo'),
(6, 'Councelling', '5000', '2021-01-02 16:14:49', '2021-01-02 16:14:49', '17', '100promo'),
(7, 'welcome', '100', NULL, NULL, '10', 'promo100');

-- --------------------------------------------------------

--
-- Table structure for table `coupons`
--

CREATE TABLE `coupons` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_d` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Category_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Rate` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `promo_code` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` smallint(6) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `coupons`
--

INSERT INTO `coupons` (`id`, `user_id`, `category_d`, `Category_name`, `Rate`, `promo_code`, `status`, `created_at`, `updated_at`) VALUES
(17, '88', '11', 'Pharmacy', '250', '100promo', 0, '2021-01-14 21:41:36', '2021-01-21 20:23:44'),
(18, '96', '14', 'Home Care', '5000', NULL, 1, '2021-01-14 22:23:19', '2021-01-14 22:23:19'),
(19, '98', '11', 'Pharmacy', '250', NULL, 1, '2021-01-14 22:26:23', '2021-01-14 22:26:23'),
(20, '102', '11', 'Pharmacy', '250', NULL, 1, '2021-01-14 22:33:53', '2021-01-14 22:33:53'),
(21, '114', '11', 'Pharmacy', '250', NULL, 1, '2021-01-17 19:09:12', '2021-01-17 19:09:12'),
(22, '144', '11', 'Pharmacy', '250', '100promo', 1, '2021-01-21 22:34:04', '2021-01-21 22:34:04'),
(23, '130', '11', 'Pharmacy', '250', '100promo', 1, '2021-01-21 22:37:37', '2021-01-21 22:37:37'),
(24, '119', '11', 'Pharmacy', '250', '100promo', 1, '2021-01-21 22:40:35', '2021-01-21 22:40:35');

-- --------------------------------------------------------

--
-- Table structure for table `create_offer_360`
--

CREATE TABLE `create_offer_360` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `offer_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `create_offer_360`
--

INSERT INTO `create_offer_360` (`id`, `user_id`, `offer_id`, `created_by`, `updated_by`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 97, '1', 97, 97, NULL, '2020-06-10 06:27:08', '2020-06-11 05:47:50', NULL),
(2, 108, '2', 108, 108, NULL, '2020-06-12 06:59:39', '2020-06-12 06:59:39', NULL),
(3, 108, '1,2', 108, 108, NULL, '2020-06-12 07:03:33', '2020-06-12 07:03:37', NULL),
(4, 105, '1', 105, 105, NULL, '2020-06-12 07:04:47', '2020-06-12 07:04:47', NULL),
(5, 109, '1', 109, 109, NULL, '2020-06-13 12:57:55', '2020-06-13 12:57:55', NULL),
(6, 115, '1,2,3', 115, 115, NULL, '2020-06-22 09:33:26', '2020-06-22 13:13:40', NULL),
(7, 57, '1', 57, 57, NULL, '2020-06-27 11:00:22', '2020-06-27 11:00:22', NULL),
(8, 233, '1', 233, 233, NULL, '2020-08-21 11:43:44', '2020-08-21 11:43:44', NULL),
(9, 371, '1,4', 371, 371, NULL, '2020-10-27 13:21:16', '2020-11-05 02:16:35', NULL),
(10, 376, '1,3,4', 376, 376, NULL, '2020-10-28 22:37:33', '2020-10-30 19:04:03', NULL),
(11, 425, '1', 425, 425, NULL, '2020-12-02 20:25:13', '2020-12-02 20:25:13', NULL),
(12, 429, '4', 429, 429, NULL, '2020-12-15 20:18:10', '2020-12-15 20:18:10', NULL),
(13, 434, '1,3,4', 434, 434, NULL, '2020-12-26 19:33:37', '2020-12-26 19:33:37', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `doctor_availability`
--

CREATE TABLE `doctor_availability` (
  `id` int(10) UNSIGNED NOT NULL,
  `doctor_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `time_status` tinyint(4) DEFAULT NULL COMMENT '1 for weekly, 2 for monthly, 3 for yearly, 4 for temporly ',
  `nonavabile_time_slot` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `booking_time_slot` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mrg_from_time` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mrg_to_time` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `aft_from_time` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `aft_to_time` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `eve_from_time` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `eve_to_time` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` smallint(6) NOT NULL DEFAULT '1' COMMENT '0 for inactive 1 for active',
  `doctor_status` int(11) DEFAULT '0' COMMENT '0 = Full Day , 1 = Morning, 2 = afternoon, 3 = Evening',
  `created_by` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `updated_by` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `doctor_availability`
--

INSERT INTO `doctor_availability` (`id`, `doctor_id`, `date`, `time_status`, `nonavabile_time_slot`, `booking_time_slot`, `mrg_from_time`, `mrg_to_time`, `aft_from_time`, `aft_to_time`, `eve_from_time`, `eve_to_time`, `status`, `doctor_status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, '140', '2020-09-16', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '304', '304', '2020-09-16 18:45:42', '2020-09-16 23:08:55'),
(2, '91', '2020-10-18', 4, '06:30,13:45,20:30', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, '238', '238', '2020-09-16 18:46:40', '2020-09-16 18:46:40'),
(3, '114', '2020-09-17', 4, '08:30,09:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '266', '266', '2020-09-16 18:58:38', '2020-09-16 18:58:38'),
(4, '114', '2020-09-18', 4, '08:30,09:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '266', '266', '2020-09-16 18:58:38', '2020-09-16 18:58:38'),
(5, '91', '2020-10-18', 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '238', '238', '2020-09-16 19:09:17', '2020-09-16 19:09:17'),
(6, '91', '2020-10-19', 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '238', '238', '2020-09-16 19:09:17', '2020-09-16 19:09:17'),
(7, '140', '2020-01-04', 4, '05:00,18:40', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '304', '304', '2020-09-16 23:08:42', '2020-09-16 23:09:56'),
(8, '140', '2020-02-04', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '304', '304', '2020-09-16 23:08:42', '2020-09-16 23:08:42'),
(9, '140', '2020-03-04', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '304', '304', '2020-09-16 23:08:42', '2020-09-16 23:08:55'),
(10, '140', '2020-04-04', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '304', '304', '2020-09-16 23:08:42', '2020-09-16 23:08:42'),
(11, '140', '2020-05-04', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '304', '304', '2020-09-16 23:08:42', '2020-09-16 23:08:42'),
(12, '140', '2020-06-04', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '304', '304', '2020-09-16 23:08:42', '2020-09-16 23:08:42'),
(13, '140', '2020-07-04', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '304', '304', '2020-09-16 23:08:42', '2020-09-16 23:08:42'),
(14, '140', '2020-08-04', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '304', '304', '2020-09-16 23:08:42', '2020-09-16 23:08:42'),
(15, '140', '2020-09-04', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '304', '304', '2020-09-16 23:08:42', '2020-09-16 23:08:42'),
(16, '140', '2020-10-04', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '304', '304', '2020-09-16 23:08:42', '2020-09-16 23:08:42'),
(17, '140', '2020-11-04', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '304', '304', '2020-09-16 23:08:42', '2020-09-16 23:08:55'),
(18, '140', '2020-12-04', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '304', '304', '2020-09-16 23:08:42', '2020-09-16 23:08:42'),
(19, '140', '2020-01-20', 4, '05:00,18:40', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '304', '304', '2020-09-16 23:08:42', '2020-09-16 23:09:56'),
(20, '140', '2020-02-20', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '304', '304', '2020-09-16 23:08:42', '2020-09-16 23:08:42'),
(21, '140', '2020-03-20', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '304', '304', '2020-09-16 23:08:42', '2020-09-16 23:08:42'),
(22, '140', '2020-04-20', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '304', '304', '2020-09-16 23:08:42', '2020-09-16 23:08:42'),
(23, '140', '2020-05-20', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '304', '304', '2020-09-16 23:08:42', '2020-09-16 23:08:55'),
(24, '140', '2020-06-20', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '304', '304', '2020-09-16 23:08:42', '2020-09-16 23:08:42'),
(25, '140', '2020-07-20', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '304', '304', '2020-09-16 23:08:42', '2020-09-16 23:08:42'),
(26, '140', '2020-08-20', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '304', '304', '2020-09-16 23:08:42', '2020-09-16 23:08:42'),
(27, '140', '2020-09-20', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '304', '304', '2020-09-16 23:08:42', '2020-09-16 23:08:42'),
(28, '140', '2020-10-20', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '304', '304', '2020-09-16 23:08:42', '2020-09-16 23:08:42'),
(29, '140', '2020-11-20', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '304', '304', '2020-09-16 23:08:42', '2020-09-16 23:08:42'),
(30, '140', '2020-12-20', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '304', '304', '2020-09-16 23:08:42', '2020-09-16 23:08:42'),
(31, '140', '2020-01-23', 4, '05:00,18:40', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '304', '304', '2020-09-16 23:08:42', '2020-09-16 23:09:56'),
(32, '140', '2020-02-23', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '304', '304', '2020-09-16 23:08:42', '2020-09-16 23:08:42'),
(33, '140', '2020-03-23', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '304', '304', '2020-09-16 23:08:42', '2020-09-16 23:08:42'),
(34, '140', '2020-04-23', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '304', '304', '2020-09-16 23:08:42', '2020-09-16 23:08:42'),
(35, '140', '2020-05-23', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '304', '304', '2020-09-16 23:08:42', '2020-09-16 23:08:42'),
(36, '140', '2020-06-23', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '304', '304', '2020-09-16 23:08:42', '2020-09-16 23:08:42'),
(37, '140', '2020-07-23', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '304', '304', '2020-09-16 23:08:42', '2020-09-16 23:08:42'),
(38, '140', '2020-08-23', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '304', '304', '2020-09-16 23:08:42', '2020-09-16 23:08:42'),
(39, '91', '2020-09-23', 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '238', '238', '2020-09-16 23:08:42', '2020-09-17 00:06:18'),
(40, '140', '2020-10-23', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '304', '304', '2020-09-16 23:08:42', '2020-09-16 23:08:42'),
(41, '140', '2020-11-23', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '304', '304', '2020-09-16 23:08:42', '2020-09-16 23:08:42'),
(42, '140', '2020-12-23', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '304', '304', '2020-09-16 23:08:42', '2020-09-16 23:08:55'),
(43, '140', '2020-01-08', 4, '05:00,18:40', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '304', '304', '2020-09-16 23:08:55', '2020-09-16 23:09:56'),
(44, '140', '2020-01-15', 4, '05:00,18:40', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '304', '304', '2020-09-16 23:08:55', '2020-09-16 23:09:56'),
(45, '140', '2020-01-22', 4, '05:00,18:40', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '304', '304', '2020-09-16 23:08:55', '2020-09-16 23:09:56'),
(46, '140', '2020-01-29', 4, '05:00,18:40', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '304', '304', '2020-09-16 23:08:55', '2020-09-16 23:09:56'),
(47, '140', '2020-02-05', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '304', '304', '2020-09-16 23:08:55', '2020-09-16 23:08:55'),
(48, '140', '2020-02-12', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '304', '304', '2020-09-16 23:08:55', '2020-09-16 23:08:55'),
(49, '140', '2020-02-19', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '304', '304', '2020-09-16 23:08:55', '2020-09-16 23:08:55'),
(50, '140', '2020-02-26', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '304', '304', '2020-09-16 23:08:55', '2020-09-16 23:08:55'),
(51, '140', '2020-03-11', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '304', '304', '2020-09-16 23:08:55', '2020-09-16 23:08:55'),
(52, '140', '2020-03-18', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '304', '304', '2020-09-16 23:08:55', '2020-09-16 23:08:55'),
(53, '140', '2020-03-25', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '304', '304', '2020-09-16 23:08:55', '2020-09-16 23:08:55'),
(54, '140', '2020-04-01', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '304', '304', '2020-09-16 23:08:55', '2020-09-16 23:08:55'),
(55, '140', '2020-04-08', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '304', '304', '2020-09-16 23:08:55', '2020-09-16 23:08:55'),
(56, '140', '2020-04-15', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '304', '304', '2020-09-16 23:08:55', '2020-09-16 23:08:55'),
(57, '140', '2020-04-22', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '304', '304', '2020-09-16 23:08:55', '2020-09-16 23:08:55'),
(58, '140', '2020-04-29', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '304', '304', '2020-09-16 23:08:55', '2020-09-16 23:08:55'),
(59, '140', '2020-05-06', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '304', '304', '2020-09-16 23:08:55', '2020-09-16 23:08:55'),
(60, '140', '2020-05-13', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '304', '304', '2020-09-16 23:08:55', '2020-09-16 23:08:55'),
(61, '140', '2020-05-27', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '304', '304', '2020-09-16 23:08:55', '2020-09-16 23:08:55'),
(62, '140', '2020-06-03', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '304', '304', '2020-09-16 23:08:55', '2020-09-16 23:08:55'),
(63, '140', '2020-06-10', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '304', '304', '2020-09-16 23:08:55', '2020-09-16 23:08:55'),
(64, '140', '2020-06-17', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '304', '304', '2020-09-16 23:08:55', '2020-09-16 23:08:55'),
(65, '140', '2020-06-24', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '304', '304', '2020-09-16 23:08:55', '2020-09-16 23:08:55'),
(66, '140', '2020-07-01', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '304', '304', '2020-09-16 23:08:55', '2020-09-16 23:08:55'),
(67, '140', '2020-07-08', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '304', '304', '2020-09-16 23:08:55', '2020-09-16 23:08:55'),
(68, '140', '2020-07-15', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '304', '304', '2020-09-16 23:08:55', '2020-09-16 23:08:55'),
(69, '140', '2020-07-22', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '304', '304', '2020-09-16 23:08:55', '2020-09-16 23:08:55'),
(70, '140', '2020-07-29', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '304', '304', '2020-09-16 23:08:55', '2020-09-16 23:08:55'),
(71, '140', '2020-08-05', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '304', '304', '2020-09-16 23:08:55', '2020-09-16 23:08:55'),
(72, '140', '2020-08-12', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '304', '304', '2020-09-16 23:08:55', '2020-09-16 23:08:55'),
(73, '140', '2020-08-19', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '304', '304', '2020-09-16 23:08:55', '2020-09-16 23:08:55'),
(74, '140', '2020-08-26', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '304', '304', '2020-09-16 23:08:55', '2020-09-16 23:08:55'),
(75, '140', '2020-09-02', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '304', '304', '2020-09-16 23:08:55', '2020-09-16 23:08:55'),
(76, '140', '2020-09-09', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '304', '304', '2020-09-16 23:08:55', '2020-09-16 23:08:55'),
(77, '140', '2020-09-30', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '304', '304', '2020-09-16 23:08:55', '2020-09-16 23:08:55'),
(78, '140', '2020-10-07', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '304', '304', '2020-09-16 23:08:55', '2020-09-16 23:08:55'),
(79, '140', '2020-10-14', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '304', '304', '2020-09-16 23:08:55', '2020-09-16 23:08:55'),
(80, '140', '2020-10-21', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '304', '304', '2020-09-16 23:08:55', '2020-09-16 23:08:55'),
(81, '140', '2020-10-28', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '304', '304', '2020-09-16 23:08:55', '2020-09-16 23:08:55'),
(82, '140', '2020-11-11', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '304', '304', '2020-09-16 23:08:55', '2020-09-16 23:08:55'),
(83, '140', '2020-11-18', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '304', '304', '2020-09-16 23:08:55', '2020-09-16 23:08:55'),
(84, '140', '2020-11-25', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '304', '304', '2020-09-16 23:08:55', '2020-09-16 23:08:55'),
(85, '140', '2020-12-02', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '304', '304', '2020-09-16 23:08:55', '2020-09-16 23:08:55'),
(86, '140', '2020-12-09', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '304', '304', '2020-09-16 23:08:55', '2020-09-16 23:08:55'),
(87, '140', '2020-12-16', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '304', '304', '2020-09-16 23:08:55', '2020-09-16 23:08:55'),
(88, '140', '2020-12-30', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '304', '304', '2020-09-16 23:08:55', '2020-09-16 23:08:55'),
(89, '140', '2020-01-14', 4, '05:00,18:40', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '304', '304', '2020-09-16 23:09:56', '2020-09-16 23:09:56'),
(90, '91', '2020-09-21', 4, '06:45,14:15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, '238', '238', '2020-09-17 00:01:48', '2020-09-17 00:01:48'),
(91, '91', '2020-09-22', 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '238', '238', '2020-09-17 00:04:22', '2020-09-17 00:04:22'),
(92, '91', '2020-09-24', 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 2, '238', '238', '2020-09-17 00:06:41', '2020-09-17 00:06:41'),
(93, '91', '2020-09-25', 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 3, '238', '238', '2020-09-17 00:07:35', '2020-09-17 00:07:35'),
(94, '160', '2020-11-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '383', '383', '2020-11-08 00:45:59', '2020-11-08 00:45:59');

-- --------------------------------------------------------

--
-- Table structure for table `doctor_details`
--

CREATE TABLE `doctor_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `d_uni_id` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plan_id` int(11) DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bio` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pincode` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `main_mobile` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `second_mobile` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `clinic_team_leader` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `clinic_admin_mobile_no` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `clinic_email` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `experience` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `language` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `speciality` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `licence_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `licence_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `valide_upto` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `issued_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `licence_copy` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `profile_pic` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `clinic_logo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bank_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `account_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ifsc_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `account_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cheque_img` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `watsapp_number` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `youtube_link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `teligram_number` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` smallint(6) NOT NULL DEFAULT '1' COMMENT '0 for active, 1 for deactive',
  `step` tinyint(4) DEFAULT NULL,
  `gst_number` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hospital_id` int(11) DEFAULT NULL COMMENT 'hospital ragister table id',
  `created_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `year_of_graduation` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `clinic_wallpapper` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `clinic_name` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `clinic_buisness_name` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pan_number` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `doctor_details`
--

INSERT INTO `doctor_details` (`id`, `d_uni_id`, `plan_id`, `first_name`, `last_name`, `email`, `bio`, `pincode`, `state`, `city`, `address`, `main_mobile`, `second_mobile`, `clinic_team_leader`, `clinic_admin_mobile_no`, `clinic_email`, `experience`, `language`, `speciality`, `price`, `licence_type`, `licence_no`, `valide_upto`, `issued_by`, `licence_copy`, `profile_pic`, `clinic_logo`, `bank_name`, `account_name`, `ifsc_code`, `account_no`, `cheque_img`, `watsapp_number`, `youtube_link`, `teligram_number`, `status`, `step`, `gst_number`, `hospital_id`, `created_by`, `updated_by`, `created_at`, `updated_at`, `year_of_graduation`, `clinic_wallpapper`, `clinic_name`, `clinic_buisness_name`, `pan_number`) VALUES
(11, NULL, NULL, 'ram11', 'raja', 'ram@admin.in', 'dshsajdk', '360002', '1', '1', 'adasdsadasd', '1234567890', '1234567890', 'akash', NULL, NULL, '5', '3', '4', '112', '1', '123456', '2020-04-20 00:00:00', 'amit', '1587116484__Consult today.png', '1587116484__consult.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, '2020-04-17 09:41:24', '2020-09-20 23:09:40', NULL, NULL, NULL, NULL, NULL),
(43, 'D-000003', NULL, 'Ramesh', 'Kumar', 'DrRameshKumar@haspatal.in', 'Dr Ramesh Kumar is a senior doctor with 15 years of work experience. He has been awarded many times.\r\n\r\nD. N. B  Neurosurgery training      (2004-2009)', '304371', '5', '6', 'Boi', '7079708269', '7079708284', 'Physicians', NULL, NULL, '6', '4', '10', '212', '1', '675', '2020-07-22', 'By', '1595859931__ac195b27-32f1-4087-bf42-9647436a0625-nevada_medical_marijuana_card.jpg', '1595859931__depositphotos_33044395-stock-photo-doctor-smiling.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '1', '1', '2020-07-27 21:25:31', '2020-09-20 23:09:40', NULL, NULL, NULL, NULL, NULL),
(91, NULL, NULL, 'helan', 'patel', 'h@h.com', 'test', '360004', '1', '2', 'Rajkot', '9904054870', NULL, 'helan', NULL, NULL, '25', '1,3,4', '2', '66', '1', 'vab', '2020-08-24', NULL, '1598248379__AatyInfoScreenMockup_02.png', '1598248379__AatyInfoScreenMockup_02.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, '2020-08-24 12:53:00', '2020-09-20 23:09:40', NULL, NULL, NULL, NULL, NULL),
(95, NULL, NULL, 'aman', 'varma', 'aman@haspatal.com', 'test  doctor bio', '110029', '5', '60', 'test address', '9106250417', NULL, 'saurabh', NULL, NULL, '18', '3,4', '10', '500', '2', 'abcd1234', '2025-08-31', NULL, '1598716923__Image-5591.jpg', '1598853900__IMG_20200715_065825.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, '2020-08-29 23:02:03', '2020-10-04 18:18:55', NULL, NULL, NULL, NULL, NULL),
(113, NULL, NULL, 'd', 'd', 'd@d.com', 'hshahaba', '788788', '1', '3', 'usuauajajaja', '9727540718', '9727540718', 'hshahs', NULL, NULL, '5', '8,9,10', '2', '52', '1', 'jwjajsjs', '2020-09-26', NULL, '1599373465__Image-6478.jpg', '1599373465__Image-1134.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, '2020-09-06 13:24:25', '2020-09-20 23:09:40', NULL, NULL, NULL, NULL, NULL),
(114, '91-D-0000002', 2, 'demo', NULL, 'jaydeepamethiya@gmail.com', 'demo demo demo demo demo demo demo demo demo demo demo demo demo demo demo demo demo demo demo demo demo demo demo demo demo demo demo demo demo h', '361150', '1', '659', 'demo demo', '9106909336', '9797979797', 'fdfdf', '7878787878', 'demo@clinic.com', NULL, '8,9,10', '2,6,9', '40', '3', 'gfgdgdfgdfgdfg', '01-08-2020', 'sfsfdfdfs', '1601285125__Image-6686.jpg', '1599378422__5.jpg', '1599378423__5.jpg', 'demo', '8488181818', 'dddd4455', '999999999999', '1602740228__1.jpg', NULL, NULL, NULL, 0, 2, '', NULL, NULL, NULL, '2020-09-06 14:26:46', '2020-11-13 19:40:38', '2020', '1599378423__5.jpg', 'demo_clinic', 'demo deo', '515151'),
(125, NULL, NULL, 'Rajiv', 'Chopra', 'rajiv@haspatal.com', 'this is bio', NULL, '1', '3', 'ahmedabad address', '9974573522', '8866252555', NULL, '8866252555', 'rajesh kapur', NULL, '4,1,3', '17,18,19', '2', '1', 'Abcd1234', '2025-09-27', 'government india', NULL, '1599650811__IMG_20200621_135359.jpg', '1599650854__Image-4270.jpg', 'HDFC', 'health care', 'hdfc1234gg', NULL, NULL, NULL, NULL, NULL, 0, 8, NULL, NULL, NULL, NULL, '2020-09-09 18:23:36', '2020-09-20 23:09:40', '1999', '1599650811__IMG_20200809_113616.jpg', 'health care', 'Health care', 'abcd1234gh'),
(129, NULL, NULL, 'harsh', 'bhandari', 'harsh.rockland@gmail.com', 'zbbshsjnsnnsm', NULL, NULL, NULL, NULL, '7982150690', '9582810009', NULL, '9582810009', 'harsh.rockland@gmail.com', NULL, '4', '10', '2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 4, NULL, NULL, NULL, NULL, '2020-09-10 12:37:21', '2020-09-20 23:09:40', '1986', NULL, NULL, NULL, NULL),
(133, NULL, NULL, 'Vijay', 'Sonam', 'vijaysonamp@gmail.com', 'MBBS MD \nClinic in Chandigarh', NULL, '8', '201', '601 Vikas Nagar Naya Gaon Chandigarh', '9988008760', '9988898966', NULL, '9988633960', 'mediconcepthealthcare@gmail.com', NULL, '4,3,10', '28', '500', '3', '123456789', '2021-09-10', 'MCI', NULL, '1599748813__20200906_114926.jpg', '1599748870__Photo_1598165393091.jpg', 'ICICI', 'Ayushi Clinica', 'ICICI00019', NULL, NULL, NULL, NULL, NULL, 0, 8, NULL, NULL, NULL, NULL, '2020-09-10 21:38:06', '2020-09-21 17:17:39', '2004', NULL, 'Ayushi Clinic', 'Ayushi Clinic', 'BIK786543'),
(136, NULL, 4, 'ashik', 'manoorayil', 'ashikmannorayil@gmail.com', 'MBBS', NULL, '3', '1622', 'medical college', '9746043286', '7994646416', NULL, '9633631592', 'mastores01@gmail.com', NULL, '5', '28', '1', '3', '8889900', '2021-03-18', 'ima', NULL, '1599822975__Haspatal.png', '1599822996__Haspatal.png', 'axis', 'ma', 'axis0007778', NULL, NULL, NULL, NULL, NULL, 0, 8, '', NULL, NULL, NULL, '2020-09-11 15:57:57', '2020-10-20 11:47:51', '2008', '1599822975__Haspatal.png', 'ma', 'ma clinic', 'yyyyaaap'),
(139, NULL, NULL, 'Samir', 'joshi', 'samir@haspatal.com', 'test bio', NULL, '1', '659', 'test', '9825700078', '8866252555', NULL, '9974573522', 'test@test.com', NULL, '4,3', '17,18,19', '2', '3', 'abcd1234', '2028-09-11', 'govt india', NULL, '1599821221__Image-9766.jpg', '1599821298__Image-9045.jpg', 'icici', '123456778', 'abcd1234gg', NULL, NULL, NULL, NULL, NULL, 1, 8, NULL, NULL, NULL, NULL, '2020-09-11 17:44:44', '2020-09-20 23:09:40', '1999', NULL, 'test clinic', 'test clinic', 'abcd1234'),
(141, NULL, NULL, 'Gokul', 'kumar', 'gnoi45@gmail.com', 'hhujj', NULL, '36', '240', 'vjklhgg ddf', '9523558483', '8178924823', NULL, '8178624823', 'gnoi45@gmail.com', NULL, '4', '17,17,17,17,17,17,19,17,19,25', '2', '3', '89900', '2020-09-29', 'govt', NULL, '1600164471__IMG_20200912_080920.jpg', '1600164524__IMG_20200915_140942.jpg', '07900', 'goiul', 'punb', NULL, NULL, NULL, NULL, NULL, 0, 8, NULL, NULL, NULL, NULL, '2020-09-15 17:04:03', '2020-09-20 23:09:40', '4', '1600164471__IMG_20200912_080920.jpg', 'hkk', 'bs', 'dchpkui90'),
(142, NULL, 4, 'vachhani', 'Siddhi', 'siddhivachhani@gmail.com', 'test bio', NULL, '1', '829', 'rajkot.', '8238576669', '8238576669', NULL, '8686868686', 'g@g.com', NULL, '4,1,3', '62,2,12', '202', '3', '6666', '2022-09-17', 'siddhi', '1601280905__Image-5086.jpg', '1600329651__IMG-20200914-WA0000.jpg', '1600329680__Image-6002.jpg', 'bob', 'vachhani Siddhi', 'nxdm', NULL, NULL, NULL, NULL, NULL, 0, 8, '', NULL, NULL, NULL, '2020-09-17 14:59:37', '2020-10-17 21:53:10', '2020', NULL, 'siddhi test clinic', 'siddhi test clinic', 'dbnd'),
(144, '91-D-0000001', 4, 'Navneet', 'Kumar', 'cdnavneet@gmail.comm', 'Focus your resume around its experience and accomplishments sections. These are the most important parts, and should contain the most information.', NULL, '1', '659', 'hdjdj', '95996617000', '9718215656', NULL, '9718215656', 'cdnavneet@gmail.com', NULL, '4,1,3,10', '62,2,12,13,14,15,16,18', '9', '3', '123456', '2020-09-30', 'gfg', '1603995272__20201029_234350.jpg', '1603995065__20201029_234029.jpg', '1600439188__IMG-20200912-WA0003.jpg', 'nskdk', '123344', 'qweer', NULL, NULL, NULL, NULL, NULL, 0, 8, NULL, NULL, NULL, NULL, '2020-09-18 21:20:55', '2020-10-30 01:24:52', '2018', '1600439111__IMG-20200916-WA0013.jpg', 'test clinic', 'hshdh', '123456AB'),
(145, NULL, NULL, 'ashik', 'm', 'mastores01@gmail.com', 'MBBS', '695525', '3', '1616', 'kallur', '9633631592', '7994646416', NULL, '9746043286', 'm.a.absal@gmail.com', NULL, '4,5', '28', '11', '3', '1234566', '2021-02-18', 'iim', NULL, '1600444881__Haspatal.png', '1600444911__Haspatal.png', 'askks', 'abbsb', 'inndi1122', NULL, NULL, NULL, NULL, NULL, 0, 8, NULL, NULL, NULL, NULL, '2020-09-18 22:59:42', '2020-09-24 17:56:00', '2008', '1600444881__Haspatal.png', 'ma', 'ma', '1112344'),
(146, NULL, NULL, 'vishal', 'laroia', 'vishalklaroia@gmail.com', 'xyz', NULL, '11', '924', 'xyz', '9873990994', '8882921234', NULL, '8882921234', 'vishalklaroia@gmail.com', NULL, '4,3,10', '32', '2', '5', '123446', NULL, 'delhi', NULL, '1600607004__IMG_20200426_150034~2.jpg', '1600607041__20200919_153521.jpg', 'icici', '12455678898', 'iciv000001', NULL, NULL, NULL, NULL, NULL, 0, 8, NULL, NULL, NULL, NULL, '2020-09-20 20:01:31', '2020-09-20 23:09:40', '1996', NULL, 'aarogya', 'aarogya world', 'asghj467gn'),
(148, NULL, NULL, 'Dr Ritesh Singh', 'Malik', 'drriteshsinghmalik@gmail.com', 'Dr Ritesh Singh Malik,\nBHMS, Masters in Immunology, Masters in insurance medicine, Masters in Sports Medicine\n\ntreated more then 10000 patients', NULL, NULL, NULL, NULL, '8128389381', '9898651339', NULL, '9898651339', 'drriteshsinghmalik@gmail.com', NULL, '4,1,3,10', '2,12,18,22,24,26,28,10,11,32,38,37,39,40,41,49,50,56,60,61', NULL, NULL, NULL, NULL, NULL, NULL, '1601524908__20181030_161711.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 5, NULL, NULL, NULL, NULL, '2020-10-01 10:53:52', '2020-10-01 11:01:48', '1998', NULL, NULL, NULL, NULL),
(149, NULL, NULL, 'absal', 'm', 'm.a.absal@gmail.com', 'MBBS', NULL, NULL, NULL, NULL, '7994646416', '9633631592', NULL, '9746043286', 'mastores01@gmail.com', NULL, '4,3,5', '28,10', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 4, NULL, NULL, NULL, NULL, '2020-10-01 17:25:44', '2020-10-01 17:26:38', '2008', NULL, NULL, NULL, NULL),
(153, NULL, NULL, 'd', NULL, 'd12345@gmail.com', 'demo demo', NULL, NULL, NULL, NULL, '1236547890', NULL, NULL, NULL, NULL, '1', NULL, '63', NULL, NULL, NULL, NULL, NULL, '1602313365__4.jpg', '1602313365__3.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '341', '341', '2020-10-10 14:02:45', '2020-10-10 14:02:45', NULL, NULL, NULL, NULL, NULL),
(156, NULL, NULL, 'dffg', 'fgfgfgfg', 'ravi@gmail.com', 'bfghfhgfhfg', NULL, NULL, NULL, NULL, '9874563210', NULL, NULL, NULL, NULL, '1', '1,3', '2,6', NULL, '3', '1236547890', '2020', 'delhi', '1602334720__4.jpg', '1602334720__3.jpg', NULL, 'ffgf', 'frmo', '481818', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '341', '341', '2020-10-10 19:58:40', '2020-10-10 19:58:40', '2020', NULL, NULL, NULL, '505050'),
(157, '91-D-0000003', 4, 'Ashish', 'Nimbark', 'ashish@haspatal.com', 'test bio text', NULL, '1', '659', 'Bopal Ahmedabad 58', '8866252555', '8866252555', NULL, '9974573522', 'test@clinic.xyz', NULL, '4,1,3,11', '21', '5000', '4', 'abvd12345678', '2024-10-31', 'govt india', NULL, '1602654394__SAVE_20201013_195149.jpg', '1602654482__Image-8782.jpg', 'icici', 'Dr. Test', 'ixic0001234', '1234567890', NULL, NULL, NULL, NULL, 0, 8, '', NULL, NULL, NULL, '2020-10-14 12:42:42', '2020-10-23 17:48:13', '1998', '1602654394__Screenshot_2020-10-05-18-30-05-018_com.phonepe.app.business.jpg', 'Aastha Clinic', 'aastha clinic', 'abcd1234abc'),
(158, '91-D-0000004', NULL, 'dddddddddd', 'dddddddddd', 'd5@gmail.co,', 'xfhdhfgvbnbvnvbn', NULL, NULL, NULL, NULL, '9106909336', NULL, NULL, NULL, NULL, '1', '3', '2', NULL, '3', '1236547890', '2020', 'delhi', '1602748006__2.jpg', '1602748006__2.jpg', NULL, 'ffgf', 'frmo', '481818', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 13, '355', '355', '2020-10-15 14:46:46', '2020-10-15 14:46:46', '2015', NULL, NULL, NULL, '505050'),
(159, '91-D-0000005', NULL, 'ffffff', 'fffffffff', 'f5@gmail.com', 'fffffffffffffff', NULL, NULL, NULL, NULL, '11111111111', NULL, NULL, NULL, NULL, '1', '3,4', '17,18', NULL, '3', '1236547890', '2020', 'delhi', '1602760673__2.jpg', '1602760673__2.jpg', NULL, 'ffgf', 'frmo', '481818', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 13, '355', '355', '2020-10-15 18:17:53', '2020-10-15 18:17:53', '2015', NULL, NULL, NULL, '505050'),
(160, '91-D-0000006', 1, 'Erik', 'Chank', 'vishaltolani0001@gmail.com', '123#', '110070', '5', '52', 'Vasant Kunj', '9643725091', NULL, 'Vijay', NULL, NULL, '6', '4', '62', '00', '5', 'D-192282', NULL, NULL, '1604649312__Image-1430.jpg', '1604649312__Image-1455.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, '2020-11-06 20:25:12', '2020-11-06 21:53:14', NULL, NULL, NULL, NULL, NULL),
(161, '91-D-0000007', NULL, 'Sandeep', 'singh', 'sandeepraj9595@gmail.com', 'sandeep', NULL, '13', '1336', 'ssdd', '7972185820', '7972185820', NULL, '7972185820', 'sandeepraj9595@gmail.com', NULL, '4,3', '2,12,14,15,16,17,18', NULL, '5', 'vfs sgnsgbsrgr', '2021-01-05', 'testing', NULL, '1609831329__images - 2021-01-04T004600.561.jpeg', '1609831366__images - 2021-01-04T004600.561.jpeg', 'swcrvtbyjykyk', 'cevrbfnhmhny', 'egrbfbfbgnhmhnfb', NULL, NULL, NULL, NULL, NULL, 1, 8, NULL, NULL, NULL, NULL, '2021-01-05 19:50:17', '2021-01-05 19:53:59', '201', NULL, 'Android Testing', 'addfghhh', 'efrhrhrgrgrhrhtht');

-- --------------------------------------------------------

--
-- Table structure for table `Doctor_withdraw`
--

CREATE TABLE `Doctor_withdraw` (
  `id` int(10) UNSIGNED NOT NULL,
  `doctor_id` int(11) DEFAULT NULL,
  `current_balance` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `requested_amount` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `available_balance` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `Doctor_withdraw`
--

INSERT INTO `Doctor_withdraw` (`id`, `doctor_id`, `current_balance`, `requested_amount`, `available_balance`, `created_by`, `updated_by`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, '60050', '50', '60000', 35, 35, NULL, '2020-05-06 05:31:58', '2020-05-06 05:31:58', NULL),
(2, 1, '60000', '2000', '58000', 35, 35, NULL, '2020-05-06 05:59:52', '2020-05-06 05:59:52', NULL),
(3, 1, '68000', '500', '67500', 35, 35, NULL, '2020-05-11 10:54:05', '2020-05-11 10:54:05', NULL),
(4, 1, '67500', '100', '67400', 35, 35, NULL, '2020-05-12 11:21:15', '2020-05-12 11:21:15', NULL),
(5, 1, '67500', '50', '67450', 35, 35, NULL, '2020-05-13 05:57:57', '2020-05-13 05:57:57', NULL),
(6, 1, '67350.0', '500', '66850', 35, 35, NULL, '2020-05-13 07:57:44', '2020-05-13 07:57:44', NULL),
(7, 1, '66850', '850', '66000', 35, 35, NULL, '2020-05-13 09:35:29', '2020-05-13 09:35:29', NULL),
(8, 1, '62000', '100', '61900', 35, 35, NULL, '2020-05-13 09:36:21', '2020-05-13 09:36:21', NULL),
(9, 1, '57800', '800', '57000', 35, 35, NULL, '2020-05-13 09:42:38', '2020-05-13 09:42:38', NULL),
(10, 1, '57000.0', '10000', '47000', 35, 35, NULL, '2020-05-13 10:03:42', '2020-05-13 10:03:42', NULL),
(11, 1, '47000.0', '500', '46500', 35, 35, NULL, '2020-05-13 10:04:43', '2020-05-13 10:04:43', NULL),
(12, 1, '51010.0', '200', '50810', 35, 35, NULL, '2020-06-08 10:43:55', '2020-06-08 10:43:55', NULL),
(13, 1, '50810.0', '5555', '45255', 35, 35, NULL, '2020-06-08 10:59:13', '2020-06-08 10:59:13', NULL),
(14, 1, '50039.0', '5200', '44839', 35, 35, NULL, '2020-06-21 13:32:46', '2020-06-21 13:32:46', NULL),
(15, 1, '40789', '69', '40720', 35, 35, NULL, '2020-06-27 10:32:34', '2020-06-27 10:32:34', NULL),
(16, 1, '41820', '20', '41800', 35, 35, NULL, '2020-08-01 19:27:05', '2020-08-01 19:27:05', NULL),
(17, 1, '41800', '20', '41780', 35, 35, NULL, '2020-08-01 19:27:13', '2020-08-01 19:27:13', NULL),
(18, 114, '440', '100', '340', 266, 266, NULL, '2020-10-01 16:54:00', '2020-10-01 16:54:00', NULL),
(19, 114, '642', '200', '442', 266, 266, NULL, '2020-10-02 12:40:49', '2020-10-02 12:40:49', NULL),
(20, 114, '695', '100', '595', 266, 266, NULL, '2020-10-15 12:36:11', '2020-10-15 12:36:11', NULL),
(21, 13, '100', '50', '50', 355, 355, NULL, '2020-10-19 18:38:37', '2020-10-19 18:38:37', NULL),
(22, 157, '30506', '6', '30500', 356, 356, NULL, '2020-10-23 19:46:19', '2020-10-23 19:46:19', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `Examination_notes_patient`
--

CREATE TABLE `Examination_notes_patient` (
  `id` int(10) UNSIGNED NOT NULL,
  `e_patient_id` int(11) NOT NULL,
  `e_doctor_id` int(11) NOT NULL,
  `e_book_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `examination_notes` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `Examination_notes_patient`
--

INSERT INTO `Examination_notes_patient` (`id`, `e_patient_id`, `e_doctor_id`, `e_book_id`, `examination_notes`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 19, 1, 'book-00001', 'test data gg', 35, 35, '2020-05-16 11:42:08', '2020-05-16 11:42:27', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `followup_period`
--

CREATE TABLE `followup_period` (
  `id` int(10) UNSIGNED NOT NULL,
  `dr_id` int(11) NOT NULL,
  `followup_list` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `followup_period`
--

INSERT INTO `followup_period` (`id`, `dr_id`, `followup_list`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 114, '1 week', 249, 249, '2020-10-05 18:56:32', '2020-10-05 18:56:32'),
(2, 114, '2 week', 249, 249, '2020-10-05 18:56:32', '2020-10-05 18:56:32'),
(3, 142, '1 Day', 307, 307, '2020-10-05 19:25:25', '2020-10-05 19:25:25'),
(4, 142, '3 Days', 307, 307, '2020-10-05 19:25:25', '2020-10-05 19:25:25'),
(5, 142, '4 Days', 307, 307, '2020-10-05 19:25:25', '2020-10-05 19:25:25'),
(6, 142, '1 Day', 307, 307, '2020-10-05 20:03:31', '2020-10-05 20:03:31'),
(7, 142, '3 Days', 307, 307, '2020-10-05 20:03:31', '2020-10-05 20:03:31'),
(8, 142, '4 Days', 307, 307, '2020-10-05 20:03:31', '2020-10-05 20:03:31'),
(9, 142, '9 Months', 307, 307, '2020-10-05 20:03:31', '2020-10-05 20:03:31'),
(10, 114, '1 week', 249, 249, '2020-10-06 19:18:54', '2020-10-06 19:18:54'),
(11, 114, '2 week', 249, 249, '2020-10-06 19:18:54', '2020-10-06 19:18:54');

-- --------------------------------------------------------

--
-- Table structure for table `H360_coupan_redeem`
--

CREATE TABLE `H360_coupan_redeem` (
  `id` int(10) UNSIGNED NOT NULL,
  `b_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `coupan_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '1',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `H360_coupan_redeem`
--

INSERT INTO `H360_coupan_redeem` (`id`, `b_id`, `user_id`, `coupan_code`, `price`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 11, 389, 'pharmacies100', '100', 1, 389, 389, '2020-11-13 21:56:50', '2020-11-13 21:56:50', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `haspatal_360_register`
--

CREATE TABLE `haspatal_360_register` (
  `id` int(10) UNSIGNED NOT NULL,
  `v_uni_id` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name_prefix` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `full_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mo_prefix` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `coupon_status` tinyint(4) NOT NULL DEFAULT '0',
  `your_coverage_status` tinyint(4) NOT NULL DEFAULT '0',
  `buisness_profile` tinyint(4) NOT NULL DEFAULT '0',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `haspatal_360_register`
--

INSERT INTO `haspatal_360_register` (`id`, `v_uni_id`, `name_prefix`, `full_name`, `email`, `mo_prefix`, `mobile_no`, `coupon_status`, `your_coverage_status`, `buisness_profile`, `created_by`, `updated_by`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(272, '91-V-000194', NULL, 'test456', '888888', '91', '8888', 0, 0, 0, NULL, NULL, 1, '2021-01-18 15:54:10', '2021-01-18 15:54:10', NULL),
(273, '91-V-000195', 'Pharmacy', 'absal', 'absalnikki@gmail.com', '+91', '9711479333', 0, 0, 0, NULL, NULL, 1, '2021-01-18 23:46:19', '2021-01-18 23:46:19', NULL),
(274, '91-V-000196', 'Pharmacy', 'ashik', 'ashik@gmail.com', '+91', '9633631592', 0, 0, 0, NULL, NULL, 1, '2021-01-18 23:53:51', '2021-01-18 23:53:51', NULL),
(275, '91-V-000197', 'Pharmacy', 'Ashik M', 'ashikmannorayil@gmail.com', '+91', '9746043286', 0, 0, 0, NULL, NULL, 1, '2021-01-20 19:16:30', '2021-01-20 19:16:30', NULL),
(279, '91-V-000200', 'Pharmacy', 'nxndndjnsnd', 'sandeepraj.rj5@gmail.com', '+91', '7972185820', 0, 0, 0, NULL, NULL, 1, '2021-01-20 22:07:39', '2021-01-20 22:07:39', NULL),
(280, '91-V-000201', 'Pharmacy', 'radhika', 'radhikagulati50@gmail.com', '+91', '7291033392', 0, 0, 0, NULL, NULL, 1, '2021-01-20 22:12:46', '2021-01-20 22:12:46', NULL),
(284, '91-V-000202', 'Pharmacy', 'ashik manoorayil', 'mastores01@gmail.com', '+91', '7994646416', 0, 0, 0, NULL, NULL, 1, '2021-01-21 05:30:32', '2021-01-21 05:30:32', NULL),
(285, '91-V-000203', NULL, 'haspatal', 'f@gmail.com', '91', '828282', 0, 0, 0, NULL, NULL, 1, '2021-01-21 16:39:05', '2021-01-21 16:39:05', NULL),
(286, '91-V-000204', NULL, 'test', 't', '91', 't', 0, 0, 0, NULL, NULL, 1, '2021-01-21 16:49:03', '2021-01-21 16:49:03', NULL),
(287, '91-V-000205', NULL, 'u', 'u', '91', 'u', 0, 0, 0, NULL, NULL, 1, '2021-01-21 16:50:12', '2021-01-21 16:50:12', NULL),
(288, '91-V-000206', NULL, 'althaf', 'ghhh@gmail.com', '91', '636363636355', 0, 0, 0, NULL, NULL, 1, '2021-01-21 17:16:25', '2021-01-21 17:16:25', NULL),
(289, '91-V-000207', NULL, 'y', 'y', '91', 'y', 0, 0, 0, NULL, NULL, 1, '2021-01-21 17:22:35', '2021-01-21 17:22:35', NULL),
(290, '91-V-000208', NULL, 'undefined', 'undefined', '91', 'undefined', 0, 0, 0, NULL, NULL, 1, '2021-01-21 17:25:27', '2021-01-21 17:25:27', NULL),
(291, '91-V-000209', NULL, 'bbbh', 'bdhshh@gmail.com', '91', '53663636363-', 0, 0, 0, NULL, NULL, 1, '2021-01-21 17:26:33', '2021-01-21 17:26:33', NULL),
(292, '91-V-000210', NULL, 'Ajay', 'ajay@gmail.com', '91', '8281478767', 0, 0, 0, NULL, NULL, 1, '2021-01-21 17:47:07', '2021-01-21 17:47:07', NULL),
(293, '91-V-000211', NULL, 'Ajay', 'aj@gmail.com', '91', '8281478767', 0, 0, 0, NULL, NULL, 1, '2021-01-21 17:48:37', '2021-01-21 17:48:37', NULL),
(294, '91-V-000212', NULL, 'h', 'h', '91', 'h', 0, 0, 0, NULL, NULL, 1, '2021-01-21 17:56:01', '2021-01-21 17:56:01', NULL),
(295, '91-V-000213', NULL, 'hh', 'hh', '91', 'hh', 0, 0, 0, NULL, NULL, 1, '2021-01-21 18:18:29', '2021-01-21 18:18:29', NULL),
(296, '91-V-000214', NULL, 'test', 't', '91', '88', 0, 0, 0, NULL, NULL, 1, '2021-01-21 18:58:26', '2021-01-21 18:58:26', NULL),
(297, '91-V-000215', NULL, 'Ajaysankar', 'ajays@gmail.com', '91', '8281478767', 0, 0, 0, NULL, NULL, 1, '2021-01-21 19:04:00', '2021-01-21 19:04:00', NULL),
(298, '91-V-000216', NULL, 'ajay', '789@gmail.com', '91', '789', 0, 0, 0, NULL, NULL, 1, '2021-01-21 19:12:29', '2021-01-21 19:12:29', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `haspatal_regsiter`
--

CREATE TABLE `haspatal_regsiter` (
  `id` int(10) UNSIGNED NOT NULL,
  `h_uni_id` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `state_id` int(11) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `pincode_id` int(11) DEFAULT NULL,
  `hospital_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bio` text COLLATE utf8mb4_unicode_ci,
  `address` text COLLATE utf8mb4_unicode_ci,
  `year_of_establishment` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gst_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `license_copy` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hospital_profile` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hospital_location` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telemedicine_coordinator` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pancard_number` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logo` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `wallpaper` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_person` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_person_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_person_phonoe_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `other_phone_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `support_watsapp_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `specialities` int(11) DEFAULT NULL,
  `services` int(11) DEFAULT NULL,
  `bank_name` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `account_name` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ifsc_code` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `account_no` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cheque_img` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `home_care_list`
--

CREATE TABLE `home_care_list` (
  `id` int(10) UNSIGNED NOT NULL,
  `dr_id` int(11) NOT NULL,
  `home_care_service_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `home_care_list`
--

INSERT INTO `home_care_list` (`id`, `dr_id`, `home_care_service_name`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(5, 114, 'demo', 249, 249, '2020-10-05 18:23:12', '2020-10-05 18:23:12', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `hospitalAddFund`
--

CREATE TABLE `hospitalAddFund` (
  `id` int(10) UNSIGNED NOT NULL,
  `hospital_id` int(11) NOT NULL,
  `payment_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `h_amount` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `hospitalAddFund`
--

INSERT INTO `hospitalAddFund` (`id`, `hospital_id`, `payment_id`, `h_amount`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 13, '', '100', 355, 355, '2020-10-19 14:45:59', '2020-10-19 14:45:59', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `hospitalWithdraw`
--

CREATE TABLE `hospitalWithdraw` (
  `id` int(10) UNSIGNED NOT NULL,
  `hospital_id` int(11) NOT NULL,
  `current_balance` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `requested_amount` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `available_balance` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `hospitalWithdraw`
--

INSERT INTO `hospitalWithdraw` (`id`, `hospital_id`, `current_balance`, `requested_amount`, `available_balance`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 13, '100', '50', '50', 355, 355, '2020-10-19 18:42:04', '2020-10-19 18:42:04', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `hospital_register`
--

CREATE TABLE `hospital_register` (
  `id` int(10) UNSIGNED NOT NULL,
  `hospital_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` int(11) DEFAULT NULL,
  `state` int(11) DEFAULT NULL,
  `city` int(11) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_person_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_details` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_time` date DEFAULT NULL,
  `logo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `services` int(11) DEFAULT NULL,
  `specialities` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `hospital_register`
--

INSERT INTO `hospital_register` (`id`, `hospital_name`, `email`, `country`, `state`, `city`, `role_id`, `address`, `contact_person_name`, `contact_details`, `date_time`, `logo`, `status`, `services`, `specialities`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Shree Ram', 'ram@aelius.in', 3, 1, 2, 15, 'Rajkotasd', 'Ram', '7412589630', '2020-05-08', '1.png', 1, 2, 2, 1, 1, '2020-05-08 12:00:30', '2020-07-27 18:45:29', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `h_plan`
--

CREATE TABLE `h_plan` (
  `id` int(10) UNSIGNED NOT NULL,
  `plan_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `limit` int(11) DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `h_plan`
--

INSERT INTO `h_plan` (`id`, `plan_name`, `price`, `limit`, `description`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Free Plan', '0', 2, 'Free Plan', NULL, 1, 1, '2020-05-28 10:35:12', '2020-06-11 12:45:56', NULL),
(2, 'Premium', '200', 5, 'Premium', NULL, 1, 1, '2020-06-08 09:27:32', '2020-06-11 12:45:39', NULL),
(3, 'free', '456', 12, 'knjkhfjdncjxbcidjokdjnkxnjbvjhfgubggdfgjdfiudg98vidfhdyfvn', NULL, 1, 1, '2020-06-22 07:44:43', '2020-06-22 07:44:43', NULL),
(4, 'frfrgf', '6585', 89, 'hgfd', NULL, 1, 1, '2020-06-22 09:19:14', '2020-08-31 19:15:26', '2020-08-31 19:15:26'),
(5, 'pk', '23', 7, 'ghbthnhg', NULL, 1, 1, '2020-07-28 13:39:17', '2020-07-28 13:39:29', '2020-07-28 13:39:29');

-- --------------------------------------------------------

--
-- Table structure for table `imaging_list`
--

CREATE TABLE `imaging_list` (
  `id` int(10) UNSIGNED NOT NULL,
  `dr_id` int(11) NOT NULL,
  `imaging_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `imaging_list`
--

INSERT INTO `imaging_list` (`id`, `dr_id`, `imaging_name`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 114, 'dummy1233', 249, 249, '2020-10-03 17:42:45', '2020-10-03 17:45:02', '2020-10-03 17:45:02'),
(2, 0, 'test img', 307, 307, '2020-10-03 19:12:03', '2020-10-03 19:12:03', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `key_points_patient`
--

CREATE TABLE `key_points_patient` (
  `id` int(10) UNSIGNED NOT NULL,
  `patient_id` int(11) NOT NULL,
  `doctor_id` int(11) NOT NULL,
  `k_book_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `key_point` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `key_points_patient`
--

INSERT INTO `key_points_patient` (`id`, `patient_id`, `doctor_id`, `k_book_id`, `key_point`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 19, 1, 'book-000001', 'test data 11', 35, 35, '2020-05-16 05:08:51', '2020-05-16 05:09:08', NULL),
(2, 19, 1, 'book-0000010', 'treatment1', 35, 35, '2020-05-16 05:36:49', '2020-05-16 05:37:03', NULL),
(3, 19, 1, 'book-0000011', 'treatment', 35, 35, '2020-05-16 12:40:35', '2020-05-16 12:40:35', NULL),
(4, 221, 136, '91-B-000000116', 'testt', 300, 300, '2020-10-20 12:36:09', '2020-10-20 12:36:09', NULL),
(5, 254, 160, '91-B-000000163', 'Take Paracetamol', 383, 383, '2020-11-08 00:52:53', '2020-11-08 00:52:53', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `lab_test`
--

CREATE TABLE `lab_test` (
  `id` int(10) UNSIGNED NOT NULL,
  `dr_id` int(11) NOT NULL,
  `lab_test_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `lab_test`
--

INSERT INTO `lab_test` (`id`, `dr_id`, `lab_test_name`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 114, 'demo123', 0, 0, '2020-10-03 17:37:16', '2020-10-03 17:40:24', '2020-10-03 17:40:24'),
(2, 0, 'test lab', 307, 307, '2020-10-03 19:02:41', '2020-10-03 19:02:41', NULL),
(3, 0, 'lab2', 307, 307, '2020-10-03 19:25:19', '2020-10-03 19:51:54', '2020-10-03 19:51:54'),
(4, 0, 'labbb data', 307, 307, '2020-10-03 19:25:46', '2020-10-05 12:16:10', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `language`
--

CREATE TABLE `language` (
  `id` int(10) UNSIGNED NOT NULL,
  `language` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '1' COMMENT '0 for inactive,1 for active',
  `created_by` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `updated_by` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `language`
--

INSERT INTO `language` (`id`, `language`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'Gujarati', 1, '1', '1', '2020-04-07 02:33:30', '2020-04-13 07:05:41'),
(3, 'Hindi', 1, '1', '1', '2020-04-10 05:32:47', '2020-04-10 05:32:47'),
(4, 'English', 1, '1', '1', '2020-04-13 07:05:55', '2020-04-13 07:05:55'),
(5, 'Malayalam', 1, '1', '1', '2020-04-13 07:06:37', '2020-04-13 07:06:37'),
(6, 'Bangla', 1, '1', '1', '2020-08-31 11:52:16', '2020-08-31 11:52:16'),
(7, 'Kashmiri', 1, '1', '1', '2020-08-31 11:52:54', '2020-08-31 11:52:54'),
(8, 'Konkani', 1, '1', '1', '2020-08-31 11:53:13', '2020-08-31 11:53:13'),
(9, 'Marathi', 1, '1', '1', '2020-08-31 11:53:46', '2020-08-31 11:53:46'),
(10, 'Punjabi', 1, '1', '1', '2020-08-31 11:54:11', '2020-08-31 11:54:11'),
(11, 'Sanskrit', 1, '1', '1', '2020-08-31 11:54:32', '2020-08-31 11:54:32'),
(12, 'Santali', 1, '1', '1', '2020-08-31 11:54:59', '2020-08-31 11:54:59'),
(13, 'Odia', 1, '1', '1', '2020-08-31 11:55:15', '2020-08-31 11:55:15'),
(14, 'Sindhi', 1, '1', '1', '2020-08-31 11:55:33', '2020-08-31 11:55:33'),
(15, 'Tamil', 1, '1', '1', '2020-08-31 11:55:50', '2020-08-31 11:55:50'),
(16, 'Telugu', 1, '1', '1', '2020-08-31 11:56:22', '2020-08-31 11:56:22'),
(17, 'Urdu', 1, '1', '1', '2020-08-31 11:56:51', '2020-08-31 11:56:51');

-- --------------------------------------------------------

--
-- Table structure for table `licence_type`
--

CREATE TABLE `licence_type` (
  `id` int(10) UNSIGNED NOT NULL,
  `licence_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '1' COMMENT '0 for inactive 1 for active',
  `created_by` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `updated_by` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `licence_type`
--

INSERT INTO `licence_type` (`id`, `licence_name`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(3, 'MCI Registration', 1, '1', '1', '2020-09-10 20:07:16', '2020-09-10 20:07:16'),
(4, 'State Medical Council Registration', 1, '1', '1', '2020-09-10 20:08:24', '2020-09-10 20:08:24'),
(5, 'Ayush Registration', 1, '1', '1', '2020-09-10 20:09:13', '2020-09-10 20:09:13');

-- --------------------------------------------------------

--
-- Table structure for table `medicine_list`
--

CREATE TABLE `medicine_list` (
  `id` int(10) UNSIGNED NOT NULL,
  `dr_id` int(11) NOT NULL,
  `generic_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `medicine_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `drug_from` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `drug_strength` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `medicine_list`
--

INSERT INTO `medicine_list` (`id`, `dr_id`, `generic_name`, `medicine_name`, `drug_from`, `drug_strength`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 114, 'Paracetamol_dem', 'Crocin', 'Tablets', '250 mg', 249, 249, '2020-10-03 12:37:08', '2020-10-03 12:51:10', NULL),
(2, 142, 'aspirin1', 'aspirin', 'test', '3g/L', 307, 307, '2020-10-03 15:55:10', '2020-10-05 12:15:54', NULL),
(4, 114, 'Paracetamol23', 'Crocin', 'Tablets', '250 mg', 249, 249, '2020-10-03 17:13:35', '2020-10-03 17:13:35', NULL),
(5, 142, 'aspirin', 'aspirin', 'test', '25mg', 307, 307, '2020-10-03 17:16:05', '2020-10-03 20:00:47', '2020-10-03 20:00:47');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_03_07_055231_create_permission_role_table', 1),
(4, '2018_03_07_055231_create_permissions_table', 1),
(5, '2018_03_07_055231_create_role_user_table', 1),
(6, '2018_03_07_055231_create_roles_table', 1),
(7, '2018_03_07_055232_add_foreign_keys_to_permission_role_table', 1),
(8, '2020_03_06_101420_create_country_master_table', 2),
(9, '2020_03_07_091705_create_state_master_table', 3),
(10, '2020_03_12_043701_create_city_master_table', 4),
(11, '2020_03_12_044242_create_brand_master_table', 5),
(12, '2020_03_12_045656_create_car_type_table', 6),
(13, '2020_03_12_051554_create_car_master_table', 7),
(14, '2020_03_12_051829_create_airport_master_table', 8),
(15, '2020_03_12_052157_create_terminal_master_table', 9),
(16, '2020_03_12_054104_create_add_airport_table', 10),
(17, '2020_03_12_054435_create_service_master_table', 11),
(18, '2020_03_12_055131_create_reservation_master_table', 12),
(19, '2020_03_12_060204_create_annual_membership_table', 13),
(20, '2020_03_12_060516_create_color_master_table', 14),
(21, '2020_03_12_075000_create_color_master_table', 15),
(22, '2020_03_12_091646_create_customer_table', 16),
(23, '2020_03_12_095637_create_employe_table', 17),
(24, '2016_06_01_000001_create_oauth_auth_codes_table', 18),
(25, '2016_06_01_000002_create_oauth_access_tokens_table', 18),
(26, '2016_06_01_000003_create_oauth_refresh_tokens_table', 18),
(27, '2016_06_01_000004_create_oauth_clients_table', 18),
(28, '2016_06_01_000005_create_oauth_personal_access_clients_table', 18),
(29, '2020_03_18_104125_create_plan_master_table', 19),
(30, '2020_03_18_110045_create_video_comment_table', 20),
(31, '2020_03_18_110434_create_video_like_dislike_table', 21),
(32, '2020_03_18_110854_create_follow_users_table', 22),
(33, '2020_03_19_101519_create_sound_master_table', 23),
(34, '2020_03_19_114000_create_report_controllers_table', 24),
(35, '2020_03_19_122703_create_reports_table', 24),
(36, '2020_03_27_083016_create_property_type_table', 24),
(37, '2020_03_27_091329_create_country_table', 25),
(38, '2020_03_27_122506_create_state_table', 26),
(39, '2020_03_28_083849_create_city_table', 27),
(40, '2020_03_28_092149_create_property_details_table', 28),
(41, '2020_03_28_102044_create_tax_setup_table', 29),
(42, '2020_03_28_121826_create_member_details_table', 30),
(45, '2020_04_07_054333_create_language_table', 31),
(46, '2020_04_07_054608_create_specilities_table', 32),
(47, '2020_04_07_100757_create_doctor_details_table', 33),
(48, '2020_04_07_101804_create_patient_details_table', 34),
(49, '2020_04_08_093933_create_service_details_table', 35),
(50, '2020_04_08_094146_create_set_service_price_table', 36),
(51, '2020_04_08_094718_create_bank_information_table', 37),
(52, '2020_04_09_045303_create_licence_type_table', 38),
(53, '2020_04_09_063437_create_booking_request_table', 39),
(54, '2020_04_09_095413_create_my_favourite_table', 40),
(55, '2020_04_09_104813_create_doctor_availability_table', 41),
(56, '2020_04_09_110653_create_prescription_details_table', 42),
(57, '2020_04_10_101441_create_patient_add_fund_table', 43),
(58, '2020_04_10_101910_create_patient_refund_request_table', 44),
(59, '2020_04_13_082834_create_time_slot_table', 45),
(60, '2020_04_17_111114_create_coupan_table', 46),
(61, '2020_04_18_075653_create_key_points_patient_table', 47),
(62, '2020_04_18_080752_create_present_medications_patient_table', 48),
(63, '2020_04_18_081518_create_examination_notes_patient_table', 49),
(64, '2020_04_18_081733_create_available_lab_patient_table', 50),
(65, '2020_04_18_081958_create_perceived_patient_table', 51),
(66, '2020_04_18_082218_create_suggested_investigations_patient_table', 52),
(67, '2020_04_18_082441_create_suggested_imagings_patient_table', 53),
(68, '2020_04_18_082635_create_suggested_specialists_patient_table', 54),
(69, '2020_04_18_083335_create_suggested_therapies_patient_table', 55),
(70, '2020_04_18_084052_create_present_complaint_patient_table', 56),
(71, '2020_04_18_084958_create_my_note_patient_table', 57),
(72, '2020_04_28_071746_create_plan_master_table', 58),
(73, '2020_05_06_043754_create_demo_table', 59),
(74, '2020_05_06_044038_create_doctor_withdraw_table', 60),
(75, '2020_05_07_051807_create_support_table', 61),
(76, '2020_05_08_103226_create_hospital_register_table', 62),
(77, '2020_05_15_044217_create_review_table', 63),
(78, '2020_05_16_114857_create_consultant_table', 64),
(79, '2020_05_28_060942_create_business_type_table', 65),
(80, '2020_05_28_094205_create_h_plan_table', 66),
(81, '2020_05_28_095545_create_business register_table', 67),
(82, '2020_05_28_112838_create_offer_table', 68),
(83, '2020_06_01_062515_create_haspatal_360_register_table', 69),
(84, '2020_06_02_112138_create_booking_price_admin_table', 70),
(85, '2020_06_04_054801_create_your_coverage_360_table', 71),
(86, '2020_06_04_060418_create_create_offer_360_table', 72),
(87, '2020_06_09_103520_create_wallet_360_table', 73),
(88, '2020_06_10_050008_create_wallet_360_transaction_table', 74),
(89, '2020_06_11_062456_create_buy_plan_table', 75),
(90, '2020_06_12_051532_create_order_360_table', 76),
(91, '2020_06_13_055325_create_order_query_table', 77),
(92, '2020_07_13_102206_create_haspatal_regsiter_table', 78),
(93, '2020_08_04_112219_create_review_360s_table', 79),
(94, '2020_09_22_053857_create_tax_table', 80),
(95, '2020_09_28_105147_create_buy_plan_dr_table', 81),
(96, '2020_10_03_051645_create_medicine_list_table', 82),
(97, '2020_10_03_102159_create_lab_test_table', 83),
(98, '2020_10_03_102326_create_imaging_list_table', 84),
(99, '2020_10_03_102442_create_therapies_list_table', 85),
(100, '2020_10_03_102547_create_counselings_list_table', 86),
(101, '2020_10_03_102649_create_home_care_list_table', 87),
(102, '2020_10_05_105551_create_referral_specialities_table', 88),
(103, '2020_10_05_110400_create_followup_period_table', 89),
(104, '2020_10_19_072241_create_hospitaladdfund_table', 90),
(105, '2020_10_19_075411_create_hospitalwithdraw_table', 91),
(106, '2020_11_03_170837_create_task_table', 92),
(107, '2020_11_13_114630_create_h360_coupan_table', 93),
(108, '2016_01_04_173148_create_admin_tables', 94);

-- --------------------------------------------------------

--
-- Table structure for table `my_favourite`
--

CREATE TABLE `my_favourite` (
  `id` int(10) UNSIGNED NOT NULL,
  `patient_id` int(11) NOT NULL,
  `doctor_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `my_favourite`
--

INSERT INTO `my_favourite` (`id`, `patient_id`, `doctor_id`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 6, 1, 1, 1, '2020-04-10 11:19:05', '2020-04-10 11:59:30', '2020-04-10 11:59:30'),
(2, 1, 1, 1, 1, 1, '2020-04-10 11:19:41', '2020-04-11 04:47:43', '2020-04-11 04:47:43'),
(3, 1, 6, 1, 1, 1, '2020-04-10 12:34:20', '2020-04-11 04:45:04', '2020-04-11 04:45:04'),
(4, 1, 1, 1, 1, 1, '2020-04-11 04:49:02', '2020-04-11 04:49:09', '2020-04-11 04:49:09'),
(5, 1, 1, 1, 1, 1, '2020-04-11 04:50:29', '2020-04-11 04:50:42', '2020-04-11 04:50:42'),
(6, 1, 1, 1, 1, 1, '2020-04-11 04:51:35', '2020-04-11 04:51:44', '2020-04-11 04:51:44'),
(7, 1, 1, 1, 1, 1, '2020-04-11 04:52:45', '2020-04-11 04:54:13', '2020-04-11 04:54:13'),
(8, 1, 6, 1, 1, 1, '2020-04-11 04:52:47', '2020-04-11 04:52:58', '2020-04-11 04:52:58'),
(9, 1, 1, 1, 1, 1, '2020-04-11 04:59:07', '2020-04-11 04:59:18', '2020-04-11 04:59:18'),
(10, 1, 6, 1, 1, 1, '2020-04-11 04:59:11', '2020-04-11 05:00:10', '2020-04-11 05:00:10'),
(11, 1, 1, 1, 1, 1, '2020-04-11 05:00:28', '2020-04-11 05:00:37', '2020-04-11 05:00:37'),
(12, 1, 6, 1, 1, 1, '2020-04-11 05:00:30', '2020-04-11 05:02:10', '2020-04-11 05:02:10'),
(13, 1, 1, 1, 1, 1, '2020-04-11 08:13:54', '2020-04-11 16:23:31', '2020-04-11 16:23:31'),
(14, 1, 6, 1, 1, 1, '2020-04-11 13:57:59', '2020-04-11 13:58:14', '2020-04-11 13:58:14'),
(15, 1, 6, 1, 1, 1, '2020-04-11 14:56:53', '2020-04-11 16:23:34', '2020-04-11 16:23:34'),
(16, 1, 6, 1, 1, 1, '2020-04-11 16:59:48', '2020-04-13 08:32:55', '2020-04-13 08:32:55'),
(17, 1, 1, 1, 1, 1, '2020-04-12 04:54:36', '2020-04-13 08:34:28', '2020-04-13 08:34:28'),
(18, 1, 1, 1, 1, 1, '2020-04-13 08:36:15', '2020-04-13 08:38:40', '2020-04-13 08:38:40'),
(19, 1, 6, 1, 1, 1, '2020-04-13 08:36:18', '2020-04-13 08:36:27', '2020-04-13 08:36:27'),
(20, 1, 6, 1, 1, 1, '2020-04-13 08:39:37', '2020-04-21 07:02:24', '2020-04-21 07:02:24'),
(21, 1, 1, 1, 1, 1, '2020-04-13 08:39:39', '2020-04-13 08:39:53', '2020-04-13 08:39:53'),
(22, 1, 1, 1, 1, 1, '2020-04-13 10:29:43', '2020-04-14 06:26:00', '2020-04-14 06:26:00'),
(23, 1, 1, 1, 1, 1, '2020-04-18 10:24:24', '2020-04-18 10:24:24', NULL),
(24, 20, 1, 1, 20, 20, '2020-04-22 05:17:20', '2020-04-22 05:17:20', NULL),
(25, 23, 1, 1, 23, 23, '2020-04-22 10:42:19', '2020-04-22 10:42:19', NULL),
(26, 19, 1, 1, 19, 19, '2020-04-23 07:52:28', '2020-06-08 09:56:52', '2020-06-08 09:56:52'),
(27, 19, 6, 1, 19, 19, '2020-04-24 10:29:36', '2020-05-05 07:08:26', '2020-05-05 07:08:26'),
(28, 23, 12, 1, 23, 23, '2020-04-24 14:24:49', '2020-04-24 14:24:49', NULL),
(29, 23, 11, 1, 23, 23, '2020-04-25 15:00:15', '2020-04-25 15:00:15', NULL),
(30, 19, 6, 1, 19, 19, '2020-05-05 07:08:40', '2020-07-05 19:14:10', '2020-07-05 19:14:10'),
(31, 26, 12, 1, 26, 26, '2020-05-27 10:22:53', '2020-05-27 10:22:53', NULL),
(32, 19, 25, 1, 19, 19, '2020-06-25 15:46:19', '2020-06-25 15:46:19', NULL),
(33, 19, 37, 1, 19, 19, '2020-07-03 18:39:55', '2020-07-03 18:39:55', NULL),
(34, 19, 37, 1, 19, 19, '2020-07-05 19:11:16', '2020-07-05 19:11:16', NULL),
(35, 19, 37, 1, 19, 19, '2020-07-05 19:11:31', '2020-07-05 19:11:31', NULL),
(36, 19, 1, 1, 19, 19, '2020-07-31 16:57:10', '2020-07-31 16:57:10', NULL),
(42, 19, 11, 1, 1, 1, '2020-08-01 16:37:41', '2020-08-01 16:37:41', NULL),
(43, 136, 1, 1, 136, 136, '2020-08-18 18:36:00', '2020-08-18 18:36:00', NULL),
(44, 197, 92, 1, 197, 197, '2020-08-27 12:59:48', '2020-08-27 12:59:48', NULL),
(45, 196, 94, 1, 196, 196, '2020-08-29 22:28:27', '2020-08-29 22:28:27', NULL),
(46, 201, 95, 1, 201, 201, '2020-09-01 20:17:47', '2020-09-01 20:27:05', '2020-09-01 20:27:05'),
(47, 201, 93, 1, 201, 201, '2020-09-01 20:26:24', '2020-09-03 21:35:30', '2020-09-03 21:35:30'),
(48, 208, 93, 1, 208, 208, '2020-09-09 03:58:59', '2020-09-09 03:58:59', NULL),
(49, 209, 135, 1, 209, 209, '2020-09-11 11:00:21', '2020-09-11 11:00:21', NULL),
(50, 200, 134, 1, 200, 200, '2020-09-12 18:52:17', '2020-09-12 18:52:17', NULL),
(51, 200, 136, 1, 200, 200, '2020-09-12 18:52:53', '2020-09-12 18:52:53', NULL),
(52, 200, 128, 1, 200, 200, '2020-09-12 18:53:10', '2020-09-12 18:53:10', NULL),
(53, 196, 91, 1, 196, 196, '2020-09-21 16:04:30', '2020-09-21 16:04:30', NULL),
(54, 196, 142, 1, 196, 196, '2020-09-26 12:31:37', '2020-09-26 12:31:37', NULL),
(55, 196, 144, 1, 196, 196, '2020-09-26 12:32:17', '2020-09-26 12:32:17', NULL),
(56, 233, 144, 1, 233, 233, '2020-10-02 14:25:31', '2020-10-02 14:25:31', NULL),
(57, 191, 114, 1, 191, 191, '2020-10-02 18:35:45', '2020-10-02 18:35:45', NULL),
(58, 196, 159, 1, 196, 196, '2020-10-15 22:33:52', '2020-10-15 22:33:52', NULL),
(59, 221, 136, 1, 221, 221, '2020-10-18 11:07:01', '2020-10-18 11:07:01', NULL),
(60, 221, 91, 1, 221, 221, '2020-10-24 12:38:14', '2020-10-24 12:38:14', NULL),
(61, 201, 144, 1, 201, 201, '2020-10-24 20:03:53', '2020-10-24 20:03:53', NULL),
(62, 191, 91, 1, 191, 191, '2020-10-26 18:50:53', '2020-10-26 18:50:53', NULL),
(63, 216, 144, 1, 216, 216, '2020-10-28 18:45:00', '2020-10-28 18:45:00', NULL),
(64, 254, 145, 1, 254, 254, '2020-10-30 01:45:24', '2020-10-30 01:45:24', NULL),
(65, 221, 144, 1, 221, 221, '2020-11-04 03:23:47', '2020-11-04 03:23:47', NULL),
(66, 261, 144, 1, 261, 261, '2020-11-24 02:48:34', '2020-11-24 02:48:34', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `my_note_patient`
--

CREATE TABLE `my_note_patient` (
  `id` int(10) UNSIGNED NOT NULL,
  `my_patient_id` int(11) NOT NULL,
  `my_doctor_id` int(11) NOT NULL,
  `m_book_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `my_note` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `s_pdf` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `my_note_patient`
--

INSERT INTO `my_note_patient` (`id`, `my_patient_id`, `my_doctor_id`, `m_book_id`, `my_note`, `s_pdf`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 19, 1, 'book-00001', 'test datacvvbbc', 'public/summary/1589546108__summary.pdf', 35, 35, '2020-05-15 08:29:13', '2020-05-15 12:35:09', NULL),
(2, 19, 1, 'book-000006', 'test note', 'public/summary/1589546228__summary.pdf', 35, 35, '2020-05-15 12:37:08', '2020-05-15 12:37:08', NULL),
(3, 19, 1, 'book-0000011', 'notes', 'public/summary/1589632814__summary.pdf', 35, 35, '2020-05-16 12:40:15', '2020-05-16 12:40:15', NULL),
(4, 221, 136, '91-B-000000114', 'patient', 'public/summary/1603170645__summary.pdf', 300, 300, '2020-10-20 12:10:46', '2020-10-20 12:10:46', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('0032c2dfb34770df76a53f91367bb24b0db61f8a07ec6b4b998e47ec425c2dea2cfcca7522248055', 309, 1, 'LaraPassport', '[]', 0, '2020-10-07 18:32:23', '2020-10-07 18:32:23', '2021-10-07 11:32:23'),
('005fad617b8e9807491111cd3b8a06807cfee39e4bde2e7952f35e297112d0d33d230d2b731eb5bc', 210, 1, 'LaraPassport', '[]', 0, '2020-08-18 17:04:41', '2020-08-18 17:04:41', '2021-08-18 10:04:41'),
('009160ad45551eeb98f59f0d7eae88954c8967ddcab41636f5c0d8ebaa46b40b6c1c83bd79ed7c3d', 33, 1, 'LaraPassport', '[]', 0, '2020-04-13 05:21:40', '2020-04-13 05:21:40', '2021-04-13 05:21:40'),
('012ca78434cfd41b7c1468178cc3b00a231e44cba563931d2d709f7657d1a99cfe24c1a95d9ad94b', 316, 1, 'LaraPassport', '[]', 0, '2021-01-05 21:15:23', '2021-01-05 21:15:23', '2022-01-05 14:15:23'),
('013ac5a2991fd75f8435283580a7f08200ef873196e89fb93e79729ec48ef4baa1d2cd8c3d9d72a6', 62, 1, 'LaraPassport', '[]', 0, '2020-05-28 06:19:57', '2020-05-28 06:19:57', '2021-05-28 06:19:57'),
('01541749f5918228860aa4c170cb6a04c4702de88a2b86e77f4e92785145e3579738c029f473c1a3', 201, 1, 'LaraPassport', '[]', 0, '2020-08-18 01:58:03', '2020-08-18 01:58:03', '2021-08-17 18:58:03'),
('015c02d152ec40569830b96a073304a74361ef1610d5908c7137d47838508e81bdfb155ca90cb166', 72, 1, 'LaraPassport', '[]', 0, '2020-05-28 10:34:37', '2020-05-28 10:34:37', '2021-05-28 10:34:37'),
('0161f4d43052be340dc041608e06614ad9a8a7904a286ce0c8399303c9ffc6901840ba3ee3752445', 33, 1, 'LaraPassport', '[]', 0, '2020-04-13 06:03:42', '2020-04-13 06:03:42', '2021-04-13 06:03:42'),
('0172b26328b098cd29de373c82e5c364a8c636aeb1bc211b0704bcd151fdd4a4aeeff69107c8eb98', 33, 1, 'LaraPassport', '[]', 0, '2020-04-11 08:08:20', '2020-04-11 08:08:20', '2021-04-11 08:08:20'),
('019aea655540735fafde149c4201bfea7866472b206e258d9a1b5a27061b6396d8c9649b90ba3dbd', 423, 1, 'LaraPassport', '[]', 0, '2020-12-01 17:20:07', '2020-12-01 17:20:07', '2021-12-01 10:20:07'),
('01d075dc15dbcb5b9e08b146432abab223a0973ba99073fad65bafa3b9676f4b41d75a2ee110957b', 376, 1, 'LaraPassport', '[]', 0, '2020-12-08 20:38:58', '2020-12-08 20:38:58', '2021-12-08 13:38:58'),
('01d2796479b58897aafe2aebb6695795068d76bf358e974326661b7a0f7cb0968bb7d8e0f8cf45f6', 57, 1, 'LaraPassport', '[]', 0, '2020-08-20 13:23:26', '2020-08-20 13:23:26', '2021-08-20 06:23:26'),
('02671351f9b1546a7c104094cf395c271917e41c69661f7254ad0feb3a0d19a8daceed1a9fc82fb8', 248, 1, 'LaraPassport', '[]', 0, '2020-08-29 21:08:00', '2020-08-29 21:08:00', '2021-08-29 14:08:00'),
('026a346af7e81afba5928ffb8ea2148e94c2d9fb423fb12d889c91d29a300adc349363c31716da69', 35, 1, 'LaraPassport', '[]', 0, '2020-07-25 18:10:54', '2020-07-25 18:10:54', '2021-07-25 11:10:54'),
('02ec4f35254cab04daea978c482ffa8b08ec96bcf09e6f0b0900cf07d87c11869a8076cc52363d83', 57, 1, 'LaraPassport', '[]', 0, '2020-07-30 18:15:07', '2020-07-30 18:15:07', '2021-07-30 11:15:07'),
('030237d534949589c15578e8587f7d3f2841c454e254dfc1e949f29f59226b1181b7ec89f54ac06b', 3, 1, 'LaraPassport', '[]', 0, '2020-03-18 23:11:41', '2020-03-18 23:11:41', '2021-03-19 04:41:41'),
('03090d12b3f18ee815faed38cb17d452e6feeb366f6ccd1120441a734f6265d40c061d8d47d1f576', 35, 1, 'LaraPassport', '[]', 0, '2020-05-12 07:42:04', '2020-05-12 07:42:04', '2021-05-12 07:42:04'),
('033f4f6d68ad7bbe2c09edd08687b9f5a2e42244d69cc331ac2a8e67a62604885fbd836f097b855b', 35, 1, 'LaraPassport', '[]', 0, '2020-05-18 10:28:36', '2020-05-18 10:28:36', '2021-05-18 10:28:36'),
('037710c134b6a8989621cfdf71585f87ed33d03f7f8f87b7272e508d81269b33c6dd85be705a36af', 5, 1, 'LaraPassport', '[]', 0, '2021-01-12 21:18:59', '2021-01-12 21:18:59', '2022-01-12 14:18:59'),
('039bff6a18b9ad4793ce7dfd2575967d9fb9a10a4e6fc5c732006590ef07aeed7841799068c35dbb', 304, 1, 'LaraPassport', '[]', 0, '2020-09-17 00:08:28', '2020-09-17 00:08:28', '2021-09-16 17:08:28'),
('03f126d4076ae518300dfe7c735708290974f643a9e9dc53933bd7336c78781f0d7fa3934c49c411', 379, 1, 'LaraPassport', '[]', 0, '2020-11-03 02:02:50', '2020-11-03 02:02:50', '2021-11-02 19:02:50'),
('043139663f656d0c9974c1e84aebbe611d72d9c1e2230b098baff8c9632dc71282f6e41044002750', 309, 1, 'LaraPassport', '[]', 0, '2020-10-04 17:28:58', '2020-10-04 17:28:58', '2021-10-04 10:28:58'),
('04379511704a290abb64d2927557cced3828fa394a7d83604145d5fb3e007ec078d7ef09c8e95629', 5, 1, 'LaraPassport', '[]', 0, '2021-01-13 00:17:44', '2021-01-13 00:17:44', '2022-01-12 17:17:44'),
('04983ccb11ec40243955c28ad8a656a20df11c3a0528fd448ef331414ebc0f13c0cea8ed0745d859', 57, 1, 'LaraPassport', '[]', 0, '2020-04-30 04:59:00', '2020-04-30 04:59:00', '2021-04-30 04:59:00'),
('04dc45adc9828083de5cd5e7a8d4295ce1b77893c817943ff596405f8741e1631e6f84d65ac4385f', 115, 1, 'LaraPassport', '[]', 0, '2020-06-22 09:23:39', '2020-06-22 09:23:39', '2021-06-22 09:23:39'),
('053a628030d24532618a221f560a6746d797091321235b6fa13b82ec3de582812b98ad82eb0dc9ff', 310, 1, 'LaraPassport', '[]', 0, '2020-09-24 17:55:35', '2020-09-24 17:55:35', '2021-09-24 10:55:35'),
('055ef19a8098fc80ff24393e577d3b63d2cd6f47625491f4438bc30d74a9fa52c1827367548e7eae', 119, 1, 'LaraPassport', '[]', 0, '2021-01-19 20:05:43', '2021-01-19 20:05:43', '2022-01-19 13:05:43'),
('0577111054c06e4f08ac978cbc0ce495d967050323a202f0a1953762d2a948db1f558917fecb560c', 424, 1, 'LaraPassport', '[]', 0, '2020-12-02 01:12:57', '2020-12-02 01:12:57', '2021-12-01 18:12:57'),
('0578bd18add3b50b515ac545e3793ebe758ae550445ee4dcfaf114d5128e1ee7e59bb4b5ea7205a8', 33, 1, 'LaraPassport', '[]', 0, '2020-04-10 07:45:43', '2020-04-10 07:45:43', '2021-04-10 07:45:43'),
('0595576a41e9c0bfbd6610a6e8199d7c1a4e580a472a3e0a5e508bdbf164cb39fc3fa55bed56509f', 311, 1, 'LaraPassport', '[]', 0, '2020-09-19 23:59:09', '2020-09-19 23:59:09', '2021-09-19 16:59:09'),
('059fbcb9b1d609289b615dbad38487e80579c1b8b0e4324a2406d63115e905a21668d328002747d3', 238, 1, 'LaraPassport', '[]', 0, '2020-09-17 14:40:52', '2020-09-17 14:40:52', '2021-09-17 07:40:52'),
('05b91aa27992193122a9aa49adc470b57cc746a4b1352ac769c8fecee6e8fc9bf5290752e9734c5c', 428, 1, 'LaraPassport', '[]', 0, '2020-12-14 21:28:03', '2020-12-14 21:28:03', '2021-12-14 14:28:03'),
('05c233474e8192d520c0dde18d8407686b838ab3693c28a71adf9024bc396ad1b12fb567750cfacc', 310, 1, 'LaraPassport', '[]', 0, '2020-09-18 23:31:03', '2020-09-18 23:31:03', '2021-09-18 16:31:03'),
('0602e8ca31dff648f2d4c939deb1da5dc9d7552118847352250c9b74fe2580fe90ff405d1057d243', 35, 1, 'LaraPassport', '[]', 0, '2020-04-11 05:27:29', '2020-04-11 05:27:29', '2021-04-11 05:27:29'),
('0613e0bad7ee32b482d7862467cb2eb80fd009806ebe44aadae246cb0582d9b527573856f13ab6b7', 35, 1, 'LaraPassport', '[]', 0, '2020-07-31 16:17:10', '2020-07-31 16:17:10', '2021-07-31 09:17:10'),
('061f5afbbea6aa9e76465f2e04eabeed0c3ed4cfbf90bce68c92a1fc79c556c65f3ae0a0b0ba801d', 266, 1, 'LaraPassport', '[]', 0, '2020-10-21 13:25:18', '2020-10-21 13:25:18', '2021-10-21 06:25:18'),
('069a387e24da00dafa0dcffa88bdc1e4ad7b5661ea1a8c761835353f95d85601aabe8d4649997e18', 316, 1, 'LaraPassport', '[]', 0, '2020-12-19 20:02:12', '2020-12-19 20:02:12', '2021-12-19 13:02:12'),
('06af4c128958e61106c4b85ca5302d79dd255784cdd6241f15ec6f7ce725eb1eac7fca3439644400', 372, 1, 'LaraPassport', '[]', 0, '2020-10-28 15:53:51', '2020-10-28 15:53:51', '2021-10-28 08:53:51'),
('06af62534570f679fabb4be19cb56bafddc7a4c461e3fb556802a0840e837c35305f312174a0e1dd', 57, 1, 'LaraPassport', '[]', 0, '2020-05-07 10:51:06', '2020-05-07 10:51:06', '2021-05-07 10:51:06'),
('06f333c3024c90633cd817c9367aa3f23d10cf3292a3edc52d1140b58008f53943ffb410ad0a9445', 153, 1, 'LaraPassport', '[]', 0, '2020-08-24 16:23:57', '2020-08-24 16:23:57', '2021-08-24 09:23:57'),
('0756f3503b3be087a691b392166683f8a0e8a39aef5ce3ec7d643e7a3db0d594b7cc7db633ea4e15', 58, 1, 'LaraPassport', '[]', 0, '2020-04-22 04:57:30', '2020-04-22 04:57:30', '2021-04-22 04:57:30'),
('075b95bccafadfaa160aa82533e34caca8342c3a06db0c284629003301f95d60bffb5ce4598cc281', 109, 1, 'LaraPassport', '[]', 0, '2020-06-22 07:54:51', '2020-06-22 07:54:51', '2021-06-22 07:54:51'),
('07b96feea8243860db701fa44bd1a9d5afbcb97c543d56c138bba450f036f960e187f8ba17fec345', 57, 1, 'LaraPassport', '[]', 0, '2020-04-22 04:58:24', '2020-04-22 04:58:24', '2021-04-22 04:58:24'),
('07cb679591962e4dc2506274cff3c10f8008bcfbd822a636ff9ee64da95298eeab8897ee658e57b4', 411, 1, 'LaraPassport', '[]', 0, '2020-11-12 15:37:02', '2020-11-12 15:37:02', '2021-11-12 08:37:02'),
('07cf8410bd9d9dac079a689a9db63ffffd3639dfc76ebbd08040386709e79f4384c73ac2153f20e7', 62, 1, 'LaraPassport', '[]', 0, '2020-05-28 05:01:33', '2020-05-28 05:01:33', '2021-05-28 05:01:33'),
('07dae8aa1def8c2430fb7eba3fecc43d5a68c1840a1988fa3bbeb0cbb3a1512249c63ca364a64248', 266, 1, 'LaraPassport', '[]', 0, '2020-09-29 19:08:43', '2020-09-29 19:08:43', '2021-09-29 12:08:43'),
('083bcff7429c085eda3741ec7d8a7f9213bcb3b17ac9952beb5464816c22ff62b9a49137a8259dbb', 309, 1, 'LaraPassport', '[]', 0, '2020-09-26 18:18:00', '2020-09-26 18:18:00', '2021-09-26 11:18:00'),
('0862bc2f74cad741f406272c735fda2759b20c5cb5b89232ce0b68b20ecf528efcd67c9ff9870eac', 119, 1, 'LaraPassport', '[]', 0, '2020-06-27 10:58:57', '2020-06-27 10:58:57', '2021-06-27 10:58:57'),
('089d7f533e00cd939863379f5f58a069f9abaf82b2bbe2ac62e689be385ff9717c9c50fd8d7df697', 372, 1, 'LaraPassport', '[]', 0, '2020-10-28 17:22:08', '2020-10-28 17:22:08', '2021-10-28 10:22:08'),
('08b595f940bbe1d861dbc3a8d6d8746f5dc6c77b40a99a74a0907f367337c07e0465676b642226b7', 232, 1, 'LaraPassport', '[]', 0, '2020-08-20 12:15:33', '2020-08-20 12:15:33', '2021-08-20 05:15:33'),
('08bb4d298c6767a19877d5639906736d4d59affd94538acfb546d7b720d364c40586f4c4a6966a02', 62, 1, 'LaraPassport', '[]', 0, '2020-05-27 10:21:21', '2020-05-27 10:21:21', '2021-05-27 10:21:21'),
('08d70e806f880423cb9ae1d3c7869eb4134df1b4600456fec43971dbfc9332f28e2dc54d982dd735', 361, 1, 'LaraPassport', '[]', 0, '2020-10-15 13:46:57', '2020-10-15 13:46:57', '2021-10-15 06:46:57'),
('08f9768fcc51f156f587b21c876547e82eb6492dc5b0dd93b763d422338e737a6afcd1ef38caecf9', 300, 1, 'LaraPassport', '[]', 0, '2020-10-13 17:56:39', '2020-10-13 17:56:39', '2021-10-13 10:56:39'),
('093c52a3a9290f153422df3d8646f029d691a2c34e2e949d5b217957e25754b3a2dd4130de72407f', 153, 1, 'LaraPassport', '[]', 0, '2020-08-27 00:28:22', '2020-08-27 00:28:22', '2021-08-26 17:28:22'),
('093caf4553eee56ad81cf596a74dd552a36395c28fde29eff288099e781682e1967aca70d1feac39', 237, 1, 'LaraPassport', '[]', 0, '2020-10-22 00:16:48', '2020-10-22 00:16:48', '2021-10-21 17:16:48'),
('094168d4deb7153b32332f10bd22024375aab7734ec402efe6b989d6a2ab360d538b6e75ead60280', 57, 1, 'LaraPassport', '[]', 0, '2020-04-29 12:23:20', '2020-04-29 12:23:20', '2021-04-29 12:23:20'),
('09bd409f9a1586f3da2290204c03d74c19ec1b57f23279294097b82637347a4980560f5f841f2dba', 347, 1, 'LaraPassport', '[]', 0, '2020-10-09 23:31:34', '2020-10-09 23:31:34', '2021-10-09 16:31:34'),
('09be96ca7e3e1747b75b1758eda6c048d4469233e65a6e9ed3c444bf57be74cc41e0daed322105e4', 249, 1, 'LaraPassport', '[]', 0, '2020-10-01 17:26:05', '2020-10-01 17:26:05', '2021-10-01 10:26:05'),
('09db6e9af7aa49e5c9eb06004bc7cdecc6d5b53ddc4a4c2682ead7b961b81dc96cb388d73b2958e4', 170, 1, 'LaraPassport', '[]', 0, '2020-08-13 18:23:36', '2020-08-13 18:23:36', '2021-08-13 11:23:36'),
('09dce9ed47cfea7e7097fd3937b665ec6ffbc15ec12b78511ae91ae23f71f2467514612d63bedca1', 276, 1, 'LaraPassport', '[]', 0, '2020-09-08 15:37:22', '2020-09-08 15:37:22', '2021-09-08 08:37:22'),
('09fc864cd7dc6e11f6739734f9ab46c58ffe4ccd377b73a1778f00894c5426d083c0b191d329e7a2', 307, 1, 'LaraPassport', '[]', 0, '2020-10-03 17:55:46', '2020-10-03 17:55:46', '2021-10-03 10:55:46'),
('0a06e3d99686a0feab5c6c7629fee2dcc16be5a47e17d7b654a89de002d4a68d56e910d47d90b3f8', 247, 1, 'LaraPassport', '[]', 0, '2020-09-01 19:39:12', '2020-09-01 19:39:12', '2021-09-01 12:39:12'),
('0a56f0156ade3aeaf3ff6211db0e355f762a793a74efd60d2bbbaa258d1e39cab1f4b4fb49686bc8', 283, 1, 'LaraPassport', '[]', 0, '2020-09-11 17:34:08', '2020-09-11 17:34:08', '2021-09-11 10:34:08'),
('0a7adbff6d2c3d4f7279e0d1a801f729fd3d86e6b341f48ce1ec8cdc67d952ea706a1b77c20dd607', 322, 1, 'LaraPassport', '[]', 0, '2020-09-28 15:05:48', '2020-09-28 15:05:48', '2021-09-28 08:05:48'),
('0afa7da58ab73822c0db193e8382e8bcd259cfd2e122cdf39c41d3f36d8ffcaf151f0005602e4704', 376, 1, 'LaraPassport', '[]', 0, '2020-10-28 22:35:03', '2020-10-28 22:35:03', '2021-10-28 15:35:03'),
('0b397078f5761ea5e87467435c6c238d0255761d9b04c0233ad74ce2031bfc6bbb01f98ab84bd0ca', 377, 1, 'LaraPassport', '[]', 0, '2020-10-30 17:09:40', '2020-10-30 17:09:40', '2021-10-30 10:09:40'),
('0b80794108bb8da3766e70d458f78c26f0b3f693c6f122282e2a2f6cb419c0a43b7eb7f476ca13b4', 198, 1, 'LaraPassport', '[]', 0, '2020-08-17 22:15:23', '2020-08-17 22:15:23', '2021-08-17 15:15:23'),
('0b9dedb62ee6c38a0aeb1ac2fbb8b32d59443cf47f11d982eac3ff5d3c237776691e596cddd89b8c', 266, 1, 'LaraPassport', '[]', 0, '2020-09-28 17:26:05', '2020-09-28 17:26:05', '2021-09-28 10:26:05'),
('0bc8110e1b16386ab15eaa6c2e053ed905be599b4b1087642b6b575dbd66bba17915864e8c9e4cd9', 389, 1, 'LaraPassport', '[]', 0, '2020-11-13 21:53:03', '2020-11-13 21:53:03', '2021-11-13 14:53:03'),
('0c83eec3de171d75408512a88585ceaaf636b79837dd8d8da34f46044f124ca775861863912b85bc', 412, 1, 'LaraPassport', '[]', 0, '2020-11-16 21:04:32', '2020-11-16 21:04:32', '2021-11-16 14:04:32'),
('0cf2eaeb74c0e38a4e935b0753a5f12e1180f914e316e479f52d51f8720e18cd0df0fdc5ec29922d', 62, 1, 'LaraPassport', '[]', 0, '2020-05-15 13:21:53', '2020-05-15 13:21:53', '2021-05-15 13:21:53'),
('0d4f8afe1ea2c469cecfb110c36f08d3c59dc1f943a487fcc68b2befc2d0c194a12e2233dbedb0d4', 35, 1, 'LaraPassport', '[]', 0, '2020-06-13 11:09:15', '2020-06-13 11:09:15', '2021-06-13 11:09:15'),
('0d52268e0b56876ff8d5a29d2d33d67f52e2e71815ea0688f8c0106cf74c9d6e58b2862491aef839', 333, 1, 'LaraPassport', '[]', 0, '2020-10-01 16:29:02', '2020-10-01 16:29:02', '2021-10-01 09:29:02'),
('0d8fc156d47990603b1edf0694eb481bc5de87db5142503f5ce447b2dce28da5c40c8c5a053d520b', 307, 1, 'LaraPassport', '[]', 0, '2020-09-25 13:15:48', '2020-09-25 13:15:48', '2021-09-25 06:15:48'),
('0e1a2fa597c1bf954660e1ed510dc6690a5f7f4006e48e04a5d45b63a5b752b218569884b25c94bd', 420, 1, 'LaraPassport', '[]', 0, '2020-11-24 20:03:20', '2020-11-24 20:03:20', '2021-11-24 13:03:20'),
('0e7188e1692cee23ffa1740e36a5c11cdce73c3e44cabbc5167f11a05b99383da23fa418967b4a41', 201, 1, 'LaraPassport', '[]', 0, '2020-08-18 02:30:32', '2020-08-18 02:30:32', '2021-08-17 19:30:32'),
('0e94c4066a3791caf121e57c6b191fba9c1739edb76e8577970561dd84327915164120de04ace894', 243, 1, 'LaraPassport', '[]', 0, '2020-08-27 15:15:32', '2020-08-27 15:15:32', '2021-08-27 08:15:32'),
('0eab0ebed8bb7f1d03236a13983111be11204bc440bf3d5fc4f945321989bfb6c9c01c89fc81da81', 305, 1, 'LaraPassport', '[]', 0, '2020-09-15 06:04:10', '2020-09-15 06:04:10', '2021-09-14 23:04:10'),
('0ece5277492afc718551c36c5cf8bf9992ea626115a35b9178670159504768a30e8de97f9c429069', 235, 1, 'LaraPassport', '[]', 0, '2020-08-22 22:54:00', '2020-08-22 22:54:00', '2021-08-22 15:54:00'),
('0ee3d9c90c242b0fc06e21293d25da3d8a9410f3a866f83ca857889af2d144639a6a6b1670c8760e', 62, 1, 'LaraPassport', '[]', 0, '2020-08-17 21:40:29', '2020-08-17 21:40:29', '2021-08-17 14:40:29'),
('0ef1575f91342d8b161f4f1e4582dc9db28cd775c6f8b7294840164381bdcd20593ef35b47dc3d7a', 33, 1, 'LaraPassport', '[]', 0, '2020-04-10 07:39:00', '2020-04-10 07:39:00', '2021-04-10 07:39:00'),
('0f354ec8a4be11b149db2bf0c9530e2b5c5ec1d5841aeae06407452addf4512f02c016b94f633ed9', 249, 1, 'LaraPassport', '[]', 0, '2020-09-30 17:54:26', '2020-09-30 17:54:26', '2021-09-30 10:54:26'),
('0f542febca47586b95fa63ad0e43c48a803967e3e81f1979918a2aee0ee8cb5cf8987a1523f658a6', 62, 1, 'LaraPassport', '[]', 0, '2020-04-28 18:27:56', '2020-04-28 18:27:56', '2021-04-28 18:27:56'),
('0f7838d61d322536c87fee2780ad837731f34f27aeaf1e80dd7116b77a4ea091d68e3c72f4e6ef0b', 105, 1, 'LaraPassport', '[]', 0, '2020-06-12 07:04:42', '2020-06-12 07:04:42', '2021-06-12 07:04:42'),
('0f88c11529a3bfa68e339fac44240dc0eef7434804d85c2b917c37ea41f6d5c57a710f44367423f9', 35, 1, 'LaraPassport', '[]', 0, '2020-08-18 12:59:29', '2020-08-18 12:59:29', '2021-08-18 05:59:29'),
('0f945ae61be227565a730721366c976794c31b6991f67c4cac2f379220e62ff71d447653ce475b4f', 412, 1, 'LaraPassport', '[]', 0, '2020-11-16 20:35:26', '2020-11-16 20:35:26', '2021-11-16 13:35:26'),
('0f9a9abd6ed9f18ea7380ff201917bffac3e49d8cac75b82f5025eb54ec3f7cf00567200950b59e3', 240, 1, 'LaraPassport', '[]', 0, '2020-08-24 15:27:21', '2020-08-24 15:27:21', '2021-08-24 08:27:21'),
('0fba510999c7f412de368674311199391951ad7b6ff3b1e91e34681fc3d5741f96ea5672e63e0e37', 35, 1, 'LaraPassport', '[]', 0, '2020-07-31 12:24:00', '2020-07-31 12:24:00', '2021-07-31 05:24:00'),
('0fcec6a42b5acde5bc2e31d7d3a97649fbf23563831404703d1f09f5bba841a2e4b91c6a8d36192c', 35, 1, 'LaraPassport', '[]', 0, '2020-07-02 11:03:12', '2020-07-02 11:03:12', '2021-07-02 11:03:12'),
('0fd8ac89bd0ffd305a4298eea001488981699795f223ad2bfa26bdf592fc752bbb58b91d57e95e99', 416, 1, 'LaraPassport', '[]', 0, '2020-11-22 19:41:14', '2020-11-22 19:41:14', '2021-11-22 12:41:14'),
('0fd9079f5cf9dc5571bed6fe5eb7e823088b10aa125e6f9c14b74c6cfcc7b9b26d76711030edd956', 35, 1, 'LaraPassport', '[]', 0, '2020-05-14 14:56:34', '2020-05-14 14:56:34', '2021-05-14 14:56:34'),
('0fe325d4b79899eac32814b720cd56ed8df28f8fc7f50e2252019f204e6d00d63e451981fa25477f', 57, 1, 'LaraPassport', '[]', 0, '2020-08-04 12:25:04', '2020-08-04 12:25:04', '2021-08-04 05:25:04'),
('10007f64669c8ac5b48ab07a13e21798685e07d898c80a4231b39f4dbd9640ca28ffc587002d43af', 34, 1, 'LaraPassport', '[]', 0, '2020-04-22 08:33:37', '2020-04-22 08:33:37', '2021-04-22 08:33:37'),
('10025b90a7592c579cbbf66fb49c015ad9a1d0b7b8623d965af88d1234f63063e65949303cf1c28b', 208, 1, 'LaraPassport', '[]', 0, '2020-08-18 16:47:12', '2020-08-18 16:47:12', '2021-08-18 09:47:12'),
('105cc666ea24a8a4697fc43d52b063bf9dc6da2d5755419f79b0969bfbcf93c099eb931805ebd0cb', 109, 1, 'LaraPassport', '[]', 0, '2020-07-03 17:47:10', '2020-07-03 17:47:10', '2021-07-03 17:47:10'),
('10c4d0e42b0c780800d8ddb008f0a27eb1f6428db6c44b2460e19e1754caf011b64d055528a25d0b', 322, 1, 'LaraPassport', '[]', 0, '2020-09-28 15:06:11', '2020-09-28 15:06:11', '2021-09-28 08:06:11'),
('10e3b02f68dfffb4a167fc30d8d5e4899399fe180cd5a2cca693e932cd13b04d1cdcd6a226a67886', 35, 1, 'LaraPassport', '[]', 0, '2020-08-13 15:13:16', '2020-08-13 15:13:16', '2021-08-13 08:13:16'),
('1123ec58d5fe5a07e5fb7fe48a877d30dd0d4e5f868ecb8d19590649c53ac37d451f2ca2db82ab41', 35, 1, 'LaraPassport', '[]', 0, '2020-05-13 16:54:18', '2020-05-13 16:54:18', '2021-05-13 16:54:18'),
('11c7d7a70ff65f4e88ca5e378c880403951256bd4ba947ab692b8feae530435432e3f2018f77a807', 33, 1, 'LaraPassport', '[]', 0, '2020-04-11 12:13:21', '2020-04-11 12:13:21', '2021-04-11 12:13:21'),
('11cb0558849129a20897bb51b664cf5da7042b316d487d62a03c13680ac4a8be8c908b7dfafa83f8', 57, 1, 'LaraPassport', '[]', 0, '2020-08-20 16:26:46', '2020-08-20 16:26:46', '2021-08-20 09:26:46'),
('11ccc7c04bed2e46a3b000a6f791b9048e18b336a155a2dcd61df66685245a9002c4c1e2fb8aee81', 313, 1, 'LaraPassport', '[]', 0, '2020-09-21 15:04:39', '2020-09-21 15:04:39', '2021-09-21 08:04:39'),
('11fd544bbff657eca0ac473883f6d72dace38b9a0ac37b7e181b7368c95a7b27cc20d42cca2f8377', 33, 1, 'LaraPassport', '[]', 0, '2020-04-15 06:15:16', '2020-04-15 06:15:16', '2021-04-15 06:15:16'),
('121d6a6db9114d0129224bdfd4fcfe26f749df263b6c469e52235e52e3a76005e66e9a8f5e87a7b6', 33, 1, 'LaraPassport', '[]', 0, '2020-04-13 05:59:00', '2020-04-13 05:59:00', '2021-04-13 05:59:00'),
('1228bb57a9bddb2e50391be87c151f9e0839e10a100f23ebebc551ff10ff9d533cdfe3464eddc287', 33, 1, 'LaraPassport', '[]', 0, '2020-04-11 13:45:21', '2020-04-11 13:45:21', '2021-04-11 13:45:21'),
('122e550346cd22d8f1ddb1b3bef8531bf39ffe883de9bb47d846740c135a34a4ca6b0edc510a82ed', 290, 1, 'LaraPassport', '[]', 0, '2020-09-11 10:37:25', '2020-09-11 10:37:25', '2021-09-11 03:37:25'),
('126feca601746ae44bbbc71d0c21791be3b541521c7ed9cbbab17173903a89041fbd1ed2ab2f62ac', 328, 1, 'LaraPassport', '[]', 0, '2020-09-30 20:45:27', '2020-09-30 20:45:27', '2021-09-30 13:45:27'),
('12cd051bf4c01372c5e1d45f52bca27af68a1737d8785ed03c1924c2596357595e851856b9402fa8', 62, 1, 'LaraPassport', '[]', 0, '2020-05-28 11:48:02', '2020-05-28 11:48:02', '2021-05-28 11:48:02'),
('1312131e490defcc6ffbca65f9fbe1b8335b0e184242c3d7dfe307bac5fc4fcf5d54acbab93ecc5f', 421, 1, 'LaraPassport', '[]', 0, '2020-11-25 22:03:59', '2020-11-25 22:03:59', '2021-11-25 15:03:59'),
('13ebe9f5e7e6b600180928602f957d82b4ff1a538120e4c54bb02dd6a02e471bdb975e69d9b16dba', 62, 1, 'LaraPassport', '[]', 0, '2020-05-02 12:53:07', '2020-05-02 12:53:07', '2021-05-02 12:53:07'),
('1415d590fcaf2f3f268e0cf130f90bda11a42d04e4b025ad7a3326d8ac315bc319508c7078fde578', 419, 1, 'LaraPassport', '[]', 0, '2020-11-24 03:41:55', '2020-11-24 03:41:55', '2021-11-23 20:41:55'),
('141704dfdb327615afda804851f5a5ba7b41ddb61cae2149adad05c16cfddfeb0c34290963ca24eb', 431, 1, 'LaraPassport', '[]', 0, '2020-12-20 20:42:24', '2020-12-20 20:42:24', '2021-12-20 13:42:24'),
('148e08445e6f77155fe8d7868f05899e5058984c4c7e7d0e3c374edef3e349d4791568f22a80db1a', 98, 1, 'LaraPassport', '[]', 0, '2020-08-16 23:03:39', '2020-08-16 23:03:39', '2021-08-16 16:03:39'),
('1491b8dd719a6002a5a1169e384e9ce734cad559c7ed4c463fcbc9d2acec1fd1fa82ef576357ef0f', 59, 1, 'LaraPassport', '[]', 0, '2020-04-23 02:16:15', '2020-04-23 02:16:15', '2021-04-23 02:16:15'),
('149c4c7693239fc2882bc93b4410b82b913e5f2bc81cb0b8b462e9261f9b71e3d6a77fdee22094a1', 33, 1, 'LaraPassport', '[]', 0, '2020-04-11 07:21:01', '2020-04-11 07:21:01', '2021-04-11 07:21:01'),
('14c9c06348a41e244f70e310ef75306eeecf37c35c07f40825ec02c8da5203c3bdc870ff0793f317', 331, 1, 'LaraPassport', '[]', 0, '2020-10-01 13:27:28', '2020-10-01 13:27:28', '2021-10-01 06:27:28'),
('1526807b321cdcb6d1ac7bc01e4f77fae7e7c28a2d50ae754215511cbdbed179f2a7dc8dad75961e', 153, 1, 'LaraPassport', '[]', 0, '2020-08-20 19:25:15', '2020-08-20 19:25:15', '2021-08-20 12:25:15'),
('153243ff26cfafc330292cda7edd2ab0f75c22cccc7ba67269b82491cbf7d6af021d5acb8ecea40d', 63, 1, 'LaraPassport', '[]', 0, '2020-06-28 11:47:08', '2020-06-28 11:47:08', '2021-06-28 11:47:08'),
('153e2f1ff6e8991f89716e7e8846a38651aaace8ae8d9741e9875b9fb3db43611fa0633a038bafef', 336, 1, 'LaraPassport', '[]', 0, '2020-10-01 20:37:14', '2020-10-01 20:37:14', '2021-10-01 13:37:14'),
('155f25fb292bf5180a424b227d2de47f6d6cf9d350647d62b483a79a434a1cf61095655ea4ff3823', 35, 1, 'LaraPassport', '[]', 0, '2020-08-01 18:03:25', '2020-08-01 18:03:25', '2021-08-01 11:03:25'),
('157dcb647e7c9b74ff1d00a174f246f7851e42bcba8b76742584b345bd979c35dc4f34ee3a7b1f19', 256, 1, 'LaraPassport', '[]', 0, '2020-09-24 18:07:00', '2020-09-24 18:07:00', '2021-09-24 11:07:00'),
('15848404cef77db60454cdc69bef6bf4ce4bb39e12c689a49401398d0dea53c4bd546213aad6e477', 33, 1, 'LaraPassport', '[]', 0, '2020-04-11 07:50:23', '2020-04-11 07:50:23', '2021-04-11 07:50:23'),
('15b36a1b8ffd323982d461907f46b9616e2f07cc80a3018428d2ab42893abde96bb8b1a90bfd0abc', 33, 1, 'LaraPassport', '[]', 0, '2020-04-11 03:57:45', '2020-04-11 03:57:45', '2021-04-11 03:57:45'),
('16087f3046b1a0623463a66b1a2836e209ef8ca108aa43fb9b895ef2a7429be75835dce125dd5f2d', 414, 1, 'LaraPassport', '[]', 0, '2020-11-20 17:22:21', '2020-11-20 17:22:21', '2021-11-20 10:22:21'),
('161af97427ea8312c88970892c0b6520c9f3d0f94a9d706a270dfa2d3fb7ffb9aea87800e54ffc25', 302, 1, 'LaraPassport', '[]', 0, '2020-09-11 17:49:52', '2020-09-11 17:49:52', '2021-09-11 10:49:52'),
('1628b5483854bd4d838251abf5dd9e87c9562b69079d759cdbb214cfee080e9f40286fb32e2e98e6', 4, 1, 'LaraPassport', '[]', 0, '2020-03-19 00:46:59', '2020-03-19 00:46:59', '2021-03-19 06:16:59'),
('16e1245dd28d3d4d775de5abce2e9831105e3ee8b1624f7109875aa5917ce84f19d7ae4314aa5e26', 130, 1, 'LaraPassport', '[]', 0, '2021-01-21 23:46:13', '2021-01-21 23:46:13', '2022-01-21 16:46:13'),
('178f41044289e1ca32a9cf9b751781a5c14656163c677ab66cf86e17c4f4d9e59756a8ad3de5f1b2', 196, 1, 'LaraPassport', '[]', 0, '2020-08-17 14:06:17', '2020-08-17 14:06:17', '2021-08-17 07:06:17'),
('17a180b5a18cb732eab13bb1cd5b9e83121053c6bbc8658efcda58a75e8ef2a38fb8261bce75d2ef', 309, 1, 'LaraPassport', '[]', 0, '2020-09-25 12:21:05', '2020-09-25 12:21:05', '2021-09-25 05:21:05'),
('17f834aa0f4887a098a829bf157a261a463316c6b4d802b457a1d63f5e7c6bf184c532c4ce3b060a', 389, 1, 'LaraPassport', '[]', 0, '2020-11-07 23:58:45', '2020-11-07 23:58:45', '2021-11-07 16:58:45'),
('181419094276246079f28452e341e0cd3a0d95a021d13b8982c1c9423dc34aaca37380df5796fa76', 232, 1, 'LaraPassport', '[]', 0, '2020-08-20 12:03:51', '2020-08-20 12:03:51', '2021-08-20 05:03:51'),
('187d5982738242e2f72154d59d3df0cf138e8a380332d2231ff342c02c297fe0a58f12bd84895395', 316, 1, 'LaraPassport', '[]', 0, '2020-11-22 04:21:12', '2020-11-22 04:21:12', '2021-11-21 21:21:12'),
('18a672ebd922a2aef2f044d9fb3ece8c48690faff70efa0fbff2e6a6e01586edd2563d379688fbe2', 33, 1, 'LaraPassport', '[]', 0, '2020-07-28 12:20:57', '2020-07-28 12:20:57', '2021-07-28 05:20:57'),
('18b8ac4e6a1b1f374ee9f0faa9a70454e5531752c6215a628b6a9c774aafbfd3f3f772d9ffcbe9a6', 58, 1, 'LaraPassport', '[]', 0, '2020-04-22 08:03:35', '2020-04-22 08:03:35', '2021-04-22 08:03:35'),
('18cf19072f84449670f44cb9905e4745cd3fc52e5ae6f613a89ee563be88a7ecd36ff52a3c5e20b3', 233, 1, 'LaraPassport', '[]', 0, '2020-08-21 13:17:33', '2020-08-21 13:17:33', '2021-08-21 06:17:33'),
('18f7f4a7ae7d65e06eaf579afc0a42753dabded389985243e0d71467787611952429b0062953af8b', 33, 1, 'LaraPassport', '[]', 0, '2020-04-11 08:12:42', '2020-04-11 08:12:42', '2021-04-11 08:12:42'),
('19919290ce4ebd3ea730ccc798ad6815a4703d916ae11eaaa861c91e014b9d43bb038f6094fb0c27', 57, 1, 'LaraPassport', '[]', 0, '2020-06-25 15:42:34', '2020-06-25 15:42:34', '2021-06-25 15:42:34'),
('1a3cb9a7e2ae73929f4fe2ac68a8215ad3111be756f389f5e75f3249bcc54df86a0e3b6558935eed', 237, 1, 'LaraPassport', '[]', 0, '2020-09-02 13:21:17', '2020-09-02 13:21:17', '2021-09-02 06:21:17'),
('1a3ebda9f520a6befe0795c286958794a82952783fe7c3926fe716672cfc9ad15faaa93d096be889', 214, 1, 'LaraPassport', '[]', 0, '2020-08-18 18:25:57', '2020-08-18 18:25:57', '2021-08-18 11:25:57'),
('1a4983fbb816b1c27cb136097e8e88dd4309ca99385488f3a3ed48111de92205c6d9905102c9b2d5', 35, 1, 'LaraPassport', '[]', 0, '2020-06-25 10:18:56', '2020-06-25 10:18:56', '2021-06-25 10:18:56'),
('1a9800265162d3f0337469c4656bc76fb49671337fec1474539c89083d14bc49b0df5ec92c310a36', 266, 1, 'LaraPassport', '[]', 0, '2020-10-05 12:30:02', '2020-10-05 12:30:02', '2021-10-05 05:30:02'),
('1ae1de23e7ecbf3e0fd531be693918b33ee3a4f4f65bc976578dcebfc3a268a1bd8d7032f4d9373b', 312, 1, 'LaraPassport', '[]', 0, '2020-09-30 16:46:26', '2020-09-30 16:46:26', '2021-09-30 09:46:26'),
('1aedf33157d852fb267395c2c46f02ee2bfd38bf698897f445fc3cf1d2919fe5ca8ab7bc3bf37ee1', 245, 1, 'LaraPassport', '[]', 0, '2020-09-10 12:02:53', '2020-09-10 12:02:53', '2021-09-10 05:02:53'),
('1af2554bbde16599966f940236765d36363ead8c2a4c3d5285a38c90c2af3d4d35effd3d1b8bd278', 57, 1, 'LaraPassport', '[]', 0, '2020-08-18 16:04:25', '2020-08-18 16:04:25', '2021-08-18 09:04:25'),
('1b0fc2c386f76c87b51df47bf4d44f8ed3a630ffe3f7dd922445d7f72b1ec463aad4a41d8652bd32', 195, 1, 'LaraPassport', '[]', 0, '2020-08-17 07:48:37', '2020-08-17 07:48:37', '2021-08-17 00:48:37'),
('1b1a2b4d8bf9cadd9956f014ff6b96e52d634c37584601d366ed195c0c5b26c8a1a9cc28cd663cc0', 153, 1, 'LaraPassport', '[]', 0, '2020-08-26 23:59:21', '2020-08-26 23:59:21', '2021-08-26 16:59:21'),
('1b2d5a12d7471376f60de29f3d32d198f80476deef38e2df5e92229575a3e208527388f12ef3ae3c', 58, 1, 'LaraPassport', '[]', 0, '2020-04-22 11:35:41', '2020-04-22 11:35:41', '2021-04-22 11:35:41'),
('1bec84d26ac0de57106ff0e027537dd91900c59a0cbdb0654eef178e1c5b84905674eb9f110e4ced', 307, 1, 'LaraPassport', '[]', 0, '2020-09-17 15:04:02', '2020-09-17 15:04:02', '2021-09-17 08:04:02'),
('1c5b83d5adfc5507242002583bfbdf17376ee56bbcef37f557db7bd353e9d377710dec091f5953b1', 33, 1, 'LaraPassport', '[]', 0, '2020-04-13 08:46:04', '2020-04-13 08:46:04', '2021-04-13 08:46:04'),
('1c61c2cc5fc60a58d0844dcc25e03c844406e9f60b252f4ec4237a73a68e3a09d2fb80dfad814121', 309, 1, 'LaraPassport', '[]', 0, '2020-09-24 19:41:25', '2020-09-24 19:41:25', '2021-09-24 12:41:25'),
('1c9612f5495ed885358a7aad1306ae379fa92fc5f439b6a4e8ef8b68711cef439e00a90971ad328a', 33, 1, 'LaraPassport', '[]', 0, '2020-04-20 14:08:08', '2020-04-20 14:08:08', '2021-04-20 14:08:08'),
('1cb26f6d7ea8033dd225e40e6a27fcccb6d17be49933e0f77ac2d6eb6fcab0305bb50fd047970ffd', 309, 1, 'LaraPassport', '[]', 0, '2020-09-27 16:23:07', '2020-09-27 16:23:07', '2021-09-27 09:23:07'),
('1cb8ddcf24386004bbde593286f5168ad7672a3e1d451bc30e50dc97bc7dc4d3a1ce41a233e1f396', 59, 1, 'LaraPassport', '[]', 0, '2020-04-23 02:16:33', '2020-04-23 02:16:33', '2021-04-23 02:16:33'),
('1ce29e6b0e6723b23dc27a303f6ccc395318f3866b0c5562caa0b76405a085a8409a0f0fdd0d512f', 238, 1, 'LaraPassport', '[]', 0, '2020-09-06 13:34:16', '2020-09-06 13:34:16', '2021-09-06 06:34:16'),
('1d5fceb0c052c967fe80ea901d99cc1a191aad463b049d4e442d271154cfba0f70fee764f35a2b0b', 364, 1, 'LaraPassport', '[]', 0, '2020-10-15 16:36:52', '2020-10-15 16:36:52', '2021-10-15 09:36:52'),
('1d6aeaa72638249c9d16c61c132ab973bfd8be31101fe3d3e932d40d92a4c4770093d8a8bc4add26', 57, 1, 'LaraPassport', '[]', 0, '2020-08-08 13:10:39', '2020-08-08 13:10:39', '2021-08-08 06:10:39'),
('1d7f9013158c3039c43b1743a811d007944dee7a688fe771218f9402cdbcfe8eb8fa9ab5e744438e', 33, 1, 'LaraPassport', '[]', 0, '2020-04-10 09:12:35', '2020-04-10 09:12:35', '2021-04-10 09:12:35'),
('1dd9aeea979e2e09f74b80035970f7a83587963bd3b22e43c6ba73b784003f35d35162363e30a47c', 57, 1, 'LaraPassport', '[]', 0, '2020-08-13 18:02:49', '2020-08-13 18:02:49', '2021-08-13 11:02:49'),
('1dfced92b0ab0f974eca852bf232257813232be9c8471f685d9a8e3bab82ead3369d122fdb30dd70', 62, 1, 'LaraPassport', '[]', 0, '2020-08-18 19:16:03', '2020-08-18 19:16:03', '2021-08-18 12:16:03'),
('1e073227c97b80139845d9dab99711ccdfe7018c48926f1f34b308a469e66b1eae5c1edabad64f97', 33, 1, 'LaraPassport', '[]', 0, '2020-04-10 08:19:31', '2020-04-10 08:19:31', '2021-04-10 08:19:31'),
('1e5d638dd36b2c3d2d9ca06fc4a7e5ed90d4f40e35688df6f7834d143bb2d5de47180f039f5a1b41', 35, 1, 'LaraPassport', '[]', 0, '2020-06-29 06:30:46', '2020-06-29 06:30:46', '2021-06-29 06:30:46'),
('1e5ed7ca73dbd85ddb5062fe8773fa8c123617584522f57e3f5afb0392dc0efa8b5d808cdfbdb0e7', 214, 1, 'LaraPassport', '[]', 0, '2020-08-18 18:28:19', '2020-08-18 18:28:19', '2021-08-18 11:28:19'),
('1e62ff578f64ed5332e0883cab95bcc4808e92f784eb56ef0a10a44d397372c51bd535841cd65288', 33, 1, 'LaraPassport', '[]', 0, '2020-04-10 12:40:33', '2020-04-10 12:40:33', '2021-04-10 12:40:33'),
('1f97eaea13f386064ac6b30329eece500e92f88e095e329fac765a780d2f0b36801ef2cb306d53c8', 62, 1, 'LaraPassport', '[]', 0, '2020-04-30 11:35:41', '2020-04-30 11:35:41', '2021-04-30 11:35:41'),
('1fa7716a4b102e707872304f9ca9442e8ead766a939b4f88b958d873173ce5ebb12b9c5e9d451cbd', 57, 1, 'LaraPassport', '[]', 0, '2020-08-04 13:09:35', '2020-08-04 13:09:35', '2021-08-04 06:09:35'),
('1fabdf6b991b730970d31336434156f2b4f0590e5e25e199a0bd3fa5a10de6c44c5e5530f5d3a310', 373, 1, 'LaraPassport', '[]', 0, '2020-11-05 18:48:49', '2020-11-05 18:48:49', '2021-11-05 11:48:49'),
('204c06f1f2437f87f920d4b2473b48deccee7147c8efd7709f86c9e83c248af03e1d25df39508c32', 305, 1, 'LaraPassport', '[]', 0, '2020-09-23 16:25:48', '2020-09-23 16:25:48', '2021-09-23 09:25:48'),
('205a91209c4db6997daff5664edd294a59e85badf11b8f985c6ab19d4ddb58cb3b3cd30654fc6a0f', 57, 1, 'LaraPassport', '[]', 0, '2020-04-30 13:05:48', '2020-04-30 13:05:48', '2021-04-30 13:05:48'),
('207187cb45d7f26dd8f73e1ddaaf6f097268654f309870cdd05330d3432e7ac29506ba67ddb9b9a6', 33, 1, 'LaraPassport', '[]', 0, '2020-04-21 14:20:17', '2020-04-21 14:20:17', '2021-04-21 14:20:17'),
('20868643d22d2fa17cddbff043fd27e334d55eed8044842c0fd97a791384f83b8eaddfd18efbbe02', 194, 1, 'LaraPassport', '[]', 0, '2020-08-16 14:35:08', '2020-08-16 14:35:08', '2021-08-16 07:35:08'),
('21022189fe175ae30ed9bbf6fc03024618de2da7b238f73b5e054a15ecc664572311eee11b749749', 8, 1, 'LaraPassport', '[]', 0, '2020-04-09 10:13:43', '2020-04-09 10:13:43', '2021-04-09 10:13:43'),
('211ed737e832b9491475d006484cfd525b04319d42c99ca5786b4a259a6856ef30d6c75044a991e4', 119, 1, 'LaraPassport', '[]', 0, '2021-01-19 21:57:06', '2021-01-19 21:57:06', '2022-01-19 14:57:06'),
('218dff04666f08d925181a59204b88347b6ec538570df7197990c3e510f1b03a77130fb7c747c0b0', 300, 1, 'LaraPassport', '[]', 0, '2020-10-18 00:10:16', '2020-10-18 00:10:16', '2021-10-17 17:10:16'),
('21aaf7281a0f71279f629af687b601f26504cb38d0e24a4ce885bfc967a303959b7df407a313aa4b', 88, 1, 'LaraPassport', '[]', 0, '2020-06-04 04:48:26', '2020-06-04 04:48:26', '2021-06-04 04:48:26'),
('21ad16928f542c49fe05cd297e668c1576fa9a6efc76781ce0a57d805ea85448a8439132702eddca', 244, 1, 'LaraPassport', '[]', 0, '2020-10-23 14:28:13', '2020-10-23 14:28:13', '2021-10-23 07:28:13'),
('21b48791b0043211aed7598ca0d68614d069ec75e8bbc65d4e1ab1a620c4db3d21b1df740f6d62af', 33, 1, 'LaraPassport', '[]', 0, '2020-04-21 13:29:08', '2020-04-21 13:29:08', '2021-04-21 13:29:08'),
('21bf22f5aa088d339656c10e91c746e351d62a95ae82a714fcb8431443f1c5d07bce01bede235f8e', 57, 1, 'LaraPassport', '[]', 0, '2020-07-03 18:26:52', '2020-07-03 18:26:52', '2021-07-03 18:26:52'),
('21d9fdb70b79d836a7832b54f01a21e9833536adcefa04b2dba5d9b2aac8cbbcad5579eb70d39a58', 300, 1, 'LaraPassport', '[]', 0, '2020-10-01 19:07:11', '2020-10-01 19:07:11', '2021-10-01 12:07:11'),
('223143e081773943ee3d139d5f8cb509fb4be5b8b9c793651d9aeccccf8eaae80e853982892fc347', 244, 1, 'LaraPassport', '[]', 0, '2020-10-24 10:42:06', '2020-10-24 10:42:06', '2021-10-24 03:42:06'),
('223299f213ff033213350afc4a769d93d1894ae5e6a0e508c4d1c57015b2816fc7e1351d357a96dc', 266, 1, 'LaraPassport', '[]', 0, '2020-09-29 23:21:23', '2020-09-29 23:21:23', '2021-09-29 16:21:23'),
('22932cfbf07a7257d483012e16d6f5f7dad3aee300a5199be585428189b5c5070301d8dfbf5017f4', 33, 1, 'LaraPassport', '[]', 0, '2020-04-11 13:56:34', '2020-04-11 13:56:34', '2021-04-11 13:56:34'),
('22b8d8f53a652b810c8186415ea7ee4b1c3c6cf80fd53a74931c0826eb1fa40b611395ad69310273', 57, 1, 'LaraPassport', '[]', 0, '2020-06-27 10:56:47', '2020-06-27 10:56:47', '2021-06-27 10:56:47'),
('22e188f9b5dc8db472341d48b3e14bb31f2820b30364c00dfc6f01f67264d8626b61cb7ceb88e0be', 58, 1, 'LaraPassport', '[]', 0, '2020-04-25 13:06:52', '2020-04-25 13:06:52', '2021-04-25 13:06:52'),
('22ea5caf28170d111d5aeea66e8b6ff7f6c31f6848993e8f42c174aff1db553bd1282077dcb8c342', 383, 1, 'LaraPassport', '[]', 0, '2020-11-06 22:19:35', '2020-11-06 22:19:35', '2021-11-06 15:19:35'),
('230fd4f297c113d3edca7ec09ce00ab1ccc46670f727f41f382b3ebe0c6fa629efde43c4b84c9a23', 109, 1, 'LaraPassport', '[]', 0, '2020-08-01 17:22:53', '2020-08-01 17:22:53', '2021-08-01 10:22:53'),
('2313f8f77892e12be99b5428c65b1bfe65001c30102f32e976cc9767db60736c6d379f9271bed18d', 33, 1, 'LaraPassport', '[]', 0, '2020-04-20 09:19:25', '2020-04-20 09:19:25', '2021-04-20 09:19:25'),
('2322dea015928d66922490c0271aa0fb590d8199d2ced7b3fb6db244e6105a63df1b24c49be592f7', 108, 1, 'LaraPassport', '[]', 0, '2020-06-12 06:25:34', '2020-06-12 06:25:34', '2021-06-12 06:25:34'),
('237e95c28505f5f3d13dec2e552a6ff0c86ae9878d7499d79d1caa8ef3be5293e8ffefb6fd697d78', 35, 1, 'LaraPassport', '[]', 0, '2020-05-12 05:44:38', '2020-05-12 05:44:38', '2021-05-12 05:44:38'),
('23cb7f1af92c9e78f8651c79d306fd62be747ca07a7d70b2b4181d0527162c43499eba21fb457354', 369, 1, 'LaraPassport', '[]', 0, '2020-10-23 15:59:16', '2020-10-23 15:59:16', '2021-10-23 08:59:16'),
('24675b85e0541652e194a43109c1e5713dfab622a133a1b66769f2acbd4d6d684157346e047f1336', 353, 1, 'LaraPassport', '[]', 0, '2020-10-10 22:04:28', '2020-10-10 22:04:28', '2021-10-10 15:04:28'),
('24ee3bef52a21c56b4f0a536b90a1598a85a06a713c7c5bae7584e1521feaa40b4a9597e5db66036', 33, 1, 'LaraPassport', '[]', 0, '2020-04-11 04:48:53', '2020-04-11 04:48:53', '2021-04-11 04:48:53'),
('2506230eab140de1c5c0aa2b596aa6343b58acd419066d10f847183b49c0c48bfd6d3ceefa8e295e', 244, 1, 'LaraPassport', '[]', 0, '2020-11-05 16:42:20', '2020-11-05 16:42:20', '2021-11-05 09:42:20'),
('2535cc76d86288a33d180acdb141b69552810bbb1b946c8d23c116fafc2d0ba572703a0114b32d6a', 389, 1, 'LaraPassport', '[]', 0, '2020-11-12 18:22:36', '2020-11-12 18:22:36', '2021-11-12 11:22:36'),
('254896bc84ab048408afdb67f0ddbc653ce9752599ebe053805ddecedca41b4205dd4f24075909a4', 340, 1, 'LaraPassport', '[]', 0, '2020-10-06 18:00:46', '2020-10-06 18:00:46', '2021-10-06 11:00:46'),
('25b86d31316de5b6f3ca7ac9a1dd42841a63ecf6021bf74e0d1c7afebbc5db9293970b8440ec3094', 316, 1, 'LaraPassport', '[]', 0, '2020-10-28 21:32:45', '2020-10-28 21:32:45', '2021-10-28 14:32:45'),
('25d6f0a4ec31235fb4003e6149cc2edb15cebb1fe388f37d2c7e8d1994167969a786fb431b4f8ee7', 300, 1, 'LaraPassport', '[]', 0, '2020-09-30 20:28:41', '2020-09-30 20:28:41', '2021-09-30 13:28:41'),
('2648f5d05a3d0c2ca8fbb712d4cd0bd6c41b8c23362a09de1ca2aa88b796f371fc59a1d747e23d13', 119, 1, 'LaraPassport', '[]', 0, '2021-01-19 20:49:52', '2021-01-19 20:49:52', '2022-01-19 13:49:52'),
('26ae8c5d84fd2851671bec78bf826ca9c7e87cbffdbc6abdcd083691cccde894f72078a2c9e595c7', 230, 1, 'LaraPassport', '[]', 0, '2020-08-19 16:41:32', '2020-08-19 16:41:32', '2021-08-19 09:41:32'),
('27200fffb31a12fabd85e18dbe2f0ba4bd453a2c94741ae640e38f91d4ae7a134925419b16d79f3f', 35, 1, 'LaraPassport', '[]', 0, '2020-07-05 20:20:52', '2020-07-05 20:20:52', '2021-07-05 20:20:52'),
('2736a5d427371d22a010df81fae341d986cb0b835b35cd153cb11a381525d3482ce38293a5d2b45e', 33, 1, 'LaraPassport', '[]', 0, '2020-04-20 13:40:37', '2020-04-20 13:40:37', '2021-04-20 13:40:37'),
('2785f09512b4236e4111d00715cc6cbb91223cac8b17ff84c00897f2a1196a76e4d0dbeffdfba5b4', 122, 1, 'LaraPassport', '[]', 0, '2020-07-03 18:19:11', '2020-07-03 18:19:11', '2021-07-03 18:19:11'),
('27baf01ca418dde2750202ae7e3489ad5b0621aac9ea060be73250801d07a8f562704f7146bd2abe', 33, 1, 'LaraPassport', '[]', 0, '2020-04-16 10:56:26', '2020-04-16 10:56:26', '2021-04-16 10:56:26'),
('27bf00ce173acb3a10232a3ef748474741ae80eaeba6d63f2e73c6846b4548cf3e37548f75627195', 119, 1, 'LaraPassport', '[]', 0, '2021-01-19 20:05:35', '2021-01-19 20:05:35', '2022-01-19 13:05:35'),
('27e44857d3f8157baa9590d079eae0b4a0ed13a5853b0a9d121d5ccea826c7b50d996ed41385619d', 35, 1, 'LaraPassport', '[]', 0, '2020-06-27 07:53:54', '2020-06-27 07:53:54', '2021-06-27 07:53:54'),
('27eaef73d064a0b3c97a634f07b45fcad98939c5bdd0ff8dfb33ec60ab6daeb84f40e9cb35baaf0c', 417, 1, 'LaraPassport', '[]', 0, '2020-11-24 03:29:55', '2020-11-24 03:29:55', '2021-11-23 20:29:55'),
('28839c225c678515a2098de21641a15065e95cbc1a49c4ce41cd9a705f1a44cd4cd52b51b7d3a78c', 239, 1, 'LaraPassport', '[]', 0, '2020-08-24 16:32:40', '2020-08-24 16:32:40', '2021-08-24 09:32:40'),
('2901d2a7f25a0ff706cb856cf9821e23b8403bdc5f2e244a9341ef07c218465a4b49b43a56db1216', 195, 1, 'LaraPassport', '[]', 0, '2020-08-17 07:44:03', '2020-08-17 07:44:03', '2021-08-17 00:44:03'),
('291999af4a8d4ea56dbe9f173d1b8403e7517ed625253de30e165ee3f9d54a93a4cb467b371bf0f1', 35, 1, 'LaraPassport', '[]', 0, '2020-08-17 19:00:35', '2020-08-17 19:00:35', '2021-08-17 12:00:35'),
('292f7e814ea1c75e6f7a6d7b61e68fdc02fa7be4c39875a24858b79841e367dcc7152435f50d03e9', 35, 1, 'LaraPassport', '[]', 0, '2020-06-27 05:04:15', '2020-06-27 05:04:15', '2021-06-27 05:04:15'),
('293f667f538d6f078c9bc9e213994eeefd85b5a6978211607e164bb3dfe82adcf8a1bb5eea695d4b', 33, 1, 'LaraPassport', '[]', 0, '2020-04-11 07:43:58', '2020-04-11 07:43:58', '2021-04-11 07:43:58'),
('29421ec23afcfcc90e965fd02761281b1c5a0233366e6c56be53a58dbb51153b9b2a9f906063eaab', 266, 1, 'LaraPassport', '[]', 0, '2020-10-25 13:22:14', '2020-10-25 13:22:14', '2021-10-25 06:22:14'),
('2949478803ba3a0e87f24b065d522ed1af2518e7ff7147f6b6ae41b071fa04638cc8162aad846eba', 62, 1, 'LaraPassport', '[]', 0, '2020-08-18 19:25:11', '2020-08-18 19:25:11', '2021-08-18 12:25:11'),
('297f97ae60dc49ff563d7a4948c99e698bcc971652eafa2d8cb3124d1d8cbf81465ab87aebbf8a44', 338, 1, 'LaraPassport', '[]', 0, '2020-10-02 19:57:23', '2020-10-02 19:57:23', '2021-10-02 12:57:23'),
('297fd0e4be2b26ed7f226bce7dcb7f7385e21ac9ea285b500ea62c81068eda2eb0649e80f107f809', 33, 1, 'LaraPassport', '[]', 0, '2020-04-11 06:09:47', '2020-04-11 06:09:47', '2021-04-11 06:09:47'),
('2a37e51c322a97aba8b7e7798475408d9651989d268e7ec092d8aeab8beb21726c8a5943656ceba5', 57, 1, 'LaraPassport', '[]', 0, '2020-06-08 11:03:35', '2020-06-08 11:03:35', '2021-06-08 11:03:35'),
('2a483c53f4e3a396380eeda40b8d9f49be68fd445d20191e3ca05c222a0f9c637258279ea91aeb08', 59, 1, 'LaraPassport', '[]', 0, '2020-04-22 13:33:04', '2020-04-22 13:33:04', '2021-04-22 13:33:04'),
('2a488acc24358801e42adc53cf77e7b113880c73647bc4d992366d3eda16cb38ff6b3a5ed0d092cf', 33, 1, 'LaraPassport', '[]', 0, '2020-04-13 04:29:13', '2020-04-13 04:29:13', '2021-04-13 04:29:13'),
('2a6a9e91ea3cfe686b0cee47f12de032be10a88afd58925580813d287d6312e9bf201a5d97135042', 234, 1, 'LaraPassport', '[]', 0, '2020-08-21 14:02:55', '2020-08-21 14:02:55', '2021-08-21 07:02:55'),
('2a967b467596ad2d792994096506ca96500f5e74c1e35273f2a8dcb4abdabd5966ffebc0463200ec', 153, 1, 'LaraPassport', '[]', 0, '2020-08-27 00:29:51', '2020-08-27 00:29:51', '2021-08-26 17:29:51'),
('2a995ed2459e0ec46dcc925cca2f60180546b3eaa1806cdc46b502af1d85223ddf5b853a7b52f871', 35, 1, 'LaraPassport', '[]', 0, '2020-07-25 15:49:43', '2020-07-25 15:49:43', '2021-07-25 08:49:43'),
('2aca3048951b98286bbed2486b99df2ec5a2b7f8e248bebe7af8d288129655d1cd3ad10e57bd853b', 33, 1, 'LaraPassport', '[]', 0, '2020-04-11 07:53:57', '2020-04-11 07:53:57', '2021-04-11 07:53:57'),
('2b3eb88fdd1f2978dd1abf6d57a68db89d358c73f23d87638b238d0ae1969fdf2a237a33ad5bf153', 234, 1, 'LaraPassport', '[]', 0, '2020-08-24 12:08:54', '2020-08-24 12:08:54', '2021-08-24 05:08:54'),
('2b3f6d41eea5d5b1bf3efb34b34fcce71321b055603c83ad7717fa0181c73101dc0d6eea999aa6df', 104, 1, 'LaraPassport', '[]', 0, '2020-06-11 12:56:06', '2020-06-11 12:56:06', '2021-06-11 12:56:06'),
('2b74aa03f7b7bad50065773b5ec4848a8b69dfb7a6f56a1d8f6d4edc2322508f03bab8ba48d6c45a', 197, 1, 'LaraPassport', '[]', 0, '2020-08-17 14:36:05', '2020-08-17 14:36:05', '2021-08-17 07:36:05'),
('2b8e7a7de032aa9f09878db82f9f21d1309339b8fc95a0d72ac31b10bf636962e3f7ff5edf29dc90', 316, 1, 'LaraPassport', '[]', 0, '2020-10-28 21:32:58', '2020-10-28 21:32:58', '2021-10-28 14:32:58'),
('2b8f456454d6aa51cdfd7efc79a8192e83fa6382d602a43d936206d2ba70eb5c97bf98d698e0f7c2', 29, 1, 'LaraPassport', '[]', 0, '2020-04-09 09:47:14', '2020-04-09 09:47:14', '2021-04-09 09:47:14'),
('2bc22a8b1fd4891d539e6940b1312898364eb54f6de2298ea4b6b663d58b5677060bf29ced6ba90d', 237, 1, 'LaraPassport', '[]', 0, '2020-10-09 18:20:36', '2020-10-09 18:20:36', '2021-10-09 11:20:36'),
('2bc7d3e94f9f98f259f42f9a04811b39afad93e70c7e2ff029896077c54e2aaaa85f9872b0838e79', 249, 1, 'LaraPassport', '[]', 0, '2020-10-06 19:19:04', '2020-10-06 19:19:04', '2021-10-06 12:19:04'),
('2c1563e7fb2957ca23af1472f2c154afb180d15f742f1e9ef440ac6a46604205ae8c25e4415aa5af', 8, 1, 'LaraPassport', '[]', 0, '2020-04-10 04:42:18', '2020-04-10 04:42:18', '2021-04-10 04:42:18'),
('2c2535a7673a747cde81c6e822da00d65072c7078bfb6d4092cd2b1608fb577a36de00d1166f545b', 326, 1, 'LaraPassport', '[]', 0, '2020-09-30 19:02:15', '2020-09-30 19:02:15', '2021-09-30 12:02:15'),
('2c36984edc01cadfd418d097caa893e80eb143fe4adae109158cdc332bbd207a29eb95cac7684e15', 33, 1, 'LaraPassport', '[]', 0, '2020-04-16 04:16:13', '2020-04-16 04:16:13', '2021-04-16 04:16:13'),
('2c665262b12bb27015e2f2c0e0313b1fa45de08dc5c7bc8f148464db04779a39f93ad5ae311beef2', 57, 1, 'LaraPassport', '[]', 0, '2020-07-28 11:38:37', '2020-07-28 11:38:37', '2021-07-28 04:38:37'),
('2c7a9656995f4065338810d32dbbbc9d058fb94da9f54568633db232d53a569623da4231c38915cf', 58, 1, 'LaraPassport', '[]', 0, '2020-05-07 11:09:59', '2020-05-07 11:09:59', '2021-05-07 11:09:59'),
('2cbea272aa1939a6da00b303b3ca0f1fba3ecf044c887de76452b8330831666768cfcc622d56fb89', 119, 1, 'LaraPassport', '[]', 0, '2021-01-19 20:42:39', '2021-01-19 20:42:39', '2022-01-19 13:42:39'),
('2ce9ca90174bd37db8e01e2b8e78b776f7b87d79f97b861f605738209aa4e1e1c8588e3987fb14bf', 8, 1, 'LaraPassport', '[]', 0, '2020-04-10 05:57:02', '2020-04-10 05:57:02', '2021-04-10 05:57:02'),
('2cf7fad538a8eb05e092d756232f8dde219a637da29efadb440c06e7e92178a2f355ae56b5a985c9', 372, 1, 'LaraPassport', '[]', 0, '2020-10-28 15:54:03', '2020-10-28 15:54:03', '2021-10-28 08:54:03'),
('2d1eba3e5fc53ff9cda7d5a66a9949893deab5bcd4355e6305aa697d629aa1c607eec304bff02c36', 237, 1, 'LaraPassport', '[]', 0, '2020-10-09 17:23:57', '2020-10-09 17:23:57', '2021-10-09 10:23:57'),
('2d593ef304f44281e0d1802f88ea842d2f182e81652fd5c4b63ad615718b30116ff9fa25c2fdb04e', 239, 1, 'LaraPassport', '[]', 0, '2020-08-24 16:22:36', '2020-08-24 16:22:36', '2021-08-24 09:22:36'),
('2da3c94ee803bc06f2ffb3dedad74d700adfd46034b3f8a1e2202ecebcbe9b5d2f79a3decbdcc68c', 300, 1, 'LaraPassport', '[]', 0, '2020-10-01 17:30:48', '2020-10-01 17:30:48', '2021-10-01 10:30:48'),
('2dabda75a8797ff92a29898745f65e948f2c34a53c96062d9821fd5d7cebb1f6ab864cea1059fdfa', 218, 1, 'LaraPassport', '[]', 0, '2020-08-18 21:02:15', '2020-08-18 21:02:15', '2021-08-18 14:02:15'),
('2dd3930b97b419746f84ff720a14e67e4c823e8b0393e6e10053b7f22e278f0765b7e96ece815ba4', 35, 1, 'LaraPassport', '[]', 0, '2020-05-17 14:51:25', '2020-05-17 14:51:25', '2021-05-17 14:51:25'),
('2e070d0d9405fe75354dccaf87cc15c18d15787ea17ea3f3157a7047d4513eb6df152c5cc913f926', 35, 1, 'LaraPassport', '[]', 0, '2020-06-25 13:00:08', '2020-06-25 13:00:08', '2021-06-25 13:00:08'),
('2e1b41232df797e5ca51e37b62e0e3a8668ec5c981902a6f6d18bca997d5ac6f1d787404d276fbe6', 58, 1, 'LaraPassport', '[]', 0, '2020-04-22 06:45:18', '2020-04-22 06:45:18', '2021-04-22 06:45:18'),
('2e410b1a3b90f75e93d5a77ae9c616ba598601b099145bd5d6433fa49db4e6fb3f84e059ca880252', 249, 1, 'LaraPassport', '[]', 0, '2020-09-02 12:59:57', '2020-09-02 12:59:57', '2021-09-02 05:59:57'),
('2e44ed6843f2cd7cab1e655afae980312f3ec0b9dab3947fd6d202180d5fff09a665c1bced124cc0', 33, 1, 'LaraPassport', '[]', 0, '2020-04-18 07:17:51', '2020-04-18 07:17:51', '2021-04-18 07:17:51'),
('2e501f5da2cd8c974d7ec293599d2db8a7aa790f42288a0257bd2aa72a86ea4f25f5e5fba25ace89', 34, 1, 'LaraPassport', '[]', 0, '2020-04-22 04:37:40', '2020-04-22 04:37:40', '2021-04-22 04:37:40'),
('2ea3c4f348150607cd96c2ec5240644d58ded7a528362d896921f0383c19ab92767ebafc0142bb78', 288, 1, 'LaraPassport', '[]', 0, '2020-09-21 17:16:47', '2020-09-21 17:16:47', '2021-09-21 10:16:47'),
('2eb49741fc1852892e281b42e40838504a5bd886e25bec9e6c368a1ce9f7b72533833dab943d4ca3', 35, 1, 'LaraPassport', '[]', 0, '2020-04-13 08:46:48', '2020-04-13 08:46:48', '2021-04-13 08:46:48'),
('2ed1fa308a6afc713d1d0c9c5b55b305ae4a511121d544b9f7b5ae006270e796c05bff1769287675', 8, 1, 'LaraPassport', '[]', 0, '2020-04-09 09:00:04', '2020-04-09 09:00:04', '2021-04-09 09:00:04'),
('2eef1692e88be72224a3cbfcfbdc373d127344927a1bb414b4a1594be83b9113bc139a18ee3d93ea', 35, 1, 'LaraPassport', '[]', 0, '2020-06-27 10:53:26', '2020-06-27 10:53:26', '2021-06-27 10:53:26'),
('2f2f46baccfd7875ffe57a59847b4be2bd9fb8887e594d568a4ccb560e7f6d77ddd31908a86175a4', 35, 1, 'LaraPassport', '[]', 0, '2020-05-19 11:09:18', '2020-05-19 11:09:18', '2021-05-19 11:09:18'),
('2f98aa90e905a83150d28e7ca767ea7da5815e08d67cbeb07a2e38167a39b53358c0286dcda76b61', 62, 1, 'LaraPassport', '[]', 0, '2020-08-18 19:11:25', '2020-08-18 19:11:25', '2021-08-18 12:11:25'),
('2fa0ea0da8d201672f5be62640e2ffe3a57635293c12f2e65fdfe6139a8ce5c8ececc611cb8ce2f1', 266, 1, 'LaraPassport', '[]', 0, '2020-09-08 15:29:24', '2020-09-08 15:29:24', '2021-09-08 08:29:24'),
('2fc06dfc044c8bdf80b10c2da9c3bae0095698e20f5093c007d08564f5cf92a48cf22263911d1c30', 389, 1, 'LaraPassport', '[]', 0, '2020-11-07 23:49:51', '2020-11-07 23:49:51', '2021-11-07 16:49:51'),
('2fed4fd6410c29a56a6d49f28d110fe9c622145ec5628a9ea2e1cb47f2d561b7dccca2cc2a2233b1', 33, 1, 'LaraPassport', '[]', 0, '2020-04-10 09:19:26', '2020-04-10 09:19:26', '2021-04-10 09:19:26'),
('3008dc6fdc728f6c8e868a88ab77abad357bcc7fe100a319e01397193b32c17e52874851947e5d72', 109, 1, 'LaraPassport', '[]', 0, '2020-08-01 18:27:19', '2020-08-01 18:27:19', '2021-08-01 11:27:19'),
('30531598beffcab73ba72151b09b34da69f8f1875ab183c7cd6168d8c8679d0fe9563678bcebca02', 109, 1, 'LaraPassport', '[]', 0, '2021-01-16 02:52:35', '2021-01-16 02:52:35', '2022-01-15 19:52:35'),
('306ec0cbb1594090357cdafc399fd67a06e988ba79b48f53581e2e38299f76efd8ad259e392631d9', 33, 1, 'LaraPassport', '[]', 0, '2020-04-11 10:15:59', '2020-04-11 10:15:59', '2021-04-11 10:15:59'),
('306fa29910ec72b43485598d27fcb3d87dfb710a6186aac8da9d1fdaa633457e003821b8a5ef6831', 35, 1, 'LaraPassport', '[]', 0, '2020-04-10 08:43:03', '2020-04-10 08:43:03', '2021-04-10 08:43:03'),
('31285afa5988f7e50c401ecb234b53ca362582e73d9d4f46fae2bc7337c1c9ce4c4be735a17ff7a2', 35, 1, 'LaraPassport', '[]', 0, '2020-05-27 10:20:41', '2020-05-27 10:20:41', '2021-05-27 10:20:41'),
('3141cda6e310f5e36c5f34e74772ca460262f63078f167a48ee1041019e7584a27e2869cea236560', 33, 1, 'LaraPassport', '[]', 0, '2020-04-16 10:46:31', '2020-04-16 10:46:31', '2021-04-16 10:46:31'),
('3159b941d01f48e10ca7c0b6de33da3c171c1a2bb07c99d51e769df0833a40407d7429406d2a86ac', 33, 1, 'LaraPassport', '[]', 0, '2020-04-10 14:02:54', '2020-04-10 14:02:54', '2021-04-10 14:02:54'),
('31919437cbb8da1dcfd2c8451c149160a9ffccfe997022843c81ccf989b4a28b972abc71213b59d2', 57, 1, 'LaraPassport', '[]', 0, '2020-08-14 17:23:46', '2020-08-14 17:23:46', '2021-08-14 10:23:46'),
('31fbbc3d4c0fddde8111ff301be9a1888a7d71cc456aa6b8bbe64bd2be049719234a5a260319c61b', 33, 1, 'LaraPassport', '[]', 0, '2020-04-10 08:55:57', '2020-04-10 08:55:57', '2021-04-10 08:55:57'),
('31fef6a4b2ed97bb64a0fc387ceb334766829188ff619d19ea1e7f4ac7e03a75b7ded1e64ad02b0c', 153, 1, 'LaraPassport', '[]', 0, '2020-08-24 16:12:30', '2020-08-24 16:12:30', '2021-08-24 09:12:30'),
('3207be621fbd0643144f56a61dce09c0d4fcc35f0c876199704d07f0f8059b5a136fba3286d28ed1', 57, 1, 'LaraPassport', '[]', 0, '2020-07-25 14:40:54', '2020-07-25 14:40:54', '2021-07-25 07:40:54'),
('321498d6a7cbd3be0faaeb3dbec91d1cd89e05117a9a58447ee4f95f860af464369340451b6e6d7b', 35, 1, 'LaraPassport', '[]', 0, '2020-06-21 12:13:45', '2020-06-21 12:13:45', '2021-06-21 12:13:45'),
('324d8ba9151ce5a32ae30eafe7d0e045dc6b72db755826c5d67f80101bdfc6326890b5ab211515cc', 97, 1, 'LaraPassport', '[]', 0, '2020-06-10 06:31:02', '2020-06-10 06:31:02', '2021-06-10 06:31:02'),
('328e0dc062b9ad6191cd44fc588a42825a71ee4b6e656e442ead58e42d958effb9b26d2237bfbdfc', 309, 1, 'LaraPassport', '[]', 0, '2020-09-23 12:51:49', '2020-09-23 12:51:49', '2021-09-23 05:51:49');
INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('32e904be4336945b504076070a3b3af9989074c5d35372bbb495b9b53674534c93c113776d611c23', 126, 1, 'LaraPassport', '[]', 0, '2020-07-05 07:40:00', '2020-07-05 07:40:00', '2021-07-05 07:40:00'),
('3339dc388c0ddcb64404e5a7ff87a583b31e49f1633e9512250079f4b40abc7d379e4863302e0bcb', 280, 1, 'LaraPassport', '[]', 0, '2020-09-09 18:36:17', '2020-09-09 18:36:17', '2021-09-09 11:36:17'),
('334977853f748879559d162b13b4172ea12c0670e5f37007114b2aedca86c5e47f199b4f9f4f7aca', 35, 1, 'LaraPassport', '[]', 0, '2020-05-16 18:31:37', '2020-05-16 18:31:37', '2021-05-16 18:31:37'),
('33502a478cda29c94313fedd3e483c94a2b1d3507d98bd6087ae6c939f1b5f83d507b06bc4a65a39', 109, 1, 'LaraPassport', '[]', 0, '2021-01-15 19:28:54', '2021-01-15 19:28:54', '2022-01-15 12:28:54'),
('33e04ca5ffc7f3d8d77531dc33e67e408791effe0cd26abe62ece397fe5d09e5e81a412418b34348', 8, 1, 'LaraPassport', '[]', 0, '2020-04-10 06:17:47', '2020-04-10 06:17:47', '2021-04-10 06:17:47'),
('3417e49a901c715ef511cfe64162db1b1b20ca63f068be695b17ffe464d55058176c7c069e6c0582', 58, 1, 'LaraPassport', '[]', 0, '2020-04-22 08:58:52', '2020-04-22 08:58:52', '2021-04-22 08:58:52'),
('346621f94eae14f6079445a865d76c4ace1776ac7b63f376486349cf5bd12c98f5690d127857b055', 249, 1, 'LaraPassport', '[]', 0, '2020-08-31 13:03:00', '2020-08-31 13:03:00', '2021-08-31 06:03:00'),
('34a582e9681352ec3b3422643d94cd045bc25cafb630b9dab1ec320992ee5aea644929946ad222ec', 352, 1, 'LaraPassport', '[]', 0, '2020-10-10 20:15:18', '2020-10-10 20:15:18', '2021-10-10 13:15:18'),
('34ec789c75e10d4e2d8cc5bdb325e9793e38b937eb4080ee8c0faea8ea1ed8620fa852844f74819a', 266, 1, 'LaraPassport', '[]', 0, '2020-09-15 13:29:47', '2020-09-15 13:29:47', '2021-09-15 06:29:47'),
('3559bd4398a0137905777352cd82d1f5a9735774084b1b36751b602807bf5ca4ba1e50ab1fe5abca', 8, 1, 'LaraPassport', '[]', 0, '2020-04-09 12:41:02', '2020-04-09 12:41:02', '2021-04-09 12:41:02'),
('35b324541fc22cbe6089150b39380f4aa789f657b969b0dec73d9a80926c2e4989108ab942f6e0af', 58, 1, 'LaraPassport', '[]', 0, '2020-04-22 06:42:48', '2020-04-22 06:42:48', '2021-04-22 06:42:48'),
('361a8b0656df2fd08a8bc1f9f6c5c6cd1adca262ae78c4009e5410e8f228e44daea77a9e62a34921', 57, 1, 'LaraPassport', '[]', 0, '2020-08-14 13:50:27', '2020-08-14 13:50:27', '2021-08-14 06:50:27'),
('363d9869e831cc7accbdb4c2e402c59a46cc052051a178405dee24ad1a2796d1186965fc93b99968', 33, 1, 'LaraPassport', '[]', 0, '2020-04-11 08:03:38', '2020-04-11 08:03:38', '2021-04-11 08:03:38'),
('367185821beff781e326d3ff3960f64277dd8f814e814fb21ca073b2080828adb187ccfb63cc986d', 50, 1, 'LaraPassport', '[]', 0, '2020-04-17 09:53:59', '2020-04-17 09:53:59', '2021-04-17 09:53:59'),
('371ce21a163eb3675c52918d6725920ff4fa03b0b49acb7bd9ceb668b0c1c0f2fea2dd3ccfadf3ab', 57, 1, 'LaraPassport', '[]', 0, '2020-05-02 04:03:24', '2020-05-02 04:03:24', '2021-05-02 04:03:24'),
('37254c9a541d2d7d1e36ae3faac7d7adbe8f5859c411daee387e941e826acd828b5baa30080bb014', 39, 1, 'LaraPassport', '[]', 0, '2021-01-13 00:48:24', '2021-01-13 00:48:24', '2022-01-12 17:48:24'),
('374df4bdbea830813591d8de965922296880ce89741c3e7910e48136f78cffb617798d290921b0de', 35, 1, 'LaraPassport', '[]', 0, '2020-07-28 12:27:01', '2020-07-28 12:27:01', '2021-07-28 05:27:01'),
('377120e06e7a9122b8496293425e3a1d4342146dd47b786256430b67376986747369ea5914e62e0f', 119, 1, 'LaraPassport', '[]', 0, '2021-01-19 22:46:08', '2021-01-19 22:46:08', '2022-01-19 15:46:08'),
('377523d56c654cb354e99cf1e093f7271943badf9035fe04f2631415dc6de2e832276362e0a487c1', 57, 1, 'LaraPassport', '[]', 0, '2020-08-18 12:05:47', '2020-08-18 12:05:47', '2021-08-18 05:05:47'),
('37c64ccf411fec288d024455261b3b2be21b406b6438b49e7610119fd5d36345c83b64f163f302b3', 8, 1, 'LaraPassport', '[]', 0, '2020-04-09 13:10:23', '2020-04-09 13:10:23', '2021-04-09 13:10:23'),
('37f575cfc7a4e23d42d09147491fb9a5d33000c68838b0d24cc7897d386ce81ea28455fd6fe28133', 498, 1, 'LaraPassport', '[]', 0, '2021-01-07 22:59:29', '2021-01-07 22:59:29', '2022-01-07 15:59:29'),
('3804e70069eaca43b8884e68f3e69c39d3bd7fe6ffb0871db7001d2ce9179dd69604029f4036ff8f', 109, 1, 'LaraPassport', '[]', 0, '2020-08-01 18:33:07', '2020-08-01 18:33:07', '2021-08-01 11:33:07'),
('380c09d1402c936447fbf718fdf2c4dc232cf9d1ba0eab6425061b12ab7afec1c244a33e7d45647e', 316, 1, 'LaraPassport', '[]', 0, '2021-01-06 18:41:36', '2021-01-06 18:41:36', '2022-01-06 11:41:36'),
('384ff961a2cc4df1fd8b64227302fbec2de7112568ea76adfa9a45e27622d91b1af5bef38359cdf6', 109, 1, 'LaraPassport', '[]', 0, '2021-01-18 19:56:05', '2021-01-18 19:56:05', '2022-01-18 12:56:05'),
('3853e6a7194cad0add36c72d174bc7a207f2e08090f1b9d88bae8d99fe03ae3c5e6dae472259d86b', 309, 1, 'LaraPassport', '[]', 0, '2020-10-06 16:26:49', '2020-10-06 16:26:49', '2021-10-06 09:26:49'),
('3898e37f3a722d2f2d4a9d04fb129e0dc828b0b3a0152f8ebeff8519de88314a3428a5bd26978cb7', 108, 1, 'LaraPassport', '[]', 0, '2020-06-12 10:48:22', '2020-06-12 10:48:22', '2021-06-12 10:48:22'),
('390c016b645b31728e1e27b6262d59bfa611c556bb64aabb3da00df6190b1c314a66f97d6ae912f8', 35, 1, 'LaraPassport', '[]', 0, '2020-05-28 12:35:41', '2020-05-28 12:35:41', '2021-05-28 12:35:41'),
('39845d8a16b7a9a5c6508f6b2f9339b76a8b4b02b2ea0575562c2da4fa77f9252e80d6b4e0879701', 51, 1, 'LaraPassport', '[]', 0, '2020-04-17 09:54:38', '2020-04-17 09:54:38', '2021-04-17 09:54:38'),
('3a09cfeaca013e239eb8c40fd4563914dfdc6f9975e06e85a4855d2db5e5cea40f499261bc99dab0', 29, 1, 'LaraPassport', '[]', 0, '2020-04-10 06:38:56', '2020-04-10 06:38:56', '2021-04-10 06:38:56'),
('3a3fe05dea1fd80100e555ea1245f909a96234a3a6eb9dc0c9a2be9976afc52cd1b12b6da6b820e7', 389, 1, 'LaraPassport', '[]', 0, '2020-11-12 17:54:42', '2020-11-12 17:54:42', '2021-11-12 10:54:42'),
('3a4063bedd8f5275adb656e7e1a5c590e9bba7b69105bfb91a784e329d80f443c524dfa25ab0c2d3', 33, 1, 'LaraPassport', '[]', 0, '2020-04-13 09:58:13', '2020-04-13 09:58:13', '2021-04-13 09:58:13'),
('3a68c45a209b1170a81d57bdbcdf3edf523262614fa2f7427af3e19a8b30b56cd21b40d9f083a0c8', 3, 1, 'LaraPassport', '[]', 0, '2020-03-18 23:24:14', '2020-03-18 23:24:14', '2021-03-19 04:54:14'),
('3ac18fbe09aaeb71a68500ecff4fe4448f8b91dcfacc74b2ea1e5ba7d5c0d8246040c32114853258', 65, 1, 'LaraPassport', '[]', 0, '2020-05-19 15:40:05', '2020-05-19 15:40:05', '2021-05-19 15:40:05'),
('3ad1875138f40f1c45b57059f1e54cfb7325818f9514ce8d7335056530f742bc2a3c5e4140787816', 104, 1, 'LaraPassport', '[]', 0, '2020-06-11 12:58:31', '2020-06-11 12:58:31', '2021-06-11 12:58:31'),
('3b05eb2625738193543149b92975517aa979213e5c114cfbeebd67ced3d8fb1096e507aff53b70ce', 3, 1, 'LaraPassport', '[]', 0, '2020-03-19 00:43:37', '2020-03-19 00:43:37', '2021-03-19 06:13:37'),
('3b63546e5230e37c5c9685b74c4d3fe40e6e11c9599c9fbce10328a7c36117c652a2d8d0f0175f96', 57, 1, 'LaraPassport', '[]', 0, '2020-08-01 16:49:10', '2020-08-01 16:49:10', '2021-08-01 09:49:10'),
('3bc4bb7eb9643661514485acd4949e31cb165478d65cb0694b5afdd25dfc64d4f714b1c1bdfe555f', 239, 1, 'LaraPassport', '[]', 0, '2020-08-24 16:30:11', '2020-08-24 16:30:11', '2021-08-24 09:30:11'),
('3bf4bb5913d1f06eb6d5e994de7a321f21f34c5ba9e64f217418c3e4ba0903d615eafad2d62929fd', 357, 1, 'LaraPassport', '[]', 0, '2020-10-14 13:32:39', '2020-10-14 13:32:39', '2021-10-14 06:32:39'),
('3c250c07c1b61f6076c724922455ad11c7b5acd696d02bab166ec63fee61f3866a174f6c3c710e32', 35, 1, 'LaraPassport', '[]', 0, '2020-05-30 05:31:30', '2020-05-30 05:31:30', '2021-05-30 05:31:30'),
('3c31c634316ad36939f35be649a2400b4ba90e874957f1f4c154a4abd33e44096a1f20557aa059d6', 195, 1, 'LaraPassport', '[]', 0, '2020-09-21 03:45:31', '2020-09-21 03:45:31', '2021-09-20 20:45:31'),
('3c96bdc20a9101280b9ff0e20151e74bfd24816b0bb46d3d91cc7a77080b8b92cea35ab95439dbeb', 65, 1, 'LaraPassport', '[]', 0, '2020-05-19 15:40:12', '2020-05-19 15:40:12', '2021-05-19 15:40:12'),
('3cce66251956c34a29644eeae916b906f65eab10ee0965fb8d9ff6c9907c08d529e2327ac056e2cb', 8, 1, 'LaraPassport', '[]', 0, '2020-04-10 04:42:18', '2020-04-10 04:42:18', '2021-04-10 04:42:18'),
('3cedc744b391fda3d48c8aa148401f313706fcacb8bd0f111dd826d4922a19399e1cb0ff733577bb', 35, 1, 'LaraPassport', '[]', 0, '2020-05-17 20:38:03', '2020-05-17 20:38:03', '2021-05-17 20:38:03'),
('3cefc50d8b706818f3ed2379293370432445d6574b0bdbf31f9e22cad55fe96a0920de975db303bc', 33, 1, 'LaraPassport', '[]', 0, '2020-04-14 06:14:37', '2020-04-14 06:14:37', '2021-04-14 06:14:37'),
('3d0ac4d2f7aedcfa547f4d840b084a7270caafa345dcafe40704fccb4cfe5afd876878bf157e03c4', 57, 1, 'LaraPassport', '[]', 0, '2020-05-07 10:41:52', '2020-05-07 10:41:52', '2021-05-07 10:41:52'),
('3d2c358004b8fa497bed45cc4605eebf3235827b13794fcb76c0e1bdb9b6ba6e4e0fea2088e6e407', 321, 1, 'LaraPassport', '[]', 0, '2020-09-28 14:27:44', '2020-09-28 14:27:44', '2021-09-28 07:27:44'),
('3d8ef76e7dd3067b5aae213e428e1e95bb41aac33be9d8b476c154ba828ae85f131e1e38edf10dc8', 28, 1, 'LaraPassport', '[]', 0, '2020-04-09 08:59:32', '2020-04-09 08:59:32', '2021-04-09 08:59:32'),
('3df4f8f4e833943852c0dfa57b0b09c54a2644b144d52f032989c15ccba2c036a40acb4afe801575', 104, 1, 'LaraPassport', '[]', 0, '2020-06-12 04:41:07', '2020-06-12 04:41:07', '2021-06-12 04:41:07'),
('3e5d6f0f5d482368d85f7a4cf7af259cc7ac5c91fafcd06e528d6688410083dc8b8fd98e190adb90', 300, 1, 'LaraPassport', '[]', 0, '2020-09-30 19:23:26', '2020-09-30 19:23:26', '2021-09-30 12:23:26'),
('3ebf1e0d4f5f64ec8676f7633bb65e04a8845dad27e13b1f5b0ca2b4ff97eb6e0023a1a7c7e8ee5e', 33, 1, 'LaraPassport', '[]', 0, '2020-04-21 12:44:10', '2020-04-21 12:44:10', '2021-04-21 12:44:10'),
('3fe1e199418fab5d764fe05d623c19800d53d0a5109f4e9713ab0c6401ebb560be5eda45f20518d7', 125, 1, 'LaraPassport', '[]', 0, '2020-07-05 07:27:34', '2020-07-05 07:27:34', '2021-07-05 07:27:34'),
('400468766e11efdb124ad20510ca95e253cba8225989f535cec13b5f8b45e8047956f1773b6cb54f', 57, 1, 'LaraPassport', '[]', 0, '2020-08-21 12:43:16', '2020-08-21 12:43:16', '2021-08-21 05:43:16'),
('40e050f134ab8b1d02be8be7400ed692184c8bd17310848683761382c6adcfffbd85801f43100b14', 107, 1, 'LaraPassport', '[]', 0, '2020-06-12 06:23:21', '2020-06-12 06:23:21', '2021-06-12 06:23:21'),
('411c9c9cc330b7e5d46dcab88c1e6b3c5e1ffa62df8c43251b924d632e0b90faa547281e9b269b7c', 57, 1, 'LaraPassport', '[]', 0, '2020-06-22 11:29:17', '2020-06-22 11:29:17', '2021-06-22 11:29:17'),
('4174d0971413a7a19a3283d221a8fa63771d4c17c9c0ddb194746e6fdbb3a47b782739d7de44e23b', 33, 1, 'LaraPassport', '[]', 0, '2020-04-10 09:18:47', '2020-04-10 09:18:47', '2021-04-10 09:18:47'),
('41cb7bcc00d1dac0378264fa1a72eaabc1f22866783f0f26a4abad5cd336dc7214cf70f89e886763', 309, 1, 'LaraPassport', '[]', 0, '2020-09-27 16:32:14', '2020-09-27 16:32:14', '2021-09-27 09:32:14'),
('41e535ee4cbdec583a5713ef45aa50979361ecee97189ee925e02424163c97e0252b3b937b07757a', 120, 1, 'LaraPassport', '[]', 0, '2020-06-27 11:09:41', '2020-06-27 11:09:41', '2021-06-27 11:09:41'),
('41f3fe0759d1d489f4e6ae1dbf7ed0ad7687b602b63bd657193185d4ebae7ee5a4065ec47af41d64', 57, 1, 'LaraPassport', '[]', 0, '2020-07-02 08:14:15', '2020-07-02 08:14:15', '2021-07-02 08:14:15'),
('4207050e94f9f8df91ddebafdba7332fc1810b982351ef4bb371316e1ae14ed35cc47e7c872a3d0d', 35, 1, 'LaraPassport', '[]', 0, '2020-07-31 16:07:44', '2020-07-31 16:07:44', '2021-07-31 09:07:44'),
('42469f87fc57466fd9e858667647656f935197052ce4295c1bc1fdf3ed0af6bff236d788ac71fde2', 57, 1, 'LaraPassport', '[]', 0, '2020-08-01 16:18:59', '2020-08-01 16:18:59', '2021-08-01 09:18:59'),
('425f9fa95816360178b944d638aa1f29d06cac22af945ecb79677c5f32623257645ced3ab9b9a595', 35, 1, 'LaraPassport', '[]', 0, '2020-05-17 18:41:26', '2020-05-17 18:41:26', '2021-05-17 18:41:26'),
('42763e76eba7d4607bb51a6f82a6a37b9719e86126d55c8f093971e13b4f3387fd06c0a97361d4b2', 8, 1, 'LaraPassport', '[]', 0, '2020-04-09 10:12:46', '2020-04-09 10:12:46', '2021-04-09 10:12:46'),
('42850569b51a759eea7e9cf3e88082d89be93ae251756d3acd1f15ee58a00059de9cba5caaaff46c', 280, 1, 'LaraPassport', '[]', 0, '2020-09-09 18:35:12', '2020-09-09 18:35:12', '2021-09-09 11:35:12'),
('42ef9a1ed77fcb382f352b8d135b840121badc42a3cc2b1c4c9bbf36994c1c83b50adc0bc825f3e7', 193, 1, 'LaraPassport', '[]', 0, '2020-08-15 19:32:00', '2020-08-15 19:32:00', '2021-08-15 12:32:00'),
('43382c402fe2821ee4a972639919386c67b73afa4b5f1be83649b25b6de83f1d15eae50e64641812', 416, 1, 'LaraPassport', '[]', 0, '2020-11-22 19:40:54', '2020-11-22 19:40:54', '2021-11-22 12:40:54'),
('43839d3bc7044cbb245d0e76f6cfde7c20dc45085a308e4f61e046d1193fac10308c088d10c13fcc', 57, 1, 'LaraPassport', '[]', 0, '2020-04-30 12:52:25', '2020-04-30 12:52:25', '2021-04-30 12:52:25'),
('438daf1eb4c1d0024d15b0eebee735d62ae3d04d19fee2de9cd9bee938ba62e02b609a9128e493c0', 197, 1, 'LaraPassport', '[]', 0, '2020-08-17 14:17:54', '2020-08-17 14:17:54', '2021-08-17 07:17:54'),
('441c56bb45ad76404d27e020521e4345871a8e9758560c9fbfc49e110af8b3c592de65c3904379bf', 316, 1, 'LaraPassport', '[]', 0, '2020-12-27 20:31:39', '2020-12-27 20:31:39', '2021-12-27 13:31:39'),
('4422817ceb65cd2e9454f7cbd137e35d1458b0e286edb96b0202ee924257cff1f648609082465624', 35, 1, 'LaraPassport', '[]', 0, '2020-06-27 07:28:24', '2020-06-27 07:28:24', '2021-06-27 07:28:24'),
('443f2b020f4d4bdafb19bd9dc60fbc3957764d4e346a8ac43e1c9ca41fb5a0c3346b33b5794bb00d', 59, 1, 'LaraPassport', '[]', 0, '2020-04-23 04:50:56', '2020-04-23 04:50:56', '2021-04-23 04:50:56'),
('445c2b8587ce7d9edb72daa95e81ad6002ffb1c1350d8867648c0c1f573a3cdd800d96ddd801d7d2', 357, 1, 'LaraPassport', '[]', 0, '2020-10-14 14:17:12', '2020-10-14 14:17:12', '2021-10-14 07:17:12'),
('44739d54ad746e5ed4fe517e7665faa7df3e671c545ae37710648455e8b7346acc8d9facb8765f4d', 316, 1, 'LaraPassport', '[]', 0, '2020-10-13 18:07:39', '2020-10-13 18:07:39', '2021-10-13 11:07:39'),
('44c3c64373f9e494f11011a32d994465682cc8d5739a0bd652f9dfea5b5f6bac43298659d4694ff5', 291, 1, 'LaraPassport', '[]', 0, '2020-09-11 10:51:34', '2020-09-11 10:51:34', '2021-09-11 03:51:34'),
('44f2ea3308e9343f2f0d488e393f75b5e0329e96eec2df3071a997b44846949d51a4461211a21b57', 58, 1, 'LaraPassport', '[]', 0, '2020-05-07 11:03:05', '2020-05-07 11:03:05', '2021-05-07 11:03:05'),
('44fdf5dc9f3326e3bc9c7b4cc61a45aecd2a0c020860f8a55d02ddeb5919aec527d51ac3b822b076', 201, 1, 'LaraPassport', '[]', 0, '2020-08-18 02:00:16', '2020-08-18 02:00:16', '2021-08-17 19:00:16'),
('4507cbc137c707b6fcf45fff253da43f89b0f467b9ba05982ce141c4ec359249de38686f0c9f8301', 35, 1, 'LaraPassport', '[]', 0, '2020-05-12 05:48:13', '2020-05-12 05:48:13', '2021-05-12 05:48:13'),
('45140ec5d4faebc65feafd2789911ec037e88c61f83570720acfa8ded552f21b68c6f2e2ea7d12fd', 119, 1, 'LaraPassport', '[]', 0, '2021-01-19 21:06:41', '2021-01-19 21:06:41', '2022-01-19 14:06:41'),
('452dfddba60c2dfa729f74357f4d6ab2cf7a5ef964a7f165b7db3632e611df42fa9493531c41dcf0', 231, 1, 'LaraPassport', '[]', 0, '2020-10-09 18:29:41', '2020-10-09 18:29:41', '2021-10-09 11:29:41'),
('455e3518d437bc148dd881b207d5fc64bfd2c31e2a8d47b8cd94aaf095a32d845614cb2d8b93ed6b', 262, 1, 'LaraPassport', '[]', 0, '2020-09-05 23:01:43', '2020-09-05 23:01:43', '2021-09-05 16:01:43'),
('4571f4e6775a10f61817de21efcd6cf6d403e5198693bfdf7391e5c86f005a67fd90c0a2e314951a', 227, 1, 'LaraPassport', '[]', 0, '2020-08-19 14:41:52', '2020-08-19 14:41:52', '2021-08-19 07:41:52'),
('45e0df3bce1a6c2818605194fe03d6fdbdb0b37476fad27e51443672207d9363bb9ba16b98169c03', 234, 1, 'LaraPassport', '[]', 0, '2020-08-21 14:03:10', '2020-08-21 14:03:10', '2021-08-21 07:03:10'),
('463788a1a4e21caf95c1322bd99388de3b3304141f99e20f935b8bfcbc0018ab73f033c4facc12c6', 57, 1, 'LaraPassport', '[]', 0, '2020-07-05 18:42:21', '2020-07-05 18:42:21', '2021-07-05 18:42:21'),
('46750e8a97d9f62fd64937665cbd5f455c818aba5c5867ef3b41fcaf85ec8b5c367dd2b9b1832914', 327, 1, 'LaraPassport', '[]', 0, '2020-09-30 19:36:49', '2020-09-30 19:36:49', '2021-09-30 12:36:49'),
('468ce38e370279c37ffca59ffa81211547d60119b92959d2f6f4a3323db82084646d15140de92df4', 196, 1, 'LaraPassport', '[]', 0, '2020-08-17 14:06:03', '2020-08-17 14:06:03', '2021-08-17 07:06:03'),
('46c11716b458552ddbabb511a0cac56ea02831acc8446af43e18f82142de9ad326b04fd6be82e97a', 358, 1, 'LaraPassport', '[]', 0, '2020-10-14 16:40:35', '2020-10-14 16:40:35', '2021-10-14 09:40:35'),
('46d70846f535eb791c812f9921f2c38d354ba2e24a7b5daa5bac70762eb64012994e89c0855e9191', 431, 1, 'LaraPassport', '[]', 0, '2020-12-15 23:59:01', '2020-12-15 23:59:01', '2021-12-15 16:59:01'),
('47144ad0c35e1e154495c321718c4d8569955d8f3866ed0867c0a2ae2735b8f3457e45f7c9d0479e', 35, 1, 'LaraPassport', '[]', 0, '2020-08-01 14:46:45', '2020-08-01 14:46:45', '2021-08-01 07:46:45'),
('4777f5e3b101a1a987ff845e94da291ddf4b954f7ba14d36826f2f45e40c7352a3fd24c23d942ff4', 109, 1, 'LaraPassport', '[]', 0, '2020-08-17 19:01:45', '2020-08-17 19:01:45', '2021-08-17 12:01:45'),
('478321c07a13e19dd96b0eedc8030bf3849f6bae190add54ef2bae1662334f1f164650f9fe03dbcd', 33, 1, 'LaraPassport', '[]', 0, '2020-04-10 11:24:42', '2020-04-10 11:24:42', '2021-04-10 11:24:42'),
('47d3fd557125f18f2a0cdcc6c20ee544ce9beba099c4512b4131970c8fe2759b987215d0e5034824', 35, 1, 'LaraPassport', '[]', 0, '2020-06-25 13:10:21', '2020-06-25 13:10:21', '2021-06-25 13:10:21'),
('47e08550e96e4404daba76b2ac342b1037fb4d69a0c6b2c2c76e5b3d11142c74032d46579dc1e6ee', 266, 1, 'LaraPassport', '[]', 0, '2020-09-09 18:47:38', '2020-09-09 18:47:38', '2021-09-09 11:47:38'),
('47f8ca5a8cf9ba797b5bc70a985539d1811887d270183364c4ea48922474d52cd8caef277a0c6fdb', 57, 1, 'LaraPassport', '[]', 0, '2020-07-30 18:09:55', '2020-07-30 18:09:55', '2021-07-30 11:09:55'),
('48338f00d80fed8f71e543a8ec052ef6a641370e4d480425f8aa4f0b5a4ba142652d4d16bf7e7c32', 237, 1, 'LaraPassport', '[]', 0, '2020-09-03 16:02:17', '2020-09-03 16:02:17', '2021-09-03 09:02:17'),
('483fc7cce8752f04453da8b3a02d64b19ee73c873570666c2122da86aed906d50a56bf25e782c022', 35, 1, 'LaraPassport', '[]', 0, '2020-08-04 16:53:39', '2020-08-04 16:53:39', '2021-08-04 09:53:39'),
('488c7bca7a9ff25ca4fcb831c5271f9d4aa209cbb287e4f09245cc3bb422bfc93746e3bddc7ce19b', 62, 1, 'LaraPassport', '[]', 0, '2020-04-27 14:41:04', '2020-04-27 14:41:04', '2021-04-27 14:41:04'),
('4909f09565f842c42b2420aba796e1b5af5397267cf5f8e698c1857973bfe2feb6d4ebe42340ed08', 247, 1, 'LaraPassport', '[]', 0, '2020-09-05 20:45:15', '2020-09-05 20:45:15', '2021-09-05 13:45:15'),
('491081a2c8d4a76a533f0376d763aa01989097e29c6fce4cb3807918aef8cd70d6fe38d9eaeba2e4', 300, 1, 'LaraPassport', '[]', 0, '2021-01-06 18:40:36', '2021-01-06 18:40:36', '2022-01-06 11:40:36'),
('4938cbd94730bb02f5129e3a7d4ede5145cac2c89a1917b3e180acc245968ee99747535fea1d0160', 126, 1, 'LaraPassport', '[]', 0, '2020-07-31 19:17:23', '2020-07-31 19:17:23', '2021-07-31 12:17:23'),
('4957a4bc0cf49c3d5a871cf715e30eadd827373c7cf6071531d05912c21c21acd3a5eb8b4dc7ee63', 116, 1, 'LaraPassport', '[]', 0, '2020-06-27 10:56:39', '2020-06-27 10:56:39', '2021-06-27 10:56:39'),
('4977efb8e27a8dac2573c37f07d88595fdc7a4f383c3dfc53e14318b7f06ccd5cbcd5d1b28d87338', 320, 1, 'LaraPassport', '[]', 0, '2020-09-26 18:11:20', '2020-09-26 18:11:20', '2021-09-26 11:11:20'),
('49b42458fe0f95d1a5c5d9f6fc643150f0eb5eeba801f37d5c9d7cda63264e0c172b138577bfafd0', 33, 1, 'LaraPassport', '[]', 0, '2020-04-11 05:31:34', '2020-04-11 05:31:34', '2021-04-11 05:31:34'),
('49cd48589ad4c1c91698c59df8368df874d5163c51fb36c3afb50d0941db9228259109dbd88629c9', 57, 1, 'LaraPassport', '[]', 0, '2020-06-27 13:18:38', '2020-06-27 13:18:38', '2021-06-27 13:18:38'),
('49f853367b5aaed66f12c59c3ef27773890604c673b940b6f369ec1eb2195b1d32f7765f389a2245', 434, 1, 'LaraPassport', '[]', 0, '2020-12-19 21:32:59', '2020-12-19 21:32:59', '2021-12-19 14:32:59'),
('49fef842e8cf209ec1c3c64b1885abf37b49176e48a98e976e9dc06cd49679df93fd895737c7c3cd', 33, 1, 'LaraPassport', '[]', 0, '2020-04-16 10:31:54', '2020-04-16 10:31:54', '2021-04-16 10:31:54'),
('4a1170bd3586e7c33f6b6c7024def85095fbbe9b511646eccde3e75fcb9b461c147b9d291d94c08a', 57, 1, 'LaraPassport', '[]', 0, '2020-08-01 17:01:17', '2020-08-01 17:01:17', '2021-08-01 10:01:17'),
('4a2a2b50e694e64ac735e6ea70fa06725e91665066952db930797f4c34871c78f4cc1126fb83db3b', 109, 1, 'LaraPassport', '[]', 0, '2020-07-05 07:11:45', '2020-07-05 07:11:45', '2021-07-05 07:11:45'),
('4a9133bfbddd0baf4671c50e5af92b94f802b5dbcc842fd49c96dfd6219fe3cc07a124d05c00972a', 33, 1, 'LaraPassport', '[]', 0, '2020-04-18 04:57:38', '2020-04-18 04:57:38', '2021-04-18 04:57:38'),
('4aa9b1a17b0f8c9ae0650d426914608269773329da37841cbfaf303ca44a65217ada49865826d54a', 35, 1, 'LaraPassport', '[]', 0, '2020-06-08 07:54:17', '2020-06-08 07:54:17', '2021-06-08 07:54:17'),
('4af729b608459be24c97ecc290218a8dae04058735a3e4d0f557fa40fc0366ffca3066bd15105412', 430, 1, 'LaraPassport', '[]', 0, '2020-12-15 18:32:26', '2020-12-15 18:32:26', '2021-12-15 11:32:26'),
('4b24dea33f96f6c372de99edb4d6b31f62492c7bdd01bd89379fd3dc641996e736058705c00f8c20', 244, 1, 'LaraPassport', '[]', 0, '2020-10-09 20:38:15', '2020-10-09 20:38:15', '2021-10-09 13:38:15'),
('4b37d2f18d480fec144b04db7df142efd29e4660b14edc96b03095228524d0f829dfcbd5a77b0416', 195, 1, 'LaraPassport', '[]', 0, '2020-08-17 00:31:02', '2020-08-17 00:31:02', '2021-08-16 17:31:02'),
('4b5638984df6059fd7932722dc34bd20a6129ade25ce5a7382dd6b7aecceac733a420472d4fc8e2d', 33, 1, 'LaraPassport', '[]', 0, '2020-04-14 10:31:14', '2020-04-14 10:31:14', '2021-04-14 10:31:14'),
('4b659de2c6bf112968f71547053d21fd584aaef674960d38f5ae17a9d51cfc0a8fef8218f82a398d', 309, 1, 'LaraPassport', '[]', 0, '2020-10-13 18:59:12', '2020-10-13 18:59:12', '2021-10-13 11:59:12'),
('4b66e40fb92ded66e3636016089e6cd7785189b673aaa1008abd7e06a424c57053eeac933890d9b3', 249, 1, 'LaraPassport', '[]', 0, '2020-09-30 18:00:24', '2020-09-30 18:00:24', '2021-09-30 11:00:24'),
('4b74d30489755dc1bf1141d78450a8d1d61463bd73f7f9758238c2f0635c302e16aa5225de012718', 142, 1, 'LaraPassport', '[]', 0, '2020-07-28 12:21:04', '2020-07-28 12:21:04', '2021-07-28 05:21:04'),
('4b8d20cabe511d61b9a387165631a3489e2f489b185b1f12bc4f0a7b5e2e6c5fb1c73e5d0af79c35', 249, 1, 'LaraPassport', '[]', 0, '2020-08-31 17:45:45', '2020-08-31 17:45:45', '2021-08-31 10:45:45'),
('4b9f00e1bef6ace253a6e5297416314c722cf974bd81d57288977192ca7932364d821a361f8c5c2b', 153, 1, 'LaraPassport', '[]', 0, '2020-08-27 00:28:26', '2020-08-27 00:28:26', '2021-08-26 17:28:26'),
('4ba560f3a634d4c4dd630405c72b37b27e1c841dc1b20f7921d57d75c9781de33007e396e838d1fe', 35, 1, 'LaraPassport', '[]', 0, '2020-05-18 03:55:19', '2020-05-18 03:55:19', '2021-05-18 03:55:19'),
('4bb6fc07413c3b6d16395b4e0c83ab295f128a2256050e63cec16d14e053a7880497790d944cd135', 223, 1, 'LaraPassport', '[]', 0, '2020-08-19 14:19:45', '2020-08-19 14:19:45', '2021-08-19 07:19:45'),
('4bda95eb98dc3f0290ef9cbb758a5dfdb2d25e717f9d95d8cbdaeeac9f7dc5d02807dded28e4e125', 35, 1, 'LaraPassport', '[]', 0, '2020-08-04 13:14:39', '2020-08-04 13:14:39', '2021-08-04 06:14:39'),
('4c00c89415b8bf7cea1ac1dee86afc66f76f3e377b52a6a5d525017bc57616599747d7d5d966f71b', 57, 1, 'LaraPassport', '[]', 0, '2020-07-03 18:34:33', '2020-07-03 18:34:33', '2021-07-03 18:34:33'),
('4c01be57e88cb415c9cdf5052a838e84903a5813e2f17da8dea606de1f33ad93cc33303fe292e631', 33, 1, 'LaraPassport', '[]', 0, '2020-04-18 07:17:51', '2020-04-18 07:17:51', '2021-04-18 07:17:51'),
('4c4ebb1ec57f953c2d2bdaa77d02b70333105b2d41499c96e016a07251e8c1caf432d234492b7402', 35, 1, 'LaraPassport', '[]', 0, '2020-06-27 10:52:00', '2020-06-27 10:52:00', '2021-06-27 10:52:00'),
('4c54c3291d11ec7424229691f05b944f7856ee0525e0a5fb13e87390eecbaf725cb2361df2bc8e4a', 417, 1, 'LaraPassport', '[]', 0, '2020-11-22 20:37:46', '2020-11-22 20:37:46', '2021-11-22 13:37:46'),
('4c7b9828b1f21e08444d9f07622b5b67e06cd273059942f4230a095480baf86cc64c5c6219303fad', 217, 1, 'LaraPassport', '[]', 0, '2020-08-18 20:54:03', '2020-08-18 20:54:03', '2021-08-18 13:54:03'),
('4cb5856c11676efb2f0dac1ddf7899c948de90e8886a148c40bdafbfee7ee2633410495ba2cd26f0', 57, 1, 'LaraPassport', '[]', 0, '2020-05-01 04:14:44', '2020-05-01 04:14:44', '2021-05-01 04:14:44'),
('4ce68e79b4248470c879becf27dc841eb258a7cbf40324c40c1d2f2bdbecfd1513a0e2b7a746cc5c', 33, 1, 'LaraPassport', '[]', 0, '2020-04-20 13:17:00', '2020-04-20 13:17:00', '2021-04-20 13:17:00'),
('4d241c12d4892985e1daf449f60d78d96ba7230f47c99ad4035fc29b174def049b587a15510d7d75', 57, 1, 'LaraPassport', '[]', 0, '2020-06-22 04:59:19', '2020-06-22 04:59:19', '2021-06-22 04:59:19'),
('4d4f9f7d411c2cf73e3b90aa5e52cb72d62a279f30fe85e4085efaf50366bc1af8302ed35cf54b34', 201, 1, 'LaraPassport', '[]', 0, '2020-08-18 01:58:25', '2020-08-18 01:58:25', '2021-08-17 18:58:25'),
('4d56cbf0360629c78903d7bd48fcbdecef429e409571cf3112eb7e7c2c51b3dac618ad95353fa867', 63, 1, 'LaraPassport', '[]', 0, '2020-04-27 06:44:47', '2020-04-27 06:44:47', '2021-04-27 06:44:47'),
('4d7cad5e864807daf3d97b301f3b124b4f378a884e956533abe88a3ea238d20ba168bfc38f715fc2', 8, 1, 'LaraPassport', '[]', 0, '2020-04-09 12:59:13', '2020-04-09 12:59:13', '2021-04-09 12:59:13'),
('4dd781f443b3e8c831c161493a458355b238bd1768f9b75f54eadaa9181cd6ef212f12001d9758ca', 35, 1, 'LaraPassport', '[]', 0, '2020-06-27 10:30:06', '2020-06-27 10:30:06', '2021-06-27 10:30:06'),
('4de97dc403a5f18c559f02358d7509a734187eb4e9a2366863fdef66450e49d4eb2945b238988acd', 247, 1, 'LaraPassport', '[]', 0, '2020-08-29 22:27:13', '2020-08-29 22:27:13', '2021-08-29 15:27:13'),
('4dfa5dd5c53adb4abca81342ad32de3ce89bd1474d6ff9c462ff355e2e606dee16c780f54af97508', 304, 1, 'LaraPassport', '[]', 0, '2020-09-17 00:18:47', '2020-09-17 00:18:47', '2021-09-16 17:18:47'),
('4dfabc4031741d0fda48b2f338b96e8be998a446cc8793d6af8900c5ac105f7b5528d4f31ead21de', 57, 1, 'LaraPassport', '[]', 0, '2020-04-28 05:43:07', '2020-04-28 05:43:07', '2021-04-28 05:43:07'),
('4e0997384a72f8ba5855e4945e3eb555158692925541996bd891dbb98a0367fb64d9d8793f2eacac', 33, 1, 'LaraPassport', '[]', 0, '2020-04-16 10:22:14', '2020-04-16 10:22:14', '2021-04-16 10:22:14'),
('4e2e4fb4a02b48dcc54da3ec6db8422b8f549ad9d57ac6874c87821286d1b9f0a35e53688bf28cf6', 237, 1, 'LaraPassport', '[]', 0, '2020-09-15 14:52:35', '2020-09-15 14:52:35', '2021-09-15 07:52:35'),
('4e5895e3393a06367817eeaac3b86999c75bd0b5da99e82746fc20660c0147c7a41760d8c569929b', 259, 1, 'LaraPassport', '[]', 0, '2020-09-05 00:03:22', '2020-09-05 00:03:22', '2021-09-04 17:03:22'),
('4e7c31abe8c47b395fd39d03fedaab0faf89e652aac8d89e42e5b13b277b0a2b0ff4b3f72f47373f', 97, 1, 'LaraPassport', '[]', 0, '2020-06-10 05:09:59', '2020-06-10 05:09:59', '2021-06-10 05:09:59'),
('4e95f3f80098ac122c8ab6650b59a239171eb0e6f1f5d24728005410e27584f246df72d9a62b2f48', 237, 1, 'LaraPassport', '[]', 0, '2020-08-24 12:18:33', '2020-08-24 12:18:33', '2021-08-24 05:18:33'),
('4e9f85a10436c08a59836dabd36ad46a1715957295c59229555973dc53b48ba48cbc2442b9fc91eb', 29, 1, 'LaraPassport', '[]', 0, '2020-04-09 10:37:55', '2020-04-09 10:37:55', '2021-04-09 10:37:55'),
('4ee890540a390f9a2a27f89394795d2ae8727e6990ed740a809083443507e2508400afb3994da354', 109, 1, 'LaraPassport', '[]', 0, '2020-08-01 18:57:46', '2020-08-01 18:57:46', '2021-08-01 11:57:46'),
('4f1c3487787e1c82ecfd91d0d73c63510fac0fe5be724eb6fc0577dd89a2696a504249bdd23baf16', 300, 1, 'LaraPassport', '[]', 0, '2021-01-06 18:40:47', '2021-01-06 18:40:47', '2022-01-06 11:40:47'),
('4f1cee39e79a7b8d0c4371d4615f3c9624fe2e816ca161ec80ca6bbbf93fa269644950f7e8cae1a3', 106, 1, 'LaraPassport', '[]', 0, '2020-06-12 06:21:39', '2020-06-12 06:21:39', '2021-06-12 06:21:39'),
('4f413c8e097c7a2e00ee91b2c83de35ae6e8b0ce4b52257eda6313dffdd97050d0760153fb3a66d2', 57, 1, 'LaraPassport', '[]', 0, '2020-08-18 12:05:41', '2020-08-18 12:05:41', '2021-08-18 05:05:41'),
('4f870b5fd0a9191ba3fc99a358154dba6af55c450bc76275bb4541cf9d06b072acff1ebb84c2d13f', 57, 1, 'LaraPassport', '[]', 0, '2020-08-01 16:55:00', '2020-08-01 16:55:00', '2021-08-01 09:55:00'),
('4f91c7b71297fea9333ef8911fb77d25ede32b8462a4f3089a60ec4b30ed3a0a36ca2c5bb5fb7bcb', 39, 1, 'LaraPassport', '[]', 0, '2021-01-13 01:02:31', '2021-01-13 01:02:31', '2022-01-12 18:02:31'),
('4fac466c17a6ae45c9ebb63055d5fd69d4f036a41f047ab8625835331ae0a8de36cd6ab414a7cff6', 119, 1, 'LaraPassport', '[]', 0, '2021-01-19 20:44:23', '2021-01-19 20:44:23', '2022-01-19 13:44:23'),
('4fd31f401f99a96bae5ee8eb8f9521c9d7ae9c42e57afda3682faca0c63178c9a530e9efbdce3fc5', 422, 1, 'LaraPassport', '[]', 0, '2020-11-30 17:56:03', '2020-11-30 17:56:03', '2021-11-30 10:56:03'),
('502c7b78a34c8729dafb610cdf1955866125c73d1460cd5ea5fe2af66b746da9aaff2925d6fdeeeb', 35, 1, 'LaraPassport', '[]', 0, '2020-05-20 16:05:07', '2020-05-20 16:05:07', '2021-05-20 16:05:07'),
('5030570763922077242a3b17e9110a2cf31cec7189a910d21c31d95f0f6355793af145c7d115cac8', 232, 1, 'LaraPassport', '[]', 0, '2020-08-20 12:03:05', '2020-08-20 12:03:05', '2021-08-20 05:03:05'),
('50a10f518a4ddc51635ad5e7c8e7b4ff806763999bb4dc5d6650e259696f05b65fb248d1650c848c', 305, 1, 'LaraPassport', '[]', 0, '2020-09-27 16:40:25', '2020-09-27 16:40:25', '2021-09-27 09:40:25'),
('51404280ae4e4087890268075570a956fe95a66e9003f958367e96dd7977846ab60d72f98bb040da', 57, 1, 'LaraPassport', '[]', 0, '2020-08-04 18:29:49', '2020-08-04 18:29:49', '2021-08-04 11:29:49'),
('514bd3dfae948dadaace5bb05f95976ce3b5a31a6f0702fb88479be740ffdac7ba50517aabb5a37b', 35, 1, 'LaraPassport', '[]', 0, '2020-05-16 04:55:02', '2020-05-16 04:55:02', '2021-05-16 04:55:02'),
('51990c823d585b10a9515e5957f513ae2c8ae36e873156a40792ecb8b1c2c56c3d802a1d945281e3', 238, 1, 'LaraPassport', '[]', 0, '2020-09-04 22:30:41', '2020-09-04 22:30:41', '2021-09-04 15:30:41'),
('51d2a792e1985fd7d537e1557632c551c63baa6eb955542a0a1b682dc73118d3ddb646f01adc0eba', 5, 1, 'LaraPassport', '[]', 0, '2020-03-18 23:29:25', '2020-03-18 23:29:25', '2021-03-19 04:59:25'),
('51e797dce2672b3ee9efcb514fcb879ae9bc7152dfe68abe25fce27b8ec581f5906e536810cfeec9', 33, 1, 'LaraPassport', '[]', 0, '2020-04-11 08:12:42', '2020-04-11 08:12:42', '2021-04-11 08:12:42'),
('521a22ebf4c7d3f8c986c929f1a1e2e0ac57d275367ec177ce9b95dbe0ad8f0cfb01d2d98e0f6f96', 35, 1, 'LaraPassport', '[]', 0, '2020-06-25 06:00:47', '2020-06-25 06:00:47', '2021-06-25 06:00:47'),
('5274c39e409038d99684d7ac0d8713b24a029af3b2610fe7729398b7aad85d5302affe11445fdd48', 424, 1, 'LaraPassport', '[]', 0, '2020-12-02 03:51:40', '2020-12-02 03:51:40', '2021-12-01 20:51:40'),
('52a166ec8446f438f3e34eb506ff7116db742d1a9d2317744f93cbf2ad6756360cdca74e244ae92d', 8, 1, 'LaraPassport', '[]', 0, '2020-04-10 04:59:53', '2020-04-10 04:59:53', '2021-04-10 04:59:53'),
('52d8160f9ffac714af5e673dc47b54b54bf7e218915dce3c09eac2da888eb489417d8daf284219e3', 309, 1, 'LaraPassport', '[]', 0, '2020-09-22 13:20:11', '2020-09-22 13:20:11', '2021-09-22 06:20:11'),
('52e571f5c524c2717cc706759e442ddc1a5233c5f4a53f3804fa92390621f679962547fe12162f0e', 57, 1, 'LaraPassport', '[]', 0, '2020-06-19 13:18:03', '2020-06-19 13:18:03', '2021-06-19 13:18:03'),
('53192e75ff47a4ed3646a97f058014ca8970514a3abd4cb1bfa46a6f66fcdbdddaaa01ee28160373', 33, 1, 'LaraPassport', '[]', 0, '2020-04-20 04:13:01', '2020-04-20 04:13:01', '2021-04-20 04:13:01'),
('537d72c470add623a9ee055c5f21484c40e30b8258549bc6af5e3423e4d2ed887c26c2d88eced658', 109, 1, 'LaraPassport', '[]', 0, '2020-07-02 08:16:39', '2020-07-02 08:16:39', '2021-07-02 08:16:39'),
('539555896b058c3e762f2b6dc91c10c16d19b414181c9f33e4ce9f666a8e0df21c435e65330d6338', 35, 1, 'LaraPassport', '[]', 0, '2020-06-27 11:04:29', '2020-06-27 11:04:29', '2021-06-27 11:04:29'),
('5397b22303e3647dcbc32e117e83c793a15d96df4f5b1d974c46cd7ad68b7db525da09935edc28eb', 243, 1, 'LaraPassport', '[]', 0, '2020-08-27 17:57:49', '2020-08-27 17:57:49', '2021-08-27 10:57:49'),
('54033addc938e0e8ff027a4580cbd3be1b87f4c49cec7501bef264c09a024345ebdd188cfa3df337', 59, 1, 'LaraPassport', '[]', 0, '2020-04-30 08:33:43', '2020-04-30 08:33:43', '2021-04-30 08:33:43'),
('549e0238977890102d30fa16351d9079ed8ed578b0b799f29472d44f32df36665ac2e18d8fe7e304', 379, 1, 'LaraPassport', '[]', 0, '2020-11-03 02:07:26', '2020-11-03 02:07:26', '2021-11-02 19:07:26'),
('54b31a1d74caf2a0ff3bd0534027897f14407efb8c2825b28d3a5a58eaba9bfef5def563207ea9d9', 3, 1, 'LaraPassport', '[]', 0, '2020-03-18 23:10:46', '2020-03-18 23:10:46', '2021-03-19 04:40:46'),
('5580c5475dabea748e447788cd84e081b3b767d53e2a9d1f99be59cbc88de9d274cc584229a500ca', 33, 1, 'LaraPassport', '[]', 0, '2020-04-11 10:34:48', '2020-04-11 10:34:48', '2021-04-11 10:34:48'),
('55af42c5db1d2a6ee942361b9781dcf85bf09ea5185a344256a475def0770bf7b09b0135c36dee65', 366, 1, 'LaraPassport', '[]', 0, '2020-10-18 08:37:00', '2020-10-18 08:37:00', '2021-10-18 01:37:00'),
('55cf0e2889b3db2ac7be0c4c068d894f78583635aadf9aba9eeb764aa400c422b3a381a89727c206', 35, 1, 'LaraPassport', '[]', 0, '2020-06-08 09:16:23', '2020-06-08 09:16:23', '2021-06-08 09:16:23'),
('55d42e84dc6487cb96a119e219fad9f7c1a79c136329ba37e2698773054e35bdaf9e9c8987f732b1', 33, 1, 'LaraPassport', '[]', 0, '2020-04-12 05:04:01', '2020-04-12 05:04:01', '2021-04-12 05:04:01'),
('561b89bc30d9fc1c77ef766486b63c5adb92168baff659f53d0c07528c89cd9cb0e12459ded074ca', 119, 1, 'LaraPassport', '[]', 0, '2021-01-19 21:55:02', '2021-01-19 21:55:02', '2022-01-19 14:55:02'),
('561e7b95c5e7c0cab145e9b351c19a9647baba07d9242eca8e8dc88c058ce51e75571051a3560dac', 57, 1, 'LaraPassport', '[]', 0, '2020-08-17 19:01:02', '2020-08-17 19:01:02', '2021-08-17 12:01:02'),
('567e0cd5b03c91d60b423140f9396a6894d683505b5435a4322a4e10e66fcecec6b1c4b4c30b7af3', 33, 1, 'LaraPassport', '[]', 0, '2020-04-11 11:56:04', '2020-04-11 11:56:04', '2021-04-11 11:56:04'),
('56a281f50e7ccd4c12a27cd866cd53f3f34a22224d3361ccd92203c9b22be1d2d1b7e63bb129920b', 234, 1, 'LaraPassport', '[]', 0, '2020-08-22 20:00:01', '2020-08-22 20:00:01', '2021-08-22 13:00:01'),
('57039730be2f4204cc9716439b2a76ceee9bb6c1a72b2425b126ffaf983635a806e4ee68ab03dd6b', 116, 1, 'LaraPassport', '[]', 0, '2020-07-31 12:16:06', '2020-07-31 12:16:06', '2021-07-31 05:16:06'),
('5734dacf9b737ea779d997859cb1573482676f85c05e335b5dbc68d542ebeab7b7318dcfdfb50b10', 307, 1, 'LaraPassport', '[]', 0, '2020-09-25 13:13:48', '2020-09-25 13:13:48', '2021-09-25 06:13:48'),
('573a3083c9ce7988d7eebff0f78a6841d4a6dc7c579e7a9dd379378a732e031dde37402ae4d95c4d', 244, 1, 'LaraPassport', '[]', 0, '2020-08-27 15:32:30', '2020-08-27 15:32:30', '2021-08-27 08:32:30'),
('57a503dfc46083c65ae0b046289b0268c8cdf71441d8d3f93d0c747a82fb4a598c1de8f6c99dd306', 33, 1, 'LaraPassport', '[]', 0, '2020-04-30 12:56:25', '2020-04-30 12:56:25', '2021-04-30 12:56:25'),
('57aed112bd48ddd101966cf30dbe204dcf80001292393cfa9c1ac5bd139ecba7f86e5fc76962a212', 33, 1, 'LaraPassport', '[]', 0, '2020-04-17 09:01:16', '2020-04-17 09:01:16', '2021-04-17 09:01:16'),
('57cdba022b7a405384598d7717db6de551d43d80532271f083954a03c7d486ab488cb7964a972af7', 327, 1, 'LaraPassport', '[]', 0, '2020-09-30 19:35:47', '2020-09-30 19:35:47', '2021-09-30 12:35:47'),
('5824b8a6c91bf3139cfd98c803ba94224b3ce4c5ca68894d29b5c3fb9964697251c531b8d832f411', 57, 1, 'LaraPassport', '[]', 0, '2020-04-22 04:33:56', '2020-04-22 04:33:56', '2021-04-22 04:33:56'),
('585c0aa40b6a96c251a1abb2965aa2123c9d7166660077cf1897cc46cbef1f593fc7cb4358ba4ef1', 195, 1, 'LaraPassport', '[]', 0, '2020-08-17 06:39:09', '2020-08-17 06:39:09', '2021-08-16 23:39:09'),
('5879e6f2c3308dfe87482653843ff3dc0e479b8d8d1972ef64b4710499258d9ab2a3f7cb83fef68e', 57, 1, 'LaraPassport', '[]', 0, '2020-08-13 14:43:39', '2020-08-13 14:43:39', '2021-08-13 07:43:39'),
('58800cdfbc68f46ff1427733c1e7859c3140dff601d2b54d6a32ccacc49626c817eaf904c5dafdd0', 57, 1, 'LaraPassport', '[]', 0, '2020-08-01 17:49:48', '2020-08-01 17:49:48', '2021-08-01 10:49:48'),
('58d837585bfa77ce1b774bed20844890a29deb530859b4628808e6dda037ccc7a9871804975f7833', 35, 1, 'LaraPassport', '[]', 0, '2020-07-31 14:17:51', '2020-07-31 14:17:51', '2021-07-31 07:17:51'),
('590f0053bd7163f26bac43f6a8aa046718d685752f91250dbb69ebbcd8edc568dd9f55da0e71de0d', 434, 1, 'LaraPassport', '[]', 0, '2020-12-23 18:31:24', '2020-12-23 18:31:24', '2021-12-23 11:31:24'),
('5924f55b1059b3140c8076e76fd094d7606e5d9697db9f1d48be6b9dca5c71e0c73a0897450d5d07', 315, 1, 'LaraPassport', '[]', 0, '2020-09-23 11:38:26', '2020-09-23 11:38:26', '2021-09-23 04:38:26'),
('592776aea00088ec39deefcb73bcab013533c342ec4c25b25e8245d29f903607324ff2daf0de23bb', 35, 1, 'LaraPassport', '[]', 0, '2020-08-04 12:51:22', '2020-08-04 12:51:22', '2021-08-04 05:51:22'),
('597a67758a6c4bb4f36261ba414933b36dc6816c1c77db5794d2998e72f73817b3e9485bf7a6c954', 307, 1, 'LaraPassport', '[]', 0, '2020-09-17 15:02:34', '2020-09-17 15:02:34', '2021-09-17 08:02:34'),
('598264ccbd76490f44327ca86a722e17fba9aa718d4fe53f75313a7fdff5ffa561193f2ee0d71dc0', 57, 1, 'LaraPassport', '[]', 0, '2020-04-30 12:59:38', '2020-04-30 12:59:38', '2021-04-30 12:59:38'),
('59a62d110a6445a95d10bd7ce80253b1344d7bd6fbb62f6adc6391ef67f26024ac5b8765dcfc3f85', 429, 1, 'LaraPassport', '[]', 0, '2020-12-15 06:25:01', '2020-12-15 06:25:01', '2021-12-14 23:25:01'),
('59c2528431a7d52a27adf3345db41fb99b1cdd6367ba7b2cab76bb92d0fbd3dfbf4189b9062a97ac', 237, 1, 'LaraPassport', '[]', 0, '2020-10-17 22:25:49', '2020-10-17 22:25:49', '2021-10-17 15:25:49'),
('5a29709f050ccc5e6602b1624721c0717b40a0025a28480652561953cedb1a3e0e96cb555d746bfd', 274, 1, 'LaraPassport', '[]', 0, '2020-09-10 16:01:42', '2020-09-10 16:01:42', '2021-09-10 09:01:42'),
('5a29ad6be87cfea64faaedc3f0a0166570ea6cfb23c9f6154e85ee16f5367643328018e9188c4c69', 376, 1, 'LaraPassport', '[]', 0, '2020-12-19 20:11:29', '2020-12-19 20:11:29', '2021-12-19 13:11:29'),
('5aa6235dbcf2aabca2b55bd3f32565c376dac468ad8b05254fecbcdd4058c38e089bcd84fe115b74', 33, 1, 'LaraPassport', '[]', 0, '2020-04-21 14:11:59', '2020-04-21 14:11:59', '2021-04-21 14:11:59'),
('5ad96da3495a6d2c758ea1b8f2a7c2e3b25580325e0bc5118052f1f50e89b55c6eb99b3cb5ff680c', 363, 1, 'LaraPassport', '[]', 0, '2020-10-15 16:35:02', '2020-10-15 16:35:02', '2021-10-15 09:35:02'),
('5b285300c60bdf00d705ee49db7aa692a5d847ee9ee01ceb62a21deeb2b5bb2a68f1450fba6050d0', 244, 1, 'LaraPassport', '[]', 0, '2020-09-02 13:11:07', '2020-09-02 13:11:07', '2021-09-02 06:11:07'),
('5b781b1782e7e9730be729b7a5a2d73653ad7d76b035f806b5e351e18cbb05d877e8f4c10b5fbb90', 238, 1, 'LaraPassport', '[]', 0, '2020-09-16 18:20:02', '2020-09-16 18:20:02', '2021-09-16 11:20:02'),
('5ba53e9be0c58f99738d4473277fe34b8dd3fb3eecd6408168feea76e07d8d4a93966ad9145c9c0f', 57, 1, 'LaraPassport', '[]', 0, '2020-06-13 13:11:47', '2020-06-13 13:11:47', '2021-06-13 13:11:47'),
('5bc97b72e54e21c154f45712b3cfbaa44a28d90639529ea35f559dedd53224e97fccc11f212b418a', 33, 1, 'LaraPassport', '[]', 0, '2020-04-12 05:04:32', '2020-04-12 05:04:32', '2021-04-12 05:04:32'),
('5bce21a2c77f8968aeee794cd9236cf384be1f670770dd4c087f5ae4dad827db7c6fb3b1cc316c93', 412, 1, 'LaraPassport', '[]', 0, '2020-11-16 20:38:51', '2020-11-16 20:38:51', '2021-11-16 13:38:51'),
('5bf36b1c71f5094aee44d7765b0159feec53dabe94dde2fe5eb4217e3e6acc9556218d48ac084ade', 42, 1, 'LaraPassport', '[]', 0, '2021-01-13 06:36:44', '2021-01-13 06:36:44', '2022-01-12 23:36:44'),
('5c10c53bd0442833823a4d2fec0434cfa839acf965ef8d99cb503b0ec0c20b604b351b922dcd96fc', 35, 1, 'LaraPassport', '[]', 0, '2020-08-15 12:37:43', '2020-08-15 12:37:43', '2021-08-15 05:37:43'),
('5c28afec8c88275877438e7ab377b5d67ef054419ffa89d809c27824064fe29ba2e910f1df7cceb5', 33, 1, 'LaraPassport', '[]', 0, '2020-04-10 09:19:00', '2020-04-10 09:19:00', '2021-04-10 09:19:00'),
('5c794cb05901bd855523dae1c722a5a9e943824842c0d4d9c1f560f25ef205d09948f6ca6fdbf9db', 237, 1, 'LaraPassport', '[]', 0, '2020-09-24 19:35:44', '2020-09-24 19:35:44', '2021-09-24 12:35:44'),
('5ca397131ebbdf8472ba648d090cec299e3d01828e1d880a7188463d2e085382575f4a2a07996ce0', 59, 1, 'LaraPassport', '[]', 0, '2020-04-26 18:46:47', '2020-04-26 18:46:47', '2021-04-26 18:46:47'),
('5d19206d2287584573b62e161d4036552cadbbaefffaa7ce4d995938970b11d547499fd5c31b9eff', 201, 1, 'LaraPassport', '[]', 0, '2020-08-18 20:22:22', '2020-08-18 20:22:22', '2021-08-18 13:22:22'),
('5debb870352ea2d124aaa7a7c99af53461fc669f10a9187392a72d1c225ca99c55b973cb7aacdc88', 8, 1, 'LaraPassport', '[]', 0, '2020-04-10 04:42:18', '2020-04-10 04:42:18', '2021-04-10 04:42:18'),
('5ded933e86e89e6b8bb309fd1827c71d02b6453fbc922fcb7a1eae8564a453e7b7a99adb69cd5d03', 57, 1, 'LaraPassport', '[]', 0, '2020-06-23 05:07:48', '2020-06-23 05:07:48', '2021-06-23 05:07:48'),
('5e88bf54dfb0319fc6adde873f0cdfb4d241d4d5b68b6a28f0383faec6aa44a73dbbff39cc989a51', 242, 1, 'LaraPassport', '[]', 0, '2020-08-27 13:56:40', '2020-08-27 13:56:40', '2021-08-27 06:56:40'),
('5ea035d62b06e112bec741710c0fea36ee149a86183bb69581b10da60a112affa2f20c6fa85ad253', 6, 1, 'LaraPassport', '[]', 0, '2020-03-18 23:30:02', '2020-03-18 23:30:02', '2021-03-19 05:00:02'),
('5ed7f9321e60b8d4c7be28313279c72fd7b63738487e37e01ee7194222d10aea79d85e4366972109', 130, 1, 'LaraPassport', '[]', 0, '2021-01-21 23:37:05', '2021-01-21 23:37:05', '2022-01-21 16:37:05'),
('5ee3ec7ff3123090711a94262c8287c6527f678d15efc6016424e30425c67ae0470d708b16564889', 33, 1, 'LaraPassport', '[]', 0, '2020-04-15 06:12:15', '2020-04-15 06:12:15', '2021-04-15 06:12:15'),
('5f295bbf395b82ea112887145d942ad607ba92fce4718aac1e3ea10fd4f52f8ec7cc40a3a35f8ce8', 304, 1, 'LaraPassport', '[]', 0, '2020-09-16 23:14:06', '2020-09-16 23:14:06', '2021-09-16 16:14:06'),
('5f3d3645f21045e4293cbe9df2d843cc90bd0afd14bbb50462b1fec9e659c832800af3f674cd7920', 321, 1, 'LaraPassport', '[]', 0, '2020-09-28 14:27:28', '2020-09-28 14:27:28', '2021-09-28 07:27:28'),
('5f56dc78d9d239083a3bde67721fd2e8dcccce482c1481178fcdcd366e5e1438fe537f96ce3b1cb1', 59, 1, 'LaraPassport', '[]', 0, '2020-04-26 18:46:40', '2020-04-26 18:46:40', '2021-04-26 18:46:40'),
('5f6c53d39a6a30702c4c9889cf9dd3c7f3b0eab410d0c4c26b77450cc307a7c1d414baf8bdbb955f', 434, 1, 'LaraPassport', '[]', 0, '2020-12-28 00:08:25', '2020-12-28 00:08:25', '2021-12-27 17:08:25'),
('5f7fa9c08408d896f6c071d9639cc079d91f2176d126808cc44181120ea87f9bc8b9303556a3107d', 35, 1, 'LaraPassport', '[]', 0, '2020-04-10 09:09:36', '2020-04-10 09:09:36', '2021-04-10 09:09:36'),
('5fd77581691b23db07fb6a8f391eb6996a5216a22fc2dcb4a9a8eaf4f8903e9534b6749e6522681d', 33, 1, 'LaraPassport', '[]', 0, '2020-04-13 05:55:45', '2020-04-13 05:55:45', '2021-04-13 05:55:45'),
('6031df86f059d0c0445c169911e6cd8fd1161ea6740b2cbd9bdafbb21537b399ce70e05fbf202ff7', 57, 1, 'LaraPassport', '[]', 0, '2020-08-01 16:54:44', '2020-08-01 16:54:44', '2021-08-01 09:54:44'),
('60386767d9657f0c419e5c0df8dea78e75ea667e76f45bc571b288fc423c02d258be465063817e6d', 354, 1, 'LaraPassport', '[]', 0, '2020-10-13 02:23:27', '2020-10-13 02:23:27', '2021-10-12 19:23:27'),
('607cf69f23e8e57aac8fafdf73878d89736e33a7b50d8931373fbb88726bfe557696ce3e2304ccf7', 124, 1, 'LaraPassport', '[]', 0, '2020-07-05 07:15:11', '2020-07-05 07:15:11', '2021-07-05 07:15:11'),
('607f6b756e9346d0a17f0d83aa769714399ed775361f49b26871f932cce22f78e6aa7d9504de8da7', 19, 1, 'LaraPassport', '[]', 0, '2020-04-09 05:14:06', '2020-04-09 05:14:06', '2021-04-09 05:14:06'),
('60971ba38cf846d3ec13a1d0f55530b562bc1f3c5a34b3c671e1cab4d0f673565f867214447c9b04', 33, 1, 'LaraPassport', '[]', 0, '2020-04-14 06:14:40', '2020-04-14 06:14:40', '2021-04-14 06:14:40'),
('60c07046c5632e95253a3b78fca0cda048f6980c61ebef51db916d5818c007e6c4ebf1b54a0f4116', 242, 1, 'LaraPassport', '[]', 0, '2020-08-27 13:56:49', '2020-08-27 13:56:49', '2021-08-27 06:56:49'),
('60e587163a8cce63e657b3965c65b3ff948a15d4eea4667a69bfe49e4dd3f2d3dd5856d5279c86f9', 244, 1, 'LaraPassport', '[]', 0, '2020-09-25 12:35:13', '2020-09-25 12:35:13', '2021-09-25 05:35:13'),
('614652d064478b0782dcf103f04e1e2f4f5326bc25839222287be732872140b080ed849abbca923d', 57, 1, 'LaraPassport', '[]', 0, '2020-06-11 07:30:58', '2020-06-11 07:30:58', '2021-06-11 07:30:58'),
('6147d6cda8606999d00a8f368838b14ebb105f5b7c6e77705f94053514542a80630c402647c6542f', 3, 1, 'LaraPassport', '[]', 0, '2020-03-18 23:11:30', '2020-03-18 23:11:30', '2021-03-19 04:41:30'),
('6158b550e8ec2634486275ee2af0b593e6b28415084de4e22c4ced762d8cbb215a30c78f1d94c057', 33, 1, 'LaraPassport', '[]', 0, '2020-04-11 12:45:42', '2020-04-11 12:45:42', '2021-04-11 12:45:42'),
('61bb7a357afa17066196ec5531c41cb6a02c8f56dd87c0f0308104ad49ae6d4dfb9127e8a3e1cc5d', 249, 1, 'LaraPassport', '[]', 0, '2020-09-30 17:54:01', '2020-09-30 17:54:01', '2021-09-30 10:54:01'),
('6227ccc925a885f466ebdd63c752a864a385245e4d210c9c1e5b5c18210095a96ce8a2c6b01b8aee', 53, 1, 'LaraPassport', '[]', 0, '2020-04-17 09:56:33', '2020-04-17 09:56:33', '2021-04-17 09:56:33'),
('623aef6329fe5a9925d51ba72ef42d29e97250c22d911dd7769a9520a9981e21dec125ddede744a3', 336, 1, 'LaraPassport', '[]', 0, '2020-10-01 20:36:51', '2020-10-01 20:36:51', '2021-10-01 13:36:51'),
('624c416fcc82432fc162a9862d583547381cdeda6931a82c81635c2f36fd53a00291aaee55efc59f', 356, 1, 'LaraPassport', '[]', 0, '2020-10-14 18:24:42', '2020-10-14 18:24:42', '2021-10-14 11:24:42'),
('6278c19f83deb1098933bbaef009364e69dfa3aa359572c9dc175e412be3897981f894bbd5fdaaea', 57, 1, 'LaraPassport', '[]', 0, '2020-06-25 04:54:24', '2020-06-25 04:54:24', '2021-06-25 04:54:24'),
('628299dd2ee990c4416c77e0b1e251f39750e69b8911607b08979bf3e218e314916c4a004ad2a29c', 59, 1, 'LaraPassport', '[]', 0, '2020-05-01 13:41:13', '2020-05-01 13:41:13', '2021-05-01 13:41:13'),
('628c1ae4658cd27df8c0456aa021e238004a041f5e64a9fa2dd7c893a1c0e8a8c3500b9a8df38d3d', 58, 1, 'LaraPassport', '[]', 0, '2020-04-22 09:00:33', '2020-04-22 09:00:33', '2021-04-22 09:00:33'),
('6292323aa1d57b2a12dbcb4a09ec7c6b37adf110dd9bdcc59e88dffa2af20795981337deed2e5c39', 198, 1, 'LaraPassport', '[]', 0, '2020-08-17 21:42:41', '2020-08-17 21:42:41', '2021-08-17 14:42:41'),
('62cd5a44f0fb07e72a78b35f34cbee09f9c0369d95eb58d0fd286006bdde16c57ed6b8a6de9f0a11', 57, 1, 'LaraPassport', '[]', 0, '2020-06-22 13:05:27', '2020-06-22 13:05:27', '2021-06-22 13:05:27'),
('62f26c2b66b0db04ccbb62824f53862ee596f6b8891d4bab1c97e798b215987bb95f67949b157708', 237, 1, 'LaraPassport', '[]', 0, '2020-10-01 11:34:48', '2020-10-01 11:34:48', '2021-10-01 04:34:48'),
('632630c3c0be9d985e75cbfa05775eff2ca6bde9aca87899a70d1bb84ac8bae12099652721e2cb9d', 88, 1, 'LaraPassport', '[]', 0, '2020-06-05 05:34:35', '2020-06-05 05:34:35', '2021-06-05 05:34:35'),
('6344fd7a42fcf3ffa43d001e98e09267595f58b2c39c680178ae8feaaab11cc24f29577ddebfae68', 57, 1, 'LaraPassport', '[]', 0, '2020-07-03 19:51:53', '2020-07-03 19:51:53', '2021-07-03 19:51:53'),
('636e154888580179a6ce53ab26e69f75221c138b142f9eca3c8610f76e8639a80ca6dac17c823e3a', 319, 1, 'LaraPassport', '[]', 0, '2020-09-25 18:28:25', '2020-09-25 18:28:25', '2021-09-25 11:28:25'),
('63c3563ea0223b6cc7e6a1832d472a815eb0b853452e633be56d0e9823bde50449df3929c5a48ada', 33, 1, 'LaraPassport', '[]', 0, '2020-04-14 13:29:41', '2020-04-14 13:29:41', '2021-04-14 13:29:41'),
('63def3060face086ddf83baaa657c966113a2fb772e6f692361d6803538aa96148ce249e010e52aa', 33, 1, 'LaraPassport', '[]', 0, '2020-04-10 09:26:04', '2020-04-10 09:26:04', '2021-04-10 09:26:04'),
('64137ef4c540d117d003da7269cea8d3dd68366fa9591325c812f13dd995fb889b16c2ddb85f4946', 54, 1, 'LaraPassport', '[]', 0, '2020-04-17 09:57:08', '2020-04-17 09:57:08', '2021-04-17 09:57:08'),
('644ab42d57892d2148a7369268c06c8beab6d3b8bc221a9da14b38b5e020b1e6144a61ee7a598091', 231, 1, 'LaraPassport', '[]', 0, '2020-10-19 13:01:00', '2020-10-19 13:01:00', '2021-10-19 06:01:00'),
('649bc940cd88b5c5ba420be764caa4b40a1f6a112d7a8cd83b0dccadccb0e618777fe05ac13fe387', 57, 1, 'LaraPassport', '[]', 0, '2020-07-30 18:10:19', '2020-07-30 18:10:19', '2021-07-30 11:10:19'),
('64a9411bfc82837b435aaeaf380eca1bd16e4956c2a9d786f04069bbf76635b6c4e258fc45891531', 197, 1, 'LaraPassport', '[]', 0, '2020-08-17 14:17:37', '2020-08-17 14:17:37', '2021-08-17 07:17:37'),
('64d1ffcce93d0a36694c8e2099765ab56972480e2393a7df94e7ae9e7e05d6bcc491d1ebd476c7dd', 249, 1, 'LaraPassport', '[]', 0, '2020-08-31 21:00:41', '2020-08-31 21:00:41', '2021-08-31 14:00:41'),
('64d8c6ea357a20acc671d1035f545962f5eeef2d8b7cc8d67d733ad00917b8e0afb573b94d288be9', 195, 1, 'LaraPassport', '[]', 0, '2020-08-18 08:46:57', '2020-08-18 08:46:57', '2021-08-18 01:46:57'),
('64de07e4454c63f18b909a1757f235b4023538a26973ea4bcfcf10e5d1c9941cde37bcd49bcbdfcd', 31, 1, 'LaraPassport', '[]', 0, '2020-04-09 10:08:16', '2020-04-09 10:08:16', '2021-04-09 10:08:16'),
('64de76e9fbe71c0b1dc9cdf79a3b337e871a00968785c8564db41e3539fbb965e79d658a47c290ca', 195, 1, 'LaraPassport', '[]', 0, '2020-08-17 06:45:49', '2020-08-17 06:45:49', '2021-08-16 23:45:49'),
('64eb9367a4fda68f5095a7ebe2e3837b370cb8e27e863fff1c60aa5cf9d0fc83839025f34442b679', 237, 1, 'LaraPassport', '[]', 0, '2020-09-10 13:14:27', '2020-09-10 13:14:27', '2021-09-10 06:14:27'),
('651e6b3863e1a5e50b7afd4a7a1fcb0771dc81a75dbe14e810d0efc84e2a050853a0d16e0379c40d', 57, 1, 'LaraPassport', '[]', 0, '2020-04-30 13:03:58', '2020-04-30 13:03:58', '2021-04-30 13:03:58'),
('655ba6d056a019f70ed9dadb08c0692424f486d8f7b59ba21359f3fa9586b0bdbebc2d3a1d9fa25b', 379, 1, 'LaraPassport', '[]', 0, '2020-11-03 02:02:25', '2020-11-03 02:02:25', '2021-11-02 19:02:25'),
('65cec8c5b0094675408880188a2d040ec3fed08e2b5d01f8eb616562b9e871e467a856f3ee47e027', 357, 1, 'LaraPassport', '[]', 0, '2020-10-14 14:20:18', '2020-10-14 14:20:18', '2021-10-14 07:20:18'),
('65ee9f682d08af76e2093481d1335b031fa32c03d7806ebb5041e80d6a4bea4d868d336fcfbab4f7', 58, 1, 'LaraPassport', '[]', 0, '2020-04-22 06:35:43', '2020-04-22 06:35:43', '2021-04-22 06:35:43'),
('65f8b8c74d568a66300b5c604edf6e2903c2e9e7201834946e3c5a31220cd4a0764f6c294c619935', 266, 1, 'LaraPassport', '[]', 0, '2020-10-24 19:26:26', '2020-10-24 19:26:26', '2021-10-24 12:26:26'),
('66a026716b41e390f20f7148f930370c57d1e78b3803e4d5701e41ed2063db5b4a56541700af50b8', 312, 1, 'LaraPassport', '[]', 0, '2020-09-30 16:47:05', '2020-09-30 16:47:05', '2021-09-30 09:47:05'),
('66af9d4c17a3ca68fd2fbd7ede8920e6927ad23093ac840565bee13202bab7192e67e8123f9d73ba', 8, 1, 'LaraPassport', '[]', 0, '2020-04-08 11:07:29', '2020-04-08 11:07:29', '2021-04-08 11:07:29'),
('66ba90820a74edbc3d03120b3e7934e0f621e196d25a020074f38c61b740feae59e3486ef67842f7', 35, 1, 'LaraPassport', '[]', 0, '2020-06-13 06:37:21', '2020-06-13 06:37:21', '2021-06-13 06:37:21'),
('66fb989488eddf60a79f6a22311c78a62293b88960326ca1c4e6684070af2c40eba67d1f43e4450b', 57, 1, 'LaraPassport', '[]', 0, '2020-07-02 11:03:47', '2020-07-02 11:03:47', '2021-07-02 11:03:47'),
('67377bc8d9a3cc550e1f0a0e4ce51daa28f292b3433e3d0cbb9ff5efd0a0793a2e43e33e8ae128c9', 414, 1, 'LaraPassport', '[]', 0, '2020-11-20 17:21:57', '2020-11-20 17:21:57', '2021-11-20 10:21:57'),
('679532c2b480e1f034da1469c12848709034a387b8e0e080c8337cfc1ff50a823f2a3f8e8e6ebba6', 35, 1, 'LaraPassport', '[]', 0, '2020-05-28 12:12:42', '2020-05-28 12:12:42', '2021-05-28 12:12:42'),
('67b0d3156804454681c1fc8ac3614f3c607628d1f9d22c5a3d4719e1591c8c6d20667f5da42a3a47', 231, 1, 'LaraPassport', '[]', 0, '2020-10-09 17:42:49', '2020-10-09 17:42:49', '2021-10-09 10:42:49');
INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('67bb71d3f91f3945de47fd0c46ebb0f547307c464437a1822e0f3b9aa28efff0b7eadb1a49852290', 42, 1, 'LaraPassport', '[]', 0, '2021-01-13 06:36:20', '2021-01-13 06:36:20', '2022-01-12 23:36:20'),
('67ed673b5cc5fafed2cde1cad25a0e36546b697fabd5f0f6d9be9f62adbf1eca90b95596158792e8', 57, 1, 'LaraPassport', '[]', 0, '2020-04-30 04:51:19', '2020-04-30 04:51:19', '2021-04-30 04:51:19'),
('683bf8ebdd6cbb1ab819a56505959e6c00fb4d6047d50da4b43dc10eb61ce2f51a1a78256c962a7e', 35, 1, 'LaraPassport', '[]', 0, '2021-01-12 22:35:40', '2021-01-12 22:35:40', '2022-01-12 15:35:40'),
('6881e998f2264d12d156997e45c9f74e0b11d718e9b28524e4615a1375cfcab0fcdb95b44814778e', 63, 1, 'LaraPassport', '[]', 0, '2020-04-27 06:45:05', '2020-04-27 06:45:05', '2021-04-27 06:45:05'),
('68cf495c2633df7cdf97417f1fbeb14315493e9ce9bbeb0b9707601965d10178c35e3a0d85bc3c4a', 220, 1, 'LaraPassport', '[]', 0, '2020-08-19 11:36:04', '2020-08-19 11:36:04', '2021-08-19 04:36:04'),
('68e4dad44f6b851e762632b20eccddfc6b3021b07004c0d620213731624a1b88400a91edc6fa1ffe', 237, 1, 'LaraPassport', '[]', 0, '2020-10-09 18:38:28', '2020-10-09 18:38:28', '2021-10-09 11:38:28'),
('69426cf3c83dd91f1cf6c131a9c4740663cb37f284866c779cf5d80592a60360e02b2e6c3a762990', 35, 1, 'LaraPassport', '[]', 0, '2020-06-25 07:22:21', '2020-06-25 07:22:21', '2021-06-25 07:22:21'),
('696db7dc32db7415cfa7732e2ca809ad96ab0500f0a0fd170967b8746c0b615d5dc9232c6db395cd', 307, 1, 'LaraPassport', '[]', 0, '2020-09-17 15:09:49', '2020-09-17 15:09:49', '2021-09-17 08:09:49'),
('6a48de5ee53ff78e0fee20de1c85366d5af821f1a200cb9723d3e2daad7204ed92c34b7a210c7670', 363, 1, 'LaraPassport', '[]', 0, '2020-10-15 16:35:19', '2020-10-15 16:35:19', '2021-10-15 09:35:19'),
('6a4da758cc2a1dcd40240980b4f1200ff42335c4ee3caa17aa9366340c78ac2f7ec0ab367a3e540d', 305, 1, 'LaraPassport', '[]', 0, '2020-09-23 01:25:53', '2020-09-23 01:25:53', '2021-09-22 18:25:53'),
('6aa5bf28218ab34610dad04428380456c052a45608291736958b08995621fe584963715623d65ece', 57, 1, 'LaraPassport', '[]', 0, '2020-05-07 11:07:42', '2020-05-07 11:07:42', '2021-05-07 11:07:42'),
('6b332f9af24bf0f534edeac3291ba790a7006ea7aedde69c55d8ed9097f6eb29f972a90d9a3dfbce', 266, 1, 'LaraPassport', '[]', 0, '2020-09-08 15:46:46', '2020-09-08 15:46:46', '2021-09-08 08:46:46'),
('6b47d1c2dbf5152d758f4116b44a4fd3062e718dafbb201c77de7912ad0194c8c6a7d7cad947188d', 59, 1, 'LaraPassport', '[]', 0, '2020-05-28 04:50:34', '2020-05-28 04:50:34', '2021-05-28 04:50:34'),
('6b9b874bd1eda990bd0d94e0e040a1aefcf93b38209cdd428d33469b6f3b8fc124b73476f92f6a8c', 244, 1, 'LaraPassport', '[]', 0, '2020-10-22 01:29:33', '2020-10-22 01:29:33', '2021-10-21 18:29:33'),
('6c3e1ddab094fcbb4c23f1b3a69d21512a4a836061e3df161b5f8eafa7efcf5d6c35dfcc42c20500', 57, 1, 'LaraPassport', '[]', 0, '2020-07-05 20:07:59', '2020-07-05 20:07:59', '2021-07-05 20:07:59'),
('6c447a279ad0aecfd863d00993388308763c77e0ab2686e8a3e5c7026711cde514ab5cea1cc74a91', 57, 1, 'LaraPassport', '[]', 0, '2020-04-23 04:05:37', '2020-04-23 04:05:37', '2021-04-23 04:05:37'),
('6c4f781fd83bb3c61a5825d78ae160b3d1cf120bdb0e5a1f08ec7ca79edadf6931f240722c54fb05', 33, 1, 'LaraPassport', '[]', 0, '2020-04-21 13:10:50', '2020-04-21 13:10:50', '2021-04-21 13:10:50'),
('6c576a3b48760549afcf3802ccf6ce19df07153aba9dff37caa9c51cc56f1093dad2bc8ce8dd6e3d', 59, 1, 'LaraPassport', '[]', 0, '2020-04-23 15:02:10', '2020-04-23 15:02:10', '2021-04-23 15:02:10'),
('6cb41adaea412b8a21a49144d47dd01f6f19109c53bd579af22bf4c3f9b1c85fc09102e1d2c4780c', 153, 1, 'LaraPassport', '[]', 0, '2020-08-26 23:37:41', '2020-08-26 23:37:41', '2021-08-26 16:37:41'),
('6d03a8238d50879f3c98a51a30951666d9f50e0da589d29ef0ff09e1b6b7b9b7b1af283f6a42f3ab', 8, 1, 'LaraPassport', '[]', 0, '2021-01-13 00:42:16', '2021-01-13 00:42:16', '2022-01-12 17:42:16'),
('6d3f7c9b6a63415f2f8120e9788fa175b6b911d273f913c14266b050e30f193f86c07e0a19a64452', 8, 1, 'LaraPassport', '[]', 0, '2020-04-10 05:23:21', '2020-04-10 05:23:21', '2021-04-10 05:23:21'),
('6d70ac1291ea2f1f7fd89a51f8cce4dcb62e7f7123f8842b0e8ae42bf887a70320efd526c0460db5', 244, 1, 'LaraPassport', '[]', 0, '2020-09-03 21:17:41', '2020-09-03 21:17:41', '2021-09-03 14:17:41'),
('6d7938adb06792535d1caaec0d7f0c570b910d4b1bb3cdca7105f627c8499c68a8172f8f2fd96b52', 195, 1, 'LaraPassport', '[]', 0, '2020-08-18 11:08:39', '2020-08-18 11:08:39', '2021-08-18 04:08:39'),
('6d8b31cba7185646271f02d6965236cac04f89cb5b717e07d8d62de916eb9672a3bcae23babef78f', 35, 1, 'LaraPassport', '[]', 0, '2020-05-17 20:37:04', '2020-05-17 20:37:04', '2021-05-17 20:37:04'),
('6db0c6398ac0f653a6b6565e82c6675aff7d751597b1a4ba66280e7ddc0ff98670903a0d55100a2c', 195, 1, 'LaraPassport', '[]', 0, '2020-08-17 00:29:53', '2020-08-17 00:29:53', '2021-08-16 17:29:53'),
('6dc4c2880037b162497e08659f5465d5c90eab325662616ecdbe509f8a2758b19c9e0dd1533738c9', 425, 1, 'LaraPassport', '[]', 0, '2020-12-02 20:33:24', '2020-12-02 20:33:24', '2021-12-02 13:33:24'),
('6ded17d1b28b278202acaf19c035eeca52ff87c170bc37ca325cae015a0eeb6bfb97f0078bd819a9', 316, 1, 'LaraPassport', '[]', 0, '2020-10-16 20:39:38', '2020-10-16 20:39:38', '2021-10-16 13:39:38'),
('6e41a109496ada4b9c9396b73151d006138f2d781b812c01de60297025248463ce6342a166cd2d0b', 312, 1, 'LaraPassport', '[]', 0, '2020-09-30 16:45:52', '2020-09-30 16:45:52', '2021-09-30 09:45:52'),
('6e5b93bdbe62f37e20fbd878d6e6a4ae062c2d6f098e14cd5cca89e56926e866b64081632441f86f', 88, 1, 'LaraPassport', '[]', 0, '2020-06-11 05:53:24', '2020-06-11 05:53:24', '2021-06-11 05:53:24'),
('6e5c1ee6e6447557dabac8fbc2d1b5be439c3199f4186efd1b0b5961a6d7dfaedc8b1ea7cdc20099', 217, 1, 'LaraPassport', '[]', 0, '2020-08-18 20:54:34', '2020-08-18 20:54:34', '2021-08-18 13:54:34'),
('6e69a6d1a7198725800c4612b328f8347418f097412b84fca6c780e47cd1a270e9af07ab9b207fa2', 33, 1, 'LaraPassport', '[]', 0, '2020-04-11 16:47:13', '2020-04-11 16:47:13', '2021-04-11 16:47:13'),
('6e6ad9f129a8951159bc4a41c07753c297034299f07a73eb4bb5204f7b691fe44b1a52dadbf057c3', 57, 1, 'LaraPassport', '[]', 0, '2020-08-18 12:04:13', '2020-08-18 12:04:13', '2021-08-18 05:04:13'),
('6e6add2bdf862d66e402a3accf6ea8a5a1c3123464bdaca0268d21eff99df6763217363dd8559e29', 35, 1, 'LaraPassport', '[]', 0, '2020-06-25 10:24:10', '2020-06-25 10:24:10', '2021-06-25 10:24:10'),
('6e7db70b38c05593802e9505714223287f4ae7d358a06fe8765751128b13657c41b2db8401690f52', 334, 1, 'LaraPassport', '[]', 0, '2020-10-01 17:18:56', '2020-10-01 17:18:56', '2021-10-01 10:18:56'),
('6e89f66c14e9f88c3e4781128c9e7c449aba7a7df3dfb6241969a06e0350d62d12ee8c34a4cf472f', 498, 1, 'LaraPassport', '[]', 0, '2021-01-07 22:58:49', '2021-01-07 22:58:49', '2022-01-07 15:58:49'),
('6e9a0724703fef211aef21a05eed5ff27f49c52353784ea5622374823def49a3b663b2a8185005f3', 305, 1, 'LaraPassport', '[]', 0, '2020-09-15 06:04:41', '2020-09-15 06:04:41', '2021-09-14 23:04:41'),
('6ea9366ba8484325633f1e2d180aea1d1606d90274c98346e7f3d5311475e63e078d1996de482e5a', 309, 1, 'LaraPassport', '[]', 0, '2020-09-22 22:17:33', '2020-09-22 22:17:33', '2021-09-22 15:17:33'),
('6ecf727ccdeaf04e53a704353de1ae8c1fadc7779f6b04c4eb5e09c75793d001b9f263bf05de70d9', 278, 1, 'LaraPassport', '[]', 0, '2020-09-09 03:55:47', '2020-09-09 03:55:47', '2021-09-08 20:55:47'),
('6eeff8565c5f7d8ae1855a0a7010ce8189874d16a45dff2d433bcc5a79f4a106e7b0bbbd1a047420', 234, 1, 'LaraPassport', '[]', 0, '2020-08-22 23:07:04', '2020-08-22 23:07:04', '2021-08-22 16:07:04'),
('6f4c2639a9a06ca41f4d4904de1b8d17a52b6c0cc6596713f9130a7950e3c513a3598cddeb61e2b3', 143, 1, 'LaraPassport', '[]', 0, '2020-07-17 13:45:18', '2020-07-17 13:45:18', '2021-07-17 06:45:18'),
('6f6254a16af6e351d1bb9f4130593c87b65823784543757499100e4f52df4c82fe0906e70d29c130', 356, 1, 'LaraPassport', '[]', 0, '2020-10-14 22:17:35', '2020-10-14 22:17:35', '2021-10-14 15:17:35'),
('6f754c225a1d7f9310246295176f0d240359f76170ca8398b2d500f4298c15aa84ddcf0d38b1a476', 33, 1, 'LaraPassport', '[]', 0, '2020-04-18 05:18:28', '2020-04-18 05:18:28', '2021-04-18 05:18:28'),
('6fdfd0ed4716f738e4db246f53cc139b8119ffaaf597d55228955d3ca3f0cd6ad80421762e86245f', 356, 1, 'LaraPassport', '[]', 0, '2020-10-14 22:36:23', '2020-10-14 22:36:23', '2021-10-14 15:36:23'),
('70729030d68829fbf8c0d9b6db73f9d3db9acac11530d63f31de21fad7dc936a77877a86a9433a0c', 153, 1, 'LaraPassport', '[]', 0, '2020-08-27 00:28:19', '2020-08-27 00:28:19', '2021-08-26 17:28:19'),
('70a59358f74354a0c1b0ac9e7c3505c4b6ed330c2f84f68f1fed494bf0e39aea96fe0cec0058c8c0', 57, 1, 'LaraPassport', '[]', 0, '2021-01-13 20:29:19', '2021-01-13 20:29:19', '2022-01-13 13:29:19'),
('70cc412ac8c2faa9da9b6e3856423fa3936ac3b4007352566bf04f0084a9b8847e046dd5c457581d', 389, 1, 'LaraPassport', '[]', 0, '2020-11-13 20:06:55', '2020-11-13 20:06:55', '2021-11-13 13:06:55'),
('70d62c25bac51623f7994207eabaaa4458509ad5dac728405e3ac57ee3dbb218206182d98a239fdc', 168, 1, 'LaraPassport', '[]', 0, '2020-07-30 19:52:20', '2020-07-30 19:52:20', '2021-07-30 12:52:20'),
('70e6b664fd63ee2487f35e6dfe5f366d81971bc05d89d6566da21805fc48b3cfc85df74a902f3435', 88, 1, 'LaraPassport', '[]', 0, '2020-06-10 05:09:38', '2020-06-10 05:09:38', '2021-06-10 05:09:38'),
('71cabee4cdb1d47cb729dec35bb25b860244114ab1e287ba935efd69cd5d78413f5f7cfa54add97b', 370, 1, 'LaraPassport', '[]', 0, '2020-10-24 22:40:42', '2020-10-24 22:40:42', '2021-10-24 15:40:42'),
('71d1e6208e3d75950e34f2a0afe547f4c519d309ae58eb4a0dadaa506fe0e952dc3b61a6d62546e5', 119, 1, 'LaraPassport', '[]', 0, '2021-01-19 00:17:30', '2021-01-19 00:17:30', '2022-01-18 17:17:30'),
('721df20a5aed93cfc15e0216eb7d8bc4cbd8146e3a774995b42d2e8d2b965d5f307ba3f84f584585', 57, 1, 'LaraPassport', '[]', 0, '2020-08-01 16:49:02', '2020-08-01 16:49:02', '2021-08-01 09:49:02'),
('722cf38c6d86f895c21387e38ad3d2b74f05973b5446fa272a849f234cd8b5f56339e43116e8a513', 376, 1, 'LaraPassport', '[]', 0, '2020-10-30 19:03:25', '2020-10-30 19:03:25', '2021-10-30 12:03:25'),
('728bd6c8f37a7d28cbfbe8985889c53d07a12831f9fa1bcdaaecf92ee41ea25abc02caabe311c063', 88, 1, 'LaraPassport', '[]', 0, '2020-06-11 05:53:27', '2020-06-11 05:53:27', '2021-06-11 05:53:27'),
('72e295021142137e6789ea947f190cfeccd3918cc4eda5b4a106522c3c8fdaa445d37412d4532a63', 33, 1, 'LaraPassport', '[]', 0, '2020-04-21 11:01:40', '2020-04-21 11:01:40', '2021-04-21 11:01:40'),
('73147676ac4ab1ebd70a5d1549c55a332547787ace9e678e0e5ca28c53d9827e1688279981097619', 1, 1, 'LaraPassport', '[]', 0, '2020-08-20 12:14:45', '2020-08-20 12:14:45', '2021-08-20 05:14:45'),
('7335aef708ca79d0065f17d24b1657735ec594139810ab3ac455c220d84aa00807eb989990b49971', 58, 1, 'LaraPassport', '[]', 0, '2020-04-22 05:42:52', '2020-04-22 05:42:52', '2021-04-22 05:42:52'),
('73db62f53d5e834c2cf251d4bbce373d3d74cf436150beb59ca0394b941c794ba1888e8981b9d72c', 35, 1, 'LaraPassport', '[]', 0, '2020-05-18 11:38:09', '2020-05-18 11:38:09', '2021-05-18 11:38:09'),
('74394dd068c13f737e5652f2915f267935bd1d1231c00fabf7e6ccb8ec01cd495ac838ed772e2949', 62, 1, 'LaraPassport', '[]', 0, '2020-04-29 14:54:07', '2020-04-29 14:54:07', '2021-04-29 14:54:07'),
('74446c11732376ae8c33fe18b6cf1e56d7a4951408e7aaa6ec6812d5cb7827295647f4b9698b08cb', 8, 1, 'LaraPassport', '[]', 0, '2020-04-09 13:02:55', '2020-04-09 13:02:55', '2021-04-09 13:02:55'),
('744d8a68b4b50ab9e77b7298671a6ea9627301bc76ea4beda858e81d4df8bbd49da7e88738960305', 238, 1, 'LaraPassport', '[]', 0, '2020-09-17 14:53:53', '2020-09-17 14:53:53', '2021-09-17 07:53:53'),
('744fda763207839b68377a2f3be681b8865e1ceb7c2e84d176c8bc6ade7cee3ba13f2f62b8d30942', 123, 1, 'LaraPassport', '[]', 0, '2020-07-05 07:04:13', '2020-07-05 07:04:13', '2021-07-05 07:04:13'),
('74b9a947f8749ee3f48f064c76d14a189ac290775cd593809280f7fe3af1b8819b39fb7c7434dd75', 8, 1, 'LaraPassport', '[]', 0, '2021-01-12 22:36:15', '2021-01-12 22:36:15', '2022-01-12 15:36:15'),
('74ea8ebff20acfd11b8e500ead98310bc1e15a170cd71ea47503f1618201e60e06a2073058f7942d', 35, 1, 'LaraPassport', '[]', 0, '2020-04-10 11:55:10', '2020-04-10 11:55:10', '2021-04-10 11:55:10'),
('7561be426a35c7a9c9c6e25906e558d2335cf9a0b080029b965be936cd3d3edd2128ff7b7eb501de', 247, 1, 'LaraPassport', '[]', 0, '2020-09-04 18:04:23', '2020-09-04 18:04:23', '2021-09-04 11:04:23'),
('7595e072de053e529b7762518d92a5c87350a1b07aa2374c3c9d367bdb8432c64be381c033ecd9e6', 57, 1, 'LaraPassport', '[]', 0, '2020-08-13 15:50:33', '2020-08-13 15:50:33', '2021-08-13 08:50:33'),
('759d524273168262fa2a90d6f6def55d99a56d4a42bfba93a4fbd1ef7019fbce9994a0e93b52d040', 266, 1, 'LaraPassport', '[]', 0, '2020-10-02 18:29:57', '2020-10-02 18:29:57', '2021-10-02 11:29:57'),
('75cacc71596fecec5231c77a51ae22336eb9b4ace68980492e5866ecb3df98f1d88aad58a96c0f42', 109, 1, 'LaraPassport', '[]', 0, '2021-01-16 02:59:01', '2021-01-16 02:59:01', '2022-01-15 19:59:01'),
('761dfc0d5bb3dbda664c184492498ef09fc9b395e6233a129718d35fb7534dfc8585eaaf36feffac', 434, 1, 'LaraPassport', '[]', 0, '2020-12-30 01:16:53', '2020-12-30 01:16:53', '2021-12-29 18:16:53'),
('762fee9c8d50c7f4dcbedf0787dd1b57c099eb419c5d87c5c7d696b74b4cf66716f7c29828afb1b5', 198, 1, 'LaraPassport', '[]', 0, '2020-08-17 21:38:09', '2020-08-17 21:38:09', '2021-08-17 14:38:09'),
('764765a0ae5ce05d4a87824e1f8cd4955433ead4176a0763e5eed4f0fd465631daa31aa8efab932c', 62, 1, 'LaraPassport', '[]', 0, '2020-08-17 21:40:24', '2020-08-17 21:40:24', '2021-08-17 14:40:24'),
('765994ab1119531dd2ed042db1d7f14ae7a454f40cc3e084db515c95f145df32a85c8327855cfde2', 247, 1, 'LaraPassport', '[]', 0, '2020-09-16 23:18:29', '2020-09-16 23:18:29', '2021-09-16 16:18:29'),
('766c696673bce11c8dcf366b1cc4e917b8d2baede9dccc9fc82c89bccd2f97794e75ff90f82d2dd7', 57, 1, 'LaraPassport', '[]', 0, '2020-04-24 12:49:23', '2020-04-24 12:49:23', '2021-04-24 12:49:23'),
('778b7e32031f51473209c21fa7be0250343835ce316da4f763bd45327d779b3ce6656890fed8ee35', 319, 1, 'LaraPassport', '[]', 0, '2020-09-25 18:27:34', '2020-09-25 18:27:34', '2021-09-25 11:27:34'),
('77921dd396ea8d0c5b1f4815fa2c6743a5257180b4a3ba8420ec9dc632fd98fcfcb65e1ddcd7c3d4', 419, 1, 'LaraPassport', '[]', 0, '2020-11-24 03:46:48', '2020-11-24 03:46:48', '2021-11-23 20:46:48'),
('77c4f5c45043f2cfa59f6673a4ff1ae605279e1605f13b85a7b500d3a4bdcd801d7a6d2112539557', 339, 1, 'LaraPassport', '[]', 0, '2020-10-05 17:52:24', '2020-10-05 17:52:24', '2021-10-05 10:52:24'),
('77e68a829fa6b58833052568e755c8d66299080df7516c2210dbbc1432e4a6c0042aa3f3a1702ced', 57, 1, 'LaraPassport', '[]', 0, '2020-08-18 11:32:25', '2020-08-18 11:32:25', '2021-08-18 04:32:25'),
('78703c1e889ae157ec11ab9d08b727f5f1197ee1c8687aaddf90ee3f55fbfbbd9b4311994446e729', 238, 1, 'LaraPassport', '[]', 0, '2020-09-04 22:30:38', '2020-09-04 22:30:38', '2021-09-04 15:30:38'),
('787d7b3a885b1628566334a5cc02e9f9686c26cc54b2f4050c34d092594f63fe1d5bb6addac99c25', 119, 1, 'LaraPassport', '[]', 0, '2021-01-19 20:02:20', '2021-01-19 20:02:20', '2022-01-19 13:02:20'),
('78a9067d2e61dbbe69776b946ea15e85f7b74e587f7527c139ef0a1853fba985b715c6f174bc8678', 219, 1, 'LaraPassport', '[]', 0, '2020-08-19 11:33:18', '2020-08-19 11:33:18', '2021-08-19 04:33:18'),
('78d0ea0e3706af65277481aa6f238b6aa82b6b1732518d631e8da658902c6b44d53efd4360b49ec1', 316, 1, 'LaraPassport', '[]', 0, '2020-10-23 21:58:53', '2020-10-23 21:58:53', '2021-10-23 14:58:53'),
('78e7c0376922130ba2ad33e32a4359449d7a276ed9bc98a68613b268f73667c7b9374716532de964', 57, 1, 'LaraPassport', '[]', 0, '2020-04-22 04:36:49', '2020-04-22 04:36:49', '2021-04-22 04:36:49'),
('7913b9ad25de1191716493237bf9d3a3912cde54a30fc524e0aacbae006ca71ef2b273c8e492a23d', 300, 1, 'LaraPassport', '[]', 0, '2020-11-22 20:38:24', '2020-11-22 20:38:24', '2021-11-22 13:38:24'),
('791c50bd62a6ca584ff9c097cefda7a10e6649ee8e3db7d8065fdb7532f996a5d7cf83ac1e16b099', 316, 1, 'LaraPassport', '[]', 0, '2020-09-23 16:45:28', '2020-09-23 16:45:28', '2021-09-23 09:45:28'),
('7922c18504938eddc74113cf815bbc8bb11678e3e0ee5bcfc6e840352e19f2f34795ff011d7887c3', 59, 1, 'LaraPassport', '[]', 0, '2020-04-25 15:32:54', '2020-04-25 15:32:54', '2021-04-25 15:32:54'),
('7947c5af586eaca57c7419cdbf4349c69d2b982fee95cbc5d5cec6a0239b99a93d4e0d5ca0aff4a9', 33, 1, 'LaraPassport', '[]', 0, '2020-04-13 09:04:14', '2020-04-13 09:04:14', '2021-04-13 09:04:14'),
('796b5100bbf8ada7f593f2c8dac51a573410015ff2dfbdb2f2fe7b167457fbd9ac566833bd85cd6d', 35, 1, 'LaraPassport', '[]', 0, '2020-04-13 08:54:49', '2020-04-13 08:54:49', '2021-04-13 08:54:49'),
('79d098a857be17b6f42acfdbb38193b6b3874773a7b01ed9009d24cdf6a69f327c2e257071e46654', 59, 1, 'LaraPassport', '[]', 0, '2020-05-15 13:16:39', '2020-05-15 13:16:39', '2021-05-15 13:16:39'),
('7a2137bb59e5ff577ed7d73088109efe655ca8c1b72a621d1c327f4b1c59874b030626fa551b988b', 153, 1, 'LaraPassport', '[]', 0, '2020-08-26 23:37:54', '2020-08-26 23:37:54', '2021-08-26 16:37:54'),
('7a8e1462c2bb2525c7e82f20d824f6f6a43a0e3e47522a672ca828a2aad5f2700eb33c2107dd2113', 201, 1, 'LaraPassport', '[]', 0, '2020-08-19 22:53:07', '2020-08-19 22:53:07', '2021-08-19 15:53:07'),
('7ac1c6c49c8b23666f3e42e6bdeac6b307a6c4f51f41c95b0c25ffdb55986131356dcdd250f1e41f', 35, 1, 'LaraPassport', '[]', 0, '2020-04-10 08:52:29', '2020-04-10 08:52:29', '2021-04-10 08:52:29'),
('7b69e218818fa426160dbe4418997b61aa1476e49768e3aee1ddeb5ede39f7290977451d9be0d98b', 97, 1, 'LaraPassport', '[]', 0, '2020-06-10 05:40:28', '2020-06-10 05:40:28', '2021-06-10 05:40:28'),
('7c0092154f192dc43e610c0873e29e53cf89a81c00f132e46a24e8d2b3331525efb2816082b8685c', 33, 1, 'LaraPassport', '[]', 0, '2020-04-18 13:50:40', '2020-04-18 13:50:40', '2021-04-18 13:50:40'),
('7c18eed17ed6005ff5019f5a98702dd1f5167e65f77d8db37635b4e0504dd65049e9a179b1cdf895', 368, 1, 'LaraPassport', '[]', 0, '2020-10-23 01:48:42', '2020-10-23 01:48:42', '2021-10-22 18:48:42'),
('7c29099aa91283769a60c875434b566f0138646be7e77a6f9764d71c8ffef2a862ae946caf41d9cf', 35, 1, 'LaraPassport', '[]', 0, '2020-05-18 19:11:35', '2020-05-18 19:11:35', '2021-05-18 19:11:35'),
('7c5f7195cf66d8ee1b4496ca5ded2fb4cb9747b8a3386731056c43e7c58de5bb8ba3aca5e02f7ed3', 8, 1, 'LaraPassport', '[]', 0, '2020-04-10 05:35:18', '2020-04-10 05:35:18', '2021-04-10 05:35:18'),
('7c97fd152a4fa969c570ceb3ce6d366d3df09bf6c33fbf695b3b9f95aa4064c8182d2313318f8d14', 211, 1, 'LaraPassport', '[]', 0, '2020-08-18 18:15:11', '2020-08-18 18:15:11', '2021-08-18 11:15:11'),
('7ca93e62b1790a9a4d7f50ea7e4320725ef3f580cadc5cbf9d5a63c7b5965041a2a1dcedf3d26ba2', 33, 1, 'LaraPassport', '[]', 0, '2020-04-21 11:09:00', '2020-04-21 11:09:00', '2021-04-21 11:09:00'),
('7cd50a8e8f938e151fe5f88b22b226794d3630846ae5efe5ffd90d02cd9cfaae67f13d1b04266b0c', 244, 1, 'LaraPassport', '[]', 0, '2020-10-24 11:14:54', '2020-10-24 11:14:54', '2021-10-24 04:14:54'),
('7cd848b62c3b97f0adba50c2ffc375dbe8832e0c37cfc1a7ae2c18bbf7a21523775d27d7dd5b2b7a', 57, 1, 'LaraPassport', '[]', 0, '2020-06-21 12:48:23', '2020-06-21 12:48:23', '2021-06-21 12:48:23'),
('7ce785fdefbd38d34b26de4fa400688c0a1a8167be38b296ad95b0687257aa3128888f1913599242', 33, 1, 'LaraPassport', '[]', 0, '2020-04-16 10:56:24', '2020-04-16 10:56:24', '2021-04-16 10:56:24'),
('7cfd5e1cf26dbed14d2319a0d2fbc542ec23b088e7ad1d35185b2a890738d3ec25f9aacde91e04bf', 8, 1, 'LaraPassport', '[]', 0, '2020-04-09 10:20:52', '2020-04-09 10:20:52', '2021-04-09 10:20:52'),
('7d20aae11f5708c132c67ccbf78e0032c2b93cccfaefc67253e5c0cbee87de9133484980a893b8bc', 33, 1, 'LaraPassport', '[]', 0, '2020-04-21 07:05:45', '2020-04-21 07:05:45', '2021-04-21 07:05:45'),
('7d58b70c0603dec7b4ea4a00fa8720addba86fc9bf3ae93e4f63024766289292acc0a3eb97f098ff', 200, 1, 'LaraPassport', '[]', 0, '2020-08-17 22:01:38', '2020-08-17 22:01:38', '2021-08-17 15:01:38'),
('7d8bfec1a249881b0d4619ae94db68a299198db8829ae2f7617e72cbc9cbcf9a90fa9aade2f937e5', 244, 1, 'LaraPassport', '[]', 0, '2020-09-12 11:43:34', '2020-09-12 11:43:34', '2021-09-12 04:43:34'),
('7da5bb9f78300b4d4bbece0f272ea47dbcd165ce840e41adc8410d9a97937c274e7bf799cefb9496', 225, 1, 'LaraPassport', '[]', 0, '2020-08-19 14:28:41', '2020-08-19 14:28:41', '2021-08-19 07:28:41'),
('7da7339ec3b8aedc5d5bc1236123bf04cbaaf5e2034eef9a520ce5db76b298c52f02402096b62146', 57, 1, 'LaraPassport', '[]', 0, '2020-05-07 11:09:27', '2020-05-07 11:09:27', '2021-05-07 11:09:27'),
('7dab2c9e9fe7f34dfb85b51fc25bcd8e58743cd4590c969e1f31b93ce6be6aa9f5a779af58ac9455', 35, 1, 'LaraPassport', '[]', 0, '2020-07-28 16:37:58', '2020-07-28 16:37:58', '2021-07-28 09:37:58'),
('7dce11e0475af113be483495b3a3e66e3cad903a9dcce85f3f7b94d2272addb6a3a4918707baa24a', 33, 1, 'LaraPassport', '[]', 0, '2020-04-22 09:36:27', '2020-04-22 09:36:27', '2021-04-22 09:36:27'),
('7e0266bef0684218bda0f9b06b7243cbed0abd8778db2b68e0981968ec6035a05e23bb98236b5df3', 250, 1, 'LaraPassport', '[]', 0, '2020-08-30 19:47:08', '2020-08-30 19:47:08', '2021-08-30 12:47:08'),
('7e1726edf5a35751de2bff9229906c2723a68a21c15faa9f94751c69fa61e069cc1102c9b4e437e9', 231, 1, 'LaraPassport', '[]', 0, '2020-10-09 18:31:23', '2020-10-09 18:31:23', '2021-10-09 11:31:23'),
('7e51b386cc609f331f7f852fb81891907cd6f0f2851485ed9320245636397563bd8da5c903888e61', 33, 1, 'LaraPassport', '[]', 0, '2020-04-10 13:58:43', '2020-04-10 13:58:43', '2021-04-10 13:58:43'),
('7e5f7b7b750f3b0a4dfad2af23b2600296f0bb9da1f185169b54da7351b354e02226e043484a277d', 234, 1, 'LaraPassport', '[]', 0, '2020-08-21 15:31:23', '2020-08-21 15:31:23', '2021-08-21 08:31:23'),
('7e8f8400d4dd43a6fd3e908e77ff3c0455305844058e4081317f5d0f8ee0d5089f22d76f0176ccc3', 59, 1, 'LaraPassport', '[]', 0, '2020-05-02 13:45:30', '2020-05-02 13:45:30', '2021-05-02 13:45:30'),
('7ecdff91eecad2c23145cde5f72e83dd9aa58acbeaa702e477b7fb94437b8e816ddffc98ad408870', 57, 1, 'LaraPassport', '[]', 0, '2020-06-22 05:27:35', '2020-06-22 05:27:35', '2021-06-22 05:27:35'),
('7f0fa442e4ea3ec29fc0e6bf526c4128e80fe2d187debcf576abbd777185a71189c124b572adf800', 231, 1, 'LaraPassport', '[]', 0, '2020-10-09 18:30:17', '2020-10-09 18:30:17', '2021-10-09 11:30:17'),
('7f6acb106a4fbea000f0bda3f96afc4d073a208767ed77bc26030c6c3235480fab9cd7fbbac3411a', 33, 1, 'LaraPassport', '[]', 0, '2020-04-13 05:47:38', '2020-04-13 05:47:38', '2021-04-13 05:47:38'),
('7f745bf74d9679150397fa95b7f554f793e9344d5b19b591931e3fc026fb7ee565c484e1c96544bd', 35, 1, 'LaraPassport', '[]', 0, '2020-05-16 15:52:24', '2020-05-16 15:52:24', '2021-05-16 15:52:24'),
('7f889d12ef3639a9b848705df35a1b14345b1dbdf5942bf5faac3efb5bb8c64a9c28f8a48dbd42a8', 8, 1, 'LaraPassport', '[]', 0, '2020-04-09 12:22:48', '2020-04-09 12:22:48', '2021-04-09 12:22:48'),
('7fe04e39fd7d21c6c841f5a4e6b20d0fb6e7207e370122cd1b3bdf64743d0a2437edc1638de42516', 33, 1, 'LaraPassport', '[]', 0, '2020-04-20 14:07:25', '2020-04-20 14:07:25', '2021-04-20 14:07:25'),
('7ff38b94b19d9e5d4d9882aecc6cdd833ca9ad85e930c3b4b5e56f95c8d497ee2af6e1441ae0fe79', 35, 1, 'LaraPassport', '[]', 0, '2020-06-29 06:26:53', '2020-06-29 06:26:53', '2021-06-29 06:26:53'),
('801efa9c96ec9230330aa324f356fa81a5971f2c663072b064d1fb40026c186c75f6c45f168a156f', 247, 1, 'LaraPassport', '[]', 0, '2020-09-02 13:06:32', '2020-09-02 13:06:32', '2021-09-02 06:06:32'),
('801f1338c3affb7c2ef966f03e577da1d44f0f0de5af8207758359e71dc6faaa00a091405e8eae72', 119, 1, 'LaraPassport', '[]', 0, '2021-01-21 22:48:14', '2021-01-21 22:48:14', '2022-01-21 15:48:14'),
('80441a03d475cccc0d4dc16e61b6b8f8b3b1ccf6d044e5c8494f466e589100790cf2617e55374f07', 62, 1, 'LaraPassport', '[]', 0, '2020-04-27 06:42:32', '2020-04-27 06:42:32', '2021-04-27 06:42:32'),
('806545e19cbb9da2681e9f8a6db2a3ac5f9ff838ea8040521bcc139d5d00aecabb19eac0078b9b5d', 57, 1, 'LaraPassport', '[]', 0, '2020-08-05 16:33:20', '2020-08-05 16:33:20', '2021-08-05 09:33:20'),
('80a88740b05fdec4a4974864f2ced120ccd736bd4086f1059eec406202267adefc214b4dbfbd46fb', 119, 1, 'LaraPassport', '[]', 0, '2021-01-19 20:05:40', '2021-01-19 20:05:40', '2022-01-19 13:05:40'),
('80dc6f4365c40edbd34b4cd40147a20fafd955b64b0037a644e0ef24d1aa470539f7071d899917c5', 234, 1, 'LaraPassport', '[]', 0, '2020-08-22 23:11:44', '2020-08-22 23:11:44', '2021-08-22 16:11:44'),
('80f0462f7a05f9a4fa671f2d335af921878e9cb2c3fca57975f267b20a3b6e75b2dfd9559d8cd1a6', 244, 1, 'LaraPassport', '[]', 0, '2020-09-22 19:49:53', '2020-09-22 19:49:53', '2021-09-22 12:49:53'),
('81b70a290bd27ca3a06eb614e5d72dc60a41de48ca992cd8b7eb7da5686ad7de66dd204a6679b08e', 238, 1, 'LaraPassport', '[]', 0, '2020-09-09 15:07:43', '2020-09-09 15:07:43', '2021-09-09 08:07:43'),
('81b882838d9ad72c7b591c48525c42bc26ed5ff3dbfe6efd929b9b70c331eb90769bd66bd31b77ff', 35, 1, 'LaraPassport', '[]', 0, '2020-06-01 10:08:58', '2020-06-01 10:08:58', '2021-06-01 10:08:58'),
('81d1c4deb87e810153ce6c6efbbfa4faf1b94e42d90c6e47dcd18f3623668e7e85613605cb30c898', 283, 1, 'LaraPassport', '[]', 0, '2020-09-11 17:47:55', '2020-09-11 17:47:55', '2021-09-11 10:47:55'),
('81d2914bf9018686a058c668e8885423bf0e0de74637f1d00698b64929a3bb78ca98debadb192039', 58, 1, 'LaraPassport', '[]', 0, '2020-04-22 06:02:33', '2020-04-22 06:02:33', '2021-04-22 06:02:33'),
('821079f25ded4e48f7db8e36cc622e9e1219cf09fbe1f65d7358791890d7bcd16b368a7eda867bd0', 57, 1, 'LaraPassport', '[]', 0, '2020-08-01 16:28:19', '2020-08-01 16:28:19', '2021-08-01 09:28:19'),
('82121066b898bf16cc04831f54abf9840c31da3e11936043b8f2b796522b85c202de3f9b9244e303', 231, 1, 'LaraPassport', '[]', 0, '2020-10-02 18:34:55', '2020-10-02 18:34:55', '2021-10-02 11:34:55'),
('82406d4b78f27dfd23072b9d3956ba2866143157166b7e4c1f2f605e45698909775cc643fa4c41e2', 58, 1, 'LaraPassport', '[]', 0, '2020-04-22 04:51:33', '2020-04-22 04:51:33', '2021-04-22 04:51:33'),
('824623ba7bae94c188b42a5ecee5ab22e6db90051753ba0642c7eb24ad69c63c3e8657229c9ee488', 394, 1, 'LaraPassport', '[]', 0, '2020-11-11 07:50:22', '2020-11-11 07:50:22', '2021-11-11 00:50:22'),
('825d96ce55b0d9ec421636b3e435446291b67385704377a0e8ea204b01d8835a41d4c7d6fd17491b', 17, 1, 'LaraPassport', '[]', 0, '2020-04-09 05:10:13', '2020-04-09 05:10:13', '2021-04-09 05:10:13'),
('82a5fd3e16a4c76dd5fbf64266b2c68fad99bc6882f58ffc37759ffe624677bf45509a12631b076a', 417, 1, 'LaraPassport', '[]', 0, '2020-11-24 03:29:51', '2020-11-24 03:29:51', '2021-11-23 20:29:51'),
('82e9623504a8ee350e251325ac59612b079ca8e5e31c3530cf30cf85ec6db6b7fc77c8e4e07b9cb0', 33, 1, 'LaraPassport', '[]', 0, '2020-04-21 14:14:19', '2020-04-21 14:14:19', '2021-04-21 14:14:19'),
('8308603f5fcf7360e4a3539c453fdb00c8a1a31efb034954cf4ed4a1850dd9144a20053c316134f7', 266, 1, 'LaraPassport', '[]', 0, '2020-11-13 19:39:51', '2020-11-13 19:39:51', '2021-11-13 12:39:51'),
('833e87111dd143e5f0dda10624833331ae6cd0006aef2b6cee9ae27744d9f7c8644cd07c3a41bc07', 236, 1, 'LaraPassport', '[]', 0, '2020-08-22 23:14:54', '2020-08-22 23:14:54', '2021-08-22 16:14:54'),
('839f4f19ca2282ab0cc54701e20f54d9b7b056d21f423f43526f55164d048f2a328077a3b8037d24', 231, 1, 'LaraPassport', '[]', 0, '2020-10-09 17:27:42', '2020-10-09 17:27:42', '2021-10-09 10:27:42'),
('83a35762b6a2d54db0d0274ee1250d62d60c3451e913e4d63eb0ed8503b73c62c07808b53c8faeca', 35, 1, 'LaraPassport', '[]', 0, '2020-06-13 09:54:31', '2020-06-13 09:54:31', '2021-06-13 09:54:31'),
('83b36859f63e0235822ab6ee4495e5e192c3b18d12572e8c3bd0dd27e3d07ec49160390ca2d5a300', 62, 1, 'LaraPassport', '[]', 0, '2020-04-30 10:34:02', '2020-04-30 10:34:02', '2021-04-30 10:34:02'),
('83b6d788c5248de7b7f957856f807fb36b158c08e5a675abc59a021140c43e601654101897cc6fd6', 29, 1, 'LaraPassport', '[]', 0, '2020-04-09 10:36:05', '2020-04-09 10:36:05', '2021-04-09 10:36:05'),
('83e161d1579d57b2bea06ef8328a459b4c13fb2e658e3f5af20d1d08edcfe867f59494f37a9dbcc0', 57, 1, 'LaraPassport', '[]', 0, '2020-06-25 06:09:08', '2020-06-25 06:09:08', '2021-06-25 06:09:08'),
('83e7d74d5fc429b83a1381198d5d29c7e8808aec9b5cb0da7101c5e2b62b989e6407c23fc2d56365', 35, 1, 'LaraPassport', '[]', 0, '2020-07-17 14:00:05', '2020-07-17 14:00:05', '2021-07-17 07:00:05'),
('8412892462f2165f97f3f2bcc4c750420c10c6d6a04f3b35aa3fdeb93954d43b07eedead099e6ead', 57, 1, 'LaraPassport', '[]', 0, '2020-06-25 10:35:40', '2020-06-25 10:35:40', '2021-06-25 10:35:40'),
('8419158dd1861d83eb2c455a1c0241ab0c3cddf9ca74d7e8567041ef41464232ce4944ce3650b892', 237, 1, 'LaraPassport', '[]', 0, '2020-08-24 12:21:23', '2020-08-24 12:21:23', '2021-08-24 05:21:23'),
('8463331e586fa636219a3e220067eabf9c5f5deedde9b5cfa2c541a536e1e8911b6f9a9366706d38', 244, 1, 'LaraPassport', '[]', 0, '2020-09-25 12:45:46', '2020-09-25 12:45:46', '2021-09-25 05:45:46'),
('84b6227632606833e56be17c7d71c60a2901f103225b2badb510064c5aff5f44c735e52577cf4d93', 33, 1, 'LaraPassport', '[]', 0, '2020-04-20 15:11:52', '2020-04-20 15:11:52', '2021-04-20 15:11:52'),
('84f1b4cecb8f7caa83d31b592a49628525139e4724d016aac67a0753756cd483f974a2ec1e8cba94', 380, 1, 'LaraPassport', '[]', 0, '2020-11-04 01:19:16', '2020-11-04 01:19:16', '2021-11-03 18:19:16'),
('857cd8442fe7b6e0a484d2c89a085ff12c1e88ff93b7219d8dc312c64020cdd18c7c4d21d474a7fe', 266, 1, 'LaraPassport', '[]', 0, '2020-10-06 18:53:16', '2020-10-06 18:53:16', '2021-10-06 11:53:16'),
('858f8fce9b66a79576a363f11badc81b47fb63fdf7539b0d87582b6edbf28255e0b79d79ee5cb641', 197, 1, 'LaraPassport', '[]', 0, '2020-08-17 15:21:53', '2020-08-17 15:21:53', '2021-08-17 08:21:53'),
('8597816d82a4a9707e01938f7d758517aac43755e95d3c0587657bc7e92467317bc29c0ff664aa96', 35, 1, 'LaraPassport', '[]', 0, '2020-05-13 12:56:29', '2020-05-13 12:56:29', '2021-05-13 12:56:29'),
('866e1fcb755da86fc1c9052adeb29e0b5464e29c9d7a39c781d37152b6c45e6030b1a44a0d24f811', 35, 1, 'LaraPassport', '[]', 0, '2020-05-13 16:57:33', '2020-05-13 16:57:33', '2021-05-13 16:57:33'),
('86fa47b9664147a0609eadfb44ad4389133410c28d69dfd9c1864768153b864cf18ac419c5246beb', 356, 1, 'LaraPassport', '[]', 0, '2020-10-14 12:50:43', '2020-10-14 12:50:43', '2021-10-14 05:50:43'),
('87386790f1f5c9f71679ea509431703eaf13b32b1ce86f0e34913d1f36000f0f46958b8ad0d93cfa', 240, 1, 'LaraPassport', '[]', 0, '2020-08-24 15:27:55', '2020-08-24 15:27:55', '2021-08-24 08:27:55'),
('8763bbdbd5a1b98fb714f49a5134d72ba64e1d855e314abf310ce96085e5e19f85f2616e980e8540', 27, 1, 'LaraPassport', '[]', 0, '2020-04-09 06:21:21', '2020-04-09 06:21:21', '2021-04-09 06:21:21'),
('8808d0ec259f161ab0b0a87a92aa0dfdaaa31b6d9c864524bb867b66e7a801f0553c3ec098db77f8', 57, 1, 'LaraPassport', '[]', 0, '2020-08-04 12:31:07', '2020-08-04 12:31:07', '2021-08-04 05:31:07'),
('88194ba3c05f090762f46811e90193d0b685c17e1d8962bde2f0b0b86c12545b5a9d69da20eca500', 215, 1, 'LaraPassport', '[]', 0, '2020-08-18 18:52:28', '2020-08-18 18:52:28', '2021-08-18 11:52:28'),
('881e674cc35a0e2e0f21c6c92be94e04abdad6f4a1bb69aea9290172ffd4ae81ec9b21c344673ebd', 33, 1, 'LaraPassport', '[]', 0, '2020-04-18 05:21:03', '2020-04-18 05:21:03', '2021-04-18 05:21:03'),
('885d205fc44b00d57be095f0e6617ea99a9216606fb2dfaa18268d815fa402dd19d678b4bd88b8c1', 119, 1, 'LaraPassport', '[]', 0, '2021-01-19 20:02:13', '2021-01-19 20:02:13', '2022-01-19 13:02:13'),
('8880b8471dffbf8ebf48f8fe358eea1fb834776c1896ef2f25fe1350a1753bcb0c9f6e7a11810b9b', 35, 1, 'LaraPassport', '[]', 0, '2020-06-27 10:31:04', '2020-06-27 10:31:04', '2021-06-27 10:31:04'),
('888589c7d3214f3f0f504e3131c3d7567c8b69138ef305a9e19a185855784290b1e9a9d3f0f0a89b', 57, 1, 'LaraPassport', '[]', 0, '2020-06-22 09:11:47', '2020-06-22 09:11:47', '2021-06-22 09:11:47'),
('89261e1adebbdcd19cec4695be369f80909d66b8bb9d949a168ba6409868f40fa9f72737184a2d38', 304, 1, 'LaraPassport', '[]', 0, '2020-09-17 00:20:43', '2020-09-17 00:20:43', '2021-09-16 17:20:43'),
('898ef1a3c1b78a03ea0e40c85066c7363ee1484024dcdaffabd5c9a5820f9fd7248f416257487ffe', 35, 1, 'LaraPassport', '[]', 0, '2020-06-27 07:01:56', '2020-06-27 07:01:56', '2021-06-27 07:01:56'),
('89c33e57ff8741395938920051ef27f5814eb0f7e7484f36da6b18abc28945af060ece8439bad8ba', 369, 1, 'LaraPassport', '[]', 0, '2020-10-23 15:58:46', '2020-10-23 15:58:46', '2021-10-23 08:58:46'),
('89cd895821cbe8cc1329fd7b5635ca5e33cba6d3d1f95f39aa42ac56681978a5617aa39a3d101d38', 109, 1, 'LaraPassport', '[]', 0, '2021-01-16 02:55:03', '2021-01-16 02:55:03', '2022-01-15 19:55:03'),
('8a2e4683dc6b6af3cbfb97b435aca2b1f152adbe6c102ac48b08d90793e9cb2143458d5c8f1ab7bb', 35, 1, 'LaraPassport', '[]', 0, '2020-06-09 05:43:07', '2020-06-09 05:43:07', '2021-06-09 05:43:07'),
('8a3c486a7ac03fac1e909deda4f4af41936de2b946db3c3d8169ef6b0cda6a7cfd962b7682ea8a34', 33, 1, 'LaraPassport', '[]', 0, '2020-04-14 06:24:29', '2020-04-14 06:24:29', '2021-04-14 06:24:29'),
('8a672cddd085ae107e857130cf7e735a32fe217f0dfb3a4f351161b8a167c4214f0c0b31dcbfd0c1', 119, 1, 'LaraPassport', '[]', 0, '2021-01-19 21:50:24', '2021-01-19 21:50:24', '2022-01-19 14:50:24'),
('8b0749ba4a80536cf10e84086ec03cdbd79cf6516c4f5039a1025ca677a11c023a6981cad567d707', 57, 1, 'LaraPassport', '[]', 0, '2020-06-25 06:00:39', '2020-06-25 06:00:39', '2021-06-25 06:00:39'),
('8b778055b2a6f76b402d3cb3f4a7b27f16dc468a5eb3d0ddc4bc2c0bf38b12c2617c5776f9a015d2', 57, 1, 'LaraPassport', '[]', 0, '2020-05-01 04:00:04', '2020-05-01 04:00:04', '2021-05-01 04:00:04'),
('8b85d12de2a8753b1456d7994698172ec47835bd74ea16060410541cf1ce04fc74233c02e30855a8', 377, 1, 'LaraPassport', '[]', 0, '2020-10-30 17:09:54', '2020-10-30 17:09:54', '2021-10-30 10:09:54'),
('8bf3f8100f834bb13f6b070ac004302533b6fb1fe9f9dcb7952ae765f17cd33b54a2ee0e5f44638e', 57, 1, 'LaraPassport', '[]', 0, '2020-05-12 11:52:49', '2020-05-12 11:52:49', '2021-05-12 11:52:49'),
('8bf40e02abf195fcf4adbea9ecd76f53390d99456ad496da09763628f0fc249df9b09964d31db11f', 110, 1, 'LaraPassport', '[]', 0, '2021-01-16 02:42:04', '2021-01-16 02:42:04', '2022-01-15 19:42:04'),
('8c05133317d58292b335319903c2ae354ab58b376588619d316f65d3d501d3587467a88442e68738', 333, 1, 'LaraPassport', '[]', 0, '2020-10-01 16:29:33', '2020-10-01 16:29:33', '2021-10-01 09:29:33'),
('8c9c4ac02661865dfb117cd0c99db497b4c15a2b57a537106eca065d26c9f8e0f227f84619430256', 35, 1, 'LaraPassport', '[]', 0, '2020-06-04 17:27:09', '2020-06-04 17:27:09', '2021-06-04 17:27:09'),
('8ca3e6142393f307946304112a5281d45616b65fb0a9643cab97c01af186754fde482c7510617ce7', 421, 1, 'LaraPassport', '[]', 0, '2020-11-25 22:04:25', '2020-11-25 22:04:25', '2021-11-25 15:04:25'),
('8cd5c02dc5ae9015255bbdf5de599be24b3f22baba2ef3940d7ccee2edb3e6d4858d2ff25e449a32', 304, 1, 'LaraPassport', '[]', 0, '2020-09-17 00:06:49', '2020-09-17 00:06:49', '2021-09-16 17:06:49'),
('8d909c51cc74802c2f78c0fbaba565d2509b53fe4e88d13423ddeb4b1132847e2eac66a8e9249778', 57, 1, 'LaraPassport', '[]', 0, '2020-08-04 18:37:40', '2020-08-04 18:37:40', '2021-08-04 11:37:40'),
('8dc0a6acdf4dc39a82ae4a5bfa61fc04bc0e1c08f16790f8aadbe1449665f0d750cfd118725a2278', 33, 1, 'LaraPassport', '[]', 0, '2020-04-17 08:59:11', '2020-04-17 08:59:11', '2021-04-17 08:59:11'),
('8df626b777f56b8c44b523478a1de0c43b053caa605458b5849142476eb3ba20744791e359dc1a9b', 153, 1, 'LaraPassport', '[]', 0, '2020-08-20 15:10:18', '2020-08-20 15:10:18', '2021-08-20 08:10:18'),
('8dfbee041e349a64a4acc8deef6a3890273379ebda57e70fd224920483556f4cda09898beec3376c', 142, 1, 'LaraPassport', '[]', 0, '2020-07-28 12:29:55', '2020-07-28 12:29:55', '2021-07-28 05:29:55'),
('8e083038d8d9e012dc863f7b1fe334f849ffc08e502217c8f94686784a17e052b0716ac604c48987', 247, 1, 'LaraPassport', '[]', 0, '2020-09-04 17:47:49', '2020-09-04 17:47:49', '2021-09-04 10:47:49'),
('8e46f55ed127b5e28207683ae25da98950b54e0552e6c05354f48b77e0e5755ed2b0c2c5595388e7', 120, 1, 'LaraPassport', '[]', 0, '2020-06-27 11:09:14', '2020-06-27 11:09:14', '2021-06-27 11:09:14'),
('8e9b993f9f687adcd4b5e8457f088db6cac3773edab46729b9d9419744058e9b9d0ee4b33a1fb0ea', 119, 1, 'LaraPassport', '[]', 0, '2021-01-19 20:47:00', '2021-01-19 20:47:00', '2022-01-19 13:47:00'),
('8e9c00a1bbf0d2e57b56cc085206a7745ffa4994326be4065459d027d5af725a03a1503bb042f8bb', 417, 1, 'LaraPassport', '[]', 0, '2020-11-22 20:38:01', '2020-11-22 20:38:01', '2021-11-22 13:38:01'),
('8ef45d02da7d26f3005b65009aebc5be1d910aa2e573d9affcd62b198ddb92e89f95c1e11c51ba0b', 153, 1, 'LaraPassport', '[]', 0, '2020-08-24 15:54:46', '2020-08-24 15:54:46', '2021-08-24 08:54:46'),
('8f1526f8f4d986f0c3470e60466d3c09ea06ffc62edc24290e1f33f694f2a27c1e761f3832752c81', 312, 1, 'LaraPassport', '[]', 0, '2020-09-30 16:45:32', '2020-09-30 16:45:32', '2021-09-30 09:45:32'),
('8f3626b08426823c76fd56b1f7d65ee09dc51a04042d3d1c97fbfe534c514a1479600cf537829751', 57, 1, 'LaraPassport', '[]', 0, '2020-08-01 16:59:59', '2020-08-01 16:59:59', '2021-08-01 09:59:59'),
('8f5eb2a38cbc631e0ca523ba41bc14f17800b7987cb318fcc1d775317ae44bb694a09b4210f34987', 266, 1, 'LaraPassport', '[]', 0, '2020-09-10 12:37:47', '2020-09-10 12:37:47', '2021-09-10 05:37:47'),
('906c2a21928acede100faa823e8fcfac324f137fdf552ffdc6b6d08d13977ec35f9f58f98dc5588f', 376, 1, 'LaraPassport', '[]', 0, '2020-10-29 12:25:33', '2020-10-29 12:25:33', '2021-10-29 05:25:33'),
('90ac9505dba1a12017049b922f450990ad6075d252601049514927dc1d3268cdd4e8c09e9cce882d', 142, 1, 'LaraPassport', '[]', 0, '2020-07-17 13:46:15', '2020-07-17 13:46:15', '2021-07-17 06:46:15'),
('90d7bb45a9c34fdfb4b8a5e5bb8b7b062435da3970cd85ce5d027d3bfc9dc9363140d4e8c75d6e01', 238, 1, 'LaraPassport', '[]', 0, '2020-09-17 14:57:02', '2020-09-17 14:57:02', '2021-09-17 07:57:02'),
('90ddd88c001e3414f5b2c5b6c0fe388f01c818476c53fafd3d59e1c321f1f5ae81171e41531dafae', 130, 1, 'LaraPassport', '[]', 0, '2021-01-21 23:47:46', '2021-01-21 23:47:46', '2022-01-21 16:47:46'),
('90f68188813f1067adc0fc3ca61b7978f87262591accb99ed3a478ca6709c8e09a5576dc1acc52e0', 278, 1, 'LaraPassport', '[]', 0, '2020-09-09 03:55:25', '2020-09-09 03:55:25', '2021-09-08 20:55:25'),
('9114309c3bdac10f3e8b29d774044e3a0d9709ee90093c55432a0c40fade9ee8b696ba5e54a65b41', 57, 1, 'LaraPassport', '[]', 0, '2020-04-26 05:39:10', '2020-04-26 05:39:10', '2021-04-26 05:39:10'),
('911b4e859c4c9f4b432ef6189df0be8e53ab67efd8ee56d2310b6c8f864ce67ed9ec1ff09690a306', 244, 1, 'LaraPassport', '[]', 0, '2020-10-09 19:58:22', '2020-10-09 19:58:22', '2021-10-09 12:58:22'),
('91740c56bbf2c8f271f65f28e8bef7458d5e651b84c7a1cf3c1c3d77d900e7c8f5f35060e8b3bbb8', 422, 1, 'LaraPassport', '[]', 0, '2020-11-30 03:50:46', '2020-11-30 03:50:46', '2021-11-29 20:50:46'),
('91d9ad3758a563bd6f9ae596fe3649c327c0364d008552745b68ce4cc89df4ec60e9e2f16df431e6', 389, 1, 'LaraPassport', '[]', 0, '2020-11-10 19:11:45', '2020-11-10 19:11:45', '2021-11-10 12:11:45'),
('923ed64f3553205bdf1f92adff5d76fadbc966995e1a490f423651af0abf08b7a8175daa44223ea6', 57, 1, 'LaraPassport', '[]', 0, '2020-08-20 16:26:46', '2020-08-20 16:26:46', '2021-08-20 09:26:46'),
('92f8da405ebb0512a887e104194912b361e2bb4d2e016838832b79a8f4cc45e1d66e8cb90fef7513', 62, 1, 'LaraPassport', '[]', 0, '2020-05-01 09:53:49', '2020-05-01 09:53:49', '2021-05-01 09:53:49'),
('933a7a63f9bbabd23d4d2ca591f51503306ce680cb6d82553fb38cf45060fba8f5510705f59d56fb', 97, 1, 'LaraPassport', '[]', 0, '2020-06-10 06:38:41', '2020-06-10 06:38:41', '2021-06-10 06:38:41'),
('93c45b1b16dec852ba4ba41f89e9fff470497a250e0abfd652297da5acbf37170bbd44e9d3b3f300', 57, 1, 'LaraPassport', '[]', 0, '2020-08-01 17:23:44', '2020-08-01 17:23:44', '2021-08-01 10:23:44'),
('93d41955dabeb23cc3a0e8e7dfb928719442ace3531da14a56c577ac61403e0a1a20cd25df78e67e', 33, 1, 'LaraPassport', '[]', 0, '2020-04-11 10:03:02', '2020-04-11 10:03:02', '2021-04-11 10:03:02'),
('9416db6211a36d66389a04213419516e9529d8882f27a698aa375eebbf741205117c57d7744236cd', 33, 1, 'LaraPassport', '[]', 0, '2020-04-20 14:06:21', '2020-04-20 14:06:21', '2021-04-20 14:06:21'),
('946f44fdfc09d4b776dbe74e04ceb5f44ece44365abd14339500cc9f01bf14fb6e0e2b0edf9751da', 1, 1, 'LaraPassport', '[]', 0, '2020-04-16 06:51:46', '2020-04-16 06:51:46', '2021-04-16 06:51:46'),
('94e10295d5d851858ada8b9ee454bf45db5abeff3c5172cd78bc7d12f78bba7c60e5132fd532431c', 33, 1, 'LaraPassport', '[]', 0, '2020-04-21 11:08:59', '2020-04-21 11:08:59', '2021-04-21 11:08:59'),
('94ec181f8863b23fee6360d8dd198cc09caba7ee72c36efa13da47000805b13025b09794ec9f0dd9', 119, 1, 'LaraPassport', '[]', 0, '2021-01-19 22:37:47', '2021-01-19 22:37:47', '2022-01-19 15:37:47'),
('9539e241ecd0ecdc91062480c4cae176fbe044ba4e0471aa91228f1b67a1f0f21ba73e8f8b099acd', 56, 1, 'LaraPassport', '[]', 0, '2020-04-17 10:09:10', '2020-04-17 10:09:10', '2021-04-17 10:09:10'),
('957ae1c25c4d3490b8592cbc8a5e5830268947bbe3726f4a403ae86ac4f72b054fb57aec345c4f69', 57, 1, 'LaraPassport', '[]', 0, '2020-08-19 13:21:51', '2020-08-19 13:21:51', '2021-08-19 06:21:51'),
('959e9c91c8f87de4b0022d15ba61a29e8c358a2300fa5bd4cbd18739ed1dca8a996008fa8093c6e9', 33, 1, 'LaraPassport', '[]', 0, '2020-04-18 04:37:28', '2020-04-18 04:37:28', '2021-04-18 04:37:28'),
('95aa206d8ba162f78eb858bd20d895031b152b6143ddc240c97e6174624e38ce770067cc9a615f07', 33, 1, 'LaraPassport', '[]', 0, '2020-04-16 10:29:43', '2020-04-16 10:29:43', '2021-04-16 10:29:43'),
('95aa997ceb3abae952c4521a8a2ce55f856e372584413fc8054fd523e79f1e8b2800da591c20f35c', 57, 1, 'LaraPassport', '[]', 0, '2020-07-05 18:12:01', '2020-07-05 18:12:01', '2021-07-05 18:12:01'),
('95c831b0238cb94c83ee97f656ac09effccf06104f94cc3a36de1062a864093efbdf8288f391313c', 59, 1, 'LaraPassport', '[]', 0, '2020-04-22 13:47:13', '2020-04-22 13:47:13', '2021-04-22 13:47:13'),
('95d18525f9c49217394f0f0cd92dcdfd07374163e47551712617027b38aa39d0edc0062fd67a5cf7', 35, 1, 'LaraPassport', '[]', 0, '2020-07-31 14:50:06', '2020-07-31 14:50:06', '2021-07-31 07:50:06'),
('962166e849d916ffd486b886c828c09efab108aaa481246d99a6da2ea270c2ce693d4163fbaaabe4', 424, 1, 'LaraPassport', '[]', 0, '2020-12-02 01:12:37', '2020-12-02 01:12:37', '2021-12-01 18:12:37'),
('962bf9fa37a8d126a0578afeb989c7b38cad05e528929a0945f71a823630235fc8329ac4365a0963', 2, 1, 'LaraPassport', '[]', 0, '2020-03-19 00:40:57', '2020-03-19 00:40:57', '2021-03-19 06:10:57'),
('9647865187c16a22662994f7d8a1c52cffa1f7b6eb56c05f1573c82fc0e2bad7f4729cf43783db04', 57, 1, 'LaraPassport', '[]', 0, '2020-08-05 16:51:54', '2020-08-05 16:51:54', '2021-08-05 09:51:54'),
('965170f5a2e819a3565c40172bd1a90eabfd4723d8407a4498fb8bc81e090cc4da5ca1c3db987222', 256, 1, 'LaraPassport', '[]', 0, '2020-09-01 19:52:30', '2020-09-01 19:52:30', '2021-09-01 12:52:30'),
('96b91136d592edca3537c229da904d40abd470284cf192fe8eca085260c3160b792d098649ac85ba', 8, 1, 'LaraPassport', '[]', 0, '2020-04-10 05:18:59', '2020-04-10 05:18:59', '2021-04-10 05:18:59'),
('979437b2df81f83c8c09e3d62e0f3862886bc9451ee1753e6e01f5678a13a8c4cac33bd8a8f6bd34', 266, 1, 'LaraPassport', '[]', 0, '2020-09-28 18:15:44', '2020-09-28 18:15:44', '2021-09-28 11:15:44'),
('9797872d5597ab5f0b52f39f95b59dc4a34eaaa42d3ebe04f89ba1deb07bd84c240d3bbb1523cfad', 57, 1, 'LaraPassport', '[]', 0, '2020-08-05 11:41:56', '2020-08-05 11:41:56', '2021-08-05 04:41:56'),
('97b27a6ef8401211579b8398aa10260d23de6b909e50863d94f27fa4ef5e500cab136cdd6e4e29fa', 266, 1, 'LaraPassport', '[]', 0, '2020-09-28 16:21:47', '2020-09-28 16:21:47', '2021-09-28 09:21:47'),
('97db28ac1e8d8f9aa2ffe53f8cfc34dc5b698e51c279625de95802984088945c827905f4a880baf9', 33, 1, 'LaraPassport', '[]', 0, '2020-04-16 10:32:11', '2020-04-16 10:32:11', '2021-04-16 10:32:11'),
('9810c9078ff278886225dd84a20bf011b57da56d7d1fbb74a8dbf570d3b5ba27357590d4299a175c', 340, 1, 'LaraPassport', '[]', 0, '2020-10-06 18:00:28', '2020-10-06 18:00:28', '2021-10-06 11:00:28'),
('982a5fb3782ed041ae67d5dd0c9081b0abe80f046b2db067ff1d757834fb7c5e253d91498ab5815d', 8, 1, 'LaraPassport', '[]', 0, '2020-04-10 05:54:42', '2020-04-10 05:54:42', '2021-04-10 05:54:42'),
('982c444c92a9cc9d36a477e39373f57e7988ef33742ad7f3251a01d70305bc8c109efa8fbf435bac', 33, 1, 'LaraPassport', '[]', 0, '2020-04-18 14:11:48', '2020-04-18 14:11:48', '2021-04-18 14:11:48'),
('985f2bfac384e069f3474ba4365d40a54ea6567257882f96f3878fb81835833b3109874064757582', 35, 1, 'LaraPassport', '[]', 0, '2020-08-01 19:22:46', '2020-08-01 19:22:46', '2021-08-01 12:22:46'),
('98bec1537f171f94437db2e67468360cd299f76baf8e87ab23fe045b176f1e0a139d3afa65894dfb', 57, 1, 'LaraPassport', '[]', 0, '2020-06-25 04:54:12', '2020-06-25 04:54:12', '2021-06-25 04:54:12'),
('98deeb0ac2b708cd28f33d401006d51d24170b7ce63a9570fd409d870e62005ab7cbb8f98b1342dc', 33, 1, 'LaraPassport', '[]', 0, '2020-04-20 09:51:23', '2020-04-20 09:51:23', '2021-04-20 09:51:23'),
('98fb2767c3d21bc6353dd544329ff39bda65bfee8e9407efd2e6779c109ee406bdca694e1ec6ea64', 57, 1, 'LaraPassport', '[]', 0, '2020-07-28 12:05:10', '2020-07-28 12:05:10', '2021-07-28 05:05:10'),
('9905d2cdb8ad559f57d9ede13a5f56f8729c7ae4761a856f637c42b4b6c25251fcc4af9ef58cd34e', 109, 1, 'LaraPassport', '[]', 0, '2020-08-13 17:17:19', '2020-08-13 17:17:19', '2021-08-13 10:17:19'),
('99e7a55970e0dc5491bc53df159ca5963861b28fe10c5b78e532d6364d9cf86fb350e51a157b7b26', 103, 1, 'LaraPassport', '[]', 0, '2021-01-14 23:56:24', '2021-01-14 23:56:24', '2022-01-14 16:56:24'),
('99f0a261c14fc7422091719a0b39fa04a1997db2864b103322de2080aabca246e5826453a7508421', 97, 1, 'LaraPassport', '[]', 0, '2020-06-10 07:59:49', '2020-06-10 07:59:49', '2021-06-10 07:59:49'),
('9a34b4c4897ad3b6c7e7d7b0c34054c26e2ad33a719734c7bc5f66e652525fb82312422c349952ab', 33, 1, 'LaraPassport', '[]', 0, '2020-04-11 08:17:07', '2020-04-11 08:17:07', '2021-04-11 08:17:07'),
('9b624b8226883be302a88ff38852c812a0bec91c8c8552abbfe7555b79acdcef6d506131de4a48b5', 324, 1, 'LaraPassport', '[]', 0, '2020-09-30 17:45:03', '2020-09-30 17:45:03', '2021-09-30 10:45:03'),
('9bc37bddd69fbfe4bcde7724c5bea1becbc24cfd0ee3a0588e77c7cfa2cf1aec899c9afe2ddd0ee1', 231, 1, 'LaraPassport', '[]', 0, '2020-10-24 19:27:15', '2020-10-24 19:27:15', '2021-10-24 12:27:15'),
('9bfa04d6bddf403260e228c14ca0f1e361eaa3777fb990551d0585d31727101a26826b78d1d2e2ed', 59, 1, 'LaraPassport', '[]', 0, '2020-04-22 10:16:37', '2020-04-22 10:16:37', '2021-04-22 10:16:37'),
('9c07b142784f9565c3fb3388ad2b84ef87d448badfa4131fe07d4c034b333a4f5ba43c7cd9f09a08', 119, 1, 'LaraPassport', '[]', 0, '2021-01-19 20:38:33', '2021-01-19 20:38:33', '2022-01-19 13:38:33'),
('9c277294c3a751ebe22f2ff81f2d28474f97746e2243ac73f095f2c907185ae28abb988d639e8863', 118, 1, 'LaraPassport', '[]', 0, '2020-06-27 10:58:16', '2020-06-27 10:58:16', '2021-06-27 10:58:16'),
('9c3a299ac5ac21def40312ddde087ab66428f5073756e403da7cd076436dbefbafac2d335d220cac', 57, 1, 'LaraPassport', '[]', 0, '2020-04-30 12:58:07', '2020-04-30 12:58:07', '2021-04-30 12:58:07'),
('9c3f6dd5f31875910b430bbb353d24998e8a6c982639da0ebb023cce34f197dc7899b73b4265cc3f', 57, 1, 'LaraPassport', '[]', 0, '2020-04-29 07:38:59', '2020-04-29 07:38:59', '2021-04-29 07:38:59'),
('9c739a895567d277f11d100fbaee9f0089c307f9f63fe59268eee26deed058c5147de48d2001f5aa', 316, 1, 'LaraPassport', '[]', 0, '2020-10-20 17:37:17', '2020-10-20 17:37:17', '2021-10-20 10:37:17'),
('9c78fb728e9a0443d1404cee7ca5d991204453254c551092f45984ae2e01dd924fe94d5f10998c59', 109, 1, 'LaraPassport', '[]', 0, '2020-06-13 17:24:08', '2020-06-13 17:24:08', '2021-06-13 17:24:08'),
('9c7b5fc9ce6730009c5314c80a022431845509ad081765453f79479cb92595eaf0b80d0b44297b20', 35, 1, 'LaraPassport', '[]', 0, '2020-05-31 19:45:15', '2020-05-31 19:45:15', '2021-05-31 19:45:15'),
('9c9366c17137724bc79176aad554e7258f685d28ab3e492d8c1bb4abee135bc5cb064c1e6319fb93', 97, 1, 'LaraPassport', '[]', 0, '2020-06-11 04:42:30', '2020-06-11 04:42:30', '2021-06-11 04:42:30'),
('9ca0bd0b19c21b226412779bafde0c63516fc4a54c4560e9a2d7935d8e393182337c73e89ec11b81', 234, 1, 'LaraPassport', '[]', 0, '2020-08-24 12:13:57', '2020-08-24 12:13:57', '2021-08-24 05:13:57'),
('9ceeb0ccba7650c41e2560f83edf094346c90937bc7d079f3de99ea8621d9e741a66bed2e485c950', 266, 1, 'LaraPassport', '[]', 0, '2020-09-12 13:44:07', '2020-09-12 13:44:07', '2021-09-12 06:44:07'),
('9d0777b94f65279c222b195cf4d4f17ced420fbe08cecba0702c89a9a343460b28ebce6277423b94', 57, 1, 'LaraPassport', '[]', 0, '2020-08-18 12:18:30', '2020-08-18 12:18:30', '2021-08-18 05:18:30'),
('9e0e38e4716e5f05962cc93c98967e4a754212058652b4a279415171cc6f2968a84d55913ca720e4', 57, 1, 'LaraPassport', '[]', 0, '2020-05-02 06:10:28', '2020-05-02 06:10:28', '2021-05-02 06:10:28'),
('9e336c9208cff56ac9b5c21625d019c4dad43b0afc7a39a4082ae929e357f8f2c8d84d36f68cbeeb', 62, 1, 'LaraPassport', '[]', 0, '2020-04-27 06:42:52', '2020-04-27 06:42:52', '2021-04-27 06:42:52'),
('9e3c89b91e9ec16e31a24b1fd958145932c04e8d517a6cc595ad2171ff38681a94a336e1baf4a8d9', 309, 1, 'LaraPassport', '[]', 0, '2020-10-24 10:04:58', '2020-10-24 10:04:58', '2021-10-24 03:04:58'),
('9e473341c8756e28cca94ed57c08140b5aefe698f3e29e2c0c59ffa12ed17e0727d087421b20d79c', 454, 1, 'LaraPassport', '[]', 0, '2020-12-25 07:17:28', '2020-12-25 07:17:28', '2021-12-25 00:17:28'),
('9e4ac2b6da9415a5dcab5b11bdb219e2ab05559f953ca255ccb120a792f1877c8a05a611133b2ca5', 33, 1, 'LaraPassport', '[]', 0, '2020-04-11 04:00:23', '2020-04-11 04:00:23', '2021-04-11 04:00:23'),
('9e94162e4ed5e865ab15055b4b8e459edb2d806be30e48ae4486cd013a060274e392933335d56fcf', 35, 1, 'LaraPassport', '[]', 0, '2020-07-31 14:06:29', '2020-07-31 14:06:29', '2021-07-31 07:06:29'),
('9e9ccf3b7d3ec98f634ad6e77b1420a6d72bd92d7566fae69ddbf64ec9942e134806a8b3de196341', 305, 1, 'LaraPassport', '[]', 0, '2020-09-23 13:11:30', '2020-09-23 13:11:30', '2021-09-23 06:11:30'),
('9ebf5c5834bd43e1ab5346923a8a2b5946ce2230f94483fe6190977ba64c6e5d299128d5df9f12d4', 119, 1, 'LaraPassport', '[]', 0, '2020-06-27 10:59:24', '2020-06-27 10:59:24', '2021-06-27 10:59:24'),
('9f069db463b6ed4e7f57992d60df05625556c9c7211038b07caf0a558daf8fca83d47435b3b226d1', 35, 1, 'LaraPassport', '[]', 0, '2020-07-17 17:34:05', '2020-07-17 17:34:05', '2021-07-17 10:34:05'),
('9f14068a7e803a571df386eb18a9f9d8057457801de2a3e8af072b8e5077d4598ec22030f3f2964a', 430, 1, 'LaraPassport', '[]', 0, '2020-12-25 22:47:16', '2020-12-25 22:47:16', '2021-12-25 15:47:16'),
('9f504dd15b36f31dee1bf658f9f912ec6e8a2ad6557d6a5fa10d9a08869d9f4d7c1c4062eb5a6254', 416, 1, 'LaraPassport', '[]', 0, '2020-11-22 20:33:47', '2020-11-22 20:33:47', '2021-11-22 13:33:47'),
('9faa27df3644f3f7b2fc34999468f69c7bdd3ed5a1672172cfc59ab1daffc86726c7db8ad1d71ba4', 33, 1, 'LaraPassport', '[]', 0, '2020-04-20 04:17:16', '2020-04-20 04:17:16', '2021-04-20 04:17:16'),
('9ff1af25af91af59da675ee3f377c0111a47a591d0f9f69633b153e9a13beef2af3593dc4646438f', 307, 1, 'LaraPassport', '[]', 0, '2020-10-16 23:23:57', '2020-10-16 23:23:57', '2021-10-16 16:23:57'),
('a00f23c0a8f5f7f5fd687219951156873dccfb044de2a6d87620d594121960ef22db74b69bcf0fed', 119, 1, 'LaraPassport', '[]', 0, '2021-01-19 20:52:57', '2021-01-19 20:52:57', '2022-01-19 13:52:57'),
('a020bde2c795cca3fe55447cb12672723f84a70c24556bc208dcbe26bf6b6a6466fd995fa74a18a6', 332, 1, 'LaraPassport', '[]', 0, '2020-10-01 14:26:34', '2020-10-01 14:26:34', '2021-10-01 07:26:34'),
('a0730b4cc1b9d70ad0120f94b1fdd6eb52eb32ced57bc91e41c141f3f5a705ed9efd2c6997fd63a4', 35, 1, 'LaraPassport', '[]', 0, '2020-06-02 04:17:19', '2020-06-02 04:17:19', '2021-06-02 04:17:19'),
('a096248001888de8723eb96b3a932d78b22a66b983190b241a54b66193d69f18a193120f5e15c104', 246, 1, 'LaraPassport', '[]', 0, '2020-08-29 12:00:06', '2020-08-29 12:00:06', '2021-08-29 05:00:06');
INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('a0a42a44e85b19a865b41a7e4e035f4c8e47eca51812a274482bde5b682959853716f660c5bcd285', 237, 1, 'LaraPassport', '[]', 0, '2020-08-24 12:18:56', '2020-08-24 12:18:56', '2021-08-24 05:18:56'),
('a0cb75ac500a91edf4eb8b61ce4f851af6acbeba0a114194058792d3a52bbe2cf3f6991317f13863', 33, 1, 'LaraPassport', '[]', 0, '2020-04-11 11:14:31', '2020-04-11 11:14:31', '2021-04-11 11:14:31'),
('a0d1e9f517e8ba9b9ad61de3c4c18f92347f00184a2e9bfcb5e935e77374240e4e5517f5cada46da', 8, 1, 'LaraPassport', '[]', 0, '2020-04-09 12:43:24', '2020-04-09 12:43:24', '2021-04-09 12:43:24'),
('a1209ec134477dbb45f66fac825dffe49e04fd43d81980426da63eea8fcb71728b61b4398e7d81e1', 386, 1, 'LaraPassport', '[]', 0, '2020-11-07 23:35:08', '2020-11-07 23:35:08', '2021-11-07 16:35:08'),
('a1b2dc45cb33b51d02eaa353353f168f6f4c820658e72b8e6b9d75afde862ee619d0bc26f685b247', 309, 1, 'LaraPassport', '[]', 0, '2020-09-19 17:38:43', '2020-09-19 17:38:43', '2021-09-19 10:38:43'),
('a1b70968c07d393f9a98e4e57bc5eee9a05b4b89f29d9bc8edd7eaf545ea2ca25e204ccf6ca53012', 62, 1, 'LaraPassport', '[]', 0, '2020-08-20 14:26:19', '2020-08-20 14:26:19', '2021-08-20 07:26:19'),
('a24974d62d36baaa0d16b7a4747c285d03511fd9208deb59fb82c1e873ed3df712a336cde16b3d2e', 35, 1, 'LaraPassport', '[]', 0, '2020-08-04 16:57:34', '2020-08-04 16:57:34', '2021-08-04 09:57:34'),
('a2586de543dcc361780ae1f5d01bad93cd38cbb29795501728a07c0976b8cb4ecec5a58f6bd682b0', 47, 1, 'LaraPassport', '[]', 0, '2020-04-17 09:46:03', '2020-04-17 09:46:03', '2021-04-17 09:46:03'),
('a259134a6107d00e107da75c5d851b0cddc40da0fe2616df6d112fc9a3dc958606831bb8480a53fc', 62, 1, 'LaraPassport', '[]', 0, '2020-08-05 18:24:19', '2020-08-05 18:24:19', '2021-08-05 11:24:19'),
('a2b868e72c7cd4cb417640cf4063bb0fe44aee0e58fd4c177896430ff88e25f0824c047d117fda3b', 316, 1, 'LaraPassport', '[]', 0, '2020-11-23 18:02:11', '2020-11-23 18:02:11', '2021-11-23 11:02:11'),
('a2ec48b2989cd999cd16edc8923c79b2a3286ce77d9171a6ebc18ad4b54905317bf8c905f98216e4', 245, 1, 'LaraPassport', '[]', 0, '2020-08-27 22:17:36', '2020-08-27 22:17:36', '2021-08-27 15:17:36'),
('a3014f7e81b617e5f52486d139d97ad193e8cf653368efcebd2e2176c61f8c710aee429d52de9e81', 33, 1, 'LaraPassport', '[]', 0, '2020-04-18 05:10:16', '2020-04-18 05:10:16', '2021-04-18 05:10:16'),
('a34c950306e9ada3f7f5d06a39a7563b12fa23475f118e5b43bf180830f165b7f304832e363941ab', 33, 1, 'LaraPassport', '[]', 0, '2020-04-11 12:15:52', '2020-04-11 12:15:52', '2021-04-11 12:15:52'),
('a3c9ef0f679966a2c28bc52fd81a0053a4449c6199b08e8ed45fc6f37e71cd2149ce2e232f1162b5', 48, 1, 'LaraPassport', '[]', 0, '2020-04-17 09:52:17', '2020-04-17 09:52:17', '2021-04-17 09:52:17'),
('a3d96728d21c51f3d57099ac55fe3123b223ea5396d73c8d88c67b57d5c71bf1cb39c2b16cf50edd', 244, 1, 'LaraPassport', '[]', 0, '2020-10-16 12:16:32', '2020-10-16 12:16:32', '2021-10-16 05:16:32'),
('a4950e8370e57bad59cc9f5448bcdfe94325a4b430544625f5e697478b76327094c2a743fb5426d7', 33, 1, 'LaraPassport', '[]', 0, '2020-04-10 08:32:52', '2020-04-10 08:32:52', '2021-04-10 08:32:52'),
('a4bb6d5d2aadabb87446ab300f7a6fbdaa5976265bac12fcaa4535c22f685195fbcfbbd4594068ec', 8, 1, 'LaraPassport', '[]', 0, '2020-04-09 09:00:14', '2020-04-09 09:00:14', '2021-04-09 09:00:14'),
('a4d9362cc9a64142ed2b428c2e9a9a9de127514110df3921e092846532d5d3269b5dfa1b9fbf8612', 8, 1, 'LaraPassport', '[]', 0, '2020-04-09 12:43:24', '2020-04-09 12:43:24', '2021-04-09 12:43:24'),
('a50340ef236b36f0987297581892215e4de271609e83051c2bfcc4f19b7a37f6d69bb361ee121ace', 316, 1, 'LaraPassport', '[]', 0, '2020-10-20 11:49:15', '2020-10-20 11:49:15', '2021-10-20 04:49:15'),
('a53a4d3eba7909b5dca3171803115f06d1fceefb5e49419863229b2389032b96b7cd416f3ca48a92', 33, 1, 'LaraPassport', '[]', 0, '2020-04-13 08:26:13', '2020-04-13 08:26:13', '2021-04-13 08:26:13'),
('a5677566c8676b194f02afa83b368a014320b3fe4f796ea1dcd32f4086b21ba9b15a111a50c74fbd', 57, 1, 'LaraPassport', '[]', 0, '2020-08-01 18:09:01', '2020-08-01 18:09:01', '2021-08-01 11:09:01'),
('a569503d0558547d4771725f5a7f5225686bbf93c6e1d875b54db1b5ab0736a3b8924fae876d7b2a', 35, 1, 'LaraPassport', '[]', 0, '2020-06-25 11:22:55', '2020-06-25 11:22:55', '2021-06-25 11:22:55'),
('a5bd1f216a50ebbff42273adb90dd16ee50df11274e86aad402ce8eeb610a92857107b7afa0306d8', 320, 1, 'LaraPassport', '[]', 0, '2020-09-26 18:11:57', '2020-09-26 18:11:57', '2021-09-26 11:11:57'),
('a5fa7104865a5576cffae91d9ba320e6a9881740861a550a86ed54c281f7b4e45f6276bfc3d6162b', 57, 1, 'LaraPassport', '[]', 0, '2020-06-27 11:06:19', '2020-06-27 11:06:19', '2021-06-27 11:06:19'),
('a6444698e6e3ecce5d518a932c2cf0491c0e8560da0370db7fc890cc7e0b3d7375aa0d4bcd83ad78', 249, 1, 'LaraPassport', '[]', 0, '2020-09-30 17:55:13', '2020-09-30 17:55:13', '2021-09-30 10:55:13'),
('a6e574e5cf3f34b33bc1f6037b12fcc0c15aba9db3d4520aa7b2f10bded7890a914797ab633bd0b7', 57, 1, 'LaraPassport', '[]', 0, '2020-08-04 13:17:34', '2020-08-04 13:17:34', '2021-08-04 06:17:34'),
('a6e94e4fcf66b0a34b3d52ada1fbf47d0600804a565ff2b527329844b9636898c4d163afa2bc5321', 339, 1, 'LaraPassport', '[]', 0, '2020-10-05 17:52:55', '2020-10-05 17:52:55', '2021-10-05 10:52:55'),
('a73dcf3fe1be6e5439f5cc40efe971f1035dca4bc5f7eeac88efc9ee3a9fcc8f5c5edfb6fb9e14ba', 380, 1, 'LaraPassport', '[]', 0, '2020-11-04 01:18:35', '2020-11-04 01:18:35', '2021-11-03 18:18:35'),
('a78019eac25e1c9005bccd2abbbb310126d7a3248e3a00f40968697149bece2117f7b883ac56b85b', 33, 1, 'LaraPassport', '[]', 0, '2020-04-11 11:41:11', '2020-04-11 11:41:11', '2021-04-11 11:41:11'),
('a7b8471fdb63b24ebb1daa0de38e45cfe04c19e2fa9953ec2439834a14a3c7d6e8847c643c3a90c7', 57, 1, 'LaraPassport', '[]', 0, '2020-04-29 10:07:31', '2020-04-29 10:07:31', '2021-04-29 10:07:31'),
('a7d64c72332753db1e4ca6830227cac2f2e37326efd1c1da348d40ed1f6471db60fc8bc9f128aea8', 231, 1, 'LaraPassport', '[]', 0, '2020-10-06 18:34:10', '2020-10-06 18:34:10', '2021-10-06 11:34:10'),
('a80497be4afcd654a6bbbe09f7e3067e61722a0b210c0485a8e2f290d48e44b7f72d8b68443bda5b', 238, 1, 'LaraPassport', '[]', 0, '2020-09-04 22:37:37', '2020-09-04 22:37:37', '2021-09-04 15:37:37'),
('a8137822be0bce10ae2838b634f0d9e6019906f5e57cd95f0fc7c69727624ad9ce930d2c34ad6763', 307, 1, 'LaraPassport', '[]', 0, '2020-09-30 21:45:06', '2020-09-30 21:45:06', '2021-09-30 14:45:06'),
('a88136c7e54aea487713228faa8797b705d15eb8219987c855d8cc3565187454a6ae13cb91a31e50', 1, 1, 'LaraPassport', '[]', 0, '2020-08-20 12:15:07', '2020-08-20 12:15:07', '2021-08-20 05:15:07'),
('a891e65abaed2f0e2959aeb5d557c07ec7ed695199bb4992d80064410741a09f3a18099e6051a527', 238, 1, 'LaraPassport', '[]', 0, '2020-09-04 22:34:16', '2020-09-04 22:34:16', '2021-09-04 15:34:16'),
('a8cf49d6a9f704b2720fb9f4895c1d0e061be3adfe8a39ef1708dd82d7fc5fdb4b80e7da3a4349a4', 198, 1, 'LaraPassport', '[]', 0, '2020-08-17 22:15:16', '2020-08-17 22:15:16', '2021-08-17 15:15:16'),
('a8f5c1dc0eba98f16dd34c50eee7663512108295b75566fc40293f420468adb0a1b398f8239c3c98', 247, 1, 'LaraPassport', '[]', 0, '2020-08-28 20:37:30', '2020-08-28 20:37:30', '2021-08-28 13:37:30'),
('a9bc562f57b9981d897bed69e8b089a667e3756ef99c0fa4b187e942927e055fc4b2529112a29582', 316, 1, 'LaraPassport', '[]', 0, '2020-12-23 18:49:14', '2020-12-23 18:49:14', '2021-12-23 11:49:14'),
('a9d2a785f17de8e4ecd5f52e0e48d8d2caceb0a187f61c6b8b8c8303959d65ca3fc09b4e27ad8fef', 366, 1, 'LaraPassport', '[]', 0, '2020-10-18 08:36:33', '2020-10-18 08:36:33', '2021-10-18 01:36:33'),
('a9db5c287070c842f940e895a5ed84e05a4929659f12b293bacb9ec7276e5a2203416a313f5ace4d', 434, 1, 'LaraPassport', '[]', 0, '2020-12-27 20:32:07', '2020-12-27 20:32:07', '2021-12-27 13:32:07'),
('a9e66e8b4a490ce3e0a9be86684a83019bdc93409a2041b86ff4c693a203e3611f3d41aafe61ce36', 33, 1, 'LaraPassport', '[]', 0, '2020-04-11 04:54:32', '2020-04-11 04:54:32', '2021-04-11 04:54:32'),
('aa69e82a6a544a4e3dc83ad49690df59a447b718b512a21f710a11687cc2059b587182c183f96f8e', 119, 1, 'LaraPassport', '[]', 0, '2021-01-19 20:45:56', '2021-01-19 20:45:56', '2022-01-19 13:45:56'),
('aad709f81f31c8e59e5916aab44b1210c021dc460364ba5dd61e812b56ba5543cfa025293b735619', 476, 1, 'LaraPassport', '[]', 0, '2020-12-28 01:13:05', '2020-12-28 01:13:05', '2021-12-27 18:13:05'),
('aaff10f2a1169b4a70fd654d529f5021f5797a6b0b7ce454f50324fbd7c3d395edb10540f830ab27', 8, 1, 'LaraPassport', '[]', 0, '2020-04-09 11:29:29', '2020-04-09 11:29:29', '2021-04-09 11:29:29'),
('ab65ed3e6c7bcd9420450843674744e7e4b029b5eb1fafe3241b348451421a60e55d5e22beaf3f13', 57, 1, 'LaraPassport', '[]', 0, '2020-08-20 21:38:37', '2020-08-20 21:38:37', '2021-08-20 14:38:37'),
('abd1ca6cb6670019b91c8a7873aa2ca2247c623378906d69c45be69f780c6eab3aa13b249953d7be', 280, 1, 'LaraPassport', '[]', 0, '2020-09-09 18:32:24', '2020-09-09 18:32:24', '2021-09-09 11:32:24'),
('acb2b7dea0f90b3bce1944c6ea5bdb4cc36daf84c4490f44ca28b2cb7db95a2648dc4d6231f52e8a', 128, 1, 'LaraPassport', '[]', 0, '2020-07-05 19:25:45', '2020-07-05 19:25:45', '2021-07-05 19:25:45'),
('ad3ba1bc832af78ca5c717d4fad4461155268ba3cb8589a48843700f27fc05a6f60140bbdec317fd', 115, 1, 'LaraPassport', '[]', 0, '2020-06-22 11:31:28', '2020-06-22 11:31:28', '2021-06-22 11:31:28'),
('ad43897484e870e51831841dbc48a412f701841379f736a7d1ae89b09415940ed295d119f3010338', 35, 1, 'LaraPassport', '[]', 0, '2020-06-27 11:08:10', '2020-06-27 11:08:10', '2021-06-27 11:08:10'),
('adef4a3bbb4ca08d9260a9401a7944a530ef9345c695c7733a6efcc04bc8423926656744c37216fd', 244, 1, 'LaraPassport', '[]', 0, '2020-10-28 15:44:36', '2020-10-28 15:44:36', '2021-10-28 08:44:36'),
('ae193397d546b0ee1574f3d3f68a5626a57b09a5423e43d6c46adcdf20c3430d17d104368f9d4e1d', 231, 1, 'LaraPassport', '[]', 0, '2020-10-24 19:39:15', '2020-10-24 19:39:15', '2021-10-24 12:39:15'),
('ae1c18a920823ad6c14b2d0eedbeae38448d7ea2718f29b14c28e03b4b7ca6316414dd8db1ca063b', 238, 1, 'LaraPassport', '[]', 0, '2020-09-17 12:20:10', '2020-09-17 12:20:10', '2021-09-17 05:20:10'),
('ae799c477350e6f158262fd32489054383da4bddea1828279e64bbe43195f534995f89689e7a18c0', 195, 1, 'LaraPassport', '[]', 0, '2020-08-17 00:26:23', '2020-08-17 00:26:23', '2021-08-16 17:26:23'),
('aeb273d1bdd227bc96a4b7aa6bcfbfb9cff2efde5a63748b13dc3d74086620f9df1f4a060667a452', 34, 1, 'LaraPassport', '[]', 0, '2020-09-25 12:47:44', '2020-09-25 12:47:44', '2021-09-25 05:47:44'),
('aed719d7913f6a89eb2b3ef81b697ad01088237eefd3339a1cf240837a400914b58e0979fbc1ada5', 417, 1, 'LaraPassport', '[]', 0, '2020-11-24 03:29:59', '2020-11-24 03:29:59', '2021-11-23 20:29:59'),
('aef0e35bc8b02bb97c3c6c9ce3b061ca9fe80d027b12d6328a30179e703bacdde1cb14d98f602ac2', 35, 1, 'LaraPassport', '[]', 0, '2020-06-23 04:48:38', '2020-06-23 04:48:38', '2021-06-23 04:48:38'),
('af13cf347c81220ba6cf89cb7289976e018e3f60ed9f5f7c48bc79023dd1ceed8d44b40010ac7a3e', 57, 1, 'LaraPassport', '[]', 0, '2020-05-07 11:10:24', '2020-05-07 11:10:24', '2021-05-07 11:10:24'),
('af33378b0132a96800c91d5dcfe9c032bc2a60994cb0383fa9b6d98e47ad56beb20d7f086dd37313', 30, 1, 'LaraPassport', '[]', 0, '2020-04-09 10:02:46', '2020-04-09 10:02:46', '2021-04-09 10:02:46'),
('af6ef23d8d75af3bb98898e7a1e0c062f72dff6741d9c465bc39ca014f06292784a2d21d11f9bb34', 195, 1, 'LaraPassport', '[]', 0, '2020-08-17 18:24:34', '2020-08-17 18:24:34', '2021-08-17 11:24:34'),
('af98e8ea9e613d47ac123c2a74e242201c00192c013d3f0ebdf284838138943925791af61cebc432', 266, 1, 'LaraPassport', '[]', 0, '2020-09-08 16:39:43', '2020-09-08 16:39:43', '2021-09-08 09:39:43'),
('afd23423da4a1e3c4dbace75eea2b310e924c450cf078a5206ea7646a47583b0f73de130c8277004', 283, 1, 'LaraPassport', '[]', 0, '2020-09-11 09:45:27', '2020-09-11 09:45:27', '2021-09-11 02:45:27'),
('b007d2acd73b34c18d75e3e9b92fcadaec4b711801458ec2ac282db6f34739cf4a891da5a3ebae94', 238, 1, 'LaraPassport', '[]', 0, '2020-09-04 23:05:19', '2020-09-04 23:05:19', '2021-09-04 16:05:19'),
('b01025d961c14f3a6e448429107f454056830d779ddc94c389e10fe161b7c2dd2cbdac6f6b0f34fc', 57, 1, 'LaraPassport', '[]', 0, '2020-06-25 06:00:57', '2020-06-25 06:00:57', '2021-06-25 06:00:57'),
('b0a7abb1a4fe98d3751c2da632da443a3715eeddb2b3d84244a8b788a8f1d3456ad7e09221a9767d', 357, 1, 'LaraPassport', '[]', 0, '2020-10-14 13:31:18', '2020-10-14 13:31:18', '2021-10-14 06:31:18'),
('b0be29a0a0f33e55644559be09a6636236c3fc142de376edfa19211fa60f2534fe009f473b3e0534', 249, 1, 'LaraPassport', '[]', 0, '2020-09-30 17:53:33', '2020-09-30 17:53:33', '2021-09-30 10:53:33'),
('b0e19b6b294cc85154031779de462efe4cfb4e58708be4d302cba90b46f0378b397b2c5ecb2d94f0', 233, 1, 'LaraPassport', '[]', 0, '2020-08-21 11:41:19', '2020-08-21 11:41:19', '2021-08-21 04:41:19'),
('b0fdeeb56660685af0e4151cb455e431b5dd8c46c82cccb764f0f14f0670cd37d72d0e43d97da984', 243, 1, 'LaraPassport', '[]', 0, '2020-08-27 15:59:01', '2020-08-27 15:59:01', '2021-08-27 08:59:01'),
('b11cf6bb2c83ac06e7720284f8cf64b0f2b31f6438763d8e094213e67c8d5d0d77bc3e2acc9ff02b', 57, 1, 'LaraPassport', '[]', 0, '2020-05-02 06:10:05', '2020-05-02 06:10:05', '2021-05-02 06:10:05'),
('b140340d16ba5b307525cfc3a5eba70555edc3242b355396318495f70d4e524db8fc8df0bd061fea', 35, 1, 'LaraPassport', '[]', 0, '2020-08-04 12:37:41', '2020-08-04 12:37:41', '2021-08-04 05:37:41'),
('b171595fb41f025c71127c23e1f4eff209977cf9c3133df387a934a8d965bd15bb75d0c938fd1913', 57, 1, 'LaraPassport', '[]', 0, '2020-04-25 13:10:28', '2020-04-25 13:10:28', '2021-04-25 13:10:28'),
('b213f93a79ef0ea18c6ef973f07cefc2b4e72a7ad39eeb2f5b883436d6b5a70e7ca45fd2b0f4a268', 153, 1, 'LaraPassport', '[]', 0, '2020-08-27 00:28:44', '2020-08-27 00:28:44', '2021-08-26 17:28:44'),
('b227cb4faa7e361c410a8964bd088703509f8fccd8952841b27bec44b81d9648208940567eb5c592', 353, 1, 'LaraPassport', '[]', 0, '2020-10-10 22:03:52', '2020-10-10 22:03:52', '2021-10-10 15:03:52'),
('b23af6f4264e346958044874c529e885cace3298022f259e74587a424d89090448ee1802d9d96a52', 109, 1, 'LaraPassport', '[]', 0, '2020-06-13 11:42:40', '2020-06-13 11:42:40', '2021-06-13 11:42:40'),
('b2672f95c334a8ea4c20943a6c5928d32640f2d8e78b1594f278464cd26ae0da8a955e4a2aa47c44', 325, 1, 'LaraPassport', '[]', 0, '2020-09-30 18:54:59', '2020-09-30 18:54:59', '2021-09-30 11:54:59'),
('b29d52d1a8d05d42d679c366fb5b35fc5d386f1d8fac5a870bfb1cdcf12aae068d1bd6b34bb012c8', 244, 1, 'LaraPassport', '[]', 0, '2020-09-30 17:38:01', '2020-09-30 17:38:01', '2021-09-30 10:38:01'),
('b2fd979d92d38fdd51438461c6b6bc9ee0d001e034323cbe2b0e33a328c44adcd412e759228a3906', 119, 1, 'LaraPassport', '[]', 0, '2021-01-19 21:53:49', '2021-01-19 21:53:49', '2022-01-19 14:53:49'),
('b30b148e1d4eea5f0435e1e3c1e784664400df9ab37bdde7b8e297a6d0c34a220e86b44bfe997f59', 276, 1, 'LaraPassport', '[]', 0, '2020-09-08 15:36:56', '2020-09-08 15:36:56', '2021-09-08 08:36:56'),
('b30bf90c88766665a46846494e969e2af3455519fc919b7104e21466456f729f206f80f941e43f6e', 57, 1, 'LaraPassport', '[]', 0, '2020-07-03 17:59:07', '2020-07-03 17:59:07', '2021-07-03 17:59:07'),
('b31bfac925e27ab87fc23a9deeb08730e5083b98345abadf9723471ea14176faea76b477a28096bf', 238, 1, 'LaraPassport', '[]', 0, '2020-09-17 12:22:46', '2020-09-17 12:22:46', '2021-09-17 05:22:46'),
('b36eb8596283801cd43540682e632f8b0ab9c1235b9d0a47dab2daa63610b35d676f7c74024a19eb', 57, 1, 'LaraPassport', '[]', 0, '2020-05-07 11:02:00', '2020-05-07 11:02:00', '2021-05-07 11:02:00'),
('b38c94d71d06cdcfef866e69ef862e2a6b76aefdbf6e09efc73440eca789a8c8fb19c5a0b15cd1ce', 352, 1, 'LaraPassport', '[]', 0, '2020-10-10 20:15:36', '2020-10-10 20:15:36', '2021-10-10 13:15:36'),
('b3a2484a1215ea63525def083a1d3ac42ce67d75cb53ea213af3ca6f0b00bf94ac4554fd687945b9', 276, 1, 'LaraPassport', '[]', 0, '2020-09-14 12:28:16', '2020-09-14 12:28:16', '2021-09-14 05:28:16'),
('b3cfd12b07fc365743bd63d32a15bdfc302dd910349b4c46c968cc235e47e2dbb455d8ca7195712d', 314, 1, 'LaraPassport', '[]', 0, '2020-09-22 13:05:52', '2020-09-22 13:05:52', '2021-09-22 06:05:52'),
('b3f8fe97dd432604b0563d0ac76826fd534a4bbbfc23b2b9ccb99895b47f9cb5f78fc6ec9ef1b041', 237, 1, 'LaraPassport', '[]', 0, '2020-08-27 11:13:24', '2020-08-27 11:13:24', '2021-08-27 04:13:24'),
('b43d345d29aea9fe4bcabe921e08654c30198dd732b55a42d786e805d14eb8f4c548616de7c94ab6', 33, 1, 'LaraPassport', '[]', 0, '2020-04-11 07:29:21', '2020-04-11 07:29:21', '2021-04-11 07:29:21'),
('b441694ebd15d2ecc103a97c149f82af3a45b29849b499ab6f50f9aff187df443e8bc63b9513b81b', 33, 1, 'LaraPassport', '[]', 0, '2020-04-17 08:37:31', '2020-04-17 08:37:31', '2021-04-17 08:37:31'),
('b44d19be8b82b3f9204eaeac04820baadc56964e1563052fd33b8dc007afd5ed5213b966aa18c1a1', 153, 1, 'LaraPassport', '[]', 0, '2020-08-27 00:29:48', '2020-08-27 00:29:48', '2021-08-26 17:29:48'),
('b459f26503368b926e08819269f1c8b9fb9e092ef6556009c590cc7afaf87f9affbba787348113b3', 33, 1, 'LaraPassport', '[]', 0, '2020-04-13 06:04:06', '2020-04-13 06:04:06', '2021-04-13 06:04:06'),
('b4851c74cfccfbe942633f29728e7e905486b4d0f4b493750170e6cfde940e72c363f4abaf3960c6', 425, 1, 'LaraPassport', '[]', 0, '2020-12-02 20:24:28', '2020-12-02 20:24:28', '2021-12-02 13:24:28'),
('b4856bf2a3dd644ec1854b4f641a7e8594bf0d3de2d27869aa33dbe8b7280a958b2f402d1284c1c8', 423, 1, 'LaraPassport', '[]', 0, '2020-12-21 23:04:27', '2020-12-21 23:04:27', '2021-12-21 16:04:27'),
('b4dc1fc2bc25e25f64f07dd3f4b521a23e3f4325f0ae0cae91804dc8b2016aab5d0ef5994e278bc8', 57, 1, 'LaraPassport', '[]', 0, '2020-06-27 10:36:09', '2020-06-27 10:36:09', '2021-06-27 10:36:09'),
('b50ef89fcc2f1f4f10f2975949f4e8dfbfe9b8124a2b6b52d7d19566480b7040b2082aa976ba0c5e', 198, 1, 'LaraPassport', '[]', 0, '2020-08-17 21:38:25', '2020-08-17 21:38:25', '2021-08-17 14:38:25'),
('b55ef3c40ba3774b2faef81a291efcadca9a18ee1d1666ba90433bf00cc1b1fd9e1200cfbdfccab4', 35, 1, 'LaraPassport', '[]', 0, '2020-06-25 09:58:40', '2020-06-25 09:58:40', '2021-06-25 09:58:40'),
('b568167c4df78e3b0386a79e639a1e494c46af0597380f244c4be82446fe55f39c58cec7488b6528', 57, 1, 'LaraPassport', '[]', 0, '2020-05-02 04:27:24', '2020-05-02 04:27:24', '2021-05-02 04:27:24'),
('b5685bd2b683b4ed4a166572bc8fbd8d84719c7cb54d7afca86eead0ceee65d2426b56b5eadce723', 57, 1, 'LaraPassport', '[]', 0, '2020-08-06 14:43:49', '2020-08-06 14:43:49', '2021-08-06 07:43:49'),
('b56ea32ef8f6d8a1ee46e39fcae0874e24423358e3a9f0fc94116dee093410bf5a6c0d66d22d1c25', 244, 1, 'LaraPassport', '[]', 0, '2020-09-18 20:50:35', '2020-09-18 20:50:35', '2021-09-18 13:50:35'),
('b5a43eeebc93db113611988e34bdf20899c4a06d414bb9c8ac7edb1d0a9ce51c7e2dee22e1e651a8', 57, 1, 'LaraPassport', '[]', 0, '2020-07-31 16:55:20', '2020-07-31 16:55:20', '2021-07-31 09:55:20'),
('b5c77d4f3ede00ad8724143e8fcd7cebbbdfddbc28c2f7f8154f01acc4eb4e4dd84bfdf30f99d5d1', 35, 1, 'LaraPassport', '[]', 0, '2020-07-03 18:06:18', '2020-07-03 18:06:18', '2021-07-03 18:06:18'),
('b60327f493f3566cb0c4d2e990488f01cc83b05d1072910e4ac7a49d94c4a8536f9799a8b16550d2', 221, 1, 'LaraPassport', '[]', 0, '2020-08-19 12:13:58', '2020-08-19 12:13:58', '2021-08-19 05:13:58'),
('b6224e7ac3aa7f17918eee8831b6b137b334a8b630a0834a73ddbc636deffcce8397d12718454d0b', 35, 1, 'LaraPassport', '[]', 0, '2020-05-16 08:32:12', '2020-05-16 08:32:12', '2021-05-16 08:32:12'),
('b6413cf590d9d4a50a6cd07501763fc5cf4b01eeeacac5565531fa338f651aafdbb59d2706b2888b', 8, 1, 'LaraPassport', '[]', 0, '2020-04-10 04:43:08', '2020-04-10 04:43:08', '2021-04-10 04:43:08'),
('b69a4d859ea0756ff396de9e87001b178cb2b171ca4df55ab7132f89a3bdc52bee175b2495724e35', 315, 1, 'LaraPassport', '[]', 0, '2020-09-23 11:37:55', '2020-09-23 11:37:55', '2021-09-23 04:37:55'),
('b6c9773089358b3cf1706ad90a1751effd3e224d0506e3fbc7199439d0267d8eb4a101c813f921e5', 57, 1, 'LaraPassport', '[]', 0, '2020-08-01 16:47:41', '2020-08-01 16:47:41', '2021-08-01 09:47:41'),
('b742dec8f04e4c13c2da7a5d09e19f2fba68a5911e93fbfc21c8efaee6ae5d357536c29c779965a2', 244, 1, 'LaraPassport', '[]', 0, '2020-10-24 11:09:01', '2020-10-24 11:09:01', '2021-10-24 04:09:01'),
('b7555707943da020bdfaa3b16583bcde0bca8cceeb126342139382603b64afb466d6bafa12e9556c', 316, 1, 'LaraPassport', '[]', 0, '2020-12-12 23:14:29', '2020-12-12 23:14:29', '2021-12-12 16:14:29'),
('b78153ad17a353b8bb10237628dae4d0f3b39272f4acfefbb369331bff1e61d2fbaff52b54670aaf', 229, 1, 'LaraPassport', '[]', 0, '2020-08-19 16:40:35', '2020-08-19 16:40:35', '2021-08-19 09:40:35'),
('b7a8687c5e76aa44fc4494d8e1f74243fee0753aa3778503f4859935e837bf296712e20db2e8b891', 454, 1, 'LaraPassport', '[]', 0, '2020-12-25 07:17:48', '2020-12-25 07:17:48', '2021-12-25 00:17:48'),
('b830bc630b6b333bddc3ae3c2b65b12451ef69db0148e559c464abbf21a2e67044116df67d444bd3', 239, 1, 'LaraPassport', '[]', 0, '2020-08-24 13:43:05', '2020-08-24 13:43:05', '2021-08-24 06:43:05'),
('b83b45a371351621cdb3acf48c48a4a79be9f0edc7432b60c9dd963b24ec9f54d617b8afe5183f2c', 247, 1, 'LaraPassport', '[]', 0, '2020-09-15 11:35:33', '2020-09-15 11:35:33', '2021-09-15 04:35:33'),
('b84bedc132471bb86fc90ace0882a35a3632e667cbc8e3d48cc752aa8e38cd7035ab3854a380fe27', 427, 1, 'LaraPassport', '[]', 0, '2020-12-06 03:29:16', '2020-12-06 03:29:16', '2021-12-05 20:29:16'),
('b8746f8b61d50a074fbae3c76107c650c654f4608a05fdbd8249a942a889d581684d4172e64448dc', 287, 1, 'LaraPassport', '[]', 0, '2020-09-10 20:13:19', '2020-09-10 20:13:19', '2021-09-10 13:13:19'),
('b880abb115b18c99863772c652f1ea2c86913374105d9ed4af4d01f25b78e7e8c814faf398f04e4b', 57, 1, 'LaraPassport', '[]', 0, '2020-08-04 13:07:51', '2020-08-04 13:07:51', '2021-08-04 06:07:51'),
('b93b1685c45703b2e5b0b519050f8a8f74de8b86f4f57ad765f08dbd56fc0149913ef3bea8071965', 238, 1, 'LaraPassport', '[]', 0, '2020-09-17 10:52:10', '2020-09-17 10:52:10', '2021-09-17 03:52:10'),
('b95be3acc2c2c0a32a7b6bd26bc4ec3c31ed934ff78452249f5780be55f79b78ac28b297cac58361', 33, 1, 'LaraPassport', '[]', 0, '2020-04-11 07:39:36', '2020-04-11 07:39:36', '2021-04-11 07:39:36'),
('b978a1df6e110735ff3e10e463b68cdb0599fc9f631f792bbcd2b406ec5d20162b7454ded63e6041', 116, 1, 'LaraPassport', '[]', 0, '2020-07-29 16:36:08', '2020-07-29 16:36:08', '2021-07-29 09:36:08'),
('b9c0f586c8df33f8ac72df06c3f03947c3efd50971e4e09a938a39e491fd8a3d9effc0d4629ddedd', 49, 1, 'LaraPassport', '[]', 0, '2020-04-17 09:52:45', '2020-04-17 09:52:45', '2021-04-17 09:52:45'),
('b9cc9c8cf6e720863218a4fddb067d7456bf46495ce97cb19db0ccc98ab04661404a6a837e811e32', 35, 1, 'LaraPassport', '[]', 0, '2020-06-25 07:09:10', '2020-06-25 07:09:10', '2021-06-25 07:09:10'),
('ba0902f872b5c25ca4f0f86965a800be03c8324078311fddb2f7013fb0f6fef1043c81d29ffc7a2a', 33, 1, 'LaraPassport', '[]', 0, '2020-04-18 05:18:15', '2020-04-18 05:18:15', '2021-04-18 05:18:15'),
('ba26cd57d26643a29ced862e0781742263a53243b6c4a6795d6e0e76579089c88e15a2cdf353e6d4', 57, 1, 'LaraPassport', '[]', 0, '2020-07-29 16:38:06', '2020-07-29 16:38:06', '2021-07-29 09:38:06'),
('ba867d6e3c8827fa86e0f73761767f9405a620e93b03603d80ea5efbdca2f21a38db345d8f0e6bff', 57, 1, 'LaraPassport', '[]', 0, '2020-05-02 04:08:20', '2020-05-02 04:08:20', '2021-05-02 04:08:20'),
('ba97733b27d6d11051400244b59d76f1cf57933eef9bbf0ba6c6d002b3c634c0f59ffce3e76bc571', 266, 1, 'LaraPassport', '[]', 0, '2020-09-28 17:37:34', '2020-09-28 17:37:34', '2021-09-28 10:37:34'),
('baaccebeb22df09b72a252ed18e968fb342f3750465016447a9e50996b842449598fd6ce2783e33c', 109, 1, 'LaraPassport', '[]', 0, '2020-08-01 17:32:15', '2020-08-01 17:32:15', '2021-08-01 10:32:15'),
('bafe789cd80e1bb57c73355d995926f33f86a6805f7b4d124fdf5c47f6af0fec5596ecc4526063e7', 57, 1, 'LaraPassport', '[]', 0, '2020-07-30 18:09:52', '2020-07-30 18:09:52', '2021-07-30 11:09:52'),
('bb17e247172a55bcc6ace62414b4d6d456298921d9e68b60dd62399978e4e320b570fcfea46d2b7d', 33, 1, 'LaraPassport', '[]', 0, '2020-04-14 07:52:16', '2020-04-14 07:52:16', '2021-04-14 07:52:16'),
('bb1f20395debacf633dfb3d7b064c5f54487ed4d643a5e6b262583a9a974a4a4c2a4253983ce76a4', 417, 1, 'LaraPassport', '[]', 0, '2020-11-24 03:29:57', '2020-11-24 03:29:57', '2021-11-23 20:29:57'),
('bb2fdf9fd2ac30066b9294463791a6a2b2758e322d36664d952ffce1f255ce57c4c96fce7d07a084', 430, 1, 'LaraPassport', '[]', 0, '2020-12-25 22:47:12', '2020-12-25 22:47:12', '2021-12-25 15:47:12'),
('bb55b86b112f9b8dc17c7fc3d034c6236be92008b45b58309ff475ccc675910fd2202995bcd361a5', 35, 1, 'LaraPassport', '[]', 0, '2020-06-04 12:48:28', '2020-06-04 12:48:28', '2021-06-04 12:48:28'),
('bb62d02c407e90804ad53b703fe1c4cb55d60cca280118ad2834221f54fc5ef05bed44687b3cb319', 33, 1, 'LaraPassport', '[]', 0, '2020-04-13 04:42:30', '2020-04-13 04:42:30', '2021-04-13 04:42:30'),
('bb9734d1f35cab597d5624b1419cf3cd9b7d2af1800de5b888f9b20d12d044370293669f9ff49772', 59, 1, 'LaraPassport', '[]', 0, '2020-05-01 04:49:52', '2020-05-01 04:49:52', '2021-05-01 04:49:52'),
('bb9d5a4d79fccfcd179cf0f084692657c170dbc98812a6728f649e4e4f8c4263b627d051b76605af', 104, 1, 'LaraPassport', '[]', 0, '2020-06-11 12:56:21', '2020-06-11 12:56:21', '2021-06-11 12:56:21'),
('bbc1547bfb837ce74fab2e9bf1f7b1020cee90a603e2cb4c5b15ec7d0c1f7855607592ed15193447', 57, 1, 'LaraPassport', '[]', 0, '2020-05-02 04:13:30', '2020-05-02 04:13:30', '2021-05-02 04:13:30'),
('bbe5805e58317d76b8c21a9f96b286be63dd1b0b30b168c0fa7500dbba646e01bc54a99b183b252f', 33, 1, 'LaraPassport', '[]', 0, '2020-04-11 05:00:25', '2020-04-11 05:00:25', '2021-04-11 05:00:25'),
('bc363a121a8effddbd8e51d7ca0640e75b3d2d86a8339d23a68de341399e31076e1f126db6e0ff9e', 109, 1, 'LaraPassport', '[]', 0, '2021-01-16 19:50:50', '2021-01-16 19:50:50', '2022-01-16 12:50:50'),
('bc365b3b1a93a84b096ec7a6002b05433e1e8b33ec885286f7654bec525147715cb8f7125eeeee18', 235, 1, 'LaraPassport', '[]', 0, '2020-08-22 22:54:18', '2020-08-22 22:54:18', '2021-08-22 15:54:18'),
('bd19ca20c48ab24a04d20775ca493cac58e1f663a3381a25702eb608734ae8a5ad8797288bc53a2a', 331, 1, 'LaraPassport', '[]', 0, '2020-10-01 13:28:20', '2020-10-01 13:28:20', '2021-10-01 06:28:20'),
('bd3e0301be1fbe783c56b87c6f89d4e48b6e95b68d156e9b547766d606996d1cb6eb67af427b62db', 35, 1, 'LaraPassport', '[]', 0, '2020-06-25 10:18:48', '2020-06-25 10:18:48', '2021-06-25 10:18:48'),
('bd5cb485a22fcce22a1456c55af1e65baf1c9c1fb6a1b29f8b370d2d624250caac1e14396040e88a', 241, 1, 'LaraPassport', '[]', 0, '2020-08-27 11:38:04', '2020-08-27 11:38:04', '2021-08-27 04:38:04'),
('bd7b40583d8bf965df0759bd7b8a193a1e9850e482f72c19234c61de7f752b63c7478f9a5b0e9a0d', 97, 1, 'LaraPassport', '[]', 0, '2020-06-09 10:46:15', '2020-06-09 10:46:15', '2021-06-09 10:46:15'),
('bdd80da5512c4d3fe39ca5fef4d0e466b19ce7468eedc7c613974de1afaba30d5c62c2ca26d0c13a', 5, 1, 'LaraPassport', '[]', 0, '2021-01-12 21:20:46', '2021-01-12 21:20:46', '2022-01-12 14:20:46'),
('bde49758c7dcf759fe1c85932571606643b3a2e0a20f78441217cc60f530dc5729ab34f4488fcb53', 33, 1, 'LaraPassport', '[]', 0, '2020-04-10 13:54:46', '2020-04-10 13:54:46', '2021-04-10 13:54:46'),
('be00e840c72d458c7298c403b00fcfa9dc2769115487c4b4e10f2ef92cdb2e26483171ef42067991', 300, 1, 'LaraPassport', '[]', 0, '2020-10-04 00:00:03', '2020-10-04 00:00:03', '2021-10-03 17:00:03'),
('be083dbbfe3507b865270a954d01bdff99ab88b7f7ea9fc819a60736efe4d093bb4e6f3f26792efa', 246, 1, 'LaraPassport', '[]', 0, '2020-08-28 14:59:27', '2020-08-28 14:59:27', '2021-08-28 07:59:27'),
('be303dc18a1c1266d83b5343e1dfc13c6f890d44d0d1b0716c4d6e09f2a05656fa6a82c89c77297f', 245, 1, 'LaraPassport', '[]', 0, '2020-08-27 22:17:48', '2020-08-27 22:17:48', '2021-08-27 15:17:48'),
('be435e20bb3ccc8627ce1f9152e0dcca6d0531cd32a5a00a780c73caaa1de723b8695d752ce87021', 57, 1, 'LaraPassport', '[]', 0, '2020-08-01 16:54:09', '2020-08-01 16:54:09', '2021-08-01 09:54:09'),
('be526e93395156b9cbaa67a3d019c95cb1577558ef58d52e250b200e82561e93775e0c756e947370', 8, 1, 'LaraPassport', '[]', 0, '2020-04-10 05:40:01', '2020-04-10 05:40:01', '2021-04-10 05:40:01'),
('be93f0d63a2d460cb03c9c34a8c3aa6e8ee2b64d41ea1e26bee3c291324737b00056b325238390ac', 59, 1, 'LaraPassport', '[]', 0, '2020-04-26 07:11:42', '2020-04-26 07:11:42', '2021-04-26 07:11:42'),
('bec44ffad01f973dac2edd0d46ecf6adeeadbc18fb1fb6777cbd9f58ce354e88f0504ea74cd71427', 238, 1, 'LaraPassport', '[]', 0, '2020-09-16 18:05:16', '2020-09-16 18:05:16', '2021-09-16 11:05:16'),
('becffc493046e07df4b0d21106baec2ec43624cfb1652dbad352bc59cf35b0ecc18a25e5225965cb', 256, 1, 'LaraPassport', '[]', 0, '2020-09-01 19:52:02', '2020-09-01 19:52:02', '2021-09-01 12:52:02'),
('beefec8975f11e59dd3b0804552a1dd3085b84cf99351f2b4682713489fd51b762d09a5520cfdb3e', 231, 1, 'LaraPassport', '[]', 0, '2020-10-25 13:21:49', '2020-10-25 13:21:49', '2021-10-25 06:21:49'),
('bf03b59f2f40975ca5c6505a6e39ec80ca98f2cc66c47bbdf1326a06607ab918a0095fbdf8d4ce8e', 290, 1, 'LaraPassport', '[]', 0, '2020-09-12 00:03:46', '2020-09-12 00:03:46', '2021-09-11 17:03:46'),
('bf5d5233dd6c3d0631ad6241ee32b5b3162cdd808e2f719fe0461c28a024f015fb236587cc1a455a', 55, 1, 'LaraPassport', '[]', 0, '2020-04-17 09:57:47', '2020-04-17 09:57:47', '2021-04-17 09:57:47'),
('bf614fb14f841cece2bae2c10c7951799711cba7d5dd5378068a73c1f180fb3c129644f0aaabb5c1', 237, 1, 'LaraPassport', '[]', 0, '2020-08-27 12:57:25', '2020-08-27 12:57:25', '2021-08-27 05:57:25'),
('bf76717f40e20194c429e8bb642487ec1b5b376e92dad954ca974b18e41134568b41858c0eb03603', 316, 1, 'LaraPassport', '[]', 0, '2020-09-28 13:12:21', '2020-09-28 13:12:21', '2021-09-28 06:12:21'),
('bf830aa8b816c1c41012d6214e5c33d0c292233c31516f90caebed6ea0a202f1b2260861abaa69fe', 9, 1, 'LaraPassport', '[]', 0, '2021-01-11 23:44:00', '2021-01-11 23:44:00', '2022-01-11 16:44:00'),
('bfa6ae75bef3fd65b8efeccf62bd89f001f44b670637fce5e25bb0c581c9397365846b40a3e4a6a9', 33, 1, 'LaraPassport', '[]', 0, '2020-04-10 08:16:58', '2020-04-10 08:16:58', '2021-04-10 08:16:58'),
('bfbbcaaa6865d1fa90ae8cbe82987d70572271e7ac2ad0a5d9dec9b04bde1c7b2edf1ede18a05b3c', 361, 1, 'LaraPassport', '[]', 0, '2020-10-15 13:46:41', '2020-10-15 13:46:41', '2021-10-15 06:46:41'),
('c01c0c95d337a098f8c4b20c53cc598eb2d8174bbd4366961a018b8c2ea682878152fce853f47bf4', 46, 1, 'LaraPassport', '[]', 0, '2020-04-17 09:44:36', '2020-04-17 09:44:36', '2021-04-17 09:44:36'),
('c074949ad58d319af27df4a1db5b8569b3b489a050c4167185edbcc6b0d292fbacb8a915da171ec3', 244, 1, 'LaraPassport', '[]', 0, '2020-10-25 18:08:58', '2020-10-25 18:08:58', '2021-10-25 11:08:58'),
('c07eaffd412556a0418280742c3fad19a83b2162fbe2ded00eb1e9a45024a5cfa8c937c515e5ff3d', 431, 1, 'LaraPassport', '[]', 0, '2020-12-15 23:59:47', '2020-12-15 23:59:47', '2021-12-15 16:59:47'),
('c10671cfa1681f9729a83bd02953c8459bfa015c5cbc2822e816912362846d4ce94fdf539d1def6f', 430, 1, 'LaraPassport', '[]', 0, '2020-12-15 18:32:38', '2020-12-15 18:32:38', '2021-12-15 11:32:38'),
('c15f201fd946c71c2b9c84bbc4124ddd545eb54aa605732c46d49eb3b21913dff7eaa44f89ffe8a9', 231, 1, 'LaraPassport', '[]', 0, '2020-09-28 16:47:59', '2020-09-28 16:47:59', '2021-09-28 09:47:59'),
('c170784ac03630fb726a81b90c1d6a34e6b625603b8be6fa9c9ba533829feebccc07cb19d2de99a1', 57, 1, 'LaraPassport', '[]', 0, '2020-06-08 09:12:55', '2020-06-08 09:12:55', '2021-06-08 09:12:55'),
('c17179a95b0e8a799e22ac85985f24751d024da84a4652b85327e20d2f5dc2037c3a0a695820599e', 35, 1, 'LaraPassport', '[]', 0, '2020-08-04 16:49:45', '2020-08-04 16:49:45', '2021-08-04 09:49:45'),
('c1d5499678a0c172b429184befde20eb6651acbaa29e5c2154b2583bee5b4f4e5054d4fa1efb0307', 33, 1, 'LaraPassport', '[]', 0, '2020-04-11 05:34:17', '2020-04-11 05:34:17', '2021-04-11 05:34:17'),
('c1e021d09f85c7a1cec55a60211e9bf144cada59e2ec53db5abfb40d2fccbf34cef707b8dc7e95f5', 33, 1, 'LaraPassport', '[]', 0, '2020-04-20 12:07:33', '2020-04-20 12:07:33', '2021-04-20 12:07:33'),
('c1e5140eca67f3545d51f49f1fbce3b7402a31861331628b39755e8f89992c054848c197279df1d2', 33, 1, 'LaraPassport', '[]', 0, '2020-04-16 11:00:31', '2020-04-16 11:00:31', '2021-04-16 11:00:31'),
('c2252ee39aeaa7b5279e791f3daf4a94f29a5ed75a1603155e51a502aff502293650da2e3a8c56e0', 262, 1, 'LaraPassport', '[]', 0, '2020-09-05 23:02:35', '2020-09-05 23:02:35', '2021-09-05 16:02:35'),
('c252749af28299991acc49a7fb9595309ecdb3a679e06b7e4632d1a99eb9250a3db614372816ae41', 35, 1, 'LaraPassport', '[]', 0, '2020-07-17 15:00:06', '2020-07-17 15:00:06', '2021-07-17 08:00:06'),
('c2a01002efa87fa98e61d3f1ecefd539fdc5e7cd9acb41665de32c421b7cc8a4ee421994f6b17b40', 105, 1, 'LaraPassport', '[]', 0, '2020-06-12 06:18:02', '2020-06-12 06:18:02', '2021-06-12 06:18:02'),
('c2a7b9472a939657d489c247afee939351e35c4ec8f0b42908df6d96b34a1da8f1f599d88561ea41', 5, 1, 'LaraPassport', '[]', 0, '2021-01-13 00:31:52', '2021-01-13 00:31:52', '2022-01-12 17:31:52'),
('c2a9fa3fbb8841fc9dfffc7dd57935edba0d12f06313128e105cd1e8b98dddaaa3356b501de4ffa4', 35, 1, 'LaraPassport', '[]', 0, '2020-06-08 10:55:51', '2020-06-08 10:55:51', '2021-06-08 10:55:51'),
('c2e4e880729c8398e692407d6529adac715e3bac48a8dee70a86a49885fa167900c3035a7a4acc57', 35, 1, 'LaraPassport', '[]', 0, '2020-08-17 17:48:05', '2020-08-17 17:48:05', '2021-08-17 10:48:05'),
('c30235fd6c6853fee140d9f274f138bbe968ba8564fe1398909f120073a3f3497d76f507409fa8a4', 35, 1, 'LaraPassport', '[]', 0, '2020-05-18 07:21:27', '2020-05-18 07:21:27', '2021-05-18 07:21:27'),
('c314966962a7acbb927f8cf6e36862a5e9138f27035549b9101fb71c53bffb93d6e08ad3aa54294b', 33, 1, 'LaraPassport', '[]', 0, '2020-04-11 07:15:16', '2020-04-11 07:15:16', '2021-04-11 07:15:16'),
('c38f87a62727fc1c7664ead9989f5a0c8a7d3b6870a6ec5c19f293395c0fc4079d26417f6b5dd8ef', 373, 1, 'LaraPassport', '[]', 0, '2020-10-30 00:17:26', '2020-10-30 00:17:26', '2021-10-29 17:17:26'),
('c3a9f9b740725719faa0f17f01e90df68ec4c6378fa4c76e95b99faa1df97a7e891c6f45371c4483', 35, 1, 'LaraPassport', '[]', 0, '2020-08-04 12:43:12', '2020-08-04 12:43:12', '2021-08-04 05:43:12'),
('c3d8c1eea282ddaead83f7caed70d0682999ec77c457cd1946ef24f1652e7c7b9ca97d3afe56749e', 243, 1, 'LaraPassport', '[]', 0, '2020-08-27 17:55:41', '2020-08-27 17:55:41', '2021-08-27 10:55:41'),
('c41c6d8b1a2c5b54affbeb855c952438495be2b42b427a71f0f94481af94f75578eacf984edc77c7', 423, 1, 'LaraPassport', '[]', 0, '2020-12-01 17:19:30', '2020-12-01 17:19:30', '2021-12-01 10:19:30'),
('c44239ee03e4307318a7addf55b6ea5f458abbb152677058cb3133e115505e5b07b1cb08b4e5887f', 237, 1, 'LaraPassport', '[]', 0, '2020-09-25 13:14:25', '2020-09-25 13:14:25', '2021-09-25 06:14:25'),
('c48150a5fee5c1e2492b1b401bbe5026b8b30011326f2b104cffc83e55b4860794b444de5b3a32fa', 313, 1, 'LaraPassport', '[]', 0, '2020-09-21 15:05:04', '2020-09-21 15:05:04', '2021-09-21 08:05:04'),
('c489b8abe384613df51b8e4e3e8e52101a9c87dc8699515e094dde142213ebba0d9ea6c4ceb57a42', 35, 1, 'LaraPassport', '[]', 0, '2020-06-29 08:00:00', '2020-06-29 08:00:00', '2021-06-29 08:00:00'),
('c5961cff1ce1b4c0f231dd6df5ff514e068e636a19ca10f21d0143a35e7d5f9035d3aec6dd2486aa', 59, 1, 'LaraPassport', '[]', 0, '2020-04-24 14:23:33', '2020-04-24 14:23:33', '2021-04-24 14:23:33'),
('c5b60f287be970d09493a1f4ba8fa03d0b4d66800adbd45d3d1868c1745aa4d27058a975e71e686c', 434, 1, 'LaraPassport', '[]', 0, '2021-01-03 00:27:59', '2021-01-03 00:27:59', '2022-01-02 17:27:59'),
('c5f9bc2eb1575531926fb4e880248d4f56be71bc4ff41046db404c0a6cd9077bf9ffc76ca50203cf', 238, 1, 'LaraPassport', '[]', 0, '2020-08-24 12:53:18', '2020-08-24 12:53:18', '2021-08-24 05:53:18'),
('c63cf87d7bc7e62859f5958e357e67436626eb3794fb263e29eb09ead92aa033e844751a9c7d83b9', 62, 1, 'LaraPassport', '[]', 0, '2020-05-02 09:05:36', '2020-05-02 09:05:36', '2021-05-02 09:05:36'),
('c6cb358a8b401369e89522d6c920c193a6b6042ef60cdba3bf03a3688e9e97f823f3edf2963d5be4', 246, 1, 'LaraPassport', '[]', 0, '2020-08-28 14:59:04', '2020-08-28 14:59:04', '2021-08-28 07:59:04'),
('c6fa12eea481a124a4d1733ac5ba95b3395ff27320f3f6fa3a3e621e361f44f83bee697faa86f348', 33, 1, 'LaraPassport', '[]', 0, '2020-04-21 11:09:27', '2020-04-21 11:09:27', '2021-04-21 11:09:27'),
('c714037d268ba23709191ab0dc0718dc0f62a8db289a91b9722478fd449c67968b201de559605765', 212, 1, 'LaraPassport', '[]', 0, '2020-08-18 18:18:08', '2020-08-18 18:18:08', '2021-08-18 11:18:08'),
('c72590f78e9da0e888b2966dc765a9040d0c2b62e35e69441ba384ff3250a45f41fbd2cc914d26b8', 109, 1, 'LaraPassport', '[]', 0, '2020-06-21 10:54:44', '2020-06-21 10:54:44', '2021-06-21 10:54:44'),
('c7c914e83ef74e69621c9b3b35de1f2adea2301c605bb83699ea716ae178a7f19e9007ced506c3e8', 339, 1, 'LaraPassport', '[]', 0, '2020-10-05 18:03:02', '2020-10-05 18:03:02', '2021-10-05 11:03:02'),
('c7f6afef7be6a5252ef9b70d5a3ae907b28f1a938ad16fa2044e1eb0a5ecf55be41a40769a417c85', 33, 1, 'LaraPassport', '[]', 0, '2020-04-11 14:54:42', '2020-04-11 14:54:42', '2021-04-11 14:54:42'),
('c849b36b4422a0f3ca8e51f7fed21903cc9ba80242b24dda7479828fc0721facc04857bcec4db0b8', 109, 1, 'LaraPassport', '[]', 0, '2020-06-21 10:53:47', '2020-06-21 10:53:47', '2021-06-21 10:53:47'),
('c84da878fbd89514e912b6b3ed9622ba7627d60dcca82d02e9ee73e123897b9f657b61408f346065', 234, 1, 'LaraPassport', '[]', 0, '2020-08-22 19:01:30', '2020-08-22 19:01:30', '2021-08-22 12:01:30'),
('c879098b30fb7708fd7fd4274fbb1892eab5eae295d0183c5f0da20555428fa8feb706673e34b54a', 249, 1, 'LaraPassport', '[]', 0, '2020-08-29 23:02:59', '2020-08-29 23:02:59', '2021-08-29 16:02:59'),
('c882ac4b64838b45f928a032a471898da8647bd31ee744c40f85d9a14a84bbb3771af7bc572c220e', 300, 1, 'LaraPassport', '[]', 0, '2020-09-30 20:28:54', '2020-09-30 20:28:54', '2021-09-30 13:28:54'),
('c8e033c77bc7d0ebaf4b27548a38b3e96b3b8fe9cc12ba86efab230ca53a567c57fab1f3a6d036c0', 123, 1, 'LaraPassport', '[]', 0, '2020-07-31 18:49:53', '2020-07-31 18:49:53', '2021-07-31 11:49:53'),
('c9b5a4ebbd7c593e275454be9ccf665b7996079eab53b5af1122fa24113f04663451b85df04f4562', 231, 1, 'LaraPassport', '[]', 0, '2020-09-24 19:15:00', '2020-09-24 19:15:00', '2021-09-24 12:15:00'),
('c9b5c2f3c5a26d86fab4e9fa8f8332815fb317db656ea4e03433da2247ca23349fda99a80ffc142e', 121, 1, 'LaraPassport', '[]', 0, '2020-07-03 18:17:05', '2020-07-03 18:17:05', '2021-07-03 18:17:05'),
('c9d300d415f338bcbad1574fee07f03f3bdae6b4f53b40b5df0dff0a224a1688a642753f054b9f32', 266, 1, 'LaraPassport', '[]', 0, '2020-10-06 18:49:06', '2020-10-06 18:49:06', '2021-10-06 11:49:06'),
('ca4f82cc694922886e1634c1a00d31941c601d2c2715dd68136e130d841ced6142986cd749530140', 35, 1, 'LaraPassport', '[]', 0, '2020-08-18 11:48:49', '2020-08-18 11:48:49', '2021-08-18 04:48:49'),
('ca4fc7b6a9a557a332496633fbba7d7765b26fc964771e0aecf1c7749f6529232a0665f8ed85e54b', 57, 1, 'LaraPassport', '[]', 0, '2020-08-20 21:38:47', '2020-08-20 21:38:47', '2021-08-20 14:38:47'),
('cb1dcebbf6e93a83dc7ab01dc9062b5df75646f50ffeced4cb43eb00c5a0fba091999150c1fd4cd0', 8, 1, 'LaraPassport', '[]', 0, '2020-04-09 12:52:09', '2020-04-09 12:52:09', '2021-04-09 12:52:09'),
('cbc7e280654b5275caa982fa2b98c38c3098f5fbb398d83d4fdcc7ff8c063aebc9b1d14b2ee49734', 2, 1, 'LaraPassport', '[]', 0, '2020-03-19 00:41:59', '2020-03-19 00:41:59', '2021-03-19 06:11:59'),
('cc3fde59607b539d1281d47a16287aed9d5638758b343eee3e2abe7f19f6561babd8596005106b04', 373, 1, 'LaraPassport', '[]', 0, '2020-10-28 19:16:23', '2020-10-28 19:16:23', '2021-10-28 12:16:23'),
('cc4ed1d02b8599d00049bc7bd824ac05be381a52263a8dbed5c162d94eb1d6159110d163811f70fb', 266, 1, 'LaraPassport', '[]', 0, '2020-09-16 18:53:51', '2020-09-16 18:53:51', '2021-09-16 11:53:51'),
('cce6295962772a815174982464d42dfb0da4dc7f2dac789118417df5ad487f5d4c039ff00e4dbfdc', 142, 1, 'LaraPassport', '[]', 0, '2020-07-17 13:43:59', '2020-07-17 13:43:59', '2021-07-17 06:43:59'),
('ccfcd5c3fa8a33824b75f7efa8b79ffc26b1e194fe544c976528a083f426bdcced524a3f80914eec', 62, 1, 'LaraPassport', '[]', 0, '2020-08-18 19:25:26', '2020-08-18 19:25:26', '2021-08-18 12:25:26'),
('cd13ca46717e1bc24062854837230fac8dc56593da45f7d4a7b5eea7108c579098c16ee7de7a24bc', 241, 1, 'LaraPassport', '[]', 0, '2020-08-27 12:53:16', '2020-08-27 12:53:16', '2021-08-27 05:53:16'),
('cd425617755c8923cea76ef63ba07cf99890e91f804ef2c6984790e2a8d03e78b4a3fd7f65e238cb', 33, 1, 'LaraPassport', '[]', 0, '2020-04-11 04:51:33', '2020-04-11 04:51:33', '2021-04-11 04:51:33'),
('cd4b8346c7d2e574b552cd49c7b220e3ed589c027faaab7c0c2875766429cd039c52c12cb03d9ef9', 227, 1, 'LaraPassport', '[]', 0, '2020-08-19 14:40:43', '2020-08-19 14:40:43', '2021-08-19 07:40:43'),
('cdbd81be4919fbc67c472a4dcee53492266693d04bbc854f312cfe465db8a1763a3dbeeda1bcf928', 323, 1, 'LaraPassport', '[]', 0, '2020-09-30 16:40:26', '2020-09-30 16:40:26', '2021-09-30 09:40:26'),
('ce12fc5a90bc75a9833e7962fcdd720e44ec14a9c28cb87bbb0fc54a8ed2cbc347014988f684fcf7', 130, 1, 'LaraPassport', '[]', 0, '2021-01-21 23:42:28', '2021-01-21 23:42:28', '2022-01-21 16:42:28'),
('ce2a1aa3848aa2e363ea405aa159e14426f63964d35d3f756881ec54341421d669d4db04cffa0a1a', 35, 1, 'LaraPassport', '[]', 0, '2020-05-30 12:00:40', '2020-05-30 12:00:40', '2021-05-30 12:00:40'),
('ceb442ceaa31ee6f68572e962b063690329a24888cac34fa739df06bd72768aae07174656b5d1962', 231, 1, 'LaraPassport', '[]', 0, '2020-08-19 20:33:00', '2020-08-19 20:33:00', '2021-08-19 13:33:00'),
('cef9a2b3d93114a335af02ce21981b1bc184b0ada08c1be17d84825bdf6c36a5c8f64526266d3dc4', 35, 1, 'LaraPassport', '[]', 0, '2020-06-17 06:55:46', '2020-06-17 06:55:46', '2021-06-17 06:55:46'),
('cf1c002190e5086fe0ef0a6e3b1bc73d9b0c4d36a40c9ca1a56f79e6150505d2d11a951e5d5f5f76', 244, 1, 'LaraPassport', '[]', 0, '2020-09-01 19:07:49', '2020-09-01 19:07:49', '2021-09-01 12:07:49'),
('cf3392c0307688d72b65f20f815dc72ff7ba5263b179f2453d5322aa0892612e840371823617e115', 119, 1, 'LaraPassport', '[]', 0, '2021-01-18 23:52:53', '2021-01-18 23:52:53', '2022-01-18 16:52:53'),
('cf33a6baf1af2e6058934910a65eeb599526430379d5cc36bbed9edf5f0cd82119ee0ffeab322e95', 4, 1, 'LaraPassport', '[]', 0, '2020-03-19 00:46:38', '2020-03-19 00:46:38', '2021-03-19 06:16:38'),
('cf73ca6cf1623af55853ceb1dd1145aea5368a90297558cb7fee78a7af448d1777c6985922aa7441', 238, 1, 'LaraPassport', '[]', 0, '2020-09-17 11:28:34', '2020-09-17 11:28:34', '2021-09-17 04:28:34'),
('cfa978dd582747f9781457de5df252ec83953aa4121ff9be54daada081b4efe42385ac09ec4c91a5', 425, 1, 'LaraPassport', '[]', 0, '2020-12-03 17:56:15', '2020-12-03 17:56:15', '2021-12-03 10:56:15'),
('cfc39020e923e421e85692ffc3d41f48ca90f478205889ed50bd7b96834e4371779f3e68ec63c2f1', 59, 1, 'LaraPassport', '[]', 0, '2020-04-25 14:08:19', '2020-04-25 14:08:19', '2021-04-25 14:08:19'),
('cff8d931a15c14c2dd6ce6bf5907eb3f6daf6a57222ab2c725c9147dbbe565c9b9f8a01ba53bbbb3', 35, 1, 'LaraPassport', '[]', 0, '2020-06-27 11:36:02', '2020-06-27 11:36:02', '2021-06-27 11:36:02'),
('d03097b433adcfefdfc85bb4d77f045bccc940b684c52c827afb1067f123f4a79f113cb1c5f5ccb2', 249, 1, 'LaraPassport', '[]', 0, '2020-09-30 17:54:15', '2020-09-30 17:54:15', '2021-09-30 10:54:15'),
('d0498a1c2e55d6b26ed3104ded6897a9c721764534384fadfbd0ff06e3a0cb15ea856405d8c058e9', 33, 1, 'LaraPassport', '[]', 0, '2020-04-11 07:50:46', '2020-04-11 07:50:46', '2021-04-11 07:50:46'),
('d06bb1177dead3000229a45f74201f1733a38735bc241fafed042e3ce91c3189c8351eb65a78ce6e', 244, 1, 'LaraPassport', '[]', 0, '2020-09-10 20:16:15', '2020-09-10 20:16:15', '2021-09-10 13:16:15'),
('d070e6dc9b0b635b3b09c159d6cb5843dfbae1658855934261fbc9b09d0fe1efb4aff0c79e4c9695', 33, 1, 'LaraPassport', '[]', 0, '2020-04-18 14:08:44', '2020-04-18 14:08:44', '2021-04-18 14:08:44'),
('d0c369fd4d87591404850bfe6b5dba6690bfff7be55070da030a4c401f89f9006995504e5f48a4ea', 312, 1, 'LaraPassport', '[]', 0, '2020-09-30 16:45:22', '2020-09-30 16:45:22', '2021-09-30 09:45:22'),
('d0fb28943658dcac16c4a8d918fa1cb24cf0271036f770e4fef0cdc8e55dbb45cfde1011e8f23b3a', 97, 1, 'LaraPassport', '[]', 0, '2020-06-11 04:45:36', '2020-06-11 04:45:36', '2021-06-11 04:45:36'),
('d13830e1cb8a647cb386647b5ddfbeb9ba478b32a9d8b5af56f03aae888235cedbdf4e4bdfd2b22b', 57, 1, 'LaraPassport', '[]', 0, '2020-04-27 04:39:39', '2020-04-27 04:39:39', '2021-04-27 04:39:39'),
('d153c7e9fd777144f6b96de084daca0f3c208e49c09630c0f57ebc4213019ea1f1a18055556e6193', 200, 1, 'LaraPassport', '[]', 0, '2020-08-17 22:00:55', '2020-08-17 22:00:55', '2021-08-17 15:00:55'),
('d15c325d172a15fdcbc80ae43bc2bd6c53eba44da7e0145ba5d6009b752ee9c042f851c583c5d373', 415, 1, 'LaraPassport', '[]', 0, '2020-11-22 19:39:07', '2020-11-22 19:39:07', '2021-11-22 12:39:07'),
('d178f2a7318e235d1abcbf64170164f695361583de692dc59747cc968b7ad8709754d4c0bca3d1fb', 33, 1, 'LaraPassport', '[]', 0, '2020-04-11 07:57:17', '2020-04-11 07:57:17', '2021-04-11 07:57:17'),
('d1e0fdd1dcb8a611b5012d9d31e5840c9cff733a5573e651ef19b0378118abdb06d5a6d1b44d9ab8', 35, 1, 'LaraPassport', '[]', 0, '2020-07-17 17:10:10', '2020-07-17 17:10:10', '2021-07-17 10:10:10'),
('d22be6a4f219df4011ae3acc724b45ae41f1bcfeef12374fe3c70dc9edc6dd55cf2a51b63ecebfd4', 62, 1, 'LaraPassport', '[]', 0, '2020-05-27 14:04:57', '2020-05-27 14:04:57', '2021-05-27 14:04:57'),
('d23941f2460d716f05fc225f4bead87a2a085dd5265b35c46f61e56fd7b8b28b42e8282e1c9523e5', 35, 1, 'LaraPassport', '[]', 0, '2020-07-17 18:19:08', '2020-07-17 18:19:08', '2021-07-17 11:19:08'),
('d24d7e17210b3f26d268d7ab846be8e6cacba3f2f85bcb22ffe219ac32545697a8bb8000284852ef', 59, 1, 'LaraPassport', '[]', 0, '2020-04-28 13:37:15', '2020-04-28 13:37:15', '2021-04-28 13:37:15'),
('d28b7000411413b553ed9b2d9c5013b7abededd54c6ec6dc2e74e086c335e9d9059b2788112e9605', 33, 1, 'LaraPassport', '[]', 0, '2020-04-16 11:05:58', '2020-04-16 11:05:58', '2021-04-16 11:05:58'),
('d2c8da70ead9f83d8b3bab13804b4f09fce56a4d4d35d5d9a5d7f63847a899305ee5dad2e2533c44', 97, 1, 'LaraPassport', '[]', 0, '2020-06-09 06:57:54', '2020-06-09 06:57:54', '2021-06-09 06:57:54'),
('d2f2fc3e9045c9c04958bf63846909455bec73617fc133f4213ed315576ef3bc72b8be8ff95ef7ee', 244, 1, 'LaraPassport', '[]', 0, '2020-09-01 19:51:50', '2020-09-01 19:51:50', '2021-09-01 12:51:50'),
('d3305f13ea5820affad21aa548704d6c5f18b71fd8966dda7033c8873dbd9a1874284112485aa2a4', 33, 1, 'LaraPassport', '[]', 0, '2020-04-12 05:03:43', '2020-04-12 05:03:43', '2021-04-12 05:03:43'),
('d3498eeacc5be26f1cf9f6943c63fb59b8af03a65181783d0fe517f279257789a9e242b83cb0f2c2', 33, 1, 'LaraPassport', '[]', 0, '2020-04-14 05:41:15', '2020-04-14 05:41:15', '2021-04-14 05:41:15'),
('d39ae199b2fb6a16e81abbd2e689a7943e15312b9a4d9911b2844641f10c6d0fc34042835b594439', 58, 1, 'LaraPassport', '[]', 0, '2020-05-07 11:08:05', '2020-05-07 11:08:05', '2021-05-07 11:08:05'),
('d39bd04b8c36bc883a872f9a0d7259d8c64530e4019c16cb7a771d70bb234318c6acb602e1ac9f2c', 244, 1, 'LaraPassport', '[]', 0, '2020-09-15 12:33:21', '2020-09-15 12:33:21', '2021-09-15 05:33:21'),
('d3db54f1dd0a279a1e190e6fc9b556d7aa1c16ec6d84f5ff1cfeb0684b0e962bcddac7a58b219f8d', 300, 1, 'LaraPassport', '[]', 0, '2020-09-30 19:23:35', '2020-09-30 19:23:35', '2021-09-30 12:23:35'),
('d3eab839562a23210cc39a636ec4578951cf8e62a8ce023170c145830799f37f4ca1202d755527bc', 249, 1, 'LaraPassport', '[]', 0, '2020-09-30 17:45:34', '2020-09-30 17:45:34', '2021-09-30 10:45:34'),
('d4695fbcd055192ef75b934c29b18d142a1f91d59d0a8665a7863fcdce2f6715eed44b0d727fed30', 236, 1, 'LaraPassport', '[]', 0, '2020-08-22 23:15:10', '2020-08-22 23:15:10', '2021-08-22 16:15:10'),
('d4c83784047ac312595dc762bbf0bca4a6c692d71e1626ebc317a309011a1e4321ec1008e9982f3d', 214, 1, 'LaraPassport', '[]', 0, '2020-08-18 18:32:11', '2020-08-18 18:32:11', '2021-08-18 11:32:11'),
('d4f13c8d5250b1796e2bc0f47dc446a396b3de7ca638dfdc712da17e5583d047bbb8080aab677206', 300, 1, 'LaraPassport', '[]', 0, '2020-09-30 20:28:22', '2020-09-30 20:28:22', '2021-09-30 13:28:22'),
('d51a07795b054dcc234fd09736fb21fa1bdb3b1ccd0e559d7b90e1119b1d273cd9ef4769e5430c4c', 33, 1, 'LaraPassport', '[]', 0, '2020-04-11 11:50:10', '2020-04-11 11:50:10', '2021-04-11 11:50:10'),
('d572b401ec05bd5081d7b14ee20633e77160266dbadaa6f1fcc0ad1ecadef9b931e6e9f65324f377', 389, 1, 'LaraPassport', '[]', 0, '2020-11-13 19:25:16', '2020-11-13 19:25:16', '2021-11-13 12:25:16'),
('d58c4a461c55280f25246ef80a4e5bbcc3984944a44000f578ec74b877d2d43118d8d27605a62fed', 59, 1, 'LaraPassport', '[]', 0, '2020-04-22 09:59:37', '2020-04-22 09:59:37', '2021-04-22 09:59:37'),
('d5bc7b66eae1cd37b3bc4c78c358d4aac40f648ad43d157ac3647169798b664ae56979ffd9d63de4', 122, 1, 'LaraPassport', '[]', 0, '2020-07-03 18:23:01', '2020-07-03 18:23:01', '2021-07-03 18:23:01'),
('d5e4b593c9d9e6ffc3597d449483e3348f15cd6b13be169d6e7165d09103ae94fc6c107a5f0159f0', 300, 1, 'LaraPassport', '[]', 0, '2021-01-05 21:16:38', '2021-01-05 21:16:38', '2022-01-05 14:16:38'),
('d6595beaa479303d91e4d68c0b8a81c7a3e620783767243d86adeaeb1c21f57a340adc7ca4e4b9ef', 33, 1, 'LaraPassport', '[]', 0, '2020-04-11 12:03:12', '2020-04-11 12:03:12', '2021-04-11 12:03:12'),
('d66b74eb16a0d55e6f00c2ccfd46a66ba87acbd71779c6cf9e2a096604ca1dc7ab983130c076a535', 332, 1, 'LaraPassport', '[]', 0, '2020-10-01 14:26:13', '2020-10-01 14:26:13', '2021-10-01 07:26:13'),
('d67d31b356821991f3a7b169a554d9d386cfa4c4a69c1266cdcbcc9bcb9ebaf07ed500f2ea5dd55e', 104, 1, 'LaraPassport', '[]', 0, '2020-06-11 12:56:19', '2020-06-11 12:56:19', '2021-06-11 12:56:19'),
('d6a7a22290583f9818b7a66acbdda9c177cfce0f1924b1aebdb838b04ab24e6783df1ed2e0677946', 383, 1, 'LaraPassport', '[]', 0, '2020-11-06 21:51:30', '2020-11-06 21:51:30', '2021-11-06 14:51:30'),
('d6fe03b90c1668fd5c2d1ef99d8221896ad50fc1799a4cff2bdc987911069c72ade2ac4406caec7f', 236, 1, 'LaraPassport', '[]', 0, '2020-08-24 11:48:55', '2020-08-24 11:48:55', '2021-08-24 04:48:55'),
('d75f37a4fc6a22e2aca0f451dee9b050533f946de35fe2e1473d2ec203415766d6fc3defc533a8d1', 316, 1, 'LaraPassport', '[]', 0, '2020-11-19 18:23:45', '2020-11-19 18:23:45', '2021-11-19 11:23:45'),
('d768d68921c5a741b8cb41d49557f6b6e1a7e853ed15ac291e674208917ca0a7c0643b2adfb09a8a', 33, 1, 'LaraPassport', '[]', 0, '2020-04-13 13:26:11', '2020-04-13 13:26:11', '2021-04-13 13:26:11'),
('d79783e0d116c6fd7caaa1e1c4d1fd919998385b7aa1bb4439d079622e3869faaedbaa140d1c34de', 35, 1, 'LaraPassport', '[]', 0, '2020-05-28 06:45:49', '2020-05-28 06:45:49', '2021-05-28 06:45:49'),
('d7a08176412be315918f617c00c3090ac255fc8c91fc9c4fda132b490638bd7cc72f93672e4cf385', 35, 1, 'LaraPassport', '[]', 0, '2020-08-04 12:39:52', '2020-08-04 12:39:52', '2021-08-04 05:39:52'),
('d7b1a116f95e0ddd59c6a4ff5400657597c05bbf7f97f211e86b9caae662696c5c6984b24bb40748', 57, 1, 'LaraPassport', '[]', 0, '2020-05-07 10:56:34', '2020-05-07 10:56:34', '2021-05-07 10:56:34'),
('d7c147d3359057b1b91ce2ef9d081e01ff31b3ee78e50900e8b07c03ffd9a90b11264bd5ca38360f', 57, 1, 'LaraPassport', '[]', 0, '2020-08-18 12:04:09', '2020-08-18 12:04:09', '2021-08-18 05:04:09'),
('d7ce60982f4eeebaa6e3201ef43e512a2cf89c3f13198153f8f8552a7ffda30662bcb2eacb5a9875', 25, 1, 'LaraPassport', '[]', 0, '2020-04-09 05:31:17', '2020-04-09 05:31:17', '2021-04-09 05:31:17'),
('d7dd4d1121a5eeb82604bcca60d4575105b3adbec17ade2be652edb4249b7183e96d792a701c539b', 364, 1, 'LaraPassport', '[]', 0, '2020-10-15 16:36:26', '2020-10-15 16:36:26', '2021-10-15 09:36:26'),
('d8a590f0b5f3aecac13072c883bbd0c5c7b9b885d71d92ab7ed9ab63dd3c09f49fa8acdf0e04553a', 33, 1, 'LaraPassport', '[]', 0, '2020-04-10 09:32:17', '2020-04-10 09:32:17', '2021-04-10 09:32:17'),
('d8fac864f689566843cfe7d64c18515334dd45314cc9da3739e8a7fd408e3c3c7dd0ac27af40e883', 58, 1, 'LaraPassport', '[]', 0, '2020-04-22 04:51:20', '2020-04-22 04:51:20', '2021-04-22 04:51:20');
INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('d906875ce17b834a00be3d0d80761e626c3988959f2325be90f016ded69bf0aaa86a9640a4faec18', 231, 1, 'LaraPassport', '[]', 0, '2020-09-30 20:28:59', '2020-09-30 20:28:59', '2021-09-30 13:28:59'),
('d9b54f8763e23771b56a549605e544aa690450be36d7a5eb3faf4bda3b7955ea8bccb3a5f0795a14', 57, 1, 'LaraPassport', '[]', 0, '2020-04-24 04:51:41', '2020-04-24 04:51:41', '2021-04-24 04:51:41'),
('d9e89b7c92c5da91e0e21dea306a06fb74f3741512b8e1c05571a6e7ac6702f170a81e04eaee2014', 326, 1, 'LaraPassport', '[]', 0, '2020-09-30 19:01:59', '2020-09-30 19:01:59', '2021-09-30 12:01:59'),
('da0ff6129eea12e8c6ad50d74310e45f44941964946f32267e5c2c0a3bacebc3f8e110d156e3fe58', 231, 1, 'LaraPassport', '[]', 0, '2020-10-09 17:45:37', '2020-10-09 17:45:37', '2021-10-09 10:45:37'),
('da9c427cf94fb57667f10384957eeb46f7198e3f0762b8d49089a0b6360c50dc8212b7b1cf84e62b', 35, 1, 'LaraPassport', '[]', 0, '2020-06-22 05:25:16', '2020-06-22 05:25:16', '2021-06-22 05:25:16'),
('daac34bd1fbb4a0703df3ef810e03b120f0333d9f8366426009bfc7668e6ce9521620f45131543d5', 243, 1, 'LaraPassport', '[]', 0, '2020-08-27 15:15:12', '2020-08-27 15:15:12', '2021-08-27 08:15:12'),
('dadd85a91f57e99f9cb7be36821b8ff0217531bedb56a4a4760f55e9b6040748cad666030ffa8686', 153, 1, 'LaraPassport', '[]', 0, '2020-08-20 12:16:47', '2020-08-20 12:16:47', '2021-08-20 05:16:47'),
('daedbd8dcabeae0ae20f7fcb2834bee6d01b736fe15f781b9f2deff8d46ec3e0a581497bf1268655', 57, 1, 'LaraPassport', '[]', 0, '2020-05-07 10:59:17', '2020-05-07 10:59:17', '2021-05-07 10:59:17'),
('db0b8ec09eefbd2293ce1ecdeef522b0f7036f000008a91e3cd6a1bf1e95ea6c3c83c31dc6a82bb8', 33, 1, 'LaraPassport', '[]', 0, '2020-04-10 08:43:14', '2020-04-10 08:43:14', '2021-04-10 08:43:14'),
('db77401a45715451ffd94a653173b324d8486eaa6788cc3fd5cf9ec5c74ad7f2f2d55783b2348036', 8, 1, 'LaraPassport', '[]', 0, '2020-04-10 05:10:55', '2020-04-10 05:10:55', '2021-04-10 05:10:55'),
('db8045eaf4a743a77a07f42f3d3a20a4da72cbf12592ab393013852d6b9763e277da2d0daf99f2e6', 423, 1, 'LaraPassport', '[]', 0, '2020-12-02 19:09:38', '2020-12-02 19:09:38', '2021-12-02 12:09:38'),
('dbae11db8fd53811ee2176ce664e597736a8dc62db48d30753960c904c10e243c5aea41d0ebe447c', 59, 1, 'LaraPassport', '[]', 0, '2020-04-22 10:01:33', '2020-04-22 10:01:33', '2021-04-22 10:01:33'),
('dbe4c1b2e86c2461722b6500fd470f60a9c3d7321a8ac9e2a8e5af6962875581214c1eae92df1c86', 39, 1, 'LaraPassport', '[]', 0, '2021-01-13 02:56:13', '2021-01-13 02:56:13', '2022-01-12 19:56:13'),
('dc2d4b9e34cc7620330a4ee9cd32600a1876b90f9da7c4e6539a545beeda7c3a9d6b0a476651a198', 35, 1, 'LaraPassport', '[]', 0, '2020-06-25 10:37:40', '2020-06-25 10:37:40', '2021-06-25 10:37:40'),
('dc5371136b1defcb818d52f05f3e5d5240a2b6beaca2f256d3111bb6cca0437e24036350e252113b', 214, 1, 'LaraPassport', '[]', 0, '2020-08-18 19:32:51', '2020-08-18 19:32:51', '2021-08-18 12:32:51'),
('dc7b447533c2661261cec5839895bf7261a595d3fdf4f51930b60e9d607e057be436cdf96c2eb6ba', 57, 1, 'LaraPassport', '[]', 0, '2020-08-05 16:15:24', '2020-08-05 16:15:24', '2021-08-05 09:15:24'),
('dc854a5c3cb6dae9ec0fb43d5b73631ccc46e1edfa93c8f7608df454ddb477e61a9de1f7e96a1434', 57, 1, 'LaraPassport', '[]', 0, '2020-07-05 18:42:18', '2020-07-05 18:42:18', '2021-07-05 18:42:18'),
('dc89256821c8d9bb70366a7eb0ff37f90dbaa52ff80cd2ee8217e0f091c594e6e2efb5ba81eda63c', 33, 1, 'LaraPassport', '[]', 0, '2020-05-12 05:46:08', '2020-05-12 05:46:08', '2021-05-12 05:46:08'),
('dc8b7183a877990f1aecf9e2d936707b9c2d020edbf2e2e00a572eedb7cb9bc7fac715d1cec87e57', 35, 1, 'LaraPassport', '[]', 0, '2020-05-12 07:29:57', '2020-05-12 07:29:57', '2021-05-12 07:29:57'),
('dd18bcb32ac5e991e760b30e0203827eccad5e9dbd55331902d6615579318d8b289121d31ccbeecd', 57, 1, 'LaraPassport', '[]', 0, '2020-06-22 05:19:05', '2020-06-22 05:19:05', '2021-06-22 05:19:05'),
('dd226574f6b37479e8b9afc87164917378edf94d1f321e01c86a635b7ebe9f4793e35229c73142af', 222, 1, 'LaraPassport', '[]', 0, '2020-08-19 12:19:56', '2020-08-19 12:19:56', '2021-08-19 05:19:56'),
('dd3419e8cad1b9f80fe62e801c94fb479799ffae52e825d123e1e86d661fcaba490967b16ab74d25', 35, 1, 'LaraPassport', '[]', 0, '2020-06-02 05:27:19', '2020-06-02 05:27:19', '2021-06-02 05:27:19'),
('dd51d463783016c5249271ddc13e5fcc3f9947666fca9975fe3fb83a47f079ab8d25a52ede040783', 35, 1, 'LaraPassport', '[]', 0, '2020-05-30 12:33:43', '2020-05-30 12:33:43', '2021-05-30 12:33:43'),
('dd53a737c6fdc943350fc18424e9d4b0c0d83d0c1eac37411cc1adbf7d2fc33d429fa1c9682cc92e', 57, 1, 'LaraPassport', '[]', 0, '2020-07-03 18:37:38', '2020-07-03 18:37:38', '2021-07-03 18:37:38'),
('ddd4c5d6136ec54b5018678df8259ea687d37be4f1c0dc198d868058bde0f10b355170f9c8f58e12', 387, 1, 'LaraPassport', '[]', 0, '2020-11-07 23:41:25', '2020-11-07 23:41:25', '2021-11-07 16:41:25'),
('de2bb3731bfec66c5b6d670e81863a3d39be1bc2e1c3bf87929cfc0c1d0b13c6cf6621d7adb3a3e3', 33, 1, 'LaraPassport', '[]', 0, '2020-04-10 07:38:20', '2020-04-10 07:38:20', '2021-04-10 07:38:20'),
('de613a0c2702ce28a420d181c3aed5d67fd1ee2a7e992c83676337b716def403eaa0f42bc2cb8726', 371, 1, 'LaraPassport', '[]', 0, '2020-10-27 13:17:55', '2020-10-27 13:17:55', '2021-10-27 06:17:55'),
('de62207185579ea4a1a7ca22d2c87af9851e9536d3fe83b6bc88e0e54064d4ab3c7c6252d6df638a', 59, 1, 'LaraPassport', '[]', 0, '2020-05-16 12:27:29', '2020-05-16 12:27:29', '2021-05-16 12:27:29'),
('de8b94e39c039c8f3cecadeeef1393634009ed372011fecb22d333a183f1b0b42a351eb62af294ad', 420, 1, 'LaraPassport', '[]', 0, '2020-11-24 20:03:03', '2020-11-24 20:03:03', '2021-11-24 13:03:03'),
('df062b59784369c0647134c8f4c16a4d0dbd093d5741cb762391677f9c94e6dcca5ead777e0ea4f6', 35, 1, 'LaraPassport', '[]', 0, '2020-07-31 15:43:20', '2020-07-31 15:43:20', '2021-07-31 08:43:20'),
('df3d8e0125ef0fafd93167eab794ecc40772fd7d5c6a30de2ce7ccd937e4818ef18f01e6be0ca6d2', 59, 1, 'LaraPassport', '[]', 0, '2020-04-29 11:47:12', '2020-04-29 11:47:12', '2021-04-29 11:47:12'),
('df5dfc7d517afc7f0f808b43cc0e2883ed235a438e096f816cd31371c652984ffca36774f8940cb6', 33, 1, 'LaraPassport', '[]', 0, '2020-04-10 12:14:17', '2020-04-10 12:14:17', '2021-04-10 12:14:17'),
('dfa024331c811907cac65b631b3756fac32e1c46cae0be98fd14aa43340c316332e1af0fc9d31013', 8, 1, 'LaraPassport', '[]', 0, '2020-04-10 05:58:35', '2020-04-10 05:58:35', '2021-04-10 05:58:35'),
('dfd862eb7bb823bfa548cb9b5a04024e4b97fa4471dc0d429660bc941950a78a7efd2da2e95f3238', 62, 1, 'LaraPassport', '[]', 0, '2020-04-27 06:47:37', '2020-04-27 06:47:37', '2021-04-27 06:47:37'),
('dfedbe284ecf8c292b78cbde54dbaf188b92a5cfbca2560298348e5d80b425de862ae51d232faedd', 307, 1, 'LaraPassport', '[]', 0, '2020-09-17 15:19:19', '2020-09-17 15:19:19', '2021-09-17 08:19:19'),
('e0007abaeaead67f90a79ed5e4fee7a1bc78f2765b45cfbeefb9b00e6220a8947bf97e7692e35f32', 33, 1, 'LaraPassport', '[]', 0, '2020-04-14 05:13:57', '2020-04-14 05:13:57', '2021-04-14 05:13:57'),
('e074169c180d0aa0028c9e5d8fd98bdc5ea971e843ba8e298b62fa88f94143b7b2c3d8e4f9994d67', 57, 1, 'LaraPassport', '[]', 0, '2020-07-05 11:17:43', '2020-07-05 11:17:43', '2021-07-05 11:17:43'),
('e0a4d2d2b86f92d2a0cc30ef1a87d146e1789c8c04433ccb82c8a072cc1deac3143a634c9820e31d', 283, 1, 'LaraPassport', '[]', 0, '2020-09-10 11:37:01', '2020-09-10 11:37:01', '2021-09-10 04:37:01'),
('e0d73a94657989305df20c99459111fa570c2fc35d2aefacacc492d06365408a31d37335462b3296', 57, 1, 'LaraPassport', '[]', 0, '2020-07-30 18:20:06', '2020-07-30 18:20:06', '2021-07-30 11:20:06'),
('e13db8e2aeb5ff3cca478b84c8fc57da0aee4d6aa29e6f023fcd3596623c8b16b65a17c4907c37fd', 316, 1, 'LaraPassport', '[]', 0, '2020-09-23 16:45:58', '2020-09-23 16:45:58', '2021-09-23 09:45:58'),
('e14bf95140f46bff5241ebda69bb2b4d3760008f44f8ea64119627ddc2e69c87fd13cda561ee7ae1', 304, 1, 'LaraPassport', '[]', 0, '2020-09-16 23:07:04', '2020-09-16 23:07:04', '2021-09-16 16:07:04'),
('e1d0e20134adde84f83f2c222c197e7416c8512e7dca73df89f4cecb1cc9454c68ecc5ead662ede5', 57, 1, 'LaraPassport', '[]', 0, '2020-08-21 11:43:00', '2020-08-21 11:43:00', '2021-08-21 04:43:00'),
('e1f38d0e265ddce1f39be5a2c25991d53af3989d2cbb57482e73572fe9ce3a45cc54064fd20704e0', 58, 1, 'LaraPassport', '[]', 0, '2020-04-22 08:52:20', '2020-04-22 08:52:20', '2021-04-22 08:52:20'),
('e1fc0eefe24e88f5d0760190fc778367298046fea6a7cbad606893e2b5c91c3c2f5cd5b7e71664e8', 35, 1, 'LaraPassport', '[]', 0, '2020-05-19 14:52:40', '2020-05-19 14:52:40', '2021-05-19 14:52:40'),
('e2367ce71bec8a777dbc88e6272c84d55546327d68423df270186fb0af41c9d13dbb613643d43407', 300, 1, 'LaraPassport', '[]', 0, '2021-01-07 01:42:31', '2021-01-07 01:42:31', '2022-01-06 18:42:31'),
('e24cfcb3a90133535c76bdd8c9e37194576b79aaa3bc4af339e93ed0d12b2a9de0a0a5dc513a2ace', 97, 1, 'LaraPassport', '[]', 0, '2020-06-13 11:36:54', '2020-06-13 11:36:54', '2021-06-13 11:36:54'),
('e2573c87f542fbad5977d91db6a60a92e7e4165370e6641d1a19cff668d9673f74b9654811d47c95', 172, 1, 'LaraPassport', '[]', 0, '2020-08-16 23:08:26', '2020-08-16 23:08:26', '2021-08-16 16:08:26'),
('e275d8116a534a988127c7edaf4c76fd1143805a68ecc5bd253d2389332dad1b32afa39b5324672d', 104, 1, 'LaraPassport', '[]', 0, '2020-06-11 12:52:08', '2020-06-11 12:52:08', '2021-06-11 12:52:08'),
('e2809de69bdc006151215107830e10e86e469a6063bc66db05fecf2bb8abf1f989ebac710692ef27', 316, 1, 'LaraPassport', '[]', 0, '2020-09-30 20:29:41', '2020-09-30 20:29:41', '2021-09-30 13:29:41'),
('e2daf72522d0ecc9197df85a8cfd890a3828ecd8d7cde321a5a6237291031dd90ffcca1a3069644d', 57, 1, 'LaraPassport', '[]', 0, '2020-04-27 04:39:51', '2020-04-27 04:39:51', '2021-04-27 04:39:51'),
('e340050e0822696f2030875795a9956a27e405219d3fa5d21e415e2cc803f2c6be2acc60f6b83e1c', 35, 1, 'LaraPassport', '[]', 0, '2020-08-14 13:43:14', '2020-08-14 13:43:14', '2021-08-14 06:43:14'),
('e35513efc029654ebda4c575249af65b7dab37e0d131b204191cea42a13caee1eb7ba13b7d2ed886', 172, 1, 'LaraPassport', '[]', 0, '2020-08-14 17:16:06', '2020-08-14 17:16:06', '2021-08-14 10:16:06'),
('e363033942bcb751a12406c2db2e2a37b966ccb5555506d47aa70055acff3e27c4d604b9be3caae4', 286, 1, 'LaraPassport', '[]', 0, '2020-09-10 20:27:52', '2020-09-10 20:27:52', '2021-09-10 13:27:52'),
('e383356a2d8c5304d9383ed032c5e4e1f706889a0765ed3c215ef2734af35c99f07f1eae9b1bab92', 316, 1, 'LaraPassport', '[]', 0, '2020-10-28 22:25:51', '2020-10-28 22:25:51', '2021-10-28 15:25:51'),
('e3c06083115d5b0b07ff6a363b30c94ea7c69af7a9a220a972aebbb4c91e694d576a141fe5d64a3e', 119, 1, 'LaraPassport', '[]', 0, '2020-06-29 06:00:32', '2020-06-29 06:00:32', '2021-06-29 06:00:32'),
('e4569bd138923eb4574b63195239582d46b4ef141ff68589afe369b315404d2d713f5e1f53aeb29e', 33, 1, 'LaraPassport', '[]', 0, '2020-04-10 11:00:49', '2020-04-10 11:00:49', '2021-04-10 11:00:49'),
('e4a248eaf9afbac94370838668c3eda41a787d30b001a1f11562d5be9619acff0b49111c45ad17b9', 249, 1, 'LaraPassport', '[]', 0, '2020-09-30 17:53:36', '2020-09-30 17:53:36', '2021-09-30 10:53:36'),
('e4dfe968441959ad8e63f945296106ebfb75fd4403eb35a155bfd10b6fb6a593dfc405407ff16e88', 266, 1, 'LaraPassport', '[]', 0, '2020-10-14 18:34:27', '2020-10-14 18:34:27', '2021-10-14 11:34:27'),
('e4e364d6ffb825ca4acaaa11d8df6f53c287dada748216ed8878e5cab8beba6fc7a31e4654d6731e', 8, 1, 'LaraPassport', '[]', 0, '2020-04-10 04:21:01', '2020-04-10 04:21:01', '2021-04-10 04:21:01'),
('e50c14f0e57d09218de9115387d8672bf418e43477ba4c9e402025a3a33c671cde6f5c2b77747a04', 33, 1, 'LaraPassport', '[]', 0, '2020-04-10 11:56:43', '2020-04-10 11:56:43', '2021-04-10 11:56:43'),
('e518ea511415debfa2e12c18b57da81b55706e3befbd65727271c83577ac6b10c0e4595511673a1b', 254, 1, 'LaraPassport', '[]', 0, '2020-08-31 17:40:56', '2020-08-31 17:40:56', '2021-08-31 10:40:56'),
('e545346fcdeb0949411e4db77934bb66b006e0aee02ee5a0a30f5898d9d8281e698ae763226ab872', 35, 1, 'LaraPassport', '[]', 0, '2020-05-30 11:46:18', '2020-05-30 11:46:18', '2021-05-30 11:46:18'),
('e5731591513972b06aff946ee385272fa027459b63be3625781a69004f346d8a24c612bed23d2a44', 33, 1, 'LaraPassport', '[]', 0, '2020-04-17 12:40:40', '2020-04-17 12:40:40', '2021-04-17 12:40:40'),
('e5d54457d2c41abd43f3b1183aa4ec9b7d5395dedfef3f85e55c06bf8951e0ea85fd99dbb4861d75', 434, 1, 'LaraPassport', '[]', 0, '2020-12-26 19:32:37', '2020-12-26 19:32:37', '2021-12-26 12:32:37'),
('e63d96b0f5ceebb86a67426fa581a103ca1a74d2493133755540d42b4013f554039b008c5cdf7316', 33, 1, 'LaraPassport', '[]', 0, '2020-04-11 07:36:35', '2020-04-11 07:36:35', '2021-04-11 07:36:35'),
('e6a6e0991ac473955b6cd3537536b7cf0e409d7a41cc996fe6d8c64b4aa062bbd244b81c36f39289', 58, 1, 'LaraPassport', '[]', 0, '2020-05-07 11:04:15', '2020-05-07 11:04:15', '2021-05-07 11:04:15'),
('e6d059ed6f1940c1d61dfca282acad7c7ee9fe8614d22937a7253eb84669009efeda0b31679a04e1', 290, 1, 'LaraPassport', '[]', 0, '2020-09-12 00:07:11', '2020-09-12 00:07:11', '2021-09-11 17:07:11'),
('e716a71b5d5aa0c2777ade99e63aa576bfc1eb963fbf402c58594d0def5951b31d4dae923a90250d', 370, 1, 'LaraPassport', '[]', 0, '2020-10-24 22:41:22', '2020-10-24 22:41:22', '2021-10-24 15:41:22'),
('e770a989ff15ce3b8e9622d54c03d5e24634d34348fd8043697ddbdf56b1938b35346c90248f997a', 338, 1, 'LaraPassport', '[]', 0, '2020-10-02 19:57:45', '2020-10-02 19:57:45', '2021-10-02 12:57:45'),
('e7f302dc0cb6b4cf96579242270fb368c48c66d1db65c88671eeaaf84c83fdbb101787a7fe2214af', 33, 1, 'LaraPassport', '[]', 0, '2020-04-13 13:27:54', '2020-04-13 13:27:54', '2021-04-13 13:27:54'),
('e85c3bd2b4ca7f5756ab67cc560b478de8e94b336cc82dd179a923d2da0f975aa26209f59a932939', 270, 1, 'LaraPassport', '[]', 0, '2020-09-07 21:23:58', '2020-09-07 21:23:58', '2021-09-07 14:23:58'),
('e8cd1d10e6f04a2a22c3fb1714b5d76250e667d082f1561621017c57905e397452af6e2ae370d06d', 109, 1, 'LaraPassport', '[]', 0, '2021-01-16 02:40:31', '2021-01-16 02:40:31', '2022-01-15 19:40:31'),
('e9bc332860cf61e6753ec6a8054e7e07eae65f0a1aa13a56b05db9d78790c2376dfc224cfb8c0e79', 307, 1, 'LaraPassport', '[]', 0, '2020-09-22 18:17:34', '2020-09-22 18:17:34', '2021-09-22 11:17:34'),
('e9e4e5c9bd14a529e3abdc9fd30c358c802ec26ed1d8e7087b5db937d476dd7ab62a0ea9448c4b94', 33, 1, 'LaraPassport', '[]', 0, '2020-04-20 09:19:56', '2020-04-20 09:19:56', '2021-04-20 09:19:56'),
('ea387b3d2e7fd569f9e9f4cbffa6bcfd6e06b1889da78e1e8744d5a45a5999ad8e5171823cb12eca', 52, 1, 'LaraPassport', '[]', 0, '2020-04-17 09:56:20', '2020-04-17 09:56:20', '2021-04-17 09:56:20'),
('ea90c8cfadbc18c9501c658c0771f50a657315b763e6564555148a3a254c24fe4b8c091dd459f566', 33, 1, 'LaraPassport', '[]', 0, '2020-04-16 17:42:19', '2020-04-16 17:42:19', '2021-04-16 17:42:19'),
('ead0ea75bf4e3237b333a70b72fe611885d760cf0bb3484ee4877e99def3cd97ee9f139016045140', 33, 1, 'LaraPassport', '[]', 0, '2020-04-14 06:50:21', '2020-04-14 06:50:21', '2021-04-14 06:50:21'),
('eaf6a782d2e8cc15c1b719a84916f0eb195d061e7f30cfdbc03e7119149dd38d6614cdca902ca662', 33, 1, 'LaraPassport', '[]', 0, '2020-04-14 05:13:33', '2020-04-14 05:13:33', '2021-04-14 05:13:33'),
('ebc234dd04a9f6de914dba33c7a45a03bb68b244f7b2be1bf8d447a76e113385a7fc01b58a67dbc6', 114, 1, 'LaraPassport', '[]', 0, '2021-01-17 19:11:11', '2021-01-17 19:11:11', '2022-01-17 12:11:11'),
('ebfa34ca4689e00aa51474c4ba89ef74935fda274366dedd667bc423308e7d9fb8b73a9dcbee3158', 57, 1, 'LaraPassport', '[]', 0, '2020-06-22 05:04:38', '2020-06-22 05:04:38', '2021-06-22 05:04:38'),
('ec197ce24c5070370d77852404f163e68350e79ad4b7235cf2b67c0ec69a5397ebb0141923b3ccd3', 334, 1, 'LaraPassport', '[]', 0, '2020-10-01 17:18:32', '2020-10-01 17:18:32', '2021-10-01 10:18:32'),
('ec1e19661a3107d395c9c2c71c42ad39eecfd9dd9f5f9d6dbda679f1bfb4210b2b1403bc380fc616', 57, 1, 'LaraPassport', '[]', 0, '2020-05-07 10:40:00', '2020-05-07 10:40:00', '2021-05-07 10:40:00'),
('ed64416f7d0aebccc9948f6ee15718e0dd6835f50a441d7a51d627ad53785533c3ac7a9ad5a14fc9', 8, 1, 'LaraPassport', '[]', 0, '2020-04-10 04:38:30', '2020-04-10 04:38:30', '2021-04-10 04:38:30'),
('ed91b3a08764cc6910f47ef6de2c26ba430ab710545852edb77e34f2afe267687be4e246cad5d87f', 57, 1, 'LaraPassport', '[]', 0, '2020-04-22 05:43:12', '2020-04-22 05:43:12', '2021-04-22 05:43:12'),
('edc89c212cab8892e3c402fa8487e313e1ab9cd358f118f6a2fd38ddfbfa9ff33633dd1016dbef39', 213, 1, 'LaraPassport', '[]', 0, '2020-08-18 18:21:11', '2020-08-18 18:21:11', '2021-08-18 11:21:11'),
('edf182591ebb52d5188ffadd2a5100a74e779eebd58f8dfb989e42cc853dd86f2f9ba1f9f49634ee', 33, 1, 'LaraPassport', '[]', 0, '2020-04-11 04:42:41', '2020-04-11 04:42:41', '2021-04-11 04:42:41'),
('ee12a754087b9ecedabaab04ec25de009ddd7b38792ef603ae17891b7e0cec7c5d6e8b9c8c2490b1', 57, 1, 'LaraPassport', '[]', 0, '2020-08-04 13:10:20', '2020-08-04 13:10:20', '2021-08-04 06:10:20'),
('ee285df948d26cfa88512395a73b3bcfffb47a745f420fdd7ddc0b928be75294fcf8c527ff2661ff', 8, 1, 'LaraPassport', '[]', 0, '2020-04-10 05:50:36', '2020-04-10 05:50:36', '2021-04-10 05:50:36'),
('ee8b1f27c873d8560bd79daf72e4ad510600a67bf36f36a87abf2e36bb9da42af57be6d6c3457e6d', 35, 1, 'LaraPassport', '[]', 0, '2020-05-12 05:57:13', '2020-05-12 05:57:13', '2021-05-12 05:57:13'),
('eeabd77652aae03014209c7f2b3e87d35c1babd6b35a4fdc8455819e1d603705a111487e2dea2c70', 237, 1, 'LaraPassport', '[]', 0, '2020-08-24 12:29:39', '2020-08-24 12:29:39', '2021-08-24 05:29:39'),
('eed34f349a26408249561f0bca6f9b8e215ead658505dc2df752b2f7049f9a345b6da6d9540e6017', 35, 1, 'LaraPassport', '[]', 0, '2020-05-15 13:01:34', '2020-05-15 13:01:34', '2021-05-15 13:01:34'),
('ef077690f6fba4c477485fd1764249408a65febc2d79012be03070c5e6c22516e1169cfda8d81e25', 62, 1, 'LaraPassport', '[]', 0, '2020-08-17 22:06:58', '2020-08-17 22:06:58', '2021-08-17 15:06:58'),
('ef234f9f16c37dfa17634f81b92b180c2621e0be212b06cbe484a2f81839c2600e7a11d1ed60b7b9', 244, 1, 'LaraPassport', '[]', 0, '2020-09-24 19:38:59', '2020-09-24 19:38:59', '2021-09-24 12:38:59'),
('ef7908572ab43cbae0d1da6c471056d825c63f09de8d9d207d7daecbc66c134a559d41fe7bf669c8', 354, 1, 'LaraPassport', '[]', 0, '2020-10-13 02:23:09', '2020-10-13 02:23:09', '2021-10-12 19:23:09'),
('ef7bbb156da1ea01581a9573b6ff17e3875ae1ed207ada57120a132941065a2881d15b50b9b55692', 316, 1, 'LaraPassport', '[]', 0, '2020-10-20 17:33:36', '2020-10-20 17:33:36', '2021-10-20 10:33:36'),
('efaeb15e201e9593c3aa208ba74337f088750182241abd0745f121eb19a19c99ab5b4ce21ada41bf', 237, 1, 'LaraPassport', '[]', 0, '2020-09-05 12:25:37', '2020-09-05 12:25:37', '2021-09-05 05:25:37'),
('efaf186fe8efb7a3dcc7b848f982a50194e99c9a7b9577cd7794f5eb7e9f90dc051ecf51c9d63a26', 241, 1, 'LaraPassport', '[]', 0, '2020-08-27 12:55:51', '2020-08-27 12:55:51', '2021-08-27 05:55:51'),
('efb4c23503e756b170afb222b06a99f2e8acbde4fd282f8f8d80ee8dfb9493ad7633d5173a27a99b', 247, 1, 'LaraPassport', '[]', 0, '2020-09-20 23:19:24', '2020-09-20 23:19:24', '2021-09-20 16:19:24'),
('efda07bcdd1a6019d7b691f41473672970be9b26fa2abcb6758e36f68a6292a79fb791c0d6eaa795', 316, 1, 'LaraPassport', '[]', 0, '2021-01-07 01:07:19', '2021-01-07 01:07:19', '2022-01-06 18:07:19'),
('efe1c4f114ec678f1ae104333e368da4fbad540aa3a44c4336b1d2ea6d2c16ec2059dc19373432b6', 127, 1, 'LaraPassport', '[]', 0, '2020-07-05 07:42:54', '2020-07-05 07:42:54', '2021-07-05 07:42:54'),
('eff34263d3ce30ed47a5d92f0db651f60402b96c65eeae3eb8ca2e2228ed2bd2b9a129a7cb2d5307', 300, 1, 'LaraPassport', '[]', 0, '2020-09-30 19:14:57', '2020-09-30 19:14:57', '2021-09-30 12:14:57'),
('effba6f68744cb1de879f32974aec4a4cc06b9b08d3e2a500ff5b6cb5593898ddfaef229503c0c4d', 239, 1, 'LaraPassport', '[]', 0, '2020-08-24 13:43:43', '2020-08-24 13:43:43', '2021-08-24 06:43:43'),
('f075524fbfeac6aaa168c7c4a27fd315e169c0dcbe430d0dab356add2d0daed0676cce3e3cb052a0', 35, 1, 'LaraPassport', '[]', 0, '2020-06-11 04:48:18', '2020-06-11 04:48:18', '2021-06-11 04:48:18'),
('f07dd05ff93aa338c9073dae232d0d7e3b93d210f20282011984a554c74983e432c2572bb3b42aac', 314, 1, 'LaraPassport', '[]', 0, '2020-09-22 13:06:24', '2020-09-22 13:06:24', '2021-09-22 06:06:24'),
('f0d5f7b7a51335bbba45124b33ba7969cc83f5341bee009d59d4a9376189facaf2fb2d23c753d153', 358, 1, 'LaraPassport', '[]', 0, '2020-10-14 16:40:22', '2020-10-14 16:40:22', '2021-10-14 09:40:22'),
('f10866a593bde602b3b82ec5c4d0b312f2929b103bc4a746dd64afcfbf88372867564fc581d01afa', 97, 1, 'LaraPassport', '[]', 0, '2020-06-05 05:41:38', '2020-06-05 05:41:38', '2021-06-05 05:41:38'),
('f10990e0c9c77f3d208e21870aed5de36bbf8bd7181ddcd13672938900d2f2a55e59adfff75e98c9', 316, 1, 'LaraPassport', '[]', 0, '2020-12-28 01:13:57', '2020-12-28 01:13:57', '2021-12-27 18:13:57'),
('f1180d35aaa42a072d681836008f73aaed115993d38d5d7434bea8a80d2d2e23825dd23e71feba07', 309, 1, 'LaraPassport', '[]', 0, '2020-10-13 18:41:20', '2020-10-13 18:41:20', '2021-10-13 11:41:20'),
('f12c1e2b7616adedbd60083c5a2ff9aef4d8713d724a9e0d15ca1c1bc51b3dd68b847b6a063d3419', 300, 1, 'LaraPassport', '[]', 0, '2021-01-05 20:46:04', '2021-01-05 20:46:04', '2022-01-05 13:46:04'),
('f17a2dc2ced5dec4337c3441195d114dab9db1b1df7f9267ffbe3bdcbb3dd4e7daea9253ba33850d', 33, 1, 'LaraPassport', '[]', 0, '2020-04-10 08:27:19', '2020-04-10 08:27:19', '2021-04-10 08:27:19'),
('f28137468acc9bf08856a8dff716b0456fb4b052f2971d129eeace5dbfe722bc202d232828825b82', 238, 1, 'LaraPassport', '[]', 0, '2020-09-05 12:28:05', '2020-09-05 12:28:05', '2021-09-05 05:28:05'),
('f290da50a18657d3f0bb5bbcf2a724044e2cf5a6de9738b92d8cfe2d606154c61735781a7672772c', 427, 1, 'LaraPassport', '[]', 0, '2020-12-06 03:28:55', '2020-12-06 03:28:55', '2021-12-05 20:28:55'),
('f2bdcc7be4164d79eff5c4aec48dcdb739aeb28de0206b8797ae389339ada1b8d211a4083fde97f1', 33, 1, 'LaraPassport', '[]', 0, '2020-04-10 11:54:06', '2020-04-10 11:54:06', '2021-04-10 11:54:06'),
('f2d8c6c18cf58813abead5653e5537e7982f48cb8280175319a90b15482eb7f2e418423bb4306164', 290, 1, 'LaraPassport', '[]', 0, '2020-09-11 10:36:15', '2020-09-11 10:36:15', '2021-09-11 03:36:15'),
('f2f040a242a654b3536eae44a2d0239496d7336afd4cf7baf624590d79d95656eafdf779632401ef', 8, 1, 'LaraPassport', '[]', 0, '2020-04-10 05:52:46', '2020-04-10 05:52:46', '2021-04-10 05:52:46'),
('f2fac327bf34c5ff8d143f8e8fd041c89b6c4e25bfa309eb2f4df0a5cba898bd521f6f20bd1f1435', 305, 1, 'LaraPassport', '[]', 0, '2020-10-04 17:43:32', '2020-10-04 17:43:32', '2021-10-04 10:43:32'),
('f31ddcfab98f3b60bf38f0fc75b39c821202a77d3e68cd9a06b7e3a4f07302d7f2de56153d41f38b', 104, 1, 'LaraPassport', '[]', 0, '2020-06-13 11:39:03', '2020-06-13 11:39:03', '2021-06-13 11:39:03'),
('f349be5f80b429bde85647081f061a0d9e04f84b264407cfe3fa421a017cd71f4d1a5c85949d3fe3', 33, 1, 'LaraPassport', '[]', 0, '2020-04-20 14:02:13', '2020-04-20 14:02:13', '2021-04-20 14:02:13'),
('f35f478cecca1a4769e7627a88d1416a9206668da991c59d2e95103a93558685390ded2859a4e5f1', 356, 1, 'LaraPassport', '[]', 0, '2020-10-24 14:53:44', '2020-10-24 14:53:44', '2021-10-24 07:53:44'),
('f39a415b18fd82d1d7af32e155c35bd6907124d9cfc772df251d0b4e2c2609d31e4bb16def3064aa', 35, 1, 'LaraPassport', '[]', 0, '2020-08-18 11:31:32', '2020-08-18 11:31:32', '2021-08-18 04:31:32'),
('f39fee8e1fec50d53569947f58d8f8951e1e896bcc96fc5f942e6e72bdb18531e5a61ab5c49b2665', 33, 1, 'LaraPassport', '[]', 0, '2020-04-11 05:26:50', '2020-04-11 05:26:50', '2021-04-11 05:26:50'),
('f4247527130668a3358a1088b182de80b47a6a2c28f89d621f0249e516ef8a63d6637ba9cee8b362', 104, 1, 'LaraPassport', '[]', 0, '2020-06-11 12:52:04', '2020-06-11 12:52:04', '2021-06-11 12:52:04'),
('f4516a85981bc1132c843d9291f53b7642a0241d7c3df4df6c524927662fa61920e086c1c167bf42', 57, 1, 'LaraPassport', '[]', 0, '2020-06-25 05:41:36', '2020-06-25 05:41:36', '2021-06-25 05:41:36'),
('f5194640e49f55c8a6dc39fc00e5e462ce8d1c242dbc11d32e08aa8c9214f59b0bd0e21e28956828', 153, 1, 'LaraPassport', '[]', 0, '2020-08-27 00:30:11', '2020-08-27 00:30:11', '2021-08-26 17:30:11'),
('f5208c5b247b0ac75d515ae2ac49f4795881224f0eb2fbc88f82ed311941522e588d5e3ec0ce21a9', 239, 1, 'LaraPassport', '[]', 0, '2020-08-25 08:38:56', '2020-08-25 08:38:56', '2021-08-25 01:38:56'),
('f55b78ba92e7b9dbae9b2b9dc12d5fbefed5c26d88c962bbd61fc7bec45ef620e9fb6d8175bd8cc3', 300, 1, 'LaraPassport', '[]', 0, '2020-09-30 19:14:39', '2020-09-30 19:14:39', '2021-09-30 12:14:39'),
('f5825562da5c091dce4dd3c35f76f58e05ba8cc68e1d970823a178070b46c819645ed714290c260c', 18, 1, 'LaraPassport', '[]', 0, '2020-04-09 05:12:51', '2020-04-09 05:12:51', '2021-04-09 05:12:51'),
('f5add678f72ef4d992be3db5a601438e2c2c8267e013bd08459c48a648308832cfe67f19ca2e3463', 57, 1, 'LaraPassport', '[]', 0, '2020-05-01 07:50:53', '2020-05-01 07:50:53', '2021-05-01 07:50:53'),
('f5bf160b861ff79f01b0b6a9b410993319dea01afebd2534e5612ef3911f61e7c4d88ad0cd718b67', 268, 1, 'LaraPassport', '[]', 0, '2020-09-06 16:00:09', '2020-09-06 16:00:09', '2021-09-06 09:00:09'),
('f5df382deda632c88a33a05afa7e4fd84b85f8f3244d7050a11dee7cca5ec72e81d796e4a65a418e', 33, 1, 'LaraPassport', '[]', 0, '2020-04-21 04:38:48', '2020-04-21 04:38:48', '2021-04-21 04:38:48'),
('f60674a3ea702edcc58628925ff9d4dc2c635a10403217ddb6b1965901f0a3bef3b17520d692e3d1', 33, 1, 'LaraPassport', '[]', 0, '2020-04-11 12:47:09', '2020-04-11 12:47:09', '2021-04-11 12:47:09'),
('f64931111d64a370ca5c8e507f09fed783fcdf4ad519cba0624dc87393a245abe35de7279d584e6a', 33, 1, 'LaraPassport', '[]', 0, '2020-04-11 12:09:21', '2020-04-11 12:09:21', '2021-04-11 12:09:21'),
('f65a413fca6a393f73d2f484743566d7dffbd724b847eb9f763a47cc09ee98ee9a3a4507a45a6e24', 244, 1, 'LaraPassport', '[]', 0, '2020-09-01 20:16:29', '2020-09-01 20:16:29', '2021-09-01 13:16:29'),
('f6b5948685bf18d8a4bffd65adcea867feccf1157f4428efe1595f036944cdb7bfd55bb97dba0f1d', 35, 1, 'LaraPassport', '[]', 0, '2020-05-16 04:03:58', '2020-05-16 04:03:58', '2021-05-16 04:03:58'),
('f6bb1f8bc990d527641109a824c0e4242793452d244e99694da8b912204ba073d98dead9a5591b9a', 35, 1, 'LaraPassport', '[]', 0, '2020-05-18 08:20:25', '2020-05-18 08:20:25', '2021-05-18 08:20:25'),
('f6f0850294447ad60eb730be7c14043c8d67eb6db3f482783f7843430001737faeb0f5e8faed436a', 224, 1, 'LaraPassport', '[]', 0, '2020-08-19 14:25:43', '2020-08-19 14:25:43', '2021-08-19 07:25:43'),
('f6f4f1dfefffa3f6668c31fc2c98700ef29be81b5e86100c82f3ecec94452cbdbc63a2229cd22ed9', 266, 1, 'LaraPassport', '[]', 0, '2020-10-15 20:09:14', '2020-10-15 20:09:14', '2021-10-15 13:09:14'),
('f72391c73f69ab614887deab5a70b09e40779c75ea7a6aed798a41c5d27e3597ea51665e4da4146d', 33, 1, 'LaraPassport', '[]', 0, '2020-04-13 09:04:20', '2020-04-13 09:04:20', '2021-04-13 09:04:20'),
('f770bdce2498878c948e4398cfd915a72e44c9ce0eec29ba41dd17ab0dcdcee5155d0f65f9f03dc2', 57, 1, 'LaraPassport', '[]', 0, '2020-08-18 12:58:02', '2020-08-18 12:58:02', '2021-08-18 05:58:02'),
('f7964e94b5bf875f88a27dfbfe6ad2cba4a44eeab07d4ba5d743584454fa212c2d63c79d7da5cce8', 57, 1, 'LaraPassport', '[]', 0, '2020-08-04 13:23:21', '2020-08-04 13:23:21', '2021-08-04 06:23:21'),
('f85764d712355c03860a32f40d3d61b1f5e39d31fe79e5a4285b72d65e3d030503a866e65d071da7', 112, 1, 'LaraPassport', '[]', 0, '2021-01-16 02:38:20', '2021-01-16 02:38:20', '2022-01-15 19:38:20'),
('f873f7a8008e6bc16cd6b9f9581ae463554fe9cc06ba77ab66b84a9d7b606c3c8947e7c4edcfb95f', 8, 1, 'LaraPassport', '[]', 0, '2020-04-09 13:20:31', '2020-04-09 13:20:31', '2021-04-09 13:20:31'),
('f907ab12cedc9fa0858253b1d0890fcf2b7e07a6152e77d91952e7885dd326e64e5e05fafa2fbd7b', 57, 1, 'LaraPassport', '[]', 0, '2020-08-14 19:26:35', '2020-08-14 19:26:35', '2021-08-14 12:26:35'),
('f964412a507051ca54414a1d06a3fb3d30c9d49cbc7de2b53997236885a64530c538ae2d8df3980c', 33, 1, 'LaraPassport', '[]', 0, '2020-04-16 14:45:01', '2020-04-16 14:45:01', '2021-04-16 14:45:01'),
('f96f43a879dc50697370d5af4c54a7cbaaeb89e471980b0c0b0c184a01db3636bd716cb00b966cac', 62, 1, 'LaraPassport', '[]', 0, '2020-07-27 22:22:08', '2020-07-27 22:22:08', '2021-07-27 15:22:08'),
('f986dec8bf800ee039aa6ca4cbc0d02aae73f2c4a96481b164420c90d2cceada350fb4690c2fb0a1', 33, 1, 'LaraPassport', '[]', 0, '2020-04-13 08:56:33', '2020-04-13 08:56:33', '2021-04-13 08:56:33'),
('f993391389e204753d58419f99d55683bc01c1574c63da6da6ada16e4a92cab93f59757aa65678f3', 218, 1, 'LaraPassport', '[]', 0, '2020-08-18 21:01:59', '2020-08-18 21:01:59', '2021-08-18 14:01:59'),
('f9b18028fb29c80f424c35effd1becea1dcb916ff1c44f282c71949055e0a4b711ff94cb4de902c6', 311, 1, 'LaraPassport', '[]', 0, '2020-09-19 23:58:47', '2020-09-19 23:58:47', '2021-09-19 16:58:47'),
('fa2cbb3ae1c10d6481a87737d0f456771bf2eeda5540691a61f9a45002a998034f5836eee0074833', 309, 1, 'LaraPassport', '[]', 0, '2020-09-23 01:18:36', '2020-09-23 01:18:36', '2021-09-22 18:18:36'),
('fa40d3fe691fa609020f256262d067649a5fb02b1285db42b2c9d16f35942ad0d4b6f867a3ee60b9', 300, 1, 'LaraPassport', '[]', 0, '2020-10-16 20:48:03', '2020-10-16 20:48:03', '2021-10-16 13:48:03'),
('fa9d254ac4ff2954b5399ca697ca110f9c94d2e8b1da770150aed67f70b94b8da8f6c1629ce9e1cd', 35, 1, 'LaraPassport', '[]', 0, '2020-06-29 06:07:39', '2020-06-29 06:07:39', '2021-06-29 06:07:39'),
('fb4bf4c049f85d522f903df1e5e298ce44e4c711f3d70f8443bcbc1a44d3dd7c76ecf8ee5e185058', 244, 1, 'LaraPassport', '[]', 0, '2020-08-27 15:32:54', '2020-08-27 15:32:54', '2021-08-27 08:32:54'),
('fb50faff9fc950567cfb677d43ec3f77552a31101be070091a349542a2ae7fbf14f8de4a9387f17b', 249, 1, 'LaraPassport', '[]', 0, '2020-09-30 17:50:23', '2020-09-30 17:50:23', '2021-09-30 10:50:23'),
('fb894ff34d9ba2fb7e100e658f77561bb7142fd7b9530d406e8ae4bb70552a0a04c77dde64db9fa5', 8, 1, 'LaraPassport', '[]', 0, '2020-04-09 09:00:16', '2020-04-09 09:00:16', '2021-04-09 09:00:16'),
('fbae71a3e889e6b8c805fe46f0de6b601ff34e2d511b6676ee658f7557733e7d504c441348738ae0', 195, 1, 'LaraPassport', '[]', 0, '2020-08-17 01:02:31', '2020-08-17 01:02:31', '2021-08-16 18:02:31'),
('fc368da8554d87c69105f2d06427463279c40a567b21bc582d76da83980fbc678410a0ffb09f9fe6', 237, 1, 'LaraPassport', '[]', 0, '2020-08-25 11:18:12', '2020-08-25 11:18:12', '2021-08-25 04:18:12'),
('fc4c81c076ec2808f8789f7b1a7a88afcf8916090348a26b3ba9522bfe493e6d7fb65952d59d2e4d', 337, 1, 'LaraPassport', '[]', 0, '2020-10-02 11:53:51', '2020-10-02 11:53:51', '2021-10-02 04:53:51'),
('fc4d0365cf1d38e323427a9aa6e36d42cf3f811cfbe5f22e7d772ddc5b46e1772a890a76830a4bf6', 62, 1, 'LaraPassport', '[]', 0, '2020-06-13 08:25:42', '2020-06-13 08:25:42', '2021-06-13 08:25:42'),
('fc5c592857b659804de90d07405d3540ddeca661c4dd15d2b2895ede99872a30ece7fa1855995a39', 274, 1, 'LaraPassport', '[]', 0, '2020-09-08 13:10:39', '2020-09-08 13:10:39', '2021-09-08 06:10:39'),
('fc6c89a2ef2eef5d4ba25a2d576466091e5d4332e50871b3b416308473b41498a1c3531e2a1fee68', 300, 1, 'LaraPassport', '[]', 0, '2020-09-30 19:23:46', '2020-09-30 19:23:46', '2021-09-30 12:23:46'),
('fc8ffe10b736641de0377a7f7086280b1ea0134e5d17347efca32731bfeaefa9f62652a38f8f53c9', 33, 1, 'LaraPassport', '[]', 0, '2020-04-13 05:42:20', '2020-04-13 05:42:20', '2021-04-13 05:42:20'),
('fcb557b68d97011e53dd0d27f8c52824d7b512fe213e3dbde5512c710961a711517053390805119e', 109, 1, 'LaraPassport', '[]', 0, '2020-08-18 11:30:52', '2020-08-18 11:30:52', '2021-08-18 04:30:52'),
('fcb7c75b50d3bca73956467fc143273cd8216b082dc1069edc114e0ea7754c122ed43b3a9d410f99', 35, 1, 'LaraPassport', '[]', 0, '2020-08-08 13:08:16', '2020-08-08 13:08:16', '2021-08-08 06:08:16'),
('fcc68cd65293302050820a0f947f0bf45120e2c2e33239d482586c09201f7c801065667a8bb25faa', 434, 1, 'LaraPassport', '[]', 0, '2020-12-23 18:47:23', '2020-12-23 18:47:23', '2021-12-23 11:47:23'),
('fdb3d70f1323a29dba2ccf38106b528441763f76eb202e55511490649ee5ac754c8a6aa1d3cbda1b', 383, 1, 'LaraPassport', '[]', 0, '2020-11-06 22:19:24', '2020-11-06 22:19:24', '2021-11-06 15:19:24'),
('fdb4d21b46c18b625e539c9bdcfaff1552334d5ae50c1162723f5336aa7e1e836308c9ca35025ac9', 328, 1, 'LaraPassport', '[]', 0, '2020-09-30 20:45:02', '2020-09-30 20:45:02', '2021-09-30 13:45:02'),
('fdc79499f13ae3a99272afc5342e53dd9ad3948dfd90e2cf93189a933d20ec8fbeb743364f6e1f86', 194, 1, 'LaraPassport', '[]', 0, '2020-08-16 14:35:20', '2020-08-16 14:35:20', '2021-08-16 07:35:20'),
('fdcff6cf295bf29ddc9e290ad3047c0fe02014d7f9cda3a6bde21a5cd08d06f31521df58d2dec84e', 1, 1, 'LaraPassport', '[]', 0, '2020-08-20 12:15:03', '2020-08-20 12:15:03', '2021-08-20 05:15:03'),
('fdd5e37b27cad5bbce29bb64ebcb71ad583212427f448badff68cc5d65bf969a13ac2501b29183c6', 195, 1, 'LaraPassport', '[]', 0, '2020-09-21 03:48:20', '2020-09-21 03:48:20', '2021-09-20 20:48:20'),
('fe082e0b2bde6b1c0210dae2ce3886c1625b73913fc8096a0965040f6c1bdf5b7359e0d91144eb5b', 35, 1, 'LaraPassport', '[]', 0, '2020-07-31 16:31:59', '2020-07-31 16:31:59', '2021-07-31 09:31:59'),
('fe1e87d2a8b431786333cdf4b5412db9df33bd3dc1c3cf23746901b4d484397fcd2eca120012cf9a', 309, 1, 'LaraPassport', '[]', 0, '2020-10-16 21:47:02', '2020-10-16 21:47:02', '2021-10-16 14:47:02'),
('fe378bcfdd54aeb83f67852c95e105d5cf50363e6deccd582912e22cddf2ba2974ce598fe6c072b7', 57, 1, 'LaraPassport', '[]', 0, '2020-06-21 12:18:44', '2020-06-21 12:18:44', '2021-06-21 12:18:44'),
('fe40143e57f184414fdd66318510456021f388f5b528433662b67759d319c1006af86572382f43ce', 237, 1, 'LaraPassport', '[]', 0, '2020-10-13 16:50:48', '2020-10-13 16:50:48', '2021-10-13 09:50:48'),
('fe46f74e49cabd82ce8922bd82f3fafc8c24a2958b9632a9c2210021b4a3260086cf665a2aa00ef4', 5, 1, 'LaraPassport', '[]', 0, '2021-01-12 17:12:47', '2021-01-12 17:12:47', '2022-01-12 10:12:47'),
('fe9d93d82f12ebd14e718630c2c1db77af8c82c8190817844ebd521d39a3fc3bfa76400c3e8b7c44', 33, 1, 'LaraPassport', '[]', 0, '2020-04-28 12:55:21', '2020-04-28 12:55:21', '2021-04-28 12:55:21'),
('fecf390d23d1b03d079462a5a7566de6214f5b8dc587a04e583c1dc7d733ecffd1aa0cfc5bd4ec72', 104, 1, 'LaraPassport', '[]', 0, '2020-06-11 12:53:50', '2020-06-11 12:53:50', '2021-06-11 12:53:50'),
('fed033f836acd3443e51d8f3e7a36b1cd25b39345d212606c1d4b0115204cea5ed3146374ffa9f44', 33, 1, 'LaraPassport', '[]', 0, '2020-04-18 08:42:24', '2020-04-18 08:42:24', '2021-04-18 08:42:24'),
('ff63c7ae732f184428a2324f7424a8f651fe69c701eba5c876252c890381bb67f9a00628a3fb01be', 373, 1, 'LaraPassport', '[]', 0, '2020-10-28 19:14:07', '2020-10-28 19:14:07', '2021-10-28 12:14:07'),
('ff75a06c5e14cfe9236b0b690d1ffff051042dc3dd49f78f262684c54e99a13083f2f48c85ddc796', 249, 1, 'LaraPassport', '[]', 0, '2020-10-06 19:16:02', '2020-10-06 19:16:02', '2021-10-06 12:16:02'),
('ff9a4bf3ad075d521bbcf14e02d199f3a125c5f740faf74a9365353535b5a726b8f7480ecfd21e65', 33, 1, 'LaraPassport', '[]', 0, '2020-04-16 14:05:03', '2020-04-16 14:05:03', '2021-04-16 14:05:03'),
('ffa99fa2dbd96e9375173b3bcb30dcbc288bca68502ecb80d184cebb6b7c6513e96770da3672fb46', 35, 1, 'LaraPassport', '[]', 0, '2020-06-27 11:03:46', '2020-06-27 11:03:46', '2021-06-27 11:03:46'),
('ffa9c945b1ed6cf901a8cf28e0796271284369a79fa620542040eef79e73ed20c26ffe44696713a2', 35, 1, 'LaraPassport', '[]', 0, '2020-05-30 11:45:05', '2020-05-30 11:45:05', '2021-05-30 11:45:05'),
('ffac3a1038dc041495a4aac16c1eb89195afefc9288ff809e78d014d6eed4df6e8a8c55b793ddd9c', 33, 1, 'LaraPassport', '[]', 0, '2020-04-11 08:00:35', '2020-04-11 08:00:35', '2021-04-11 08:00:35'),
('ffc3a74d8071fbd2295706b345718d9810f03da0fe252e26a7ec46da691428626c2f2deabbb94713', 238, 1, 'LaraPassport', '[]', 0, '2020-09-09 23:24:10', '2020-09-09 23:24:10', '2021-09-09 16:24:10'),
('fffee16ee67f6e2cff83603bf36bac688869c007ac0493c7e8a51c8c7985cd3339f85827e59f1f58', 104, 1, 'LaraPassport', '[]', 0, '2020-06-11 12:54:11', '2020-06-11 12:54:11', '2021-06-11 12:54:11');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Parking Personal Access Client', '3meo4XioFgqLtDxQkXz8tO5rG1F1trOaLU56cn9I', 'http://localhost', 1, 0, 0, '2020-03-16 00:29:13', '2020-03-16 00:29:13'),
(2, NULL, 'Parking Password Grant Client', 'quhFGokLiQPDKXZwTcy7AVzt0YDypFo5bpBugVuf', 'http://localhost', 0, 1, 0, '2020-03-16 00:29:13', '2020-03-16 00:29:13');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2020-03-16 00:29:13', '2020-03-16 00:29:13');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `offer`
--

CREATE TABLE `offer` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `discount` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `delivery_status` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `offer`
--

INSERT INTO `offer` (`id`, `title`, `discount`, `delivery_status`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'free', '5%', 1, NULL, 1, 1, '2020-05-28 12:20:51', '2020-05-28 12:20:51', NULL),
(2, '2000', 'jdgfujnmdbf7gyhdbfyudrtgfkutf87eudg76eyh', 0, NULL, 1, 1, '2020-06-08 09:28:47', '2020-09-02 14:00:33', '2020-09-02 14:00:33'),
(3, '456', '67', 1, NULL, 1, 1, '2020-06-22 07:45:23', '2020-06-22 07:45:23', NULL),
(4, 'hasp', '100', 0, NULL, 1, 1, '2020-09-02 14:01:16', '2020-09-02 15:58:41', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `Order_360`
--

CREATE TABLE `Order_360` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `patient_id` int(11) DEFAULT NULL,
  `doctor_id` int(11) DEFAULT NULL,
  `book_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  `order_status` int(11) DEFAULT '0' COMMENT '1 for order complate',
  `haspatal_prescription` int(11) DEFAULT '1',
  `prescription_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `Order_360`
--

INSERT INTO `Order_360` (`id`, `user_id`, `patient_id`, `doctor_id`, `book_id`, `role_id`, `order_status`, `haspatal_prescription`, `prescription_image`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 109, 19, 1, 'book-0000022', 13, 0, 1, NULL, 57, 57, '2020-06-13 13:00:08', '2020-06-13 13:00:08', NULL),
(2, 109, 19, 1, 'book-0000020', 13, 1, 1, NULL, 57, 57, '2020-06-22 09:29:15', '2020-06-22 09:29:15', NULL),
(3, 115, 19, 1, 'book-000004', 11, 1, 1, NULL, 57, 57, '2020-06-22 11:31:00', '2020-06-22 11:31:00', NULL),
(4, 109, 19, 1, 'book-0000017', 13, 1, 1, NULL, 57, 57, '2020-08-01 17:24:17', '2020-08-01 17:24:17', NULL),
(5, 109, 19, 1, 'book-0000027', 13, 1, 1, NULL, 57, 57, '2020-08-04 13:15:11', '2020-08-04 13:15:11', NULL),
(6, 115, 221, 136, '91-B-000000114', 11, 0, 1, NULL, 316, 316, '2020-10-20 12:40:18', '2020-10-20 12:40:18', NULL),
(7, 115, 221, 136, '91-B-000000115', 11, 0, 1, NULL, 316, 316, '2020-10-20 12:40:38', '2020-10-20 12:40:38', NULL),
(8, 233, 221, 136, '91-B-000000116', 11, 0, 1, NULL, 316, 316, '2020-10-20 12:40:45', '2020-10-20 12:40:45', NULL),
(9, 115, 201, 144, 'book-0000064', 11, 0, 1, NULL, 244, 244, '2020-10-24 19:21:15', '2020-10-24 19:21:15', NULL),
(10, 233, 196, 142, 'book-0000089', 11, 0, 1, NULL, 237, 237, '2020-10-25 12:21:12', '2020-10-25 12:21:12', NULL),
(11, 0, 191, 114, '91-B-000000136', 11, 0, 1, NULL, 231, 231, '2020-10-26 18:29:12', '2020-10-26 18:29:12', NULL),
(13, 371, 191, 114, '91-B-000000150', 11, 2, 1, NULL, 231, 231, '2020-10-27 13:27:27', '2020-10-27 13:27:27', NULL),
(14, 371, 191, NULL, NULL, 11, 0, 2, '1603877742__1.jpg', 266, 266, '2020-10-28 16:35:42', '2020-10-28 16:35:42', NULL),
(15, 233, 196, NULL, NULL, 11, 2, 2, '1603904081__Image-5365.jpg', 237, 237, '2020-10-28 23:54:41', '2020-10-28 23:54:41', NULL),
(16, 233, 196, NULL, NULL, 11, 2, 2, '1603904934__Image-3787.jpg', 237, 237, '2020-10-29 00:08:54', '2020-10-29 00:08:54', NULL),
(17, 233, 196, NULL, NULL, 11, 2, 2, '1603905117__Image-7260.jpg', 237, 237, '2020-10-29 00:11:57', '2020-10-29 00:11:57', NULL),
(18, 233, 196, NULL, NULL, 11, 2, 2, '1603905217__Image-9043.jpg', 237, 237, '2020-10-29 00:13:37', '2020-10-29 00:13:37', NULL),
(19, 368, 216, NULL, NULL, 11, 2, 2, '1603975250__IMG-20201029-WA0003.jpg', 305, 305, '2020-10-29 19:40:51', '2020-10-29 19:40:51', NULL),
(20, 376, 221, NULL, NULL, 11, 0, 2, '1604059354__IMG_20200902_194824.jpg', 316, 316, '2020-10-30 19:02:35', '2020-10-30 19:02:35', NULL),
(21, 368, 216, NULL, NULL, 11, 0, 2, '1604281782__Screenshot_20201028-172119.jpg', 305, 305, '2020-11-02 08:49:42', '2020-11-02 08:49:42', NULL),
(22, 368, 216, NULL, NULL, 11, 0, 2, '1604281897__Screenshot_20201028-172119.jpg', 305, 305, '2020-11-02 08:51:37', '2020-11-02 08:51:37', NULL),
(23, 368, 216, NULL, NULL, 11, 0, 2, '1604302524__Screenshot_20201031-090943.jpg', 305, 305, '2020-11-02 20:05:24', '2020-11-02 20:05:24', NULL),
(24, 233, 201, NULL, NULL, 11, 0, 2, '1604549616__SAVE_20201013_195149.jpg', 244, 244, '2020-11-05 16:43:36', '2020-11-05 16:43:36', NULL),
(25, 368, 254, NULL, NULL, 11, 0, 2, '1604579427__IMG_20201031_163135.jpg', 373, 373, '2020-11-06 01:00:30', '2020-11-06 01:00:30', NULL),
(26, 368, 254, NULL, NULL, 11, 0, 2, '1604746443__Image-1430.jpg', 373, 373, '2020-11-07 23:24:03', '2020-11-07 23:24:03', NULL),
(27, 115, 254, 160, '91-B-000000163', 11, 0, 1, NULL, 373, 373, '2020-11-08 00:55:51', '2020-11-08 00:55:51', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `order_query`
--

CREATE TABLE `order_query` (
  `id` int(10) UNSIGNED NOT NULL,
  `book_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `patient_id` int(11) DEFAULT NULL,
  `doctor_id` int(11) DEFAULT NULL,
  `query` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_query`
--

INSERT INTO `order_query` (`id`, `book_id`, `patient_id`, `doctor_id`, `query`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '1', 1, 1, 'test data', 35, 35, '2020-06-13 06:05:30', '2020-06-13 06:05:30', NULL),
(2, 'book-0000021', 19, 1, 'test question query', 108, 108, '2020-06-13 06:10:10', '2020-06-13 06:10:10', NULL),
(3, 'book-0000021', 19, 1, 'test1', 108, 108, '2020-06-13 06:11:14', '2020-06-13 06:11:14', NULL),
(4, 'book-0000021', 19, 1, 'test2', 108, 108, '2020-06-13 06:12:14', '2020-06-13 06:12:14', NULL),
(5, 'book-000004', 19, 1, 'Flgiyffigcirs gditksalhhcit gadihvglajffxwypuc vcitdhvrztk gxdkxbh', 115, 115, '2020-06-22 13:11:12', '2020-06-22 13:11:12', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('akash@aelius.in', '$2y$10$PP1xW3/tQedAjjL6y7cdNupxrsJvuTIluHVF3RtsmLGLKuPXztLk.', '2020-08-01 18:04:40'),
('amit@aelius.in', '$2y$10$.OkAlALJPhZtWAWBIgehqeOpnSz5AFBHU.EUgQ2j2JAD32dCEn62.', '2020-08-17 17:04:00'),
('jaydeep@aelius.in', '$2y$10$iYzoODwiwwWR.ux433eKzupCvQI0Ut21t7FkqCermERrOR5IDRWAe', '2020-08-18 12:54:40'),
('siddhi@aelius.in', '$2y$10$pWHrGEp/QTd7n3vmuI1Bxu3X8MBDLZjZCsuRRqHoA8uq7TnN0cPm.', '2020-08-21 14:27:42'),
('cdavneet@gmail.com', '$2y$10$TRKEothBfN5e.9LteBu4SOxvpw4OGGb7aK6/HpeQk2EccmAF3sYIW', '2020-09-16 23:04:01'),
('thesadafkhan@gmail.com', '$2y$10$IyDThO0o0BpeYpP9FzxY1u9PlPc1tQVU9ioAv0oAfEhOqy64eYez.', '2020-10-14 14:15:13'),
('ashish@haspatal.com', '$2y$10$4FOSY/pUcceoi.DzbVxL7e8hI1mUgvbspE.JDeMaVEJMcJdxwRBZi', '2020-10-24 15:19:34'),
('arjun@haspatal.com', '$2y$10$j3qLhBoEgWAHbbEWRNxKo.B7aYCMyHXlFOKewzlVOi/09aDiuHJ4a', '2020-11-05 16:41:38'),
('vishaltolani0001@gmail.com', '$2y$10$pLPpQlp7i8rhmEs44ADMcOi9.F2fmb1dOg95KFFExEnUTE4zDD7Gy', '2020-11-07 20:14:52'),
('sandeepraj9595@gmail.com', '$2y$10$9MRDIrT2vMWKcSHuV2dZtOg31EKaIozstDhg/UxfxyqyKNan5f0X.', '2021-01-05 05:25:00'),
('aapnavneet@gmail.com', '$2y$10$MEqVUOogbT.25W7OWfzXneVfKHJqetbrlOBdbSyD2kkGEbgwZyAxy', '2021-01-07 21:43:37'),
('ashikmannorayil@gmail.com', '$2y$10$DjELOYTIN5uOnYKmD8tpmuAlV8l1Gwvb6upR3lpeny.liAVcDD8Qq', '2021-01-10 19:42:16'),
('althafthajudeen@gmail.com', '$2y$10$kLeIGoghgqKvpqQI7onR5e.tegXKYDT3Zwdp0Ai9HkXi0rG2dbC1C', '2021-01-14 23:58:36');

-- --------------------------------------------------------

--
-- Table structure for table `patient_add_fund`
--

CREATE TABLE `patient_add_fund` (
  `id` int(10) UNSIGNED NOT NULL,
  `patient_id` int(11) DEFAULT NULL,
  `payment_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `requested_amount` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `wa_amount` int(11) DEFAULT NULL,
  `doctor_id` int(11) DEFAULT NULL,
  `dr_razorpay_payment_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dr_amount` int(11) DEFAULT NULL,
  `dr_w_amount` int(11) DEFAULT NULL,
  `balance_after_approve` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `approved_balance` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `transaction_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `transaction_proof` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `transaction_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '1 for approved 2 for reject',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `patient_add_fund`
--

INSERT INTO `patient_add_fund` (`id`, `patient_id`, `payment_id`, `requested_amount`, `wa_amount`, `doctor_id`, `dr_razorpay_payment_id`, `dr_amount`, `dr_w_amount`, `balance_after_approve`, `approved_balance`, `transaction_type`, `transaction_proof`, `transaction_date`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 19, 'pay_El6GyIZkLc5kDY', '2800', 14394, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 57, 57, '2020-05-01 12:54:53', '2020-05-01 12:54:53', NULL),
(2, 201, 'pay_ElVkRjWC2kg9Ah', '6000', 15010, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 59, 59, '2020-05-02 13:50:08', '2020-05-02 13:50:08', NULL),
(4, 26, 'pay_EvLdIaWatRbTwB', '600', 1100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 62, 62, '2020-05-27 10:26:26', '2020-05-27 10:26:26', NULL),
(6, 196, 'pay_FdIjRP62Nbcek4', '100', 1757, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 237, 237, '2020-09-15 19:12:16', '2020-09-15 19:12:16', NULL),
(7, NULL, 'pay_FsL7OQP9UsTzlN', NULL, NULL, 114, NULL, 760, NULL, NULL, NULL, NULL, NULL, NULL, 1, 266, 266, '2020-10-01 16:53:18', '2020-10-01 16:53:18', NULL),
(8, 191, 'pay_FjuppUd0vfhJsK', '100', 452, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 231, 231, '2020-10-02 12:22:41', '2020-10-02 12:22:41', NULL),
(9, 62, NULL, NULL, 50, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, NULL, NULL, NULL),
(10, 56, NULL, NULL, 150, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, NULL, NULL, NULL),
(11, 197, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, NULL, NULL, NULL),
(12, 202, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, NULL, NULL, NULL),
(13, 216, NULL, NULL, 180, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, NULL, NULL, NULL),
(14, 221, NULL, NULL, 22, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, NULL, NULL, NULL),
(15, 204, NULL, NULL, 12, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, NULL, NULL, NULL),
(16, 208, NULL, NULL, 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, NULL, NULL, NULL),
(17, 229, NULL, NULL, 18, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, NULL, NULL, NULL),
(18, NULL, NULL, NULL, NULL, 13, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 355, 355, '2020-10-19 14:40:18', '2020-10-19 14:40:18', NULL),
(19, NULL, NULL, NULL, NULL, 157, NULL, 10500, NULL, NULL, NULL, NULL, NULL, NULL, 1, 356, 356, '2020-10-23 17:38:48', '2020-10-23 17:38:48', NULL),
(20, 254, NULL, NULL, 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, NULL, NULL, NULL),
(21, 260, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `patient_details`
--

CREATE TABLE `patient_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `p_uni_id` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `age` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '1 for male 2 for female 3 for other',
  `height` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `weight` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lmp` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zipcode` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `personal_profile` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `family_profile` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `social_profile` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `work_profile` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_phone1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_phone2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_phone3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `language` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `profile_pic` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` smallint(6) NOT NULL DEFAULT '1' COMMENT '0 for inactive,1 for active',
  `created_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `patient_details`
--

INSERT INTO `patient_details` (`id`, `p_uni_id`, `first_name`, `last_name`, `age`, `gender`, `height`, `weight`, `lmp`, `email`, `mobile`, `country`, `state`, `city`, `zipcode`, `address`, `personal_profile`, `family_profile`, `social_profile`, `work_profile`, `contact_phone1`, `contact_phone2`, `contact_phone3`, `email1`, `email2`, `language`, `profile_pic`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(2, NULL, 'ashish', 'nimbark', NULL, NULL, NULL, NULL, '', 'ashishnimbark@gmail.com', '1234567890', '2', '1', '2', '360001', 'rajkot', 'pp', 'pp', 'pp', 'pp', '55666666666', '445454554', '4554544545', 'ashishnimbark1@gmail.com', 'ashishnimbark1@gmail.com', '1', '1586504256__index.png', 1, '1', '1', '2020-04-10 07:37:36', '2020-04-10 07:37:36'),
(24, NULL, 'ish midha', NULL, NULL, NULL, NULL, NULL, NULL, 'midhaik@gmail.com', '8447313733', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-04-25 16:07:51', '2020-04-25 16:07:51'),
(26, NULL, 'robot', NULL, NULL, NULL, NULL, NULL, NULL, 'robot@chp.md', '8888888888', '3', '3', '2', 'fgghhj', 'mg road', 'I am a male, 55 years old, married business man living in Delhi. My weight is 85 kg and height is 5.4 ft. my vision is ok.', 'i have 3 children, aged 28, 26 and 21 years.  My family has a history of diabetes and parkunson. father passed away at age of 73 and Mom is doing well. she has arthritis.', 'i do not smoke, do not drink alcohol. i have many good friends. my hobby is listening to misic', 'i run a healthcare company with a team of 7 employees. We provide serv ices of different kinds to help clinics and hospitals run smoothly', '9599661700', '9599661700', '9599661700', 'robert@chp.md', 'robot@chp.md', NULL, '1590576399__IMG-20200527-WA0016.jpg', 1, NULL, NULL, '2020-04-27 06:42:31', '2020-06-13 08:53:12'),
(27, NULL, 'robot1', NULL, NULL, NULL, NULL, NULL, NULL, 'robot1@chp.md', '8888888888', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-04-27 06:44:47', '2020-04-27 06:44:47'),
(43, NULL, 'Tarun Kantrar', NULL, NULL, NULL, NULL, NULL, NULL, 'deja.vudu26@gmail.com', '9897228260', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-05-22 14:22:46', '2020-05-22 14:22:46'),
(61, NULL, 'rameez', NULL, NULL, NULL, NULL, NULL, NULL, 'test@yopmail.com', '9532391092', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-07-03 18:17:04', '2020-07-03 18:17:04'),
(70, NULL, 'johdoe', NULL, NULL, NULL, NULL, NULL, NULL, 'playstorecnx06@gmail.com', '6364908612', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-08-16 14:35:07', '2020-08-16 14:35:07'),
(71, NULL, 'amit', NULL, NULL, NULL, NULL, NULL, NULL, 'akumarmd@gmail.com', '9136694176', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-08-17 00:26:22', '2020-08-17 00:26:22'),
(100, NULL, 'raju', NULL, NULL, NULL, NULL, NULL, NULL, 'raju@gmail.com', '9638713258', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-08-17 21:38:08', '2020-08-17 21:38:08'),
(102, NULL, 'mohsin', NULL, NULL, NULL, NULL, NULL, NULL, 'mohsinsajjad853@gmail.com', '7513200000', '3', '1', '2', '64000', 'Indian Ahmeda bad', 'I\'m mohsin sajjad', NULL, NULL, NULL, '7513200000', '7513200000', '7513200000', 'admin@gmail.com', 'admin@gmail.com', NULL, NULL, 1, NULL, NULL, '2020-08-18 01:58:02', '2020-08-18 02:06:23'),
(191, NULL, 'jd test', NULL, '2020-10-23', 'Male', '6', '60', NULL, 'jaydeepamethiya@gmail.com', '9106909336', '3', '1', '659', '110005', 'demo', NULL, NULL, NULL, NULL, '7777777777', '9784563120', '9876543210', 'a@a.com', 'b@b.com', NULL, NULL, 1, NULL, NULL, '2020-08-19 20:33:00', '2020-10-24 23:47:17'),
(194, NULL, 'PG Sitapara', NULL, NULL, NULL, NULL, NULL, NULL, 'sitapara9794@gmail.com', '9427141335', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-08-22 22:53:59', '2020-08-22 22:53:59'),
(196, NULL, 'siddhi', NULL, '1995-03-02', 'Male', '5\'0', '45', NULL, 'siddhi@aelius.in', '8238576669', '3', '1', '829', '360004', 'rajkot', NULL, NULL, NULL, NULL, '8238576699', NULL, NULL, 'der@der.com', 'vachhanisiddhi@gmail.com', NULL, '1603732985__shape_09_01_34.png', 1, NULL, NULL, '2020-08-24 12:18:33', '2020-10-27 00:23:05'),
(200, NULL, 'absal', NULL, NULL, NULL, NULL, NULL, NULL, 'mastores01@gmail.com', '9633631592', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-08-27 15:15:12', '2020-08-27 15:15:12'),
(201, NULL, 'Arjun Marhur', NULL, '2017-11-15', 'Male', '55', '56', NULL, 'arjun@haspatal.com', '9974573522', '3', '1', '659', '380058', 'kabir road', 'my name is mohsin', NULL, NULL, NULL, '8866252555', '8866252555', '8866252555', 'ashishnimbark@gmail.com', 'ashishnimbark@gmail.com', NULL, '1599216330__SAVE_20200111_103701.jpg', 1, NULL, NULL, '2020-08-27 15:32:30', '2020-10-24 20:09:41'),
(203, NULL, 'harsh bhandari', NULL, NULL, NULL, NULL, NULL, NULL, 'harshcorp28@gmail.com', '+917982150690', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-08-28 14:59:03', '2020-08-28 14:59:03'),
(204, NULL, 'Rajin Rahiman', NULL, NULL, NULL, NULL, NULL, NULL, 'raj.n20s@gmail.com', '8592989859', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-09-01 19:52:02', '2020-09-01 19:52:02'),
(205, NULL, 'Dhruv Nimbark', NULL, NULL, NULL, NULL, NULL, NULL, 'dhruvnimbark@gmail.com', '7623849712', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-09-05 23:01:42', '2020-09-05 23:01:42'),
(206, NULL, 'Ghanshyam', NULL, NULL, NULL, NULL, NULL, NULL, 'ghanshyam@haspatal.com', '9825700078', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-09-07 21:23:57', '2020-09-07 21:23:57'),
(207, NULL, 'patient', NULL, NULL, NULL, NULL, NULL, NULL, 'p@p.com', '9016509583', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-09-08 15:36:56', '2020-09-08 15:36:56'),
(208, NULL, 'Navneet Kumar', NULL, NULL, NULL, NULL, NULL, NULL, 'aapnavneet@gmail.com', '9818158289', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-09-09 03:55:24', '2020-09-09 03:55:24'),
(214, NULL, 'a', NULL, NULL, NULL, NULL, NULL, NULL, 'a@a.com', '1234567890', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-09-11 14:39:55', '2020-09-11 14:39:55'),
(215, NULL, 'b', NULL, NULL, NULL, NULL, NULL, NULL, 'b@b.com', '1284628618', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-09-11 14:39:55', '2020-09-11 14:39:55'),
(216, NULL, 'Shiva Singh', NULL, '1968-12-29', 'Male', '160', '85', NULL, 'cdavneet@gmail.com', '9818158932', '3', '5', '35', '110005', 'ghgv', NULL, 'cgcc', 'xfgc', 'cvvv', '8866252522', '9599661701', '9718215625', 'aapnavneet@gmail.com', 'cdnavneet@gmail.com', NULL, '1603300594__20201021_224610.jpg', 1, NULL, NULL, '2020-09-15 06:04:09', '2020-10-23 01:15:46'),
(217, NULL, 'NK', NULL, NULL, NULL, NULL, NULL, NULL, 'cdx@gmail.com', '97182156567', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-09-19 23:58:46', '2020-09-19 23:58:46'),
(218, NULL, 'Anil kumar Behera', NULL, NULL, NULL, NULL, NULL, NULL, 'anil.ajvpb@gmail.com', '8249556573', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-09-21 15:04:38', '2020-09-21 15:04:38'),
(219, NULL, 'mohsin', NULL, NULL, NULL, NULL, NULL, NULL, 'delete@gmail.com', '9599661700', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-09-22 13:05:52', '2020-09-22 13:05:52'),
(220, NULL, 'vinay yadav', NULL, NULL, NULL, NULL, NULL, NULL, 'vy7496775@gmail.com', '8108541056', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-09-23 11:37:54', '2020-09-23 11:37:54'),
(221, NULL, 'absal', NULL, '2004-01-06', 'Male', '180', '95', NULL, 'm.a.absal@gmail.com', '7994646416', '3', '3', '1664', '673008', 'medical college', NULL, NULL, NULL, NULL, '9746043286', '9742916416', '9633631592', 'm.a.absal@gmail.com', 'mastores01@gmail.com', NULL, '1603610913__PSZV0298.jpg', 1, NULL, NULL, '2020-09-23 16:45:27', '2021-01-07 01:35:54'),
(222, NULL, 'Rohan rai', NULL, NULL, NULL, NULL, NULL, NULL, 'rrai57536@gmail.com', '9667519302', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-09-25 18:27:34', '2020-09-25 18:27:34'),
(223, NULL, 'Vinay Singh', NULL, NULL, NULL, NULL, NULL, NULL, 'aapvinay@gmail.com', '8375910558', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-09-26 18:11:19', '2020-09-26 18:11:19'),
(224, NULL, 'reshma prasad', NULL, NULL, NULL, NULL, NULL, NULL, 'amhserreshma.1.v@gmail.com', '9495616804', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-09-28 14:27:28', '2020-09-28 14:27:28'),
(225, NULL, 'Rajveer Singh Solanki', NULL, NULL, NULL, NULL, NULL, NULL, 'rajveersinghsolanki17@gmail.com', '8769857656', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-09-28 15:05:47', '2020-09-28 15:05:47'),
(226, NULL, 'Vishal Laroia', NULL, NULL, NULL, NULL, NULL, NULL, 'vishalklaroia@gmail.com', '9873990994', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-09-30 16:40:25', '2020-09-30 16:40:25'),
(228, NULL, 'ashik mannorayil', NULL, NULL, NULL, NULL, NULL, NULL, 'ashikmannorayil@gmail.com', '+919742916416', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-09-30 18:54:58', '2020-09-30 18:54:58'),
(229, NULL, 'Vishal Laroia', NULL, NULL, NULL, NULL, NULL, NULL, 'vishal@aarogyaworld.com', '7373300009', NULL, NULL, NULL, NULL, NULL, 'male', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-09-30 19:01:59', '2020-09-30 19:03:54'),
(230, NULL, 'Esther peecee', NULL, NULL, NULL, NULL, NULL, NULL, 'vanlalhriatiesther@gmail.com', '8413875909', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-09-30 19:35:47', '2020-09-30 19:35:47'),
(232, NULL, 'sonu sharma', NULL, NULL, NULL, NULL, NULL, NULL, 'sonu@haspatal.com', '8866252555', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-10-01 13:27:28', '2020-10-01 13:27:28'),
(233, NULL, 'ritesh singh malik', NULL, NULL, NULL, NULL, NULL, NULL, 'riteshema@gmail.com', '8128389381', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-10-01 14:26:12', '2020-10-01 14:26:12'),
(234, NULL, 'avish adak', NULL, NULL, NULL, NULL, NULL, NULL, 'arnab4865@gmail.com', '6289810319', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-10-01 16:29:02', '2020-10-01 16:29:02'),
(235, NULL, 'thameema', NULL, NULL, NULL, NULL, NULL, NULL, 'thameemahasan@gmail.com', '9847125723', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-10-01 17:18:32', '2020-10-01 17:18:32'),
(236, NULL, 'Priya Arjun', NULL, NULL, NULL, NULL, NULL, NULL, 'Pri.Sur@gmail.com', '9380455470', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-10-01 20:36:51', '2020-10-01 20:36:51'),
(237, NULL, 'Sabitha yousef', NULL, NULL, NULL, NULL, NULL, NULL, 'sabithayousef@gmail.com', '9846208161', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-10-02 11:53:51', '2020-10-02 11:53:51'),
(238, NULL, 'Milan Shyamal', NULL, NULL, NULL, NULL, NULL, NULL, 'shyamalmilan07@gmail.com', '7407728008', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-10-02 19:57:23', '2020-10-02 19:57:23'),
(239, NULL, 'Althaf Ali', NULL, NULL, NULL, NULL, NULL, NULL, 'althafali138@gmail.com', '7510712012', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-10-05 17:52:23', '2020-10-05 17:52:23'),
(240, NULL, 'harman', NULL, NULL, NULL, NULL, NULL, NULL, 'harmandeep36195@gmail.com', '8727016032', '3', '35', '201', '144041', 'Jalandhar', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-10-06 18:00:27', '2020-10-06 18:01:50'),
(241, '91-P-0000001', 'Manoj Gotefode', NULL, NULL, NULL, NULL, NULL, NULL, 'gotefodemanoj@gmail.com', '8767333781', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-10-09 23:31:33', '2020-10-09 23:31:33'),
(242, '91-P-0000002', 'Shubham shakya', NULL, NULL, NULL, NULL, NULL, NULL, 'shakya9138@gmail.com', '9467433419', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-10-10 20:15:17', '2020-10-10 20:15:17'),
(243, '91-P-0000003', 'Kuravatti Basaveshwra', NULL, NULL, NULL, NULL, NULL, NULL, 'shashankshashank1030@gmail.com', '7676633025', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-10-10 22:03:51', '2020-10-10 22:03:51'),
(244, '91-P-0000004', 'patelharish', NULL, NULL, NULL, NULL, NULL, NULL, 'harishpatel471989@gmail.com', '9913499875', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-10-13 02:23:09', '2020-10-13 02:23:09'),
(245, '91-P-0000005', 'kiyara', NULL, '1995-09-20', 'female', '155', '68', NULL, 'thesadafkhan@gmail.com', '7838632687', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-10-14 13:31:18', '2020-11-09 19:04:38'),
(246, '91-P-0000006', 'ssk raj', NULL, NULL, NULL, NULL, NULL, NULL, 'mdaffan8434@gmail.com', '8434195397', NULL, NULL, NULL, NULL, NULL, 'ssk Raj', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-10-14 16:40:21', '2020-10-14 16:41:22'),
(247, '91-P-0000007', 'sagar', NULL, NULL, NULL, NULL, NULL, NULL, 'meetsagarn@gmail.com', '7756098265', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-10-15 13:46:40', '2020-10-15 13:46:40'),
(248, '91-P-0000008', 'rajan kumar', NULL, NULL, NULL, NULL, NULL, NULL, 'prajapatirajan110@gmail.com', '7509205693', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-10-15 16:35:01', '2020-10-15 16:35:01'),
(249, '91-P-0000009', 'himanshu prajapati', NULL, NULL, NULL, NULL, NULL, NULL, 'hpprajapati5719@gmail.com', '9302468221', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-10-15 16:36:26', '2020-10-15 16:36:26'),
(250, '91-P-00000010', 'ayush', NULL, NULL, NULL, NULL, NULL, NULL, 'ayushnayal3@gmail.com', '8445567394', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-10-18 08:36:32', '2020-10-18 08:36:32'),
(251, '91-P-00000011', 'balbir singh gill', NULL, NULL, NULL, NULL, NULL, NULL, 'rajgill1975@gmail.com', '9311669306', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '9650287283', '8287884723', '9311669306', 'bgill92@gmail.com', 'bgill913@gmail.com', NULL, '1603443676__balbir passport photo.jpg', 1, NULL, NULL, '2020-10-23 15:58:45', '2020-10-23 16:01:16'),
(252, '91-P-00000012', 'Arbaj Khan', NULL, NULL, NULL, NULL, NULL, NULL, 'arbajkhan91505@gmail.com', '8923632618', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-10-24 22:40:41', '2020-10-24 22:40:41'),
(254, '91-P-00000013', 'Vishal Tolani', NULL, '05-Jul-1993', 'Male', '5,11', '65', NULL, 'opmgr.haspatal@gmail.com', '9643725091', '3', '5', '65', '5555', 'fgfh', NULL, NULL, NULL, NULL, '9643725091', NULL, NULL, 'opmgr.haspatal@gmail.com', 'opmgr.haspatal@gmail.com', NULL, '1604578229__Khalifa_27.jpg', 1, NULL, NULL, '2020-10-28 19:14:07', '2020-11-06 00:46:39'),
(255, '91-P-00000014', 'riddhee', NULL, '2012-10-30', 'Male', '5', '53', NULL, 'riddhee@mailinator.com', '6352777416', '3', '1', '829', '360001', 'rajkot', NULL, NULL, NULL, NULL, '6352777416', '9409528616', '9409257175', 'riddhee@mailinator.com', 'riddhee1@mailinator.com', NULL, NULL, 1, NULL, NULL, '2020-10-30 17:09:40', '2020-10-30 17:18:40'),
(256, '91-P-00000015', 'Rajan Arora', NULL, '2032-10-29', 'Male', '5.6', '155', NULL, 'p_rajanarora8687@gmail.com', '8424892019', '3', '5', '58', '110053', '12/20', NULL, NULL, NULL, NULL, '8424892018', NULL, NULL, 'rajanarora8687@gmail.com', 'rajanarora8687@gmail.com', NULL, NULL, 1, NULL, NULL, '2020-11-03 02:02:25', '2020-11-06 20:15:48'),
(257, '91-P-00000016', 'hlo', NULL, NULL, NULL, NULL, NULL, NULL, 'kishorgupta7329@gmail.com', '7979095514', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-11-04 01:18:35', '2020-11-04 01:18:35'),
(258, '91-P-00000017', 'Manish', NULL, NULL, NULL, NULL, NULL, NULL, 'manishjobs1999@gmail.com', '9990147259', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-11-20 17:21:56', '2020-11-20 17:21:56'),
(259, '91-P-00000018', 'Shahin', NULL, NULL, NULL, NULL, NULL, NULL, 'shahinabdulla@gmail.com', '9633638812', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-11-22 19:39:06', '2020-11-22 19:39:06'),
(260, '91-P-00000019', 'Shahin', NULL, '1989-11-22', 'Male', '175', '75', NULL, 'shahinabdullamp@gmail.com', '7907071953', '3', '5', '35', '6820222', 'wjejek', NULL, NULL, NULL, NULL, '9876543210', NULL, NULL, 'shahin@gmail.com', 'shahin@gmail.in', NULL, NULL, 1, NULL, NULL, '2020-11-22 19:40:54', '2020-11-22 19:43:39'),
(261, '91-P-00000020', 'Rohith .k', NULL, '1998-11-17', 'Male', '150', '190', '0', 'rohith.ktpb333@gmail.com', '8848309676', '3', '3', '1649', '673635', 'test', NULL, NULL, NULL, NULL, '7736207913', NULL, NULL, 'rohith.ktpb333@gmail.com', 'rohith.ktpb333@gmail.com', NULL, NULL, 1, NULL, NULL, '2020-11-22 20:37:45', '2020-11-22 20:41:16'),
(262, '91-P-00000021', 'Shreya Dixit', NULL, '1999-08-12', 'Female', '5\'4', '78', NULL, 'shreyaseventh@gmail.com', '7011350195', '3', '30', '4158', '201301', 'b-122 sector 41 , Noida', NULL, NULL, NULL, NULL, '8852888844', NULL, NULL, 'shreyaseventh@gmail.com', 'aasn@ggma.com', NULL, NULL, 1, NULL, NULL, '2020-11-24 20:03:03', '2020-11-24 20:07:51'),
(263, '91-P-00000022', 'Vicky Gupta', NULL, NULL, NULL, NULL, NULL, NULL, 'rrauniyar50@gmail.com', '8565915382', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-11-25 22:03:58', '2020-11-25 22:03:58'),
(264, '91-P-00000023', 'Ani', NULL, '2020-12-01', 'Male', '5', '70', NULL, 'anichhetry94@gmail.com', '8794206104', '3', '11', '924', '122017', 'A41', NULL, NULL, NULL, NULL, '8794206104', NULL, NULL, 'anichhetry94@gmail.com', 'anichhetry94@gmail.com', NULL, NULL, 1, NULL, NULL, '2020-12-01 17:19:29', '2020-12-01 17:23:11'),
(265, '91-P-00000024', 'ajay', NULL, '1994-06-24', 'Male', '168', '63', NULL, 'ajaysankar.snair@gmail.com', '8281478767', '3', '3', '1733', '695551', 'lekshmi vilasam', NULL, NULL, NULL, NULL, '8281478767', NULL, NULL, 'ajaysankar.snair@gmail.com', 'ajaysankar.snait@gmail.com', NULL, NULL, 1, NULL, NULL, '2020-12-02 01:12:36', '2020-12-02 01:15:59'),
(266, '91-P-00000025', 'Himanshu', NULL, '1997-03-11', 'Male', '6.0', '78', NULL, 'thehimanshumalik@gmail.com', '7015828065', '3', '11', '924', '124001', 'snsnd', NULL, NULL, NULL, NULL, '7015828065', NULL, NULL, 'thehimanshumalik@gmail.com', 'nsnsnsn@gmail.com', NULL, NULL, 1, NULL, NULL, '2020-12-06 03:28:54', '2020-12-06 03:31:11'),
(267, '91-P-00000026', 'althaf', NULL, '1995-06-15', 'Male', '5\'9', '75', NULL, 'althafthajudeen@gmail.com', '8590139005', '3', '3', '1733', '695026', 'hhh', NULL, NULL, NULL, NULL, '8590139005', '8590139005', '8590139005', 'althafthajudeen@gmail.com', 'althafthajudeen@gmail.com', NULL, NULL, 1, NULL, NULL, '2020-12-15 18:32:26', '2020-12-15 18:36:24'),
(268, '91-P-00000027', 'Sandeep singh', NULL, '1992-01-05', 'Male', '6.0', '75', NULL, 'sandeepraj9595@gmail.com', '7972185820', '3', '11', '924', '1220018', 'Gurgaon', NULL, NULL, NULL, NULL, '7972185820', '9955228999', NULL, 'sandeepraj.rj5@gmail.com', 'sandeepraj.rj5@gmail.com', NULL, NULL, 1, NULL, NULL, '2020-12-15 23:59:00', '2021-01-05 19:58:32'),
(269, '91-P-00000028', 'Yuvraj', NULL, NULL, NULL, NULL, NULL, NULL, 'yuvrajsinghrawat101@gmail.com', '9456077563', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-12-25 07:17:28', '2020-12-25 07:17:28'),
(270, '91-P-00000029', 'Malini Mani', NULL, NULL, NULL, NULL, NULL, NULL, 'malini.malini2803@gmail.com', '8778228675', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2021-01-07 22:58:48', '2021-01-07 22:58:48');

-- --------------------------------------------------------

--
-- Table structure for table `patient_refund_request`
--

CREATE TABLE `patient_refund_request` (
  `id` int(10) UNSIGNED NOT NULL,
  `patient_id` int(11) DEFAULT NULL,
  `requested_amount` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bank_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bank_ifsc_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `account_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `account_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cheque_img` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `balance_after_approve` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `approved_balance` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `patient_refund_request`
--

INSERT INTO `patient_refund_request` (`id`, `patient_id`, `requested_amount`, `bank_name`, `bank_ifsc_code`, `account_name`, `account_type`, `cheque_img`, `balance_after_approve`, `approved_balance`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, '25', NULL, NULL, NULL, NULL, NULL, '25', '25', 1, 1, 1, '2020-04-10 11:42:56', '2020-04-10 11:42:56', NULL),
(2, 1, '25', NULL, NULL, NULL, NULL, NULL, '25', '25', 1, 1, 1, '2020-04-10 11:50:03', '2020-04-10 11:50:03', NULL),
(3, 1, '25', 'SBI', 'Abc123', 'Current', 'abcd', '1588394975__1.png', NULL, NULL, 1, 33, 33, '2020-05-02 04:49:35', '2020-05-02 04:49:35', NULL),
(4, 1, '25', 'SBI', 'Abc123', 'Current', 'abcd', '1588398466__1.png', NULL, NULL, 1, 33, 33, '2020-05-02 05:47:46', '2020-05-02 05:47:46', NULL),
(5, 19, '180', 'sbi', '123456789', 'sid', 'current', '1588399908__Screenshot_20200502_094339.jpg', NULL, NULL, 1, 57, 57, '2020-05-02 06:11:48', '2020-05-02 06:11:48', NULL),
(6, 26, '100', 'icici', 'abcd', 'test account', 'current', NULL, NULL, NULL, 1, 62, 62, '2020-06-13 08:46:04', '2020-06-13 08:46:04', NULL),
(7, 19, '125', 'jdjdj', '12345678111', 'dndke', '1222344554444', NULL, NULL, NULL, 1, 57, 57, '2020-06-21 12:35:55', '2020-06-21 12:35:55', NULL),
(8, 19, '6458572', 'Sbi', 'Ty588558', '577985433456788999888', '507707', NULL, NULL, NULL, 1, 57, 57, '2020-06-22 10:03:13', '2020-06-22 10:03:13', NULL),
(9, 59, '500', 'sbi', '12345678', 'siddhi Patel', 'saving', NULL, NULL, NULL, 1, 119, 119, '2020-06-27 12:02:04', '2020-06-27 12:02:04', NULL),
(10, 19, '2', 'test', 'quusuhsh223', 'test', 'test', NULL, NULL, NULL, 1, 57, 57, '2020-07-05 20:32:45', '2020-07-05 20:32:45', NULL),
(11, 102, '100', 'Indian national Bank', '11223', 'Mohsin', 'current', '1597692090__Screenshot_2020-08-18-00-17-44-28.png', NULL, NULL, 1, 201, 201, '2020-08-18 02:21:30', '2020-08-18 02:21:30', NULL),
(12, 102, '2', 'tjj', '12345', 'hhhj', 'hhh', NULL, NULL, NULL, 1, 201, 201, '2020-08-18 02:22:11', '2020-08-18 02:22:11', NULL),
(13, 201, '5000', 'Abcd bank', 'abcd1234', 'Arjun mathur', 'sav', '1603544032__IMG_20200730_122127.jpg', NULL, NULL, 1, 244, 244, '2020-10-24 19:53:53', '2020-10-24 19:53:53', NULL),
(14, 254, '10', 'SBI', 'SBIN0004677', '32328086413', 'Savings', '1603996603__IMG_20201020_235852.jpg', NULL, NULL, 1, 373, 373, '2020-10-30 01:36:43', '2020-10-30 01:36:43', NULL),
(15, 255, '100', 'yg', 'fvb', 'fhh', 'fhh', NULL, NULL, NULL, 1, 377, 377, '2020-11-03 04:59:52', '2020-11-03 04:59:52', NULL),
(16, 254, '8', 'SBI', 'SBIN0004677', 'Vishal Tolani', 'savings', '1604746188__IMG_20201020_235852.jpg', NULL, NULL, 1, 373, 373, '2020-11-07 23:19:48', '2020-11-07 23:19:48', NULL),
(17, 254, '7', 'SBI', 'SBIN0004677', 'vishal Tolani', 'savings account', '1605348603__IMG_20201020_235852.jpg', NULL, NULL, 1, 373, 373, '2020-11-14 22:40:05', '2020-11-14 22:40:05', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `payment_status`
--

CREATE TABLE `payment_status` (
  `id` int(11) NOT NULL,
  `payment_status` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `payment_status`
--

INSERT INTO `payment_status` (`id`, `payment_status`, `created_at`, `updated_at`) VALUES
(1, 'pending', '2020-04-09 09:18:12', '2020-04-09 09:18:12'),
(2, 'completed', '2020-04-09 09:18:12', '2020-04-09 09:18:12'),
(3, 'Faild', '2020-04-09 09:18:29', '2020-04-09 09:19:54');

-- --------------------------------------------------------

--
-- Table structure for table `perceived_patient`
--

CREATE TABLE `perceived_patient` (
  `id` int(10) UNSIGNED NOT NULL,
  `p_patient_id` int(11) NOT NULL,
  `p_doctor_id` int(11) NOT NULL,
  `p_book_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `perceived` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `perceived_patient`
--

INSERT INTO `perceived_patient` (`id`, `p_patient_id`, `p_doctor_id`, `p_book_id`, `perceived`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 19, 1, 'book-00001', 'test data ww', 35, 35, '2020-05-16 11:19:46', '2020-05-16 11:20:00', NULL),
(2, 19, 1, 'book-0000011', 'hpi', 35, 35, '2020-05-16 12:40:25', '2020-05-16 12:40:25', NULL),
(3, 221, 136, '91-B-000000116', 'hppa', 300, 300, '2020-10-20 12:35:58', '2020-10-20 12:35:58', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `group_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `display_name`, `description`, `group_key`, `created_at`, `updated_at`) VALUES
(1, 'add_country', 'add_country', 'Add Country', 'country', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'view_country', 'view_country', 'View Country', 'country', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'edit_country', 'edit_country', 'Edit Country', 'country', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'delete_country', 'delete_country', 'Delete Country', 'country', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'show_country', 'show_country', 'Show Country', 'country', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'add_city', 'add_city', 'Add City', 'city', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 'view_city', 'view_city', 'View City', 'city', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 'edit_city', 'edit_city', 'Edit City', 'city', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 'delete_city', 'delete_city', 'Delete City', 'city', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 'show_city', 'show_city', 'Show City', 'city', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 'add_state', 'add_state', 'Add State', 'state', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 'view_state', 'view_state', 'View State', 'state', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 'edit_state', 'edit_state', 'Edit State', 'state', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 'delete_state', 'delete_state', 'Delete State', 'state', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 'show_state', 'show_state', 'Show State', 'state', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 'add_language', 'add_language', 'Add Language', 'Language', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 'view_language', 'view_language', 'View Language', 'Language', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 'edit_language', 'edit_language', 'Edit Language', 'Language', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, 'delete_language', 'delete_language', 'Delete Language', 'Language', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, 'show_language', 'show_language', 'Show Language', 'Language', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, 'add_specialities', 'add_specialities', 'Add Specialities', 'Specialities', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, 'view_specialities', 'view_specialities', 'View Specialities', 'Specialities', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, 'edit_specialities', 'edit_specialities', 'Edit Specialities', 'Specialities', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, 'delete_specialities', 'delete_specialities', 'Delete Specialities', 'Specialities', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(25, 'show_specialities', 'show_specialities', 'show Specialities', 'Specialities', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(26, 'add_doctor', 'add_doctor', 'Add Doctor', 'Doctor', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(27, 'view_doctor', 'view_doctor', 'View Doctor', 'Doctor', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(28, 'edit_doctor', 'edit_doctor', 'Edit Doctor', 'Doctor', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(29, 'delete_doctor', 'delete_doctor', 'Delete Doctor', 'Doctor', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(30, 'show_doctor', 'show_doctor', 'Show Doctor', 'Doctor', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(31, 'add_patient', 'add_patient', 'Add Patient', 'Patient', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(32, 'view_patient', 'view_patient', 'View Patient', 'Patient', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(33, 'edit_patient', 'edit_patient', 'Edit Patient', 'Patient', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(34, 'delete_patient', 'delete_patient', 'Delete Patient', 'Patient', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(35, 'show_patient', 'show_patient', 'Show Patient', 'Patient', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(36, 'add_service', 'add_service', 'Add Service', 'Service', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(37, 'view_service', 'view_service', 'View Service', 'Service', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(38, 'edit_service', 'edit_service', 'Edit Service', 'Service', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(39, 'delete_service', 'delete_service', 'Delete Service', 'Service', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(40, 'show_service', 'show_service', 'Show Service', 'Service', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(41, 'add_service_price', 'add_service_price', 'Add Service Price', 'Service Price', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(42, 'view_service_price', 'view_service_price', 'View Service Price', 'Service Price', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(43, 'edit_service_price', 'edit_service_price', 'Edit Service Price', 'Service Price', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(44, 'delete_service_price', 'delete_service_price', 'Delete Service Price', 'Service Price', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(45, 'show_service_price', 'show_service_price', 'Show Service Price', 'Service Price', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(46, 'add_bankinfo', 'add_bankinfo', 'Add Bankinfo', 'Bankinfo', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(47, 'view_bankinfo', 'view_bankinfo', 'View Bankinfo', 'Bankinfo', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(48, 'edit_bankinfo', 'edit_bankinfo', 'Edit Bankinfo', 'Bankinfo', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(49, 'delete_bankinfo', 'delete_bankinfo', 'Delete Bankinfo', 'Bankinfo', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(50, 'show_bankinfo', 'show_bankinfo', 'Delete Bankinfo', 'Bankinfo', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(51, 'add_licenceTypes', 'add_licenceTypes', 'Add LicenceTypes', 'LicenceTypes', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(52, 'view_licenceTypes', 'view_licenceTypes', 'View LicenceTypes', 'LicenceTypes', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(53, 'edit_licenceTypes', 'edit_licenceTypes', 'Edit LicenceTypes', 'LicenceTypes', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(54, 'delete_licenceTypes', 'delete_licenceTypes', 'Delete LicenceTypes', 'LicenceTypes', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(55, 'show_licenceTypes', 'show_licenceTypes', 'Show LicenceTypes', 'LicenceTypes', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(56, 'add_booking_requests', 'add_booking_requests', 'Add Booking Requests', 'Booking Requests', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(57, 'view_booking_requests', 'view_booking_requests', 'View Booking Requests', 'Booking Requests', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(58, 'edit_booking_requests', 'edit_booking_requests', 'Edit Booking Requests', 'Booking Requests', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(59, 'delete_booking_requests', 'delete_booking_requests', 'Delete Booking Requests', 'Booking Requests', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(60, 'show_booking_requests', 'show_booking_requests', 'Show Booking Requests', 'Booking Requests', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(61, 'add_doctor_availabilities', 'add_doctor_availabilities', 'Add Doctor Availabilities', 'Doctor Availabilities', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(62, 'view_doctor_availabilities', 'view_doctor_availabilities', 'View Doctor Availabilities', 'Doctor Availabilities', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(63, 'edit_doctor_availabilities', 'edit_doctor_availabilities', 'Edit Doctor Availabilities', 'Doctor Availabilities', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(64, 'delete_doctor_availabilities', 'delete_doctor_availabilities', 'Delete Doctor Availabilities', 'Doctor Availabilities', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(65, 'show_doctor_availabilities', 'show_doctor_availabilities', 'Show Doctor Availabilities', 'Doctor Availabilities', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(66, 'add_prescription_details', 'add_prescription_details', 'Add Prescription Details', 'Prescription Details', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(67, 'view_prescription_details', 'view_prescription_details', 'View Prescription Details', 'Prescription Details', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(68, 'edit_prescription_details', 'edit_prescription_details', 'Edit Prescription Details', 'Prescription Details', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(69, 'delete_prescription_details', 'delete_prescription_details', 'Delete Prescription Details', 'Prescription Details', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(70, 'show_prescription_details', 'show_prescription_details', 'Show Prescription Details', 'Prescription Details', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(71, 'admin', 'Admin', 'Admin', 'admin', '2020-04-11 12:22:43', '2020-04-11 12:22:43'),
(72, 'add_haspatal_register', 'add_haspatal_register', 'add haspatal register', 'haspatal register', '2020-10-31 19:43:29', '2020-10-31 19:44:24'),
(73, 'view_haspatal_register', 'view_haspatal_register', 'view haspatal register', 'haspatal register', NULL, NULL),
(74, 'edit_haspatal_register', 'edit_haspatal_register', 'Edit haspatal register', 'haspatal register', NULL, NULL),
(75, 'delete_haspatal_register', 'delete_haspatal_register', 'Delete haspatal register', 'haspatal register', NULL, NULL),
(76, 'show_haspatal_register', 'show_haspatal_register', 'Show haspatal register', 'haspatal register', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 1),
(28, 1),
(29, 1),
(30, 1),
(31, 1),
(32, 1),
(33, 1),
(34, 1),
(35, 1),
(36, 1),
(37, 1),
(38, 1),
(39, 1),
(40, 1),
(41, 1),
(42, 1),
(43, 1),
(44, 1),
(45, 1),
(46, 1),
(47, 1),
(48, 1),
(49, 1),
(50, 1),
(51, 1),
(52, 1),
(53, 1),
(54, 1),
(55, 1),
(56, 1),
(57, 1),
(58, 1),
(59, 1),
(60, 1),
(61, 1),
(62, 1),
(63, 1),
(64, 1),
(65, 1),
(66, 1),
(67, 1),
(68, 1),
(69, 1),
(70, 1),
(71, 1),
(31, 3),
(32, 3),
(33, 3),
(34, 3),
(35, 3),
(46, 3),
(47, 3),
(48, 3),
(49, 3),
(50, 3),
(56, 3),
(57, 3),
(58, 3),
(59, 3),
(60, 3),
(61, 3),
(62, 3),
(63, 3),
(64, 3),
(65, 3),
(66, 3),
(67, 3),
(68, 3),
(69, 3),
(70, 3),
(1, 15),
(2, 15),
(3, 15),
(5, 15),
(26, 15),
(27, 15),
(28, 15),
(29, 15),
(30, 15);

-- --------------------------------------------------------

--
-- Table structure for table `plan_master`
--

CREATE TABLE `plan_master` (
  `pl_id` int(10) UNSIGNED NOT NULL,
  `pl_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pl_price` int(11) NOT NULL,
  `pl_deduction` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pl_daily_sub_amount` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pl_validity` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pl_desc` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `plan_master`
--

INSERT INTO `plan_master` (`pl_id`, `pl_name`, `pl_price`, `pl_deduction`, `pl_daily_sub_amount`, `pl_validity`, `pl_desc`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Plan 20', 15, '15', '20', '365', 'subscription Rs 20 per day, Unlimited Consult', 2, 1, 1, '2020-04-28 07:45:47', '2020-09-28 13:52:57', NULL),
(2, 'Basic Plan', 20, '20', '0', '365', 'zero subscription, Unlimited Consult', 2, 1, 1, '2020-04-28 07:47:04', '2020-09-28 13:53:19', NULL),
(3, 'Plan 30', 10, '10', '30', '365', 'subscription Rs 30 per day, Unlimited Consult', 2, 1, 1, '2020-09-22 17:23:51', '2020-09-28 13:53:41', NULL),
(4, 'Plan 100', 0, '0', '1', '365', 'subscription Rs 50 per day, Unlimited Consult', 1, 1, 1, '2020-09-22 17:24:15', '2020-10-14 12:55:52', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `prescription_details`
--

CREATE TABLE `prescription_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `patient_id` int(11) NOT NULL,
  `doctor_id` int(11) NOT NULL,
  `booking_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `age` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `height` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `weight` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lmp` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `medicine_id` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `faq_administration` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `duration` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lab_test_id` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `imaging_id` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `therapy_id` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `counseling_id` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `home_care_id` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `referral_specialit_id` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `folloup_id` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `prescription` text COLLATE utf8mb4_unicode_ci,
  `prescription_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pdf` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `prescription_details`
--

INSERT INTO `prescription_details` (`id`, `patient_id`, `doctor_id`, `booking_id`, `age`, `gender`, `height`, `weight`, `lmp`, `medicine_id`, `faq_administration`, `duration`, `lab_test_id`, `imaging_id`, `therapy_id`, `counseling_id`, `home_care_id`, `referral_specialit_id`, `folloup_id`, `prescription`, `prescription_image`, `pdf`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 19, 1, 'book-00001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'tett data 111', NULL, 'public/pdf/1589547938__Prescription.pdf', 1, 35, 35, '2020-05-15 13:02:59', '2020-05-15 13:05:39', NULL),
(2, 19, 1, 'book-0000010', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'test preq', NULL, 'public/pdf/1589603885__Prescription.pdf', 1, 35, 35, '2020-05-16 04:37:58', '2020-05-16 04:38:05', NULL),
(3, 19, 1, 'book-0000011', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'present', NULL, 'public/pdf/1589632856__Prescription.pdf', 1, 35, 35, '2020-05-16 12:40:56', '2020-05-16 12:40:56', NULL),
(4, 19, 1, 'book-0000017', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'present', NULL, 'public/pdf/1589632844__Prescription.pdf', 1, 35, 35, '2020-05-16 12:41:45', '2020-05-16 12:41:45', NULL),
(7, 19, 1, 'book-0000024', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'hello test Test test', NULL, 'public/pdf/1596279820__Prescription.pdf', 1, 35, 35, '2020-08-01 18:03:41', '2020-08-01 18:03:41', NULL),
(8, 19, 1, 'book-0000027', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'testing....', NULL, 'public/pdf/1597387415__Prescription.pdf', 1, 35, 35, '2020-08-04 13:02:00', '2020-08-14 13:43:36', NULL),
(9, 19, 1, 'book-0000028', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'testing....11', NULL, 'public/pdf/1597409278__Prescription.pdf', 1, 35, 35, '2020-08-14 13:51:03', '2020-08-14 19:47:59', NULL),
(10, 19, 1, 'book-0000049', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'test pres', NULL, 'public/pdf/1597490277__Prescription.pdf', 1, 35, 35, '2020-08-15 12:43:18', '2020-08-15 18:17:58', NULL),
(13, 191, 114, 'book-0000088', NULL, NULL, NULL, NULL, NULL, '1', 'after meal', '1-1-0', '1', '1', '1', '1', '1', '1', '1', '', NULL, 'public/pdf/1602076972__Prescription.pdf', 1, 249, 249, '2020-10-06 19:27:25', '2020-10-07 20:22:52', NULL),
(14, 196, 142, 'book-0000089', NULL, NULL, NULL, NULL, NULL, '2', 'fr', '1', '2', '2', '2', '2', '5', 'Bariatric Surgery', '3 Days', '', NULL, 'public/pdf/1602064115__Prescription.pdf', 1, 307, 307, '2020-10-07 16:43:12', '2020-10-07 16:48:38', NULL),
(15, 221, 136, '91-B-000000115', '1992-03-07', 'Male', '170', '78', NULL, NULL, NULL, NULL, '2', '2', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, 1, 300, 300, '2020-10-20 12:21:09', '2020-10-20 12:21:27', NULL),
(16, 254, 160, '91-B-000000163', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Dizziness.\nRecommended PCM . BD', NULL, NULL, 1, 383, 383, '2020-11-08 00:54:59', '2020-11-08 00:54:59', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `present_complaint_patient`
--

CREATE TABLE `present_complaint_patient` (
  `id` int(10) UNSIGNED NOT NULL,
  `pc_patient_id` int(11) NOT NULL,
  `pc_doctor_id` int(11) NOT NULL,
  `book_id` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `present_complaint` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `complaint_pic` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `complaint_pdf` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `present_complaint_patient`
--

INSERT INTO `present_complaint_patient` (`id`, `pc_patient_id`, `pc_doctor_id`, `book_id`, `present_complaint`, `complaint_pic`, `complaint_pdf`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, 19, 1, 'book-0000034', 'test 29-4-2020', 'Screenshot_20200429_133741.jpg$Screenshot_20200428_183523.jpg', NULL, 57, 57, '2020-04-29 10:33:58', '2020-04-29 10:33:58', NULL),
(3, 19, 1, 'book-0000039', 'test', 'Screenshot_20200429_133741.jpg', NULL, 57, 57, '2020-04-29 12:14:33', '2020-04-29 12:16:17', NULL),
(4, 19, 6, 'book-0000042', 'test', 'IMG-20200428-WA0004.jpg$IMG-20200428-WA0003.jpg$Screenshot_20200428_132224.jpg$Screenshot_20200428_092817.jpg', NULL, 57, 57, '2020-04-29 12:49:00', '2020-04-29 12:49:00', NULL),
(5, 26, 1, 'book-0000044', 'testtttt NKkkk', NULL, NULL, 62, 62, '2020-04-29 18:09:38', '2020-04-29 18:09:38', NULL),
(6, 23, 1, 'book-0000045', 'test 2542020', 'IMG-20200430-WA0013.jpg', NULL, 59, 59, '2020-04-30 08:57:13', '2020-04-30 08:57:13', NULL),
(7, 23, 1, 'book-0000046', 'test promo code', 'IMG-20200430-WA0015.jpg$IMG-20200430-WA0012.jpg$IMG-20200430-WA0013.jpg$IMG-20200430-WA0014.jpg$IMG-20200430-WA0010.jpg', NULL, 59, 59, '2020-04-30 09:00:25', '2020-04-30 09:00:25', NULL),
(8, 26, 1, 'book-0000047', 'test book counsult call', 'IMG-20200430-WA0017.jpg$IMG-20200430-WA0016.jpg$IMG-20200430-WA0016.jpg$IMG_20200430_142226.jpg$Screenshot_2020-04-30-14-21-43-913_com.haspatal.hospital.jpg', NULL, 62, 62, '2020-04-30 11:37:47', '2020-04-30 11:37:47', NULL),
(9, 19, 1, 'book-0000050', 'test 01052020', 'Screenshot_20200430_111539.jpg', NULL, 57, 57, '2020-05-01 03:52:33', '2020-05-01 03:52:33', NULL),
(10, 19, 1, 'book-0000052', 'test52', 'Screenshot_20200430_144539.jpg$Screenshot_20200430_141537.jpg$IMG-20200430-WA0003.jpg', NULL, 57, 57, '2020-05-01 06:43:27', '2020-05-01 06:43:27', NULL),
(11, 19, 1, 'book-0000054', 'test', 'Screenshot_20200502_094339.jpg$Screenshot_20200501_212149.jpg', NULL, 57, 57, '2020-05-02 10:09:30', '2020-05-02 10:09:30', NULL),
(12, 23, 1, 'book-000001', 'test 5220', 'IMG-20200502-WA0012.jpg', NULL, 59, 59, '2020-05-02 13:47:59', '2020-05-02 13:47:59', NULL),
(13, 19, 1, 'book-000001', 'test booking', 'Screenshot_20200502_184342.jpg$Screenshot_20200502_154716.jpg', NULL, 57, 57, '2020-05-05 05:11:53', '2020-05-05 05:11:53', NULL),
(14, 19, 1, 'book-000002', 'test', 'Image-3442.jpg$Screenshot_20200511_132705.jpg', NULL, 57, 57, '2020-05-13 11:03:23', '2020-05-13 11:03:23', NULL),
(15, 19, 1, 'book-000007', 'test', 'Screenshot_20200511_132705.jpg', NULL, 57, 57, '2020-05-15 10:13:43', '2020-05-15 10:13:43', NULL),
(16, 19, 1, 'book-0000010', 'test', 'IMG-20200705-WA0001.jpg', NULL, 57, 57, '2020-05-16 04:15:33', '2020-07-05 18:50:42', NULL),
(17, 19, 1, 'book-0000011', 'test', 'Image-807.jpg', NULL, 57, 57, '2020-05-16 08:13:16', '2020-05-16 08:13:16', NULL),
(18, 23, 1, 'book-0000013', 'test data 16520', NULL, NULL, 59, 59, '2020-05-16 13:31:29', '2020-05-16 13:31:29', NULL),
(19, 19, 6, 'book-0000014', 'test 19-05-2020', 'Screenshot_20200518_102801.jpg', NULL, 57, 57, '2020-05-19 09:26:49', '2020-05-19 09:26:49', NULL),
(20, 26, 12, 'book-0000015', 'testing the complaint', 'IMG-20200527-WA0015.jpg', NULL, 62, 62, '2020-05-27 10:24:31', '2020-05-27 10:24:31', NULL),
(21, 26, 12, 'book-0000016', 'I am having severe pain in right ankle .... no swelling ...but in past years I used to play football.', 'IMG-20200522-WA0040.jpg', NULL, 62, 62, '2020-05-27 14:15:19', '2020-05-27 14:15:19', NULL),
(22, 19, 1, 'book-0000017', 'test', 'IMG-20200526-WA0005.jpg', NULL, 57, 57, '2020-05-29 04:22:10', '2020-05-29 04:22:10', NULL),
(23, 19, 1, 'book-0000018', 'test', NULL, NULL, 57, 57, '2020-06-01 12:32:53', '2020-06-01 12:32:53', NULL),
(24, 19, 1, 'book-0000019', 'test', NULL, NULL, 57, 57, '2020-06-02 05:56:56', '2020-06-02 05:56:56', NULL),
(25, 19, 1, 'book-0000020', 'test update', NULL, NULL, 57, 57, '2020-06-04 09:40:40', '2020-06-04 09:40:40', '2020-07-31 23:31:18'),
(26, 19, 1, 'book-0000021', 'test wallet', NULL, NULL, 57, 57, '2020-06-04 11:21:39', '2020-06-04 11:21:39', NULL),
(27, 19, 1, 'book-0000022', 'test', NULL, NULL, 57, 57, '2020-06-08 07:51:03', '2020-06-08 07:51:03', NULL),
(28, 19, 1, 'book-000001', 'Test', NULL, NULL, 57, 57, '2020-06-08 09:56:18', '2020-06-08 09:56:18', NULL),
(29, 26, 1, 'book-000002', 'beta test application', 'IMG-20200612-WA0008.jpg', NULL, 62, 62, '2020-06-13 08:43:37', '2020-06-13 08:43:37', NULL),
(30, 19, 38, 'book-000001', 'Dutdugcycktfitsrs', 'images.jpeg', NULL, 57, 57, '2020-06-22 09:09:54', '2020-06-22 09:09:54', NULL),
(31, 19, 1, 'book-000002', 'test', 'IMG-20200622-WA0002.jpg', NULL, 57, 57, '2020-06-23 05:08:51', '2020-06-23 05:08:51', NULL),
(32, 19, 39, 'book-000009', 'Cough n sore throat.', 'IMG-20200704-WA0024.jpg', NULL, 57, 57, '2020-07-05 18:47:13', '2020-07-05 18:47:13', NULL),
(33, 19, 39, 'book-0000013', 'Fever and cough', 'magazine-unlock-01-2.3.4822-_5FF5A90803958DA4C5793F124A2C523E.jpg', NULL, 57, 57, '2020-07-05 18:55:21', '2020-07-05 18:55:21', NULL),
(34, 19, 39, 'book-0000014', 'test', NULL, NULL, 57, 57, '2020-07-05 20:18:32', '2020-07-05 20:18:32', NULL),
(35, 56, 1, 'book-0000020', 'test data', 'IMG-20200727-WA0004.jpg', NULL, 116, 116, '2020-07-31 16:44:56', '2020-07-31 16:44:56', NULL),
(36, 201, 93, 'book-0000054', 'payment done', 'Screenshot_2020-09-02-14-45-02-140_com.haspatal.hospital.jpg', NULL, 244, 244, '2020-09-03 12:56:35', '2020-09-03 12:56:35', NULL),
(37, 201, 144, 'book-0000064', 'this is chief complaint test by ashish\n24/9/20', 'IMG-20200923-WA0016.jpg', NULL, 244, 244, '2020-09-24 18:15:10', '2020-09-24 18:15:10', NULL),
(38, 196, 142, 'book-0000065', 'test complain', 'Screenshot_20200923_163036.jpg$IMG-20200923-WA0011.jpg', NULL, 237, 237, '2020-09-24 18:25:00', '2020-09-24 18:25:00', NULL),
(39, 201, 144, 'book-0000068', '25/9/2020 test counsult', 'Screenshot_2020-09-24-17-47-35-218_com.haspatal.doctor.jpg$Image-2772.jpg', NULL, 244, 244, '2020-09-25 12:46:58', '2020-09-25 12:46:58', NULL),
(40, 196, 142, 'book-0000070', 'test complain 70', 'IMG-20200924-WA0007.jpg$IMG-20200924-WA0006.jpg$IMG-20200924-WA0004.jpg$Screenshot_20200924_172304.jpg$IMG-20200923-WA0012.jpg$Screenshot_20200923_163036.jpg$IMG-20200923-WA0011.jpg$IMG-20200923-WA0010.jpg$IMG-20200923-WA0009.jpg', NULL, 237, 237, '2020-09-25 14:37:16', '2020-09-25 14:51:47', NULL),
(41, 196, 142, 'book-0000071', 'testy', NULL, NULL, 237, 237, '2020-09-25 16:05:52', '2020-09-25 16:05:52', NULL),
(42, 196, 142, 'book-0000072', 'test', 'IMG-20200924-WA0007.jpg$IMG-20200924-WA0006.jpg', NULL, 237, 237, '2020-09-25 18:04:05', '2020-09-25 18:04:05', NULL),
(43, 216, 144, 'book-0000074', 'Pain in legs for last 5 yeas', 'Screenshot_20200927-150400.jpg', NULL, 305, 305, '2020-09-27 16:42:21', '2020-09-27 16:42:21', NULL),
(44, 208, 144, 'book-0000075', 'pain in feet', '20200926_112147.jpg', NULL, 278, 278, '2020-09-27 16:51:12', '2020-09-27 16:51:12', NULL),
(45, 229, 144, 'book-0000077', 'need hair treatment for falling hair', NULL, NULL, 326, 326, '2020-09-30 19:13:45', '2020-09-30 19:13:45', NULL),
(46, 196, 142, 'book-0000078', 'test', 'IMG-20200930-WA0002.jpg', NULL, 237, 237, '2020-10-01 11:39:19', '2020-10-01 11:39:19', NULL),
(47, 196, 142, 'book-0000079', 'test', NULL, NULL, 237, 237, '2020-10-01 15:14:38', '2020-10-01 15:14:38', NULL),
(48, 216, 144, 'book-0000086', 'this is test chief complaint of 4 oct', 'IMG-20201003-WA0019.jpg', NULL, 305, 305, '2020-10-04 17:46:05', '2020-10-04 17:46:05', NULL),
(49, 216, 144, '91-B-00000096', 'pain in left leg lower part.near foot', 'Screenshot_20201011-093536.jpg', NULL, 305, 305, '2020-10-11 13:17:17', '2020-10-11 13:17:17', NULL),
(50, 216, 144, '91-B-000000104', 'this is sample chief complaint', 'IMG-20201017-WA0039.jpg', NULL, 305, 305, '2020-10-19 01:34:52', '2020-10-19 01:34:52', NULL),
(51, 221, 136, '91-B-000000110', 'getting drowsy', NULL, NULL, 316, 316, '2020-10-20 11:52:24', '2020-10-20 11:52:24', NULL),
(52, 201, 157, '91-B-000000112', 'test complaints', 'IMG-20201018-WA0004.jpeg', NULL, 244, 244, '2020-10-20 12:00:44', '2020-10-20 12:00:44', NULL),
(53, 201, 157, '91-B-000000113', 'test chief complaint 2', 'IMG-20201019-WA0003.jpg', NULL, 244, 244, '2020-10-20 12:02:38', '2020-10-20 12:02:38', NULL),
(54, 221, 136, '91-B-000000114', 'dizziness', NULL, NULL, 316, 316, '2020-10-20 12:02:55', '2020-10-20 12:02:55', NULL),
(55, 221, 136, '91-B-000000115', 'test', NULL, NULL, 316, 316, '2020-10-20 12:17:00', '2020-10-20 12:17:00', NULL),
(56, 216, 144, '91-B-000000120', 'testing chief complaint', 'IMG-20201019-WA0007.jpg', NULL, 305, 305, '2020-10-21 11:54:26', '2020-10-21 11:54:26', NULL),
(57, 216, 144, '91-B-000000129', 'tfyv', 'IMG-20201021-WA0000.jpg', NULL, 305, 305, '2020-10-22 00:20:50', '2020-10-22 00:20:50', NULL),
(58, 201, 157, '91-B-000000130', 'test advance booking', NULL, NULL, 244, 244, '2020-10-22 01:31:26', '2020-10-22 01:31:26', NULL),
(59, 216, 144, '91-B-000000135', 'grsdt', NULL, NULL, 305, 305, '2020-10-23 01:19:20', '2020-10-23 01:19:20', NULL),
(60, 191, 114, '91-B-000000136', 'demo demo', 'IMG-20201022-WA0009.jpg', NULL, 231, 231, '2020-10-23 13:40:29', '2020-10-23 13:40:29', NULL),
(61, 216, 144, '91-B-000000138', 'test chief complaint', NULL, NULL, 305, 305, '2020-10-24 00:17:05', '2020-10-24 00:17:05', NULL),
(62, 201, 157, '91-B-000000139', 'test time complaint', NULL, NULL, 244, 244, '2020-10-24 11:12:14', '2020-10-24 11:12:14', NULL),
(63, 201, 157, '91-B-000000141', 'test haspatal wallet payment', '9974573522@sbi-2020921211837.png', NULL, 244, 244, '2020-10-24 19:42:27', '2020-10-24 19:42:27', NULL),
(64, 201, 157, '91-B-000000143', 'test booking confirmation', NULL, NULL, 244, 244, '2020-10-24 20:10:56', '2020-10-24 20:10:56', NULL),
(65, 196, 142, '91-B-000000144', 'demo 44', 'IMG-20201024-WA0004.jpg$IMG-20201024-WA0003.jpg', NULL, 237, 237, '2020-10-25 09:24:07', '2020-10-25 09:24:07', NULL),
(66, 216, 144, '91-B-000000151', 'hgjjfjhcj', 'Screenshot_20201028-070524.jpg', NULL, 305, 305, '2020-10-28 18:48:19', '2020-10-28 18:48:19', NULL),
(67, 254, 136, '91-B-000000159', 'nausea', 'Screenshot_20201031-182029.png', NULL, 373, 373, '2020-11-06 19:20:45', '2020-11-06 19:20:45', NULL),
(68, 254, 160, '91-B-000000162', 'nausea', 'Image-1430.jpg', NULL, 373, 373, '2020-11-08 00:47:44', '2020-11-08 00:47:44', NULL),
(69, 254, 160, '91-B-000000163', 'abc', 'Image-1430.jpg', NULL, 373, 373, '2020-11-08 00:51:31', '2020-11-08 00:51:31', NULL),
(70, 221, 136, '91-B-000000170', 'rtyyu', 'IMG-20210101-WA0018.jpg', NULL, 316, 316, '2021-01-05 21:19:37', '2021-01-05 21:19:37', NULL),
(71, 221, 136, '91-B-000000172', 'bjjj', NULL, NULL, 316, 316, '2021-01-07 01:37:01', '2021-01-07 01:37:01', NULL),
(72, 221, 136, '91-B-000000173', '..', NULL, NULL, 316, 316, '2021-01-07 01:59:40', '2021-01-07 01:59:40', NULL),
(73, 221, 136, '91-B-000000175', 'yy', NULL, NULL, 316, 316, '2021-01-10 00:55:17', '2021-01-10 00:55:17', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `present_medications_patient`
--

CREATE TABLE `present_medications_patient` (
  `id` int(10) UNSIGNED NOT NULL,
  `patient_id` int(11) NOT NULL,
  `doctor_id` int(11) NOT NULL,
  `m_book_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `present_medications` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `present_medications_patient`
--

INSERT INTO `present_medications_patient` (`id`, `patient_id`, `doctor_id`, `m_book_id`, `present_medications`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 19, 1, 'book-000006', 'test medicine...1', 35, 35, '2020-05-15 08:00:35', '2020-05-15 08:00:43', NULL),
(2, 19, 1, 'book-0000011', 'medicine', 35, 35, '2020-05-16 12:39:57', '2020-05-16 12:39:57', NULL),
(3, 221, 136, '91-B-000000114', 'diclofenac', 300, 300, '2020-10-20 12:10:36', '2020-10-20 12:10:36', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `referral_specialities`
--

CREATE TABLE `referral_specialities` (
  `id` int(10) UNSIGNED NOT NULL,
  `dr_id` int(11) NOT NULL,
  `specialities_name` varchar(110) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `referral_specialities`
--

INSERT INTO `referral_specialities` (`id`, `dr_id`, `specialities_name`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(3, 114, '1,2,3', 249, 249, '2020-10-05 18:45:14', '2020-10-05 18:45:14'),
(4, 142, 'Andrology,Bariatric Surgery,Bone Marrow Transplantation,Cardiac Surgery', 307, 307, '2020-10-05 19:24:58', '2020-10-05 19:24:58');

-- --------------------------------------------------------

--
-- Table structure for table `reports`
--

CREATE TABLE `reports` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `report_controllers`
--

CREATE TABLE `report_controllers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `review`
--

CREATE TABLE `review` (
  `id` int(10) UNSIGNED NOT NULL,
  `patient_id` int(11) DEFAULT NULL,
  `doctor_id` int(11) DEFAULT NULL,
  `book_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rating` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `review_que` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `review_ans` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `review`
--

INSERT INTO `review` (`id`, `patient_id`, `doctor_id`, `book_id`, `rating`, `review_que`, `review_ans`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 19, 1, 'book-000004', '5.0', 'It was wonderful to have consulted', 'Thank you so much', 57, 57, '2020-07-29 16:50:10', '2020-06-22 09:52:02', NULL),
(2, 19, 1, 'book-000006', '3.0', 'It was wonderful to have consulted', 'Thank you so much', 57, 57, '2020-06-01 10:10:20', '2020-06-01 10:10:20', NULL),
(3, 19, 1, 'book-0000017', '4.5', 'Nice', NULL, 57, 57, '2020-07-29 17:01:56', '2020-07-29 17:01:56', NULL),
(4, 221, 136, '91-B-000000114', '5.0', 'good service', NULL, 316, 316, '2020-10-21 20:39:30', '2020-10-21 20:39:30', NULL),
(5, 221, 136, '91-B-000000119', '1.5', 'bad', NULL, 316, 316, '2020-10-30 18:59:58', '2020-10-30 19:00:16', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `review_360s`
--

CREATE TABLE `review_360s` (
  `id` int(10) UNSIGNED NOT NULL,
  `patient_id` int(11) NOT NULL,
  `f_id` int(11) NOT NULL,
  `book_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rating` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `review_que` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `review_ans` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `review_360s`
--

INSERT INTO `review_360s` (`id`, `patient_id`, `f_id`, `book_id`, `rating`, `review_que`, `review_ans`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 2, 109, 'book-0000028', '4.5', 'test', 'test demo', 57, 57, '2020-08-05 16:00:53', '2020-08-05 16:00:53', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `review_hospital`
--

CREATE TABLE `review_hospital` (
  `id` int(10) UNSIGNED NOT NULL,
  `patient_id` int(11) DEFAULT NULL,
  `hospital_id` int(11) DEFAULT NULL,
  `book_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rating` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `review_que` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `review_ans` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `review_hospital`
--

INSERT INTO `review_hospital` (`id`, `patient_id`, `hospital_id`, `book_id`, `rating`, `review_que`, `review_ans`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(4, 191, 1, NULL, '5', 'demo demo', NULL, 249, 249, NULL, NULL, NULL),
(5, 191, 1, NULL, '5', 'demo demo', NULL, 249, 249, NULL, NULL, NULL),
(6, 191, 1, NULL, '5', 'demo demo', NULL, 249, 249, NULL, NULL, NULL),
(7, 196, 13, NULL, '4.0', 'test review', NULL, 237, 237, NULL, NULL, NULL),
(8, 221, 12, NULL, '4.5', 'good service', NULL, 316, 316, NULL, NULL, NULL),
(9, 254, 12, NULL, '4.0', 'cgghhh', NULL, 373, 373, NULL, NULL, NULL),
(10, 254, 13, NULL, '3.0', 'hi', NULL, 373, 373, NULL, NULL, NULL),
(11, 216, 12, NULL, '0.0', 'gcgvvb', NULL, 305, 305, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'superadmin', 'Super Admin', 'Super Admin', '2020-03-11 20:33:00', '2020-05-08 09:34:36'),
(2, 'patient', 'Patient', 'Patient', '2020-03-30 18:30:00', NULL),
(3, 'doctor', 'doctor', 'doctor', '2020-03-28 04:30:13', '2020-03-28 04:30:13'),
(4, 'Member', 'Member', 'Member', '2020-03-30 06:52:47', '2020-03-30 06:52:47'),
(11, 'Pharmacies', 'Pharmacies', 'Pharmacies', '2020-05-04 05:43:10', '2020-05-04 05:43:10'),
(12, 'Diagnostics', 'Diagnostics', 'Diagnostics', '2020-05-04 05:44:05', '2020-05-04 05:44:05'),
(13, 'Imaging', 'Imaging', 'Imaging', '2020-05-04 05:44:27', '2020-05-04 05:44:27'),
(14, 'Homecare', 'Homecare', 'Homecare', '2020-05-04 05:46:40', '2020-05-04 05:46:40'),
(15, 'Hospital Admin', 'Admin', 'Hospital Admin', '2020-05-08 09:38:24', '2020-07-06 06:02:46'),
(16, 'specialist', 'Specialist', 'specialist', '2020-06-08 10:25:05', '2020-11-10 18:37:17'),
(17, 'operation manager', 'operation manager', 'operation manager', '2020-10-31 18:46:32', '2020-10-31 18:46:32'),
(18, 'operator', 'operator', 'operator', '2020-11-05 01:10:38', '2020-11-05 01:10:38');

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`user_id`, `role_id`) VALUES
(1, 1),
(435, 1),
(2, 2),
(4, 2),
(5, 2),
(8, 2),
(9, 2),
(10, 2),
(12, 2),
(16, 2),
(17, 2),
(18, 2),
(19, 2),
(25, 2),
(27, 2),
(28, 2),
(29, 2),
(30, 2),
(31, 2),
(32, 2),
(33, 2),
(34, 2),
(46, 2),
(47, 2),
(48, 2),
(49, 2),
(50, 2),
(51, 2),
(52, 2),
(53, 2),
(54, 2),
(55, 2),
(56, 2),
(57, 2),
(59, 2),
(62, 2),
(63, 2),
(65, 2),
(113, 2),
(116, 2),
(118, 2),
(119, 2),
(120, 2),
(121, 2),
(142, 2),
(143, 2),
(170, 2),
(193, 2),
(194, 2),
(195, 2),
(196, 2),
(197, 2),
(198, 2),
(200, 2),
(201, 2),
(208, 2),
(210, 2),
(211, 2),
(212, 2),
(213, 2),
(214, 2),
(217, 2),
(218, 2),
(219, 2),
(220, 2),
(221, 2),
(222, 2),
(223, 2),
(224, 2),
(225, 2),
(226, 2),
(227, 2),
(229, 2),
(230, 2),
(231, 2),
(232, 2),
(234, 2),
(235, 2),
(236, 2),
(237, 2),
(239, 2),
(240, 2),
(242, 2),
(243, 2),
(244, 2),
(245, 2),
(246, 2),
(256, 2),
(262, 2),
(270, 2),
(276, 2),
(278, 2),
(290, 2),
(305, 2),
(311, 2),
(313, 2),
(314, 2),
(315, 2),
(316, 2),
(319, 2),
(320, 2),
(321, 2),
(322, 2),
(323, 2),
(324, 2),
(325, 2),
(326, 2),
(327, 2),
(328, 2),
(331, 2),
(332, 2),
(333, 2),
(334, 2),
(336, 2),
(337, 2),
(338, 2),
(339, 2),
(340, 2),
(347, 2),
(352, 2),
(353, 2),
(354, 2),
(357, 2),
(358, 2),
(361, 2),
(363, 2),
(364, 2),
(366, 2),
(369, 2),
(370, 2),
(372, 2),
(373, 2),
(377, 2),
(379, 2),
(380, 2),
(414, 2),
(415, 2),
(416, 2),
(417, 2),
(420, 2),
(421, 2),
(423, 2),
(424, 2),
(427, 2),
(430, 2),
(431, 2),
(454, 2),
(498, 2),
(7, 3),
(35, 3),
(40, 3),
(41, 3),
(42, 3),
(44, 3),
(45, 3),
(60, 3),
(61, 3),
(66, 3),
(70, 3),
(71, 3),
(72, 3),
(73, 3),
(74, 3),
(75, 3),
(80, 3),
(81, 3),
(82, 3),
(83, 3),
(101, 3),
(114, 3),
(122, 3),
(128, 3),
(141, 3),
(144, 3),
(145, 3),
(146, 3),
(147, 3),
(148, 3),
(149, 3),
(150, 3),
(151, 3),
(152, 3),
(153, 3),
(156, 3),
(166, 3),
(167, 3),
(168, 3),
(172, 3),
(174, 3),
(175, 3),
(176, 3),
(177, 3),
(178, 3),
(179, 3),
(180, 3),
(181, 3),
(182, 3),
(183, 3),
(184, 3),
(185, 3),
(186, 3),
(187, 3),
(188, 3),
(189, 3),
(190, 3),
(191, 3),
(192, 3),
(199, 3),
(209, 3),
(215, 3),
(216, 3),
(238, 3),
(241, 3),
(247, 3),
(248, 3),
(249, 3),
(250, 3),
(251, 3),
(252, 3),
(253, 3),
(254, 3),
(255, 3),
(257, 3),
(258, 3),
(259, 3),
(260, 3),
(261, 3),
(263, 3),
(264, 3),
(265, 3),
(266, 3),
(267, 3),
(268, 3),
(269, 3),
(271, 3),
(272, 3),
(273, 3),
(274, 3),
(275, 3),
(277, 3),
(279, 3),
(280, 3),
(281, 3),
(282, 3),
(283, 3),
(284, 3),
(285, 3),
(286, 3),
(287, 3),
(288, 3),
(289, 3),
(291, 3),
(300, 3),
(301, 3),
(302, 3),
(303, 3),
(304, 3),
(306, 3),
(307, 3),
(308, 3),
(309, 3),
(310, 3),
(312, 3),
(317, 3),
(318, 3),
(329, 3),
(330, 3),
(335, 3),
(342, 3),
(343, 3),
(344, 3),
(348, 3),
(349, 3),
(350, 3),
(351, 3),
(356, 3),
(362, 3),
(365, 3),
(367, 3),
(383, 3),
(393, 3),
(489, 3),
(1, 11),
(2, 11),
(5, 11),
(6, 11),
(8, 11),
(9, 11),
(10, 11),
(20, 11),
(25, 11),
(27, 11),
(29, 11),
(30, 11),
(31, 11),
(32, 11),
(33, 11),
(34, 11),
(35, 11),
(36, 11),
(38, 11),
(39, 11),
(41, 11),
(42, 11),
(43, 11),
(52, 11),
(53, 11),
(54, 11),
(57, 11),
(58, 11),
(59, 11),
(61, 11),
(62, 11),
(63, 11),
(64, 11),
(65, 11),
(67, 11),
(68, 11),
(69, 11),
(70, 11),
(71, 11),
(72, 11),
(73, 11),
(74, 11),
(75, 11),
(76, 11),
(77, 11),
(78, 11),
(79, 11),
(81, 11),
(82, 11),
(83, 11),
(84, 11),
(85, 11),
(86, 11),
(87, 11),
(88, 11),
(89, 11),
(90, 11),
(91, 11),
(92, 11),
(93, 11),
(94, 11),
(95, 11),
(96, 11),
(98, 11),
(99, 11),
(100, 11),
(101, 11),
(103, 11),
(109, 11),
(110, 11),
(116, 11),
(119, 11),
(120, 11),
(121, 11),
(122, 11),
(123, 11),
(124, 11),
(125, 11),
(126, 11),
(127, 11),
(128, 11),
(129, 11),
(130, 11),
(131, 11),
(132, 11),
(133, 11),
(134, 11),
(135, 11),
(136, 11),
(137, 11),
(138, 11),
(139, 11),
(140, 11),
(141, 11),
(142, 11),
(143, 11),
(144, 11),
(233, 11),
(368, 11),
(371, 11),
(374, 11),
(375, 11),
(376, 11),
(384, 11),
(385, 11),
(386, 11),
(387, 11),
(388, 11),
(390, 11),
(391, 11),
(392, 11),
(394, 11),
(395, 11),
(396, 11),
(397, 11),
(398, 11),
(399, 11),
(400, 11),
(401, 11),
(402, 11),
(403, 11),
(404, 11),
(405, 11),
(406, 11),
(407, 11),
(408, 11),
(409, 11),
(410, 11),
(412, 11),
(413, 11),
(419, 11),
(422, 11),
(426, 11),
(428, 11),
(429, 11),
(432, 11),
(434, 11),
(438, 11),
(439, 11),
(442, 11),
(443, 11),
(445, 11),
(447, 11),
(448, 11),
(449, 11),
(450, 11),
(451, 11),
(452, 11),
(453, 11),
(455, 11),
(456, 11),
(457, 11),
(458, 11),
(459, 11),
(460, 11),
(461, 11),
(462, 11),
(465, 11),
(466, 11),
(467, 11),
(470, 11),
(471, 11),
(474, 11),
(477, 11),
(478, 11),
(479, 11),
(480, 11),
(482, 11),
(483, 11),
(484, 11),
(492, 11),
(493, 11),
(497, 11),
(499, 11),
(500, 11),
(24, 12),
(40, 12),
(102, 12),
(207, 12),
(389, 12),
(411, 12),
(425, 12),
(464, 12),
(468, 12),
(485, 12),
(486, 12),
(496, 12),
(44, 13),
(45, 13),
(46, 13),
(47, 13),
(48, 13),
(49, 13),
(51, 13),
(84, 13),
(86, 13),
(87, 13),
(93, 13),
(95, 13),
(97, 13),
(98, 13),
(104, 13),
(105, 13),
(106, 13),
(107, 13),
(108, 13),
(109, 13),
(110, 13),
(114, 13),
(115, 13),
(123, 13),
(124, 13),
(125, 13),
(126, 13),
(127, 13),
(173, 13),
(418, 13),
(440, 13),
(463, 13),
(469, 13),
(476, 13),
(491, 13),
(97, 14),
(494, 14),
(495, 14),
(64, 15),
(130, 15),
(132, 15),
(139, 15),
(140, 15),
(341, 15),
(355, 15),
(359, 15),
(360, 15),
(382, 15),
(16, 16),
(17, 16),
(108, 16),
(472, 16),
(473, 16),
(481, 16),
(490, 16),
(113, 17),
(378, 17),
(441, 17),
(444, 17),
(446, 17),
(475, 17),
(381, 18);

-- --------------------------------------------------------

--
-- Table structure for table `service_details`
--

CREATE TABLE `service_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `service` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '1',
  `created_by` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `updated_by` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `service_details`
--

INSERT INTO `service_details` (`id`, `service`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'test', 1, '1', '1', '2020-04-08 10:09:54', '2020-04-08 10:09:54', NULL),
(2, 'Demo test', 1, '1', '1', '2020-06-08 11:23:16', '2020-06-08 11:23:16', NULL),
(3, 'scsaca', 1, '1', '1', '2020-06-09 12:06:12', '2020-06-09 12:06:12', NULL),
(4, '4523', 1, '1', '1', '2020-06-09 12:07:39', '2020-06-09 12:07:39', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `set_consult_prices`
--

CREATE TABLE `set_consult_prices` (
  `id` int(10) UNSIGNED NOT NULL,
  `specialities` int(11) NOT NULL,
  `doctor` int(11) NOT NULL,
  `consult_price` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `set_consult_prices`
--

INSERT INTO `set_consult_prices` (`id`, `specialities`, `doctor`, `consult_price`, `date`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, '100', '2020-07-25 00:00:00', 64, 64, '2020-07-25 07:35:00', '2020-07-25 11:11:12', '2020-07-25 11:11:12'),
(3, 15, 51, '200', '2020-07-25 00:00:00', 64, 64, '2020-07-25 13:16:08', '2020-07-25 13:16:08', NULL),
(4, 15, 51, '50', '2020-07-22 00:00:00', 64, 64, '2020-07-25 13:17:19', '2020-07-25 13:17:43', '2020-07-25 13:17:43'),
(5, 15, 50, '1000', '2020-07-21 00:00:00', 64, 64, '2020-07-25 13:21:55', '2020-07-25 13:22:01', '2020-07-25 13:22:01'),
(6, 15, 52, '50', '2020-07-30 00:00:00', 64, 64, '2020-07-25 13:39:21', '2020-07-25 13:39:21', NULL),
(7, 1, 1, '10', '2020-07-27 00:00:00', 35, 35, '2020-07-27 17:12:43', '2020-07-27 17:12:43', NULL),
(8, 1, 1, '10', '2020-07-27 00:00:00', 64, 64, '2020-07-27 17:15:37', '2020-07-27 17:15:37', NULL),
(9, 13, 42, '10', '2020-07-26 00:00:00', 64, 64, '2020-07-27 18:20:03', '2020-07-27 18:20:03', NULL),
(10, 0, 0, '23', '2020-07-20 00:00:00', 1, 1, '2020-07-28 13:40:43', '2020-07-28 13:40:43', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `set_service_price`
--

CREATE TABLE `set_service_price` (
  `id` int(10) UNSIGNED NOT NULL,
  `service_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '1' COMMENT '0 forinactive 1 for active',
  `created_by` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `updated_by` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `set_service_price`
--

INSERT INTO `set_service_price` (`id`, `service_name`, `price`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '1', '5551', 1, '1', '1', '2020-04-08 12:32:12', '2020-06-13 05:58:37', NULL),
(2, '1', '456', 1, '1', '1', '2020-06-08 09:35:03', '2020-06-08 09:35:03', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `specilities`
--

CREATE TABLE `specilities` (
  `id` int(10) UNSIGNED NOT NULL,
  `specility` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL,
  `created_by` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `updated_by` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `specilities`
--

INSERT INTO `specilities` (`id`, `specility`, `description`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(2, 'Allergists/Immunologists', 'They treat immune system disorders such as asthma, eczema, food allergies, insect sting allergies, and some autoimmune diseases.', 1, '8', '8', '2020-04-08 13:49:03', '2020-04-08 13:49:03'),
(6, 'Critical Care Medicine Specialists', 'They care for people who are critically ill or injured, often heading intensive care units in hospitals. You might see them if your heart or other organs are failing or if you’ve been in an accident.', 1, '8', '8', '2020-04-08 13:50:51', '2020-04-08 13:50:51'),
(9, 'Emergency Medicine Specialists', 'These doctors make life-or-death decisions for sick and injured people, usually in an emergency room. Their job is to save lives and to avoid or lower the chances of disability.', 1, '8', '8', '2020-04-08 13:51:59', '2020-04-08 13:51:59'),
(10, 'Family Physicians', 'They care for the whole family, including children, adults, and the elderly. They do routine checkups and screening tests, give you flu and immunization shots, and manage diabetes and other ongoing medical conditions.', 1, '8', '8', '2020-04-08 13:52:31', '2020-04-08 13:52:31'),
(11, 'Gastroenterologists', 'They’re specialists in digestive organs, including the stomach, bowels, pancreas, liver, and gallbladder. You might see them for abdominal pain, ulcers, diarrhea, jaundice, or cancers in your digestive organs.', 1, '8', '8', '2020-04-08 13:54:13', '2020-04-08 13:54:13'),
(12, 'Andrology', 'Andrology', 1, '1', '1', '2020-08-31 12:38:08', '2020-08-31 12:38:08'),
(14, 'Bariatric Surgery', 'Bariatric Surgery', 1, '1', '1', '2020-08-31 12:39:12', '2020-08-31 12:39:12'),
(15, 'Bone Marrow Transplantation', 'Bone Marrow Transplantation', 1, '1', '1', '2020-08-31 12:39:30', '2020-08-31 12:39:30'),
(16, 'Breast Surgery', 'Breast Surgery', 1, '1', '1', '2020-08-31 12:39:46', '2020-08-31 12:39:46'),
(17, 'Cardiac Surgery', 'Cardiac Surgery', 1, '1', '1', '2020-08-31 12:40:13', '2020-08-31 12:40:13'),
(18, 'Cardiology', 'Cardiology', 1, '1', '1', '2020-08-31 12:40:29', '2020-08-31 12:40:29'),
(19, 'Cardiology Electrophysiology', 'Cardiology Electrophysiology', 1, '1', '1', '2020-08-31 12:41:01', '2020-08-31 12:41:01'),
(21, 'Critical Care', 'Critical Care', 1, '1', '1', '2020-08-31 12:41:39', '2020-08-31 12:41:39'),
(22, 'Dermatology', 'Dermatology', 1, '1', '1', '2020-08-31 12:42:14', '2020-08-31 12:42:14'),
(24, 'Dietary', 'Dietary', 1, '1', '1', '2020-08-31 12:43:22', '2020-08-31 12:43:22'),
(25, 'Emergency & Trauma', 'Emergency & Trauma', 1, '1', '1', '2020-08-31 12:43:45', '2020-08-31 12:43:45'),
(26, 'Endocrinology', 'Endocrinology', 1, '1', '1', '2020-08-31 12:44:19', '2020-08-31 12:44:19'),
(27, 'ENT', 'ENT', 1, '1', '1', '2020-08-31 12:44:34', '2020-08-31 12:44:34'),
(28, 'Family Medicine', 'Family Medicine', 1, '1', '1', '2020-08-31 12:44:51', '2020-08-31 12:44:51'),
(29, 'Fetal Medicine', 'Fetal Medicine', 1, '1', '1', '2020-08-31 12:45:09', '2020-08-31 12:45:09'),
(30, 'Gastrointestinal Oncology', 'Gastrointestinal Oncology', 1, '1', '1', '2020-08-31 12:45:51', '2020-08-31 12:45:51'),
(31, 'General Surgery', 'General Surgery', 1, '1', '1', '2020-08-31 12:46:12', '2020-08-31 12:46:12'),
(32, 'General Medicine', 'General Medicine', 1, '1', '1', '2020-08-31 12:46:34', '2020-08-31 12:46:34'),
(33, 'Head and Neck Surgery', 'Head and Neck Surgery', 1, '1', '1', '2020-08-31 12:48:13', '2020-08-31 12:48:13'),
(35, 'Hemato Oncology', 'Hemato Oncology', 1, '1', '1', '2020-08-31 12:49:16', '2020-08-31 12:49:16'),
(36, 'Hematology', 'Hematology', 1, '1', '1', '2020-08-31 12:49:59', '2020-08-31 12:49:59'),
(37, 'Hepatology', 'Hepatology', 1, '1', '1', '2020-08-31 12:50:30', '2020-08-31 12:50:30'),
(38, 'Geriatric Medicine', 'Geriatric Medicine', 1, '1', '1', '2020-08-31 12:51:27', '2020-08-31 12:51:27'),
(39, 'Immunology', 'Immunology', 1, '1', '1', '2020-08-31 12:51:48', '2020-08-31 12:51:48'),
(40, 'Infectious Diseases', 'Infectious Diseases', 1, '1', '1', '2020-08-31 12:52:12', '2020-08-31 12:52:12'),
(41, 'Internal Medicine', 'Internal Medicine', 1, '1', '1', '2020-08-31 12:52:29', '2020-08-31 12:52:29'),
(42, 'International Radiology', 'International Radiology', 1, '1', '1', '2020-08-31 12:52:49', '2020-08-31 12:52:49'),
(43, 'IVF and Reproductive Medicine', 'IVF and Reproductive Medicine', 1, '1', '1', '2020-08-31 12:53:04', '2020-08-31 12:53:04'),
(44, 'Joint Replacement', 'Joint Replacement', 1, '1', '1', '2020-08-31 12:53:26', '2020-08-31 12:53:26'),
(45, 'Laparoscopic Surgery', 'Laparoscopic Surgery', 1, '1', '1', '2020-08-31 12:53:44', '2020-08-31 12:53:44'),
(46, 'Liver Transplantation', 'Liver Transplantation', 1, '1', '1', '2020-08-31 12:54:20', '2020-08-31 12:54:20'),
(47, 'Medical Oncology', 'Medical Oncology', 1, '1', '1', '2020-08-31 12:54:37', '2020-08-31 12:54:37'),
(48, 'Neonatology', 'Neonatology', 1, '1', '1', '2020-08-31 12:54:57', '2020-08-31 12:54:57'),
(49, 'Nephrology', 'Nephrology', 1, '1', '1', '2020-08-31 12:55:24', '2020-08-31 12:55:24'),
(50, 'Neurology', 'Neurology', 1, '1', '1', '2020-08-31 12:55:47', '2020-08-31 12:55:47'),
(51, 'Neuroradiology', 'Neuroradiology', 1, '1', '1', '2020-08-31 12:56:05', '2020-08-31 12:56:05'),
(52, 'Neurosurgery', 'Neurosurgery', 1, '1', '1', '2020-08-31 12:56:23', '2020-08-31 12:56:23'),
(53, 'Nuclear Medicine', 'Nuclear Medicine', 1, '1', '1', '2020-08-31 12:56:43', '2020-08-31 12:56:43'),
(54, 'Obstetrics & Gynecology', 'Obstetrics & Gynecology', 1, '1', '1', '2020-08-31 12:56:59', '2020-08-31 12:56:59'),
(55, 'Ophthalmology', 'Ophthalmology', 1, '1', '1', '2020-08-31 12:57:19', '2020-08-31 12:57:19'),
(56, 'Orthopedics', 'Orthopedics', 1, '1', '1', '2020-08-31 12:57:37', '2020-08-31 12:57:37'),
(57, 'Pain Management', 'Pain Management', 1, '1', '1', '2020-08-31 12:57:54', '2020-08-31 12:57:54'),
(58, 'Pathology', 'Pathology', 1, '1', '1', '2020-08-31 12:58:12', '2020-08-31 12:58:12'),
(59, 'Pediatric Cardiac Surgery', 'Pediatric Cardiac Surgery', 1, '1', '1', '2020-08-31 12:58:31', '2020-08-31 12:58:31'),
(60, 'Pediatric Cardiology', 'Pediatric Cardiology', 1, '1', '1', '2020-08-31 12:58:48', '2020-08-31 12:58:48'),
(61, 'Pediatric Endocrinology', 'Pediatric Endocrinology', 1, '1', '1', '2020-08-31 12:59:06', '2020-08-31 12:59:06'),
(62, 'Aesthetic & Reconstructive Surgery', 'Aesthetic & Reconstructive Surgery', 1, '1', '1', '2020-08-31 13:00:08', '2020-08-31 13:00:08'),
(63, 'demo1', 'demo', 1, '341', '341', '2020-10-10 14:01:12', '2020-10-10 14:01:12');

-- --------------------------------------------------------

--
-- Table structure for table `state`
--

CREATE TABLE `state` (
  `id` int(10) UNSIGNED NOT NULL,
  `state` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '1' COMMENT '0 for inactive 1 for active',
  `created_by` smallint(6) NOT NULL,
  `updated_by` smallint(6) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `state`
--

INSERT INTO `state` (`id`, `state`, `country_id`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Gujarat', '3', 1, 1, 1, '2020-03-28 03:02:19', '2020-07-15 18:08:59', NULL),
(2, 'Punjab', '3', 1, 1, 1, '2020-04-07 01:31:56', '2020-09-01 13:57:44', NULL),
(3, 'Kerala', '3', 1, 1, 1, '2020-04-10 09:45:53', '2020-05-28 06:17:17', NULL),
(4, 'Bihar', '3', 1, 1, 1, '2020-06-08 09:23:58', '2020-06-08 09:23:58', NULL),
(5, 'Delhi', '3', 1, 1, 1, '2020-07-27 20:22:40', '2020-07-27 20:22:40', NULL),
(6, 'Andhra Pradesh', '3', 1, 1, 1, '2020-08-31 14:02:52', '2020-09-01 13:57:04', NULL),
(7, 'Assam', '3', 1, 1, 1, '2020-08-31 14:03:30', '2020-08-31 14:03:30', NULL),
(8, 'Chandigarh', '3', 1, 1, 1, '2020-08-31 14:14:14', '2020-08-31 14:14:14', NULL),
(9, 'Chhattisgarh', '3', 1, 1, 1, '2020-08-31 14:14:46', '2020-08-31 14:14:46', NULL),
(10, 'Goa', '3', 1, 1, 1, '2020-08-31 14:15:23', '2020-08-31 14:15:23', NULL),
(11, 'Haryana', '3', 1, 1, 1, '2020-08-31 14:15:58', '2020-08-31 14:15:58', NULL),
(12, 'Jharkhand', '3', 1, 1, 1, '2020-08-31 14:16:31', '2020-08-31 14:16:31', NULL),
(13, 'Karnataka', '3', 1, 1, 1, '2020-08-31 14:16:50', '2020-08-31 14:16:50', NULL),
(14, 'Ladakh', '3', 1, 1, 1, '2020-08-31 14:17:20', '2020-08-31 14:17:20', NULL),
(15, 'Lakshdweep', '3', 1, 1, 1, '2020-08-31 14:17:42', '2020-08-31 14:17:42', NULL),
(16, 'Maharashtra', '3', 1, 1, 1, '2020-08-31 14:18:06', '2020-08-31 14:18:06', NULL),
(17, 'Manipur', '3', 1, 1, 1, '2020-08-31 14:18:29', '2020-08-31 14:18:29', NULL),
(18, 'Meghalya', '3', 1, 1, 1, '2020-08-31 14:18:48', '2020-08-31 14:18:48', NULL),
(19, 'Mizoram', '3', 1, 1, 1, '2020-08-31 14:19:16', '2020-08-31 14:19:16', NULL),
(20, 'Nagaland', '3', 1, 1, 1, '2020-08-31 14:19:35', '2020-08-31 14:19:35', NULL),
(21, 'Odisha', '3', 1, 1, 1, '2020-08-31 14:19:55', '2020-08-31 14:19:55', NULL),
(22, 'Puducherry', '3', 1, 1, 1, '2020-08-31 14:20:12', '2020-08-31 14:20:12', NULL),
(23, 'Rajasthan', '3', 1, 1, 1, '2020-08-31 14:20:37', '2020-08-31 14:20:37', NULL),
(24, 'Sikkim', '3', 1, 1, 1, '2020-08-31 14:20:56', '2020-08-31 14:20:56', NULL),
(25, 'Tamilnadu', '3', 1, 1, 1, '2020-08-31 14:21:27', '2020-08-31 14:21:27', NULL),
(26, 'Telangana', '3', 1, 1, 1, '2020-08-31 14:21:49', '2020-09-01 14:00:30', NULL),
(27, 'Tripura', '3', 1, 1, 1, '2020-08-31 14:22:09', '2020-08-31 14:22:09', NULL),
(28, 'Uttrakhand', '3', 1, 1, 1, '2020-08-31 14:22:31', '2020-08-31 14:22:31', NULL),
(29, 'West Bengal', '3', 1, 1, 1, '2020-08-31 14:41:45', '2020-08-31 14:41:45', NULL),
(30, 'Uttar Pradesh', '3', 1, 1, 1, '2020-08-31 14:43:55', '2020-08-31 14:43:55', NULL),
(31, 'Madhya Pradesh', '3', 1, 1, 1, '2020-08-31 14:44:14', '2020-08-31 14:44:14', NULL),
(32, 'Jammu and Kashmir', '3', 1, 1, 1, '2020-08-31 14:44:34', '2020-08-31 14:44:34', NULL),
(33, 'Himachal Pradesh', '3', 1, 1, 1, '2020-08-31 14:44:53', '2020-08-31 14:44:53', NULL),
(34, 'Telangana', '3', 1, 1, 1, '2020-08-31 18:10:34', '2020-09-01 14:00:12', '2020-09-01 14:00:12'),
(35, 'Andaman', '3', 1, 1, 1, '2020-08-31 18:11:17', '2020-08-31 18:11:17', NULL),
(36, 'Arunachal Pradesh', '3', 1, 1, 1, '2020-08-31 18:11:41', '2020-08-31 18:11:41', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `suggested_imagings_patient`
--

CREATE TABLE `suggested_imagings_patient` (
  `id` int(10) UNSIGNED NOT NULL,
  `si_patient_id` int(11) NOT NULL,
  `si_doctor_id` int(11) NOT NULL,
  `si_book_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `suggested_imagings` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `suggested_imagings_patient`
--

INSERT INTO `suggested_imagings_patient` (`id`, `si_patient_id`, `si_doctor_id`, `si_book_id`, `suggested_imagings`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 19, 1, 'book-00001', 'tes data', 35, 35, '2020-05-15 12:40:08', '2020-05-15 12:40:19', NULL),
(2, 19, 1, 'book-0000010', 'test imagine1', 35, 35, '2020-05-16 04:43:07', '2020-05-16 04:43:17', NULL),
(3, 19, 1, 'book-0000011', 'imagine', 35, 35, '2020-05-16 12:41:16', '2020-05-16 12:41:16', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `suggested_investigations_patient`
--

CREATE TABLE `suggested_investigations_patient` (
  `id` int(10) UNSIGNED NOT NULL,
  `s_patient_id` int(11) NOT NULL,
  `s_doctor_id` int(11) NOT NULL,
  `s_book_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `suggested_investigations` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `suggested_investigations_patient`
--

INSERT INTO `suggested_investigations_patient` (`id`, `s_patient_id`, `s_doctor_id`, `s_book_id`, `suggested_investigations`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 19, 1, 'book-000001', 'test data 1', 35, 35, '2020-05-16 10:51:47', '2020-05-16 10:51:58', NULL),
(2, 19, 1, 'book-0000011', 'test allergies1', 35, 35, '2020-05-16 12:39:07', '2020-05-16 12:39:14', NULL),
(3, 208, 144, 'book-0000075', 'dust and pollens', 309, 309, '2020-09-27 16:55:05', '2020-09-27 16:55:05', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `suggested_specialists_patient`
--

CREATE TABLE `suggested_specialists_patient` (
  `id` int(10) UNSIGNED NOT NULL,
  `ss_patient_id` int(11) NOT NULL,
  `ss_doctor_id` int(11) NOT NULL,
  `ss_book_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `suggested_specialists` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `suggested_specialists_patient`
--

INSERT INTO `suggested_specialists_patient` (`id`, `ss_patient_id`, `ss_doctor_id`, `ss_book_id`, `suggested_specialists`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 19, 1, 'book-00001', 'test data dd', 35, 35, '2020-05-16 11:31:42', '2020-05-16 11:31:57', NULL),
(2, 19, 1, 'book-0000011', 'labss report', 35, 35, '2020-05-16 12:41:08', '2020-05-16 12:41:08', NULL),
(3, 254, 160, '91-B-000000163', '.', 383, 383, '2020-11-08 00:53:14', '2020-11-08 00:53:14', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `suggested_therapies_patient`
--

CREATE TABLE `suggested_therapies_patient` (
  `id` int(10) UNSIGNED NOT NULL,
  `st_patient_id` int(11) NOT NULL,
  `st_doctor_id` int(11) NOT NULL,
  `st_book_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `suggested_therapies` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `suggested_therapies_patient`
--

INSERT INTO `suggested_therapies_patient` (`id`, `st_patient_id`, `st_doctor_id`, `st_book_id`, `suggested_therapies`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 19, 1, 'book-00001', 'tet data', 35, 35, '2020-05-16 04:51:23', '2020-05-16 04:51:23', NULL),
(2, 19, 1, 'book-0000010', 'refer1', 35, 35, '2020-05-16 05:37:18', '2020-05-16 05:37:33', NULL),
(3, 19, 1, 'book-0000011', 'refer', 35, 35, '2020-05-16 12:59:32', '2020-05-16 12:59:32', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `support`
--

CREATE TABLE `support` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ticket_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `doctor_id` int(11) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `support`
--

INSERT INTO `support` (`id`, `title`, `ticket_no`, `description`, `doctor_id`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Test', '1234', 'test', NULL, 35, 35, '2020-05-07 06:10:07', '2020-05-07 06:10:07', NULL),
(2, 'abcd', '987', 'test', 1, 35, 35, '2020-05-07 06:12:06', '2020-05-07 06:12:06', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `task`
--

CREATE TABLE `task` (
  `id` int(10) UNSIGNED NOT NULL,
  `patient_id` int(11) NOT NULL,
  `issue` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `patient_contactinfo` int(11) DEFAULT '2',
  `patient_addressinfo` int(11) DEFAULT '2',
  `status` int(11) NOT NULL COMMENT '1 for padding, 2 for assign,3 for done,',
  `operator_id` int(11) DEFAULT NULL,
  `register_date` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `task`
--

INSERT INTO `task` (`id`, `patient_id`, `issue`, `patient_contactinfo`, `patient_addressinfo`, `status`, `operator_id`, `register_date`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 256, NULL, 2, 2, 3, 381, '2020-11-02 19:02:25', '2020-11-03 13:09:27', '2020-11-06 22:01:39', NULL),
(2, 257, NULL, 2, 1, 2, 381, '2020-11-03 18:18:35', '2020-11-03 19:00:01', '2020-11-05 18:19:46', NULL),
(3, 258, NULL, 1, 1, 1, NULL, '2020-11-20 10:21:56', '2020-11-20 19:00:01', '2020-11-20 19:00:01', NULL),
(4, 259, NULL, 1, 1, 1, NULL, '2020-11-22 12:39:06', '2020-11-22 19:00:02', '2020-11-22 19:00:02', NULL),
(5, 259, NULL, 1, 1, 1, NULL, '2020-11-22 12:39:06', '2020-11-22 19:00:02', '2020-11-22 19:00:02', NULL),
(6, 259, NULL, 1, 1, 1, NULL, '2020-11-22 12:39:06', '2020-11-22 19:00:02', '2020-11-22 19:00:02', NULL),
(7, 263, NULL, 1, 1, 1, NULL, '2020-11-25 15:03:58', '2020-11-25 19:00:01', '2020-11-25 19:00:01', NULL),
(8, 268, NULL, 2, 2, 3, NULL, '2020-12-15 16:59:00', '2020-12-15 19:00:02', '2020-12-15 19:00:02', NULL),
(9, 269, NULL, 1, 1, 1, NULL, '2020-12-25 00:17:28', '2020-12-25 19:00:01', '2020-12-25 19:00:01', NULL),
(10, 270, NULL, 1, 1, 1, NULL, '2021-01-07 15:58:48', '2021-01-07 19:00:02', '2021-01-07 19:00:02', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tax`
--

CREATE TABLE `tax` (
  `id` int(10) UNSIGNED NOT NULL,
  `tax_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tax_percentage` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tax`
--

INSERT INTO `tax` (`id`, `tax_name`, `tax_percentage`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'test', '12', 0, 0, '2020-09-22 12:53:39', '2020-09-22 12:53:39', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `temp_hasaptal_data`
--

CREATE TABLE `temp_hasaptal_data` (
  `id` int(11) NOT NULL,
  `otp` int(11) NOT NULL,
  `data` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `temp_hasaptal_data`
--

INSERT INTO `temp_hasaptal_data` (`id`, `otp`, `data`) VALUES
(4, 6059, '{\"_token\":\"NiFnOLqQRna1vdb1yFhfQH9Fx0445Grs6hxKrl2z\",\"country_id\":\"3\",\"state_id\":\"5\",\"city_id\":\"52\",\"pincode_id\":\"110070\",\"hospital_name\":\"V-1234\",\"address\":\"V-1234\",\"year_of_establishment\":\"2016\",\"gst_no\":\"36373737\",\"email\":\"opmgr.haspatal@gmail.com\",\"mobile\":\"09643725091\",\"contact_person\":\"Dhdh\",\"contact_person_email\":\"ogr.haspatal@gmail.com\",\"contact_person_phonoe_no\":\"999999999\",\"other_phone_no\":\"9999996666\",\"support_watsapp_no\":\"66666666666\",\"telemedicine_coordinator\":\"Jsjsj\",\"otp\":\"6059\",\"tearm_and_condition\":\"on\",\"license_copy\":\"1604581342__Screenshot_20201031-161709.png\",\"hospital_profile\":\"1604581342__Screenshot_20201031-182134.png\",\"hospital_location\":\"1604581342__Screenshot_20201031-161709.png\"}'),
(5, 6059, '{\"_token\":\"NiFnOLqQRna1vdb1yFhfQH9Fx0445Grs6hxKrl2z\",\"country_id\":\"3\",\"pincode_id\":\"110070\",\"hospital_name\":\"V-1234\",\"address\":\"V-1234\",\"year_of_establishment\":\"2016\",\"gst_no\":\"36373737\",\"email\":\"opmgr.haspatal@gmail.com\",\"mobile\":\"9643725091\",\"contact_person\":\"Dhdh\",\"contact_person_email\":\"ogr.haspatal@gmail.com\",\"contact_person_phonoe_no\":\"999999999\",\"other_phone_no\":\"9999996666\",\"support_watsapp_no\":\"66666666666\",\"telemedicine_coordinator\":\"Jsjsj\",\"otp\":\"6059\",\"tearm_and_condition\":\"on\",\"license_copy\":\"1604581385__Screenshot_20201031-161709.png\",\"hospital_profile\":\"1604581385__Screenshot_20201031-182134.png\",\"hospital_location\":\"1604581385__Screenshot_20201031-161709.png\"}');

-- --------------------------------------------------------

--
-- Table structure for table `therapies_list`
--

CREATE TABLE `therapies_list` (
  `id` int(10) UNSIGNED NOT NULL,
  `dr_id` int(11) NOT NULL,
  `therapy_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `therapies_list`
--

INSERT INTO `therapies_list` (`id`, `dr_id`, `therapy_name`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 114, 'ddddddd', 0, 0, '2020-10-03 17:47:22', '2020-10-03 17:49:37', '2020-10-03 17:49:37'),
(2, 0, 'test therapy', 307, 307, '2020-10-03 19:12:18', '2020-10-03 19:12:18', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `time_slot`
--

CREATE TABLE `time_slot` (
  `id` int(10) UNSIGNED NOT NULL,
  `doctor_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `booking_time_slot` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mrg_from_time` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mrg_to_time` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `aft_from_time` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `aft_to_time` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `eve_from_time` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `eve_to_time` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` smallint(6) NOT NULL DEFAULT '1' COMMENT '0 for inactive 1 for active',
  `created_by` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `updated_by` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `time_slot`
--

INSERT INTO `time_slot` (`id`, `doctor_id`, `booking_time_slot`, `mrg_from_time`, `mrg_to_time`, `aft_from_time`, `aft_to_time`, `eve_from_time`, `eve_to_time`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, '1', '30', '11:30', '15:01', '13:00', '18:00', '18:00', '22:00', 1, '35', '35', '2020-04-13 08:41:31', '2020-08-05 19:51:57'),
(2, '6', '1', '09', '10', '15', '16', '17', '19', 1, '40', '40', '2020-04-13 09:10:12', '2020-04-13 09:10:12'),
(3, '7', '15', '09:30', '12:30', '02:30', '05:30', '06:00', '09:00', 1, '41', '41', '2020-04-14 10:45:32', '2020-04-25 05:03:44'),
(4, '12', '20', '8', '12', '15', '18', '20', '21', 1, '60', '60', '2020-04-22 13:36:07', '2020-04-22 13:36:07'),
(5, '13', '30', '09:00', '12:00', '14:00', '17:00', '19:00', '20:00', 1, '61', '61', '2020-04-24 11:31:08', '2020-04-24 11:31:08'),
(9, '15', '30', '08:30', '12:00', '15:00', '18:00', '19:00', '21:00', 1, '70', '70', '2020-05-28 10:12:53', '2020-05-28 10:12:53'),
(10, '17', '30', '09', '12', '15', '18', '20', '23', 1, '72', '72', '2020-05-28 10:14:04', '2020-05-28 10:14:04'),
(14, '11', '09:30', '12:00', '05:30', '06:00', '09:00', '03:00', '06:00', 1, '35', '35', '2020-05-30 12:03:52', '2020-05-30 12:09:00'),
(15, '38', '30', '14:45', '10:55', '14:00', '00:00', '19:00', '23:57', 1, '114', '114', '2020-06-22 09:08:40', '2020-06-22 09:08:40'),
(16, '39', '30', '00:01', '11:59', '12:00', '17:59', '18:00', '23:59', 1, '122', '122', '2020-07-05 18:20:28', '2020-07-05 18:53:26'),
(17, '65', '20', '09:00', '11:00', '12:00', '13:15', '15:00', '17:30', 1, '168', '168', '2020-07-30 20:26:34', '2020-07-30 20:33:32'),
(18, '67', '30', '11:06', '06:06', '06:06', '17:07', '23:07', '14:07', 1, '172', '172', '2020-08-15 12:37:12', '2020-08-15 12:37:12'),
(19, '51', '20', '09:00', '11:30', '14:00', '16:00', '17:00', '19:00', 1, '153', '153', '2020-08-20 15:15:05', '2020-08-20 15:15:05'),
(20, '91', '10', '06:00', '10:00', '13:00', '16:00', '19:00', '23:00', 1, '238', '238', '2020-08-24 15:08:36', '2020-09-17 14:54:19'),
(21, '92', '20', '10:00', '12:00', '02:00', '05:00', '06:00', '08:00', 1, '241', '241', '2020-08-27 11:39:23', '2020-08-27 11:39:23'),
(22, '93', '25', '09:00', '12:00', '03:00', '05:00', '06:00', '08:30', 1, '247', '247', '2020-08-28 20:38:51', '2020-08-28 20:38:51'),
(23, '94', '15', '07:00', '10:00', '12:00', '14:00', '15:00', '18:00', 1, '248', '248', '2020-08-29 22:11:20', '2020-08-29 22:11:20'),
(24, '97', '15', '08:30', '10:30', '02:30', '05:30', '05:30', '08:30', 1, '250', '250', '2020-08-30 19:49:02', '2020-08-30 19:49:02'),
(25, '95', '45', '09:30', '12:30', '02:00', '05:00', '06:00', '08:00', 1, '249', '249', '2020-08-31 13:03:44', '2020-08-31 13:03:44'),
(26, '104', '5', '12:00', '15:00', '15:00', '18:00', '18:00', '22:00', 1, '254', '254', '2020-08-31 16:33:28', '2020-08-31 16:33:28'),
(31, '103', '12', '07:00', '11:00', '13:00', '16:00', '18:00', '22:00', 1, '253', '253', '2020-08-31 16:51:16', '2020-09-05 15:14:35'),
(32, '121', '30', '10:00', '12:00', '13:00', '15:00', '18:00', '20:00', 1, '274', '274', '2020-09-08 13:11:32', '2020-09-08 13:11:32'),
(34, '125', '30', '08:30', '12:00', '03:00', '05:00', '06:00', '08:00', 1, '280', '280', '2020-09-09 18:33:34', '2020-09-09 18:33:34'),
(35, '128', '15', '10:30', '13:30', '15:30', '18:30', '19:30', '21:00', 1, '283', '283', '2020-09-10 11:58:06', '2020-09-11 09:46:48'),
(37, '114', '5', '08:00', '10:00', '14:00', '16:00', '18:00', '20:00', 1, '266', '266', '2020-09-10 12:42:24', '2020-10-25 14:51:25'),
(38, '132', '6', '05:00', '06:00', '12:00', '13:00', '18:00', '19:00', 1, '287', '287', '2020-09-10 20:13:40', '2020-09-10 20:13:40'),
(39, '131', '15', '06:00', '08:00', '13:00', '15:00', '20:00', '22:00', 1, '286', '286', '2020-09-10 20:28:12', '2020-09-14 15:04:54'),
(40, '135', '15', '08:52', '08:52', '06:52', '06:52', '09:53', '09:53', 1, '291', '291', '2020-09-11 10:53:45', '2020-09-11 10:53:45'),
(41, '138', '15', '08:00', '12:00', '13:00', '16:00', '18:00', '21:00', 1, '302', '302', '2020-09-11 17:51:04', '2020-09-11 17:51:04'),
(42, '140', '10', '08:00', '11:00', '15:00', '18:00', '18:00', '21:00', 1, '304', '304', '2020-09-16 23:08:07', '2020-09-17 00:09:18'),
(43, '142', '30', '05:00', '10:00', '13:00', '15:00', '18:00', '23:00', 1, '307', '307', '2020-09-17 15:14:33', '2020-10-25 14:45:23'),
(44, '145', '5', '08:00', '11:00', '12:00', '18:00', '20:00', '23:00', 1, '310', '310', '2020-09-18 23:32:02', '2020-09-24 17:56:35'),
(45, '144', '15', '09:00', '12:00', '12:00', '16:00', '18:00', '22:00', 1, '309', '309', '2020-09-19 17:39:42', '2020-09-23 01:20:02'),
(46, '133', '5', '07:00', '08:00', '15:00', '16:00', '19:00', '20:00', 1, '288', '288', '2020-09-21 17:18:13', '2020-09-21 17:18:13'),
(47, '136', '5', '10:00', '12:00', '12:00', '18:00', '19:00', '22:00', 1, '300', '300', '2020-10-01 17:31:29', '2020-10-21 14:00:04'),
(48, '157', '4', '10:00', '12:00', '12:00', '18:00', '19:00', '23:00', 1, '356', '356', '2020-10-14 12:51:30', '2020-10-23 17:52:34'),
(49, '160', '30', '06:00', '12:00', '12:00', '04:00', '04:00', '08:00', 1, '383', '383', '2020-11-06 21:52:37', '2020-11-08 00:45:36');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `username` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_name` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `uid` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plan_id` int(11) DEFAULT NULL COMMENT '1 for free, 2 for premium',
  `email` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `version` int(11) NOT NULL DEFAULT '0',
  `device` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0 for active 1 for deactive',
  `role_id` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_type` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '1 for doctor,2 for patiet,3 for hospital, 4 for Haspatal_360_register',
  `userDetailsId` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `patient_login_status` int(11) DEFAULT NULL,
  `patient_login_status_contactinfo` tinyint(4) DEFAULT NULL,
  `patient_login_status_addressinfo` tinyint(4) DEFAULT NULL,
  `doctor_login_status` int(11) DEFAULT NULL COMMENT '0 = New Login , 1 = Old Login',
  `doctor_login_status_mobile` tinyint(4) NOT NULL DEFAULT '0',
  `doctor_plan_status` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `mobile2` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `first_name`, `last_name`, `uid`, `plan_id`, `email`, `mobile`, `email_verified_at`, `password`, `remember_token`, `version`, `device`, `mobile_token`, `status`, `role_id`, `user_type`, `userDetailsId`, `patient_login_status`, `patient_login_status_contactinfo`, `patient_login_status_addressinfo`, `doctor_login_status`, `doctor_login_status_mobile`, `doctor_plan_status`, `created_at`, `updated_at`, `mobile2`) VALUES
(117, 'admin', NULL, NULL, NULL, NULL, 'admin@digitintech.com', '8281478767', NULL, 'admin@123', NULL, 0, NULL, NULL, 1, '21', '0', NULL, NULL, NULL, NULL, NULL, 0, 0, '2021-01-18 16:50:31', '2021-01-18 16:50:31', NULL),
(119, NULL, 'absal', NULL, NULL, NULL, 'absalnikki@gmail.com', '+91-9711479333', NULL, '$2y$10$qTJOIJ1C6Wo2hvKIMJXZc.72GUzJD0K9kegJoONQmDNjZsA.qIqz6', NULL, 0, NULL, NULL, 1, '11', '4', '273', NULL, NULL, NULL, NULL, 0, 0, '2021-01-18 23:46:19', '2021-01-18 23:46:19', NULL),
(120, NULL, 'ashik', NULL, NULL, NULL, 'ashik@gmail.com', '+91-9633631592', NULL, '$2y$10$b1zBJlkvf/hX7UQF4rHjPeevDxfglaUjfpANd.t.4g4rt6u.HNC8.', NULL, 0, NULL, NULL, 1, '11', '4', '274', NULL, NULL, NULL, NULL, 0, 0, '2021-01-18 23:53:51', '2021-01-18 23:53:51', NULL),
(121, NULL, 'Ashik M', NULL, NULL, NULL, 'ashikmannorayil@gmail.com', '+91-9746043286', NULL, '$2y$10$9iMa31jjXTFVyoE6pHi6.OwjPi/h1lUw6P16l47fQj9b.w/7zq5QG', NULL, 0, NULL, NULL, 1, '11', '4', '275', NULL, NULL, NULL, NULL, 0, 0, '2021-01-20 19:16:31', '2021-01-20 19:16:31', NULL),
(125, NULL, 'nxndndjnsnd', NULL, NULL, NULL, 'sandeepraj.rj5@gmail.com', '+91-7972185820', NULL, '$2y$10$02JDsi1PNUlMuuH.5AYmH.YXoQ2WOm3Vb6CQgBzQId1M1mBgGvjqu', NULL, 0, NULL, NULL, 1, '11', '4', '279', NULL, NULL, NULL, NULL, 0, 0, '2021-01-20 22:07:39', '2021-01-20 22:07:39', NULL),
(126, NULL, 'radhika', NULL, NULL, NULL, 'radhikagulati50@gmail.com', '+91-7291033392', NULL, '$2y$10$57fJSi5YWvJ/wn5xdYrkO.FitKeQEF0xwMEMDJQVPNBclEUjfOoYC', NULL, 0, NULL, NULL, 1, '11', '4', '280', NULL, NULL, NULL, NULL, 0, 0, '2021-01-20 22:12:46', '2021-01-20 22:12:46', NULL),
(130, NULL, 'ashik manoorayil', NULL, NULL, NULL, 'mastores01@gmail.com', '+91-7994646416', NULL, '$2y$10$HROLSuw0I7tGcf01KyskSe6dLADwlB0P.3bg5FadZleX7t1Oppe/C', NULL, 0, NULL, NULL, 1, '11', '4', '284', NULL, NULL, NULL, NULL, 0, 0, '2021-01-21 05:30:32', '2021-01-21 05:30:32', NULL),
(131, NULL, 'haspatal', NULL, NULL, NULL, 'f@gmail.com', '91-828282', NULL, '$2y$10$yRSAnakRa0urymXGs6cNL.UB0FFKuHdYvQ40ZXgzMYfxRPITd3o2q', NULL, 0, NULL, NULL, 1, '11', '4', '285', NULL, NULL, NULL, NULL, 0, 0, '2021-01-21 16:39:05', '2021-01-21 16:39:05', NULL),
(132, NULL, 'test', NULL, NULL, NULL, 't', '91-t', NULL, '$2y$10$./FEQc/MGIjigo5ALJHNGOlHMXPuGwOkaX8h5u1d50RopZCUHK4.W', NULL, 0, NULL, NULL, 1, '11', '4', '286', NULL, NULL, NULL, NULL, 0, 0, '2021-01-21 16:49:03', '2021-01-21 16:49:03', NULL),
(133, NULL, 'u', NULL, NULL, NULL, 'u', '91-u', NULL, '$2y$10$anc/N3NqoNSzSy0V0F9pnug.sdWOCXvRiRS4jNLoDg5F6PspK76W2', NULL, 0, NULL, NULL, 1, '11', '4', '287', NULL, NULL, NULL, NULL, 0, 0, '2021-01-21 16:50:12', '2021-01-21 16:50:12', NULL),
(134, NULL, 'althaf', NULL, NULL, NULL, 'ghhh@gmail.com', '91-636363636355', NULL, '$2y$10$UonbZ.JSzpgXQsFbDGqu5ePrWylF/2nvON5MAgoh0OsGHhDikuAWa', NULL, 0, NULL, NULL, 1, '11', '4', '288', NULL, NULL, NULL, NULL, 0, 0, '2021-01-21 17:16:25', '2021-01-21 17:16:25', NULL),
(135, NULL, 'y', NULL, NULL, NULL, 'y', '91-y', NULL, '$2y$10$rxhWzTeZ6o4IwO0bGqnB5u28w.g78yLeDuDPtffxTQLlUMLeGkCbW', NULL, 0, NULL, NULL, 1, '11', '4', '289', NULL, NULL, NULL, NULL, 0, 0, '2021-01-21 17:22:35', '2021-01-21 17:22:35', NULL),
(136, NULL, 'undefined', NULL, NULL, NULL, 'undefined', '91-undefined', NULL, '$2y$10$7cCZxebXshM8RaP8MES5veCmGRWiLvw.yt5RruMmAX3C0yIwPh8HS', NULL, 0, NULL, NULL, 1, '11', '4', '290', NULL, NULL, NULL, NULL, 0, 0, '2021-01-21 17:25:27', '2021-01-21 17:25:27', NULL),
(137, NULL, 'bbbh', NULL, NULL, NULL, 'bdhshh@gmail.com', '91-53663636363-', NULL, '$2y$10$JyfZ5/.LUczQoFcRvC3oYujFBWAbXSB9d28dcDkd01EXeJFKcRpHq', NULL, 0, NULL, NULL, 1, '11', '4', '291', NULL, NULL, NULL, NULL, 0, 0, '2021-01-21 17:26:33', '2021-01-21 17:26:33', NULL),
(138, NULL, 'Ajay', NULL, NULL, NULL, 'ajay@gmail.com', '91-8281478767', NULL, '$2y$10$q7yMRKNzv57vAEHQcZcspes4iF6UiuK9B8W1hte3FXXeMqsyTgLeW', NULL, 0, NULL, NULL, 1, '11', '4', '292', NULL, NULL, NULL, NULL, 0, 0, '2021-01-21 17:47:07', '2021-01-21 17:47:07', NULL),
(139, NULL, 'Ajay', NULL, NULL, NULL, 'aj@gmail.com', '91-8281478767', NULL, '$2y$10$A0tsJSDDlRYinwP10LcSqOyXoDhtMuxFngyptpoiH6aBdTdyxk/ie', NULL, 0, NULL, NULL, 1, '11', '4', '293', NULL, NULL, NULL, NULL, 0, 0, '2021-01-21 17:48:37', '2021-01-21 17:48:37', NULL),
(140, NULL, 'h', NULL, NULL, NULL, 'h', '91-h', NULL, '$2y$10$UD4pzV6I7Yrte1M9C2PyLuJBSWK.eIrAqlrj9eULkyaAzUqcB50Aa', NULL, 0, NULL, NULL, 1, '11', '4', '294', NULL, NULL, NULL, NULL, 0, 0, '2021-01-21 17:56:01', '2021-01-21 17:56:01', NULL),
(141, NULL, 'hh', NULL, NULL, NULL, 'hh', '91-hh', NULL, '$2y$10$AmHemYODo3PJd0TkbUIAO.EtXI3QpmU0wHUd2w0MSrgQ1lv7orMWm', NULL, 0, NULL, NULL, 1, '11', '4', '295', NULL, NULL, NULL, NULL, 0, 0, '2021-01-21 18:18:29', '2021-01-21 18:18:29', NULL),
(142, NULL, 'test', NULL, NULL, NULL, 't', '91-88', NULL, '$2y$10$0g/AZXXDSXPnLeaWJV.2I.BuEOjW.ugnlmr20YFM.WLuKbnmvyT06', NULL, 0, NULL, NULL, 1, '11', '4', '296', NULL, NULL, NULL, NULL, 0, 0, '2021-01-21 18:58:26', '2021-01-21 18:58:26', NULL),
(143, NULL, 'Ajaysankar', NULL, NULL, NULL, 'ajays@gmail.com', '91-8281478767', NULL, '$2y$10$lQnOAuOblxg1BNGANvemmOGsehN9ts1UvM8mH7/4dZyQodIHnaYoy', NULL, 0, NULL, NULL, 1, '11', '4', '297', NULL, NULL, NULL, NULL, 0, 0, '2021-01-21 19:04:00', '2021-01-21 19:04:00', NULL),
(144, NULL, 'ajay', NULL, NULL, NULL, '789@gmail.com', '91-789', NULL, '$2y$10$aSYc5wVE5brozzd6z9ptR..D4mB0eBTzvE6WSuWENpIVobfsW2A5C', NULL, 0, NULL, NULL, 1, '11', '4', '298', NULL, NULL, NULL, NULL, 0, 0, '2021-01-21 19:12:29', '2021-01-21 19:12:29', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `wallet_360`
--

CREATE TABLE `wallet_360` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `payment_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `wa_amount` int(11) DEFAULT NULL,
  `cut_amount` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_amount` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wallet_360`
--

INSERT INTO `wallet_360` (`id`, `user_id`, `payment_id`, `wa_amount`, `cut_amount`, `total_amount`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 97, '1234568', 600, NULL, NULL, NULL, 97, 97, '2020-06-09 12:53:32', '2020-06-09 12:54:13', NULL),
(2, 11, 'pay_F1CbBWKIIh0hTc', 600, NULL, NULL, NULL, 97, 97, '2020-06-11 05:28:42', '2020-06-11 05:30:13', NULL),
(3, 20, 'pay_F5FVuP4QMOmxLe', 5200, NULL, NULL, NULL, 109, 109, '2020-06-21 10:57:19', '2020-06-21 10:57:19', NULL),
(4, 27, 'pay_FLSmPFONpOpHdT', 100, NULL, NULL, NULL, 126, 126, '2020-08-01 17:20:00', '2020-08-01 17:20:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `wallet_360_transaction`
--

CREATE TABLE `wallet_360_transaction` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `wa_amount` int(11) DEFAULT NULL,
  `cut_amount` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Your_coverage_360`
--

CREATE TABLE `Your_coverage_360` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `c_state_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `c_city_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `c_pincode` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `Your_coverage_360`
--

INSERT INTO `Your_coverage_360` (`id`, `user_id`, `c_state_id`, `c_city_id`, `c_pincode`, `created_by`, `updated_by`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 11, '1', '4', '360009', 11, 97, NULL, '2020-06-10 05:52:43', '2020-06-10 05:53:03', NULL),
(2, 15, '1', '3', '380001', 104, 104, NULL, '2020-06-13 11:41:04', '2020-06-13 11:41:04', NULL),
(3, 20, '1', '2', '1234545', 20, 109, NULL, '2020-06-21 10:59:55', '2020-08-01 17:27:41', NULL),
(4, 23, '1', '3', '678867899', 23, 115, NULL, '2020-06-22 09:35:28', '2020-06-22 13:09:41', NULL),
(5, 48, '5', '35', '11005', 368, 368, NULL, '2020-10-23 01:49:46', '2020-10-23 01:49:46', NULL),
(6, 49, '1', '829', '360005', 371, 371, NULL, '2020-10-27 13:22:24', '2020-10-27 13:22:24', NULL),
(7, 52, '3', '1661', '673008', 376, 376, NULL, '2020-10-28 22:37:21', '2020-10-28 22:37:21', NULL),
(8, 58, '1', '659', '360008', 389, 389, NULL, '2020-11-09 03:29:57', '2020-11-09 03:29:57', NULL),
(9, 97, NULL, NULL, '360410', 389, 389, NULL, '2020-11-13 22:28:39', '2020-11-13 22:28:39', NULL),
(10, 97, NULL, NULL, '360411', 389, 389, NULL, '2020-11-13 22:30:25', '2020-11-13 22:30:25', NULL),
(11, 97, NULL, NULL, '360412', 389, 389, NULL, '2020-11-13 22:32:19', '2020-11-13 22:32:19', NULL),
(12, 97, NULL, NULL, '360413', 389, 389, NULL, '2020-11-13 23:25:10', '2020-11-13 23:25:10', NULL),
(13, 97, NULL, NULL, '360414', 389, 389, NULL, '2020-11-13 23:25:10', '2020-11-13 23:25:10', NULL),
(14, 97, NULL, NULL, '360415', 389, 389, NULL, '2020-11-13 23:25:10', '2020-11-13 23:25:10', NULL),
(15, 115, NULL, NULL, '695551', NULL, NULL, NULL, '2021-01-14 15:21:54', '2021-01-14 15:21:54', NULL),
(16, 44, NULL, NULL, '44452858', NULL, NULL, NULL, '2021-01-14 15:49:28', '2021-01-14 15:49:28', NULL),
(17, 44, NULL, NULL, '88888', NULL, NULL, NULL, '2021-01-14 15:50:30', '2021-01-14 15:50:30', NULL),
(18, 44, NULL, NULL, '8888', NULL, NULL, NULL, '2021-01-14 17:01:58', '2021-01-14 17:01:58', NULL),
(19, 44, NULL, NULL, 'undefined', NULL, NULL, NULL, '2021-01-14 17:03:04', '2021-01-14 17:03:04', NULL),
(20, 44, NULL, NULL, '77', NULL, NULL, NULL, '2021-01-14 17:10:48', '2021-01-14 17:10:48', NULL),
(21, 44, NULL, NULL, '77ii', NULL, NULL, NULL, '2021-01-14 17:10:56', '2021-01-14 17:10:56', NULL),
(22, 0, NULL, NULL, 'undefined', NULL, NULL, NULL, '2021-01-14 20:10:52', '2021-01-14 20:10:52', NULL),
(23, 0, NULL, NULL, '44444', NULL, NULL, NULL, '2021-01-14 20:10:57', '2021-01-14 20:10:57', NULL),
(24, 0, NULL, NULL, '4111', NULL, NULL, NULL, '2021-01-14 20:11:16', '2021-01-14 20:11:16', NULL),
(25, 44, NULL, NULL, '695574', NULL, NULL, NULL, '2021-01-14 20:12:40', '2021-01-14 20:12:40', NULL),
(26, 0, NULL, NULL, '88888', NULL, NULL, NULL, '2021-01-14 20:25:00', '2021-01-14 20:25:00', NULL),
(27, 0, NULL, NULL, '88888', NULL, NULL, NULL, '2021-01-14 20:28:17', '2021-01-14 20:28:17', NULL),
(28, 0, NULL, NULL, '88888', NULL, NULL, NULL, '2021-01-14 20:28:35', '2021-01-14 20:28:35', NULL),
(29, 0, NULL, NULL, '88888', NULL, NULL, NULL, '2021-01-14 20:28:36', '2021-01-14 20:28:36', NULL),
(30, 0, NULL, NULL, '88888', NULL, NULL, NULL, '2021-01-14 20:28:54', '2021-01-14 20:28:54', NULL),
(31, 76, NULL, NULL, '695551', NULL, NULL, NULL, '2021-01-14 20:34:38', '2021-01-14 20:34:38', NULL),
(32, 0, NULL, NULL, '7777', NULL, NULL, NULL, '2021-01-14 20:56:42', '2021-01-14 20:56:42', NULL),
(33, 88, NULL, NULL, '695574', NULL, NULL, NULL, '2021-01-14 21:42:12', '2021-01-14 21:42:12', NULL),
(34, 95, NULL, NULL, '22', NULL, NULL, NULL, '2021-01-14 22:18:48', '2021-01-14 22:18:48', NULL),
(35, 95, NULL, NULL, '2244', NULL, NULL, NULL, '2021-01-14 22:18:51', '2021-01-14 22:18:51', NULL),
(36, 96, NULL, NULL, '55', NULL, NULL, NULL, '2021-01-14 22:23:26', '2021-01-14 22:23:26', NULL),
(37, 96, NULL, NULL, '255', NULL, NULL, NULL, '2021-01-14 22:23:32', '2021-01-14 22:23:32', NULL),
(38, 102, NULL, NULL, '673008', NULL, NULL, NULL, '2021-01-14 22:34:02', '2021-01-14 22:34:02', NULL),
(39, 102, NULL, NULL, '673008', NULL, NULL, NULL, '2021-01-14 22:34:04', '2021-01-14 22:34:04', NULL),
(40, 102, NULL, NULL, '673008', NULL, NULL, NULL, '2021-01-14 22:34:06', '2021-01-14 22:34:06', NULL),
(41, 102, NULL, NULL, '673008', NULL, NULL, NULL, '2021-01-14 22:34:06', '2021-01-14 22:34:06', NULL),
(42, 102, NULL, NULL, '673001', NULL, NULL, NULL, '2021-01-14 22:34:13', '2021-01-14 22:34:13', NULL),
(43, 103, NULL, NULL, '695551', NULL, NULL, NULL, '2021-01-14 23:51:02', '2021-01-14 23:51:02', NULL),
(44, 109, NULL, NULL, '695574', NULL, NULL, NULL, '2021-01-15 19:28:08', '2021-01-15 19:28:08', NULL),
(45, 114, NULL, NULL, '673001', NULL, NULL, NULL, '2021-01-17 19:09:21', '2021-01-17 19:09:21', NULL),
(46, 114, NULL, NULL, '673002', NULL, NULL, NULL, '2021-01-17 19:09:25', '2021-01-17 19:09:25', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_menu`
--
ALTER TABLE `admin_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_operation_log`
--
ALTER TABLE `admin_operation_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `admin_operation_log_user_id_index` (`user_id`);

--
-- Indexes for table `admin_permissions`
--
ALTER TABLE `admin_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admin_permissions_name_unique` (`name`),
  ADD UNIQUE KEY `admin_permissions_slug_unique` (`slug`);

--
-- Indexes for table `admin_roles`
--
ALTER TABLE `admin_roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admin_roles_name_unique` (`name`),
  ADD UNIQUE KEY `admin_roles_slug_unique` (`slug`);

--
-- Indexes for table `admin_role_menu`
--
ALTER TABLE `admin_role_menu`
  ADD KEY `admin_role_menu_role_id_menu_id_index` (`role_id`,`menu_id`);

--
-- Indexes for table `admin_role_permissions`
--
ALTER TABLE `admin_role_permissions`
  ADD KEY `admin_role_permissions_role_id_permission_id_index` (`role_id`,`permission_id`);

--
-- Indexes for table `admin_role_users`
--
ALTER TABLE `admin_role_users`
  ADD KEY `admin_role_users_role_id_user_id_index` (`role_id`,`user_id`);

--
-- Indexes for table `admin_users`
--
ALTER TABLE `admin_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admin_users_username_unique` (`username`);

--
-- Indexes for table `admin_user_permissions`
--
ALTER TABLE `admin_user_permissions`
  ADD KEY `admin_user_permissions_user_id_permission_id_index` (`user_id`,`permission_id`);

--
-- Indexes for table `Available_lab_patient`
--
ALTER TABLE `Available_lab_patient`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bank_information`
--
ALTER TABLE `bank_information`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `booking_price_admin`
--
ALTER TABLE `booking_price_admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `booking_request`
--
ALTER TABLE `booking_request`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `business_register`
--
ALTER TABLE `business_register`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `business_type`
--
ALTER TABLE `business_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `buy_plan`
--
ALTER TABLE `buy_plan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `buy_plan_dr`
--
ALTER TABLE `buy_plan_dr`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `consultant`
--
ALTER TABLE `consultant`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `counselings_list`
--
ALTER TABLE `counselings_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coupan`
--
ALTER TABLE `coupan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `couponrates`
--
ALTER TABLE `couponrates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coupons`
--
ALTER TABLE `coupons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `create_offer_360`
--
ALTER TABLE `create_offer_360`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `doctor_availability`
--
ALTER TABLE `doctor_availability`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `doctor_details`
--
ALTER TABLE `doctor_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Doctor_withdraw`
--
ALTER TABLE `Doctor_withdraw`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Examination_notes_patient`
--
ALTER TABLE `Examination_notes_patient`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `followup_period`
--
ALTER TABLE `followup_period`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `H360_coupan_redeem`
--
ALTER TABLE `H360_coupan_redeem`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `haspatal_360_register`
--
ALTER TABLE `haspatal_360_register`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `haspatal_regsiter`
--
ALTER TABLE `haspatal_regsiter`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `home_care_list`
--
ALTER TABLE `home_care_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hospitalAddFund`
--
ALTER TABLE `hospitalAddFund`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hospitalWithdraw`
--
ALTER TABLE `hospitalWithdraw`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hospital_register`
--
ALTER TABLE `hospital_register`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `h_plan`
--
ALTER TABLE `h_plan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `imaging_list`
--
ALTER TABLE `imaging_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `key_points_patient`
--
ALTER TABLE `key_points_patient`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lab_test`
--
ALTER TABLE `lab_test`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `language`
--
ALTER TABLE `language`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `licence_type`
--
ALTER TABLE `licence_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `medicine_list`
--
ALTER TABLE `medicine_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `my_favourite`
--
ALTER TABLE `my_favourite`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `my_note_patient`
--
ALTER TABLE `my_note_patient`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_personal_access_clients_client_id_index` (`client_id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `offer`
--
ALTER TABLE `offer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Order_360`
--
ALTER TABLE `Order_360`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_query`
--
ALTER TABLE `order_query`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`(191));

--
-- Indexes for table `patient_add_fund`
--
ALTER TABLE `patient_add_fund`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `patient_details`
--
ALTER TABLE `patient_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `patient_refund_request`
--
ALTER TABLE `patient_refund_request`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_status`
--
ALTER TABLE `payment_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `perceived_patient`
--
ALTER TABLE `perceived_patient`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_unique` (`name`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_role_id_foreign` (`role_id`);

--
-- Indexes for table `plan_master`
--
ALTER TABLE `plan_master`
  ADD PRIMARY KEY (`pl_id`);

--
-- Indexes for table `prescription_details`
--
ALTER TABLE `prescription_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `present_complaint_patient`
--
ALTER TABLE `present_complaint_patient`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `present_medications_patient`
--
ALTER TABLE `present_medications_patient`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `referral_specialities`
--
ALTER TABLE `referral_specialities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reports`
--
ALTER TABLE `reports`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `report_controllers`
--
ALTER TABLE `report_controllers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `review`
--
ALTER TABLE `review`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `review_360s`
--
ALTER TABLE `review_360s`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `review_hospital`
--
ALTER TABLE `review_hospital`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `role_user_role_id_foreign` (`role_id`);

--
-- Indexes for table `service_details`
--
ALTER TABLE `service_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `set_consult_prices`
--
ALTER TABLE `set_consult_prices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `set_service_price`
--
ALTER TABLE `set_service_price`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `specilities`
--
ALTER TABLE `specilities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `state`
--
ALTER TABLE `state`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `suggested_imagings_patient`
--
ALTER TABLE `suggested_imagings_patient`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `suggested_investigations_patient`
--
ALTER TABLE `suggested_investigations_patient`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `suggested_specialists_patient`
--
ALTER TABLE `suggested_specialists_patient`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `suggested_therapies_patient`
--
ALTER TABLE `suggested_therapies_patient`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `support`
--
ALTER TABLE `support`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `task`
--
ALTER TABLE `task`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tax`
--
ALTER TABLE `tax`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp_hasaptal_data`
--
ALTER TABLE `temp_hasaptal_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `therapies_list`
--
ALTER TABLE `therapies_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `time_slot`
--
ALTER TABLE `time_slot`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wallet_360`
--
ALTER TABLE `wallet_360`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wallet_360_transaction`
--
ALTER TABLE `wallet_360_transaction`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Your_coverage_360`
--
ALTER TABLE `Your_coverage_360`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_menu`
--
ALTER TABLE `admin_menu`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `admin_operation_log`
--
ALTER TABLE `admin_operation_log`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `admin_permissions`
--
ALTER TABLE `admin_permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `admin_roles`
--
ALTER TABLE `admin_roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `admin_users`
--
ALTER TABLE `admin_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Available_lab_patient`
--
ALTER TABLE `Available_lab_patient`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `bank_information`
--
ALTER TABLE `bank_information`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `booking_price_admin`
--
ALTER TABLE `booking_price_admin`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `booking_request`
--
ALTER TABLE `booking_request`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=206;

--
-- AUTO_INCREMENT for table `business_register`
--
ALTER TABLE `business_register`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT for table `business_type`
--
ALTER TABLE `business_type`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `buy_plan`
--
ALTER TABLE `buy_plan`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `buy_plan_dr`
--
ALTER TABLE `buy_plan_dr`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4818;

--
-- AUTO_INCREMENT for table `consultant`
--
ALTER TABLE `consultant`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `counselings_list`
--
ALTER TABLE `counselings_list`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `coupan`
--
ALTER TABLE `coupan`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `couponrates`
--
ALTER TABLE `couponrates`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `coupons`
--
ALTER TABLE `coupons`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `create_offer_360`
--
ALTER TABLE `create_offer_360`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `doctor_availability`
--
ALTER TABLE `doctor_availability`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=95;

--
-- AUTO_INCREMENT for table `doctor_details`
--
ALTER TABLE `doctor_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=162;

--
-- AUTO_INCREMENT for table `Doctor_withdraw`
--
ALTER TABLE `Doctor_withdraw`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `Examination_notes_patient`
--
ALTER TABLE `Examination_notes_patient`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `followup_period`
--
ALTER TABLE `followup_period`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `H360_coupan_redeem`
--
ALTER TABLE `H360_coupan_redeem`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `haspatal_360_register`
--
ALTER TABLE `haspatal_360_register`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=299;

--
-- AUTO_INCREMENT for table `haspatal_regsiter`
--
ALTER TABLE `haspatal_regsiter`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `home_care_list`
--
ALTER TABLE `home_care_list`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `hospitalAddFund`
--
ALTER TABLE `hospitalAddFund`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `hospitalWithdraw`
--
ALTER TABLE `hospitalWithdraw`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `hospital_register`
--
ALTER TABLE `hospital_register`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `h_plan`
--
ALTER TABLE `h_plan`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `imaging_list`
--
ALTER TABLE `imaging_list`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `key_points_patient`
--
ALTER TABLE `key_points_patient`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `lab_test`
--
ALTER TABLE `lab_test`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `language`
--
ALTER TABLE `language`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `licence_type`
--
ALTER TABLE `licence_type`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `medicine_list`
--
ALTER TABLE `medicine_list`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=109;

--
-- AUTO_INCREMENT for table `my_favourite`
--
ALTER TABLE `my_favourite`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- AUTO_INCREMENT for table `my_note_patient`
--
ALTER TABLE `my_note_patient`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `offer`
--
ALTER TABLE `offer`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `Order_360`
--
ALTER TABLE `Order_360`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `order_query`
--
ALTER TABLE `order_query`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `patient_add_fund`
--
ALTER TABLE `patient_add_fund`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `patient_details`
--
ALTER TABLE `patient_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=271;

--
-- AUTO_INCREMENT for table `patient_refund_request`
--
ALTER TABLE `patient_refund_request`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `payment_status`
--
ALTER TABLE `payment_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `perceived_patient`
--
ALTER TABLE `perceived_patient`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=77;

--
-- AUTO_INCREMENT for table `plan_master`
--
ALTER TABLE `plan_master`
  MODIFY `pl_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `prescription_details`
--
ALTER TABLE `prescription_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `present_complaint_patient`
--
ALTER TABLE `present_complaint_patient`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;

--
-- AUTO_INCREMENT for table `present_medications_patient`
--
ALTER TABLE `present_medications_patient`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `referral_specialities`
--
ALTER TABLE `referral_specialities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `reports`
--
ALTER TABLE `reports`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `report_controllers`
--
ALTER TABLE `report_controllers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `review`
--
ALTER TABLE `review`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `review_360s`
--
ALTER TABLE `review_360s`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `review_hospital`
--
ALTER TABLE `review_hospital`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `service_details`
--
ALTER TABLE `service_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `set_consult_prices`
--
ALTER TABLE `set_consult_prices`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `set_service_price`
--
ALTER TABLE `set_service_price`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `specilities`
--
ALTER TABLE `specilities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT for table `state`
--
ALTER TABLE `state`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `suggested_imagings_patient`
--
ALTER TABLE `suggested_imagings_patient`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `suggested_investigations_patient`
--
ALTER TABLE `suggested_investigations_patient`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `suggested_specialists_patient`
--
ALTER TABLE `suggested_specialists_patient`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `suggested_therapies_patient`
--
ALTER TABLE `suggested_therapies_patient`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `support`
--
ALTER TABLE `support`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `task`
--
ALTER TABLE `task`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `tax`
--
ALTER TABLE `tax`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `temp_hasaptal_data`
--
ALTER TABLE `temp_hasaptal_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `therapies_list`
--
ALTER TABLE `therapies_list`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `time_slot`
--
ALTER TABLE `time_slot`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=145;

--
-- AUTO_INCREMENT for table `wallet_360`
--
ALTER TABLE `wallet_360`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `wallet_360_transaction`
--
ALTER TABLE `wallet_360_transaction`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Your_coverage_360`
--
ALTER TABLE `Your_coverage_360`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
